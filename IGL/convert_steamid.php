<form action="" method="POST">
    <textarea name="steam_ids"></textarea><br />
    <input type="checkbox" name="link" value="1"> Add hyperlink<br/>
    <input type="submit" value="Submit">
</form>

<?php

/*
 * @copyright 2013 PlayIGL.com
 * @author Darryl Allen
 */

if($_POST)
{
    include('includes/functions.php');
    $list = explode("\n",$_POST['steam_ids']);
    
    foreach($list as $v)
    {
        $v = convert32to64($v);
        if($_POST['link'])
            echo "<a href='http://steamcommunity.com/profiles/{$v}'>{$v}</a><br />";
        else
            echo $v."<br />";    
    }
        
}

?>
