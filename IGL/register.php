<?php
error_reporting(E_ALL ^ E_NOTICE);
if($_POST['register'])
{
    include('./includes/config.php');
    include('./includes/functions.php');
    include('./model/register.class.php');
    include('./model/mysql.class.php');
    include('./model/activity.class.php');
    
    $register = new register();
    $return = $register->addUser($_POST['username'], $_POST['email'], $_POST['password']);
    $result = json_decode($return,true);
    if($result['success'])
    { ?>
    <script type="text/javascript">
        <!--
        window.location = "<? echo BASEURL.'beta_login.php?username='.$_POST['username']; ?>"
        //-->
    </script>
    <?php
    die();
    }
}


?>
<!DOCTYPE html>
<html>
  <head>
   <title>International Gaming League - Beta Participant Login</title>
   <!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/stylesheet.css" rel="stylesheet">
  </head>
  <body class="betabackground">
   <script src="http://code.jquery.com/jquery-latest.js"></script>
   <script src="js/bootstrap.min.js"></script>
   <script src="js/bootstrap-modal.js"></script>
   <script src="js/jquery.placeholder-1.0.6.js"></script>
   <div class="container" style="vertical-align: middle;">
    <div style="height:150px; width:100%; text-align: center; margin-top:40px;"></div>
    <div class="row beta_logo"> 
        <div class="offset4 span4"><img src="img/IGL_beta-login.png" alt="IGL Beta Registration"></div>
    </div>
        <div class="row-fluid"> 
        <div class="offset4 span4 beta_login">
            <form action="" method="post" > 
                <fieldset> 
                    <legend>BETA REGISTRATION</legend> 
                    <div class="control center"> 
                            <input name="username" tabindex="1" maxlength="100" placeholder="Username..." holder="Username..." type="text" class="input-large" id="register_username" /> 
                    </div> 
                    <div class="control center"> 
                            <input name="email" tabindex="2" maxlength="100" placeholder="Email..." holder="Email..." type="text" class="input-large" id="register_email" value="<?php echo $_GET['email']; ?>" /> 
                    </div>
                    <div class="control center"> 
                            <input name="password" tabindex="3" maxlength="100" placeholder="Password..." holder="Password..." type="password" class="input-large" id="register_password" /> 
                    </div>
                        <p align="center">
                        <input class="btn btn-danger" tabindex="4" type="submit" name="register" value="REGISTER">
                        <input class="btn btn-info" tabindex="5" data-toggle="modal" href="#invite" type="submit" value="Get an invite!">
                        
                        </p>
                </fieldset> 
            </form> 
        </div>
    </div> 
<?php if($_POST['register'] && false==$result['success']) {?><div class="alert alert-error" style="margin-top:40px; width:400px;margin-left:auto;margin-right: auto;"><h4>Registration failed.</h4><ul><?php echo $result['error']; ?></ul></div><?php } ?>        
    
    
    
<div id="invite" class="modal hide fade in" style="display: none; ">
            <div class="modal-header">
              <a class="close" data-dismiss="modal">x</a>
              <h3>Getting &nbsp an &nbsp IGL &nbsp invite...</h3>
            </div>
            <div class="modal-body">
              <h4>Ask &nbsp your &nbsp friends</h4>
              <p>This is the fastest way to get activated.<br> Thousands of invites have already been given out. Ask around, you'd be surprised who has invites.</p>
              <center><h4>-OR-</h4></center>
              <h4>Hit &nbsp us &nbsp up &nbsp on &nbsp Twitter</h4>
              <p><a href="https://twitter.com/intent/tweet?screen_name=PlayIGL&text=I%20want%20a%20#PlayIGL%20invite!" class="twitter-mention-button" data-related="PlayIGL">Tweet to @PlayIGL</a>
<script type="text/javascript">!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script></p>
            </div>
            <div class="modal-footer">
              <a href="#" class="btn" data-dismiss="modal">Close</a>
            </div>
</div>

</div> 
  </body>
</html>
