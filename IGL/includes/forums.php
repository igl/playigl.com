<?php
class Forums {
  //Declarations
  protected $db;
  protected $available_inputs = array('post', 'thread', 'tag', 'search', 'new_post', 'delete_post', 'edit_post', 'comment', 'vote', 'index');
  protected $player_id = NULL;
  protected $current_thread_id = NULL;
  protected $page_limit = 30;
  protected $data = array();
  protected $tags = NULL;
  protected $offset = 0;
  protected $offset_increment = 40;
  protected $templates;
  protected $tbl_name = array(
    "tbl_threads" => "thread",
    "tbl_votes" => "vote",
    "tbl_tags" => "tag_name",
    "tbl_tag_join" => "tag",
    "vw_players" => "players",
    "vw_threads" => "threads",
    "vw_comments" => "comments"
  );

  //Initialize
  public function __construct() {
    //Constructor
    $this->db = new MySQL(FORUMS);
    $this->_parseInputs();
    $this->_getTags();
    $this->_saveTemplates();
    $this->player_id = isLoggedIn() ? $_SESSION['playerid'] : NULL;
  }

  //Close
  public function __destroy() {
    unset($this->db);
    unset($this->available_inputs);
    unset($this->player_id);
    unset($this->current_thread_id);
    unset($this->page_limit);
    unset($this->data);
    unset($this->tags);
    unset($this->offset);
    unset($this->offset_increment);
    unset($this->templates);
    unset($this->tbl_name);
  }

  //Retrieve member variable
  public function __get($nm) {
    return $this->$nm;
  }

  //Set member variables
  public function __set($nm, $val) {
    $this->$nm = $val;
  }

  //Check if member variable is set
  public function __isset($nm) {
    return isset($this->$nm);
  }

  //Unset member variable
  public function __unset($nm) {
    unset($nm);
  }

  private function _parseInputs() {
    foreach ($this->available_inputs as $key) {
      $this->data[$key] = NULL;

      if (isset($_REQUEST[$key])) {
        $val = $_REQUEST[$key];

        switch ($key) {
          case 'post':
            $this->data[$key] = 'new';
            break;

          case 'thread':
          case 'index':
            $this->data[$key] = (int) $val;
            break;

          case 'tag':
            $this->data[$key] = urldecode($val);
            break;

          case 'search':
            $data = array();

            foreach ($_REQUEST as $nKey => $nVal) {
              $data[$nKey] = NULL;

              switch ($nKey) {
                case 's':
                  $data[$nKey] = urldecode($nVal);
                  break;
              }
            }

            $this->data[$key] = $data;
            break;

          case 'new_post':
          case 'delete_post':
          case 'edit_post':
          case 'comment':
          case 'vote':
            $data = array();

            if (is_array($_REQUEST[$key])) {
              foreach ($_REQUEST[$key] as $nKey => $nVal) {
                $data[$nKey] = NULL;

                switch ($nKey) {
                  case 'title':
                  case 'content':
                  case 'tags':
                    $data[$nKey] = !empty($nVal) ? $nVal : NULL;
                    break;

                  case 'id':
                  case 'media_id':
                  case 'thread_id':
                  case 'preview':
                  case 'value':
                    $data[$nKey] = (int) $nVal;
                    break;
                }
              }
            }

            $this->data[$key] = $data;
            break;
        }
      }
    }

    if (!isset($data['index']))
      $data['index'] = 1;
  }

  public function getForums() {
    if (!is_null($this->player_id) && $this->data['post'] == 'new')
      return $this->getNewPostHTML();

    if (!is_null($this->data['thread']))
      return $this->getThreadHTML();

    return $this->getForumsHTML();
  }

  public function getCustomHTML($query_type) {
    $return = array();

    switch ($query_type) {
      case 'top players':
        $this->db->ExecuteSQL(
          "SELECT
            t.player_id,
            p.player_avatar,
            p.player_name,
            p.twitter,
            SUM(v. VALUE) AS score
          FROM
            vote v
          INNER JOIN thread t ON v.media_id = t.id
          INNER JOIN `players` p ON t.player_id = p.id
          GROUP BY
            t.player_id
          ORDER BY
            score DESC
          LIMIT 0,5;"
        );

        if ($this->db->iRecords()) {
          $players = $this->db->ArrayResults();

          foreach ($players as $key => $p) {
            $twitter = "";

            if (!is_null($p['twitter']))
              $twitter = $this->_getTemplate("twitter", array("player_twitter" => $p['twitter']));

            $links = array(
              "player" => $this->buildForumUserLink($p['player_name'])
            );

            $tpl_data = array(
              "css_class" => $key == count($players) - 1 ? " class='last'" : "",
              "points" => $p['score'],
              "point_plural" => $p['score'] == 1 ? "" : "s",
              "player_link" => $links['player'],
              "player_avatar" => $this->_getPlayerAvatar($p['player_avatar']),
              "player_name" => $p['player_name'],
              "twitter" => $twitter
            );
            $tpl = $this->_getTemplate("top_player", $tpl_data);

            $return[] = $tpl;
          }
        }
        break;

      case 'newest threads':
        $this->db->ExecuteSQL(
          "SELECT *
            FROM `threads` t
            WHERE `t`.`status` = 1
            ORDER BY `rts` DESC
            LIMIT 0,5;"
        );

        if ($this->db->iRecords()) {
          $threads = $this->db->ArrayResults();

          foreach ($threads as $key => $thread) {
            $links = array(
              "thread" => $this->buildForumThreadLink($thread['id'], $thread['title_friendly']),
              "player" => $this->buildForumUserLink($thread['player_name']),
            );

            $tpl_data = array(
              "css_class" => $key == count($threads) - 1 ? " class='last'" : "",
              "thread_id" => $thread['id'],
              "thread_link" => $links['thread'],
              "thread_title" => $thread['title'],
              "thread_value" => $thread['value'],
              "player_link" => $links['player'],
              "player_avatar" => $this->_getPlayerAvatar($thread['player_avatar']),
              "player_name" => $thread['player_name'],
              "post_rts" => $this->_getTemplate('post_rts', array("post_rts" => $this->_formatDate($thread['rts']), "post_date_unix" => strtotime($thread['rts']))),
              "comment_count" => $thread['comments'],
              "comment_plural" => $thread['comments'] == 1 ? "" : "s"
            );
            $return[] .= $this->_getTemplate('thread_new', $tpl_data);
          }
        }
        break;
    }
    return $return;
  }

  public function doAction() {
    if (is_null($this->player_id))
      return array("success" => false, "error" => "Please log in to use this feature.");

    if (!is_null($this->data['new_post']) && !empty($this->data['new_post'])) {
      if ($this->data['new_post']['preview'] == 1)
        return $this->doPreviewPost();
      else
        return $this->doNewPost('new_post');
    }

    if (!is_null($this->data['delete_post']) && !empty($this->data['delete_post'])) {
      return $this->doDeletePost();
    }

    if (!is_null($this->data['edit_post']) && !empty($this->data['edit_post'])) {
      return $this->doEditPost();
    }

    if (!is_null($this->data['comment']) && !empty($this->data['comment']))
      return $this->doNewPost('comment');

    if (!is_null($this->data['vote']) && !empty($this->data['vote']))
      return $this->doVote();
  }

  private function doNewPost($type) {
    $data = $this->data[$type];
    $tag_ids = NULL;
    $return = array("success" => false);

    if (($type == 'new_post' && is_null($data['title']) || is_null($data['content'])) || ($type == 'comment' && is_null($data['content']))) {
      $return['error'] = "Invalid entry. Please fill out required fields.";
      return $return;
    }

    if ($type == 'new_post' && isset($data['tags'])) {
      $tag_ids = $data['tags'];
      unset($data['tags']);
    }

    if ($type == "new_post")
      $data['title_friendly'] = $this->_friendlyUrl($data['title']);

    $data['player_id'] = $this->player_id;
    unset($data['preview']);

    $insert = $this->db->Insert($data, $this->tbl_name['tbl_threads']);

    if (!$insert) {
      $return['error'] = "Post failed to create.";
      return $return;
    }

    $insert_id = $this->db->lastId();

    if (!is_null($tag_ids)) {
      foreach ($tag_ids as $t_id) {
        $tag_id = intval($t_id);

        if ($tag_id > 0)
          $this->db->Insert(array(
            "tag_id" => intval($t_id),
            "thread_id" => $insert_id
            ), $this->tbl_name['tbl_tag_join']);
      }
    }

    $vote = $this->db->Insert(array(
      "media_id" => $insert_id,
      "player_id" => $this->player_id,
      "value" => 1
      ), $this->tbl_name['tbl_votes']);

    if (!$vote) {
      $return['error'] = "You failed to cast your vote. Please contact tech support.";
      return $return;
    }

    $this->db = new MySQL(MYSQL_DATABASE);
    $this->db->ExecuteSQL(
      "SELECT posts FROM player WHERE playerid = {$this->player_id} LIMIT 1;"
    );

    $player = $this->db->ArrayResult();
    $this->db->Update("player", array("posts" => $player['posts'] + 1), array("playerid" => $this->player_id));

    $this->db = new MySQL(FORUMS);

    if ($type == 'new_post')
      return array("success" => true, "redirect" => $this->buildForumThreadLink($insert_id, $data['title_friendly']), "msg" => "Your post was created.");
    else {
      $d = array();
      $op_msg = "";

      if ($data['media_id'] == $data['thread_id']) {
        $this->db->ExecuteSQL(
          "SELECT title, player_id FROM thread WHERE id = {$data['thread_id']} AND status = 1"
        );
        $d = $this->db->ArrayResult();
        $op_msg = "Someone commented on your thread '{$d['title']}'";
      } else {
        $this->db->ExecuteSQL(
          "SELECT z.title, t.player_id
          FROM thread t
          JOIN thread z ON z.id = t.thread_id
          WHERE t.id = {$data['media_id']}
          AND t.status = 1"
        );
        $d = $this->db->ArrayResult();
        $op_msg = "Someone commented on your comment in thread '{$d['title']}'";
      }

      return array("success" => true, "op" => $d['player_id'], "op_msg" => $op_msg, "msg" => "Your comment was created.");
    }
  }

  private function doPreviewPost() {
    $data = $this->data['new_post'];

    $data['title_friendly'] = $this->_friendlyUrl($data['title']);
    $data['player_id'] = $this->player_id;
    $data['player_name'] = $_SESSION['username'];
    $data['player_avatar'] = $_SESSION['avatar'];

    $links = array(
      "player" => $this->buildForumUserLink($data['player_name']),
    );

    $tpl_data = array(
      "thread_id" => $thread['id'],
      "vote_html" => $this->_getVotingHTML(0),
      "tag_html" => $this->getTagsHTML(join(",", $data['tags'])),
      "thread_title" => $data['title'],
      "thread_value" => 0,
      "point_plural" => "s",
      "player_link" => $links['player'],
      "player_avatar" => $this->_getPlayerAvatar($data['player_avatar']),
      "player_name" => $data['player_name'],
      "post_rts" => $this->_getTemplate('post_rts', array("post_rts" => $this->_formatDate(time()), "post_date_unix" => strtotime(strtotime(time())))),
      "content" => $this->_parseContent($data['content'])
    );

    return array("success" => true, "preview" => $this->_getTemplate('preview', $tpl_data));
  }

  private function doEditPost() {
    $data = $this->data['edit_post'];
    $return = array("success" => false);

    $this->db->ExecuteSQL(
      "SELECT id, player_id, thread_id, status FROM `" . FORUMS . "`.{$this->tbl_name['tbl_threads']} WHERE id = {$data['id']} LIMIT 1;"
    );
    $thread = $this->db->ArrayResult();

    if ($thread['player_id'] == $this->player_id)
      $update = $this->db->Update($this->tbl_name['tbl_threads'], array("content" => $data['content'], "edited" => 1, "edited_rts" => date("Y-m-d H:i:s")), array("id" => $data['id']));

    return array("success" => true, "msg" => "Thread successfully edited", "txt" => $this->_parseContent($data['content']));
  }

  private function doDeletePost() {
    $data = $this->data['delete_post'];
    $return = array("success" => false);

    $this->db->ExecuteSQL(
      "SELECT id, player_id, thread_id, status FROM `" . FORUMS . "`.{$this->tbl_name['tbl_threads']} WHERE id = {$data['thread_id']} LIMIT 1;"
    );
    $thread = $this->db->ArrayResult();

    if ($thread['player_id'] == $this->player_id)
      $this->db->Update($this->tbl_name['tbl_threads'], array("status" => 0), array("id" => $thread['id']));

    if (is_null($thread['thread_id']))
      return array("success" => true, "msg" => "Thread successfully deleted", "redirect" => "/forums/");
    else
      return array("success" => true, "msg" => "Thread successfully deleted");
  }

  private function doVote() {
    $data = $this->data['vote'];
    $return = array("success" => false);

    if (is_null($data['media_id']) || is_null($data['value'])) {
      $return['error'] = "Invalid entry. Please fill out required fields.";
      return $return;
    }

    $this->db->ExecuteSQL(
      "SELECT id FROM {$this->tbl_name['tbl_votes']} WHERE media_id = {$data['media_id']} AND player_id = {$this->player_id} LIMIT 1;"
    );

    if ($this->db->iRecords()) {
      $record = $this->db->ArrayResult();
      $update = $this->db->Update("vote", array("value" => $data['value']), array("id" => $record['id']));

      if ($update)
        return array("success" => true, "msg" => "Successfully cast vote", "mod" => $data['value']);
      else {
        $return['error'] = "There was an error. Please try again later.";
        return $return;
      }
    } else {
      $data['player_id'] = $this->player_id;

      $insert = $this->db->Insert($data, $this->tbl_name['tbl_votes']);

      if ($insert)
        return array("success" => true, "msg" => "Successfully cast vote", "mod" => $data['value']);
      else {
        $return['error'] = "There was an error creating record. Please try again later.";
        return $return;
      }
    }

    return $return;
  }

  private function getNewPostHTML() {
    $return = array(
      "view" => "view/forum_new_post.php"
    );

    return $return;
  }

  private function getThreadHTML() {
    $return = array(
      "view" => "view/forum_post.php",
      "html" => ""
    );

    $this->db->ExecuteSQL(
      "SELECT * FROM `" . FORUMS . "`.{$this->tbl_name['vw_threads']} WHERE id = {$this->data['thread']} AND status = 1 LIMIT 1;"
    );

    $thread = $this->db->ArrayResult();
    $this->current_thread_id = $thread['id'];

    $links = array(
      "player" => $this->buildForumUserLink($thread['player_name']),
    );

    $tpl_data = array(
      "thread_id" => $thread['id'],
      "vote_html" => $this->_getVotingHTML($thread['id']),
      "tag_html" => $this->getTagsHTML($thread['tags']),
      "thread_title" => $thread['title'],
      "thread_value" => $thread['value'],
      "point_plural" => $thread['value'] == 1 ? "" : "s",
      "player_link" => $links['player'],
      "player_avatar" => $this->_getPlayerAvatar($thread['player_avatar']),
      "player_name" => $thread['player_name'],
      "post_rts" => $this->_getTemplate('post_rts', array("post_rts" => $this->_formatDate($thread['rts']), "post_date_unix" => strtotime($thread['rts']))),
      "content" => $this->_parseContent($thread['content']),
      "comment_form" => $this->_getCommentFormHTML($thread['id']),
      "admin" => $this->_getThreadAdminHTML($thread['player_id']),
      "edited" => $thread['edited'] ? "Last edited on " . $this->_getTemplate('post_rts', array("post_rts" => $this->_formatDate($thread['edited_rts']), "post_date_unix" => strtotime($thread['edited_rts']))) : "",
      "edit_content" => $this->_getTemplate('edit_content', array("id" => $thread['id'])),
      "tweet" => $this->_getTemplate('tweet', array("url" => BASEURL . substr($this->buildForumThreadLink($thread['id'], $thread['title_friendly']), 1), "txt" => $this->_shorten($thread['content'])))
    );
    $return['html'] .= $this->_getTemplate('thread', $tpl_data);
    
    if ($thread['comments'] > 0)
      $return['html'] .= $this->_getCommentsHTML($thread['id']);

    return $return;
  }

  private function getForumsHTML() {
    if (isLoggedIn())
      $this->_addOnlinePresence();

    $query = array(
      "SELECT" => "",
      "FROM" => "FROM `" . FORUMS . "`.`{$this->tbl_name['vw_threads']}` t",
      "JOIN" => array(),
      "WHERE" => "WHERE t.status = 1",
      "LIMIT" => "LIMIT " . ($this->data['index'] > 1 ? (($this->data['index'] - 1) * $this->page_limit) . "," . ($this->data['index'] * $this->page_limit) : "0," . $this->page_limit) . ";"
    );

    $return = array(
      "view" => "view/forums.php",
      "html" => "",
      "pagination" => "",
      "search" => ""
    );

    if (isset($this->data['search']))
      $return['search'] = <<< GO_BACK_LINK
        <p class="text-right" style="margin:0;"><a href="/forums/">« Back to Forums</a></p>
GO_BACK_LINK;

    if (isset($this->data['search']) && is_array($this->data['search']) && isset($this->data['search']['s']) && !empty($this->data['search']['s'])) {
      if (isset($this->data['tag']) && !empty($this->data['tag'])) {
        $query['SELECT'] = "SELECT t.*";
        $query['JOIN'][] = "LEFT JOIN `" . FORUMS . "`.`{$this->tbl_name['tbl_tag_join']}` z ON z.thread_id = t.id";
        $query['JOIN'][] = "LEFT JOIN `" . FORUMS . "`.`{$this->tbl_name['tbl_tags']}` n ON n.id = z.tag_id";
        $query['WHERE'] = "WHERE (n.name = '{$this->data['tag']}' AND t.content LIKE '%{$this->data['search']['s']}%') OR (n.name = '{$this->data['tag']}' AND t.title LIKE '%{$this->data['search']['s']}%') AND t.status = 1";
      } else {
        $query['SELECT'] = "SELECT t.*";
        $query['WHERE'] = "WHERE t.content LIKE '%{$this->data['search']['s']}%' OR t.title LIKE '%{$this->data['search']['s']}%' AND t.status = 1";
      }
    } elseif (isset($this->data['tag']) && !empty($this->data['tag'])) {
      $query['SELECT'] = "SELECT t.*";
      $query['JOIN'][] = "LEFT JOIN `" . FORUMS . "`.`{$this->tbl_name['tbl_tag_join']}` z ON z.thread_id = t.id";
      $query['JOIN'][] = "LEFT JOIN `" . FORUMS . "`.`{$this->tbl_name['tbl_tags']}` n ON n.id = z.tag_id";
      $query['WHERE'] = "WHERE n.name = '{$this->data['tag']}' AND t.status = 1";
    } else {
      $query['SELECT'] = "SELECT *";
    }

    $this->db->ExecuteSQL($this->_authorSQL($query));

    foreach ($this->db->ArrayResults() as $thread) {
      $links = array(
        "thread" => $this->buildForumThreadLink($thread['id'], $thread['title_friendly']),
        "player" => $this->buildForumUserLink($thread['player_name']),
      );

      $tpl_data = array(
        "thread_id" => $thread['id'],
        "vote_html" => $this->_getVotingHTML($thread['id']),
        "tag_html" => $this->getTagsHTML($thread['tags']),
        "thread_link" => $links['thread'],
        "thread_title" => $thread['title'],
        "thread_value" => $thread['value'],
        "point_plural" => $thread['value'] == 1 ? "" : "s",
        "player_link" => $links['player'],
        "player_avatar" => $this->_getPlayerAvatar($thread['player_avatar']),
        "player_name" => $thread['player_name'],
        "post_rts" => $this->_getTemplate('post_rts', array("post_rts" => $this->_formatDate($thread['rts']), "post_date_unix" => strtotime($thread['rts']))),
        "comment_count" => $thread['comments'],
        "comment_plural" => $thread['comments'] == 1 ? "" : "s"
      );
      $return['html'] .= $this->_getTemplate('thread_entry', $tpl_data);
    }

    if (empty($return['html']))
      $return['html'] = $this->_getTemplate('empty_forum', array());

    $return['pagination'] = $this->_getPaginationHTML($query);
    $return['viewers'] = $this->_getViewersHTML();

    return $return;
  }

  public function buildForumThreadLink($id, $title) {
    return "/forums/{$id}/{$title}/";
  }

  public function buildForumUserLink($player_name) {
    return "/player/{$player_name}";
  }

  public function buildTagLink($tag_name) {
    return "/forums/tag/" . urlencode($tag_name) . "/";
  }

  private function _authorSQL($query_arr) {
    if (!empty($query_arr['JOIN']))
      $query_arr['JOIN'] = join(" ", $query_arr['JOIN']);
    else
      unset($query_arr['JOIN']);

    return join(" ", $query_arr);
  }

  private function _saveTemplates() {
    $this->template = array();

    $this->template['comment_admin'] = <<< ADMIN
      <a class='btn btn-mini btn-danger pull-right' onclick="deletePost(this)"><i></i> Delete</a>
      <a class='btn btn-mini pull-right' onclick="editPost(this)" style='margin-right:3px;'><i></i> Edit</a>
ADMIN;

    $this->template['thread_admin'] = <<< ADMIN
        <a class='btn btn-mini' onclick="editPost(this)"><i></i> Edit</a>
        <a class='btn btn-mini btn-danger' onclick="deletePost(this)"><i></i> Delete</a>
ADMIN;

    $this->template['preview'] = <<< PREVIEW
      <div class="post" id="post_:thread_id">
          <h2>:thread_title</h2>
          <p class="byline">
              :vote_html
              <span class="points">:thread_value</span> point:point_plural by
              <a class="user-img" href=":player_link"><img src=":player_avatar"></a>
              <a class="user-link" href=":player_link">:player_name</a> on :post_rts
              :tag_html
          </p>
          <p>:content</p>
          <p></p>
      </div>
      <hr>
PREVIEW;

    $this->template['thread'] = <<< THREAD
      <div class="post" id="post_:thread_id">
          <h2>:thread_title</h2>
          <p class="byline">
              :vote_html
              <span class="points">:thread_value</span> point:point_plural by
              <a class="user-img" href=":player_link"><img src=":player_avatar"></a>
              <a class="user-link" href=":player_link">:player_name</a> on :post_rts
              :tag_html
          </p>
          <p class="forum-content">:content</p>
          <p class="forum-edit">:edit_content</p>
          <p class='text-right' style='margin-bottom:0px;'>
            :tweet
            :edited
            :admin
          </p>
      </div>
      <hr>
      :comment_form
THREAD;

    $this->template['edit_content'] = <<< EDIT
      <textarea rows="20" name="edit_post[content]" cols="40" class="span8"></textarea>
      <input type="hidden" value=":id" name="edit_post[id]">
EDIT;

    $this->template['thread_entry'] = <<< THREAD
        <tr class="post" id="post_:thread_id">
            <td>:vote_html</td>
            <td class="description">
                <div class="pull-right">:tag_html</div>
                <h5><a href=":thread_link" class="post-link">:thread_title</a></h5>
                <p class="byline">
                    <span class="points">:thread_value</span> point:point_plural by
                    <a class="user-img" href=":player_link"><img src=":player_avatar"></a>
                    <a class="user-link" href=":player_link">:player_name</a> on :post_rts with <a href=":thread_link">:comment_count comment:comment_plural</a>
                </p>
            </td>
        </tr>
THREAD;

    $this->template['thread_new'] = <<< THREAD
        <li:css_class>
          <h4><a href=":thread_link"><span class="count">+:thread_value</span> :thread_title</a></h4>
          Started by
          <a class="user-img" href=":player_link"><img src=":player_avatar"></a>
          <a class="user-link" href=":player_link">:player_name</a>
          on :post_rts with <a href=":thread_link">:comment_count comment:comment_plural</a>
        </li>
THREAD;

    $this->template['empty_forum'] = <<< EMPTY
      <tr class="post">
        <td colspan="2"><b>No threads found.</b></td>
      </tr>
EMPTY;

    $this->template['pagination'] = <<< PAGINATION
      <div class="pagination text-center">
        <ul>
          :prev_button_html
          :list_html
          :next_button_html
        </ul>
      </div>
PAGINATION;

    $this->template['add_comment_form'] = <<< FORM
      <form accept-charset="UTF-8" class="comment-form" id="new_comment" method="post" style="margin-left::offsetpx">
        <input name="comment[media_id]" value=":media_id" type="hidden">
        <input name="comment[thread_id]" value=":thread_id" type="hidden">
        <p><textarea class="span8" cols="40" name="comment[content]" rows="20"></textarea></p>
        <p>
          <button class="btn btn-primary" type="button" onclick="newPost(this)"><i></i> Add Comment</button>
          :cancel_html
        </p>
      </form>
FORM;

    $this->template['comment'] = <<< COMMENT
      <div class="comment" id="comment_:comment_id" style="margin-left::offsetpx;">
        <div class="show">
          <p class="byline">
            :vote_html
            <span class="points">:comment_value</span> :points by
            <a class="user-img" href=":player_link"><img src=":player_avatar"></a>
            <a class="user-link" href=":player_link">:player_name</a> about :post_rts
            :admin
          </p>
          <p class="forum-content">:content</p>
          <p class="forum-edit">:edit_content</p>
          <p>
            :reply_link
            :edited
          </p>
        </div>
        :comment_form
      </div>
COMMENT;

    $this->template['tag'] = <<< TAG
      <a href=":tag_link" class="tag">:name</a>
TAG;

    $this->template['tag_input'] = <<< TAG
      <span>
        <input type="checkbox" value=":id" name="new_post[tags][]" id="new_post_tag_ids_" class="tag-checkbox">
        <a class="tag tag-link">:name</a>
      </span>
TAG;

    $this->template['vote'] = <<< VOTE
      <span class="voting">
        <a :up_classonclick="vote(this, :id, 1)">▲</a>
        <a :down_classonclick="vote(this, :id, -1)">▼</a>
      </span>
VOTE;

    $this->template['twitter'] = <<< TWITTER
      <div class="twitter-follow">
        <a href="https://twitter.com/:player_twitter" class="twitter-follow-button" data-show-count="false">Follow @:player_twitter</a>
        <script>
          !function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
            if (!d.getElementById(id)) {
              js = d.createElement(s);
              js.id = id;
              js.src = p + '://platform.twitter.com/widgets.js';
              fjs.parentNode.insertBefore(js, fjs);
            }
          }(document, 'script', 'twitter-wjs');
        </script>
      </div>
TWITTER;

    $this->template['tweet'] = <<< TWEET
      <a href="https://twitter.com/share" class="twitter-share-button" data-url=":url" data-text=":txt" data-via="playIGL" data-count="none">Tweet</a>
      <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
TWEET;

    $this->template['top_player'] = <<< PLAYER
      <li:css_class>
        <div class="avatar"><img src=":player_avatar" /></div>
        <h4><a href=":player_link">:player_name</a></h4>
        <strong>:points</strong> point:point_plural this week
        :twitter
      </li>
PLAYER;

    $this->template['post_rts'] = <<< TIME
      <span class="forum_date">:post_rts</span><input type="hidden" value=":post_date_unix">
TIME;

    $this->template['viewers'] = <<< VIEWERS
      <div class="online-players">
        <p>Current users online: :users.</p>
      </div>
VIEWERS;
  }

  private function _getTemplate($tpl, $data) {
    $placeholders = array();
    $values = array();

    if (count($data)) {
      foreach ($data as $key => $val) {
        $placeholders[] = ":" . $key;
        $values[] = $val;
      }
    }

    return str_replace($placeholders, $values, $this->template[$tpl]);
  }

  private function _getPaginationHTML($query_arr) {
    $return = "";

    $query_arr['SELECT'] = "SELECT count(t.id) as entries";
    unset($query_arr['LIMIT']);

    $this->db->ExecuteSQL($this->_authorSQL($query_arr));
    $result = $this->db->ArrayResult();

    $total_records = $result['entries'];

    if ($total_records <= 0)
      return $return;

    $total_pages = ceil($total_records / $this->page_limit);
    $list_html = "";

    for ($i = 1; $i <= $total_pages; $i++) {
      $css = $i == $this->data['index'] ? "page active" : "page";
      $list_html .= <<< LIST
        <li class="{$css}"><a href="/forums/page/{$i}">{$i}</a></li>
LIST;
    }

    $tpl_data = array(
      "prev_button_html" => $total_pages > 1 && $this->data['index'] > 1 ? "<li class=\"prev\"><a href=\"/forums/page/" . ($this->data['index'] - 1) . "\">&lt; Prev</a></li>" : "",
      "list_html" => $list_html,
      "next_button_html" => $total_pages > 1 && $this->data['index'] < $total_pages ? "<li class=\"next_page\"><a href=\"/forums/page/" . ($this->data['index'] + 1) . "\">Next &gt;</a></li>" : ""
    );

    return $this->_getTemplate('pagination', $tpl_data);
  }

  private function _getViewersHTML() {
    $players = $this->_reportOnlinePresence();

    $tpl_data = array(
      "users" => count($players) ? join(", ", $players) : "No users."
    );

    return $this->_getTemplate('viewers', $tpl_data);
  }

  private function _getTags() {
    $this->db->ExecuteSQL(
      "SELECT * FROM `" . FORUMS . "`.{$this->tbl_name['tbl_tags']} ORDER BY 'name' ASC;"
    );

    foreach ($this->db->ArrayResults() as $t)
      $this->tags[$t['id']] = $t;
  }

  private function _getCommentFormHTML($media_id, $cancel = false) {
    $cancel_html = "";

    if (is_null($this->player_id))
      return $cancel_html;

    $offset = 0;

    if ($cancel) {
      $offset = $this->offset_increment;
      $cancel_html = "&nbsp; <a class=\"btn\" onclick=\"cancelReply(this)\">Cancel</a>";
    }

    $tpl_data = array(
      "media_id" => $media_id,
      "offset" => $offset,
      "thread_id" => $this->current_thread_id,
      "cancel_html" => $cancel_html
    );
    return $this->_getTemplate('add_comment_form', $tpl_data);
  }

  private function _getCommentsHTML($id) {
    $return = "";

    $this->db->ExecuteSQL(
      "SELECT * FROM `" . FORUMS . "`.{$this->tbl_name['vw_comments']} WHERE media_id = {$id} AND status = 1;"
    );

    foreach ($this->db->ArrayResults() as $comment) {
      $links = array(
        "player" => $this->buildForumUserLink($comment['player_name']),
      );

      $tpl_data = array(
        "comment_id" => $comment['id'],
        "offset" => $this->offset,
        "vote_html" => $this->_getVotingHTML($comment['id']),
        "comment_value" => $comment['value'],
        "points" => abs($comment['value']) == 1 ? "point" : "points",
        "player_link" => $links['player'],
        "player_avatar" => $this->_getPlayerAvatar($comment['player_avatar']),
        "player_name" => $comment['player_name'],
        "post_rts" => $this->_getTemplate('post_rts', array("post_rts" => $this->_formatDate($comment['rts']), "post_date_unix" => strtotime($comment['rts']))),
        "content" => $this->_parseContent($comment['content']),
        "reply_link" => !is_null($this->player_id) ? "<a class=\"reply-link\" onclick=\"reply(this)\">reply</a>" : "",
        "edited" => $comment['edited'] ? "<span class='pull-right'>Last edited on " . $this->_getTemplate('post_rts', array("post_rts" => $this->_formatDate($comment['edited_rts']), "post_date_unix" => strtotime($comment['edited_rts']))) . "</span>" : "",
        "comment_form" => $this->_getCommentFormHTML($comment['id'], true),
        "admin" => $this->_getCommentAdminHTML($comment['player_id']),
        "edit_content" => $this->_getTemplate('edit_content', array("id" => $comment['id']))
      );
      $return .= $this->_getTemplate('comment', $tpl_data);

      $this->db->ExecuteSQL(
        "SELECT media_id FROM `" . FORUMS . "`.{$this->tbl_name['vw_comments']} WHERE media_id = {$comment['id']} AND status = 1;"
      );

      if ($this->db->iRecords()) {
        $this->offset += $this->offset_increment;
        foreach ($this->db->ArrayResults() as $inline_comment)
          $return .= $this->_getCommentsHTML($inline_comment['media_id']);
        $this->offset -= $this->offset_increment;
      }
    }

    return $return;
  }

  public function getTagsHTML($tags, $new_post = false) {
    $return = "";

    if (is_null($tags) || empty($tags))
      return $return;

    if (!$new_post) {
      $tags = explode(",", $tags);
      $tag_names_sorted = array();

      foreach ($tags as $tag)
        $tag_names_sorted[] = $this->tags[$tag]['name'];

      sort($tag_names_sorted);

      foreach ($tag_names_sorted as $name) {
        $tpl_data = array(
          "tag_link" => $this->buildTagLink($name),
          "name" => $name
        );
        $return .= $this->_getTemplate('tag', $tpl_data);
      }
    } else {
      $tags_sorted = array();

      foreach ($tags as $tag)
        $tags_sorted[$tag['id'] . '-'] = $tag['name'];

      asort($tags_sorted);

      foreach ($tags_sorted as $key => $name) {
        $tpl_data = array(
          "id" => (int) $key,
          "name" => $name
        );
        $return .= $this->_getTemplate('tag_input', $tpl_data);
      }
    }

    return $return;
  }

  private function _getVotingHTML($id) {
    $return = "";
    $up_class = "";
    $down_class = "";

    if (!is_null($this->player_id)) {
      $this->db->ExecuteSQL(
        "SELECT value FROM `" . FORUMS . "`.{$this->tbl_name['tbl_votes']} WHERE media_id = {$id} and player_id = {$this->player_id} LIMIT 1;"
      );

      if ($this->db->iRecords()) {
        $result = $this->db->ArrayResult();
        if ($result['value'] > 0)
          $up_class = "class='active' ";
        else {
          $down_class = "class='active' ";
        }
      }
    }

    $tpl_data = array(
      "id" => $id,
      "up_class" => $up_class,
      "down_class" => $down_class
    );
    $return = $this->_getTemplate('vote', $tpl_data);

    return $return;
  }

  private function _getPlayerAvatar($img) {
    return "/img{$img}";
  }

  private function _friendlyUrl($string) {
    if (empty($string))
      return NULL;

    $string = strtolower($string);                          //Unwanted:  {UPPERCASE} ; / ? : @ & = + $ , . ! ~ * ' ( )
    $string = preg_replace("/[^a-z0-9_\s-]/", "", $string); //Strip any unwanted characters
    $string = preg_replace("/[\s-]+/", " ", $string);       //Clean multiple dashes or whitespaces
    $string = preg_replace("/[\s_]/", "-", $string);        //Convert whitespaces and underscore to dash
    $string = trim($string, '-');

    return $string;
  }

  private function _parseContent($text) {
    $text = trim($text);

    while ($text != stripslashes($text)) {
      $text = stripslashes($text);
    }

    $text = strip_tags($text);
    $text = preg_replace("/(?<!http:\/\/)www\./", "http://www.", $text);
    $text = preg_replace("/((http|ftp)+(s)?:\/\/[^<>\s]+)/i", "<a href=\"\\0\" target=\"_blank\">\\0</a>", $text);

    return nl2br($text);
  }

  private function _formatDate($rts) {
    if (is_string($rts))
      $rts = strtotime($rts);

    $timezone = date("Z", $rts) / 3600;

    if ($timezone !== 0)
      $timezone = $timezone > 0 ? "+" . $timezone : "-" . $timezone;
    else
      $timezone = "";

    return join(" ", array(date("M j, g:ia", $rts), "GMT", $timezone));
  }

  private function _getThreadAdminHTML($player_id) {
    if ($this->player_id == $player_id)
      return $this->_getTemplate('thread_admin',array());
    return "<p></p>";
  }

  private function _getCommentAdminHTML($player_id) {
    if ($this->player_id == $player_id)
      return $this->_getTemplate('comment_admin',array());
    return "";
  }

  private function _addOnlinePresence() {
    $this->db->ExecuteSQL(
      "SELECT * FROM online WHERE player_id = {$this->player_id} LIMIT 1"
    );
      
      $data = array(
        "player_id" => $this->player_id,
        "online" => time()
      );

    if (!$this->db->iRecords()) {
      $this->db->Insert($data, "online");
    } else {
      $this->db->Update('online', $data, array("player_id" => $data['player_id']));
    }
  }

  public function deleteOnlinePresence($timeout = 900) {
    $this->db->ExecuteSQL(
      "SELECT * FROM online WHERE online < " . (time() - $timeout)
    );
    if (!$this->db->iRecords())
      return false;

    $deletes = $this->db->ArrayResults();
    foreach ($deletes as $d) {
      $this->db->Delete("online", array("player_id" => $d['player_id']));
    }
  }

  private function _reportOnlinePresence() {
    $this->db->ExecuteSQL(
      "SELECT * FROM viewers"
    );

    $viewers = $this->db->ArrayResults();
    $players = array();

    foreach ($viewers as $v) {
      $players[] = "<a href='" . $this->buildForumUserLink($v['player_name']) . "'>{$v['player_name']}</a>";
    }

    return $players;
  }
  
  private function _shorten($txt) {
    $length = 94; //default text length minus ellipses minus url minus 'via @playIGL'
    if (strlen($txt) < $length) return $txt;
    return substr($txt, 0, $length) . "...";
  }
}

?>
