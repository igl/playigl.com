<?php
require_once("Analytics.php");
Analytics::init("fi16vucloyzllwxclsum");

if (isset($_SESSION['playerid'])) {
    Analytics::identify($_SESSION['playerid'], array("email" => $_SESSION['email']));
}

function playigl_analytics_track($action, $data) {
    if (isset($_SESSION['playerid'])) {
        Analytics::track($_SESSION['playerid'], $action, $data);
    }
}
?>
