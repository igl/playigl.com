<?php

$usergroups = explode(",", $_SESSION['membergroupids']);
if((bool) array_intersect(array(6), $usergroups) || $_SESSION['usergroupid']==6)
{
    define("SUPERUSER", true);
    define("LEAGUEADMIN", true);
    define("PRESS", true);
    define("ADMIN", true);
}
elseif((bool) array_intersect(array(10), $usergroups) || $_SESSION['usergroupid']==6)
{
    define("SUPERUSER", false);
    define("LEAGUEADMIN", true);
    define("PRESS", true);
    define("ADMIN", true);
}
elseif((bool) array_intersect(array(12), $usergroups) || $_SESSION['usergroupid']==6)
{
    define("SUPERUSER", false);
    define("LEAGUEADMIN", false);
    define("PRESS", true);
    define("ADMIN", true);
}
else
{
    define("SUPERUSER", false);
    define("LEAGUEADMIN", false);
    define("PRESS", false);
    define("ADMIN", false);
}
?>
