<?php

function parseContent($text) {
  $text = trim($text);

  while ($text != stripslashes($text)) {
    $text = stripslashes($text);
  }

  $text = strip_tags($text);
  $text = preg_replace("/(?<!http:\/\/)www\./", "http://www.", $text);
  $text = preg_replace("/((http|ftp)+(s)?:\/\/[^<>\s]+)/i", "<a href=\"\\0\" target=\"_blank\">\\0</a>", $text);

  return nl2br($text);
}

function friendlyURL($string) {
  if (empty($string))
    return NULL;

  $string = strtolower($string);                          //Unwanted:  {UPPERCASE} ; / ? : @ & = + $ , . ! ~ * ' ( )
  $string = preg_replace("/[^a-z0-9_\s-]/", "", $string); //Strip any unwanted characters
  $string = preg_replace("/[\s-]+/", " ", $string);       //Clean multiple dashes or whitespaces
  $string = preg_replace("/[\s_]/", "-", $string);        //Convert whitespaces and underscore to dash
  $string = trim($string, '-');

  return $string;
}

function returnJSON($arr) {
  header('Content-type: application/json');
  echo PRETTY_JSON ? prettyJSON(json_encode($arr)) : json_encode($arr);
  die();
}

function isLoggedIn() {
  return isset($_SESSION) && !empty($_SESSION) && $_SESSION['playerid'] > 0;
}
/*
  function sort_array($a, $b)
  {
  return strnatcmp($a[1], $b[1]);
  }
 */

function ago($time) {
  if (is_string($time) && !is_numeric($time))
    $time = strtotime($time);

  $periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
  $lengths = array("60", "60", "24", "7", "4.35", "12", "10");

  $now = time();

  $difference = $now - $time;
  $tense = "ago";

  for ($j = 0; $difference >= $lengths[$j] && $j < count($lengths) - 1; $j++)
    $difference /= $lengths[$j];

  $difference = round($difference);

  if ($difference != 1)
    $periods[$j].= "s";

  return "$difference $periods[$j] ago";
}

function age($dob) {
  if (!empty($dob)) {
    list($y, $m, $d) = explode('-', $dob);

    if (($m = (date('m') - $m)) < 0) {
      $y++;
    } elseif ($m == 0 && date('d') - $d < 0) {
      $y++;
    }

    return date('Y') - $y;
  } else {
    return "?";
  }
}

function convert32to64($steam_id) {
  list(, $m1, $m2) = explode(':', $steam_id, 3);
  list($steam_cid, ) = explode('.', bcadd((((int) $m2 * 2) + $m1), '76561197960265728'), 2);
  return $steam_cid;
}

function convert64to32($steam_cid) {
  $id = array();
  $id[1] = substr($steam_cid, -1, 1) % 2 == 0 ? 0 : 1;
  $id[2] = bcsub($steam_cid, '76561197960265728');

  if (bccomp($id[2], '0') != 1)
    return false;

  $id[2] = bcsub($id[2], $id[1]);
  list($id[2], ) = explode('.', bcdiv($id[2], 2), 2);
  return implode(':', $id);
}

function array_flatten_recursive($array) {
  if (!$array)
    return false;

  $flat = array();
  $RII = new RecursiveIteratorIterator(new RecursiveArrayIterator($array));
  foreach ($RII as $value)
    $flat[] = $value;

  return $flat;
}

function genServerPass($length = 8) {
  $keys = array_merge(range(0, 9), range('a', 'z'));

  for ($i = 0; $i < $length; $i++)
    $key .= $keys[array_rand($keys)];

  return $key;
}

function ISOtime($date) { // YYYY-MM-DD H:m:s
  $split = explode(' ', $date);
  $date_seconds = strtotime($split[0]);
  $time = explode(':', $split[1]);
  if (isset($time[2])) {
    $seconds = $time[2];
  }
  else
    $seconds = 0;
  $new_time = ($time[0] * 3600) + ($time[1] * 60) + $seconds + $date_seconds;
  return $new_time;
}

function truncate($string, $limit, $break = " ", $pad = "...") { // return with no change if string is shorter than $limit 
  $string = strip_tags($string);
  if (strlen($string) <= $limit)
    return $string;
  $string = substr($string, 0, $limit);

  if (false !== ($breakpoint = strrpos($string, $break)))
    $string = substr($string, 0, $breakpoint);

  return $string . $pad;
}

function escape($value) {
  $search = array("\x00", "\n", "\r", "\\", "'", "\"", "\x1a");
  $replace = array("\\x00", "\\n", "\\r", "\\\\", "\'", "\\\"", "\\\x1a");

  return str_replace($search, $replace, $value);
}

function get_day_name($timestamp) {
  $date = date('F jS, Y', strtotime($timestamp));
  if ($date == date('F jS, Y', time()))
    $when = 'Today';
  elseif ($date == date('F jS, Y', time() - (24 * 60 * 60)))
    $when = 'Yesterday';
  else
    $when = $date;
  return $when;
}

function get_day_name_time($timestamp) {
  $date = date('F jS, Y', strtotime($timestamp));
  if ($date == date('F jS, Y', time()))
    $when = 'Today';
  elseif ($date == date('F jS, Y @ g:iA', time() - (24 * 60 * 60)))
    $when = 'Yesterday';
  else
    $when = date('F jS, Y @ g:iA', strtotime($timestamp));
  return $when;
}

function debug($var) {
  echo '<pre>';
  print_r($var);
  echo '</pre>';
}

/**
 * prettyJSON - parses json_encoded string and returns it in a nicely readable format
 * @param string $json
 * @return string 
 */
function prettyJSON($json) {
  $result = '';
  $level = 0;
  $prev_char = '';
  $in_quotes = false;
  $ends_line_level = NULL;
  $json_length = strlen($json);

  for ($i = 0; $i < $json_length; $i++) {
    $char = $json[$i];
    $new_line_level = NULL;
    $post = "";
    if ($ends_line_level !== NULL) {
      $new_line_level = $ends_line_level;
      $ends_line_level = NULL;
    }
    if ($char === '"' && $prev_char != '\\') {
      $in_quotes = !$in_quotes;
    } else if (!$in_quotes) {
      switch ($char) {
        case '}':
        case ']':
          $level--;
          $ends_line_level = NULL;
          $new_line_level = $level;
          break;

        case '{':
        case '[':
          $level++;
        case ',':
          $ends_line_level = $level;
          break;

        case ':':
          $post = " ";
          break;

        case " ":
        case "\t":
        case "\n":
        case "\r":
          $char = "";
          $ends_line_level = $new_line_level;
          $new_line_level = NULL;
          break;
      }
    }
    if ($new_line_level !== NULL) {
      $result .= "\n" . str_repeat("\t", $new_line_level);
    }
    $result .= $char . $post;
    $prev_char = $char;
  }

  echo $result;
}

function player_from_communityID($communityID) {
  include_once ('model/mysql.class.php');
  $db = new mysql(MYSQL_DATABASE);
  $sql = "SELECT playerid, username FROM player WHERE communityid = '{$communityID}'";
  $db->ExecuteSQL($sql);
  if ($db->records)
    return $db->ArrayResult;
  return false;
}

function salt($length = 30, $charset = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()-=') {
  $str = '';
  $count = strlen($charset);
  while ($length--)
    $str .= $charset[mt_rand(0, $count - 1)];
  return $str;
}

function genPass($password, $salt) {
  return md5(md5($password) . $salt);
}

?>
