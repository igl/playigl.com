<?php
/**
 * Notification handling
 * @author Shaun Delaney <sdelaney@playigl.com>
 * @copyright Copyright 2013 PlayIGL.com
 */

/*  NOTE: Ensure that the following are included on your page:
 * - include_once('./includes/config.php');
 * - include_once('./model/mysql.class.php');
 * - include_once('./includes/functions.php');
 * - include_once('./includes/pusher.php');
 */

//Notification System
function notify($actor_id, $actor_type, $subject_id, $subject_type, $verb) {
    $actions    = array("follow");  //Actionable verbs
    $return     = array("success" => false, "error" => "Please log in to use this feature.");

    //Ensure we're logged in first
    if (!isLoggedIn())
        returnJSON($return);

    //Ensure real data was passed
    if (!is_numeric($actor_id) || !is_numeric($subject_id) || !in_array($verb, $actions)) {
        $return['error'] = "Please pass valid data.";
        returnJSON($return);
    }

    //Ensure that we're not targetting ourselves
    if ($actor_id == $subject_id) {
        $return['error'] = "You cannot {$verb} yourself.";
        returnJSON($return);
    }

    //Connect to the database
    $db = new MySQL(ACTIVITY);

    //Depending on what action we want authored, do something special
    switch($verb) {
        //User follows an entity
        case "follow":
            $db->ExecuteSQL(
                "SELECT `id` FROM `notifications` 
                WHERE `actor_id` = {$actor_id} AND `actor_type` = '{$actor_type}'
                AND `subject_id` = {$subject_id} AND `subject_type` = '{$subject_type}'
                AND `verb` = '{$verb}'
                LIMIT 1;"
            );

            //Already follows that entity
            if ($db->iRecords()) {
                $record_id = $db->ArrayResult();
                $db->Delete('notifications', array('id' => $record_id['id']));
                
                //Unfollow the user
                $msg = "You have successfully unfollowed this {$subject_type}.";
                $return = array("success" => true, "error" => $msg, "following" => false);
                //Push our message through
                pushMessage(
                    array("{$actor_type}-{$actor_id}"),
                    array($msg)
                );
                returnJSON($return);
            }

            $sentence = array(
                "actor_id"      => $actor_id,
                "actor_type"    => $actor_type,
                "verb"          => $verb,
                "subject_id"    => $subject_id,
                "subject_type"  => $subject_type
            );

            //Attempt to create a connection
            if (!$db->Insert($sentence, 'notifications')) {
                $msg = "User not followed due to an error. Please try again later.";
                $return['error'] = $msg;
                //Push our message through
                pushMessage(
                    array("{$actor_type}-{$actor_id}"),
                    array($msg)
                );
                returnJSON($return);
            }

            //Connection established.
            $actor = null;
            
            $actor_username = '';
            
            //Grab our actor information
            switch($actor_type) {
                case 'player':
                    $db->ExecuteSQL(
                        "SELECT `firstname`, `username`, `lastname` FROM `".MYSQL_DATABASE."`.`player` 
                        WHERE `playerid` = {$actor_id}
                        LIMIT 1;"
                    );
                    $data   = $db->ArrayResult();
                    $actor  = "{$data['firstname']} \"{$data['username']}\" {$data['lastname']}";
                    $actor_username = $data['username'];
                break;

                case 'team':
                    $db->ExecuteSQL(
                        "SELECT `name` FROM `".MYSQL_DATABASE."`.`team` 
                        WHERE `id` = {$actor_id}
                        LIMIT 1;"
                    );
                    $data   = $db->ArrayResult();
                    $actor  = "Team {$data['name']}";
                break;
            }
            
            $subjet_email = '';

            //Grab our subject information
            switch($subject_type) {
                //If the connection was a player
                case 'player':
                    $db->ExecuteSQL(
                        "SELECT `firstname`, `username`, `email`, `lastname` FROM `".MYSQL_DATABASE."`.`player` 
                        WHERE `playerid` = {$subject_id}
                        LIMIT 1;"
                    );

                    $data   = $db->ArrayResult();
                    $msg    = "{$data['firstname']} \"{$data['username']}\" {$data['lastname']}";
                    $subjet_email = $data['email'];
                    $subject_username = $data['username'];

                    //Push our message through
                    pushMessage(
                        array("{$actor_type}-{$actor_id}", "{$actor_type}-{$actor_id}-followers"),
                        array(
                            "You are now following {$msg}.",
                            "{$actor} is now following {$msg}."
                        )
                    );
                break;

                //If the connection was a team
                case 'team':
                    $db->ExecuteSQL(
                        "SELECT t.`name` as team_name, t.`captain` as team_captain, g.`name` as game_name FROM `".MYSQL_DATABASE."`.`team` t
                        JOIN `".MYSQL_DATABASE."`.`game` g ON g.`id` = t.`game` 
                        WHERE t.`id` = {$subject_id}
                        LIMIT 1;"
                    );

                    $data   = $db->ArrayResult();
                    $msg    = "Team {$data['team_name']} of {$data['game_name']}";;
                    $db2 = new MySQL();
                    $db2->ExecuteSQL(
                        "SELECT email, username FROM player WHERE playerid = '".$data['team_captain']."'"
                    );
                    $emaildata = $db2->ArrayResult();
                    $subjet_email = $emaildata['email'];
                    $subject_username = $emaildata['username'];
                    $subject_id = $data['team_name'];
                    //Push our message through
                    pushMessage(
                        array("{$actor_type}-{$actor_id}", "{$actor_type}-{$actor_id}-followers"),
                        array(
                            "You are now following {$msg}.",
                            "{$actor} is now following {$msg}."
                        )
                    );
                break;
            }

            $return = array("success" => true, "following" => true, 'actor' => $actor_id, 'actor_username' => $actor_username, 'subject' => $subject_id, 'subject_username' => $subject_username, 'subject_email' => $subjet_email);
        break;

        default:
            $return['error'] = "Unsupported command.";
        break;
    }

    returnJSON($return);
}

function getTeamChannels($team_id) {
  $return = array();
  $db = new MySQL(MYSQL_DATABASE);
  
  $db->executeSQL(
    "SELECT
      id
    FROM
      roster
    WHERE
      team = {$team_id}"
  );
      
  if ($db->iRecords()) {
    $return = $db->ArrayResults();
    foreach($return as $key=>$val) {
      $return[$key] = "player-{$val}";
    }
  }
  
  return $return;
}

function pushMessage($channels, $messages) {
    global $pusher;
    
    for ($i = 0; $i < count($channels) && $i < count($messages); $i++) {
        $pusher->trigger($channels[$i], 'notification', array('message' => $messages[$i]) );
    }
}
?>