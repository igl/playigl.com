<?php

date_default_timezone_set('UTC');

#SQL Constants

define ("MYSQL_HOST", "localhost"); // MySQL Hostname

define ("MYSQL_USER", "root");	// MySQL Username

define ("MYSQL_PASS", "");	// MySQL Password

define ("MYSQL_DATABASE", "playigl_dev");	// MySQL Database

define ("ACTIVITY", "playigl_dev_activity"); // Dayabase storing activity feeds

#Interactive constants

define ("BASEURL", "http://localhost/"); #Include trailing slash
?>