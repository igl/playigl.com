<?php
class activity
{

    public function __construct()  
    {
        $this->db = new MySQL(ACTIVITY);
    }

    function addEvent($event,$player=false,$team=false,$player2=false,$team2=false)
    {
        $query = "SELECT * FROM `".ACTIVITY."`.`follow` WHERE ";
        if($player)
        {
            $query .= " playerid = '{$player}' OR";
        }
        if($player2)
        {
            $query .= " playerid = '{$player2}' OR";
        }
        if($team)
        {
            $query .= " teamid = '{$team}'";
        }
        elseif($team2)
        {
            $query .= " teamid = '{$team2}'";
        }
        else
        {
            $query = substr($query, 0, -3);
        }
        
        $this->db->Insert(array(
                'event' => $event,
                'player_id' => $player,
                'team_id' => $team,
                'player2_id' => $player2,
                'team2_id' => $team2,
                'timestamp' => time(),
            ), 'event');
        $event_id = $this->db->lastId();
        $time = time();
        
        $this->db->ExecuteSQL($query);
        $followers = $this->db->ArrayResults();

        foreach( $followers as $follower )
        {
            $this->db->Insert(array(
                'playerid' => $follower['followerid'],
                'eventid' => $event_id,
                'timestamp' => $time
            ),'notifications');
        }
        
    }
}
?>