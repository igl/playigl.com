<?php

class image {

    public function __construct() {
        $this->db = new mysql(MYSQL_DATABASE);
    }

    function display($id, $style = false) {
        if ($style) {
            $class = 'class="' . $style . '"';
        }
        if (intval($id) == 0) {
            return '<img $class src="' . BASEURL . 'img/missing.png">';
        } else {
            $file = BASEURL . 'image.php?id=' . $id;
            if (false == ($str = @file_get_contents('$file', NULL, NULL, 0, 1))) {
                return '<img $class src="{$file}">';
            } else {
                return '<img $class src="' . BASEURL . 'img/missing.png">';
            }
        }
    }

    function resize($id, $newwidth, $link = true) {
        $image = BASEURL . 'image.php?id=' . $id;
        if ($id == 0) {
            return '<img width="' . $width . '" src="' . BASEURL . 'img/missing.png">';
        } else {
            $image = BASEURL . "image.php?id=" . $id;
            list($width, $height, $type, $attr) = getimagesize($image);
            if ($width > $newwidth) {
                $width = $newwidth;
            }
            if ($link) {
                return "<a title=\"URL:<br>" . $image . "\" class=\"lightbox\" id=\"gallery\" href=\"" . $image . "\"><img width=\"" . $width . "\" src=\"" . $image . "\"></a>";
            } else {
                return "<img width=\"" . $width . "\" src=\"" . $image . "\">";
            }
        }
    }

    function upload($fieldname) {
        $return = false;
        if (is_uploaded_file($_FILES[$fieldname]['tmp_name'])) {
            $image_name = "/{$table}/{$id}_{$fieldname}.png";
            $data = file_get_contents($_FILES[$fieldname]['tmp_name']);
            $file = $_SERVER['DOCUMENT_ROOT'] . "/img{$image_name}";
            file_put_contents($file, $data);

            if ($table == 'player') {
                $where = array('playerid' => $id);
            }
            else {
                $where = array('id' => $id);
            }
           $return = "/img".$image_name;
        }
    }

    function uploadArticleBanner($fieldname) {
        $return = false;
        if (is_uploaded_file($_FILES[$fieldname]['tmp_name'])) {
            $rand = md5(time());
            $image_name = "/article/{$fieldname}_{$rand}.png";
            $data = file_get_contents($_FILES[$fieldname]['tmp_name']);
            $file = $_SERVER['DOCUMENT_ROOT'] . "/img{$image_name}";
            file_put_contents($file, $data);


           $return = $image_name;
        }
        return $return;
    }
    
    function uploadtoprofile($fieldname, $table, $id) {
        
        
        $return = false;
        if (is_uploaded_file($_FILES['avatar']['tmp_name'])) {
            $image_name = "/{$table}/{$id}_{$fieldname}.png";
            $data = file_get_contents($_FILES['avatar']['tmp_name']);
            $file = $_SERVER['DOCUMENT_ROOT'] . "/img{$image_name}";
            file_put_contents($file, $data);

            if ($table == 'player') {
                $where = array('playerid' => $id);
            }
            else {
                $where = array('id' => $id);
            }
            
            if ($this->db->Update($table, array($fieldname => $image_name), $where)) {
                $return = "/img".$image_name;
            }
        }
        return $return;
        
        
        
        /*
        if (is_uploaded_file($_FILES['avatar']['tmp_name'])) {
            
            $imageblob = addslashes(file_get_contents($_FILES['avatar']['tmp_name']));

            if ($table == 'player')
                $where = array('playerid' => $id);
            else
                $where = array('id' => $id);

            //Get old image & remove from the database
            $this->db->ExecuteSQL("DELETE FROM `image` WHERE id = (SELECT {$fieldname} FROM {$table} WHERE id = {$id})");

            $this->db->ExecuteSQL("INSERT INTO `image` (`id`, `image`) VALUES ('', '{$imageblob}')");

            $id = $this->db->lastId();
            $fields = array($fieldname => $id);

            if ($this->db->Update($table, $fields, $where))
                return $id;
            return false;
        }
        else {
            return false;
        } */
    }

    function uploadGameImage($fieldname, $game_id) {
        $return = false;
        if (is_uploaded_file($_FILES[$fieldname]['tmp_name'])) {
            $image_name = "/game/{$game_id}_{$fieldname}.png";
            $data = file_get_contents($_FILES[$fieldname]['tmp_name']);
            $file = $_SERVER['DOCUMENT_ROOT'] . "/img{$image_name}";
            file_put_contents($file, $data);

            if ($this->db->Update('game', array($fieldname => $image_name), array('id' => $game_id)))
                $return = $image_name;
        }
        return $return;
    }

    function listall() {
        $query = "SELECT id FROM image";
        $this->db->ExecuteSQL($query);
        $result = $this->db->ArrayResults();
        foreach ($result as $value) {
            $return[] = $value['id'];
        }
        return $return;
    }

}

?>
