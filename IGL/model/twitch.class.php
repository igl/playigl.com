<?php

/*
 * @author Darryl Allen
 * @copyright Copyright 2012 PlayIGL.com
 */

class twitch {

    public function __construct() {
        $this->db = new mysql(MYSQL_DATABASE);
    }

    function getChannels($userid) {
        $return = false;
        $this->db->ExecuteSQL("SELECT twitch FROM player WHERE playerid = {$userid}");
        if ($this->db->iRecords()) {
            $r = $this->db->Arrayresult();
            if (!empty($r['twitch'])) {
                // create curl resource
                $ch = curl_init();
                
                // set url
                $json_url = "https://api.twitch.tv/kraken/channels/{$r['twitch']}/videos?limit=15&broadcasts=true";
                curl_setopt($ch, CURLOPT_URL, $json_url);

                //return the transfer as a string
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

                // $output contains the output string
                $result = curl_exec($ch);
                $return = json_decode($result);

                // close curl resource to free up system resources
                curl_close($ch); 
            }
        }
        return $return;
    }

}

?>