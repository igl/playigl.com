<?php

// Tournament Class

class showmatch {

public function __construct()  
     {  
          $this->db             = new mysql(MYSQL_DATABASE);
     }
     
function save($id)
{
    $table = 'showmatches';
    if($this->db->update($table, $_POST, array('id' => $id), array('id', 'ajax', 'action')))
        return true;
    return false;
}

function create()
{
    $table = 'showmatches';
    if($this->db->insert($_POST, $table, array('action','id','ajax'))) {
        return true;
    } else {
    echo $this->db->sLastError;
    }
} 
function delete($id)
{
   $table = 'showmatches';
   if($this->db->update($table, array('status' => 0),array('id' => $id)))
       return true;
   return false;
   
}
// Get Full Tournament Array based on ID
function get($id)
{
    $query = "SELECT * FROM showmatches WHERE id = $id";
    $this->db->ExecuteSQL($query);
    $tour_info = $this->db->ArrayResult();
    return $tour_info;
}

function all()
{
    $query = "SELECT * FROM showmatches WHERE status = 1";
    $this->db->ExecuteSQL($query);
    return $this->db->ArrayResults();
}

}

?>
