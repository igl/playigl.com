<?php

/*
 * @author Darryl Allen
 * @copyright Copyright 2012 PlayIGL.com
 */

class team {

    public function __construct() {
        $this->db = new mysql(MYSQL_DATABASE);
    }

    function getLast10Record($id) {
        $return = array();
        $this->db->ExecuteSQL("SELECT SUM( CASE WHEN `winner` = {$id} THEN 1 ELSE 0 END ) AS wins, SUM( CASE WHEN `loser` = {$id} THEN 1 ELSE 0 END ) AS losses, TRUNCATE (( SUM( CASE WHEN `winner` = 500 THEN 1 ELSE 0 END ) / count(id)), 3 ) AS pct FROM ( SELECT id, winner, loser FROM schedule WHERE {$id} IN (`winner`, `loser`) ORDER BY `officialdate` DESC LIMIT 10 ) q");
        if ($this->db->iRecords()) {
            $return = $this->db->ArrayResult();
        }
        return $return;
    }

    function getWinRecord($id) {
        $return = array();
        $this->db->ExecuteSQL("SELECT id, SUM( CASE WHEN winner = {$id} THEN 1 ELSE 0 END ) AS wins, SUM( CASE WHEN loser = {$id} THEN 1 ELSE 0 END ) AS losses, TRUNCATE ( SUM( CASE WHEN winner = {$id} THEN 1 ELSE 0 END ) / COUNT(id), 3 ) AS pct FROM schedule WHERE {$id} IN (winner, loser)");
        if ($this->db->iRecords()) {
            $return = $this->db->ArrayResult();
        }
        return $return;
    }

    function getLadderRecord($id) {
        $return = array();
        $this->db->ExecuteSQL("SELECT COUNT(CASE WHEN winner = {$id} THEN 1 END) AS wins, COUNT(CASE WHEN loser = {$id} THEN 1 END) AS losses, TRUNCATE (( COUNT( CASE WHEN `winner` = {$id} THEN 1 END ) / COUNT(id)), 3 ) AS pct FROM ( SELECT * FROM schedule WHERE {$id} IN (`winner`, `loser`) AND ladder > 0 ) q");
        if ($this->db->iRecords()) {
            $return = $this->db->ArrayResult();
        }
        return $return;
    }

    function getLeagueRecord($id) {
        $return = array();
        //Requires join to settings table to get current season records only.
        $this->db->ExecuteSQL("SELECT COUNT(CASE WHEN winner = {$id} THEN 1 END) AS wins, COUNT(CASE WHEN loser = {$id} THEN 1 END) AS losses, TRUNCATE (( COUNT( CASE WHEN `winner` = {$id} THEN 1 END ) / COUNT(id)), 3 ) AS pct FROM ( SELECT * FROM schedule JOIN league_settings ON ( league_settings.division = schedule.league ADD league_settings.current_season = schedule.season) WHERE {$id} IN (`winner`, `loser`) AND league > 0 ) q");
        if ($this->db->iRecords()) {
            $return = $this->db->ArrayResult();
        }
        return $return;
    }

    function getTournamentRecord($id) {
        $return = array();
        $this->db->ExecuteSQL("SELECT COUNT(CASE WHEN winner = {$id} THEN 1 END) AS wins, COUNT(CASE WHEN loser = {$id} THEN 1 END) AS losses, TRUNCATE (( COUNT( CASE WHEN `winner` = {$id} THEN 1 END ) / COUNT(id)), 3 ) AS pct FROM ( SELECT * FROM schedule WHERE {$id} IN (`winner`, `loser`) AND tournament > 0 ) q");
        if ($this->db->iRecords()) {
            $return = $this->db->ArrayResult();
        }
        return $return;
    }

    function getResults($id, $filter = NULL, $limit = NULL) {
        $return = array();

        if ($filter != NULL)
            $case = " AND s.{$filter} > 0";

        if ($limit != NULL)
            $limit = " LIMIT {$limit}";

        $this->db->ExecuteSQL("
            SELECT
                s.id AS match_id,
                CASE 
                WHEN s.league > 0 THEN 'League' 
                WHEN s.ladder > 0 THEN 'Ladder' 
                WHEN s.tournament > 0 THEN 'Tournament' END AS comp,

                CASE 
                WHEN s.league > 0 THEN 'League Name Here'
                WHEN s.ladder > 0 THEN ls.name 
                WHEN s.tournament > 0 THEN 'Tournament Name Here' END AS comp_name,

                CASE 
                WHEN s.league > 0 THEN 'League Format Here'
                WHEN s.ladder > 0 THEN ls.format
                WHEN s.tournament > 0 THEN 'Tournament Format Here' END AS format,

                g.name AS game_name,
                s.officialdate AS date,
                CASE WHEN s.home = {$id} THEN s.away ELSE s.home END AS opponent_id,
                CASE WHEN s.home = {$id} THEN a.name ELSE h.name END AS opponent_name,
                CASE WHEN s.home = {$id} THEN h.name ELSE a.name END AS my_name,
                CASE WHEN s.home = {$id} THEN a.tag ELSE h.tag END AS opponent_tag,
                CASE WHEN s.home = {$id} THEN s.home_points ELSE s.away_points END AS my_points,
                CASE WHEN s.home = {$id} THEN s.home_diff ELSE s.away_diff END AS my_diff,
                CASE WHEN s.home = {$id} THEN s.away_score ELSE s.home_score END AS opponent_score,
                CASE WHEN s.home = {$id} THEN s.home_score ELSE s.away_score END AS my_score, 
                s.map AS maps
            FROM
                schedule s
            LEFT JOIN 
                ladder l ON l.id = s.ladder
            LEFT JOIN 
                ladder_settings ls ON ls.id = l.settings
            JOIN 
                game g ON g.id = s.game
            JOIN 
                team h ON h.id = s.home
            JOIN 
                team a ON a.id = s.away
            WHERE
            {$id} IN (s.home, s.away) AND s.completed = 1 {$case} ORDER BY date DESC {$limit}");
        if ($this->db->iRecords())
            $return = $this->db->ArrayResults();

        return $return;
    }

    function add() {

        if (isset($_POST['game']) && !empty($_POST['game']) && isset($_POST['captain']) && !empty($_POST['captain']) && isset($_POST['name']) && !empty($_POST['name']) && isset($_POST['tag']) && !empty($_POST['tag'])) {
            //verify the team captain is not already participating
            $query = "SELECT `id` FROM `roster` WHERE `roster`.`game` = {$_POST['game']} AND `roster`.`player` = {$_POST['captain']}";
            $this->db->ExecuteSQL($query);
            if (!$this->db->iRecords()) {
                if ($this->db->insert($_POST, 'team')) {
                    $team_id = $this->db->LastId();
                    if ($this->db->insert(array('player' => $_POST['captain'], 'game' => $_POST['game'], 'team' => $team_id), 'roster')) {
                        return array('success' => true);
                    } else {
                        return array('success' => false, 'error' => 'Team was added, however there was an error adding the captain to the roster.');
                    }
                } else {
                    return array('success' => false, 'error' => 'Unknown error while adding team');
                }
            } else {
                return array('success' => false, 'error' => 'Captain is already participating on a team for this game');
            }
        } else {
            return array('success' => false, 'error' => 'Some required fields are missing');
        }
    }

    function all() {
        $query = "SELECT * from `active_teams`";
        $this->db->ExecuteSQL($query);
        if ($this->db->iRecords()) {
            return $this->db->Arrayresults();
        }
        else
            return array();
    }

    function get() {
        $id = (int) $_REQUEST['id'];
        $query = "SELECT * FROM `team` WHERE `team`.`id` = {$id}";
        $this->db->ExecuteSQL($query);
        if ($this->db->iRecords()) {
            $team = $this->db->Arrayresult();
            $query = "
            SELECT 
                `player`.`playerid` as player_id,
                `player`.`username` as player_username
            FROM
                `player`
            JOIN
                `roster` ON `roster`.`player` = `player`.`playerid`
            WHERE
                `roster`.`team` = {$id}";
            $this->db->ExecuteSQL($query);
            if ($this->db->iRecords()) {
                $roster = $this->db->Arrayresults();
                $team['team_roster'] = $roster;
            }
            else
                $team['team_roster'] = array();
            return $team;
        }
        else
            return array();
    }

    function save() {
        $sTable = "team";
        $id = (int) $_REQUEST['id'];
        return $this->db->Update($sTable, $_POST, array('id' => $id), array('action'));
    }

    function delete($team) {
        if ($this->db->update('team', array('status' => 0, 'deleted' => 1), array('id' => $team))) {
            $sql = "SELECT player FROM roster WHERE team = '{$team}'";
            $this->db->ExecuteSQL($sql);
            if ($this->db->iRecords()) {
                $roster_list = $this->db->ArrayResults();
                foreach ($roster_list as $key => $value) {
                    $agents[] = "player-{$value['player']}";
                    $msg[] = "Your team has been deleted";
                }
            }
            if ($this->db->delete('roster', array('team' => $team))) {
                pushMessage(
                    $agents, $msgs
                  );
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

}

?>