<?php
// This class is originally developed by "Bendikt Martin Myklebust" this is updated by "Rakesh Chandel".
//To Secure Global varaible values consisting in array while posting from GET,Session, and POST.

$db = new mysql(MYSQL_DATABASE);
class SecureSqlInjection
{
    
	function specificData($val)
	{
	    $val = htmlspecialchars(stripslashes(trim($val)));
        $val = str_ireplace("script", "blocked", $val);
        $val = mysql_escape_string($val);
        return $val;
	}
	
	 function secureSuperGlobalGET(&$value, $key)
    {
        
		if(!is_array($_GET[$key]))
		{
			$blocked = array (";DELETE", ";SELECT","; DELETE", "; SELECT","SELECT *","1--");
			$replace = array ("DELETE","SELECT","DELETE","SELECT","SELECT [*]","1-");
			$_GET[$key] = htmlspecialchars(stripslashes($_GET[$key]));
			$_GET[$key] = str_ireplace($blocked, $replace, $_GET[$key]);
			$_GET[$key] = mysql_real_escape_string($_GET[$key]);;
		}
		else
		{
		 
		 $c=0;
		 foreach($_GET[$key] as $val)
		 {
		 	 $_GET[$key][$c] = mysql_real_escape_string($_GET[$key][$c]);
		 $c++;
		 }
		}
        return $_GET[$key];
    }
    
    function secureSuperGlobalPOST(&$value, $key)
    {
       
		if(!is_array($_POST[$key]))
		{
		$blocked = array (";DELETE", ";SELECT","; DELETE", "; SELECT","SELECT *","1--");
		$replace = array ("DELETE","SELECT","DELETE","SELECT","SELECT [*]","1-");
        $_POST[$key] = htmlspecialchars(stripslashes($_POST[$key]));
        $_POST[$key] = str_ireplace($blocked, $replace, $_POST[$key]);
        $_POST[$key] = mysql_real_escape_string($_POST[$key]);
        }
		else
		{
		 
		 $c=0;
		 foreach($_POST[$key] as $val)
		 {
		 	 $_POST[$key][$c] = mysql_escape_string($_POST[$key][$c]);
		 $c++;
		 }
		}
		return $_POST[$key];
    }
        
    function secureGlobals()
    {
       
	    array_walk($_GET, array($this, 'secureSuperGlobalGET'));
        array_walk($_POST, array($this, 'secureSuperGlobalPOST'));
    }
}

?>