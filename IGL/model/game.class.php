<?php

class game {

    public function __construct() {
        $this->db = new mysql(MYSQL_DATABASE);
        $this->image = new image();
    }
    
    function isCaptain($game_id,$player_id) {
        if(!is_numeric($player_id))
            return false;
        $this->db->ExecuteSQL("SELECT team_id FROM ".MYSQL_DATABASE.".active_teams WHERE team_game_id = {$game_id} AND {$player_id} IN (`team_captain_id`,`team_alternate_id`)");
        if($this->db->iRecords()) {
            return true;
        }
        return false;
    }

    function isAgent($game_id,$player_id) {
        if(!is_numeric($player_id))
            return false;
        $this->db->ExecuteSQL("SELECT id FROM ".MYSQL_DATABASE.".freeagents WHERE game = {$game_id} AND player = {$player_id}");
        if($this->db->iRecords()) {
            return true;
        }
        return false;
    }
    
    //Check if play is on team
    function onTeam($game_id,$player_id) {
        if(!is_numeric($player_id))
            return false;
        $this->db->ExecuteSQL("SELECT id FROM ".MYSQL_DATABASE.".roster WHERE game = {$game_id} AND player = {$player_id}");
        if($this->db->iRecords()) {
            return true;
        }
        return false;
    }
    
    //Get free agents
    function getFreeagents($game_id,$limit=1) {
        $return = array();
        $limit = ($limit - 1) * 50;
        $this->db->ExecuteSQL("SELECT player_id FROM ".MYSQL_DATABASE.".freeagents JOIN ".MYSQL_DATABASE.".active_players ON active_players.player_id = freeagents.player WHERE freeagents.game = {$game_id}");
        $return['num_players'] = $this->db->iRecords();
        if ($return['num_players'] > 0) {
                $this->db->ExecuteSQL("SELECT active_players.*, freeagents.joindate as player_join_date FROM ".MYSQL_DATABASE.".freeagents JOIN ".MYSQL_DATABASE.".active_players ON active_players.player_id = freeagents.player WHERE freeagents.game = {$game_id} ORDER BY joindate DESC LIMIT {$limit},50");
            $return['players'] = $this->db->ArrayResults();
        }
        return $return;
    }
    
    //Get players
    function getPlayers($game_id,$limit=1) {
        $return = array();
        $limit = ($limit - 1) * 50;
        $this->db->ExecuteSQL("SELECT player_id FROM ".MYSQL_DATABASE.".roster JOIN ".MYSQL_DATABASE.".active_players ON active_players.player_id = roster.player WHERE roster.game = {$game_id}");
        $return['num_players'] = $this->db->iRecords();
        if ($return['num_players'] > 0) {
                $this->db->ExecuteSQL("SELECT active_players.*, roster.team AS player_team_id, team.name AS player_team_name FROM ".MYSQL_DATABASE.".roster JOIN ".MYSQL_DATABASE.".team ON roster.team = team.id JOIN ".MYSQL_DATABASE.".active_players ON active_players.player_id = roster.player WHERE roster.game = {$game_id} ORDER BY team.name ASC LIMIT {$limit},50");
            $return['players'] = $this->db->ArrayResults();
        }
        return $return;
    }
    
    //Get teams
    function getTeams($game_id,$limit=1) {
        $return = array();
        $limit = ($limit - 1) * 50;
        $this->db->ExecuteSQL("SELECT * FROM ".MYSQL_DATABASE.".active_teams WHERE team_game_id = {$game_id} ORDER BY team_continent DESC, team_region ASC");

        $return['num_teams'] = $this->db->iRecords();
        if ($return['num_teams'] > 0) {
                $this->db->ExecuteSQL("SELECT * FROM ".MYSQL_DATABASE.".active_teams WHERE team_game_id = {$game_id} ORDER BY team_continent DESC, team_region ASC LIMIT {$limit},50");
            $return['teams'] = $this->db->ArrayResults();
        }
        return $return;        
    }

    //Get schedule
    function getSchedule($game_id, $date = array()) {
        $return = array();

        if (!empty($date) && is_array($date)) {
            $from = $date[0];
            $to = $date[1];
            $range = "AND date >= $date[0]";
            if (isset($date[1])) {
                $range .= " AND date < $date[1]";
            }
        }

        $query = "SELECT * FROM ".MYSQL_DATABASE.".simple_schedule WHERE game_id = {$game_id} {$range}";

        $this->db->ExecuteSQL($query);
        if ($this->db->iRecords()) {
            $return = $this->db->ArrayResults();
        }
        return $return;
    }

    //List all games. Used in admin panel
    function all() {
        $query = "SELECT id, name, hostedservers FROM game WHERE deleted = 0";
        $this->db->ExecuteSQL($query);
        return $this->db->ArrayResults();
    }

    function get($id) {
        $this->db->ExecuteSQL(
          "SELECT * FROM game WHERE id = {$id}"
        );
        $game_info = $this->db->ArrayResult();
        
        $this->db->ExecuteSQL(
          "SELECT * FROM game_character_category WHERE game_id = {$id}"
        );
          
        if ($this->db->iRecords())
          $game_info['categories'] = $this->db->ArrayResults();
        else
          $game_info['categories'] = array();
        
        return $game_info;
    }

    //Save a modified game. Used in admin panel
    function save() {
        $table = 'game';
        $id = (int) $_REQUEST['id'];
        if ($this->db->update($table, $_POST, array('id' => $id), $Exclude = array('id','icon','logo','tile','banner', 'game_role'))) {
            $this->image->uploadGameImage("icon", $id);
            $this->image->uploadGameImage("logo", $id);
            $this->image->uploadGameImage("tile", $id);
            $this->image->uploadGameImage("banner", $id);
            if ($_POST['status'] == 0) {
                //Get all leagues for this game
                $query = "SELECT id FROM league WHERE game = {$id}";
                $this->db->ExecuteSQL($query);
                if ($this->db->iRecords()) {
                    $leagues = $this->db->ArrayResults();
                    foreach ($leagues as $l) {
                        //Remove all teams from a league
                        $this->db->update('team', array('league' => 0), array('league' => $l['id']));
                    }
                }
                //Get all ladders for this game
                $query = "SELECT id FROM ladder WHERE game = {$id}";
                $this->db->ExecuteSQL($query);
                if ($this->db->iRecords()) {
                    $ladders = $this->db->ArrayResults();
                    foreach ($ladders as $l) {
                        //Remove all teams from a ladder
                        $this->db->update('team', array('ladder' => 0), array('ladder' => $l['id']));
                    }
                }

                $this->db->update('league', array('status' => 0), array('game' => $id));
                $this->db->update('ladder', array('status' => 0), array('game' => $id));
                $this->db->update('tournaments', array('status' => 0), array('game' => $id));

                return true;
            }
            return true;
        }
        return false;
    }

    //Add a new game. Used in admin panel
    function add() {
        $table = 'game';
        if ($this->db->insert($_POST, $table, $Exclude = array('id'))) {
            $gameid = $this->db->lastId();
            $this->image->uploadtoprofile("icon", $table, $gameid);
            $this->image->uploadtoprofile("logo", $table, $gameid);
            $this->image->uploadtoprofile("tile", $table, $gameid);
            $this->image->uploadtoprofile("banner", $table, $gameid);
            return true;
        }
        return false;
    }

    /*
     * Deleting a league performs the following actions:
     * Sets game status to 0, and deleted to 1. Gam eno loner available via API
     * Sets all leagues, ladders & tournaments for a game to inactive (status 0)
     * Removes all players from team rosters. Updates player history
     * Removed all players from free agents
     */

    function delete() {
        $table = 'game';
        $id = (int) $_REQUEST['id'];
        //We never delete games, we just set their status to 0 & set the deleted flag.
        if ($this->db->update($table, array('status' => 0, 'deleted' => 1), array('id' => $id))) {
            //Get all leagues for this game
            $query = "SELECT id FROM league WHERE game = {$id}";
            $this->db->ExecuteSQL($query);
            if ($this->db->iRecords()) {
                $leagues = $this->db->ArrayResults();
                foreach ($leagues as $l) {
                    //Remove all teams from a league
                    $this->db->update('team', array('league' => 0), array('league' => $l['id']));
                }
            }
            //Get all ladders for this game
            $query = "SELECT id FROM ladder WHERE game = {$id}";
            $this->db->ExecuteSQL($query);
            if ($this->db->iRecords()) {
                $ladders = $this->db->ArrayResults();
                foreach ($ladders as $l) {
                    //Remove all teams from a ladder
                    $this->db->update('team', array('ladder' => 0), array('ladder' => $l['id']));
                }
            }

            $this->db->update('league', array('status' => 0), array('game' => $id));
            $this->db->update('ladder', array('status' => 0), array('game' => $id));
            $this->db->update('tournaments', array('status' => 0), array('game' => $id));

            //Check for all users still on a team. Update their player history before removing them
            $query = "SELECT player, team FROM `roster` WHERE `game` = {$id}";

            $this->db->ExecuteSQL($query);
            if ($this->db->iRecords()) {
                $history = new history();
                $players = $this->db->Arrayresults();
                foreach ($players as $player) {
                    $history->update($player['player'], $player['team'], 'leave');
                }
            }

            //clear rosters
            $this->db->delete('roster', array('game' => $id));

            //clear freeagents
            $this->db->delete('freeagents', array('game' => $id));


            return true;
        }
        return false;
    }

}

?>
