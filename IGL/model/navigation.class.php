<?php

class navigation {

    public function __construct() {
        $this->db = new mysql(MYSQL_DATABASE);
    }

    function ads() {
        $this->db->ExecuteSQL('SELECT * FROM ads LIMIT 1');
        if ($this->db->iRecords()) {
            $ad = $this->db->ArrayResult();
            $return['ad'] = $ad;
        } else {
            $return['ad'] = array();
        }
        return $return;
    }

    function game($game_id) {
        $return = array('All Schedules', 'Teams', 'Players', 'Free Agents', 'Admins');
        return $return;
    }
    
    function competitions($game_id) {
        //Default error message
        $return = array();

        //Ladder Settings/Info
        $this->db->ExecuteSQL("SELECT id as settings_id, name as comp_name, format as comp_format, continent as comp_continent FROM ladder_settings WHERE game = {$game_id} AND status = 1 AND deleted = 0 ORDER BY continent");
        if ($this->db->iRecords()) {
            $return['Ladder'] = $this->db->ArrayResults();
            
            //Ladder divisions/tiers
            foreach($return['Ladder'] as $i => $tier) {
                $this->db->ExecuteSQL("SELECT id AS division_id FROM ladder WHERE settings = {$tier['settings_id']} AND status = 1");
                if($this->db->iRecords()) {
                    $return['Ladder'][$i]['divisions'] = $this->db->ArrayResults();
                }
                
            }
            
        }

        //Tournaments
        $this->db->ExecuteSQL("SELECT id as comp_id, name as comp_name, format as comp_format, continent as comp_continent FROM tournaments WHERE is_published = 1 AND game = {$game_id}");
        if ($this->db->iRecords()) {
            $return['Tournament'] = $this->db->ArrayResults();
        }
        
        //Show Matches
        $this->db->ExecuteSQL("SELECT id as comp_id, name as comp_name FROM showmatches WHERE status = 1 AND game_id = {$game_id}");
        if ($this->db->iRecords()) {
            $return['Event'] = $this->db->ArrayResults();
        }

        return $return;
    }

}

?>