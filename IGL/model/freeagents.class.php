<?php
class freeagents {
    
public function __construct()  
{  
     $this->db             = new mysql(MYSQL_DATABASE);
     $this->player         = new player();
     $this->search         = new search();
}      

function addFreeagent($playerid,$game)
{
    $joindate=date('Y-m-d',time());
    $query = "SELECT * FROM
    (
    SELECT player as player from roster WHERE game = '{$game}'
    UNION ALL
    SELECT player from freeagents WHERE game = '{$game}'
    ) a
    WHERE player = '{$playerid}' LIMIT 1";

    $this->db->ExecuteSQL($query);

    if(!$this->db->iRecords())
    {
        $query = "INSERT into freeagents 
        (`id`, `player`, `username`, `game`, `joindate`)
        VALUES
        ('','{$playerid}','{$this->player->name($playerid)}',{$game}','{$joindate}')";
        $this->db->ExecuteSQL($query);
        return 	true;
    }
    return false;

}

function removeFreeagent($playerid,$league)
{
    $joindate=date('Y-m-d',time());
    $query = "DELETE FROM freeagents WHERE
    `player` = '".$playerid."' AND
    `league` = '".$league."' LIMIT 1";
    $this->db->ExecuteSQL($query);
    return 	$this->player->name($playerid). " is no longer a free agent.";
}


function fetch($game)
{
    $query = "SELECT * FROM freeagents WHERE game= '{$game}'";
    $this->db->ExecuteSQL($query);
    if(!$this->db->iRecords())
        false;
    return $this->db->ArrayResults();
}

}
?>