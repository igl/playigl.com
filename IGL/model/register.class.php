<?php
header('location: '.BASEURL);

//file is lengacy


class register {

    public function __construct()  
     {  
        global $vbulletin;
        $this->activity     = new activity();
        $this->db           = new mysql(MYSQL_DATABASE);
     }
    
    function addUser($username,$email,$password,$beta=true,$admin=false)
    {
        if( filter_var($email, FILTER_VALIDATE_EMAIL) )
        {
            if($username != $password)
            {
                $query = "SELECT playerid FROM player WHERE username = '{$username}' OR email = '{$email}' LIMIT 1";
                $this->db->ExecuteSQL($query);
                if( $this->db->iRecords() == 0 )
                {
                    if( strlen($password) > 7 ) 
                    {

                        $salt = salt();
                        
                        //Geolocation Data
                        $country_code = @file_get_contents('http://api.hostip.info/country.php?ip='.$_SERVER['REMOTE_ADDR']);

                        $added = $this->db->Insert(array(
                            'playerid' => '',
                            'usergroupid' => 2, 
                            'username' => $username, 
                            'password' => genPass($password,$salt), 
                            'passworddate' => date('Y-m-d',time()), 
                            'email' => $email, 
                            'joindate' => time(), 
                            'salt' => $salt, 
                            'country' => $country_code,
                            'referrerid' => $result['referer'],
                        ),'player');

                        if( $added )
                        {
                            $playerid = $this->db->lastId();
                            $this->activity->addEvent('Complete your profile to increase your visibility on IGL.',$playerid);
                            $this->activity->addEvent('Welcome to IGL',$playerid);
                            $this->db->Insert(array('playerid' => $playerid),'playertextfield');
                            $this->db->Insert(array('playerid' => $playerid),'playerfield');

                            if( false==$admin )
                            {
                                define(VB_AREA, 'EXTERNAL_REGISTER');
                                define(THIS_SCRIPT, 'REGISTER.CLASS');
                                if(basename(getcwd())=="admin")
                                    $dir = '..';
                                else
                                    $dir = '.';
                                global $vbulletin;
                                $cwd = getcwd();
                                chdir($dir.'/forums');
                                require_once('global.php');
                                require_once('includes/init.php');
                                require_once('includes/functions_login.php');
                                require_once('includes/functions.php');
                                chdir($cwd);

                                $_SESSION = fetch_userinfo($playerid);
                                vbsetcookie('playerid', $_SESSION['playerid'], true, true, true);
                                process_new_login('', true, true);
                                set_authentication_cookies(true);
                            }
                            if( $beta )
                            {
                                $this->db->Update('referral',array('activated' => 1, refereeId => $playerid),array('referee' => $email));
                                $query = "
                                    SELECT 
                                        `referral`.`id`, 
                                        `referral`.`referer`
                                    FROM 
                                        `referral`
                                    WHERE 
                                        `referral`.`referee` = '{$email}'
                                    LIMIT 1";

                                if($this->db->ExecuteSQL($query))
                                {
                                    if($this->db->iRecords())
                                    {
                                        $result = $this->db->ArrayResult();
                                        $this->db->Update('player',array('referrerid' => $result['referer']),array('playerid' => $playerid));
                                    }
                                    else 
                                    {
                                        $return['success'] = false;
                                        $return['error'] = "Please seek a referral to join IGL...";
                                        return json_encode($return);
                                    }  
                                }
                            }
                            echo "<script src='/js/mixpanel.js'></script>";
                            echo "<script type='text/javascript'>mixpanel.track('User Registration');</script>";
                            echo "<script type='text/javascript'>mixpanel.track('User Login');</script>";
                            $return['success'] = true;
                        }
                        else
                        {
                            $return['success'] = false;
                            $return['error'] = "User could not be added.";
                        }
                    }
                    else
                    {
                        $return['success'] = false;
                        $return['error'] = "Password must be at least 8 characters.";
                    }
                }    
                else
                {
                    $return['success'] = false;
                    $return['error'] = 'You double dipped the chip! You already have an <a href="/beta_login.php"><strong>account</strong></a>.';
                }
            }    
            else
            {
                $return['success'] = false;
                $return['error'] = 'Your username and password cannot be the same.';
            }   
        }
        else
        {
            $return['success'] = false;
            $return['error'] = 'Your email is not valid.';
        }
        return json_encode($return);
    }
 
 }
 ?>
