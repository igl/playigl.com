<?php

class serverConfig {

    public function __construct() {
        $this->db = new mysql(MYSQL_DATABASE);
    }

    function all($type='all') {
        if($type!='all') {
            $case = "WHERE type = '{$type}' ORDER BY type";
        }
        $this->db->ExecuteSQL("SELECT * FROM server_configs JOIN active_games ON game_id = game {$case}");
        if ($this->db->iRecords()) {
            return $this->db->ArrayResults();
        }
        return false;
    }

    function add() {
        if ($this->db->Insert($_POST, 'server_configs', array('id')))
            return $this->db->LastId();
        return false;
    }

    function get() {
        $id = $_REQUEST['id'];
        $this->db->ExecuteSQL("SELECT * FROM server_configs WHERE id = {$id}");
        if ($this->db->iRecords()) {
            return $this->db->ArrayResult();
        }
        return false;
    }

    function getAll() {
        $id = $_REQUEST['id'];
        $this->db->ExecuteSQL("SELECT * FROM server_configs WHERE game = {$id}");
        if ($this->db->iRecords()) {
            return $this->db->ArrayResults();
        }
        return false;
    }

    function save() {
        $id = $_REQUEST['id'];
        if ($this->db->Update('server_configs', $_POST, array('id' => $id)))
            return true;
        return false;
    }

    function delete() {
        $id = $_REQUEST['id'];
        if ($this->db->Delete('server_configs', array('id' => $id)))
            return true;
        return false;
    }

}

?>