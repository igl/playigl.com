<?php
class Article {

  protected $db;
  protected $image;
  
  public function __construct(&$db = NULL) {
    $this->db = is_null($db) ? new MySQL(MYSQL_DATABASE) : $db;
    $this->image = new Image();
  }

  function all() {
    $query = "SELECT id, title, rts, featured FROM article ORDER BY id DESC";
    $this->db->ExecuteSQL($query);
    if ($this->db->iRecords()) {
      return $this->db->Arrayresults();
    } else {
      return array();
    }
  }

  function add() {
    if (!empty($_FILES['banner'])) {
      if ($this->image->uploadArticleBanner("banner")) {
        list($width, $height) = getimagesize($_FILES['banner']['tmp_name']);
        if ($width == '640' && $height == '240') {
          $_POST['banner'] = $this->image->uploadArticleBanner("banner");
          $table = 'article';

          unset($_POST['dateposted']);
          $_POST['title'] = str_replace('’', "'", $_POST['title']);
          $_POST['content'] = str_replace('’', "'", $_POST['content']);
          if ($this->db->Insert($_POST, $table)) {
            $article_id = $this->db->lastId();
            $title = stripslashes($_POST['title']);
            $event = "Article posted: <a href='/article/{$article_id}-{$title}'>{$title}</a>.";
            return array('success' => true);
          } else {
            return array('success' => false, 'alert' => 'error', 'message' => 'Error posting article.');
          }
        } else {
          return array('success' => false, 'alert' => 'error', 'message' => 'Article image must be exactly 640 x 240.');
        }
      } else {
        return array('success' => false, 'alert' => 'error', 'message' => 'Unable to upload image, article was not posted.');
      }
    } else {
      return array('success' => false, 'alert' => 'error', 'message' => 'Articles cannot be submitted without an image.');
    }
  }

  function delete() {
    $id = (int) $_REQUEST['id'];
    return $this->db->Delete('article', array('id' => $id));
  }

  function save() {
    $table = 'article';
    $where = array('id' => $_POST['id']);
    $exclude = array('action');
    $_POST['title'] = str_replace('’', "'", $_POST['title']);
    $_POST['description'] = str_replace('’', "'", $_POST['description']);
    $_POST['content'] = str_replace('’', "'", $_POST['content']);
    if ($this->image->uploadArticleBanner("banner")) {
      $_POST['banner'] = $this->image->uploadArticleBanner("banner");
    } else {
      unset($_POST['banner']);
    }
    
    return $this->db->Update($table, $_POST, $where, $exclude);
  }

  function get($id) {
    $this->db->ExecuteSQL(
      "SELECT
        a.id, a.game_id, a.author_id, p.username as author_name, a.title, a.description, a.content, a.meta, a.banner, a.status, a.rts
      FROM article a
      JOIN player p ON p.playerid = a.author_id
      WHERE id = {$id}"
    );
    if ($this->db->iRecords()) {
      return $this->db->Arrayresult();
    } else {
      return false;
    }
  }

  function carousel($game_id = NULL, $limit = "LIMIT 0,5") {
    $return = array();
    
    $this->db->ExecuteSQL(
      "SELECT
        id, title, description, content, banner
      FROM
        article "
        . (!is_null($game_id) ? "WHERE game_id = {$game_id}" : "") . "
      ORDER BY featured DESC, rts DESC
      {$limit};"
    );
      
    if ($this->db->iRecords())
      $return = $this->db->ArrayResults();
    
    return $return;
  }
}

?>