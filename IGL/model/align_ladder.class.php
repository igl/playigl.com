<?php
/*
 * @copyright 2012 PlayIGL.com
 * @author Darryl Allen
 * This file grabs all teams in a ladder, equalizes their points based on weight, then redistributes teams among each ladder
 * 
 * This file runs every 7 days.
 */

class alignLadder {

    public function __construct(){  
        $this->db = new mysql(MYSQL_DATABASE);
    }

    function _getAllLadders() {
        $return = array();
        $this->db->ExecuteSQL(
            "SELECT DISTINCT(ladder_settings_id), ladder_game_name, ladder_name, ladder_format, ladder_maxteams FROM active_ladders"
            );
        if($this->db->iRecords())
            $return = $this->db->ArrayResults();
        return $return;
    }

    function _getParticipatingTeams($ladder_settings_id) {
        $return = array();
        $this->db->ExecuteSQL("
            
        SELECT 
            ladder_teams.team_id 
        FROM 
            ladder_teams
        JOIN
            active_teams ON active_teams.team_id = ladder_teams.team_id
        WHERE
            ladder_teams.settings_id = {$ladder_settings_id}
        ORDER BY team_points DESC");
        echo "There are a total of {$this->db->iRecords()} teams in this ladder...<br />";
        if($this->db->iRecords())
            $return = $this->db->ArrayResults();
        return $return;
    }
    
    function _addTier($ladder_settings_id) {
        $this->db->ExecuteSQL("UPDATE `ladder` SET status = 1 WHERE status = 0 AND settings = {$ladder_settings_id} ORDER BY id ASC LIMIT 1");
        if( $this->db->iRecords() )//No previous ladders removed, adding a new one
             echo "...Reactivated tier <br/>";
        if($this->db->Insert(array('settings' => $ladder_settings_id, 'status' => 1), 'ladder'))
            echo "...Added new tier <br/>";
        return false;
    }

    function _removeTier($ladder_settings_id,$num_ladders) {
        if($this->db->ExecuteSQL("UPDATE ladder SET status = 0 WHERE id IN ( SELECT id FROM ( SELECT id FROM ladder WHERE settings = {$ladder_settings_id} ORDER BY id ASC LIMIT {$num_ladders}, 500 ) AS tmp ) "))
            echo "...Removed tier<br/>";
        return false;
    }

    function _assignTeam($team_id,$tier_id,$settings_id)
    {
        $this->db->Update("ladder_teams",array('tier_id' => $tier_id),array('team_id' => $team_id, 'settings_id' => $settings_id));
        echo "--Assigned team_id {$team_id} to tier_id {$tier_id}";
        return false;
    }
    
    function _getActiveTiers($ladder_settings_id) {
        $return = array();
        $this->db->ExecuteSQL("SELECT * FROM active_ladders WHERE ladder_settings_id = {$ladder_settings_id}");
        if( $this->db->iRecords() )
            $return = $this->db->ArrayResults();
        return $return;
    }


}