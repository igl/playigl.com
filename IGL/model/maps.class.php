<?php

class maps {

    public function __construct() {
        $this->db = new mysql(MYSQL_DATABASE);
    }

    function getMap($id) {
        $sql = "SELECT 
                maplist.id, 
                maplist.map, 
                game.id AS game_id, 
                game.name AS game_name 
            FROM 
                maplist 
            JOIN 
                game ON game.id = maplist.game 
            WHERE 
                maplist.id = {$id}";
        $this->db->ExecuteSQL($sql);
        if ($this->db->iRecords()) {
            return $this->db->ArrayResult();
        } else {
            return false;
        }
    }

    function getAll($game) {
        $sql = "SELECT server_configs.`name` as config_name, maplist.id, maplist.map, game.id AS game_id, game.`name` AS game_name FROM maplist JOIN game ON game.id = maplist.game LEFT JOIN server_configs ON maplist.config_id = server_configs.id WHERE maplist.game = {$game} ORDER BY maplist.map";
        $this->db->ExecuteSQL($sql);
        if ($this->db->iRecords()) {
            return $this->db->ArrayResults();
        } else {
            return false;
        }
    }

    function getMapPools($game) {
        if(isset($game) && is_numeric($game)) {
        $sql = "SELECT * FROM mappools WHERE game = '{$game}'";
        }
        else {
            $sql = "SELECT * FROM mappools";
        }
        
        $this->db->ExecuteSQL($sql);
        if ($this->db->iRecords()) {
            return $this->db->ArrayResults();
        } else {
            return array();
        }
    }

    function getMapPool($id) {
        $sql = "SELECT * FROM mappools WHERE id = '{$id}'";
        $this->db->ExecuteSQL($sql);
        if ($this->db->iRecords()) {
            return $this->db->ArrayResult();
        } else {
            return array();
        }
    }

}

?>