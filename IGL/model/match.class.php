<?php

class match {

    public function __construct() {
        $this->db = new mysql(MYSQL_DATABASE);
        #$this->team           = new team();  
    }

    function matchdata($matchid) {
        $result = array();
        $query = "SELECT * FROM schedule WHERE id = " . $matchid;
        $this->db->ExecuteSQL($query);
        if ($this->db->iRecords > 0) {
            $result = $this->db->ArrayResult();

            $result['date'] = $result['scheduleddate'];
            if ($result['officialdate'])
                $result['date'] = $result['officialdate'];

            $query = "SELECT * FROM game WHERE id = " . $result['game'];
            $this->db->ExecuteSQL($query);
            if ($this->db->iRecords > 0) {
                $result['game'] = $this->db->ArrayResult();
            }
            if ($result['league'] > 0) {
                $query = "SELECT * FROM league WHERE id = " . $result['league'];
                $this->db->ExecuteSQL($query);
                $result['league'] = $this->db->ArrayResult();
            }
            if ($result['ladder'] > 0) {
                $query = "SELECT * FROM ladder JOIN ladder_settings ON ladder.settings = ladder_settings.id WHERE ladder.id = " . $result['ladder'];
                $this->db->ExecuteSQL($query);
                $result['ladder'] = $this->db->ArrayResult();
            }
            if ($result['tournament'] > 0) {
                $query = "SELECT * FROM tournaments WHERE id = " . $result['tournament'];
                $this->db->ExecuteSQL($query);
                $result['tournament'] = $this->db->ArrayResult();
            }
            // Team
            $query = "SELECT * FROM team WHERE id = " . $result['home'];
            $this->db->ExecuteSQL($query);
            $result['home'] = $this->db->ArrayResult();
            $query = "SELECT * FROM team WHERE id = " . $result['away'];
            $this->db->ExecuteSQL($query);
            $result['away'] = $this->db->ArrayResult();
            // Roster
            $query = "SELECT * FROM roster JOIN player ON roster.player = player.playerid WHERE roster.team = " . $result['away']['id'];
            $this->db->ExecuteSQL($query);
            $result['away']['roster'] = $this->db->ArrayResults();
            $query = "SELECT * FROM roster JOIN player ON roster.player = player.playerid WHERE roster.team = " . $result['home']['id'];
            $this->db->ExecuteSQL($query);
            $result['home']['roster'] = $this->db->ArrayResults();
            // Rounds
            $rounds = explode("|", $result['map']);
            $home_score = explode("|", $result['home_score']);
            $away_score = explode("|", $result['away_score']);
            $i = 0;
            foreach ($rounds as $r) {
                $result['maprounds'][$i] = array('id' => $i + 1, "map" => $r, "home_score" => $home_score[$i], "away_score" => $away_score[$i]);
                $i++;
            }
            // Similiar Matches
            $query = "SELECT * FROM schedule WHERE game = " . $result['game']['id'] . " AND id != " . $result['id'];
            $this->db->ExecuteSQL($query);
            $result['similar'] = $this->db->ArrayResults();

            foreach ($result['similar'] as $i => $s) {
                $result['similar'][$i]['date'] = $s['scheduleddate'];
                if ($s['officialdate'])
                    $result['similar'][$i]['date'] = $s['officialdate'];
            }

            // Teams
            $i = 0;
            foreach ($result['similar'] as $s) {
                $query = "SELECT * FROM team WHERE id = " . $s['home'];
                $this->db->ExecuteSQL($query);
                $result['similar'][$i]['home'] = $this->db->ArrayResult();
                $query = "SELECT * FROM team WHERE id = " . $s['away'];
                $this->db->ExecuteSQL($query);
                $result['similar'][$i]['away'] = $this->db->ArrayResult();
                $i++;
            }
        } else {
            $result = array('error' => $this->db->sLastError);
        }
        return $result;
    }

    /*
      function teamdata($matchid) {
      $matchdata  = $this->matchdata($matchid);
      $homeid     = $matchdata['home'];
      $awayid     = $matchdata['away'];

      $result = array();
      $result['home']             = $this->team($homeid);
      $result['home']['roster']   = $this->team->roster($matchdata['league'],$homeid);
      $result['away']             = $this->team($awayid);
      $result['away']['roster']   = $this->team->roster($matchdata['league'],$awayid);

      return $result;

      } */

    function stream($matchid) {
        $result = $this->matchdata($matchid);
        if ($result['stream'] == 0) {
            return "This match is not set to be livestreamed.";
        }
        return '<object type="application/x-shockwave-flash" height="378" width="620" id="live_embed_player_flash" data="http://www.twitch.tv/widgets/live_embed_player.swf?channel=playigl" bgcolor="#000000"><param name="allowFullScreen" value="true" /><param name="allowScriptAccess" value="always" /><param name="allowNetworking" value="all" /><param name="movie" value="http://www.twitch.tv/widgets/live_embed_player.swf" /><param name="flashvars" value="hostname=www.twitch.tv&channel=playigl&auto_play=true&start_volume=25" /></object>';
    }

}

?>
