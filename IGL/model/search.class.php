<?
class search {
    
public function __construct()  
     {  
          $this->db             = new mysql(MYSQL_DATABASE);
          $this->form           = new form();
     }    
    
function table($table,$term,$field=false,$view=1)
	{
	/* Pagination use:
	$previous = $results[0][0];
	$eachpage = $results[0][1];
	$next = $results[0][2];
	*/

	if(intval($_GET['view'])<2)
        {
            $view=1;
            $limitstart = 0;
        }
        else
        {
            $view = $_GET['view'];
            $limitstart = ($view-1)*NUMRESULTS;
        }            
        
	$columns = $this->form->getcolumns($table);
	$query = "SELECT * FROM $table WHERE ";
	#get field array. first value contains filed name, second value contains value to search
	foreach($columns as $column)
        {
            if($field)
                $query .= "{$field[0]}={$field[1]} AND ";
        $query .= "`{$column['Field']}` LIKE '%" .$term. "%' OR ";
        }
	$query = substr_replace($query ,"",-4);
	$this->db->ExecuteSQL($query); 
	$records = $this->db->iRecords();
	$query .= " LIMIT {$limitstart}, ".NUMRESULTS;
	$this->db->ExecuteSQL($query);  
	$results = $this->db->ArrayResults();
	if($this->db->iRecords == 0)
            return false;
	
	if(isset($_REQUEST['game']))
            $game = "&game={$_REQUEST['game']}";
	if($view>1)
		{
		$first = "<li><a href='?page={$_REQUEST['page']}&action={$_REQUEST['action']}{$game}&search={$_REQUEST['search']}&view=1'>First</a></li>";
		$previous = "<li><a href='?page={$_REQUEST['page']}&action={$_REQUEST['action']}{$game}&search={$_REQUEST['search']}&view=".($view-1)."'><</a></li> ";
		}
		$totalviews = ceil($records/NUMRESULTS);
			$count = 0;
			while($count < $totalviews)
				{
                                $count++;
				if($_GET['view']==$count)
                                    $eachpage .= "<li class='active'><a href='?page={$_REQUEST['page']}&action={$_REQUEST['action']}{$game}&search={$_REQUEST['search']}&view={$count}'>{$count}</a></li>";
                                else
                                    $eachpage .= "<li><a href='?page={$_REQUEST['page']}&action={$_REQUEST['action']}{$game}&search={$_REQUEST['search']}&view={$count}'>{$count}</a></li>";
				}
		if($view!=$totalviews)
                {
                    $view = $view++;
                    $next = "<li><a href='?page={$_REQUEST['page']}&action={$_REQUEST['action']}{$game}&search={$_REQUEST['search']}&view=".($view+1)."'>></a></li>";
                    $last = "<li><a href='?page={$_REQUEST['page']}&action={$_REQUEST['action']}{$game}&search={$_REQUEST['search']}&view={$totalviews}'>Last</a></li>";
                }
	$pagination = array($first,$previous,$eachpage,$next,$last);	
	$results = array($pagination,$results);
	return $results;
	}
	

}
?>