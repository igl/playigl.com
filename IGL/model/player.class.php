<?php

class player {

    public function __construct() {
        $this->activity = new activity();
        $this->db = new mysql(MYSQL_DATABASE);
    }
    
    function getTeams($player_id) {
        $return = array();
        $this->db->ExecuteSQL("SELECT team_id, team_name, CASE WHEN player.playerid IN (team_captain_id, team_alternate_id) THEN true ELSE false END AS is_captain FROM roster JOIN active_teams ON roster.team = active_teams.team_id JOIN player ON player.playerid = roster.player WHERE roster.player = {$player_id}");
        if($this->db->iRecords()) {
            $return = $this->db->ArrayResults();
        }
        return $return;
    }

    function all() {
        $query = "SELECT * FROM active_players";
        $this->db->ExecuteSQL($query);
        if ($this->db->iRecords()) {
            return $this->db->Arrayresults();
        }
        else
            return array();
    }

    function add() {
        if (isset($_POST['username']) && !empty($_POST['username']) && isset($_POST['email']) && !empty($_POST['email']) && isset($_POST['password']) && !empty($_POST['password'])) {
            //verify username is alphanumeric
            if (ctype_alnum($_POST['username'])) {
                //check if email is valid
                if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
                    //check if username exists
                    $query = "SELECT username FROM player WHERE username = '{$_POST['username']}'";
                    if ($this->db->ExecuteSQL($query) && !$this->db->iRecords()) {
                        //check if email exists
                        $query = "SELECT email FROM player WHERE email = '{$_POST['email']}'";
                        if ($this->db->ExecuteSQL($query) && !$this->db->iRecords()) {
                            if ($this->db->Insert('player', $_POST)) {
                                return true;
                            } else {
                                return array('success' => false, 'error' => "Unknown error adding player.");
                            }
                        } else {
                            return array('success' => false, 'error' => "The email {$_POST['email']} is already in use.");
                        }
                    } else {
                        return array('success' => false, 'error' => "The username {$_POST['username']} is already in use.");
                    }
                } else {
                    return array('success' => false, 'error' => "Email appears to be invalid.");
                }
            } else {
                return array('success' => false, 'error' => "Username must be alphanumeric.");
            }
        } else {
            return array('success' => false, 'error' => "You are missing some required fields.");
        }
    }

    function save() {
        $id = (int) $_POST['id'];

        if (isset($_POST['username']) && !empty($_POST['username']) && isset($_POST['email']) && !empty($_POST['email'])) {
            //verify username is alphanumeric
            if (ctype_alnum($_POST['username'])) {
                //check if email is valid
                if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
                    //check if username exists
                    $query = "SELECT username FROM player WHERE username = '{$_POST['username']}'";
                    $this->db->ExecuteSQL($query);
                    if (!$this->db->iRecords())
                        $available = true;
                    else
                        $available = false;

                    //Check if the username is in use by the current player
                    $query = "SELECT username FROM player WHERE username = '{$_POST['username']}' AND playerid = {$id}";
                    $this->db->ExecuteSQL($query);
                    if ($this->db->iRecords() || $available) {
                        //check if email exists
                        $query = "SELECT email FROM player WHERE email = '{$_POST['email']}' AND playerid = {$id}";
                        if ($this->db->ExecuteSQL($query) && $this->db->iRecords()) {
                            if (isset($_POST['password']) && strlen($_POST['password']) >= 8) {
                                $salt = salt();
                                $_POST['password'] = genPass($_POST['password'], $salt);
                                $_POST['salt'] = $salt;
                            } else {
                                unset($_POST['password']);
                            }
                            if ($this->db->Update('player', $_POST, array('playerid' => $id), array('id'))) {
                                return array('success' => true);
                            }
                            else
                                return array('success' => false, 'error' => 'Unknown error while saving player.');
                        }
                        else {
                            return array('success' => false, 'error' => "The email {$_POST['email']} is already in use.");
                        }
                    } else {
                        return array('success' => false, 'error' => "The username {$_POST['username']} is already in use.");
                    }
                } else {
                    return array('success' => false, 'error' => "Email appears to be invalid.");
                }
            } else {
                return array('success' => false, 'error' => "Username must be alphanumeric.");
            }
        } else {
            return array('success' => false, 'error' => "You are missing some required fields.");
        }
    }

    function delete($playerid) {
        if ($this->db->update('player', array('deleted' => 1), array('playerid' => $playerid))) {
            return true;
        }
        return 'Error removing player';
    }

    function get($playerid) {
        if (isset($playerid)) {
            $id = $playerid;
        } else {
            $id = (int) $_REQUEST['id'];
        }
        $query = "
            SELECT
                *
            FROM
                `player`
            WHERE 
                `player`.`playerid` = {$id}";

        if ($this->db->ExecuteSQL($query) && $this->db->iRecords()) {
            return $this->db->Arrayresult();
        }
        else
            return array();
    }

}

?>