<?php

//error_reporting(0);
class Backbone {

    //Declarations
    protected $db;
    protected $template;
    protected $data;
    protected $player_id = NULL;

    //Initialize
    public function __construct(&$db = NULL) {
        //includes
        include_once('matchservers.class.php');

        //Constructor
        $this->db = is_null($db) ? new MySQL(MYSQL_DATABASE) : $db;
        $this->match_server = new matchservers();
        $this->template = NULL;
        $this->data = array();
        $this->player_id = $_SESSION['playerid'];
    }

    //Close
    public function __destroy() {
        unset($this->db);
        unset($this->template);
        unset($this->data);
    }

    //Retrieve member variable
    public function __get($nm) {
        if ($nm == 'safe_data')
            return json_encode($this->data);
        return $this->$nm;
    }

    //Set member variables
    public function __set($nm, $val) {
        $this->$nm = $val;
    }

    //Check if member variable is set
    public function __isset($nm) {
        return isset($this->$nm);
    }

    //Unset member variable
    public function __unset($nm) {
        unset($nm);
    }

    private function _transcodeSQLFields($data, $ignore_fields, $translate_fields) {
        $sql_fields = array();
        $translate_keys = array_keys($translate_fields);

        foreach ($data as $key => $val) {
            if (in_array($key, $translate_keys))
                $sql_fields[$translate_fields[$key]] = $val;
            elseif (!in_array($key, $ignore_fields))
                $sql_fields[$key] = $val;
        }

        return $sql_fields;
    }

    public function getData($data_type, $page = NULL, $view = NULL, $data = NULL) {
        if (empty($data_type) || is_null($data_type))
            return;

        if (is_null($this->player_id))
            return;

        switch ($page) {
            case "dashboard":
                switch ($view) {
                    case "profile":
                        switch ($data_type) {
                            case "player": $this->_getPlayerData($data);
                                break;
                        }
                        break;

                    case "teams":
                        switch ($data_type) {
                            case "get player team":
                            case "get all player teams": $this->_getPlayerTeams($data);
                                break;
                            case "get all player teams and invites": $this->_getPlayerTeamsAndInvites($data);
                                break;
                            case "get team data": $this->_getTeamData($data);
                                break;
                            case "get team password": $this->_getTeamPassword($data);
                                break;
                            case "get team recruitment": $this->_getTeamRecruitment($data);
                                break;
                        }
                        break;

                    case "matches":
                        switch ($data_type) {
                            case "get all matches for player":
                                $this->_getAllPlayerMatches($data);
                                break;
                            case "get team data for score submission":
                                $this->_getTeamDataForScores($data);
                                break;
                        }
                        break;

                    case "referrals":
                        switch ($data_type) {
                            case "referrals": $this->_getPlayerReferrals($data);
                                break;
                        }
                        break;

                    case "following":
                        switch ($data_type) {
                            case "followee": $this->_getPlayerFollowing($data);
                                break;
                        }
                        break;
                }
                break;

            case null:
                switch ($data_type) {
                    case "games": $this->_getAllGames();
                        break;
                }
                break;
        }
    }

    public function getTemplate($page = NULL, $view = NULL) {
        $url = $_SERVER['DOCUMENT_ROOT'] . "/backbone/templates/{$page}/{$view}.php";

        ob_start();
        include($url);
        $this->template = ob_get_contents();
        ob_end_clean();
        return $this->template;
    }

    public function saveData($data_type, $page = NULL, $view = NULL, $data) {
        if (empty($data_type) || is_null($data_type))
            return;

        if (is_null($this->player_id))
            return;

        switch ($page) {
            case "dashboard":
                switch ($view) {
                    case "profile":
                        switch ($data_type) {
                            case "player": $this->_savePlayerData($data);
                                break;
                        }
                        break;

                    case "teams":
                        switch ($data_type) {
                            case "import roster from ugc": $this->_importRoster($data);
                                break;
                            case "create new team": $this->_createTeam($data);
                                break;
                            case "accept team invite": $this->_acceptTeamInvite($data);
                                break;
                            case "reject team invite": $this->_rejectTeamInvite($data);
                                break;
                            case "save team details": $this->_saveTeamData($data);
                                break;
                            case "save team password": $this->_saveTeamPassword($data);
                                break;
                            case "leave team": $this->_leaveTeam($data);
                                break;
                            case "promote player to captain": $this->_promoteCaptain($data);
                                break;
                            case "promote player to alternate captain": $this->_promoteAlternateCaptain($data);
                                break;
                            case "demote player from alternate captain": $this->_demoteAlternateCaptain($data);
                                break;
                            case "remove player from team": $this->_removeFromTeam($data);
                                break;
                            case "cancel invite to team": $this->_cancelTeamInvite($data);
                                break;
                            case "accept team join request": $this->_acceptTeamJoinRequest($data);
                                break;
                            case "reject team join request": $this->_acceptTeamJoinRequest($data, false);
                                break;
                            case "invite player to join team": $this->_inviteToTeam($data);
                                break;
                            case "invite email to join team": $this->_inviteEmailToTeam($data);
                                break;
                        }
                        break;

                    case "referrals":
                        switch ($data_type) {
                            case "revoke referral": $this->_cancelReferral($data);
                                break;
                            case "send referral": $this->_createReferral($data);
                                break;
                        }
                        break;

                    case "matches":
                        switch ($data_type) {
                            case "submit match url": $this->_saveMatchURL($data);
                                break;
                            case "submit match score": $this->_saveMatchScore($data);
                                break;
                            case "accept challenge": $this->_acceptChallenge($data);
                                break;
                            case "reject challenge": $this->_rejectChallenge($data);
                                break;
                        }
                }
                break;
        }
    }

    public function createMail($data, $template, &$mail) {
        $this->db->ExecuteSQL(
                "SELECT `from`, `body`, `subject` FROM `email_templates` WHERE `type` = '{$template}' LIMIT 1;"
        );

        $result = $this->db->ArrayResult();
        $message = $result['body'];
        $subject = $result['subject'];
        $from = $result['from'];

        $recipient_name = $data->user_name;
        $recipient_email = array($data->email);
        $team_name = $data->team_name;

        //Category for SendGrid stats
        $mail->setCategory('Team Join Requests');
        //Array of users to receive email
        $mail->setTos($recipient_email);
        //Set From email
        $mail->setFrom($from);
        //Set from display name
        $mail->setFromName('International Gaming League');
        //set subject
        $mail->setSubject(str_replace(
                        array("[team_name]"), array($team_name), $subject)
        );
        //set HTML message body
        $mail->setHtml(str_replace(
                        array("[player_username]", "[team_name]"), array($recipient_name, $team_name), $message)
        );

        return $mail;
    }

    //dive into multi-dimensional array to see if the user is a captain
    public function isCaptain($array, $key = 1, $val = 'Leader') {
        if (is_array($array)) {
            foreach ($array as $item)
                if (isset($item[$key]) && $item[$key] == $val)
                    return true;
        }
        return false;
    }

    //Check if the user exists based on community id
    public function userExists($community_id) {

        $this->db->ExecuteSQL("SELECT playerid, username FROM player WHERE communityid = {$community_id}");
        if ($this->db->iRecords()) {
            $r = $this->db->ArrayResult();
            return array('exists' => true, 'username' => $r['username'], 'player_id' => $r['playerid']);
        }
        return array('exists' => false);
    }

//check if a player is on a team
    public function onTeam($player_id, $game_id) {

        $this->db->ExecuteSQL("SELECT * FROM roster WHERE game = {$game_id} AND player = {$player_id}");
        if ($this->db->iRecords()) {
            return true;
        }
        return false;
    }

//send invite to a player
    public function sendInvite($player_id, $team_id) {
        if($player_id == $_SESSION['playerid'])
            return true;
        $this->db->ExecuteSQL("SELECT game as game_id FROM ".MYSQL_DATABASE.".team WHERE id = {$team_id}");
        if ($this->db->iRecords()) {
            $r = $this->db->ArrayResult();
            $game_id = $r['game_id'];
            if ($this->db->Insert(array(
                        'game' > $game_id,
                        'team' => $team_id,
                        'player' => $player_id
                            ), 'inviterequests')) {
                return true;
            }
        }
        return false;
    }

//Create an IGL user account
    public function createUser($username, $community_id) {

        if (is_numeric($community_id)) {


            $this->db->ExecuteSQL("SELECT username FROM player WHERE username = '{$username}'");
            if ($this->db->iRecords()) {
                $username .= rand(0, 10);
            }

            if ($this->db->Insert(array(
                        'username' => $username,
                        'communityid' => $community_id,
                        'joindate' => time()
                            ), "player")) {
                $player_id = $this->db->lastId();

                //Get image from Steam..
                $xml = file_get_contents("http://steamcommunity.com/profiles/{$community_id}?xml=1");
                $step_one = explode('<avatarFull>', $xml);
                $step_two = explode('<![CDATA[', $step_one[1]);
                $step_three = explode(']]></avatarFull>', $step_two[1]);
                $save = "../img/player/{$player_id}_avatar.png";
                file_put_contents($save, file_get_contents($step_three[0]));

                $this->db->Update("player", array('avatar' => "/player/{$player_id}_avatar.png"), array('playerid' => $player_id));
                return array('success' => true, 'player_id' => $player_id, 'username' => $username);
            }
        } else {
            echo "{$community_id} is not numeric";
        }

        return array('success' => false);
    }

    private function _importRoster($data) {

        $clan_id = $data->clan_id;
        $team_id = $data->team_id;
        if (is_numeric($clan_id) && is_numeric($team_id)) {

            //import roster
            $ugc_roster = json_decode(file_get_contents("http://www.ugcleague.com/team_roster_json.cfm?clan_id={$clan_id}"), true);
            $msg = array();

            if ($this->isCaptain($ugc_roster['DATA'])) {
                if (is_array($ugc_roster['DATA']))
                    foreach ($ugc_roster['DATA'] as $p) {
                        $user_exists = $this->userExists($p[5]);
                        if ($user_exists['exists']) {
                            $msg[] = "User {$p['0']} has an IGL account under the username '{$user_exists['username']}'.";
                            if ($this->onTeam) {
                                $msg[] = "{$user_exists['username']} is already on another team.";
                            } else {
                                $this->sendInvite($user_exists['player_id'], $team_id);
                                $msg[] = "Invite sent to {$user_exists['username']}.";
                            }
                        } else {
                            $create_user = $this->createUser($p[0], $p[5]);
                            if ($create_user['success']) {
                                $msg[] = "IGL Account created for {$create_user['username']} with Community ID {$p[5]}. User may now log into IGL using Steam.";
                                $this->sendInvite($create_user['player_id'], $team_id);
                                $msg[] = "Invite sent to {$create_user['username']}.";
                            } else {
                                $msg[] = "Unable to create an IGL account for {$p[0]}.";
                            }
                        }
                    }
                $return = array('success' => true, 'messages' => $msg);
            } else {
                $return = array('success' => false, 'error' => 'You are not captain of the team being imported');
            }
        } else {
            $return = array('success' => false, 'error' => 'Missing clan ID');
        }
        $this->data = $return;
    }

    private function _getAllGames() {
        $this->db->ExecuteSQL(
                "SELECT `id`, `name` FROM `game` WHERE status = 1;"
        );
        $this->data = $this->db->ArrayResults();
    }

    private function _getPlayerData($data) {
        $this->db->ExecuteSQL(
                "SELECT
        playerid as id,
        username as user_name,
        firstname as first_name,
        lastname as last_name,
        ingame_name,
        country as country,
        city as city,
        state as state,
        communityid as community_id,
        twitch as twitch,
        facebook_token as facebook_token,
        twitter_token as twitter_token,
        facebook as facebook,
        twitter as twitter,
        avatar as avatar,
        birthday as birthday,
        sex as sex,
        website as website,
        timezoneoffset as time_zone_offset
      FROM
        `player`
      WHERE playerid = '{$data['id']}'
      LIMIT 1;"
        );

        if ($this->db->iRecords()) {
            $return = $this->db->ArrayResult();

            $return["ingame_name"] = json_decode($return["ingame_name"]);

            $this->db->ExecuteSQL(
                    "SELECT
          id, name
        FROM
          game
        WHERE status = 1 AND deleted = 0
        ORDER BY id"
            );

            if ($this->db->iRecords())
                $return["game_labels"] = $this->db->ArrayResults();

            //Calculate age
            //$return["age"] = date("Y") - date("Y", strtotime($return['birthday']));

            $this->data = $return;
        }
    }

    private function _getPlayerExists($id) {
        $this->db->ExecuteSQL(
                "SELECT
        playerid as id,
      FROM
        `player`
      WHERE playerid = '{$id}'
      LIMIT 1;"
        );

        if ($this->db->iRecords())
            return true;

        return false;
    }

    private function _getPlayerTeams($data) {
        $return = array();

        $this->db->ExecuteSQL(
                "SELECT
        `team`.`id`,
        `team`.`tag`,
        `team`.`name`,
        `team`.`captain`,
        `team`.`alternate`,
        `roster`.`player` as player_id,
        `team`.`league`,
        `team`.`password`,
        `league`.`name` as `league_name`,
        `team`.`game`,
        `game`.`logo` as `game_logo`,
        `league`.`teamsize` as league_team_size,
        `team`.`logo`
    FROM
        `roster`
      LEFT JOIN `team` on `roster`.`team` = `team`.`id`
      LEFT JOIN `league` on `team`.`league` = `league`.`id`
      LEFT JOIN `game` on `team`.`game` = `game`.`id`
    WHERE
        `team`.`status` = 1 AND
        `roster`.`player` = '{$data['id']}'" .
                (!is_null($data['team_id']) ? " AND `team`.`id` = {$data['team_id']} LIMIT 1" : "")
        );

        if ($this->db->iRecords()) {
            $return = array();
            $my_teams = $this->db->ArrayResults();
            foreach ($my_teams as $doc) {
                $this->_getTeamFollowers($doc['id']);
                $doc['password'] = is_null($doc['password']) || empty($doc['password']) ? false : true;
                $doc['followers'] = is_null($this->data) ? 0 : count($this->data);

                $this->db->ExecuteSQL(
                        "SELECT count(`player`) as `players` FROM `roster` WHERE `team` = '{$doc['id']}'"
                );

                if ($this->db->iRecords) {
                    $team_players = $this->db->ArrayResult();
                    $doc['total_players'] = $team_players['players'];
                }
                else
                    $doc['total_players'] = 0;

                $return[] = $doc;
            }
        }

        if (!is_null($data['team_id']) && !empty($return))
            $return = array_pop($return);

        $this->data = $return;
    }

    private function _getPlayerInvites($data) {
        $return = array();

        $this->db->ExecuteSQL(
                "SELECT
        i.id,
        g.id as game_id,
        g.logo as game_logo,
        t.id as team_id,
        t.tag as team_tag,
        t.name as team_name,
        t.logo as team_logo,
        t.league as team_league
      FROM
        inviterequests i
      LEFT JOIN team t ON i.team = t.id
      LEFT JOIN game g ON i.game = g.id
      WHERE
        i.player = {$data['id']};"
        );

        if ($this->db->iRecords()) {
            $return = $this->db->ArrayResults();
        }

        $this->data = $return;
    }

    private function _getPlayerTeamsAndInvites($data) {
        $return = array();

        $this->_getPlayerTeams($data);
        $return['teams'] = $this->data;

        $this->_getPlayerInvites($data);
        $return['invites'] = $this->data;

        $this->data = $return;
    }

    private function _getTeamData($data) {
        $return = array();

        $this->db->ExecuteSQL(
                "SELECT
        `team`.`id`,
        `team`.`tag`,
        `team`.`name`,
        `team`.`captain` as captain_id,
        `team`.`alternate` as alternate_id,
        `team`.`steamgroup` as steam_group,
        `team`.`irc`,
        `team`.`twitter`,
        `team`.`website`,
        `team`.`league`,
        `team`.`open`,
        `team`.`continent`,
        `team`.`region`,
        `league`.`name` as `league_name`,
        `team`.`game`,
        `game`.`logo` as `game_logo`,
        `team`.`logo` as avatar
      FROM
        `team`
      LEFT JOIN `league` on `team`.`league` = `league`.`id`
      LEFT JOIN `game` on `team`.`game` = `game`.`id`
      WHERE
        `team`.`id` = '{$data['id']}'
      LIMIT 1;"
        );

        if ($this->db->iRecords()) {
            $return = $this->db->ArrayResult();
            $return['player_id'] = $this->player_id;

            $this->db->ExecuteSQL(
                    "SELECT
          `player`.`playerid` as player_id,
          `player`.`username` as user_name
        FROM
          `roster`
        LEFT JOIN `player` on `roster`.`player` = `player`.`playerid`
        WHERE
          `roster`.`team` = {$data['id']};"
            );

            $return['players'] = array();

            if ($this->db->iRecords()) {
                $players = $this->db->ArrayResults();

                foreach ($players as $doc)
                    $return['players'][] = $doc;
            }
        }

        $this->data = $return;
    }

    private function _getTeamRoster($data) {
        $return = array();

        $this->db->ExecuteSQL(
                "SELECT
        `id`,
        `name`,
        `captain` as captain_id,
        `alternate` as alternate_id
      FROM
        `team`
      WHERE
        `id` = '{$data->id}'
      LIMIT 1;"
        );

        if ($this->db->iRecords()) {
            $return = $this->db->ArrayResult();
            $return['player_id'] = $this->player_id;

            $this->db->ExecuteSQL(
                    "SELECT
          `player`.`playerid` as player_id,
          `player`.`username` as user_name,
          0 as remove,
          false as is_alternate
        FROM
          `roster`
        LEFT JOIN `player` on `roster`.`player` = `player`.`playerid`
        WHERE
          `roster`.`team` = {$data->id};"
            );

            $return['players'] = array();

            if ($this->db->iRecords()) {
                $players = $this->db->ArrayResults();

                foreach ($players as $doc) {
                    if ($doc['player_id'] == $return['alternate_id'])
                        $doc['is_alternate'] == true;

                    $return['players'][] = $doc;
                }
            }
        }

        $this->data = $return;
    }

    private function _getTeamPassword($data) {
        $return = array("password" => false);

        $this->db->ExecuteSQL(
                "SELECT
        `password`
      FROM
        `team`
      WHERE
        `id` = {$data['id']}
      LIMIT 1;"
        );

        if ($this->db->iRecords()) {
            $return = $this->db->ArrayResult();

            if (is_null($return['password']) || empty($return['password']))
                $return['password'] = false;
            else
                $return['password'] = true;
        }

        $this->data = $return;
    }

    private function _getTeamRecruitment($data) {
        $return = array();

        $this->db->ExecuteSQL(
                "SELECT
        `team`.`game` as game_id,
        `team`.`captain` as captain_id,
        `team`.`alternate` as alternate_id,
        `game`.`name` as `game_name`,
        `game`.`logo` as `game_logo`,
        `game`.`teamsize` as `team_size`
      FROM
        `team`
      LEFT JOIN `game` on `team`.`game` = `game`.`id`
      WHERE
        `team`.`id` = {$data['id']}
      LIMIT 1;"
        );

        if ($this->db->iRecords()) {
            $return = $this->db->ArrayResult();
            if (is_null($return['alternate_id']))
                $return['alternate_id'] = 0;
            $return['invites'] = array();
            $return['joins'] = array();
            $return['roster'] = array();
            $return['free_agents'] = array();

            $this->db->ExecuteSQL(
                    "SELECT
          `player`.`playerid` as id,
          `player`.`country` as country,
          `player`.`avatar`,
          `player`.`username` as user_name,
          `player`.`firstname` as first_name,
          `player`.`lastname` as last_name,
          `player`.`email` as email
        FROM
          `roster`
        LEFT JOIN `player` on `roster`.`player` = `player`.`playerid`
        WHERE
          `roster`.`team` = {$data['id']}
        ORDER BY
          `player`.`playerid` = {$return['captain_id']} DESC,
          `player`.`playerid` = {$return['alternate_id']} DESC,
          `player`.`joindate` DESC;"
            );

            if ($this->db->iRecords())
                $return['roster'] = $this->db->ArrayResults();

            $this->db->ExecuteSQL(
                    "SELECT
          `player`.`playerid` as id,
          `player`.`country` as country,
          `player`.`avatar`,
          `player`.`username` as user_name,
          `player`.`firstname` as first_name,
          `player`.`lastname` as last_name,
          `player`.`email` as email
        FROM
          `inviterequests`
        LEFT JOIN `player` on `inviterequests`.`player` = `player`.`playerid`
        WHERE
          `inviterequests`.`team` = {$data['id']};"
            );

            if ($this->db->iRecords())
                $return['invites'] = $this->db->ArrayResults();

            $this->db->ExecuteSQL(
                    "SELECT
          `player`.`playerid` as id,
          `player`.`country` as country,
          `player`.`avatar`,
          `player`.`username` as user_name,
          `player`.`firstname` as first_name,
          `player`.`lastname` as last_name,
          `player`.`email` as email
        FROM
          `joinrequests`
        LEFT JOIN `player` on `joinrequests`.`player` = `player`.`playerid`
        WHERE
          `joinrequests`.`team` = {$data['id']};"
            );

            if ($this->db->iRecords())
                $return['joins'] = $this->db->ArrayResults();

            $this->db->ExecuteSQL(
                    "SELECT
          `player`.`playerid` as id,
          `player`.`avatar`,
          `player`.`username` as user_name,
          `player`.`firstname` as first_name,
          `player`.`lastname` as last_name,
          `player`.`email` as email
        FROM
          `freeagents`
          LEFT JOIN `player` on `freeagents`.`player` = `player`.`playerid`
        WHERE
          `freeagents`.`game` = {$return['game_id']};"
            );

            if ($this->db->iRecords())
                $return['free_agents'] = $this->db->ArrayResults();
        }

        $this->data = $return;
    }

    private function _getPlayerReferrals($data) {
        $return = array();
        $return['invites'] = array();
        $return['progress'] = 0;
        $return['progress_percent'] = 0;

        $this->db->ExecuteSQL(
                "SELECT
        `id`,
        `referee` as email,
        `date`,
        `refereeId` as player_id,
        `activated`
      FROM
        `referral`
      WHERE
        `referer` = {$this->player_id}"
        );

        if ($this->db->iRecords()) {
            $return['invites'] = $this->db->ArrayResults();
            $return['progress'] = 0;

            foreach ($return['invites'] as $invite) {
                if ($invite['activated'])
                    $return['progress']++;
            }

            $return['progress_percent'] = 100 * round($return['progress'] / count($return['invites']), 2);
        }

        $this->data = $return;
    }

    private function _getTeamFollowers($data) {
        $return = array("follows" => array());

        $this->db = new MySQL(ACTIVITY);
        $this->db->ExecuteSQL(
                "SELECT
        actor_id AS id
      FROM
        notifications
      WHERE
        actor_type = 'player'
      AND subject_id = {$data}
      AND subject_type = 'team'
      ORDER BY rts;"
        );

        if ($this->db->iRecords())
            $this->data = $this->db->ArrayResults();
        else
            $this->data = null;

        $this->db = new MySQL(MYSQL_DATABASE);
    }

    private function _getPlayerFollowing($data) {
        $return = array("follows" => array());

        $this->db = new MySQL(ACTIVITY);
        $this->db->ExecuteSQL(
                "SELECT
        subject_id as id,
        subject_type as media_type
      FROM
        notifications
      WHERE
        actor_id = {$this->player_id}
      AND
        actor_type = 'player'
      ORDER BY rts;"
        );

        if ($this->db->iRecords()) {
            $followees = $this->db->ArrayResults();

            $this->db = new MySQL(MYSQL_DATABASE);

            foreach ($followees as $f) {
                switch ($f['media_type']) {
                    case "player":
                        $this->_getPlayerData(array("id" => $f['id']));
                        break;

                    case "team":
                        $this->_getTeamData(array("id" => $f['id']));
                        break;
                }

                $return["follows"][] = $this->data;
            }
        }

        $this->data = $return;
    }

    private function _getTeamPoints($team_id, $tier_id) {
        $return = array();

        $this->db->ExecuteSQL(
                "SELECT
          current_points,
          highest_points
        FROM
          `ladder_teams`
        WHERE
          `ladder_teams`.`team_id` = {$team_id}
              AND 
              `ladder_teams`.`tier_id` = {$tier_id}
        LIMIT 1;"
        );

        if ($this->db->iRecords()) {
            $return = $this->db->ArrayResult();
        }

        $this->data = $return;
    }

    private function _getAllPlayerMatches($data) {
        $return = array();

        $this->_getPlayerChallenges();
        $return['challenge'] = $this->data;

        $data['tense'] = "pending";
        $this->_getPlayerMatches($data);
        $return['upcoming'] = $this->data;

        $data['tense'] = "completed";
        $this->_getPlayerMatches($data);
        $return['completed'] = $this->data;

        $return['player_id'] = $this->player_id;
        $this->data = $return;
    }

    private function _getPlayerChallenges() {
        $return = array();

        $this->db->ExecuteSQL(
                "SELECT
        `c`.`id`,
        `home`.`id` as team_id,
        `home`.`tag` as team_tag,
        `home`.`name` as team_name,
        `home`.`captain` as team_captain,
        `home`.`alternate` as team_alternate_captain,
        IF({$this->player_id} IN (`r`.`player`), 1, 0) as on_challenger_team,
        `away`.`id` as this_team_id,
        `away`.`tag` as this_team_tag,
        `away`.`name` as this_team_name,
        `away`.`captain` as this_captain,
        `away`.`alternate` as this_alternate_captain,
        IF({$this->player_id} IN (`z`.`player`), 1, 0) as on_challengee_team,
        `c`.`map`,
        `c`.`time` as match_time,
        `g`.`id` as game_id,
        `g`.`logo` as game_logo,
        `g`.`name` as game_name
      FROM
        `ladder_challenge` c
      LEFT JOIN `team` as `home` ON `c`.`challenger` = `home`.`id`
      LEFT JOIN `team` as `away` ON `c`.`challengee` = `away`.`id`
      LEFT JOIN	`roster` as `r` ON `r`.`team` = `home`.`id`
      LEFT JOIN `roster` as `z` ON `z`.`team` = `away`.`id`
      LEFT JOIN `game` as `g` ON `g`.id = `home`.`game`
      WHERE
        (`r`.`player` = {$this->player_id} AND `c`.`deleted` = 0)
      OR
        (`z`.`player` = {$this->player_id} AND `c`.`deleted` = 0)
      ORDER BY
        `c`.time DESC;"
        );

        if ($this->db->iRecords())
            $return = $this->db->ArrayResults();

        $this->data = $return;
    }

    private function _getPlayerMatches($data) {
        $return = array();

        if (!isset($data['tense'])) {
            $this->data = $return;
            return;
        }

        $tense = $data['tense'] == "pending" ? 0 : 1;

        $this->db->ExecuteSQL(
                "SELECT
        `s`.`id`,
        `g`.`id` as game_id,
        `g`.`logo` as game_logo,
        `g`.`name` as game_name,
        `s`.`scheduleddate` as scheduled_date,
        `s`.`officialdate` as official_date,
        `s`.`home`,
        `s`.`away`,
        `home`.`name` as `home_name`,
        `home`.`tag` as `home_tag`,
        `home`.`id` as `home_id`,
        `home`.`logo` as `home_logo`,
        `home`.`wins` as `home_wins`,
        `home`.`losses` as `home_losses`,
        `home`.`captain` as `home_captain`,
        `home`.`alternate` as `home_alternate`,
        `away`.`name` as `away_name`,
        `away`.`tag` as `away_tag`,
        `away`.`id` as `away_id`,
        `away`.`logo` as `away_logo`,
        `away`.`wins` as `away_wins`,
        `away`.`losses` as `away_losses`,
        `away`.`captain` as `away_captain`,
        `away`.`alternate` as `away_alternate`,
        `matchservers`.`ip` AS server_ip,
        `matchservers`.`port` AS server_port,
        `sv_password` as server_password,
        `match_url_address`,
        `game`.`match_url`
      FROM
        `schedule` s
      LEFT JOIN `team` as `home` ON `s`.`home` = `home`.`id`
      LEFT JOIN `team` as `away` ON `s`.`away` = `away`.`id`
      LEFT JOIN	`roster` as `r` ON `r`.`team` IN (`home`.`id`, `away`.`id`)
      LEFT JOIN `game` as `g` ON `g`.id = `home`.`game`
      JOIN `game` ON `game`.`id` = `s`.`game`
      LEFT JOIN `matchreserve` ON `matchreserve`.`id` = `s`.`reservation_id`
      LEFT JOIN `matchservers` ON `matchservers`.`id` = `matchreserve`.`server_id`
      WHERE
        `s`.`completed` = {$tense}
      AND
        `r`.`player` = {$this->player_id}
      ORDER BY
        `s`.`officialdate` ASC
      LIMIT 15;"
        );

        if ($this->db->iRecords()) {
            $return = $this->db->ArrayResults();

            foreach ($return as $key => $match) {
                if (!is_null($match['official_date']))
                    $return[$key]['date'] = $match['official_date'];
                else if (!is_null($match['scheduled_date']))
                    $return[$key]['date'] = $match['scheduled_date'];

                /*  if (($return[$key]['date'] + 1800) > time()) {
                  $this->db->ExecuteSQL(
                  "SELECT
                  matchservers.ip AS server_ip,
                  matchservers.port AS server_port,
                  sv_password as server_password,
                  match_url_address,
                  match_url
                  FROM
                  schedule
                  JOIN
                  game ON game.id = schedule.game
                  LEFT JOIN `matchreserve` ON `matchreserve`.`id` = `schedule`.`reservation_id`
                  LEFT JOIN `matchservers` ON `matchservers`.`id` = `matchreserve`.`server_id`
                  WHERE
                  schedule.id = {$match['id']}
                  LIMIT 1"
                  );

                  $return[$key]['server_ip'] = NULL;
                  $return[$key]['match_url_address'] = NULL;
                  if ($this->db->iRecords()) {
                  $steam_info = $this->db->ArrayResult();

                  foreach ($steam_info as $skey => $field) {
                  if (!is_null($field))
                  $return[$key][$skey] = $field;
                  }
                  }
                  } */
                unset($return[$key]['official_date']);
                unset($return[$key]['scheduled_date']);
            }
        }
        $this->data = $return;
    }

    private function _getSchedule($data) {
        $return = array();

        $this->db->ExecuteSQL(
                "SELECT
        `home` as home_id,
        `away` as away_id,
        `officialdate` as official_date,
        `completed`,
        `league`,
        `ladder`,
        `scoring_method`,
        `round_details`
      FROM
        `schedule`
      WHERE
        `id` = {$data['id']}
      LIMIT 1;"
        );

        if ($this->db->iRecords()) {
            $return = $this->db->ArrayResult();
            $return['rounds'] = 0;

            if (!is_null($return['league']) && $return['league'] > 0) {
                $this->db->ExecuteSQL(
                        "SELECT `rounds` FROM `league` WHERE `id` = {$return['league']} LIMIT 1;"
                );

                if ($this->db->iRecords()) {
                    $rounds = $this->db->ArrayResult();
                    $return['rounds'] = $rounds['rounds'];
                }
            }

            if (!is_null($return['ladder']) && $return['ladder'] > 0) {
                $this->db->ExecuteSQL(
                        "SELECT `ladder_rounds` as rounds
          FROM
            `game`
          JOIN `ladder` ON `ladder`.`game` = `game`.`id`
          WHERE `ladder`.`id` = {$return['ladder']}
          LIMIT 1;"
                );

                if ($this->db->iRecords()) {
                    $rounds = $this->db->ArrayResult();
                    $return['rounds'] = $rounds['rounds'];
                }
            }
        }

        $this->data = $return;
    }

    private function _getTeamDataForScores($data) {
        //This will need to be adjusted for tournaments & leagues in order to geth their scoring methods
        $return = array();

        $this->db->ExecuteSQL(
                "SELECT
        z.rounds,
        z.round_details,
        IFNULL(z.scoring_method,0) as scoring_method
      FROM `schedule` s
      LEFT JOIN `ladder` l on `l`.`id` = `s`.ladder
      LEFT JOIN `ladder_settings` `z` on z.id = l.settings
      WHERE
        s.id = {$data['id']}
      LIMIT 1;"
        );

        if ($this->db->iRecords())
            $return = $this->db->ArrayResult();

        $this->data = $return;
    }

    private function _createTeam($data) {
        include_once("../includes/pusher.php");
        include_once("../includes/notify.php");

        $channels = array("player-{$this->player_id}");
        $ignore_fields = array("all_games", "disallowed_games");
        $translate_fields = array();

        $sql_fields = $this->_transcodeSQLFields($data, $ignore_fields, $translate_fields);
        $sql_fields['captain'] = $this->player_id;

        if (!in_array('', $sql_fields)) {

            $b = $this->db->Insert($sql_fields, 'team');

            if ($b && !$this->_isOnTeam(NULL, $sql_fields['game'])) {
                $sql_fields = array('game' => $data->game, 'team' => $this->db->lastId(), 'player' => $this->player_id);
                $this->db->Insert($sql_fields, 'roster');

                unset($sql_fields['team']);

                $this->db->Delete('freeagents', $sql_fields);
                $this->db->Delete('inviterequests', $sql_fields);
                $msgs = array("{$sql_fields['name']} created successfully.");
            } else {
                $msgs = array("There was an error creating this team.");
            }
        } else {
            $msgs = array("Please verify all fields are filled out.");
        }
        pushMessage($channels, $msgs);
    }

    private function _savePlayerData($data) {
        $ignore_fields = array("age", "field", "avatar", "media_type", "game_labels");
        $translate_fields = array(
            "id" => "playerid",
            "user_name" => "username",
            "first_name" => "firstname",
            "last_name" => "lastname",
            "community_id" => "communityid",
            "time_zone_offset" => "timezoneoffset"
        );

        $sql_fields = $this->_transcodeSQLFields($data, $ignore_fields, $translate_fields);

        if (isset($sql_fields['country']))
            $sql_fields['country'] = strtolower($sql_fields['country']);

        if (isset($sql_fields['password']) && strlen($sql_fields['password']) > 7) {
            $salt = salt();
            $password = $sql_fields['password'];
            $new_password = md5(md5($password) . $salt);

            $b = $this->db->ExecuteSQL(
                    "UPDATE
          `player`
        SET
          `password` = '{$new_password}',
          `salt` = '{$salt}'
        WHERE
          `playerid` = {$sql_fields['playerid']}"
            );

            if ($b)
                $sql_fields['passworddate'] = date('Y-m-d', time());
        }

        unset($sql_fields['password']);
        $this->db->Update('player', $sql_fields, array('playerid' => $sql_fields['playerid']));
    }

    private function _saveTeamData($data) {
        $ignore_fields = array("players", "name", "league", "league_name", "game", "game_logo", "ladder", "avatar", "player_id");
        $translate_fields = array(
            "captain_id" => "captain",
            "alternate_id" => "alternate",
            "steam_group" => "steamgroup"
        );

        $sql_fields = $this->_transcodeSQLFields($data, $ignore_fields, $translate_fields);
        $this->db->Update('team', $sql_fields, array('id' => $sql_fields['id']));
    }

    private function _saveTeamRoster($data) {
        $players = $data->players;

        foreach ($players as $p) {
            if ($p->remove) {
                if ($p->is_alternate)
                    $this->db->Update('team', array('alternate' => null), array('id' => $data->id));

                $this->db->Delete('roster', array('player' => $p->player_id, 'team' => $data->id), 1);
                $this->db->Delete('inviterequests', array('player' => $p->player_id, 'team' => $data->id), 1);
            }
        }
    }

    private function _saveTeamPassword($data) {
        if (is_null($data->password) || empty($data->password))
            $data->password = NULL;
        $this->db->Update('team', array('password' => $data->password), array('id' => $data->id));
    }

    private function _leaveTeam($data) {
        $this->_getTeamRoster($data);
        if ($this->player_id == $this->data['captain_id']) {
            if (count($this->data['players']) == 1) {
                $this->db->Update('team', array('status' => 0), array('id' => $this->data['id']));
                $this->db->Delete('roster', array('player' => $this->player_id, 'team' => $this->data['id']), 1);
            }
        } elseif ($this->player_id == $this->data['alternate_id']) {
            $this->db->Update('team', array('alternate' => 0), array('id' => $this->data['id']));
            $this->db->Delete('roster', array('player' => $this->player_id, 'team' => $this->data['id']), 1);
        }
        else
            $this->db->Delete('roster', array('player' => $this->player_id, 'team' => $this->data['id']), 1);
    }

    private function _promoteCaptain($data) {
        $ignore_fields = array("country", "avatar", "user_name", "first_name", "last_name", "email", "alternate_id");
        $translate_fields = array(
            "team_id" => "id",
            "id" => "captain"
        );

        $sql_fields = $this->_transcodeSQLFields($data, $ignore_fields, $translate_fields);

        if ($data->id == $data->alternate_id)
            $sql_fields['alternate'] = 0;

        $this->db->Update('team', $sql_fields, array('id' => $sql_fields['id']));
    }

    private function _promoteAlternateCaptain($data) {
        $ignore_fields = array("country", "avatar", "user_name", "first_name", "last_name", "email", "team_name");
        $translate_fields = array(
            "team_id" => "id",
            "id" => "alternate"
        );

        $sql_fields = $this->_transcodeSQLFields($data, $ignore_fields, $translate_fields);
        $this->db->Update('team', $sql_fields, array('id' => $sql_fields['id']));
    }

    private function _demoteAlternateCaptain($data) {
        $ignore_fields = array("country", "avatar", "user_name", "first_name", "last_name", "email", "team_name");
        $translate_fields = array(
            "team_id" => "id",
            "alternate_id" => "alternate"
        );

        $sql_fields = $this->_transcodeSQLFields($data, $ignore_fields, $translate_fields);
        $sql_fields['alternate'] = 0;
        $this->db->Update('team', array('alternate' => 0), array('id' => $sql_fields['id']));
    }

    private function _removeFromTeam($data) {
        $ignore_fields = array("country", "avatar", "user_name", "first_name", "last_name", "alternate_id", "email", "team_name");
        $translate_fields = array(
            "team_id" => "team",
            "id" => "player"
        );

        $sql_fields = $this->_transcodeSQLFields($data, $ignore_fields, $translate_fields);
        $this->db->Delete('roster', $sql_fields, 1);

        if ($data->id == $data->alternate_id) {
            $this->db->Update('team', array('alternate' => 0), array('id' => $data->team_id));
        }
    }

    private function _acceptTeamInvite($data) {
        $ignore_fields = array("id", "game_logo", "team_tag", "team_name", "team_logo", "email");
        $translate_fields = array(
            "team_id" => "team",
            "game_id" => "game",
            "team_league" => "league"
        );

        $sql_fields = $this->_transcodeSQLFields($data, $ignore_fields, $translate_fields);

        if (!array_key_exists('player', $sql_fields))
            $sql_fields['player'] = $this->player_id;

        $b = $this->db->Insert($sql_fields, 'roster');
        if ($b) {
            $this->db->Delete('inviterequests', array("id" => $data->id), 1);
            $this->db->Delete('team_email_invites', array("email" => $data->email));
        }
    }

    private function _rejectTeamInvite($data) {
        $this->db->Delete('inviterequests', array("id" => $data->id), 1);
    }

    private function _cancelTeamInvite($data) {
        $ignore_fields = array("country", "avatar", "user_name", "first_name", "last_name", "email", "team_name");
        $translate_fields = array(
            "team_id" => "team",
            "id" => "player"
        );

        $sql_fields = $this->_transcodeSQLFields($data, $ignore_fields, $translate_fields);
        $this->db->Delete('inviterequests', $sql_fields, 1);
    }

    private function _acceptTeamJoinRequest($data, $accept = true) {
        $ignore_fields = array("country", "avatar", "user_name", "first_name", "last_name", "game_id", "email", "team_name");
        $translate_fields = array(
            "team_id" => "team",
            "id" => "player"
        );

        $sql_fields = $this->_transcodeSQLFields($data, $ignore_fields, $translate_fields);
        $this->db->Delete('joinrequests', $sql_fields, 1);

        if ($accept) {
            $sql_fields['game'] = $data->game_id;
            $b = $this->db->Insert($sql_fields, 'roster');

            if ($b) {
                unset($sql_fields['team']);
                $this->db->Delete('freeagents', $sql_fields);
            }
        }
    }

    private function _inviteToTeam($data) {
        $ignore_fields = array("country", "avatar", "user_name", "first_name", "last_name", "email", "team_name");
        $translate_fields = array(
            "team_id" => "team",
            "game_id" => "game",
            "id" => "player",
            "game_id" => "game"
        );

        $sql_fields = $this->_transcodeSQLFields($data, $ignore_fields, $translate_fields);
        $this->db->Insert($sql_fields, 'inviterequests');
    }

    private function _inviteEmailToTeam($data) {
        $ignore_fields = array();
        $translate_fields = array();

        $this->db->executesql(
                "SELECT email FROM team_email_invites WHERE email = '{$data->email}'"
        );

        if (!$this->db->iRecords()) {
            $sql_fields = $this->_transcodeSQLFields($data, $ignore_fields, $translate_fields);
            $this->db->Insert($sql_fields, 'team_email_invites');
        }
    }

    private function _cancelReferral($data) {
        $ignore_fields = array();
        $translate_fields = array();

        $sql_fields = $this->_transcodeSQLFields($data, $ignore_fields, $translate_fields);
        $sql_fields['referer'] = $this->player_id;
        $this->db->Delete('referral', $sql_fields, 1);
    }

    private function _createReferral($data) {
        $ignore_fields = array();
        $translate_fields = array(
            "email" => "referee"
        );

        $sql_fields = $this->_transcodeSQLFields($data, $ignore_fields, $translate_fields);
        $sql_fields['referer'] = $this->player_id;

        $this->db->Insert($sql_fields, 'referral');
    }

    private function _winStreak($team_id) {
        //Get the teams 20 most recent matches, organized by DESC.
        $q = "
            SELECT `schedule`.`home`, `schedule`.`away`, `schedule`.`winner`, CASE WHEN `schedule`.`winner` = {$team_id} THEN 'true' ELSE 'false' END AS winner_flag
            FROM
                `schedule`
            WHERE
                {$team_id} IN (`schedule`.`home`, `schedule`.`away`)
            ORDER BY
                `schedule`.`officialdate` DESC
            LIMIT 20";
        if ($this->db->ExecuteSQL($q) && $this->db->iRecords()) {
            $streak = 0; //Set the bonus points to award the winner
            $r = $this->db->ArrayResults();
            foreach ($r as $v) {
                if ($v['winner_flag']) {
                    $streak++;
                } else {
                    break; // Kill the loop on the first loss detected
                }
            }
            if ($streak >= 5) { //only return bonus points if over 5
                return $streak;
            }
        }
        return 0;
    }

    private function _getCompType($matchid) {
        $q = "SELECT `league`, `ladder`, `tournament`, NOT (league IS NULL AND ladder IS NULL AND tournament IS NULL) AS comp_id FROM `schedule` WHERE `id` = {$matchid} ";
        $this->db->ExecuteSQL($q);
        $r = array_filter($this->db->ArrayResult());
        $this->data = array('type' => key($r), 'id' => $r['comp_id']);
    }

    private function _saveMatchURL($data) {
        $this->db->Update('schedule', array('match_url_address' => $data->match_url), array('id' => $data->id));
    }

    private function _saveMatchScore($data) {
        $this->_getCompType($data->id);
        $comp_type = $this->data;

        if (array_sum($data->home_score) > array_sum($data->away_score)) {
            $winner = $data->home_id;
            $loser = $data->away_id;
        } elseif (array_sum($data->home_score) < array_sum($data->away_score)) {
            $winner = $data->away_id;
            $loser = $data->home_id;
        }

        if ($comp_type['type'] == 'ladder' && $comp_type['id'] > 0) {

            //Get team points
            $this->_getTeamPoints($data->home_id, $comp_type['id']);
            $home_team = $this->data;

            $this->_getTeamPoints($data->away_id, $comp_type['id']);
            $away_team = $this->data;

            //Give the winning team 2.5% of the losing team's points
            //Dock the losing team 2.5% of their points

            if ($winner == $data->home_id) {
                if ($home_team['current_points'] > $away_team['current_points']) { //favoured win
                    //Get home diff
                    $home_diff = (.025 * $away_team['current_points']);

                    //Get away diff
                    $loser_multiplier = .975;
                    if ($data->forfeit == 1)
                        $loser_multiplier = .93;
                    $away_diff = ($away_team['current_points'] * $loser_multiplier) - $away_team['current_points'];
                } elseif ($home_team['current_points'] < $away_team['current_points']) { //underdog win
                    //Get home diff
                    $home_diff = .05 * $away_team['current_points'];

                    //Get away diff
                    $loser_multiplier = .95;
                    if ($data->forfeit == 1)
                        $loser_multiplier = .93;
                    $away_diff = ($away_team['current_points'] * $loser_multiplier) - $away_team['current_points'];
                }
                else { //equal points
                    //Get home diff
                    $home_diff = .0125 * $away_team['current_points'];

                    //Get away diff
                    $away_diff = ($away_team['current_points'] * .9875) - $away_team['current_points'];
                }
                $home_diff = $home_diff + $this->_winStreak($data->home_id);
            } elseif ($winner == $data->away_id) {
                if ($home_team['current_points'] > $away_team['current_points']) { //underdog win
                    //Get away diff
                    $away_diff = (.05 * $home_team['current_points']);

                    //Get away diff
                    $loser_multiplier = .95;
                    if ($data->forfeit == 1)
                        $loser_multiplier = .93;
                    $home_diff = ($home_team['current_points'] * $loser_multiplier) - $home_team['current_points'];
                } elseif ($home_team['current_points'] < $away_team['current_points']) { //favoured win
                    //Get home diff
                    $away_diff = .025 * $home_team['current_points'];

                    //Get away diff
                    $loser_multiplier = .975;
                    if ($data->forfeit == 1)
                        $loser_multiplier = .93;
                    $home_diff = ($home_team['current_points'] * $loser_multiplier) - $home_team['current_points'];
                }
                else { //equal points
                    //Get home diff
                    $loser_multiplier = .9875;
                    if ($data->forfeit == 1)
                        $loser_multiplier = .93;
                    $home_diff = ($away_team['current_points'] * $loser_multiplier) - $away_team['current_points'];

                    //Get away diff
                    $away_diff = .0125 * $away_team['current_points'];
                }
                $away_diff = $away_diff + $this->_winStreak($data->away_id);
            } else {
                //Looks like they tied... this is half win, half loss.
                if ($home_team['current_points'] > $away_team['current_points']) { //underdog win
                    //Get home diff
                    $away_diff = (.05 * $home_team['current_points']) + ($away_team['current_points'] * .975) - $away_team['current_points'];

                    //Get away diff
                    $home_diff = (.025 * $away_team['current_points']) + ($home_team['current_points'] * .95) - $home_team['current_points'];
                } elseif ($home_team['current_points'] < $away_team['current_points']) { //favoured win
                    //Get home diff
                    $away_diff = (.025 * $home_team['current_points']) + ($away_team['current_points'] * .95) - $away_team['current_points'];

                    //Get away diff
                    $home_diff = (.05 * $away_team['current_points']) + ($home_team['current_points'] * .975) - $home_team['current_points'];
                } else {// tie between 2 teams with equal points
                    //Get home diff
                    $away_diff = (.0125 * $home_team['current_points']) + ($away_team['current_points'] * .9875) - $away_team['current_points'];

                    //Get away diff
                    $home_diff = (.0125 * $away_team['current_points']) + ($home_team['current_points'] * .9875) - $home_team['current_points'];
                }
            }


            //Adjust points
            if ($home_diff > ($home_team['current_points'] * .1))
                $home_diff = $home_team['current_points'] * .1;
            if ($away_diff > ($away_team['current_points'] * .1))
                $away_diff = $away_team['current_points'] * .1;

            $home_team['current_points'] = $home_team['current_points'] + $home_diff;
            $away_team['current_points'] = $away_team['current_points'] + $away_diff;

            if ($home_team['current_points'] > $home_team['highest_points'])
                $home_team['highest_points'] = $home_team['current_points'];

            if ($away_team['current_points'] > $away_team['highest_points'])
                $away_team['highest_points'] = $away_team['current_points'];

            $this->db->Update('schedule', array(
                'home_diff' => $home_diff,
                'home_points' => $home_team['current_points'],
                'away_diff' => $away_diff,
                'away_points' => $away_team['current_points'],
                'forfeit' => $data->forfeit
                    ), array('id' => $data->id));
        }

        $b = $this->db->Update('schedule', array(
            'home_score' => implode($data->home_score, "|"),
            'away_score' => implode($data->away_score, "|"),
            'winner' => $winner,
            'loser' => $loser,
            'completed' => 1
                ), array('id' => $data->id));

        if ($b) {
            $this->db->Update('ladder_teams', array('current_points' => $home_team['current_points'], 'highest_points' => $home_team['highest_points']), array('tier_id' => $comp_type['id'], 'team_id' => $data->home_id));
            $this->db->Update('ladder_teams', array('current_points' => $away_team['current_points'], 'highest_points' => $away_team['highest_points']), array('tier_id' => $comp_type['id'], 'team_id' => $data->away_id));
        }
    }

    /*
      private function _clearSchedule($team_id, $time) {
      $this->db->ExecuteSQL("SELECT match_length FROM ladder_settings JOIN ladder ON ladder.settings = ladder_settings.id JOIN ladder_teams ON ladder_teams.tier_id = ladder.id WHERE ladder_teams.team_id = {$team_id}");
      if ($this->db->iRecords()) {
      $r = $this->db->ArrayResult();
      $start = $time;
      $end = $time + ($r['match_length'] * 3600) -1;
      //Remove Challenges
      $this->db->ExecuteSQL("DELETE FROM ladder_challenge WHERE {$team_id} IN (`challengee`,`challenger`) AND `time` BETWEEN {$start} AND {$end} ");
      //Remove Challenges
      $this->db->ExecuteSQL("DELETE FROM ladder_availability WHERE team_id = {$team_id} AND `time` BETWEEN {$start} AND {$end} ");
      }
      } */

    private function _acceptChallenge($data) {
        include_once("../includes/pusher.php");
        include_once("../includes/notify.php");
        $ignore_fields = array("id", "team_tag", "team_name", "on_challenger_team", "on_challengee_team", "this_team_tag", "this_team_name", "team_captain", "team_alternate_captain", "this_captain", "this_alternate_captain", "game_logo", "game_name");
        $translate_fields = array(
            "game_id" => "game",
            "team_id" => "home",
            "this_team_id" => "away",
            "match_time" => "officialdate"
        );

        $sql_fields = $this->_transcodeSQLFields($data, $ignore_fields, $translate_fields);
        $this->db->ExecuteSQL("SELECT ladder, reservation_id, listing_id FROM ladder_challenge WHERE id = {$data->id}");
        if ($this->db->iRecords()) {
            $l = $this->db->ArrayResult();
            $sql_fields['ladder'] = $l['ladder'];
            $sql_fields['reservation_id'] = $l['reservation_id'];
        }


        $sql_fields['sv_password'] = genServerPass();
        $b = $this->db->Insert($sql_fields, 'schedule');

        if ($b) {
            $this->db->Update('ladder_challenge', array('deleted' => 1), array('id' => $data->id));
            $this->db->Update('ladder_availability', array('deleted' => 1), array('id' => $l['listing_id']));

            $channels = getTeamChannels(getTeamChannels($sql_fields['home']), getTeamChannels($sql_fields['away']));
            $msgs = array();

            foreach ($channels as $c) {
                $msgs[] = "Match has been scheduled for " . date("F m, Y @ h:i:s a", $sql_fields['officialdate']);
            }

            pushMessage($channels, $msgs);
        }
    }

    private function _rejectChallenge($data) {
        $this->db->ExecuteSQL("DELETE FROM `matchreserve` WHERE id = ( SELECT reservation_id FROM ladder_challenge WHERE id = {$data->id} )");
        $this->db->Update('ladder_challenge', array('deleted' => 1, 'reservation_id' => 0), array('id' => $data->id));
    }

    private function _isOnTeam($team_id = null, $game_id = null) {
        $this->db->ExecuteSQL(
                "SELECT
        id
      FROM
        roster
      WHERE
        player = {$this->player_id}" .
                (!is_null($team_id) ? " AND team = {$team_id}" : "") .
                (!is_null($game_id) ? " AND game = {$game_id}" : "")
        );

        if ($this->db->iRecords())
            return true;
        return false;
    }

}
