<?php

// Ladder Class

class ladder {

    public function __construct() {
        $this->db = new mysql(MYSQL_DATABASE);
    }

    //Check if the player is on a team for the ladder this tier belongs to
    function onTeam($tier_id, $player_id) {
        if (!is_numeric($player_id))
            return false;
        $this->db->ExecuteSQL("
                                SELECT
                                        ladder_teams.*
                                FROM
                                        ladder_settings
                                JOIN ladder_teams ON ladder_teams.settings_id = ladder_settings.id
                                WHERE
                                        tier_id IN (
                                                SELECT DISTINCT
                                                        (tier_id)
                                                FROM
                                                        ladder_teams
                                                WHERE
                                                        settings_id IN (
                                                                SELECT
                                                                        settings_id
                                                                FROM
                                                                        ladder_teams
                                                                WHERE
                                                                        tier_id = {$tier_id}
                                                        )
                                        )
                                AND ladder_teams.team_id IN (
                                        SELECT
                                                team
                                        FROM
                                                roster
                                        WHERE
                                                player = {$player_id}
                                )");
        if ($this->db->iRecords()) {
            return true;
        }
        return false;
    }

    function teamInLadder($game_id, $player_id) {
        if (!is_numeric($player_id))
            return false;
        $this->db->ExecuteSQL("SELECT team_id FROM active_teams WHERE team_game_id = {$game_id} AND {$player_id} IN (`team_captain_id`, `team_alternate_id`)");
        if ($this->db->iRecords()) {
            return true;
        }
        return false;
    }

    function getMaps($ladder_id) {
        $return = array();
        $this->db->ExecuteSQL("SELECT mappool FROM ladder_settings JOIN ladder ON ladder.settings = ladder_settings.id WHERE ladder.id = {$ladder_id}");
        if ($this->db->iRecords()) {
            $r = $this->db->ArrayResult();
            $pool_id = $r['mappool'];

            $this->db->ExecuteSQL("SELECT poolarray FROM mappools WHERE id = {$pool_id}");

            if ($this->db->iRecords()) {
                $map_list = $this->db->ArrayResult();
                $pool_array = rtrim($map_list['poolarray'], ',');

                $this->db->ExecuteSQL(
                        "SELECT map FROM maplist WHERE id IN ({$pool_array})"
                );

                if ($this->db->iRecords()) {
                    $return = $this->db->ArrayResults();
                }
            }
        }
        return $return;
    }

    //Get Open Matches for a ladder tier
    function getMatchListings($tier_id) {
        $return = array();
        //Clear expired listings
        $this->db->ExecuteSQL("UPDATE ladder_availability SET deleted = 1 WHERE " . time() . " > time");

        //Get users team if signed in
        $team_id = 0;
        if (isset($_SESSION['playerid'])) {
            $this->db->ExecuteSQL("
                    SELECT
                            team.id as team_id
                    FROM
                            team
                    JOIN roster ON roster.team = team.id
                    JOIN ladder_teams ON ladder_teams.team_id = team.id
                    JOIN ladder ON ladder.id = ladder_teams.tier_id
                    WHERE
                            roster.player = {$_SESSION['playerid']}
                    AND tier_id = {$tier_id}
                    ");
            if ($this->db->iRecords()) {
                $r = $this->db->ArrayResult();
                $team_id = $r['team_id'];
            }
        }
        $time = time();
        $this->db->ExecuteSQL("SELECT
                                        ladder_availability.*, team. NAME AS team_name, schedule.id AS match_id,
                                        team.captain AS captain_id, team.alternate AS alternate_id,
                                 CASE
                                WHEN ladder_challenge.id > 0 AND `challenger` = {$team_id} THEN
                                        TRUE
                                ELSE
                                        FALSE
                                END AS challenged
                                FROM
                                        ladder_availability
                                JOIN team ON team.id = ladder_availability.team_id
                                LEFT JOIN ladder_challenge ON ladder_challenge.listing_id = ladder_availability.id
                                LEFT JOIN schedule ON `schedule`.officialdate = ladder_challenge.time AND ladder_challenge.challenger = `schedule`.`home` AND `ladder_challenge`.`challengee` = `schedule`.`away` AND `ladder_challenge`.`map` = 	`schedule`.`map`
                                WHERE
                                        tier_id = {$tier_id} AND {$time} < ladder_availability.time AND ladder_availability.deleted = 0
                                ORDER BY
                                        time ASC");
        if ($this->db->iRecords()) {
            $return = $this->db->ArrayResults();
        }
        return $return;
    }

    function getAvailability() {
        $return = array();
        if (is_numeric($_SESSION['playerid'])) {
            $this->db->ExecuteSQL("SELECT team.id as team_id FROM team JOIN ladder_teams ON ladder_teams.team_id = team.id WHERE ladder_teams.tier_id = {$_GET['tier_id']} AND {$_SESSION['playerid']} IN (`captain`,`alternate`)");
            if ($this->db->iRecords()) {
                $r = $this->db->ArrayResult();

                $this->db->ExecuteSQL("SELECT * from ladder_availability WHERE time > " . time() . " AND team_id = {$r['team_id']} ");
                if ($this->db->iRecords()) {
                    $return = $this->db->ArrayResults();
                }
            }
        }
        return $return;
    }

    //Set a teams availability
    function setAvailability() {
        $tier_id = $_POST['tier_id'];
        $time = $_POST['time'];
        $offset = $_POST['offset'] * 3600;
        $this->db->ExecuteSQL("SELECT team.id as team_id FROM team JOIN ladder_teams ON ladder_teams.team_id = team.id WHERE ladder_teams.tier_id = {$tier_id} AND {$_SESSION['playerid']} IN (`captain`,`alternate`)");
        if ($this->db->iRecords()) {
            $r = $this->db->ArrayResult();
            $this->db->Delete('ladder_availability', array('team_id' => $r['team_id'], 'tier_id' => $tier_id));

            foreach ($time as $i => $t) {
                $t = ISOtime($t);
                if ($t > 0) {
                    $gmtTime = $t - ($offset);
                    if ($gmtTime > time()) {

                        if (!$this->db->Insert(array('time' => $gmtTime, 'map' => $_POST['map'][$i], 'team_id' => $r['team_id'], 'tier_id' => $tier_id), 'ladder_availability')) {
                            return array('success' => false, 'error' => date('Y-m-d H:m:s', $t) . ' - Unable to set availability.');
                        }
                    } else {
                        return array('success' => false, 'error' => date('Y-m-d H:m:s', $t) . ' - Unable to set availability to past dates.');
                    }
                }
            }
        } else {
            return array('success' => false, 'error' => 'You are not captain of a team in this ladder tier.');
        }
        return array('success' => true);
    }

    function isCaptain($tier_id, $player_id) {
        if (!is_numeric($player_id))
            return false;
        $this->db->ExecuteSQL("
            SELECT
                    active_teams.team_id
            FROM
                    active_teams
            JOIN ladder_teams ON ladder_teams.team_id = active_teams.team_id
            WHERE
                    tier_id = {$tier_id}
            AND {$player_id} IN (
                    `team_captain_id`,
                    `team_alternate_id`)
            ");
        if ($this->db->iRecords()) {
            return true;
        }
        return false;
    }

    function getStandings($id) {
        $return = array();
        $this->db->ExecuteSQL("SELECT
                `active_teams`.`team_id`,
                `active_teams`.`team_name`,
                `active_teams`.`team_continent`,
                `active_teams`.`team_region`,
                `active_teams`.`team_tag`,
                `active_teams`.`team_rpi`,
                `ladder_teams`.`current_points` AS team_points,
                `ladder_teams`.`highest_points` AS team_highest_points,
                `active_teams`.`team_facebook`,
                `active_teams`.`team_twitter`,
                `active_teams`.`team_steamgroup`,
                `active_teams`.`team_website`,
                `active_teams`.`team_captain_id`,
                `active_teams`.`team_captain_name`,
                `active_teams`.`team_alternate_id`,
                `active_teams`.`team_alternate_name`,
                `active_teams`.`team_logo`,
                `active_teams`.`team_created`,
                `active_teams`.`team_biography`,
                (SELECT count(id) FROM `schedule` s WHERE `ladder` = {$id} AND `winner` = `active_teams`.`team_id`) as team_ladder_wins,
                (SELECT count(id) FROM `schedule` s WHERE `ladder` = {$id} AND `loser` =  `active_teams`.`team_id`) as team_ladder_loses
            FROM
                `active_teams`
            JOIN
                `ladder_teams` ON `ladder_teams`.`team_id` = `active_teams`.`team_id`
            WHERE
                `ladder_teams`.`tier_id` = {$id}
            ORDER BY team_points DESC"
        );

        if ($this->db->iRecords())
            $return = $this->db->ArrayResults();
        return $return;
    }

//Pass a single ladder tier id. Returns matches scheduled for all tiers of a ladder
    function getSchedule($id) {
        $return = array();
        $this->db->ExecuteSQL("SELECT
            s.id as match_id,
            s.ladder as tier_id,
            s.home as home_id,
            h.name AS home_name,
            h.tag AS home_tag,
            s.away as away_id,
            a.name AS away_name,
            a.tag AS away_tag,
            s.officialdate as date,
            s.map as maps
            FROM
            ladder l
            JOIN
            `schedule` s ON s.ladder = l.id
            JOIN team h ON h.id = s.home
            JOIN team a ON a.id = s.away
            WHERE
            l.settings = (SELECT settings FROM ladder WHERE id = {$id})
            ORDER BY date ASC");
        if ($this->db->iRecords())
            $return = $this->db->ArrayResults();
        return $return;
    }

    function getResults($id) {
        $return = array();
        $this->db->ExecuteSQL("
        SELECT
            s.id AS match_id,
            s.officialdate AS date,
            h.id AS home_id,
            h.name AS home_name,
            h.tag AS home_tag,
            s.home_points AS home_points,
            s.home_diff AS home_diff,
            s.home_score,
            a.id AS away_id,
            a.name AS away_name,
            a.tag AS away_tag,
            s.away_points AS away_points,
            s.away_diff AS away_diff,
            s.away_score,
            s.map AS maps
        FROM
            `schedule` s
        JOIN
            team AS h ON s.home = h.id
        JOIN
            team AS a ON s.away = a.id
        WHERE
            s.ladder = {$id} AND completed = 1");
        if ($this->db->iRecords())
            $return = $this->db->ArrayResults();

        return $return;
    }

// Get Full Ladder Array based on ID
    function get($id) {
        $id = (int) $id;
        $return = array();
        $query = "SELECT ladder_settings.* FROM `ladder_settings` JOIN `ladder` ON `ladder`.`settings` = `ladder_settings`.`id` WHERE `ladder`.`id` = {$id}";
        if ($this->db->ExecuteSQL($query) && $this->db->iRecords()) {
            $return = $this->db->ArrayResult();
        }
        return $return;
    }

    function all() {
        $query = "SELECT
                `ladder_settings`.`id`,
                `ladder_settings`.`game`,
                `ladder_settings`.`status`,
                `ladder_settings`.`deleted`,
                `ladder_settings`.`name`,
                `ladder_settings`.`continent`,
                `ladder_settings`.`details`,
                `ladder_settings`.`format`,
                `ladder_settings`.`rounds`,
                `ladder_settings`.`scoring_method`,
                `ladder_settings`.`round_details`,
                `ladder_settings`.`maxteams`,
                `ladder_settings`.`teamsize`,
                `ladder_settings`.`rules`,
                `ladder_settings`.`mappool`,
                `ladder_settings`.`livedate`,
                `game`.`name` AS game_name
            FROM
                ladder_settings
            JOIN
                game ON game.id = ladder_settings.game
            WHERE 
                `ladder_settings`.`deleted` = 0
                ORDER BY game_name, continent, format";
        $this->db->ExecuteSQL($query);
        return $this->db->ArrayResults();
    }

    function add() {
        $_POST['name'] = preg_replace('/[^a-zA-Z0-9_ \-()\/%-&]/s', '', $_POST['name']);

        $query = "SELECT id FROM ladder_settings WHERE name = '{$_POST['name']}' AND game = {$_POST['game']} AND continent = {$_POST['continent']}";
        $this->db->ExecuteSQL($query);
        if ($this->db->iRecords())
            return array('success' => false, 'error' => 'This game & format exists for this continent. Wait for the script to run for more tiers to be added.');

        $_POST['livedate'] = strtotime($_POST['livedate']);

        //Update the ladder settings
        if ($this->db->Insert($_POST, 'ladder_settings', array('action', 'id'))) {
            $this->db->Insert(array('settings' => $this->db->lastId()), 'ladder');
            return array('success' => true, 'ladder_id' => $this->db->lastId());
        }
        return array('success' => false, 'error' => 'Error saving ladder changes.');
    }

    function save($id) {
        $_POST['livedate'] = strtotime($_POST['livedate']);
        $_POST['registration_date'] = strtotime($_POST['registration_date']);

        //Check if the ladder is inactive
        if ($_POST['status'] == 0) {
            //Update the ladder settings
            $this->db->update('ladder_settings', $_POST, array('id' => $id), array('action', 'id'));

            //Remove all teams from inactive ladder
            $query = "SELECT ladder.id as ladder_id FROM ladder JOIN ladder_settings ON ladder_settings.id = ladder.settings WHERE ladder_settings.id = {$id}";
            $this->db->ExecuteSQL($query);
            if ($this->db->iRecords()) {
                foreach ($this->db->ArrayResults() as $l) {
                    $this->db->update('team', array('ladder' => 0), array('id' => $l['ladder_id'])); //Remove all teams from the ladder
                }
                return array('success' => true);
            } else {
                return array('success' => false, 'error' => 'Unable to locate ladder.');
            }
        } else {
            //Update the ladder settings
            if ($this->db->update('ladder_settings', $_POST, array('id' => $id), array('action', 'id')))
                return array('success' => true);
            return array('success' => false, 'error' => 'Error saving ladder changes.');
        }
    }

    function delete($id) {
        //Get all ladder IDs
        $query = "SELECT ladder.id FROM ladder JOIN ladder_settings ON ladder_settings.id = ladder.settings WHERE ladder_settings.id = {$id}";
        $this->db->ExecuteSQL($query);
        if ($this->db->iRecords()) {
            $a = $this->db->Update('ladder_settings', array('deleted' => 1, 'status' => 0), array('id' => $id));

            if ($a) {
                $b = $this->db->ExecuteSQL("DELETE FROM ladder_teams WHERE tier_id IN (SELECT ladder.id FROM ladder JOIN ladder_settings ON ladder_settings.id = ladder.settings WHERE ladder_settings.id = {$id})");
            }

            if ($a && $b) {
                return true;
            }
        } else {
            return false;
        }
    }

}

?>