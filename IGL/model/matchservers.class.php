<?php

class matchservers 
{

    public function __construct()  
    {  
         $this->db             = new mysql(MYSQL_DATABASE);
    }
    
    
    function getCommonLocations($home, $away) {
        $return = array();
        $return['success'] = false;
        $return['error'] = 'Unknown error';

        //Get home locations
        $q = "SELECT
            s.`primary_region`, s.`alternate_region`, s.`continent`
            FROM
            server_locations s
            JOIN team t ON s.`primary_region` = t.region AND s.`continent` = t.continent
            WHERE t.id = {$home}
            LIMIT 1";

        $this->db->ExecuteSQL($q);
        if($this->db->iRecords())
        {
            $l = $this->db->ArrayResult();
            $alts = explode('|', $l['alternate_region']);
            foreach ($alts as $k => $v)
              $alts[$k + 1] = $v;
            $alts[0] = $l['primary_region'];
            $locals['home'] = $alts;


            //Get away locations
            $q = "SELECT
                s.`primary_region`, s.`alternate_region`, s.`continent`
                FROM
                server_locations s
                JOIN team t ON s.`primary_region` = t.region AND s.`continent` = t.continent
                WHERE t.id = {$away}
                LIMIT 1";
            $this->db->ExecuteSQL($q);
            if($this->db->iRecords())
            {
                $l = $this->db->ArrayResult();
                $alts = explode('|', $l['alternate_region']);
                foreach ($alts as $k => $v)
                  $alts[$k + 1] = $v;
                $alts[0] = $l['primary_region'];
                $locals['away'] = $alts;


                //Here we intersect the team locations to determine which servers are best for both teams
                $regions = array_values(array_intersect($locals['home'], $locals['away']));

                $return['success'] = true;
                $return['continent'] = $l['continent'];
                $return['regions'] = $regions;
            }
            else {
                $return['error'] = 'Challenged team does not have a location set';
            }
            
        }
        else {
            $return['error'] = 'Challenger\'s team does not have a location set';
    }
        
        
        return $return;
        }

    function reserveServer($locations, $game_id, $competition_type, $time, $length) {
        $continent = $locations['continent'];
        //Run through each location & try to reserve a server in one...
        //There should always be a server available, as ladders restrict the # of sign-ups.
        foreach ($locations['regions'] as $k => $region) {
          $server_data = $this->reserve($game_id, $competition_type, $continent, $region, $time, $length);
          if ($server_data['success'])
            return $server_data;
        }
        return false; //Oh shit! No servers available for this location
    }
    
    function reserve($game,$competition_type,$continent,$region,$time,$match_length=1)
    {
        $length = (3600 * $match_length);
        $matchstart = $time+1;  //need to add a second to allow matches to start at end date.
        $matchend   = $time+$length; 
        $matchserver_ids = array();
        
        $sql = "SELECT id FROM `matchservers` WHERE `game` = {$game} AND continent = '{$continent}' AND region = '{$region}' AND competition_type = '{$competition_type}'";
        
        if($this->db->executeSQL($sql) && $this->db->iRecords())
        {
            
            $r = $this->db->ArrayResults();
            foreach( $r as $value )
                $matchserver_ids[] = $value['id'];
            
            //Get server ids already in use at this time
            $sql = "
                SELECT 
                    `server_id` as used
                FROM 
                    `matchreserve`
                INNER JOIN 
                    `matchservers` on `matchservers`.`id` = `matchreserve`.`server_id`
                WHERE 
                    `matchservers`.`game` = {$game} AND
                    ({$matchstart} BETWEEN `matchreserve`.`from` AND `matchreserve`.`to` OR
                    {$matchend} BETWEEN `matchreserve`.`from` AND `matchreserve`.`to`)";

            //server_ids always needs a value, or we break the query
            $server_ids = array(0);
            
            if( $this->db->ExecuteSQL($sql) && $this->db->iRecords() )
            $r = $this->db->ArrayResults();
            foreach( $r as $k => $value )
            {
                $server_ids[] = $value['used'];
            }


            $sql = "SELECT
                    `matchservers`.`id`,
                    `matchservers`.`ip`,
                    `matchservers`.`port`
                FROM
                    `matchservers`
                WHERE
                    `matchservers`.`game` = {$game}
                AND `matchservers`.`continent` = '{$continent}'
                AND `matchservers`.`region` = '{$region}'
                AND `matchservers`.`id` NOT IN (".trim(implode(',',$server_ids),',').")
                ORDER BY RAND() LIMIT 1";

            if( $this->db->ExecuteSQL($sql) && $this->db->iRecords() )
            {
                $doc = $this->db->ArrayResult();
                $server_id = $doc['id'];
                $server_ip = $doc['ip'];
                $server_port = $doc['port'];

                $starttime = round( $time / 900 ) * 900; //round start time to nearest 15 minutes. (this should have been done already)
                $finishtime = round( $matchend / 900 ) * 900; //round start time to nearest 15 minutes.

                if($this->db->Insert(array(
                    'server_id' => $server_id,
                    'from'      => $starttime,
                    'to'        => $finishtime
                ), 'matchreserve'))
                {
                    return array('success' => true, 'reservation_id' => $this->db->lastId(), 'from' => $starttime, 'to' => $finishtime, 'message' =>  'Match server has successfully been reserved.');
                }
                else
                {
                    return array('success' => false, 'message' => 'Match server could not be reserved.');
                }
            }
            else
            {
                return array('success' => false, 'message' => 'No match servers available for the requested time.');
            }
        }
        else
        {
            return array('success' => false, 'message' => 'There are no servers available for the game requested.');
        }
    }

    function locations()
    {
        $return = array('continents' => array(), 'regions' => array());
        $query = "SELECT DISTINCT(`continent`) FROM server_locations";
        $this->db->ExecuteSQL($query);
        $return['continents'] = $this->db->ArrayResults();
        
        $query = "SELECT DISTINCT(`primary_region`) as region FROM server_locations";
        $this->db->ExecuteSQL($query);
        $return['regions'] = $this->db->ArrayResults();
        
        return $return;
    }
    
    function get($id)
    {
        $query = "SELECT * FROM matchservers WHERE game = {$id} AND deleted = 0 ORDER BY competition_type, ip, port ASC";
        if( $this->db->ExecuteSQL($query) )
        {
            return array('success' => true, 'servers' => $this->db->ArrayResults());
        }
        else
            return array('success' => false, 'error' => 'Unable to load match servers');
    }    

    function save()
    {
        $id = $_REQUEST['id'];
        foreach( $_POST['ip'] as $i => $v )
        {
            if(isset($_POST['delete'][$i]) && $_POST['delete'][$i]==1)
            {
                $this->db->Update('matchservers',array('deleted' => 1),array('id' => $_POST['server_id'][$i]));
                continue;
            }
            elseif(isset($_POST['ip'][$i]) && !empty($_POST['ip'][$i]) && isset($_POST['port'][$i]) && !empty($_POST['port'][$i]) && isset($_POST['rcon'][$i]) && !empty($_POST['rcon'][$i]) )
            {
                $this->db->ExecuteSQL(
                        "SELECT competition_type FROM matchservers WHERE ip = '{$_POST['ip'][$i]}' AND port = '{$_POST['port'][$i]}' AND deleted = 0"
                        );
                if( $this->db->iRecords() )
                {
                    if( isset($_POST['server_id'][$i]) )
                    {
                        $this->db->Update('matchservers',
                            array(
                                'ip' => $_POST['ip'][$i],
                                'port' => $_POST['port'][$i],
                                'rcon' => $_POST['rcon'][$i],
                                'continent' => $_POST['continent'][$i],
                                'region' => $_POST['region'][$i],
                                'competition_type' => $_POST['competition_type'][$i]
                            ),array('id' => $_POST['server_id'][$i]));
                    }
                    else
                    {
                        $r = $this->db->ArrayResult();
                        if( $_POST['competition_type'][$i] == $r['competition_type'] )
                        {
                            return array('success' => false, 'error' => "Server already exists for {$_POST['ip'][$i]}:{$_POST['port'][$i]}. Please update the existing.");
                        }
                        else
                        {
                            return array('success' => false, 'error' => "Server already exists for {$r['competition_type']}. Please provide another server.");
                        }
                    }
                }
                else
                {
                    if( empty($_POST['server_id'][$i]) )
                    {
                        $this->db->Insert(array(
                                    'game' => $id,
                                    'ip' => $_POST['ip'][$i],
                                    'port' => $_POST['port'][$i],
                                    'rcon' => $_POST['rcon'][$i],
                                    'continent' => $_POST['continent'][$i],
                                    'region' => $_POST['region'][$i],
                                    'competition_type' => $_POST['competition_type'][$i]
                                    ),
                                    'matchservers');
                        
                    }
                }
                        

                
            }
            else
            {
                return array('success' => false, 'error' => "Some parameters are missing for {$_POST['ip'][$i]}");
            }
        }
        return array('success' => true);
    }
}

?>
