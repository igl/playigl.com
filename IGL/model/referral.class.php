<?php

class referral {

    public function __construct()  
     {  
          $this->db             = new mysql(MYSQL_DATABASE);
     }
    
    function referred($id)
    {
        $sql = "SELECT * FROM referral WHERE referer = '{$id}'";
        $this->db->ExecuteSQL($sql);
        return $this->db->ArrayResults();
    }
    
    function converted($id)
    {
        $sql = "SELECT id FROM referral WHERE referer = '{$id}' AND activated = 1";
        $this->db->ExecuteSQL($sql);
        return $this->db->iRecords();
    }
    
    function exists($email)
    {
        $from = 'referral';
        $where = array('referee' => $email);
        return $this->db->Select($from, $where);
    }
    
    function remaining($id,$allowed)
    {
        $remaining = $allowed-count($this->referred($id));
        return $remaining;
    }
    
    function send($emails,$referer)
    {
        
        $remaining = $this->remaining($referer);

        if($remaining < 1)
            {
            return false;
            }
            else
            {
                $table = "referral";
                if(empty($emails))
                    return false;
                $emails = array_filter($emails);
                foreach($emails as $email)
                {
                if(!filter_var($email, FILTER_VALIDATE_EMAIL))
                {
                    return false;
                }
                if($this->exists($email))
                {
                    return false;
                }
                else
                {
                    $referee = array('referee' => $email, 'referer' => $referer);
                    if(!$this->db->Insert($referee,$table))
                            return false;
                }
                    
                }               
            }
         return true;
    }
}

?>
