<?php
class news{

public function __construct()  
    {  
         $this->db             = new mysql(MYSQL_DATABASE);
    } 

    //Get all news posts
    function all()
    {
        $query = "
            SELECT 
                `news`.`id`, 
                `title`, 
                `game`.`name` as game_name, 
                `dateposted`
            FROM 
                `news`
            LEFT JOIN
                `game` on `game`.`id` = `news`.`game`
            ORDER BY
                `news`.`id` DESC";
        $this->db->ExecuteSQL($query);
        if( $this->db->iRecords() )
        {
            return $this->db->Arrayresults();
        }
        else
        {
            return array();
        }
    }
    
    //Get a specific news post
    function get()
    {
        $id = (int)$_REQUEST['id'];
        $query = "SELECT * FROM news WHERE id = {$id}";
        $this->db->ExecuteSQL($query);
        if( $this->db->iRecords() )
        {
            return $this->db->Arrayresult();
        }
        else
        {
            return false;
        }
    }
    
    //add a new news post
    function add()
    {
        $table = 'news';
        $exclude = array('action');
        $_POST['dateposted']= strtotime($_POST['dateposted']);
        $_POST['title'] = str_replace('’', "'", $_POST['title']);
        $_POST['content'] = str_replace('’', "'", $_POST['content']);
        if($this->db->Insert($_POST,$table,$exclude))
        {
            $news_id = $this->db->lastId();
            $title = stripslashes($_POST['title']);
            $event = "News posted: <a href='/news/{$news_id}-{$title}'>{$title}</a>.";
            $activity = new activity();
            $activity->addEvent($event, $_SESSION['playerid']);
            return true;
        }
        else
        {
            return false;
        }
    }

    //permanently delete a news post
    function delete()
    {
        $id = (int)$_REQUEST['id'];
        return $this->db->Delete('news',array('id' => $id));
    }

    //save changes to a news post
    function save()
    {  
        $table = 'news';
        $where = array('id'=>$_POST['id']);
        $exclude = array('action','ajax');
        $_POST['title'] = str_replace('’', "'", $_POST['title']);
        $_POST['content'] = str_replace('’', "'", $_POST['content']);
        $_POST['dateposted']= strtotime($_POST['dateposted']);

        return $this->db->Update($table,$_POST,$where,$exclude);
    }

    //Legacy functions below this point
/*
function name()
	{
	$query = "SELECT * FROM news";
	$this->db->ExecuteSQL($query);
	$result = $this->db->ArrayResults();
	foreach($result as $value)
		{
		$return[] = array ($value[id], $this->game->name($value[game])." - $value[title] ($value[dateposted])");
		}
	return $return;
	}
*/	
function profile($id=false,$game=false,$limit=false,$preview=false)
	{
        if($id)
            $id = "AND news.id = {$id}";
        if($game)
            $game = "AND game = {$game}";
        if($limit)
            $limit = "LIMIT {$limit}";
            
        $query = "SELECT news.id, news.title, news.game, game.icon as icon, news.content, news.poster 
            FROM news 
            LEFT JOIN game ON game.id=news.game WHERE 1=1 $id $game ORDER BY news.id DESC $limit";

        if($this->db->ExecuteSQL($query))
        {
            if($id)
                return $this->db->ArrayResult();
            return $this->db->ArrayResults();
        }
        else
        {
            return false;
        }
	}


}
?>