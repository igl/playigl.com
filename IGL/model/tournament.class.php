<?php

// Tournament Class

class tournament {

public function __construct()  
     {  
          $this->db             = new mysql(MYSQL_DATABASE);
     }
     
     
// Get Full Tournament Array based on ID
function get($id)
{
    $query = "SELECT * FROM tournaments WHERE id = $id";
    $this->db->ExecuteSQL($query);
    $tour_info = $this->db->ArrayResult();
    return $tour_info;
}

function getTeams($id)
{
    $query = "select * from tournament_teams join team on tournament_teams.team_id = team.id where tournament_id = ".$id;
    $this->db->ExecuteSQL($query);
    $result = array();
    $result['id'] = $id;
    $result['members'] = $this->db->ArrayResults();
    return $result;
}

function getTeamsByGame($id) {
    $query = "select * from team where game = ".$id;
    $this->db->ExecuteSQL($query);
    $result = $this->db->ArrayResults();
    return $result;
}

function addTeam($teamId, $tournamentId) {
     $table = 'tournament_teams';
    if($this->db->insert(array('tournament_id' => $tournamentId, 'team_id' => $teamId,  ), $table))
        return true;
    return false;
}

function deleteTeam($id) {
   $table = 'tournament_teams';
   if($this->db->delete($table, array('team_id' => $id)))
       return true;
   return false;
}

function getRound($id) {
    $query = "select * from tournament_schedule where id = ".$id;
    $this->db->ExecuteSQL($query);
    $result = $this->db->ArrayResults();
    return $result;
}

function getMatch($id) {
    $query = "select * from tournament_matches where id = ".$id;
    $this->db->ExecuteSQL($query);
    $result = $this->db->ArrayResult();
    return $result;
}

function saveRound($data, $id) {
    $table = 'tournament_schedule';
    if($this->db->update($table, $data, array('id' => $id), array('id', 'ajax', 'action')))
        return true;
    return false; 
}

function saveMatch($data, $id) {
    $table = 'tournament_matches';
    if($this->db->update($table, $data, array('id' => $id), array('id', 'ajax', 'action'))) {
        // Add to Schedule Table for Profile Submission
        $query = "select tournament_matches.id, tournaments.id as tid, tournaments.game from tournament_matches join tournaments on tournament_matches.tournament_id = tournaments.id where tournament_matches.id = ".$id;
        $this->db->ExecuteSQL($query);
        $result = $this->db->ArrayResult();
        $matchData = array("game" => $result['game'], "tournament" => $result['tid'], "home" => $data['team1_id'], "away" => $data['team2_id'], "officialdate" => $data['match_time'] );
        if ($this->db->insert($matchData, 'schedule')) {
            $submission_id = $this->db->lastId();
            $this->db->update($table, array('submission_id' => $submission_id), array('id' => $id));
            return true;
        } else {
            return false;
        }
    } else {
        return false; 
    }
}

function getSchedule($id) {
    $query = "select * from tournament_schedule where tournament_id = ".$id." order by round_number";
    $this->db->ExecuteSQL($query);
    $result = $this->db->ArrayResults();
    return $result;
}

function getMatches($id) {
    $query = "select * from tournament_matches where schedule_id = ".$id." order by match_number";
    $this->db->ExecuteSQL($query);
    $result = $this->db->ArrayResults();
    foreach($result as $i=>$r) {
        $query = "select team.name, tournament_teams.seed from team join tournament_teams on tournament_teams.team_id = team.id  where team.id =".$r['team1_id'];
        $this->db->ExecuteSQL($query);
        if ($this->db->iRecords()) {
            $rr = $this->db->ArrayResult();
            $result[$i]['team1_name'] = $rr['name'];
            $result[$i]['team1_seed'] = $rr['seed'];
        }
        $query = "select team.name, tournament_teams.seed from team join tournament_teams on tournament_teams.team_id = team.id  where team.id =".$r['team2_id'];
        $this->db->ExecuteSQL($query);
        if ($this->db->iRecords()) {
            $rr = $this->db->ArrayResult();
            $result[$i]['team2_name'] = $rr['name'];
            $result[$i]['team2_seed'] = $rr['seed'];
        }
    }
    return $result;
}

function createSchedule($tid, $size) {
    $table = 'tournament_schedule';
    if($this->db->insert(array('tournament_id' => $tid, 'name' => 'New Round', 'round_number' => 0, 'match_count' => $size), $table)) {
        $table = 'tournament_matches';
        $sid = $this->db->lastId();
        for($i=0;$i<$size;$i++) {
            $this->db->insert(array('tournament_id' => $tid, 'schedule_id' => $sid, 'match_number' => ($i+1)), $table);
        }
        return true;
    } else {
        return false;
    }
}

function deleteSchedule($id) {
    $table = 'tournament_schedule';
   if($this->db->delete($table, array('id' => $id))) {
       $table = 'tournament_matches';
       if($this->db->delete($table, array('schedule_id' => $id))) {
           return true;
       } else {
           return false;
       }
   } else {
    return false;
   }
}

function all()
{
    $query = "
        SELECT
            `tournaments`.`id` AS tournament_id,
            `tournaments`.`name` AS tournament_name,
            `tournaments`.`game` AS tournament_game_id,
            `game`.`name` AS  tournament_game_name, 
            `tournaments`.`registration_type` AS tournament_registrationstyle,
            `tournaments`.`format` AS tournament_format,
            `tournaments`.`map_pool` AS tournament_mappool,
            `tournaments`.`details` AS tournament_details,
            `tournaments`.`rules` AS tournament_rules,
            `tournaments`.`eventstartdatetime` AS tournament_startdatetime,
            `tournaments`.`eventenddatetime` AS tournament_enddatetime,
            `tournaments`.`checkinstartdatetime` AS tournament_checkinstartdatetime,
            `tournaments`.`checkinenddatetime` AS tournament_checkinenddatetime,
            `tournaments`.`prizedistribution` AS tournament_prizedistribution,
            `tournaments`.`totalcashprize` AS tournament_totalcashprize,
            `tournaments`.`totalproductprizevalue` AS tournament_totalproductprizevalue,
            `tournaments`.`status` AS tournament_status
        FROM 
            `tournaments`
        LEFT JOIN `game` ON `game`.`id` = `tournaments`.`game` WHERE `tournaments`.`status` = 1";
    $this->db->ExecuteSQL($query);
    return $this->db->ArrayResults();
}


function save($data, $id)
{
    $table = 'tournaments';
    if($this->db->update($table, $data, array('id' => $id), array('id', 'ajax', 'action')))
        return true;
    return false;
}

function lastError() {
    return $this->db->sLastError;
}

function create($data)
{
    $table = 'tournaments';
    if($this->db->insert($data, $table, array('action','id','ajax'))) {
        return true;
    } else {
        return false;
    }
}

function delete($id)
{
   $table = 'tournaments';
   if($this->db->update($table, array('status' => 0),array('id' => $id)))
       return true;
   return false;
   
}
    
  
}

?>
