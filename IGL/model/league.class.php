<?php
class league {

  public function __construct() {
    $this->db = new mysql(MYSQL_DATABASE);
    $this->image = new image();
  }

  function all() {
    $query = "
      SELECT
      `league`.`id` as league_id,
      `league`.`name` as league_name,
      `game`.`id` as league_game_id,
      `game`.`name` as league_game_name,
      `league`.`status` as league_status
      FROM
        `league`
      JOIN
        `game` ON `game`.`id` = `league`.`game`
      WHERE
        `league`.`deleted` = 0
      ORDER BY
          `league`.`status`, `game`.`name` ASC ";
    $this->db->ExecuteSQL($query);
    if ($this->db->iRecords()) {
      $result = $this->db->ArrayResults();
      foreach ($result as $k => $value) {
        if ($value['league_status'] == 0) {
          $result[$k]['league_name'] .= " (inactive)";
        }
      }
      return $result;
    } else {
      return array();
    }
  }

  //Add a new league to the DB
  function add() {
    $table = 'league';
    //timezone offset
    $offset = $_POST['offset'] * 60 * 60;

    $_POST['livedate'] = strtotime($_POST['livedate']);

    //apply offset to time
    $_POST['time'] = strtotime($_POST['time']) - $offset;

    if ($this->db->insert($_POST, $table, array('offset', 'action', 'id'))) {
      $id = $this->db->LastId();
      $this->image->uploadtoprofile("icon", $table, $id);
      $this->image->uploadtoprofile("logo", $table, $id);
      return true;
    }
    else
      return false;
  }

  //Save league changes
  function save() {
    $table = 'league';
    $id = (int) $_POST['id'];
    //timezone offset
    $offset = $_POST['offset'] * 60 * 60;
    //Offset isn't stored in the database
    unset($_POST['offset']);

    $_POST['livedate'] = strtotime($_POST['livedate']);

    //apply offset to time
    $_POST['time'] = strtotime($_POST['time']) - $offset;

    if ($this->db->update($table, $_POST, array('id' => $id))) {
      $this->image->uploadtoprofile("icon", $table, $id);
      $this->image->uploadtoprofile("logo", $table, $id);
      return true;
    }
    else
      return false;
  }

  function get($id) {
    $query = "SELECT * FROM league WHERE id = $id";
    $this->db->ExecuteSQL($query);
    return $this->db->ArrayResult();
  }
  /*
   * We don't need to delete any teams or player data, as teams may still participate in other leagues, ladders or tournaments
   */

  function delete() {
    $id = (int) $_REQUEST['id'];
    //We never delete leagues, we just set their status to 0 & set the deleted flag.
    if ($this->db->update('league', array('status' => 0, 'deleted' => 1), array('id' => $id))) {
      //Remove all teams from a league
      $this->db->update('team', array('league' => 0), array('league' => $id));
      return true;
    }
    return false;
  }

  function standings($id, $season) {
    $query = "
      SELECT
        `standings`.`game` as team_game_id,
        `game`.`name` as team_game_name,
        `standings`.`league` as team_league_id,
        `league`.`name` as team_league_name,
        `standings`.`team` as team_id,
        `team`.`name` as team_name,
        `standings`.`season` as team_season,
        `standings`.`wins` as team_wins,
        `standings`.`loses` as team_loses,
        `standings`.`rpi` as team_rpi
      FROM
        `standings`
      JOIN
        `game` ON `game`.`id` = `standings`.`game`
      JOIN
        `league` ON `league`.`id` = `standings`.`league`
      JOIN
        `team` ON `team`.`id` = `standings`.`team`
      WHERE
        `standings`.`league` = {$id} AND `standings`.`season` = {$season}
      ORDER BY
        `standings`.`rpi` DESC, `standings`.`wins` DESC";

    $this->db->ExecuteSQL($query);
    if ($this->db->iRecords())
      return $this->db->ArrayResults();
    return false;
  }

//legacy functions below this line

  function schedule($league, $season, $week = false, $broadcast = false) {
    if (!$week)
      $week = "(SELECT max(week) from schedule WHERE league = $league)";
    $query = "SELECT * FROM `schedule` WHERE week = $week AND league = $league";
    if ($broadcast)
      $query .= " AND stream = 1 LIMIT 1";
    $this->db->ExecuteSQL($query);
    return $this->db->ArrayResults();
  }

  function nextstream($id) {
    $query = "SELECT * FROM `schedule` WHERE league = " . $id . " AND week = " . $this->currentweek($id) . " AND season = " . $this->currentseason($id) . " AND stream = 1 LIMIT 1";
    $this->db->ExecuteSQL($query);
    if ($this->db->iRecords == 0) {
      return false;
    }
    return $this->db->ArrayResult();
  }

  function nextmatch($id) {
    $query = "SELECT * FROM `schedule` WHERE league = " . $id . " AND week = " . $this->currentweek($id) . " AND season = " . $this->currentseason($id) . " LIMIT 1";
    if ($this->db->iRecords == 0) {
      return false;
    }
    $this->db->ExecuteSQL($query);
    return $this->db->ArrayResult();
  }

  function game($id) {
    if (!intval($id))
      return false;
    $result = $this->profile($id);
    return $result['game'];
  }

  function profile($id) {
    $query = "SELECT league.id, league.game, league.weight, league.order, league.name, league.format, league.rounds, league.day, league.time, league.currentseason, league.currentweek, league.totalweeks, league.livedate, league.rules, league.status, game.about AS about, game.forum
      FROM league
      JOIN game ON league.game = game.id
      WHERE league.id = '{$id}'";
    $this->db->ExecuteSQL($query);
    if ($this->db->iRecords())
      return $this->db->ArrayResult();
    return false;
  }

  function rules($id) {
    return $this->profile['rules'];
  }

  function name($id, $link = true) {
    $query = "SELECT name FROM league WHERE id = '{$id}'";
    $this->db->ExecuteSQL($query);
    if ($this->db->iRecords()) {
      $result = $this->db->ArrayResult();
      if ($link) {
        return "<a href=\"?page=league&id=$id\">" . $result['name'] . "</a>";
      } else {
        return $result['name'];
      }
    } else {
      return false;
    }
  }

  function currentseason($id) {
    $result = $this->profile($id);
    return $result['currentseason'];
  }

  function day($id) {
    $result = $this->profile($id);
    return $result['day'];
  }

  function matchtime($id) {
    $result = $this->profile($id);
    return $result['time'];
  }

  function currentweek($id) {
    $result = $this->profile($id);
    return $result['currentweek'];
  }

  function totalweeks($id) {
    $result = $this->profile($id);
    return $result['totalweeks'];
  }

  function rounds($id) {
    $result = $this->profile($id);
    return $result['rounds'];
  }

  function teams($league) {
    $query = "SELECT id, name FROM team WHERE league = $league";
    $this->db->ExecuteSQL($query);
    $result = $this->db->ArrayResults();
    foreach ($result as $value) {
      $return[] = array($value['id'], $value['name']);
    }
    if ($MySQL->iRecords == 0) {
      $return[] = array("0", "NULL");
    }
    return $return;
  }

  function listmatchtype($league) {
    $query = "SELECT id, type FROM matchtype";
    $this->db->ExecuteSQL($query);
    $result = $this->db->ArrayResults();
    foreach ($result as $value) {
      $return[] = array($value['id'], $value['type']);
    }
    return $return;
  }

  function mapoftheweek($week, $league) {
    $query = "SELECT map FROM maplist WHERE week = $week AND league = $league AND season = " . $this->currentseason($league) . " LIMIT 1";
    $this->db->ExecuteSQL($query);

    if ($this->db->iRecords > 0) {
      $result = $this->db->ArrayResult();
      return $result['map'];
    }
    return "Map";
  }

  function endSeason($id) {
    $query = "SELECT id FROM schedule WHERE completed = 0 AND season =" . $this->currentseason($id) . " AND league = $id LIMIT  1";
    $this->db->ExecuteSQL($query);
    if ($this->db->iRecords == 1) {

      ?>
      <script language="JavaScript">
        function code()
        {
          if (!confirm("There are outstanding matches. Please confirm all matches have been completed."))
            history.go(-1);
          return""
        }
        document.writeIn(code())
      </script>
      <?php
    } elseif ($this->currentweek($id) < $this->totalweeks($id)) {

      ?>
      <script language="JavaScript">
        function code()
        {
          if (!confirm("The current week is less than the total weeks. Season not advanced."))
            history.go(-1);
          return""
        }
        document.writeIn(code())
      </script>
      <?php
    } else {
      $leaguename = $this->name($id, false);

      ?>
      <script language="JavaScript">
        function code()
        {
          if (!confirm("Are you sure you want to advance the season for <?php echo $leaguename; ?>?"))
            history.go(-1);
          return""
        }
        document.writeIn(code())
      </script>
      <script language="JavaScript">
        function code()
        {
          if (!confirm("Season Advanced! Live date reset. Week reset to 1."))
            history.go(-1);
          return""
        }
        document.writeIn(code())
      </script>
      <?php
      $query = "UPDATE league SET
		`currentseason` = currentseason+1,
		`currentweek` = '1',
		`livedate` = ''
		WHERE id = $id";
      $this->db->ExecuteSQL($query);
    }
  }
}

?>