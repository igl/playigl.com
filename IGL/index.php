<?php
/*********************
 * IGL Landing Page
 * *******************/
include_once("./includes/functions.php");
session_start();
// Send authorized users to the /home page
if (isLoggedIn()) {
    header('Location: /home');
}
?>
<html>
  <head>
    <title>International Gaming League - Elevate Your Game</title>
    <meta name="description" content="Your destination for Amateur eSports. Divisional leagues, tiered ladders and weekend tournaments. Join today!">
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <link rel="Shortcut Icon" href="/favicon.ico" type="image/x-icon" />
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Headland+One' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
    
    <link type="text/css" href="css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="css/font-awesome.css">
    <link type="text/css" href="css/datetimepicker.css" rel="stylesheet">
    
    <link rel="stylesheet" href="css/stylesheet.css" />
    <link rel="stylesheet" href="css/tablecloth.css" />
    <link rel="stylesheet" href="css/prettify.css" />

    <link rel="stylesheet" href="css/landing.css" />
    <script type="text/javascript">
        var analytics=analytics||[];(function(){var e=["identify","track","trackLink","trackForm","trackClick","trackSubmit","page","pageview","ab","alias","ready","group"],t=function(e){return function(){analytics.push([e].concat(Array.prototype.slice.call(arguments,0)))}};for(var n=0;n<e.length;n++)analytics[e[n]]=t(e[n])})(),analytics.load=function(e){var t=document.createElement("script");t.type="text/javascript",t.async=!0,t.src=("https:"===document.location.protocol?"https://":"http://")+"d2dq2ahtl5zl1z.cloudfront.net/analytics.js/v1/"+e+"/analytics.min.js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(t,n)};
        analytics.load("ia4hmptjr2");
    </script>
  </head>
  <body class="landing">
    <?php include_once('./templates/msg_bar.php'); ?>
    <header>
      <div class="headerwrap clearfix">
          <ul class="menu">
            <!--<li><a href="#">Tour</a></li>-->
            <li><a href="/login">Log In</a></li>
            <li><a href="/register">Sign Up</a></li>
          </ul>
          <a class="logo" href="/">
            <img alt="International Gaming League" src="img/igl_logo_1.png">
          </a>
      </div>
    </header>
    <div class="cta-slider">
      <div class="overlay">
        <div class="container">
          <div class="row">
            <div class="span5"><h2>Your Destination for Amateur eSports</h2></div>
            <div class="span5 offset2"><a href="/register" class="competitor-btn">Elevate Your Game</a></div>
          </div>
          <div class="row">
            <div class="span12 stats">
              <span class="players lead"><span id="num_players" class="bold">0</span> registered players, <span id="num_teams" class="bold">0</span> teams</span>
              <span class="teams lead">across <a href="#games" class="bold"><span id="num_games">0</span> titles</a></span>
            </div>
          </div>
          
          <!-- <div class="row">  // POPULAR MATCHES //
            <div class="span12">
              <h3 class="popular">Popular Matches Right Now</h3>
              <ul class="matches">
                <li><a href="#"><img src="http://placekitten.com/200/200" class="cta-img"></a><a href="#" class="cta-item">EG vs aL</a></li>
                <li><a href="#"><img src="http://placekitten.com/200/200" class="cta-img"></a><a href="#" class="cta-item">EG vs aL</a></li>
                <li><a href="#"><img src="http://placekitten.com/200/200" class="cta-img"></a><a href="#" class="cta-item">EG vs aL</a></li>
                <li><a href="#"><img src="http://placekitten.com/200/200" class="cta-img"></a><a href="#" class="cta-item">EG vs aL</a></li>
                <li class="last"><a href="#"><img src="http://placekitten.com/200/200" class="cta-img"></a><a href="#" class="cta-item">EG vs aL</a></li>
              </ul>
            </div>
          </div> -->
        </div>
      </div>
    </div>
    <div class="cta-elevate">
      <div class="container">
        <div class="row">
          <div class="span12"><h3>eSports for Everyone</h3></div>
        </div>
        <div class="row">
          <div class="span4 cta-item">
            <img class="cta-img" src="img/compete.png">
            <h4>COMPETE</h4>
            Participate in tournaments or play in tiered leagues & ladders against players of your caliber.
          </div>
          <div class="span4 cta-item">
            <img class="cta-img" src="img/spectate.png">
            <h4>SPECTATE</h4>
            Watch your favourite teams & players take on their opponents in real-time broadcasted matches with play-by-play commentating.
          </div>
          <div class="span4 cta-item">
            <img class="cta-img" src="img/improve.png">
            <h4>IMPROVE</h4>
            Play in tournaments & leagues to increase your team's RPI. Challenge teams above you to move up the ladder.
          </div>
        </div>
      </div>
    </div>
   
    <div class="login-area">
      <div class="container">
        <div class="row">
          <div class="span4 left offset1">
            <h4>Log In Now</h4>
            <a href="/login" class="btn btn-large btn-action">Login</a>
          </div>
          <div class="span2"><div class="or">Or</div></div>
          <div class="span4 right">
            <h4>Don't have an account yet?</h4>
            <a href="/register" class="btn btn-large">Register</a>
          </div>
        </div>
      </div>
    </div>
    <a name="games"></a> 
    <div class="games-list container">
        <div class="row">
            <div class="span12"><a name="games"></a><h3>Today's Best Games</h3></div>
        </div>
    </div>

    <div class="teams-list container">
      
    </div>

<footer>
    <div class="container">
      <div class="row">
        <div class="span10">
          <img class="logo" src="img/igl_logo-footer.png">
          <ul class="menu">
            <li><a href="/about">About</a></li>
            <li><a href="/blog">Blog</a></li>
            <li><a href="/premium">Premium</a></li>
            <li><a href="/advertise">Advertise</a></li>
            <li><a href="/developers">Developers</a></li>
            <li><a href="/partners">Partners</a></li>
            <li><a href="/jobs">Jobs</a></li>
            <li><a href="/investors">Investors</a></li>
            <li><a href="/help">Help</a></li>
          </ul>
        </div>
        <div class="span2">
          <a class="what-is" href="#">What is eSports?</a>
        </div>
      </div>
    </div>
</footer>
    <script type="text/javascript" src="/js/mixpanel.js"></script>
    <script type="text/javascript" src="/js/jquery-1.8.2.js"></script>
    <script type="text/javascript" src="/js/jquery-ui.js"></script>
    <script type="text/javascript" src="/js/bootstrap.js"></script>
    <script type="text/javascript" src="/js/md5.js"></script>
    <script type="text/javascript" src="/js/register.js"></script>
    <script type="text/javascript" src="/js/authenticate.js"></script>
    <script type="text/javascript" src="/js/index.js"></script>
    <script type="text/javascript" src="/js/global.js"></script>
    <script type="text/javascript" src="/js/seo.js"></script>
    <script>
        $(document).ready(function() {
            games_list(); teams_list(); initGoogle()
        });
    </script>
  </body>
</html>