<?php

/*
 * @copyright 2012 
 * @author Darryl Allen
 * This file runs once every 24hrs to copy the playerid & Username from the IGL users DB to the IGL stats DB.
 */
header('Cache-Control: no-cache, must-revalidate');
header('Content-type: application/json');
/*
define ("MYSQL_HOST", "stats-dev.cimukvvq20st.us-east-1.rds.amazonaws.com"); // MySQL Hostname
define ("MYSQL_USER", "stats_dev");	// MySQL Username
define ("MYSQL_PASS", "g3tRAg5D");	// MySQL Password
define ("MYSQL_DATABASE", "stats");	// MySQL Database
*/

define ("MYSQL_HOST", "localhost"); // MySQL Hostname
define ("MYSQL_USER", "root");	// MySQL Username
define ("MYSQL_PASS", "");	// MySQL Password
define ("MYSQL_DATABASE", "stats");	// MySQL Database
define ("BASEURL", "http://localhost/"); #Include trailing slash

require('../includes/functions.php');
require('../model/mysql.class.php');
date_default_timezone_set('UTC');

$db = new mysql(MYSQL_DATABASE);

$userlist = json_decode(file_get_contents(BASEURL."api/activeusers.php"),true);

foreach($userlist as $key => $value)
{
    $steamid = convert64to32($value['steamid']);
    $sql = "UPDATE hlstats_playeruniqueids
            INNER JOIN hlstats_players
            ON hlstats_playeruniqueids.playerId = hlstats_players.playerId
            SET 
                    hlstats_players.playerid = '{$value['playerid']}',
                    hlstats_players.username= '{$value['username']}',
                    hlstats_players.country= '{$value['country']}'
            WHERE hlstats_playeruniqueids.uniqueId = '{$steamid}'";
            $db->ExecuteSQL($sql);
}
?>