<?php

ini_set('max_execution_time', 600); //Allow the script to run for 10 mins
include('../includes/config.php');
include('../model/rcon.class.php');
include('../model/mysql.class.php');

$db = new mysql(MYSQL_DATABASE);

/*
 * @copyright 2012 PlayIGL.com
 * This file is run at a 30 minute interval to check, generate & set server passwords for every match
 */

if (isset($_REQUEST['time']) && is_numeric($_REQUEST['time']))
    $time = $_REQUEST['time'];
else
    $time = time();

if (isset($_REQUEST['force']) && is_numeric($_REQUEST['force']) && $_REQUEST['force'] == 1)
    $configured = "";
else
    $configured = "AND `schedule`.`server_configured`= 0";
    
function cleanConfig($settings) {
    $settings = stripslashes($settings);
    $settings = str_replace('\t', ' ', $settings);
    $settings = trim(preg_replace('/[ ]{2,}|[\t]/', ' ', $settings));
    $settings = preg_split('/$\R?^/m', $settings);
    return $settings;
}


//GEt nearest half hour.
$offset = ($time % 1800);
$prev = $time - $offset;
if ($offset > 900) {
    $time = $prev + 1800;
} else {
    $time = $prev;
}

$sql = "SELECT
	`matchservers`.`rcon` AS rcon,
	`matchservers`.`ip` AS server_ip,
	`matchservers`.`port` AS server_port,
	`schedule`.`map` AS map,
	`schedule`.`sv_password`,
	`schedule`.`id` AS match_id,
	`schedule`.`game` AS game_id,
	`match_config`.`settings` AS match_settings,
	`map_config`.`settings` AS map_settings,
        `home`.`tag` AS home_tag,
        `away`.`tag` AS away_tag
FROM
	`matchreserve`
LEFT JOIN `schedule` ON `schedule`.`reservation_id` = `matchreserve`.`id`
LEFT JOIN `matchservers` ON `matchservers`.`id` = `matchreserve`.`server_id`
LEFT JOIN `maplist` ON `maplist`.`map` = `schedule`.`map`
LEFT JOIN `server_configs` map_config ON `map_config`.`id` = `maplist`.`config_id`
LEFT JOIN `ladder` ON `ladder`.`id` = `schedule`.`ladder`
LEFT JOIN `ladder_settings` ON `ladder`.`settings` = `ladder_settings`.`id`
LEFT JOIN `server_configs` match_config ON `match_config`.`id` = `ladder_settings`.`config_id`
LEFT JOIN `team` home ON `home`.`id` = `schedule`.`home`
LEFT JOIN `team` away ON `away`.`id` = `schedule`.`home`
WHERE
	`matchreserve`.`from` = {$time}
        AND `schedule`.`completed` = 0
        {$configured}";

$db->ExecuteSQL($sql);
if ($db->iRecords()) {
    $doc = $db->ArrayResults();
    foreach ($doc as $key => $value) {
        
        //Testing
        /*
          $value['ip'] = '23.23.104.211';
          $value['port'] = '27015';
          $value['map'] = 'de_nuke';
          $value['sv_password']= 'secure';
         */

        $rcon = new Valve_RCON($value['rcon'], $value['server_ip'], $value['server_port'], Valve_RCON::PROTO_SOURCE);
        echo "{$value['server_ip']}:{$value['server_port']}";
            $rcon->connect();
            $auth = $rcon->authenticate();

            if (isset($auth['success']) && false == $auth['success']) {
                echo "Error: {$auth['error']}<br />";
            } else {
                echo "<br/><br/>connected...<br/>";

                //Make sure we dont change configs
                echo $rcon->execute('servercfgfile  ""'); //TF2/CS Specific
                
                
                if (isset($value['match_settings']) && !empty($value['match_settings'])) {
                    echo "<pre>";
                    $match_settings = cleanConfig($value['match_settings']);
                    $match_settings[] = "hostname \"PlayIGL.com Official Match | {$value['home_tag']} vs {$value['away_tag']}\"";
                    foreach ($match_settings as $cvar)
                        echo $rcon->execute($cvar);
                    echo "</pre>";
                } else {
                    echo "No match settings<br>";
                }
                
                if (isset($value['map_settings']) && !empty($value['map_settings'])) {
                    echo "<pre>";
                    $map_settings = cleanConfig($value['map_settings']);
                    foreach ($map_settings as $cvar)
                        echo $rcon->execute($cvar);                    
                    echo "</pre>";
                }else {
                    echo "No map settings<br>";
                }
                
                echo $rcon->execute("sv_password ".$value['sv_password']);

                echo $rcon->execute("changelevel {$value['map']}");
                echo "<br>";

                $rcon->disconnect();
                echo "<hr>";
            }
            $db->Update('schedule', array('server_configured' => 1), array('id' => $value['match_id']));

    }
} else {
    echo "No matches require updating.";
}
?>