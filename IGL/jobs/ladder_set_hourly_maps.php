<?php
/*
 * @copyright 2013 PlayIGL.com
 * @author Darryl Allen
 * This script selects 2 random maps from the assigned map pool. These maps are then saved to 
 * ladder_map_schedule, where he players will have an opportunity to vote for which map they
 * want to play on that hour. The script runs hourly, and sets maps 4 hours into the future
 */

/*
 *
 * SQL Queries required for this update
 * 
 *
 */
/*
include('../includes/config.php');
include('../model/mysql.class.php');
require('../includes/functions.php');

//Setup our classes
$db = new mysql();

//Get all active ladders
$query = "SELECT ladder_mappool, ladder_game_id ,ladder_rounds FROM active_ladders";
if( $db->ExecuteSQL($query) && $db->iRecords() )
{
    $ladders= $db->ArrayResults();
    foreach( $ladders as $i => $l )
    {
        $query = "SELECT poolarray FROM mappools WHERE id = {$l['ladder_mappool']}";
        
        if( $db->ExecuteSQL($query) && $db->iRecords() )
        {
            $num_maps = $l['ladder_rounds'] + 2; //Offer 2 more maps to vote for over the number of rounds required.
            $map_list = $db->ArrayResult();
            $pool_array = rtrim($map_list['poolarray'],',');
            $query = "SELECT map FROM maplist WHERE id IN ({$pool_array}) ORDER BY RAND() LIMIT {$num_maps}";
            if( $db->ExecuteSQL($query) && $db->iRecords() )
            {
                foreach( $db->ArrayResults() as $map )
                $ladders[$i]['ladder_maps'][] = $map['map'];
            }
            
        }
    }
    $time = strtotime(date('Y-m-d H:0:0', strtotime("+4 hours")));

    foreach( $ladders as $l )
    {
        if(is_array($l['ladder_maps']))
        foreach( $l['ladder_maps'] as $map )
        {
            $db->Insert(array('game' => $l['ladder_game_id'],'map' => $map, 'time' => $time), 'ladder_map_schedule' );
        }
    }
}*/