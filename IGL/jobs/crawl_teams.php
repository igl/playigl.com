<?php

//Allow 5 minutes for the script to execute
set_time_limit(1500);

//include DB stuff
include('../includes/config.php');
include('../includes/functions.php');
include('../model/mysql.class.php');

$start = round(microtime(true) * 1000);
$start_seconds = time();

function get_web_page($url) {
    $user_agent = 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)';

    $options = array(
        CURLOPT_CUSTOMREQUEST => "GET", //set request type post or get
        CURLOPT_POST => false, //set to GET
        CURLOPT_USERAGENT => $user_agent, //set user agent
        CURLOPT_COOKIEFILE => "cookie.txt", //set cookie file
        CURLOPT_COOKIEJAR => "cookie.txt", //set cookie jar
        CURLOPT_RETURNTRANSFER => true, // return web page
        CURLOPT_HEADER => false, // don't return headers
        CURLOPT_FOLLOWLOCATION => true, // follow redirects
        CURLOPT_ENCODING => "", // handle all encodings
        CURLOPT_AUTOREFERER => true, // set referer on redirect
        CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
        CURLOPT_TIMEOUT => 120, // timeout on response
        CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
    );

    $ch = curl_init($url);
    curl_setopt_array($ch, $options);
    $content = curl_exec($ch);
    $err = curl_errno($ch);
    $errmsg = curl_error($ch);
    $header = curl_getinfo($ch);
    curl_close($ch);

    $header['errno'] = $err;
    $header['errmsg'] = $errmsg;
    $header['content'] = $content;
    return $header;
}

$db = new MySQL();




if (isset($_GET['division_id']) && is_numeric($_GET['division_id'])) {

    $id = $_GET['division_id'];
    $team_list_url = "http://play.esea.net/index.php?s=league&d=standings&division_id={$id}";

    $r = get_web_page($team_list_url);
    if ($r['http_code'] == 200) {
        $teams = array();
        $parse_teams = explode('<a href="/teams/', $r['content']);
        foreach ($parse_teams as $i => $t) {
            if ($i == 0)
                continue;
            $team_id = explode('"', $t);
            $teams[] = $team_id[0];
        }
    }

    $players = array();
    foreach ($teams as $t) {
        $r = get_web_page("http://play.esea.net/teams/{$t}");
        if ($r['http_code'] == 200) {
            $active_roster = explode('Active Roster', $r['content']);
            if (is_array($active_roster)) {
                $parse_players = explode('<a href="/users/', $active_roster[1]);

                if (is_array($parse_players)) {
                    foreach ($parse_players as $i => $p) {
                        if ($i == 0)
                            continue;
                        $player_id = explode('"', $p);
                        $players[] = $player_id[0];
                    }
                }
            }
        }
    }

    foreach ($players as $p) {
        $url = "http://play.esea.net/users/{$p}?tab=history";

        $r = get_web_page($url);

        if ($r['http_code'] == 200) {
            // Check Game ID
            $get_games = explode('userInfo.gameIds = ', $r['content']);
            if (is_array($get_games)) {
                $get_game = explode(';', $get_games[1]);
                $game_list = str_replace(array('"', ']', '['), "", $get_game[0]);
                $game_ids = explode(",", $game_list);


                $data = explode('<div id="profile-column-right">', $r['content']);
                $get_id = explode(' alt="Team Fortress 2"> 0:', $data[1]);
                if (isset($get_id[1])) {
                    $steam_id = explode('</div>', $get_id[1]);
                    $community_id = convert32to64("0:" . $steam_id[0]);
                    echo $url . " 0:" . $steam_id[0];
                    echo " | ";
                    echo $community_id;
                    echo "<br><br>";
                    $db->Insert(array('user_id' => $p, 'steam_id' => "0:" . $steam_id[0], 'community_id' => $community_id), 'esea');
                }
            }
        }
    }

    $time_seconds = time() - $start_seconds;

    echo "<hr>Took {$time_seconds} seconds to run..";
} else {
    echo "Please specify the division_id parameter.";
}
?>