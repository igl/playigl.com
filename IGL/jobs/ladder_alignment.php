<?php
/*
 * @copyright 2012 PlayIGL.com
 * @author Darryl Allen
 * This file grabs all teams in a ladder, equalizes their points based on weight, then redistributes teams among each ladder
 * 
 * This file runs every 7 days.
 */


include('../includes/config.php');
include('../model/mysql.class.php');
include('../model/align_ladder.class.php');
require('../includes/functions.php');

$align = new alignLadder();

foreach( $align->_getAllLadders() as $ladder )
{
    echo "<br /><br />";  
    echo "################{$ladder['ladder_game_name']} {$ladder['ladder_name']} {$ladder['ladder_format']}################";
    echo "<br /><br />";  
    
    $participating_teams = $align->_getParticipatingTeams($ladder['ladder_settings_id']);
    $num_teams = count($participating_teams);
    //total teams, divided by max teams = number of tiers req'd
    $num_tiers_reqd = $num_teams / $ladder['ladder_maxteams'];
    
    //Get our remainder
    $r = $num_tiers_reqd - floor($num_tiers_reqd); 
    
    //Check if the remaining # of teams is above or below the 25% threshold
    if( $r <= .25 )
        $num_tiers_reqd = floor($num_tiers_reqd); //round down
    else
        $num_tiers_reqd = ceil($num_tiers_reqd); //round up
    
    //make sure we always have 1 tier
    if( $num_tiers_reqd < 1 ) 
        $num_tiers_reqd = 1;
    
    $active_tiers = $align->_getActiveTiers($ladder['ladder_settings_id']);

    $num_active_tiers = count($active_tiers);
    echo "-----------------------------------------------<br />";
    echo "------Adding & Removing Tiers-----<br />";
    echo "-----------------------------------------------<br />";
    
    //remove tiers
    while($num_active_tiers > $num_tiers_reqd) {
        $align->_removeTier($ladder['ladder_settings_id'],$num_tiers_reqd);
        $num_active_tiers--;
    }
    
    //add tiers
    while($num_active_tiers < $num_tiers_reqd) {
        $align->_addTier($ladder['ladder_settings_id']);
        $num_active_tiers++;
    }
    echo "-----------------------------------------------<br />";
    echo "--Done Adding & Removing Tiers--<br />";
    echo "-----------------------------------------------<br />";
    echo "<br /><br />";  
    
    //Set a counter
    $counter = 1;
        
    //run through our active tiers & assign teams
    foreach( $align->_getActiveTiers($ladder['ladder_settings_id']) as $tier) {

        echo "-Assigning teams to {$ladder['ladder_game_name']} {$ladder['ladder_name']} {$ladder['ladder_format']} ladder tier {$counter} [id {$ladder['ladder_id']}] <br />";
        //Monitor the number of teams assigned to a ladder
        $assigned_teams = 0;

        $last = ''; //Set last ladder to false by default
        //assign a set # of teams to a ladder tier
        $team_count = 1;
        foreach( $participating_teams as $i => $t )
        {
            //Assign team to a ladder
            $align->_assignTeam($t['team_id'],$tier['ladder_id'],$ladder['ladder_settings_id']);

            unset($participating_teams[$i]);//remove team from array
            $assigned_teams++;

            echo "--Teamid {$t['id']} assigned ({$assigned_teams} total) <br />";

            //Check if the ladder is at max capacity
            if( $assigned_teams >= $ladder['ladder_maxteams'] )
            {
                //create a new ladder tier, we're at capacity!
                echo "Full...<br />";

                //If it's not the last ladder for a game/format, break the loop.
                if( $counter != $num_tiers_reqd )
                {
                    echo "{$ladder['ladder_game_name']} {$ladder['ladder_name']} {$ladder['ladder_format']} has reached capactity ({$team_count}/{$ladder['ladder_maxteams']}) <br /><br />";
                    break;
                }
                else//this is the last ladder for this game & format
                    $last = "---{$ladder['ladder_game_name']} {$ladder['ladder_name']} {$ladder['ladder_format']} was the last or only ladder. It was filled with the remaining teams....<br />";
            }
            $team_count++;
        }
        echo $last;
        $counter++; //Current ladder iteration   

        echo "---Done! <br /><br />";

    }
    
}