<?php
include_once('../../includes/config.php');
include_once('../../model/mysql.class.php');
include_once('../../includes/functions.php');
include_once('../../includes/forums.php');

$return = array("success" => false, "error" => "Please log in to use this feature.");

if (isLoggedIn()) {
  $forums = new Forums();
  $forums->deleteOnlinePresence();
}

?>