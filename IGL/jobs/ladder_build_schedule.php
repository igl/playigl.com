<?php

/*
 * @copyright 2012 PlayIGL.com
 * @author Steve Rabouin & Darryl Allen
 * This file creates a schedule for teams ready in the upcoming hour
 * Additionally, it will remove the pool for teams that have not been matched.
 * A notification is sent to each team about their upcoming match or lack of.
 */

/*
 *
 * SQL Queries required for this update
 * 
 *
 */
/*
include('../includes/config.php');
include('../model/mysql.class.php');
include('../model/matchservers.class.php');
require('../includes/functions.php');

$db = new mysql(MYSQL_DATABASE);
$match_server = new matchservers();

/*
 * Assuming this scripts run properly and before the hour. It should run a few minutes prior to the hour to ensure
 * teams have enough time to prepare and the servers have enough time to be ready.
 */
/*
if(isset($_REQUEST['hour']) && is_numeric($_REQUEST['hour']))
    $nexthour = $_REQUEST['hour'];
else
    $nexthour = strtotime(date('Y-m-d H:0:0', strtotime("+1 hour")));

$query = "
	SELECT
		`ladder_pool`.`id`,
		`ladder_pool`.`team`,
		`ladder_pool`.`ladder`
	FROM 
		`ladder_pool`
	WHERE
		`ladder_pool`.`time` = '{$nexthour}' AND `ladder_pool`.`scheduled` = 0";

$db->ExecuteSQL($query);

$requests = array();
if ($db->iRecords()) {
    $doc = $db->ArrayResults();

    foreach ($doc as $team) {
        $requests[$team['ladder']][$team['team']] = $team;
    }
}

// Let's cycle through the ladders
foreach ($requests as $ladder => $teams) {
    // get ladder information
    $query = "SELECT * FROM `active_ladders` WHERE `ladder_id` = '{$ladder}'";
    $db->ExecuteSQL($query);

    if ($db->iRecords()) {
        $ladderinfo = $db->ArrayResult();
        echo "Checking ladder: {$ladderinfo['ladder_game_name']} {$ladderinfo['ladder_name']} {$ladderinfo['ladder_format']} ...";

        // find the last matches played by this team, to prevent from playing them again right away.
        // this makes match making more fun, since you're teamed with a team closer to you all the time.

        echo "<br />Finding last matches played for each team playing in the next hour... ";

        foreach ($teams as $teamid => $team) {
            echo ".";
            $query = "SELECT `points`, `continent` FROM `team` WHERE `id` = '{$teamid}' ORDER BY points DESC";
            $db->ExecuteSQL($query);

            if ($db->iRecords()) {
                $doc = $db->ArrayResult();
                if (!$doc['points'])
                    $doc['points'] = 0;
                $teams[$teamid]['points'] = $doc['points'];
                $teams[$teamid]['continent'] = $doc['continent'];
            }

            $query = "SELECT `home`, `away` FROM `schedule` WHERE {$teamid} IN (`away`,`home`) AND completed = 1 ORDER BY id desc LIMIT 2";
            $db->ExecuteSQL($query);

            $teamsplayed = array();
            if ($db->iRecords()) {
                $doc = $db->ArrayResults();
                $teams[$teamid]['teamsplayed'] = array();

                foreach ($doc as $match) {
                    if ($match['away'] != $teamid)
                        $teams[$teamid]['teamsplayed'][$match['away']] = 1;
                    else
                        $teams[$teamid]['teamsplayed'][$match['home']] = 1;
                }
            }
        }

        echo "Done!<br />";

        // let's sort the teams based on their points, this will allow us to match the teams that are the closest.
        /* Not needed as we pull teams from the DB ordered by points
          uasort($teams, function($a, $b) {
          if($a['points'] != $b['points'])
          return ($a['points'] < $b['points'] ? 1 : 0);
          else
          return ($a['id'] < $b['id']);
          }); */
/*
        $schedule = array();

        echo "Scheduling matches ... <br />";

        while (count($teams)) {
            reset($teams);
            $currentteam = current($teams);
            unset($teams[$currentteam['team']]);

            if (count($teams)) {
                foreach ($teams as $teamid => $team) {
                    if (!isset($currentteam['teamsplayed'][$teamid]) && !isset($team['teamsplayed'][$currentteam['team']])) {
                        // so we haven't played this team recently (both sides) -- let's schedule
                        echo "{$currentteam['team']} has not played {$teamid} recently<br />";
                        $schedule[] = array('a' => $currentteam, 'b' => $team);
                        unset($teams[$team['team']]);
                        break;
                    } else {
                        echo "{$currentteam['team']} has played {$teamid} recently<br />";
                    }
                }
            } else {
                echo "Not enough teams to match {$currentteam['team']}.<br />";
            }
        }

        echo "Done scheduling matches.<br />";

        echo "Creating schedules... <br/>";

        //Check which maps go the most votes
        $query = "SELECT map, count(map) as num_votes FROM ladder_pool_votes WHERE game = {$ladderinfo['ladder_game_id']} AND time = {$nexthour} GROUP BY map ORDER BY num_votes DESC LIMIT {$ladderinfo['ladder_rounds']}";

        $db->ExecuteSQL($query);
        $r = $db->ArrayResults();
        $map = "";
        foreach ($r as $m) {
            $map .= $m['map'] . "|";
        }
        $map = trim($map, "|");
        
        if(strlen($map)==0)
        {
            $db->ExecuteSQL("SELECT map FROM ladder_map_schedule WHERE time = {$nexthour}");
            if($db->iRecords())
            {
                $mp = $db->ArrayResult();
                $map = $mp['map'];
            }
        }

        foreach ($schedule as $doc) {
            //Check if IGL hosts the servers
            $q = "SELECT hostedservers FROM game WHERE id = {$ladderinfo['ladder_game_id']} AND hostedservers = 1 ";
            if ($db->ExecuteSQL($q) && $db->iRecords()) {
                //Get server locals for each team
                $locals = array();

                //Get home locations
                $q = "SELECT `primary_region`, `alternate_region` FROM `server_locations` WHERE `continent` = '{$doc['a']['continent']}' LIMIT 1";

                $db->ExecuteSQL($q);
                $l = $db->ArrayResult();
                $alts = explode('|', $l['alternate_region']);
                foreach ($alts as $k => $v)
                    $alts[$k + 1] = $v;
                $alts[0] = $l['primary_region'];
                $locals['home'] = $alts;


                //Get away locations
                $q = "SELECT `primary_region`, `alternate_region` FROM `server_locations` WHERE `continent` = '{$doc['b']['continent']}' LIMIT 1";
                $db->ExecuteSQL($q);
                $l = $db->ArrayResult();
                $alts = explode('|', $l['alternate_region']);
                foreach ($alts as $k => $v)
                    $alts[$k + 1] = $v;
                $alts[0] = $l['primary_region'];
                $locals['away'] = $alts;


                //Here we intersect the team locations to determine which servers are best for both teams
                $locations = array_values(array_intersect($locals['home'], $locals['away']));
                echo "<br/>Requested locations: ";
                print_r($locations);
                echo "<br/>";
                //Get our server data
                $server_data = array();
                $last_location = end(array_keys($locations));
                foreach ($locations as $k => $l) {
                    echo "Checking {$l}...<br/>";
                    $server_data = $match_server->reserve($ladderinfo['ladder_game_id'], 'ladder', $doc['a']['continent'], $l, $nexthour);
                    if ($server_data['success']) {
                        echo "Used {$l}<br/>";
                        $db->Insert(array(
                            'game' => $ladderinfo['ladder_game_id'],
                            'ladder' => $ladder,
                            'home' => $doc['a']['team'],
                            'away' => $doc['b']['team'],
                            'map' => $map,
                            'officialdate' => $nexthour,
                            'reservation_id' => $server_data['reservation_id'],
                            'sv_password' => genServerPass()
                                ), 'schedule');
                        break;
                    }
                    if ($k == $last_location) {
                        unset($server_data);
                        echo "-----Need another {$locations[0]} Server. <br>";
                    }
                }
            } else {
                //We dont need to get server data, so just make the schedule
                $db->Insert(array(
                    'game' => $ladderinfo['ladder_game_id'],
                    'ladder' => $ladder,
                    'home' => $doc['a']['team'],
                    'away' => $doc['b']['team'],
                    'scoring_method' => $ladderinfo['ladder_scoring_method'],
                    'round_details' => $ladderinfo['ladder_round_details'],
                    'map' => $map,
                    'officialdate' => $nexthour
                        ), 'schedule');
            }
        }

        echo "Done!<br />";

        echo "Removing teams from the pool... <br />";
        $db->Update('ladder_pool', array('scheduled' => 1), array('time' => $nexthour));
        echo "Done!<br />";

        echo "Completed schedule for ladder: {$ladderinfo['ladder_game_name']} {$ladderinfo['ladder_name']}.<br />";
    } else {
        echo "{$ladder} does not exist.. Skipping.<br />";
    }
}
*/