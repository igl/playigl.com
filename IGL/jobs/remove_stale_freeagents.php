<?php

/*
 * @copyright 2012 
 * @author Darryl Allen
 * This file runs once every 24hrs to remove free agents who have not been recruited within 14 days.
 * User also have all invites removed
 */

//include globla configs
require('../includes/config.php');
//include mySQL class
require('../model/mysql.class.php');
//Include SendGrid
require('../sendgrid-php/SendGrid_loader.php');

//Set our SendGrid classes
$sendgrid = new SendGrid('playigl', 'xeCr9feJ');
$mail = new SendGrid\Mail();

$db = new MySQL();

$time = time();
$deadline = $time - (60*60*24*14); //add 2 weeks
$date = date('Y-m-d H:0:0',$deadline);

$query = "
        SELECT 
            `freeagents`.`id` as agent_id,
            `freeagents`.`player` as agent_player_id,
            `freeagents`.username as agent_player_username,
            `freeagents`.`game` as agent_player_game_id,
            `game`.`name` as agent_player_game_name,
            `player`.`email` as agent_player_email
        FROM
            `freeagents`
        JOIN
            `player` ON `player`.`playerid` = `freeagents`.`player`
        JOIN
            `game` ON `game`.`id` = `freeagents`.`game`
        WHERE
            `freeagents`.`joindate` < '{$date}'";
            

$db->ExecuteSQL($query);
$agents = $db->ArrayResults();

$recipient_id = array();
$recipient_name = array();
$recipient_email = array();
$game_id = array();
$game_name = array();
$sender_id = array();
$sender_name = array();
$sender_email = array();
$team_id = array();
$team_name = array();

if( $db->iRecords() )
{
    $query = "SELECT `from`, `body`, `subject` FROM `email_templates` WHERE `type` = 'freeagent_prune' LIMIT 1";
    $db->ExecuteSQL($query);
    if($db->iRecords())
    {
        $result         = $db->ArrayResult();
        $message        = $result['body'];
        $subject        = $result['subject'];
        $from           = $result['from'];

    }
    else
    {
        $message        = "You have been removed as a <a href='/freeagents/[game_id]-[game_name]'>[game_name]</a> free agent. To increase your chances of being recruited, try participating in pick-up games. To re-regiser as a freeagent, visit <a href='[baseurl]freeagents/[game_id]-[game_name]'>here</a>.";
        $subject        = "You've been removed as a free agent.";
        $from           = "noreply@playigl.com";
    }
    foreach( $agents as $agent )
    {
        $recipient_id[] = $agent['agent_player_id'];
        $recipient_name[] = $agent['agent_player_username'];
        $recipient_email[] = $agent['agent_player_email'];
        $game_id[] = $agent['agent_player_game_id'];
        $game_name[] = $agent['agent_player_game_name'];
        $baseurl[] = BASEURL;

        $db->Delete('freeagents',array('id' => $agent['agent_id']));
        $db->Delete('inviterequests', array('player' => $agent['agent_player_id'], 'game' => $agent['agent_player_game_id']));
    }
    //Category for SendGrid stats
    $mail->setCategory('Pruned free agents');
    //Array of users to receive email
    $mail->setTos($recipient_email);
    //Set From email
    $mail->setFrom($from);
    //Set from display name
    $mail->setFromName('International Gaming League');
    //set subject
    $mail->setSubject($subject);
    //set HTML message body
    $mail->setHtml($message);
    //arrays of replacemens for the message body
    $mail->addSubstitution('[recipient_id]', $recipient_id);
    $mail->addSubstitution('[recipient_name]', $recipient_name);
    $mail->addSubstitution('[recipient_email]', $recipient_email);
    $mail->addSubstitution('[sender_id]', $sender_id);
    $mail->addSubstitution('[sender_name]', $sender_name);
    $mail->addSubstitution('[sender_email]', $sender_email);
    $mail->addSubstitution('[team_id]', $team_id);
    $mail->addSubstitution('[team_name]', $team_name);
    $mail->addSubstitution('[game_id]', $game_id);
    $mail->addSubstitution('[game_name]', $game_name);
    $mail->addSubstitution('[baseurl]', $baseurl);
    //Send the email!
    $sendgrid->web->send($mail); 
}

 
?>