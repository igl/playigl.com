<?php
include('../includes/config.php');
include('../model/mysql.class.php');
require('../includes/functions.php');

$db = new MySQL(MYSQL_DATABASE);
$db->ExecuteSQL(
  "SELECT id FROM youtube;"
);

$playigl_vods = $db->iRecords() ? $db->ArrayResults() : array();
$youtube = json_decode(file_get_contents('http://gdata.youtube.com/feeds/api/playlists/PLKARvKbbmVQZXiZQLyR1sN3aQq6qtFxJP/?v=2&alt=json&feature=plcp&key=AI39si7of_lX9RB8gDQVkfncCWH5MFAAhNgrtNGl4Npa2vFfkBfzppGyMbB9Wa4ZCZVYifpp3z2S8f6gJm-y3K2W789hpDOQ1w'));
$feed = $youtube->feed->entry;

for($i = 0; $i < count($feed); $i++) {
  $data = array(
    "id" => $feed[$i]->id->{'$t'},
    "title" => $feed[$i]->title->{'$t'},
    "link" => $feed[$i]->link[0]->href,
    "description" => $feed[$i]->{'media$group'}->{'media$description'}->{'$t'},
    "thumbnail" => $feed[$i]->{'media$group'}->{'media$thumbnail'}[3]->url
  );
  if (!in_array($data['id'], $playigl_vods)) {
    $db->Insert($data, "youtube");
  } 
}

?>