<?php
//This file is run at intervals to check Twitch for the ILG stream status & details
header('Cache-Control: no-cache, must-revalidate');
header('Content-type: application/json');
require('../includes/functions.php');
require('../includes/config.php');
require('../model/mysql.class.php');

$db = new MySQL(MYSQL_DATABASE);

$json = json_decode(file_get_contents('https://api.twitch.tv/kraken/streams/igl1'),true);

if( is_array($json['stream']) )
{
    $db->Update('stream', array(
    'live' => 1,
    'game' => $json['stream']['game'],
    'status' => $json['stream']['channel']['status'],
    'url' => $json['stream']['channel']['url'],
    'display_name' => $json['stream']['channel']['display_name'],
    'viewers' => $json['stream']['viewers'],
    'broadcaster' => $json['stream']['broadcaster'],
        ), array()
            );

    if( is_array($json['stream']['channel']['teams']))
    {
        foreach( $json['stream']['channel']['teams'] as $key => $team )
        $db->Update('stream', array(
            "team".$key => $team[$key]
            ), array()
                );

    }
}
else
{
    $db->Update('stream', array(
        'live' => 0,
        'status' => "",
        'team0' => "",
        'team1' => "",
        'team2' => ""
        ), array()
            );
}


?>