<?php

/*
 * @copyright 2012 
 * @author Darryl Allen
 * This file runs after every match submission to update a team's Rating Percentage Index based on the strength of their opponents
 */


//script to be updated to get a teams winning percentage for their last 10 matches, not the overall WP.

header('Cache-Control: no-cache, must-revalidate');
header('Content-type: application/json');

require('../includes/functions.php');
require('../includes/config.php');
require('../model/mysql.class.php');

/*
 * Functions we'll use throughout the process
 */
function getOpponents($teamid)
{
    $db = new mysql();

    $query = "
            SELECT
		DISTINCT(`schedule`.`id`),
		`team_home`.`id` as home_id,
		`team_home`.`pct` as home_pct,
		`team_away`.`id` as away_id,
		`team_away`.`pct` as away_pct,
                IFNULL((`league`.`weight`),1) as weight
            FROM
                `schedule`
            LEFT JOIN
                `league` ON `league`.`id` = `schedule`.`league`
            LEFT JOIN 
                `team` as `team_home` ON `schedule`.`home` = `team_home`.`id`
            LEFT JOIN 
                `team` as `team_away` ON `schedule`.`away` = `team_away`.`id`
            WHERE
                {$teamid} IN (`schedule`.`home`, `schedule`.`away`) AND completed = 1

            ORDER BY `schedule`.`officialdate` DESC LIMIT 10";//Limit to last 10 teams you played, and last 10 teams each opponent played

                
    $db->ExecuteSQL($query);
    $opponents = $db->ArrayResults();
    $return = array();
    foreach( $opponents as $team )
    {
        if( $team['home_id'] == $teamid )
        {
            $return[] = array(
                'team_id' => $team['away_id'],
                'weight' => $team['weight'],
                'WP' => ($team['away_pct'] * $team['weight']),
                'trueWP' => $team['away_pct'],
                'OOWP' => 0);
        }
        elseif ( $team['away_id'] == $teamid )
        {
            $return[] = array(
                'team_id' => $team['home_id'],
                'weight' => $team['weight'],
                'WP' => ($team['home_pct'] * $team['weight']),
                'trueWP' => $team['home_pct'],
                'OOWP' => 0);
        } 
    }
    return $return;
}


function calcWP($team)
{
    $db = new mysql();
    $query = "SELECT ((SELECT count(id) FROM schedule WHERE {$team} IN (home,away) AND winner = {$team})/(SELECT count(id) FROM schedule WHERE {$team} IN (home,away) AND completed = 1)) as pct FROM schedule";
    $db->ExecuteSQL($query);
    $result = $db->ArrayResult();
    //Team's winning PCT
    return $result['pct']; // This is a team's winning %
}

/*
 * Get a list of all active teams
 */

$query = "SELECT id FROM team WHERE status = 1 AND deleted = 0";
$db = new MySQL();
$db->ExecuteSQL($query);
$teams = $db->ArrayResults();

/*
 * Filter through each team to calculate each RPI;
 */
foreach ( $teams as $team )
{
    $team_id = $team['id'];
    //Set Parent Team to get RPI for
    $opponents['team_id'] = $team_id;
    $opponents['WP'] = calcWP($team_id);
    $opponents['OWP'] = 0;
    $opponents['OOWP'] = 0;
    $opponents['opponents'] = getOpponents($team_id);


    foreach ( $opponents['opponents'] as $key => $team ) //Get Opponents' WP
    {
        $opponents['opponents'][$key]['opponents'] = getOpponents($team['team_id']);
    }

    foreach($opponents['opponents'] as $key => $array)// Scan the array to remove OPOP's if equal to parent team
    {
        $opponents['OWP'] = $opponents['OWP'] + $array['WP'];
        foreach($array['opponents'] as $subKey => $value)
        {
            if( $value['team_id'] == $team_id )
            {
                unset($opponents['opponents'][$key]['opponents'][$subKey]);
            }
            else
            {
                $opponents['opponents'][$key]['OOWP'] = $opponents['opponents'][$key]['OOWP'] + $value['WP']; //Update individual nested OOWP
            }
            if($subKey == count($array['opponents'])-1)
            {
                $opponents['opponents'][$key]['OOWP'] = 0;
                $num_opponents = count($opponents['opponents'][$key]['opponents']);
                if($num_opponents>0)
                    $opponents['opponents'][$key]['OOWP'] = $opponents['opponents'][$key]['OOWP']/$num_opponents;
            }
        }
        $opponents['OOWP'] = $opponents['OOWP'] + $opponents['opponents'][$key]['OOWP'];//Update parent total average OOWP
        if($key == count($opponents['opponents'])-1)
        {
            $num_opponents = count($opponents['opponents']);
            $opponents['OWP'] = $opponents['OWP']/$num_opponents;
            $opponents['OOWP'] = $opponents['OOWP']/$num_opponents;
        }
    }

    print_r($opponents);

    //RPI = (WP * 0.25) + (OWP * 0.50) + (OOWP * 0.25)
    $RPI = round((( $opponents['WP'] * .25 ) + ( $opponents['OWP'] * .50 ) + ( $opponents['OOWP'] * .25 )),3);
    $db->Update('team', array('RPI' => $RPI), array('id' => $team_id));
    #echo "{$RPI}\n";
}
?>