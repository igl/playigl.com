<?php
//Allow 5 minutes for the script to execute
set_time_limit(1500);

//include DB stuff
include('../includes/config.php');
include('../includes/functions.php');
include('../model/mysql.class.php');

$start = round(microtime(true) * 1000);
$start_seconds = time();

function get_web_page($url) {
    $user_agent = 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)';

    $options = array(
        CURLOPT_CUSTOMREQUEST => "GET", //set request type post or get
        CURLOPT_POST => false, //set to GET
        CURLOPT_USERAGENT => $user_agent, //set user agent
        CURLOPT_COOKIEFILE => "cookie.txt", //set cookie file
        CURLOPT_COOKIEJAR => "cookie.txt", //set cookie jar
        CURLOPT_RETURNTRANSFER => true, // return web page
        CURLOPT_HEADER => false, // don't return headers
        CURLOPT_FOLLOWLOCATION => true, // follow redirects
        CURLOPT_ENCODING => "", // handle all encodings
        CURLOPT_AUTOREFERER => true, // set referer on redirect
        CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
        CURLOPT_TIMEOUT => 120, // timeout on response
        CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
    );

    $ch = curl_init($url);
    curl_setopt_array($ch, $options);
    $content = curl_exec($ch);
    $err = curl_errno($ch);
    $errmsg = curl_error($ch);
    $header = curl_getinfo($ch);
    curl_close($ch);

    $header['errno'] = $err;
    $header['errmsg'] = $errmsg;
    $header['content'] = $content;
    return $header;
}


$db = new MySQL();

//Set defualt user
$user = 1;

//Check where we left off...
$q = "SELECT max(user_id) as user_id FROM esea";
$db->ExecuteSQL($q);
if($db->iRecords())
{
    $r = $db->ArrayResult();
    $user = $r['user_id'] +1;
}

while ($user <= 584480) {

    $url = "http://play.esea.net/users/{$user}?tab=history";

    $r = get_web_page($url);

    if ($r['http_code'] == 200) {
        // Check Game ID
        $get_games = explode('userInfo.gameIds = ', $r['content']);
        $get_game = explode(';', $get_games[1]);
        $game_list = str_replace(array('"', ']', '['), "", $get_game[0]);
        $game_ids = explode(",", $game_list);

        if (in_array(43, $game_ids)) {
            $data = explode('<div id="profile-column-right">', $r['content']);
            $get_id = explode(' alt="Team Fortress 2"> 0:', $data[1]);
            if (isset($get_id[1])) {
                $steam_id = explode('</div>', $get_id[1]);
                $community_id = convert32to64("0:".$steam_id[0]);
                echo $url. " 0:" . $steam_id[0];
                echo " | ";
                echo $community_id;
                echo "<br><br>";
                $db->Insert(array('user_id' => $user, 'steam_id' => "0:".$steam_id[0],'community_id' => $community_id), 'esea');
            }
        }
    }
    $user++;
}

$time_seconds = time() - $start_seconds;

echo "<hr>Took {$time_seconds} seconds to run..";
?>