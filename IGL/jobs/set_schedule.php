<?php
ini_set ('max_execution_time', 600); //Allow the script to run for 10 mins
include('../includes/config.php');
include('../model/rcon.class.php');
include('../model/mysql.class.php');
require('../includes/functions.php');

$db = new mysql(MYSQL_DATABASE);

/*
 * @copyright 2012 PlayIGL.com
 * This file is run at a 1 hour intervals to check which matches are within 48hrs of starting and do not have an official date set.
 * The official date is then set to the scheduled date.
 */


$due_date = time() + (48*3600); //We need the time 48hrs from now.

$sql = "SELECT id, scheduleddate FROM schedule WHERE officialdate = '0' AND scheduleddate < $due_date AND completed = 0";


$db->ExecuteSQL($sql);
if($db->iRecords())
{
    $doc = $db->ArrayResults();
    foreach($doc as $key => $value)
    {
        $db->Update('schedule', array('officialdate' => $value['scheduleddate']), array('id' => $value['id']));
    }
    $return = array('success' => true, 'message' => 'One or more matches were updated.');
}
else
{
    $return = array('success' => false, 'message' => 'No matches require updates.');
}
prettyJSON(json_encode($return));
?>