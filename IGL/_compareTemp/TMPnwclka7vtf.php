<?PHP
class schedule{

#gets matches for player or team ID provided
function todaysmatches($id,$type="player")
	{
	if($type=="player")
		{
		$player = new player();
		$teams = $player->teams($id);
		$team = array();
		foreach($teams as $key => $value)
			{
			$case .= "home = '".$value['team']."' OR away = '".$value['team']."' OR ";
			array_push($team,$value['team']);
			}
		$case = substr_replace($case ,"",-3);
		if($case==''){$case=0;}
		}
		else
		{
		$case = "team = '$id'";
		$team = array($value['team']);
		}
	$query = "SELECT * FROM schedule WHERE ".$case." AND completed = 0 AND officialdate LIKE '".date('Y-m-d',time())."' ORDER BY officialdate ASC";
	$MySQL = new MySQL();
	$MySQL->ExecuteSQL($query);
	$results = $MySQL->ArrayResults;
	#compile output
	if(count($results)==0)
		{
		return "You do not have any matches today.";
		}
		else
		{
		$getmatchdate = explode('-',$results[0]['officialdate']);
		$getmatchtime = explode(':',$getmatchdate[2]);
		$year = $getmatchdate[0];
		$month = $getmatchdate[1];
		$dayhour = str_replace(' ',',',$getmatchtime[0]);
		$minute = $getmatchtime[1];
		?>
		<script type="text/javascript">
		$(function () {
			var nextMatch = new Date();
			nextMatch = new Date(<? echo $year; ?>,<? echo $month-1; ?>,12,<? echo $dayhour; ?>,<? echo $minute; ?>,0);
			$('#nextmatch').countdown({until: nextMatch, format: 'HMS'});
			
		});
		</script>
		<?

		$game = new game();
		$league = new league();
		$team = new team();
		
		
				$opponent = $nextmatch['home'];
		if(!$player->onTeam($_SESSION['playerid'],$nextmatch['away']))
			{
			$opponent = $nextmatch['away'];
			}
		$nextmatch = $results[0];
		$return .= '<div id="row_matchdata">';
		$return .= '<div id="column_matchdata">Time</div>';
		$return .= '<div id="column_matchdata">'.date('H:i T',strtotime($results[0]['officialdate'])).'</div>';
		$return .= '</div>';

		

		
		
			$return .= '<div id="row_matchdata">';
			$return .= '<div id="column_matchdata">Time</div>';
			$return .= '<div id="column_matchdata_game">Game</div>';
			$return .= '<div id="column_matchdata">League</div>';
			$return .= '<div id="column_matchdata_map">Map</div>';
			$return .= '<div id="column_matchdata">Opponent</div>';
			$return .= '</div>';
		$return .= '<hr id="mymatches_hr">';
		foreach($results as $match)
			{
			if($isFirst)
				{
				$isFirst = false;
				continue;
				}

			$return .= '<div id="row_matchdata">';
			if($match['officialdate']=='0000-00-00 00:00:00')
				{
				$time=date('H:i T',strtotime($match['scheduleddate']));
				}
				else
				{
				$time=date('H:i T',strtotime($match['officialdate']));
				}
			$opponent = $match['home'];
			if(!$player->onTeam($_SESSION['playerid'],$match['away']))
				{
				$opponent = $match['away'];
				}
			$return .= '<div id="column_matchdata"><a href="#">'.$time.'</a></div>';
			$return .= '<div id="column_matchdata_game">'.$game->name($league->game($match['league'])).'</div>';
			$return .= '<div id="column_matchdata">'.$league->name($match['league']).'</div>';
			$return .= '<div id="column_matchdata_map">'.$match['map'].'</div>';
			$return .= '<div id="column_matchdata">'.$team->name($opponent,true);
			$return .= ' ('.$team->wins($match['home'],$match['league'],$match['season']).'-'.$team->loses($opponent,$match['league'],$match['season']).')</div>';
			$return .= '<div id="column_matchdata_last"><a href="#">More</a> | <a href="">Submit Score</a> | Dispute</div>';
			$return .= '</div>
			';
			}	
		}
return $return;	



	}

#gets matches for player or team ID provided
function previousmatches($id,$type="player",$limit=5)
	{
	if($type=="player")
		{
		$player = new player();
		$teams = $player->teams($id);
		$team = array();
		foreach($teams as $key => $value)
			{
			$case .= "home = '".$value['team']."' OR away = '".$value['team']."' OR ";
			array_push($team,$value['team']);
			}
		$case = substr_replace($case ,"",-3);
		if($case=='')
			{
			$case=0;
			}
		}
		else
		{
		$case = "team = '$id'";
		$team = array($value['team']);
		}
	$query = "SELECT * FROM schedule WHERE ".$case." AND completed = 1 LIMIT ".$limit;
	$MySQL = new MySQL();
	$MySQL->ExecuteSQL($query);
	$results = $MySQL->ArrayResults;
	
	#compile output
	if(count($results)==0)
		{
		return "You have not yet completed any matches.";
		}
		else
		{
		$game = new game();
		$league = new league();
		$team = new team();
			$return .= '<div id="row_matchdata">';
			$return .= '<div id="column_matchdata">Date</div>';
			$return .= '<div id="column_matchdata">Time</div>';
			$return .= '<div id="column_matchdata_game">Game</div>';
			$return .= '<div id="column_matchdata">League</div>';
			$return .= '<div id="column_matchdata_map">Map</div>';
			$return .= '<div id="column_matchdata">Opponent</div>';
			$return .= '</div>';
		$return .= '<hr id="mymatches_hr">';
		foreach($results as $match)
			{
			$return .= '<div id="row_matchdata">';
			if($match['officialdate']=='0000-00-00 00:00:00')
				{
				$date=date("M j, Y",strtotime($match['scheduleddate']));
				$time=date('H:i T',strtotime($match['scheduleddate']));
				}
				else
				{
				$date=date("M j, Y",strtotime($match['officialdate']));
				$time=date('H:i T',strtotime($match['officialdate']));
				}
			$opponent = $match['home'];
			if(!$player->onTeam($_SESSION['playerid'],$match['away']))
				{
				$opponent = $match['away'];
				}
			$return .= '<div id="column_matchdata">'.$date.'</div>';
			$return .= '<div id="column_matchdata">'.$time.'</div>';
			$return .= '<div id="column_matchdata_game">'.$game->name($league->game($match['league'])).'</div>';
			$return .= '<div id="column_matchdata">'.$league->name($match['league']).'</div>';
			$return .= '<div id="column_matchdata_map">'.$match['map'].'</div>';
			$return .= '<div id="column_matchdata">'.$team->name($opponent,true);
			$return .= ' ('.$team->wins($match['home'],$match['league'],$match['season']).'-'.$team->loses($opponent,$match['league'],$match['season']).')</div>';
			$return .= '<div id="column_matchdata_last"><a href="#">More...</a></div>';
			$return .= '</div>
			';
			}	
		}
return $return;	

	}

#gets matches for player or team ID provided
function nextmatches($id,$type="player",$chat=false)
	{
	if($type=="player")
		{
		$player = new player();
		$teams = $player->teams($id);
		$team = array();
		foreach($teams as $key => $value)
			{
			$case .= "home = '".$value['team']."' OR away = '".$value['team']."' OR ";
			array_push($team,$value['team']);
			}
		$case = substr_replace($case ,"",-3);
		if($case==''){$case=0;}
			}
			else
			{
			$case = "team = '$id'";
			$team = array($value['team']);
			}
	$query = "SELECT * FROM schedule WHERE ".$case." AND completed = 0 ORDER BY officialdate, scheduleddate ASC";
	$MySQL = new MySQL();
	$MySQL->ExecuteSQL($query);
	$results = $MySQL->ArrayResults;
	
	#compile output
	if(count($results)==0)
		{
		return "You do not have any future matches scheduled.";
		}
		else
		{
		$game = new game();
		$league = new league();
		$team = new team();
		$return .= '<div id="row_matchdata">';
		$return .= '<div id="column_matchdata">Date</div>';
		$return .= '<div id="column_matchdata">Time</div>';
		$return .= '<div id="column_matchdata_game">Game</div>';
		$return .= '<div id="column_matchdata">League</div>';
		$return .= '<div id="column_matchdata_map">Map</div>';
		$return .= '<div id="column_matchdata">Opponent</div>';
		$return .= '</div>';
		$return .= '<hr id="mymatches_hr">';

		foreach($results as $match)
			{
			$return .= '<div id="row_matchdata">';
			if($match['officialdate']=='0000-00-00 00:00:00')
				{
				$date=date("M j, Y",strtotime($match['scheduleddate']));
				$time=date('H:i T',strtotime($match['scheduleddate']));
				}
				else
				{
				$date=date("M j, Y",strtotime($match['officialdate']));
				$time=date('H:i T',strtotime($match['officialdate']));
				}
			$opponent = $match['home'];
			if(!$player->onTeam($_SESSION['playerid'],$match['away']))
				{
				$opponent = $match['away'];
				}
			$return .= '<div id="column_matchdata">'.$date.'</div>';
			$return .= '<div id="column_matchdata">'.$time.'</div>';
			$return .= '<div id="column_matchdata_game">'.$game->name($league->game($match['league'])).'</div>';
			$return .= '<div id="column_matchdata">'.$league->name($match['league']).'</div>';
			$return .= '<div id="column_matchdata_map">'.$match['map'].'</div>';
			$return .= '<div id="column_matchdata">'.$team->name($opponent,true);
			$return .= ' ('.$team->wins($match['home'],$match['league'],$match['season']).'-'.$team->loses($opponent,$match['league'],$match['season']).')</div>';
			if($chat)
				{
				$return .= '<div id="column_matchdata_last"><a href="?p=mymatches&do=matchcommunication&matchid='.$match['id'].'">Chat</a></div>';
				}
				else
				{
				$return .= '<div id="column_matchdata_last"><a href="#">More...</a></div>';
				}
			$return .= '</div>
			';
			}	
		}
return $return;	
	}

function advanceWeek($league)
	{
	$MySQL = new MySQL();
	$query = "UPDATE league SET currentweek=currentweek+1 WHERE id = $league";
	return $MySQL->ExecuteSQL($query);	
	}

function matchdate($scheduleid)
	{
	$MySQL = new MySQL();
	$query = "SELECT scheduleddate, officialdate FROM schedule WHERE id = $scheduleid";
	$MySQL->ExecuteSQL($query);	
	if($MySQL->iRecords==0)
		{
		return "Date Missing";
		}
	$results = $MySQL->ArrayResult;
	if($results['officialdate']=="0000-00-00 00:00:00")
		{
		return $results['scheduleddate'];
		}
	return $results['officialdate'];
	}

function insert($week,$league,$season,$home,$away,$map,$scheduleddate,$matchtype)
	{
	$MySQL = new MySQL();
	$query = "INSERT INTO schedule 
	(`id`,`week`,`league`,`season`,`home`,`away`,`map`,`scheduleddate`,`suggesteddate`,`officialdate`,`matchtype`) 
	VALUES
	('', '".$week."', '".$league."', '".$season."', '".$home."', '".$away."', '".$map."', '".$scheduleddate."', '', '', '".$matchtype."')";
	return $MySQL->ExecuteSQL($query);	
	}

function getweeks($leagueid)
	{
	$league = new league();
	$MySQL = new MySQL();
	$query = "SELECT DISTINCT week FROM schedule WHERE league = '".$leagueid."' AND season = '".$league->currentseason($leagueid)."' ";
	$MySQL->ExecuteSQL($query);
	if($MySQL->records > 0)
		{
		$result = $MySQL->ArrayResults;
		foreach($result as $value)
			{
			$return[] = array ($value['week'], $value['week']);
			}
		}
		else
		{
		$return[] = array (0,"No Entries");
		}
	return $return;
		
	}

function generate($id)
	{
	$MySQL = new MySQL();
	$leagueid = serialize($id);
	$query = "SELECT team FROM league_association WHERE league = $id";
	$MySQL->ExecuteSQL($query);
	$numteams = $MySQL->iRecords;
	$count = 0;

	#League details
	$league = new league();
	$season = $league->currentseason($id);
	$week = $league->currentweek($id);
	$day = $league->day($id);
	$map = $league->mapoftheweek($week,$id);
	$time = $league->matchtime($id);
	
	#Get even number of teams
	$limit = floor($numteams/2) * 2;

	#If odd number of teams. do not include team which has played the most matches
	if( $odd = $numteams%2 )
		{
		#seletc most common team from schedule whee league = league & season = season
		$query = "SELECT count(home),count(away) as team FROM schedule WHERE league = $id AND season = ".$season;
		#echo $query;
		}


	$query = "SELECT DISTINCT team FROM league_association WHERE league = $id ORDER BY RAND()";
	$MySQL->ExecuteSQL($query);
	$result = $MySQL->ArrayResults;
	foreach($result as $value)
		{
		$results[] = $value['team'];
		}
	$matchups = array();
	while($limit > $count)
		{
		$home = $results[$count];
		$count++;
		$away = $results[$count];
		$count++;
		array_push($matchups, array("home" => $home, "away" => $away));
		}
	$schedule = array("season" => $season, "week" => $week, "day" => $day, "time" => $time, "map" => $map, "matches" => $matchups);
	
	return $schedule;
    }

}

?>