<?php

/**
 * This script will save a team if the player is captain or alternate
 * @author Steve Rabouin
 * @copyright Copyright 2012 PlayIGL.com
 */

header('Cache-Control: no-cache, must-revalidate');
header('Content-type: application/json');

require('../../includes/config.php');
require('../../includes/functions.php');
require('../../model/mysql.class.php');

$db = new mysql(MYSQL_DATABASE);
$return = array("success" => false, "msg" => "You do not have permission to do that.");

if (isLoggedIn()) {
  if ($_REQUEST['action'] == 'add') {
    $data = array(
      "game_id" => (int) $_REQUEST['game_id'],
      "category" => $_REQUEST['role']
    );
    $b = $db->Insert($data, 'game_character_category');
    if ($b) {
      $return = array("success" => true, "msg" => $data['category']);
    }
  } elseif ($_REQUEST['action'] == 'remove') {
    $data = array(
      "game_id" => (int) $_REQUEST['game_id'],
      "id" => $_REQUEST['id']
    );
    $b = $db->Delete('game_character_category', $data);
    if ($b) {
      $return = array("success" => true);
    }
  }
}

echo prettyJSON(json_encode($return));
?>