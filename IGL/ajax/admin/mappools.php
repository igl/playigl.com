<?php

/**
 * This script will save a team if the player is captain or alternate
 * @author Steve Rabouin
 * @copyright Copyright 2012 PlayIGL.com
 */

header('Cache-Control: no-cache, must-revalidate');
header('Content-type: application/json');

require('../../includes/config.php');
require('../../includes/functions.php');
require('../../model/mysql.class.php');

$return = array();

$db = new mysql(MYSQL_DATABASE);

if ($_REQUEST['action'] == 'savePool' && isset($_REQUEST['game'])) {
       $db->Insert(array('game'=> $_REQUEST['game'],'name'=>$_REQUEST['name'], 'poolarray' => $_REQUEST['mappool']),'mappools');
       $return['success'] = true; 
       $return['error'] = $db->sLastError; 
}

if ($_REQUEST['action'] == 'updatePool' && isset($_REQUEST['id'])) {
       $db->Update('mappools', array('name'=>$_REQUEST['name'], 'poolarray' => $_REQUEST['mappool']),array('id' => $_REQUEST['id']));
       $return['success'] = true; 
       $return['error'] = $db->sLastError; 
}

if ($_REQUEST['action'] == 'deletePool' && isset($_REQUEST['id'])) {
       $db->Delete(array('id'=> $_REQUEST['id']),'mappools');
       $return['success'] = true; 
       $return['error'] = $db->sLastError; 
}

echo prettyJSON(json_encode($return));