<?php
/**
 * Registration Script for Public Website
 * @author Kevin Langlois*
 * @copyright Copyright 2012 PlayIGL.com
 */
header('Cache-Control: no-cache, must-revalidate');
header('Content-type: application/json');

require('../../includes/config.php');
require('../../includes/functions.php');
require('../../model/mysql.class.php');


$return = array();

$db = new mysql(MYSQL_DATABASE);

if (isset($_REQUEST['action'])) {
  $action = $_REQUEST['action'];
  /*   * **********************************
   * Check Username Availability
   * ******************************** */
  if ($action == "checkname") {
    if (isset($_GET['username'])) {
      $query = "SELECT `username` FROM `" . MYSQL_DATABASE . "`.`player` WHERE `username` = '" . $_GET['username'] . "'";
      $db->ExecuteSQL($query);
      if ($db->iRecords()) {
        $data = $db->ArrayResult();
        $return['available'] = false;
      } else {
        $return['available'] = true;
      }
      $return['success'] = true;
    } else {
      $return['success'] = false;
      $return['error'] = "No username provided";
    }
  }
  /*   * **********************************
   * Check Information
   * ******************************** */
  if ($action == "checkinfo") {
    if (!empty($_GET['register-username']) && !empty($_GET['register-password']) && !empty($_GET['register-email'])) {
      $query = "SELECT `username` FROM `" . MYSQL_DATABASE . "`.`player` WHERE `username` = '" . $_GET['register-username'] . "'";
      $db->ExecuteSQL($query);
      if ($db->iRecords()) {
        $data = $db->ArrayResult();
        $return['success'] = false;
        $return['error'] = 'Username already in use.';
      } else {
        if (!empty($_GET['register-steam']) && $_GET['register-steam'] != "") {
          $query = "SELECT `username` FROM `" . MYSQL_DATABASE . "`.`player` WHERE `communityid` = '" . $_GET['register-steam'] . "'";
          $db->ExecuteSQL($query);
          if ($db->iRecords()) {
            $return['success'] = false;
            $return['error'] = "Steam ID in use.";
          } else {
            $return['success'] = true;
          }
        } else {
          $return['success'] = true;
        }
      }
    } else {
      $return['success'] = false;
      $return['error'] = "Empty fields.";
    }
  }
  /*   * **********************************
   * Add user to DB
   * ******************************** */
  if ($action == "register") {
    $salt = rand(1000, 99999);
    $_GET['usergroupid'] = 3;
    $_GET['password'] = md5(md5($_GET['password']) . $salt);
    $_GET['salt'] = $salt;
    $_GET['joindate'] = time();
    $_GET['lastactivity'] = time();
    $_GET['lastvisit'] = time();
    if ($db->Insert($_GET, 'player', array('action', 'playerid'))) { // UserGroupId 3 = awaiting email confirmation
      $return['email'] = $_GET['email'];
      $return['username'] = $_GET['username'];
      $return['user_id'] = $db->lastId();
      $db->Update('referral', array('activated' => 1), array('referee' => $_GET['email']));
      $return['success'] = true;
      // Update initial Pusher channel
      $db->Update('player', array('channels', $return['username'] . '-' . $return['user_id']), array('playerid', $return['user_id']));
      // Send MailChimp COnfirmation Email
      include('../../model/MCAPI.class.php');
      $api = new MCAPI('5954afbfca7e52346df6f4b2b743f4d6-us6');
      $retval = $api->listSubscribe('65d1b46936', $_GET['email']);

      //* Execute Team Invites (if applicable)
      $db->ExecuteSQL(
        "SELECT * FROM team_email_invites WHERE email = '{$_GET['email']}' LIMIT 1"
      );
      if ($db->iRecords()) {
        $team_id = $db->ArrayResult();
        $db->ExecuteSQL(
          "SELECT id as team_id, league as team_league, game as game_id FROM team WHERE id = {$team_id['team_id']}"
        );

        $team_data = $db->ArrayResult();
        $team_data['email'] = $_GET['email'];
        $team_data['player'] = $return['user_id'];
        $team_details = json_decode(json_encode($team_data), FALSE);
        
        include('../../model/backbone.class.php');
        $backbone = new Backbone();
        $backbone->player_id = -1;
        $backbone->saveData("accept team invite", "dashboard", "teams", $team_details);
      }
    } else {
      var_dump("error");
      $return['success'] = false;
      $return['error'] = $db->sLastError;
    }
  }
} else {
  $return['success'] = false;
  $return['error'] = "No Action Provided";
}

echo prettyJSON(json_encode($return));