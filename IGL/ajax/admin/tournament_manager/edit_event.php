<?php 
    require('../../../includes/config.php');
    require('../../../includes/functions.php');
    require('../../../model/mysql.class.php');
    require('../../../model/tournament.class.php');
    require('../../../model/player.class.php');
    require('../../../model/activity.class.php');
    require('../../../model/history.class.php');
    require('../../../model/image.class.php');
    require('../../../model/league.class.php');
    require('../../../model/game.class.php');
    require('../../../model/team.class.php');
    $db = new MySQL();
    $game = new game();
    $games = $game->all();
    $tournaments = new tournament();
    $tournament = $tournaments->get($_POST['id']);   
?>

<script>
    var tournament = <?php echo json_encode($tournament); ?>;
    function edit_eventCallback() {
        for(var item in tournament) {
            $('input[name="'+item+'"][type!="radio"]').val(tournament[item]);
            $('select[name="'+item+'"]').val(tournament[item]);
            $('textarea[name="'+item+'"]').val(tournament[item]);
        }
        $('input[type="checkbox"][value="1"]').attr('checked', 'checked');
        $('input[type="radio"][name="wait_list_type"][value="'+tournament['wait_list_type']+'"]').attr('checked', 'checked');
        $('.dateTimePicker').each(function(i, v) {
                $(this).find('input').val(PlayIGL.formatTimestamp($(v).find('input').val(), true));
       }); 
       $('input[name="offset"]').val(PlayIGL.getLocalTimezoneOffset());
       $('textarea[name="rules"]').redactor();
       $('textarea[name="details"]').redactor();
    }
</script>
<style>
    input[type="text"] { height:20px !important; }
    select {height: 30px !important; margin-top:0px !important;}
    input[type="checkbox"] { position:relative; top:-3px; }
    fieldset { margin-bottom:10px; }
    legend { background:#ddd; padding:0px; text-indent:10px; position:relative; display:inline-block; left:-10px; }
</style>

<form id='newEventForm' style='padding:0px 10px;'>
    <input type="hidden" name="id" value="" />
    <input type="hidden" name="offset" value="" />
    <!-- Tournament Manager New Event Row -->
  <fieldset style='float:left; width:50%;'>
    <legend>General Information</legend>
    <label>Tournament Title</label>
    <input name='name' type="text" placeholder="">
  </fieldset>
  <fieldset style='float:right; width:50%;'>
    <legend>&nbsp;</legend>
    <label>Game</label>
    <select name='game'>
        <?php
            foreach($games as $g) {
                echo '<option value="'.$g['id'].'">'.$g['name'].'</option>';
            }
        ?>
    </select>
  </fieldset>
  <div style='clear:both; width:100%;'></div>
  <!-- Tournament Manager New Event Row -->
  <fieldset style='float:left; width:50%;'>
    <legend>Basic Settings</legend>
    <label>Registration Restrictions</label>
    <select name="registration_type"><option value='0'>Open</option><option value='1'>Division</option><option value='2'>Manual</option></select>
    <label>Format</label>
    <select name="format"><option value='0'>Single Elimination</option><option value='1'>Double Elimination</option><option value='3'>Custom (No schedule/brackets)</option></select>
    <label>Bracket Size</label>
    <input type="text" name="bracket_size" />
  </fieldset>
  <fieldset style='float:right; width:50%;'>
    <legend>Advanced Settings</legend>
    <label>Map Pool</label>
    <select name="map_pool"><option value="0">None</option></select>
    <label>Scoring Method</label>
    <select name="scoring_method"><option value='0'>Round Scores</option><option value='1'>Total</option></select>
    <label>Published</label>
    <select name="is_published"><option value="0">No</option><option value="1">Yes</option></select>
  </fieldset>
  <div style='clear:both; width:100%;'></div>
  <!-- Tournament Manager New Event Row -->
  <fieldset style='float:left; width:50%;'>
    <legend>Wait List &AMP; Check In Settings</legend>
    <label style='display:inline-block; margin-bottom:15px;'><input name='enable_check_in' type="checkbox" value="1"> Enable Check In</label>
    <label style='display:inline-block; margin-bottom:15px; margin-left:10px;'><input name='enable_wait_list' type="checkbox" value="1"> Enable Wait List</label>
    <label>Wait List Size</label>
    <input name='wait_list_size' type="text" placeholder="">
    <label>Wait List Opens</label>
    <label style='display:inline-block;'><input name='wait_list_opens' style='width:20px;' type="text" placeholder=""> minutes before check in closes.</label>
    </fieldset>
  <fieldset style='float:right; width:50%;'>
    <legend>&nbsp;</legend>
    <label>Wait List Type</label>
    <input type="radio" name="wait_list_type" value="0"> <label style='display:inline-block;'>Manual - Wait listed players are assigned by admins</label><br/>
    <input type="radio" name="wait_list_type" value="1"> <label style='display:inline-block;'>Open - All registered players can check in</label><br/>
    <input type="radio" name="wait_list_type" value="2"> <label style='display:inline-block;'>Timed - Open wait list after specified time</label><br/><br/>
    </fieldset>
  <div style='clear:both; width:100%;'></div>
    <!-- Tournament Manager New Event Row -->
  <fieldset style='float:left; width:50%;'>
    <legend>Dates &amp; Times</legend>
    <label>Event Start</label>
    <div class="input-append dateTimePicker">
        <input name="eventstartdatetime" data-format="dd-MM-yyyy hh:mm:ss" type="text"></input>
        <span class="add-on">
            <i data-time-icon="icon-time" data-date-icon="icon-calendar">
        </i>
    </span>
    </div>
    <label>Registration Start</label>
    <div class="input-append dateTimePicker">
        <input name="registrationstartdatetime" data-format="dd-MM-yyyy hh:mm:ss" type="text"></input>
        <span class="add-on">
            <i data-time-icon="icon-time" data-date-icon="icon-calendar">
        </i>
    </span>
    </div>
    <label>Check In Start</label>
    <div class="input-append dateTimePicker">
        <input name="checkinstartdatetime" data-format="dd-MM-yyyy hh:mm:ss" type="text"></input>
        <span class="add-on">
            <i data-time-icon="icon-time" data-date-icon="icon-calendar">
        </i>
    </span>
    </div>
  </fieldset>
  <fieldset style='float:right; width:50%;'>
    <legend>&nbsp;</legend>
    <label>Event End</label>
    <div class="input-append dateTimePicker">
        <input name="eventenddatetime" data-format="dd-MM-yyyy hh:mm:ss" type="text"></input>
        <span class="add-on">
            <i data-time-icon="icon-time" data-date-icon="icon-calendar">
        </i>
    </span>
    </div>
    <label>Registration End</label>
    <div class="input-append dateTimePicker">
        <input name="registrationenddatetime" data-format="dd-MM-yyyy hh:mm:ss" type="text"></input>
        <span class="add-on">
            <i data-time-icon="icon-time" data-date-icon="icon-calendar">
        </i>
    </span>
    </div>
    <label>Check In End</label>
    <div class="input-append dateTimePicker">
        <input name="checkinenddatetime" data-format="dd-MM-yyyy hh:mm:ss" type="text"></input>
        <span class="add-on">
            <i data-time-icon="icon-time" data-date-icon="icon-calendar">
        </i>
    </span>
    </div>
  </fieldset>
      <div style='clear:both; width:100%;'></div>
      <!-- Tournament Manager New Event Row -->
  <fieldset style='float:left; width:100%;'>
      <legend>Event Rules</legend>
    <textarea name="rules"></textarea>
    </fieldset>
  <div style='clear:both; width:100%;'></div>  
       <!-- Tournament Manager New Event Row -->
  <fieldset style='float:left; width:100%;'>
      <legend>Event Details</legend>
    <textarea name="details"></textarea>
    </fieldset>
  <div style='clear:both; width:100%;'></div>    
    <!-- Tournament Manager New Event Row -->
  <fieldset style='float:left; width:50%;'>
    <legend>Administrators</legend>
    <label>Admin 1</label>
    <select id="admin1" name="admin1"><option value=''>--------</option>
    <?php
        $db->ExecuteSQL("SELECT playerid, username FROM player WHERE usergroupid = 6");
        $admins = $db->ArrayResults();
        foreach($admins as $a) { echo "<option value='".$a['playerid']."'>".$a['username']."</option>";}
    ?>
    </select>
    <label>Admin 2</label>
    <select id="admin2" name="admin2"><option value=''>--------</option>
    <?php
        $db->ExecuteSQL("SELECT playerid, username FROM player WHERE usergroupid = 6");
        $admins = $db->ArrayResults();
        foreach($admins as $a) { echo "<option value='".$a['playerid']."'>".$a['username']."</option>";}
    ?>
    </select>
    <label>Admin 3</label>
    <select id="admin3" name="admin3"><option value=''>--------</option>
    <?php
        $db->ExecuteSQL("SELECT playerid, username FROM player WHERE usergroupid = 6");
        $admins = $db->ArrayResults();
        foreach($admins as $a) { echo "<option value='".$a['playerid']."'>".$a['username']."</option>";}
    ?>
    </select>
  </fieldset>
  <fieldset style='float:right; width:50%;'>
    <legend>Casters</legend>
    <label>Caster 1</label>
    <select id="caster1" name="caster1"><option value=''>--------</option>
    <?php
        $db->ExecuteSQL("SELECT playerid, username FROM player WHERE usergroupid = 6");
        $admins = $db->ArrayResults();
        foreach($admins as $a) { echo "<option value='".$a['playerid']."'>".$a['username']."</option>";}
    ?>
    </select>
    <label>Caster 2</label>
    <select id="caster2" name="caster2"><option value=''>--------</option>
    <?php
        $db->ExecuteSQL("SELECT playerid, username FROM player WHERE usergroupid = 6");
        $admins = $db->ArrayResults();
        foreach($admins as $a) { echo "<option value='".$a['playerid']."'>".$a['username']."</option>";}
    ?>
    </select>
    <label>Caster 3</label>
    <select id="caster3" name="caster3"><option value=''>--------</option>
    <?php
        $db->ExecuteSQL("SELECT playerid, username FROM player WHERE usergroupid = 6");
        $admins = $db->ArrayResults();
        foreach($admins as $a) { echo "<option value='".$a['playerid']."'>".$a['username']."</option>";}
    ?>
    </select>
  </fieldset>
</form>
<div style='clear:both; width:100%;'></div>

