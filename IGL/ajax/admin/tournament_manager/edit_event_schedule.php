<?php 
    require('../../../includes/config.php');
    require('../../../includes/functions.php');
    require('../../../model/mysql.class.php');
    require('../../../model/tournament.class.php');
    require('../../../model/player.class.php');
    require('../../../model/activity.class.php');
    require('../../../model/history.class.php');
    require('../../../model/image.class.php');
    require('../../../model/league.class.php');
    require('../../../model/game.class.php');
    require('../../../model/team.class.php');
    $db = new MySQL();
    $game = new game();
    $games = $game->all();
    $tournaments = new tournament();
    $tournament = $tournaments->get($_POST['id']); 
    $schedule = $tournaments->getSchedule($_POST['id']);
?>

<script>
    var tournament = <?php echo json_encode($tournament); ?>;
    function schedule_eventCallback() {
       $('input[name="id"]').val(tournament['id']);
       $('input[name="offset"]').val(PlayIGL.getLocalTimezoneOffset());
    }
</script>
<style>
    input[type="text"] { height:14px !important; }
    .seed input { width:26px; margin:0; }
    select {height: 24px !important; margin:0px !important;}
    input[type="checkbox"] { position:relative; top:-3px; }
    fieldset { margin-bottom:10px; }
    legend { background:#ddd; padding:0px; text-indent:10px; position:relative; display:inline-block; left:-10px; }
    .table { margin-bottom:none; }
</style>

<form id='newEventForm'>
    <input type="hidden" name="id" value="" />
    <input type="hidden" name="offset" value="" />
    
    <div class="addRoundForm" style="padding:5px;">
        <small>Number of Matches:</small> 
        <select id="sizeList">
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="4">4</option>
            <option value="8">8</option>
            <option value="16">16</option>
            <option value="32">32</option>
            <option value="64">64</option>
        </select>
        <div class="btn addRoundBtn"><i class="icon-plus-sign"></i> Add Round</div>
    </div>
    
    <?php
    foreach($schedule as $s) {
        $matches = $tournaments->getMatches($s['id']);
    ?>
    
    <div class="roundDetails" data-round-id="<?php echo $s['id']; ?>">
        <div style="background:#1b1b1b; height:25px; color:#999; font-size:14px; font-weight:normal; padding:5px; line-height:25px;"><?php echo $s['round_number']; ?> - <?php echo $s['name']; ?><div class='btn btn-mini btn-inverse pull-right deleteRoundBtn' style='min-width:20px;'><i class='icon-remove'></i></div><div class="btn btn-mini btn-inverse pull-right editRoundBtn" style="min-width:20px; margin-right:5px;"><i class="icon-pencil"></i></div></div>
        <table class="table table-bordered table-striped table-condensed">
            <thead><tr><th width='18px'>ID</th><th>Matches</th><th>Start Time</th><th>Format</th><th>Map Pool</th><th width="40px"></th></tr></thead>
            <tbody>
                <?php
                    echo '<tr><td class="id">'.$s['id'].'</td><td>'.$s['match_count'].'</td><td><span class="localTime">'.$s['startdatetime'].'</td><td>'.$s['format'].'</td><td>'.$s['map_pool'].'</td><td></td></tr>';
                ?>
            </tbody>
        </table>
        <table class="table table-bordered table-striped table-condensed">
            <thead><tr style="background:#fff;" class="subHeader"><th width='18px'>ID</th><th>Match #</th><th>Home Team</th><th>Away Team</th><th>Match Time</th><th>Status</th><th width="40px"></th></tr></thead>
            <tbody>
                <?php
                foreach($matches as $m) {
                    if ($m['status'] == 1) { $m['status'] = '<span class="label label-success">Complete</span>'; } else { $m['status'] = '<span class="label">Pending</span>'; }
                    echo '<tr><td class="id">'.$m['id'].'</td><td>'.$m['match_number'].'</td><td>'.$m['team1_seed'].' - '.$m['team1_name'].'</td><td>'.$m['team2_seed'].' - '.$m['team2_name'].'</td><td><span class="localTime">'.$m['match_time'].'</span></td><td>'.$m['status'].'</td><td><div class="btn btn-mini btn-inverse editMatchBtn" style="min-width:20px; margin-right:5px;"><i class="icon-pencil"></i></div></td></tr>';
                }
                ?>
            </tbody>
        </table>
    </div>
    
    <?php
    }
    ?>
    
    
</form>
<div style='clear:both; width:100%;'></div>

