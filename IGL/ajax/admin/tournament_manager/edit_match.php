<?php 
    require('../../../includes/config.php');
    require('../../../includes/functions.php');
    require('../../../model/mysql.class.php');
    require('../../../model/tournament.class.php');
    require('../../../model/player.class.php');
    require('../../../model/activity.class.php');
    require('../../../model/history.class.php');
    require('../../../model/image.class.php');
    require('../../../model/league.class.php');
    require('../../../model/game.class.php');
    require('../../../model/team.class.php');
    $db = new MySQL();
    $game = new game();
    $games = $game->all();
    $tournaments = new tournament();
    $match = $tournaments->getMatch($_POST['id']); 
    $teams = $tournaments->getTeams($match['tournament_id']);
?>
<script>
    var match = <?php echo json_encode($match); ?>;
    function edit_matchCallback() {
        for(var item in match) {
            $('input[name="'+item+'"][type!="radio"]').val(match[item]);
            $('select[name="'+item+'"]').val(match[item]);
        }
        $('.dateTimePicker').each(function(i, v) {
                $(this).find('input').val(PlayIGL.formatTimestamp($(v).find('input').val(), true));
       }); 
       $('input[name="offset"]').val(PlayIGL.getLocalTimezoneOffset());
    }
</script>
<style>
    input[type="text"] { height:20px !important; }
    select {height: 30px !important; margin-top:0px !important;}
    input[type="checkbox"] { position:relative; top:-3px; }
    fieldset { margin-bottom:10px; }
    legend { background:#ddd; padding:0px; text-indent:10px; position:relative; display:inline-block; left:-10px; }
</style>

<form id='newEventForm' style='margin-top:-20px; padding:0px 10px;'>
    <input type="hidden" name="id" value="" />
    <input type="hidden" name="offset" value="" />
    <!-- Tournament Manager New Event Row -->
  <fieldset style='float:left; width:50%;'>
    <legend>Match Information</legend>
    <label>Team 1</label>
    <select name='team1_id'>
        <?php
        foreach($teams['members'] as $t) {
            echo "<option value='".$t['id']."'>".$t['seed']." - ".$t['name']."</option>";
        }
        ?>
    </select>
    <label>Team 2</label>
    <select name='team2_id'>
        <?php
        foreach($teams['members'] as $t) {
            echo "<option value='".$t['id']."'>".$t['seed']." - ".$t['name']."</option>";
        }
        ?>
    </select>
  </fieldset>
  <fieldset style='float:right; width:50%;'>
    <legend>&nbsp;</legend>
    <label>Match Date Time</label>
    <div class="input-append dateTimePicker">
        <input name="match_time" data-format="dd-MM-yyyy hh:mm:ss" type="text"></input>
        <span class="add-on">
            <i data-time-icon="icon-time" data-date-icon="icon-calendar">
        </i>
    </span>
    </div>
    <label>Status</label>
    <select name="status">
        <option value="0">Incomplete</option>
        <option value="1">Complete</option>
    </select>
  </fieldset>
</form>
<div style='clear:both; width:100%;'></div>

