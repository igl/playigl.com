<?php 
    require('../../../includes/config.php');
    require('../../../includes/functions.php');
    require('../../../model/mysql.class.php');
    require('../../../model/tournament.class.php');
    $tournament = new tournament();
    $tournaments = $tournament->all();
?>

<style>
    thead tr {
        background-color: #F5F5F5;
        background-image: -moz-linear-gradient(top, #FFF, #E6E6E6);
        background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#FFF), to(#E6E6E6));
        background-image: -webkit-linear-gradient(top, #FFF, #E6E6E6);
        background-image: -o-linear-gradient(top, #FFF, #E6E6E6);
        background-image: linear-gradient(to bottom, #FFF, #E6E6E6);
        background-repeat: repeat-x;
    }
</style>

<table class="table table-bordered table-striped table-condensed">
        <thead><tr><th width='18px'>ID</th><th>Event Name</th><th>Game</th><th>Start Date</th><th>Status</th><th width='195px'></th></tr></thead>
        <tbody>
            <?php
            foreach($tournaments as $t) {
                echo "<tr><td class='id'>".$t['tournament_id']."</td><td>".$t['tournament_name']."</td><td>".$t['tournament_game_name']."</td><td><span class='localTime'>".$t['tournament_startdatetime']."</span></td><td>Pending</td><td><div class='btn btn-mini btn-inverse editEventBtn' style='min-width:20px; margin-right:5px;'><i class='icon-pencil'></i></div><div class='btn btn-mini btn-inverse membersBtn' style='min-width:20px; margin-right:5px;'><i class='icon-group'></i></div><div class='btn btn-mini btn-inverse scheduleBtn' style='min-width:20px; margin-right:5px;'><i class='icon-table'></i></div><div class='btn btn-mini btn-inverse bannerBtn' style='min-width:20px; margin-right:5px;'><i class='icon-picture'></i></div><div class='btn btn-mini btn-inverse deleteEventBtn' style='min-width:20px;'><i class='icon-remove'></i></div></td></tr>";
            }
            ?>
         </tbody>
    </table>
