<?php 
    require('../../../includes/config.php');
    require('../../../includes/functions.php');
    require('../../../model/mysql.class.php');
    require('../../../model/tournament.class.php');
    require('../../../model/player.class.php');
    require('../../../model/activity.class.php');
    require('../../../model/history.class.php');
    require('../../../model/image.class.php');
    require('../../../model/league.class.php');
    require('../../../model/game.class.php');
    require('../../../model/team.class.php');
    $db = new MySQL();
    $game = new game();
    $games = $game->all();
    $tournaments = new tournament();
    $schedule = $tournaments->getRound($_POST['id']);   
?>

<script>
    var schedule1 = <?php echo json_encode($schedule); ?>;
    var schedule = schedule1[0];
    function edit_roundCallback() {
        for(var item in schedule) {
            $('input[name="'+item+'"][type!="radio"]').val(schedule[item]);
            $('select[name="'+item+'"]').val(schedule[item]);
        }
        $('.dateTimePicker').each(function(i, v) {
                $(this).find('input').val(PlayIGL.formatTimestamp($(v).find('input').val(), true));
       }); 
       $('input[name="offset"]').val(PlayIGL.getLocalTimezoneOffset());
    }
</script>
<style>
    input[type="text"] { height:20px !important; }
    select {height: 30px !important; margin-top:0px !important;}
    input[type="checkbox"] { position:relative; top:-3px; }
    fieldset { margin-bottom:10px; }
    legend { background:#ddd; padding:0px; text-indent:10px; position:relative; display:inline-block; left:-10px; }
</style>

<form id='newEventForm' style='margin-top:-20px; padding:0px 10px;'>
    <input type="hidden" name="id" value="" />
    <input type="hidden" name="offset" value="" />
    <!-- Tournament Manager New Event Row -->
  <fieldset style='float:left; width:50%;'>
    <legend>Round Information</legend>
    <label>Round Name</label>
    <input name='name' type="text" placeholder="">
    <label>Round Number</label>
    <input name='round_number' type="text" placeholder="">
  </fieldset>
  <fieldset style='float:right; width:50%;'>
    <legend>&nbsp;</legend>
    <label>Start Date Time</label>
    <div class="input-append dateTimePicker">
        <input name="startdatetime" data-format="dd-MM-yyyy hh:mm:ss" type="text"></input>
        <span class="add-on">
            <i data-time-icon="icon-time" data-date-icon="icon-calendar">
        </i>
    </span>
    </div>
    <label>Format</label>
    <input name='format' type="text" placeholder="">
    <label>Map Pool</label>
    <input name='map_pool' type="text" placeholder="">
  </fieldset>
</form>
<div style='clear:both; width:100%;'></div>

