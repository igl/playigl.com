<?php

require('../../../includes/config.php');
require('../../../includes/functions.php');
require('../../../model/mysql.class.php');
require('../../../model/tournament.class.php');
$tournament = new tournament();
$tournaments = $tournament->all();
$data = $_POST;

$offset = $data['offset'];
unset($data['offset']);
try {
$dateObject = new DateTime($data['startdatetime']);$data['startdatetime'] = $dateObject->getTimestamp()-$offset;
} catch (Exception $e) {
    print_r($e);
}
if (!$tournament->saveRound($data, $data['id'])) {
    echo "<div class='alert alert-danger' style='margin:0px 10px; position:relative; top:10px;'><strong>Error.</strong> " . $tournament->lastError() . "</div>";
} else {
    echo "<div class='alert alert-success' style='margin:0px 10px; position:relative; top:10px;'><strong>Round Updated!</strong> Press back to return to the event list.</div>";
}
?>

