<?php

require('../../../includes/config.php');
require('../../../includes/functions.php');
require('../../../model/mysql.class.php');
require('../../../model/tournament.class.php');
$tournament = new tournament();
$tournaments = $tournament->all();
$data = $_POST;
if (!$tournament->deleteTeam($data['id'])) {
    echo "<div class='alert alert-danger' style='margin:0px 10px; position:relative; top:10px;'><strong>Error.</strong> " . $tournament->lastError() . "</div>";
} else {
    echo "<div class='alert alert-warning' style='margin:0px 10px; position:relative; top:10px;'><strong>Member Deleted!</strong> Press back to return to the event list.</div>";
}
?>

