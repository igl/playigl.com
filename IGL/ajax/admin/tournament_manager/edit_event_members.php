<?php 
    require('../../../includes/config.php');
    require('../../../includes/functions.php');
    require('../../../model/mysql.class.php');
    require('../../../model/tournament.class.php');
    require('../../../model/player.class.php');
    require('../../../model/activity.class.php');
    require('../../../model/history.class.php');
    require('../../../model/image.class.php');
    require('../../../model/league.class.php');
    require('../../../model/game.class.php');
    require('../../../model/team.class.php');
    $db = new MySQL();
    $tournaments = new tournament();
    $tournament = $tournaments->get($_POST['id']);
    $allteams = $tournaments->getTeamsByGame($tournament['game']);
    $members = $tournaments->getTeams($_POST['id']); 
?>

<script>
    var members = <?php echo json_encode($members); ?>;
    console.log(members);
    function members_eventCallback() {
       $('input[name="id"]').val(members['id']);
       $('input[name="offset"]').val(PlayIGL.getLocalTimezoneOffset());
       $(members.members).each(function(i, v) {
           $('select[name="tstatus_'+v.id+'"]').val(v.tstatus);
           console.log(v);
       })
    }
</script>
<style>
    input[type="text"] { height:14px !important; }
    .seed input { width:26px; margin:0; }
    select {height: 24px !important; margin:0px !important;}
    input[type="checkbox"] { position:relative; top:-3px; }
    fieldset { margin-bottom:10px; }
    legend { background:#ddd; padding:0px; text-indent:10px; position:relative; display:inline-block; left:-10px; }
</style>

<form id='newEventForm'>
    <input type="hidden" name="id" value="" />
    <input type="hidden" name="offset" value="" />
    
    <div class="addMemberForm" style="padding:5px;">
        <select id="memberList">
            <option value="0">--- (bye)</option>
            <?php
            foreach($allteams as $team) {
                echo "<option value='".$team['id']."'>".$team['name']."</option>";
            }
            ?>
        </select>
        <div class="btn addTeamBtn"><i class="icon-plus-sign"></i> Add Team</div>
    </div>
    
    <table class="table table-bordered table-striped table-condensed">
        <thead><tr><th width='18px'>ID</th><th width='26px'>Seed</th><th>Team Name</th><th>Captain</th><th width="230px">Status</th><th width="40px"></th></tr></thead>
        <tbody>
            <?php
            foreach($members['members'] as $m) {
                echo "<tr><td class='id'>".$m['id']."</td><td class='seed'><input name='seed_".$m['id']."' type='text' value='".$m['seed']."' /></td><td>".$m['name']."</td><td>".$m['captain']."</td><td><select name='tstatus_".$m['id']."'><option value='0'>Registered</option><option value='1'>Checked In</option><option value='3'>Wait List</option></select></td><td><div class='btn btn-mini btn-inverse deleteMemberBtn' style='min-width:20px;'><i class='icon-remove'></i></div></td></tr>";
            }
            ?>
         </tbody>
    </table>
</form>
<div style='clear:both; width:100%;'></div>

