<?php 
    require('../../../includes/config.php');
    require('../../../includes/functions.php');
    require('../../../model/mysql.class.php');
    require('../../../model/tournament.class.php');
    require('../../../model/player.class.php');
    require('../../../model/activity.class.php');
    require('../../../model/history.class.php');
    require('../../../model/image.class.php');
    require('../../../model/league.class.php');
    require('../../../model/game.class.php');
    require('../../../model/team.class.php');
    $db = new MySQL();
    $game = new game();
    $games = $game->all();
    $tournaments = new tournament();
?>
<script>
    function banner_eventCallback() {
        $('#banner_image').on('change', function() {
            $('#newEventForm').submit();
        })
    }
</script>
<style>
    input[type="text"] { height:20px !important; }
    select {height: 30px !important; margin-top:0px !important;}
    input[type="checkbox"] { position:relative; top:-3px; }
    fieldset { margin-bottom:10px; }
    legend { background:#ddd; padding:0px; text-indent:10px; position:relative; display:inline-block; left:-10px; }
</style>

<form id='newEventForm' method="POST" enctype="multipart/form-data" target="banner_frame" action="/ajax/admin/tournament_manager/save_event_banner.php" style='margin-top:-20px; padding:0px 10px;'>
    <input type="hidden" name="id" value="<?php echo $_REQUEST['id']; ?>" />

    <!-- Tournament Manager New Event Row -->
  <fieldset style='float:left; width:100%;'>
    <legend>Event Banner</legend>
    <iframe id="banner_frame" style="height:200px; width:950px;" frameborder="0" src="/img/event_banners/<?php echo $_REQUEST['id']; ?>.jpg" />
  </fieldset>
  <fieldset style='float:left; width:100%;'>
    <legend>Upload New Banner</legend>
    <p>Required Size: 950px by 200px</p>
    <p>Required Type: .jpg</p>
    <input type="file" name="banner_image" id="banner_image" />
  </fieldset>
</form>
<div style='clear:both; width:100%;'></div>

