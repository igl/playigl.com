<?php

require('../../../includes/config.php');
require('../../../includes/functions.php');
require('../../../model/mysql.class.php');
require('../../../model/tournament.class.php');
$db = new MySQL();
$tournament = new tournament();
$tournaments = $tournament->all();
$data = $_POST;

$offset = $data['offset'];
$tid = $data['id'];
unset($data['offset']);
unset($data['id']);
$error = false;
$catchError = '';
foreach($data as $k => $d) {
    $kd = explode('_', $k);
    $id = $kd[1];
    $col = $kd[0];
    $val = $d;
    if ($db->Update('tournament_teams', array($col => $val), array('team_id' => $id))) {
        $error = false;
    } else {
        $catchError = $db->sLastError;
        $error = true;
    }
}
if ($error) {
    echo "<div class='alert alert-danger' style='margin:0px 10px; position:relative; top:10px;'><strong>Error.</strong> " . $catchError . "</div>";
} else {
    echo "<div class='alert alert-success' style='margin:0px 10px; position:relative; top:10px;'><strong>Members Updated!</strong> Press back to return to the event list.</div>";
}
?>

