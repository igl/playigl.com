<?php

require('../../../includes/config.php');
require('../../../includes/functions.php');
require('../../../model/mysql.class.php');
require('../../../model/tournament.class.php');
$tournament = new tournament();
$tournaments = $tournament->all();
$data = $_POST;

$offset = $data['offset'];
unset($data['offset']);

if(isset($data['enable_check_in'])) { $data['enable_check_in'] = 1; } else { $data['enable_check_in'] = 0; }
if(isset($data['enable_wait_list'])) { $data['enable_wait_list'] = 1; } else { $data['enable_wait_list'] = 0; }

try {
$dateObject = new DateTime($data['eventstartdatetime']);$data['eventstartdatetime'] = $dateObject->getTimestamp()-$offset;

$dateObject = new DateTime($data['eventenddatetime']);$data['eventenddatetime'] = $dateObject->getTimestamp()-$offset;

$dateObject = new DateTime($data['checkinstartdatetime']);$data['checkinstartdatetime'] = $dateObject->getTimestamp()-$offset;

$dateObject = new DateTime($data['checkinenddatetime']);$data['checkinenddatetime'] = $dateObject->getTimestamp()-$offset;

$dateObject = new DateTime($data['registrationstartdatetime']);$data['registrationstartdatetime'] = $dateObject->getTimestamp()-$offset;

$dateObject = new DateTime($data['registrationenddatetime']);$data['registrationenddatetime'] = $dateObject->getTimestamp()-$offset;
} catch (Exception $e) {
    print_r($e);
}
if (!$tournament->save($data, $data['id'])) {
    echo "<div class='alert alert-danger' style='margin:0px 10px; position:relative; top:10px;'><strong>Error.</strong> " . $tournament->lastError() . "</div>";
} else {
    echo "<div class='alert alert-success' style='margin:0px 10px; position:relative; top:10px;'><strong>Event Updated!</strong> Press back to return to the event list.</div>";
}
?>

