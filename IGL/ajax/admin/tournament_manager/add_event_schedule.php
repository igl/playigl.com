<?php

require('../../../includes/config.php');
require('../../../includes/functions.php');
require('../../../model/mysql.class.php');
require('../../../model/tournament.class.php');
$tournament = new tournament();
$data = $_POST;
if (!$tournament->createSchedule($data['tid'], $data['id'])) {
    echo "<div class='alert alert-danger' style='margin:0px 10px; position:relative; top:10px;'><strong>Error.</strong> " . $catchError . "</div>";
} else {
    echo "<div class='alert alert-success' style='margin:0px 10px; position:relative; top:10px;'><strong>Round Added!</strong> Press back to return to the event list.</div>";
}
?>

