<?php

/**
 * This script will fetch email templates
 * @author Darryl Allen
 * @copyright Copyright 2013 PlayIGL.com
 */

header('Cache-Control: no-cache, must-revalidate');
header('Content-type: application/json');

require('../../includes/config.php');
require('../../includes/functions.php');
require('../../includes/permissions.php');
require('../../model/mysql.class.php');

if( ADMIN )
{
    $db = new MySQL();
    $query = "SELECT * FROM email_templates";
    $db->ExecuteSQL($query);
    $return = array('success' => true, 'templates' => $db->ArrayResults());
}
else
{
    $return = array('success' => false, 'error' => 'You do not have access to this page.');
}

echo prettyJSON(json_encode($return));