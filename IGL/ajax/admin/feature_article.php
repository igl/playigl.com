<?php
/**
 * Features an article
 * @author Shaun Delaney <sdelaney@playigl.com>
 * @return JSON Success flag
 * @copyright Copyright 2013 PlayIGL.com
 */

include_once('../../includes/config.php');
include_once('../../model/mysql.class.php');
include_once('../../includes/functions.php');

$return = array("success" => false, "error" => "Please log in to use this feature.");

if (isLoggedIn()) {
    $article_id   = $_REQUEST['id'];
    $featured = $_REQUEST['featured'];
    
    //Reject non-numeric IDs
    if (!is_numeric($article_id)) {
        $return['error'] = "Invalid data passed.";
        returnJSON($return);
    }
    
    if ($featured == "true")
      $featured = 0;
    else
      $featured = 1;

    $db = new MySQL(MYSQL_DATABASE);
    $b = $db->Update('article', array("featured" => $featured), array("id" => $article_id));
    
    if ($b)
      $return = array("success" => true, "featured" => $featured);
    else
      $return["error"] = "MySQL error";
}

returnJSON($return);