<?php

/**
 * This script uploads images for the WYSIWYG editor
 * @author Darryl Allen
 * @copyright Copyright 2012 PlayIGL.com
 */

header('Cache-Control: no-cache, must-revalidate');
header('Content-type: application/json');

require('../../includes/config.php');
require('../../includes/functions.php');
require('../../model/mysql.class.php');

$usergroups = explode(",", $_SESSION['membergroupids']);
if((bool) array_intersect(array(6,10,12), $usergroups) || $_SESSION['usergroupid']==6)
{
    if(isset($_FILES['file']))
    {
        if(is_uploaded_file($_FILES['file']['tmp_name']))
        {
            $db = new mysql(MYSQL_DATABASE);
            #format images as binary data
            $imageblob = file_get_contents($_FILES['file']['tmp_name']);

            if($db->Insert(array('image' => $imageblob), 'image'))
            {
                $return = array('filelink' => BASEURL."userimages/logo_");		
            }
            else
            {
                $return['success'] = false;
                $return['error'] = 'Image upload failed.';
            }
            
        }
        else
        {
            $return['success'] = false;
            $return['error'] = 'Image upload failed.';
        }
    }
    else
    {
        $return['success'] = false;
        $return['error'] = 'No file selected.';
    }
}
else
{
    $return['success'] = false;
    $return['error'] = 'You lack permissions.';
}
echo stripslashes(json_encode($return)); 
