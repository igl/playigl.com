<?php

/**
 * This script will save email templates
 * @author Darryl Allen
 * @copyright Copyright 2013 PlayIGL.com
 */

header('Cache-Control: no-cache, must-revalidate');
header('Content-type: application/json');

require('../../includes/config.php');
require('../../includes/functions.php');
require('../../includes/permissions.php');
require('../../model/mysql.class.php');
require('../../sendgrid-php/SendGrid_loader.php');

if( ADMIN )
{
    if(isset($_REQUEST['from']) && isset($_REQUEST['subject']) && isset($_REQUEST['body']))
    {
        $sendgrid = new SendGrid('playigl', 'xeCr9feJ');
        $mail = new SendGrid\Mail();
        $mail->addTo($_SESSION['email']);
        $mail->setCategory('Template Tests');
        $mail->setFrom($_REQUEST['from']);
        $mail->setFromName('International Gaming League');
        $mail->setSubject($_REQUEST['subject']);
        $mail->setHtml($_REQUEST['body']);
        $sendgrid->web->send($mail);
        $return = array('success' => true);      
    }
    else
    {
        $return = array('success' => false, 'error' => 'Some fields are missing.');
    }
    
}
else
{
    $return = array('success' => false, 'error' => 'You do not have access to this page.');
}

echo prettyJSON(json_encode($return));
