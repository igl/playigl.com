<?php

/**
 * This script will save a team if the player is captain or alternate
 * @author Steve Rabouin
 * @copyright Copyright 2012 PlayIGL.com
 */

header('Cache-Control: no-cache, must-revalidate');
header('Content-type: application/json');

require('../../includes/config.php');
require('../../includes/functions.php');
require('../../model/mysql.class.php');
include('../../model/history.class.php');
include('../../model/activity.class.php');

$return = array();

if( isset($_SESSION['playerid']) && $_SESSION['playerid'] > 0 )
{
    if(isset($_GET['player']) && is_numeric($_GET['player']) && isset($_GET['team']) && is_numeric($_GET['team']))
    {
        $player = (int)$_GET['player'];
        $team   = (int)$_GET['team'];
        $activity = new activity();
        $db = new mysql(MYSQL_DATABASE);
        $history = new history();
        
        
        // get team information to ensure we can modify it
        $query = "
                SELECT 
                    `team`.`id`,
                    `team`.`captain`,
                    `team`.`alternate`,
                    `team`.`game`
                FROM 
                    `".MYSQL_DATABASE."`.`team`
                WHERE 
                    `team`.`id` = '{$team}'
                LIMIT 1
        ";   
        $db->ExecuteSQL($query);
        if($db->iRecords())
        {
            $team_data = $db->ArrayResult();
            $usergroups = explode(",", $_SESSION['membergroupids']);
            $usergroups = explode(",", $_SESSION['membergroupids']);
            if(((bool) array_intersect(array(6,10), $usergroups) || $_SESSION['usergroupid']==6) )
            {
                $query = "SELECT `username` FROM `".MYSQL_DATABASE."`.`roster` LEFT JOIN `player` ON `player`.`playerid` = `roster`.`player` WHERE `roster`.`player` = '{$player}' AND `game` = {$team_data['game']}";
                $db->ExecuteSQL($query);
                if(!$db->iRecords())
                {
                    if($db->Insert(array('player' => $player, 'team' => $team, 'game' => $team['game']), 'roster'))
                    {
                        $db->Delete('joinrequests', array('team' => $team, 'player' => $player));
                        $db->Delete('freeagents', array('team' => $team, 'player' => $player));
                        $db->Delete('inviterequests', array('game' => $team_data['game'], 'player' => $player));
                        $return['success'] = true;
                    }
                    else
                    {
                        $return['success'] = false;
                        $return['error'] = "Error adding user to roster.";
                    }
                }
                else
                {
                    $user = $db->ArrayResult();
                    $return['success'] = false;
                    $return['error'] = "{$user['username']} is already participating in this game/division.";
                }
            }
            else
            {
                $return['success'] = false;
                $return['error'] = "Only admins may use this feature.";
            }
        }
        else
        {
            $return['success'] = false;
            $return['error'] = "This team does not exist.";
        }
    }
    else
    {
        $return['success'] = false;
        $return['error'] = "Missing parameters.";
    }
}
else
{
    $return['success'] = false;
    $return['error'] = "Sorry, you need to be logged in to use this feature.";
}

echo prettyJSON(json_encode($return));
