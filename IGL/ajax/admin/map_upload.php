<?php

header('Cache-Control: no-cache, must-revalidate');

require('../../includes/config.php');
require('../../includes/functions.php');
require('../../model/mysql.class.php');

$return = array();

$db = new mysql(MYSQL_DATABASE);

$allowedExts = array("zip", "rar");
$extension = end(explode(".", $_FILES["file"]["name"]));
$game_id = $_POST['game'];
$name = $_POST['name'];
if (!empty($name)) {
    $db->Insert(array('game' => $game_id, 'map' => addslashes($name),'config_id' => $_POST['config_id']), 'maplist');
    
    if ($_FILES["file"]["error"] > 0) {
        echo "Return Code: " . $_FILES["file"]["error"] . "<br>";
    } else {
        echo "Upload: " . $_FILES["file"]["name"] . "<br>";
        echo "Type: " . $_FILES["file"]["type"] . "<br>";
        echo "Size: " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
        echo "Temp file: " . $_FILES["file"]["tmp_name"] . "<br>";

        if (!file_exists("../../maps/" . $game_id . "/")) {
            mkdir("../../maps/" . $game_id, 0777);
        }
        if (file_exists("../../maps/" . $game_id . "/" . $name . "." . $extension)) {
            echo $name . "." . $extension . " already exists. ";
        } else {
            move_uploaded_file($_FILES["file"]["tmp_name"], "../../maps/" . $game_id . "/" . $name . "." . $extension);
            echo "Stored in: " . "/maps/" . $game_id . "/" . $name . "." . $extension;
        }
    }
} else {
    echo "Return Code: empty<br>";
}
?>
