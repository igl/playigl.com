<?php

/**
 * Registration Script for Public Website
 * @author Kevin Langlois*
 * @copyright Copyright 2012 PlayIGL.com
 */

header('Cache-Control: no-cache, must-revalidate');
header('Content-type: application/json');

require('../../includes/config.php');
require('../../includes/functions.php');
require('../../model/mysql.class.php');

#require('../../includes/userfox_tracking.php');

$return = array();

$db = new mysql(MYSQL_DATABASE);

if (isset($_GET['user']) && isset($_GET['hash'])) {
    /* ***********************************
     * Standard Authentication
     * ******************************** */
    $query = "SELECT `username`,`password`,`salt` FROM `".MYSQL_DATABASE."`.`player` WHERE `username` = '".$_GET['user']."'";
    $db->ExecuteSQL($query);
    if($db->iRecords()) {
        $result = $db->ArrayResult();
        /* $realPass = md5($result['password'].$result['salt']); //Not needed, as the password in the DB is already hashed.   */    
        $tryPass = md5($_GET['hash'].$result['salt']);

        if ($tryPass == $result['password'] || $_GET['hash'] == $result['password']) {
            $return['success'] = true; 
            $query = "SELECT * FROM `".MYSQL_DATABASE."`.`player` WHERE `username` = '".$result['username']."'";
            $db->ExecuteSQL($query);
            if($db->iRecords()) {
                $userinfo = $db->ArrayResult();
                unset($userinfo['salt']);
                unset($userinfo['password']);
                $_SESSION['playerid'] = $userinfo['playerid'];
                $_SESSION['avatar'] = $userinfo['avatar'];
                $_SESSION['communityid'] = $userinfo['communityid'];
                $_SESSION['usergroupid'] = $userinfo['usergroupid'];
                $_SESSION['membergroupids'] = $userinfo['membergroupids'];
                $_SESSION['timezoneoffset'] = $userinfo['timezoneoffset'];
                $_SESSION['username'] = $userinfo['username'];
                $_SESSION['email'] = $userinfo['email'];
                $_SESSION['channels'] = $userinfo['channels'];
                $_SESSION['firstname'] = $userinfo['firstname'];
                $_SESSION['lastname'] = $userinfo['lastname'];
                $_SESSION['country'] = $userinfo['country'];
                $_SESSION['gender'] = $userinfo['sex'];
                $_SESSION['birthday'] = $userinfo['birthday'];
                $_SESSION['joindate'] = $userinfo['joindate'];
                $_SESSION['facebook'] = $userinfo['facebook'];
                $_SESSION['twitter'] = $userinfo['twitter'];
                $_SESSION['steam'] = $userinfo['communityid'];
                $_SESSION['twitch'] = $userinfo['twitch'];
                $_SESSION['browseroffset'] = $_GET['offset'];
                
                $return['userinfo'] = $userinfo;
                
                #trackAction('last_login_date', time(), $_SERVER['HTTP_REFERER']);
                //Update last activity
                $db->Update('player', array('lastactivity' => time()), array('playerid' => $userinfo['playerid'], 'ingame_name' => $ingame_name));
            }   
        } else {
            $return['success'] = false; 
           $return['error'] = 'Password or Username is incorrect'; 
        }
    } else {
        $return['success'] = false; 
        $return['error'] = 'Username does not exist'; 
    }
}

echo prettyJSON(json_encode($return));
