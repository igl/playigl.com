<?php

/**
 * This script will save a team if the player is captain or alternate
 * @author Steve Rabouin
 * @copyright Copyright 2012 PlayIGL.com
 */
header('Cache-Control: no-cache, must-revalidate');
header('Content-type: application/json');

require('../../includes/config.php');
require('../../includes/functions.php');
require('../../model/mysql.class.php');
require('../../model/tournament.class.php');
require('../../model/player.class.php');
require('../../model/activity.class.php');
require('../../model/history.class.php');
require('../../model/image.class.php');
require('../../model/league.class.php');
require('../../model/game.class.php');
require('../../model/team.class.php');

if (isset($_REQUEST['action'])) {
    $return = array();

    $db = new mysql(MYSQL_DATABASE);
    $player = new player();


    /*     * *******************************
     * Enroll Team
     * ******************************* */

    if ($_REQUEST['action'] == 'enrollTeam' && isset($_REQUEST['team_id'])) {
        // Check for waitlist
        $query = "SELECT * FROM tournaments WHERE id = " . $_REQUEST['tournament_id'] . "";
        $db->ExecuteSQL($query);
        if ($db->iRecords()) {
            // Check Bracket Size
            $t = $db->ArrayResult();
            $t = $t['bracketsize'];
            $query = "SELECT count(id) as count FROM tournament_teams WHERE tournament_id = '" . $_REQUEST['tournament_id'] . "' AND waitlist_flag = 0";
            $db->ExecuteSQL($query);
            if ($db->iRecords()) {
                $tt = $db->ArrayResult();
                $tt = $tt['count'];
                if ((int) $t > (int) $tt) {
                    if ($db->Insert(array('tournament_id' => $_REQUEST['tournament_id'], 'team_id' => $_REQUEST['team_id'], 'registered_flag' => 1, 'waitlist_flag' => 0, 'checkedin_flag' => 0), 'tournament_teams')) {
                        $return['success'] = true;
                        $return['waitlisted'] = false;
                    } else {
                        $return['success'] = false;
                        $return['error'] = $db->sLastError;
                    }
                } else {
                    if ($db->Insert(array('tournament_id' => $_REQUEST['tournament_id'], 'team_id' => $_REQUEST['team_id'], 'registered_flag' => 1, 'waitlist_flag' => 1, 'checkedin_flag' => 0), 'tournament_teams')) {
                        $return['success'] = true;
                        $return['waitlisted'] = true;
                    } else {
                        $return['success'] = false;
                        $return['error'] = $db->sLastError;
                    }
                }
            }
        } else {
            $return['success'] = false;
            $return['error'] = 'Tournament does not exist';
        }
    }

    /*     * *******************************
     * Withdraw Team
     * ******************************* */

    if ($_REQUEST['action'] == 'withdrawTeam' && isset($_REQUEST['team_id'])) {
        if ($db->Delete('tournament_teams', array('team_id' => $_REQUEST['team_id'], 'tournament_id' => $_REQUEST['tournament_id']))) {
            $return['success'] = true;
            // Recalculate Waitlist
            $query = "SELECT * FROM tournaments WHERE id = " . $_REQUEST['tournament_id'] . "";
            $db->ExecuteSQL($query);
            if ($db->iRecords()) {
                // Check Bracket Size
                $t = $db->ArrayResult();
                $t = $t['bracketsize'];
                $return['bracketsize'] = $t;
                $query = "SELECT count(id) as count FROM tournament_teams WHERE tournament_id = '" . $_REQUEST['tournament_id'] . "' AND waitlist_flag = 0";
                $db->ExecuteSQL($query);
                if ($db->iRecords()) {
                    $tt = $db->ArrayResult();
                    $tt = $tt['count'];
                    $return['enrolled'] = $tt;
                    if ((int) $t > (int) $tt) {
                        $return['moving_waitlisted_team'] = true;
                        // Non waitlisted team removed, move first waitlited team forward
                        $query = "SELECT min(id) as id FROM tournament_teams WHERE tournament_id = '" . $_REQUEST['tournament_id'] . "' AND waitlist_flag = 1";
                        $db->ExecuteSQL($query);
                        $new_id = $db->ArrayResult();
                        $new_id = $new_id['id'];
                        $db->Update('tournament_teams', array('waitlist_flag' => 0), array('id' => $new_id));
                    }
                }
            }
        } else {
            $return['success'] = false;
            $return['error'] = $db->sLastError;
        }
    }

    /*     * *******************************
     * Check In
     * ******************************* */

    if ($_REQUEST['action'] == 'checkIn' && isset($_REQUEST['team_id'])) {
        if ($db->Update('tournament_teams', array('checkedin_flag' => 1), array('team_id' => $_REQUEST['team_id'], 'tournament_id' => $_REQUEST['tournament_id']))) {
            $return['success'] = true;
        } else {
            $return['success'] = false;
            $return['error'] = $db->sLastError;
        }
    }

    /*     * *******************************
     * Get Schedule
     * ******************************* */

    if ($_REQUEST['action'] == 'getSchedule' && isset($_REQUEST['tournament_id'])) {
        $query = "SELECT * FROM tournament_schedule WHERE tournament_id = '" . $_REQUEST['tournament_id'] . "' ORDER BY round_number ASC";
        $db->ExecuteSQL($query);
        if ($db->iRecords()) {
            $schedule = $db->ArrayResults();
            $return = array('schedule' => array('rounds' => $schedule));
        }
    }
    
        /*     * *******************************
     * Get Round
     * ******************************* */

    if ($_REQUEST['action'] == 'getRound' && isset($_REQUEST['round_id'])) {
        $query = "SELECT * FROM tournament_schedule WHERE id = '" . $_REQUEST['round_id'] . "'";
        $db->ExecuteSQL($query);
        if ($db->iRecords()) {
            $schedule = $db->ArrayResult();
            $return = array('schedule' => array('round' => $schedule));
        }
    }
    
        /*     * *******************************
     * Update Round
     * ******************************* */

    if ($_REQUEST['action'] == 'updateRound' && isset($_REQUEST['round_id'])) {
        if ($db->Update('tournament_schedule', array('name' => $_GET['name'], 'startdatetime' => $_GET['startdatetime'], 'format' => $_GET['format'], 'map_pool' => $_GET['map_pool']), array('id' => $_REQUEST['round_id']))) {
            $return['success'] = true;
        } else {
            $return['success'] = false;
            $return['error'] = $db->sLastError;
        }
    }

    /*     * *******************************
     * Generate Schedule
     * ******************************* */

    if ($_REQUEST['action'] == 'generateSchedule' && isset($_REQUEST['tournament_id'])) {
        $query = "SELECT bracketsize FROM tournament WHERE tournament_id = '" . $_REQUEST['tournament_id'] . "'";
        $db->ExecuteSQL($query);
        if ($db->iRecords()) {
            $tournament = $db->ArrayResult();
            if ($tournament['bracketsize'] != "") {
                // Generate Blank Rounds
                $i = $tournament['bracketsize'];
                $n = 1;
                while ($i > 1) {
                    $this->db->insert(array('tournament_id' => $_REQUEST['tournament_id'], 'name' => 'Round ' . $n, 'team_count' => $i), 'tournament_schedule');
                    $n = $n + 1;
                    $i = $i / 2;
                }
                return true;
            } else {
                return true;
            }
        }
    }

    /*     * *******************************
     * Delete Match
     * ******************************* */

    if ($_REQUEST['action'] == 'deleteMatch' && isset($_REQUEST['tournament_id'])) {
        $query = "SELECT max(round) as round FROM tournament_results WHERE tournament_id = " . $_REQUEST['tournament_id'] . "";
        $db->ExecuteSQL($query);
        if ($db->iRecords() > 0) {
            $rounds = $db->ArrayResults();
            $round = ($rounds[0]['round']);
            $query = "DELETE FROM tournament_results WHERE tournament_id = " . $_REQUEST['tournament_id'] . " AND round = " . $round;
            $db->ExecuteSQL($query);
        }
    }

    /*     * *******************************
     * Get Match
     * ******************************* */

    if ($_REQUEST['action'] == 'getMatch' && isset($_REQUEST['match_id'])) {
        $query = "SELECT * FROM tournament_matches WHERE id = " . $_REQUEST['match_id'] . "";
        $db->ExecuteSQL($query);
        if ($db->iRecords() > 0) {
            $match = $db->ArrayResult();
            $return['success'] = true;
            $return['match'] = $match;
        } else {
            $return['success'] = false;
            $return['error'] = $db->sLastError;
        }
    }

    /*     * *******************************
     * Update Match
     * ******************************* */

    if ($_REQUEST['action'] == 'updateMatch' && isset($_REQUEST['match_id'])) {
        if ($db->Update('tournament_matches', array('team1_id' => $_GET['team1_id'], 'team2_id' => $_GET['team2_id'], 'status' => $_GET['status'], 'match_time' => $_GET['match_time']), array('id' => $_REQUEST['match_id']))) {
            $return['success'] = true;
        } else {
            $return['success'] = false;
            $return['error'] = $db->sLastError;
        }
    }

    /*     * *******************************
     * Get All Matches
     * ******************************* */

    if ($_REQUEST['action'] == 'getMatches' && isset($_REQUEST['tournament_id'])) {
        $query = "SELECT id, name, round_number FROM tournament_schedule WHERE tournament_id = '" . $_REQUEST['tournament_id'] . "' ORDER BY round_number ASC";
        $db->ExecuteSQL($query);
        if ($db->iRecords()) {
            $rounds = $db->ArrayResults();
            $i = 0;
            foreach ($rounds as $r) {
                $query = "SELECT * FROM tournament_matches WHERE schedule_id = '" . $r['id'] . "' AND tournament_id = '" . $_REQUEST['tournament_id'] . "' ORDER BY id ASC";
                $db->ExecuteSQL($query);
                if ($db->iRecords()) {
                    $each_match = $db->ArrayResults();
                    for ($t = 0; $t < count($each_match); $t++) {
                        $query = "SELECT * FROM team WHERE id = '" . $each_match[$t]['team1_id'] . "' OR id = '" . $each_match[$t]['team2_id'] . "'";
                        $db->ExecuteSQL($query);
                        $teams = $db->ArrayResults();
                        if (empty($teams[0])) {
                            $teams[0] = array('name' => 0);
                        }
                        if (empty($teams[1])) {
                            $teams[1] = array('name' => 0);
                        }
                        $each_match[$t]['teams'] = $teams;
                        $each_match[$t]['teams'][0]['captain'] = $player->get($each_match[$t]['teams'][0]['captain']);
                        $each_match[$t]['teams'][1]['captain'] = $player->get($each_match[$t]['teams'][1]['captain']);
                        
                        $query = "SELECT * FROM schedule WHERE id = " . $each_match[$t]['submission_id'];
                        $db->ExecuteSQL($query);
                        $schedule = $db->ArrayResult();
                        $each_match[$t]['teams'][0]['score'] = $schedule['home_score'];
                        $each_match[$t]['teams'][1]['score'] = $schedule['away_score'];
                    }
                    $rounds[$i]['matches'] = $each_match;
                } else {
                    $return['success'] = false;
                    $return['error'] = $db->sLastError;
                }
                $return['success'] = true;
                $return['matches'] = $rounds;
                $i++;
            }
        } else {
            $return['success'] = false;
            $return['error'] = $db->sLastError;
        }
    }

    /*     * *******************************
     * Generate Matches
     * ******************************* */

    if ($_REQUEST['action'] == 'generateMatches' && isset($_REQUEST['tournament_id'])) {
        $query = "SELECT * FROM tournaments WHERE id = '" . $_REQUEST['tournament_id'] . "'";
        $db->ExecuteSQL($query);
        if ($db->iRecords()) {
            $tournament = $db->ArrayResult();
            $query = "SELECT max(schedule_id) as schedule_id FROM tournament_matches WHERE tournament_id = " . $_REQUEST['tournament_id'] . "";
            $db->ExecuteSQL($query);
            if ($db->iRecords()) {
                $max_sched_id = $db->ArrayResults();
                if ($max_sched_id[0]['schedule_id'] != "") {
                    $next_sched_id = ($max_sched_id[0]['schedule_id']) + 1;
                    $round1 = false;
                } else {
                    $query = "SELECT min(id) as schedule_id FROM tournament_schedule WHERE tournament_id = " . $_REQUEST['tournament_id'] . "";
                    $db->ExecuteSQL($query);
                    $max_sched_id = $db->ArrayResults();
                    $next_sched_id = ($max_sched_id[0]['schedule_id']);
                    $round1 = true;
                }
            }
            $query = "SELECT * FROM tournament_schedule WHERE id = '" . $next_sched_id . "'";
            $db->ExecuteSQL($query);
            if ($db->iRecords()) {
                $schedule = $db->ArrayResult();
                $next_team_count = $schedule['team_count'];

                $teams = array();
                if ($round1) {
                    // Generate New Team Matchup
                    $query = "SELECT * FROM tournament_teams WHERE tournament_id = '" . $_REQUEST['tournament_id'] . "' AND checkedin_flag = 1";
                    $db->ExecuteSQL($query);
                    $teams = $db->ArrayResults();
                    $teams = array_pad($teams, $next_team_count, array('name' => 'undefined'));
                } else {
                    // Generate Matches based on previous matchup
                    $query = "SELECT team.id as team_id FROM team JOIN tournament_matches ON tournament_matches.team1_id = team.id WHERE tournament_matches.tournament_id = '" . $_REQUEST['tournament_id'] . "'";
                    $db->ExecuteSQL($query);
                    $teams = $db->ArrayResults();
                    $teams = array_pad($teams, $next_team_count, array('name' => 'undfined'));
                }
                // Random Seed Generation
                if ($tournament['seed_method'] == 0) {
                    $tt = 0;
                    for ($i = 0; $i < $next_team_count / 2; $i++) {
                        if ($round1) {
                            $match_teams = array_rand($teams, 2);
                        }
                        if (!$round1) {
                            $match_teams[0] = $tt;
                            $tt++;
                            $match_teams[1] = $tt;
                            $tt++;
                        }
                        $db->Insert(array('tournament_id' => $_REQUEST['tournament_id'], 'schedule_id' => $schedule['id'], 'team1_id' => $teams[$match_teams[0]]['team_id'], 'team2_id' => $teams[$match_teams[1]]['team_id'], 'status' => '0'), 'tournament_matches');
                        unset($teams[$match_teams[0]]);
                        unset($teams[$match_teams[1]]);
                    }
                } else {
                    // Ordered Seed Generation (Based on 1vs32, 2vs31, 1vs2 not in 2nd round)    
                }
                $return['success'] = true;
            } else {
                $return['success'] = false;
                $return['error'] = "No More Rounds";
            }
        } else {
            $return['success'] = false;
            $return['error'] = "Tournament does not exist";
        }
    }
} else {
    $return['success'] = false;
    $return['error'] = "No action provided";
}

echo prettyJSON(json_encode($return));