<?php

/**
 * This script will save email templates
 * @author Darryl Allen
 * @copyright Copyright 2013 PlayIGL.com
 */

header('Cache-Control: no-cache, must-revalidate');
header('Content-type: application/json');

require('../../includes/config.php');
require('../../includes/functions.php');
require('../../includes/permissions.php');
require('../../model/mysql.class.php');

if( ADMIN )
{
    if(isset($_REQUEST['from']) && isset($_REQUEST['subject']) && isset($_REQUEST['body']) && isset($_REQUEST['id']))
    {
        $db = new MySQL();
        $db->Update('email_templates', array(
            'from' => $_REQUEST['from'],
            'subject' => $_REQUEST['subject'],
            'body' => $_REQUEST['body']
        ), array('id' => $_REQUEST['id']));
        $return = array('success' => true);
    }
    else
    {
        $return = array('success' => false, 'error' => 'Some fields are missing.');
    }
    
}
else
{
    $return = array('success' => false, 'error' => 'You do not have access to this page.');
}

echo prettyJSON(json_encode($return));