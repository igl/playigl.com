<?php

/**
 * This script will save a team if the player is captain or alternate
 * @author Steve Rabouin
 * @copyright Copyright 2012 PlayIGL.com
 */

header('Cache-Control: no-cache, must-revalidate');
header('Content-type: application/json');

require('../../includes/config.php');
require('../../includes/functions.php');
require('../../model/mysql.class.php');
require('../../model/tournament.class.php');

if (isset($_GET['id'])) {
$return = array();

$db = new mysql(MYSQL_DATABASE);

$query = "SELECT * FROM ads WHERE id = '".$_GET['id']."'";
$db->ExecuteSQL($query);
if ($db->iRecords()) {
    $result = $db->ArrayResult();
    $return['ad'] = $result;
    
}
} else {    
    $return['success'] = false; $return['error'] = "No action provided";
}
echo prettyJSON(json_encode($return));