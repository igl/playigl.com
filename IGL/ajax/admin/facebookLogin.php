<?php

require('../../includes/functions.php');

$app_id = '190820527646793';
$app_secret = '3555cca5676bbb429279c03009805dda';
$my_url = 'http://www.playigl.com/ajax/admin/facebookLogin.php';

if (!isset($_REQUEST['error']) && isset($_REQUEST['code'])) {
    $code = $_REQUEST["code"];
    $token_url = "https://graph.facebook.com/oauth/access_token?"
       . "client_id=" . $app_id . "&redirect_uri=" . urlencode($my_url)
       . "&client_secret=" . $app_secret . "&code=" . $code;

     $response = file_get_contents($token_url);
     $params = null;
     parse_str($response, $params);
     $graph_url = "https://graph.facebook.com/me?fields=name,username,first_name,last_name,birthday,location&access_token=" . $params['access_token'];

     $info = json_decode(file_get_contents($graph_url));
     $json_user = array();
     $json_user['name'] = $info->username;
     $json_user['first_name'] = $info->first_name;
     $json_user['last_name'] = $info->last_name;
     $json_user['birthday'] = $info->birthday;
     $json_user['location'] = $info->location;
     $json_user_all = json_encode($json_user);
?>
Returning to playigl.com ...
<script>window.opener.Authentication.facebookCallback('<?php echo $params['access_token']; ?>','<?php echo $json_user['name']; ?>','<?php echo $json_user_all; ?>', window);</script>

<?php
} else {
   echo $_REQUEST['error_description']; 
}
?>
