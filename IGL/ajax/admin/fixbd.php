<?php
ini_set('error_reporting', E_ALL);
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
include_once('../../includes/config.php');
include_once('../../model/mysql.class.php');
include_once('../../includes/functions.php');

$query = "SELECT playerid, birthday FROM player";

//Connect to the db
$db = new mysql(MYSQL_DATABASE);
$db->ExecuteSQL($query);

//If theres a return
if($db->iRecords()) {
    $result = $db->ArrayResults();
    foreach($result as $r) {
    if (strstr($r['birthday'], '-') !== false) {
     $birthday = explode('-', $r['birthday']);
     if (strlen($birthday[0]) > 0 && strlen($birthday[1]) > 0 && strlen($birthday[2]) > 0) {
     if (strlen($birthday[0]) != 4) {
         if (strlen($birthday[2]) != 4) {
             if ($birthday[2] < 14) {
                $birthday[2] = "20".$birthday[2];
             } else {
                $birthday[2] = "19".$birthday[2];
             }
         }
         if (strlen($birthday[0]) != 2) {
             $birthday[0] = "0".$birthday[0];
         }
         if (strlen($birthday[1]) != 2) {
             $birthday[1] = "0".$birthday[1];
         }
         $newbd = $birthday[2]."-".$birthday[0]."-".$birthday[1];
         $db->Update('player', array('birthday' => $newbd), array('playerid' => $r['playerid']));
     }
    }
    }
}
foreach($result as $r) {
     if (strstr($r['birthday'], '/') !== false) {
     $birthday = explode('/', $r['birthday']);
     if (strlen($birthday[0]) > 0 && strlen($birthday[1]) > 0 && strlen($birthday[2]) > 0) {
     if (strlen($birthday[0]) != 4) {
         if (strlen($birthday[2]) != 4) {
             if ($birthday[2] < 14) {
                $birthday[2] = "20".$birthday[2];
             } else {
                $birthday[2] = "19".$birthday[2];
             }
         }
         if (strlen($birthday[0]) != 2) {
             $birthday[0] = "0".$birthday[0];
         }
         if (strlen($birthday[1]) != 2) {
             $birthday[1] = "0".$birthday[1];
         }
         $newbd = $birthday[2]."-".$birthday[0]."-".$birthday[1];
         $db->Update('player', array('birthday' => $newbd), array('playerid' => $r['playerid']));
     }
    }
    }
}
foreach($result as $r) {
     if (strstr($r['birthday'], '/') === false && strstr($r['birthday'], '-') === false) {
         if (strlen($r['birthday']) == 8) {
            $b = str_split($r['birthday']);
            $newbd = $b[0].$b[1]."-".$b[2].$b[3]."-".$b[4].$b[5].$b[6].$b[7];
            $db->Update('player', array('birthday' => $newbd), array('playerid' => $r['playerid']));
         }
     }
}
}
?>
