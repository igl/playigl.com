<?php
/**
 * Creates a new forum post
 * @author Shaun Delaney <sdelaney@playigl.com>
 * @return JSON Success
 * @copyright Copyright 2013 PlayIGL.com
 */
include_once('../../includes/config.php');
include_once('../../model/mysql.class.php');
include_once('../../includes/functions.php');
include_once('../../includes/pusher.php');
include_once('../../includes/notify.php');
include_once('../../includes/forums.php');

$return = array("success" => false, "error" => "Please log in to use this feature.");

if (isLoggedIn()) {
  $forums = new Forums();
  $return = $forums->doAction();

  if ($return['success']) {
    if ($return['op']) {
      pushMessage(
        array("player-{$return['op']}"), array($return['op_msg'])
      );
    }
    if ($return['msg']) {
      pushMessage(
        array("player-{$_SESSION['playerid']}"), array($return['msg'])
      );
    }
  } else {
    pushMessage(
      array("player-{$_SESSION['playerid']}"), array($return['error'])
    );
  }
}

returnJSON($return);

?>