<?php
header('Cache-Control: no-cache, must-revalidate');
header('Content-type: application/json');

require('../../includes/config.php');
require('../../includes/functions.php');
require('../../model/mysql.class.php');


$db = new mysql(MYSQL_DATABASE);

$return = array('continents' => array(), 'regions' => array());
$query = "SELECT DISTINCT(`continent`) FROM server_locations";
$db->ExecuteSQL($query);
$return['continents'] = $db->ArrayResults();

$query = "SELECT DISTINCT(`primary_region`) as region FROM server_locations";
$db->ExecuteSQL($query);
$return['regions'] = $db->ArrayResults();

echo prettyJSON(json_encode($return));
?>
