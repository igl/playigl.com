<?php

/**
 * This page handles join requests. Accepts or deny via Ajax from the dashboard 
 */
require('../../includes/config.php');
require('../../includes/functions.php');
require('../../model/mysql.class.php');
include('../../model/activity.class.php');
include('../../model/history.class.php');

$activity = new activity();

if( isset($_SESSION['playerid']) && $_SESSION['playerid'] > 0 )
{
    if(isset($_REQUEST['id']) && is_numeric($_REQUEST['id']) && isset($_REQUEST['action']) && in_array($_REQUEST['action'], array('accept','deny','cancel')))
    {
        $db = new mysql(MYSQL_DATABASE);

        $id = (int)$_REQUEST['id'];
        
        $query = "
        SELECT
            `challengee`.`game` as team_game,
            `challengee`.`captain` as challengee_captain,
            `challengee`.`alternate` as challengee_alternate,
            `challenger`.`captain` as challenger_captain,
            `challenger`.`alternate` as challenger_alternate,
            `ladder_challenge`.`challengee` as team_challenge_away,
            `ladder_challenge`.`challenger` as team_challenge_home,
            `ladder_challenge`.`map` as team_challenge_map,
            `ladder_challenge`.`time` as team_challenge_time
        FROM
            `ladder_challenge`
        JOIN
            `team` as challengee ON `challengee`.`id` = `ladder_challenge`.`challengee`
        JOIN
            `team` as challenger ON `challenger`.`id` = `ladder_challenge`.`challenger`
        WHERE
            `ladder_challenge`.`id` = '{$id}'";
        $db->ExecuteSQL($query);
        if( $db->iRecords() )
        {
            $doc = $db->ArrayResult();
            $challengee_captains[] = $doc['challengee_captain'];
            $challengee_captains[] = $doc['challengee_alternate'];
            
            $challenger_captains[] = $doc['challenger_captain'];
            $challenger_captains[] = $doc['challenger_alternate'];
            
            if(in_array($_SESSION['playerid'], $challenger_captains))
            {
                if( $_REQUEST['action'] == 'cancel' )
                {
                    if($db->Delete('ladder_challenge', array('id' => $id)))
                    {
                         $return = array('success' => true);
                    }
                    else
                    {
                         $return = array('success' => false, 'error' => 'Error removing request.');
                    }
                }
            }
            elseif(in_array($_SESSION['playerid'], $challengee_captains))
            {
                if( $_REQUEST['action'] == 'deny' )
                {
                    if( $db->Update('ladder_challenge', array('deleted' => 1), array('id' => $id)) )
                    {
                         $return = array('success' => true);
                    }
                    else
                    {
                         $return = array('success' => false, 'error' => 'Error removing request.');
                    }
                }
                if( $_REQUEST['action'] == 'accept' )
                {
                    $teams = array($doc['team_challenge_home'],$doc['team_challenge_away']);
                    
                    
                    $query = "
                        SELECT 
                            `schedule`.`id`
                        FROM 
                            `schedule`
                        WHERE
                            (`schedule`.`officialdate` = {$doc['team_challenge_time']} OR `schedule`.`scheduleddate` = {$doc['team_challenge_time']} )
                        AND
                            ((`home` IN (". join(',', $teams) .")) OR (`away` IN (". join(',', $teams) .")))";

                    $db->ExecuteSQL($query);
                    if( !$db->iRecords() )
                    {
                        
                        
                        /*
                         * Added by darryl
                         */
                        
                        //Get home locations
                        $q = "SELECT `primary`, `alternate` FROM `server_locations` WHERE `primary` = '{$doc['team_challenge_away']}' LIMIT 1";

                        $db->ExecuteSQL($q);
                        $l = $db->ArrayResult();
                        $alts = explode('|', $l['alternate']);
                        foreach ($alts as $k => $v)
                            $alts[$k + 1] = $v;
                        $alts[0] = $l['primary'];
                        $locals['home'] = $alts;


                        //Get away locations
                        $q = "SELECT `primary`, `alternate` FROM `server_locations` WHERE `primary` = '{$doc['team_challenge_home']}' LIMIT 1";
                        $db->ExecuteSQL($q);
                        $l = $db->ArrayResult();
                        $alts = explode('|', $l['alternate']);
                        foreach ($alts as $k => $v)
                            $alts[$k + 1] = $v;
                        $alts[0] = $l['primary'];
                        $locals['away'] = $alts;


                        //Here we intersect the team locations to determine which servers are best for both teams
                        $locations = array_values(array_intersect($locals['home'], $locals['away']));
                        
                        
                        
                        
                        
                        //Get our server data
                        $reserve_server = array();
                        $reserved = false;
                        $last_location = end(array_keys($locations));
                        foreach ($locations as $k => $l) {

                            $reserve_server = $match_server->reserve($doc['team_game'], 'ladder_challenge', $l, $doc['team_challenge_time']);
                            if ($reserve_server['success']) {
                                $reserved = true;
                                break;
                            }
                            if ($k == $last_location)
                                unset($server_data);
                        }
                        
                        
                        /*
                         * Added by darryl
                         */

                        if($reserved)
                        {
                            if($db->Insert(array(
                            'game' => $doc['team_game'],
                            'home' => $doc['team_challenge_home'],
                            'away' => $doc['team_challenge_away'],
                            'map' => $doc['team_challenge_map'],
                            'officialdate' => $doc['team_challenge_time'],
                            'reservation_id' => $reserve_server['reservation_id']

                            ), 'schedule'))
                            {
                                $db->Update('ladder_challenge', array('deleted' => 1), array('id' => $id));
                                $return = array('success' => true);
                            }
                            else
                            {
                                $return = array('success' => false, 'error' => 'Sorry, there was an error accepting the match.'); // this should never happen
                            }
                        }
                        else
                        {
                            $return = array('success' => false, 'error' => $reserve_server['message']); 
                        }
                    }
                    else
                    {
                        $return = array('success' => false, 'error' => 'One or more of the teams have a conflicting match for this time.');
                    }
                }
            }
            else
            {
                $return = array('success' => false, 'error' => 'Sorry, only team captains have this access.'); // this should never happen
            }
        }
        else
        {
            $return = array('success' => false, 'error' => 'Sorry, this request no longer exists.'); 
        }
    }
    else
    {
        $return = array('success' => false, 'error' => 'Sorry, some parameters are missing. Please try again.');
    }
}
else
{
    $return = array('success' => false, 'error' => 'Sorry, you need to be logged in to perform this action.');
}

header('Content-type: application/json');
echo json_encode($return);
