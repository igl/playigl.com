<?php

/**
 * This script will return a list of leagues available for a game
 * @author Steve Rabouin
 * @copyright Copyright 2012 PlayIGL.com
 * @global $_GET['id'] is player id to return invites for
 */

header('Cache-Control: no-cache, must-revalidate');
header('Content-type: application/json');

require('../../includes/config.php');
require('../../includes/functions.php');
require('../../model/mysql.class.php');

$return = array();

if(isset($_GET['id']))
{
    $return['success'] = true;

    $id = (int)$_GET['id'];
    $db = new mysql(MYSQL_DATABASE);
    
    $query = "
            SELECT 
                `league`.`id`,
                `league`.`name`,
                `league`.`skill`
            FROM 
                `league`
            WHERE 
                `league`.`status` = 1 AND
                `league`.`game` = {$id}
    ";
    $db->ExecuteSQL($query);
    // so we are in a team, let's look further.
    if($db->iRecords())
    {
        $return['leagues'] = array();
        $leagues = $db->ArrayResults();
        foreach($leagues as $doc)
        {
            $return['leagues'][] = $doc;
        }
    }
}
else
{
    $return['success'] = false;
    $return['error'] = 'Invalid game specified.';
}

prettyJSON(json_encode($return));