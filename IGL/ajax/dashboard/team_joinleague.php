<?php

/**
 * This page handles join requests. Accepts or deny via Ajax from the dashboard 
 */
require('../../includes/config.php');
require('../../model/mysql.class.php');
include('../../model/activity.class.php');
$activity       = new activity();

if( isset($_SESSION['playerid']) && $_SESSION['playerid'] > 0 )
{
    if(isset($_REQUEST['league']) && is_numeric($_REQUEST['league']) && isset($_REQUEST['team']) && is_numeric($_REQUEST['team']))
    {
        $db = new mysql(MYSQL_DATABASE);

        $league = (int)$_REQUEST['league'];
        $team = (int)$_REQUEST['team'];
        
        // make sure we have enough players to join this league
        
        $teamplayers = 0;
        $query = "SELECT count(`player`) as `players` FROM `roster` WHERE `team` = '{$team}'";
        $db->ExecuteSQL($query);
        if($db->iRecords)
        {
            $teamplayers = $db->ArrayResult();
            $teamplayers = $teamplayers['players'];
        }
        else
        {
            $teamplayers = 0;
        }

        $query = "
            SELECT 
                `game`.`id`,
                `game`.`teamsize`,
                `league`.`currentseason`
            FROM
                `league`
                LEFT JOIN `game` on `league`.`game` = `game`.`id`
            WHERE
                `league`.`id` = {$league}
        ";
        $db->ExecuteSQL($query);
        if($db->iRecords)
        {
            $leagueinfo = $db->ArrayResult();
            $teamsize = $leagueinfo['teamsize'];
            $game = $leagueinfo['id'];
            $currentseason = $leagueinfo['currentseason'];
            
            if($teamplayers >= $teamsize)
            {
                // make sure we're a captain/alternate for this team.
                $query = "SELECT `captain`, `alternate` FROM `team` WHERE id = '{$team}'";
                $db->ExecuteSQL($query);
                if($db->iRecords)
                {
                    $captains = $db->ArrayResult();
                    if($_SESSION['playerid'] == $captains['captain'] || $_SESSION['playerid'] == $captains['alternate'])
                    {
                        $return['success'] = true;
                        $db->update('team', array('league' => $league), array('id' => $team));
                        $query = "
                            INSERT INTO `standings` 
                                (`game`, `league`, `team`, `season`) 
                            VALUES 
                                ('{$game}', '{$league}', '{$team}', '{$currentseason}');
                        ";
                        $db->ExecuteSQL($query);
                        
                        //Update activity feed
                        $activity->addEvent('joined league', '', $team, $league);
                    }
                    else
                    {
                        $return['success'] = false;
                        $return['error'] = "Sorry, only team captains/alternates can perform this function.";
                    }

                }
                else
                {
                    $return['success'] = false;
                    $return['error'] = 'No captain found for this team.';
                }
            }
            else
            {
                $return['success'] = false;
                $return['error'] = "Sorry, this team doesn't have enough players to join this league.";
            }            
        }
        else
        {
            $return['success'] = false;
            $return['error'] = "Sorry, this league doesn't appear to exist.";
        }
    }
    else
    {
        $return = array('success' => false, 'message' => 'Sorry, some parameters are missing. Please try again.'); // this should never happen
    }
}
else
{
    $return = array('success' => false, 'message' => 'Sorry, you need to be logged in to perform this action.');
}

header('Content-type: application/json');
echo json_encode($return);
