<?php
/*
 * @copyright 2012 PlayIGL.com
 * @author Darryl Allen
 * This file retrieves the activity feed
 */

header('Cache-Control: no-cache, must-revalidate');
header('Content-type: application/json');


include('../../model/mysql.class.php');
require('../../includes/config.php');
require('../../includes/functions.php');

if( isset($_SESSION['playerid']) && $_SESSION['playerid'] > 0 )
{
    $team_results = array();
    $player_results = array();
    $db = new mysql(ACTIVITY);
    $query = "
        SELECT 
            DISTINCT(`event`.`id`), 
            `event`.`event`,
            `event`.`player_id` AS player1_id,
            `player_1`.`username` AS player1_name,
            `player_1`.`avatar` AS player1_image,
            `event`.`player2_id` AS player2_id,
            `player_2`.`username` AS player2_name,
            `player_2`.`avatar` AS player2_image,
            `team_1`.`id` AS team1_id,
            `team_1`.`name` AS team1_name,
            `team_1`.`logo` AS team1_image,
            `team_2`.`id` AS team2_id,
            `team_2`.`name` AS team2_name,
            `team_2`.`logo` AS team2_image,
            `event`.`timestamp`
        FROM 
            `".ACTIVITY."`.`event`
        LEFT JOIN
            `".MYSQL_DATABASE."`.`player` as player_1 ON `player_1`.`playerid` = `event`.`player_id`
        LEFT JOIN
            `".MYSQL_DATABASE."`.`player` as player_2 ON `player_2`.`playerid` = `event`.`player2_id`
        LEFT JOIN
            `".MYSQL_DATABASE."`.`team` as team_1 ON `team_1`.`id` = `event`.`team_id`
        LEFT JOIN
            `".MYSQL_DATABASE."`.`team` as team_2 ON `team_2`.`id` = `event`.`team2_id`
        WHERE 
            `event`.`team_id`
        IN (SELECT teamid FROM ".ACTIVITY.".follow WHERE teamid > 0 AND followerid = '{$_SESSION['playerid']}')
            OR
            `event`.`team2_id`
        IN (SELECT teamid FROM ".ACTIVITY.".follow WHERE teamid > 0 AND followerid = '{$_SESSION['playerid']}')    
        ORDER BY timestamp DESC LIMIT 25";

    $db->ExecuteSQL($query);

    if($db->iRecords())  
    {
        $team_results = $db->ArrayResults();
        foreach($team_results as $key => $value)
        {
            $team_results[$key]['following'] = 'team';
            $team_results[$key]['when'] = ago($value['timestamp']);
        }
    }
    $query = "
        SELECT 
            DISTINCT(`event`.`id`), 
            `event`.`event`,
            `event`.`player_id` AS player1_id,
            `player_1`.`username` AS player1_name,
            `player_1`.`avatar` AS player1_image,
            `event`.`player2_id` AS player2_id,
            `player_2`.`username` AS player2_name,
            `player_2`.`avatar` AS player2_image,
            `team_1`.`id` AS team1_id,
            `team_1`.`name` AS team1_name,
            `team_1`.`logo` AS team1_image,
            `team_2`.`id` AS team2_id,
            `team_2`.`name` AS team2_name,
            `team_2`.`logo` AS team2_image,
            `event`.`timestamp`
        FROM 
            `".ACTIVITY."`.`event`
        LEFT JOIN
            `".MYSQL_DATABASE."`.`player` as player_1 ON `player_1`.`playerid` = `event`.`player_id`
        LEFT JOIN
            `".MYSQL_DATABASE."`.`player` as player_2 ON `player_2`.`playerid` = `event`.`player2_id`
        LEFT JOIN
            `".MYSQL_DATABASE."`.`team` as team_1 ON `team_1`.`id` = `event`.`team_id`
        LEFT JOIN
            `".MYSQL_DATABASE."`.`team` as team_2 ON `team_2`.`id` = `event`.`team2_id`
        WHERE 
            `event`.`player_id`
        IN (SELECT playerid FROM ".ACTIVITY.".follow WHERE playerid > 0 AND followerid = '{$_SESSION['playerid']}')
            OR
            `event`.`player2_id`
        IN (SELECT playerid FROM ".ACTIVITY.".follow WHERE playerid > 0 AND followerid = '{$_SESSION['playerid']}')

        ORDER BY timestamp DESC LIMIT 25";

        $db->ExecuteSQL($query);

    if($db->iRecords())  
    {
        $player_results = $db->ArrayResults();
        foreach($player_results as $key => $value)
        {
            $player_results[$key]['following'] = 'player';
            $player_results[$key]['when'] = ago($value['timestamp']);
        }
    }
    
    $r = array_merge($team_results, $player_results);
    


    function cmp($a, $b)
    {
        if ($a == $b) {
            return 0;
        }
        return ($a > $b) ? -1 : 1;
    }
    
    //trim the blank values
    foreach( $r as $k => $e )
    {
        if( $e['player1_id'] > 0 )
        {
            //Set first set of data to display
            $r[$k]['link1'] = "/player/{$r[$k]['player1_name']}";
            $r[$k]['name1'] = $r[$k]['player1_name'];
            $r[$k]['image1'] = $r[$k]['player1_image'];
        }
        
        if( $e['player2_id'] > 0 )
        {
            //Set second set of data to display
            $r[$k]['link2'] = "/player/{$r[$k]['player2_name']}";
            $r[$k]['name2'] = $r[$k]['player2_name'];
            $r[$k]['image2'] = $r[$k]['player2_image'];
        }

        if( $e['team1_id'] > 0 )
        {
            if (strlen($r[$k]['name1']) == 0  ) //Check if the first name has been se
            {
                //Set first set of data to display
                $r[$k]['link1'] = "/team/{$r[$k]['team1_id']}-{$r[$k]['team1_name']}";
                $r[$k]['name1'] = $r[$k]['team1_name'];
                $r[$k]['image1'] = $r[$k]['team1_image'];
            }
            else
            {
                //Set first set of data to display
                $r[$k]['link2'] = "/team/{$r[$k]['team1_id']}-{$r[$k]['team1_name']}";
                $r[$k]['name2'] = $r[$k]['team1_name'];
                $r[$k]['image2'] = $r[$k]['team1_image'];
            }
        }

        if( $e['team2_id'] > 0 )
        {
            //Set second set of data to display
            $r[$k]['link2'] = "/team/{$r[$k]['team2_id']}-{$r[$k]['team2_name']}";
            $r[$k]['name2'] = $r[$k]['team2_name'];
            $r[$k]['image2'] = $r[$k]['team2_image'];
        }
        
        unset($r[$k]['player1_id']);
        unset($r[$k]['player1_name']);
        unset($r[$k]['player1_image']);
        unset($r[$k]['player2_id']);
        unset($r[$k]['player2_name']);
        unset($r[$k]['player2_image']);
        unset($r[$k]['team1_id']);
        unset($r[$k]['team1_name']);
        unset($r[$k]['team1_image']);
       # unset($r[$k]['team2_id']);
        unset($r[$k]['team2_name']);
        unset($r[$k]['team2_image']);
        
        
        
        if( isset($r[$k]['name1']) && $r[$k]['name1'] == $_SESSION['username'] )
            $r[$k]['name1'] = 'You';
        if( isset($r[$k]['name2']) && $r[$k]['name2'] == $_SESSION['username'] )
            $r[$k]['name2'] = 'You';
    }
    
    
    usort($r, "cmp");
    
    if(is_array($r))
    {
        $return = array('success' => true, 'events' => $r);
    }
    else
    {
        $return = array('success' => false, 'error' => 'Sorry, data could not be compiled');
    }
}
else
{
    $return = array('success' => false, 'error' => 'You ware not logged in.');
}
prettyJSON(json_encode($return));
