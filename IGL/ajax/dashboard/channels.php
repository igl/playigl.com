<?php
/**
 * Retrieves Pusher channels user subscribes to
 * @author Shaun Delaney <sdelaney@playigl.com>
 * @return JSON list of channels
 * @copyright Copyright 2012-13 PlayIGL.com
 */

include_once('../../includes/config.php');
include_once('../../model/mysql.class.php');
include_once('../../includes/functions.php');

//Default error message
$return = array("success" => true, "channels" => array('global'));

//Variables
$user_id = isset($_SESSION) ? $_SESSION['playerid'] : null;

if (isLoggedIn() && is_numeric($user_id)) {
    if (!empty($_SESSION['channels'])) {
        foreach ($_SESSION['channels'] as $channel)
            $return['channels'][] = $channel;
    } else {
        $return['channels'][] = "player-".$user_id;
    }
}

header('Cache-Control: no-cache, must-revalidate');
returnJSON($return);
?>