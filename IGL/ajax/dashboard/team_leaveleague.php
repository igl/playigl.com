<?php

/**
 * This script will leave a league if the player is captain or alternate
 * @author Steve Rabouin
 * @copyright Copyright 2012 PlayIGL.com
 */

header('Cache-Control: no-cache, must-revalidate');
header('Content-type: application/json');

require('../../includes/config.php');
require('../../includes/functions.php');
require('../../model/mysql.class.php');

$return = array();

if( isset($_SESSION['playerid']) && $_SESSION['playerid'] > 0 )
{
    if(isset($_GET['league']) && isset($_GET['team']))
    {
        $league = (int)$_GET['league'];
        $team = (int)$_GET['team'];
        $db = new mysql(MYSQL_DATABASE);
        
        // get team information to ensure we can leave it (i.e. we are not captain, and if we're alternate need to set it to 0)
        $query = "
                SELECT 
                    `team`.`id`,
                    `team`.`captain`,
                    `team`.`alternate`
                FROM 
                    `team`
                WHERE 
                    `team`.`id` = '{$team}'
                LIMIT 1
        ";   
        $db->ExecuteSQL($query);
        if($db->iRecords())
        {
            $teaminfo = $db->ArrayResult();
            if($_SESSION['playerid'] == $teaminfo['captain'] || $_SESSION['playerid'] == $teaminfo['alternate'])
            {
                $db->update('team', array('league' => null), array('id' => $team));
                $db->ExecuteSQL("DELETE FROM `standings` WHERE `team` = '{$team}' AND `league` = '{$league}'");
                $return['success'] = true;
            }
            else
            {
                $return['success'] = false;
                $return['error'] = "You need to be a captain for this team to leave a league.";
            }
        }
        else
        {
            $return['success'] = false;
            $return['error'] = "This team does not exist.";
        }
    }
    else
    {
        $return['success'] = false;
        $return['error'] = "Missing parameters.";
    }
}
else
{
    $return['success'] = false;
    $return['error'] = "Sorry, you need to be logged in to use this feature.";
}

prettyJSON(json_encode($return));
