<?php

/*
 * @copyright 2012 PlayIGL.comT
 * @author Darryl Allen
 * This file retrieves ladder info
 * http://localhost/api/ladders
 * http://localhost/api/ladder/1 
 * http://localhost/api/ladder/teams/1
 */

header('Cache-Control: no-cache, must-revalidate');
header('Content-type: application/json');


include('../../model/mysql.class.php');
include('../../model/activity.class.php');
include('../../model/matchservers.class.php');
require('../../includes/config.php');
require('../../includes/functions.php');

$db = new mysql(MYSQL_DATABASE);


if ($_POST['remove']) {
    if (isLoggedIn()) {
        if (isset($_POST['id']) && is_numeric($_POST['id'])) {
            $r = $db->ExecuteSQL("DELETE `ladder_availability` FROM
                        `ladder_availability`
                    JOIN 
                        `team` ON `team`.`id` = `ladder_availability`.`team_id`
                    WHERE
                    {$_SESSION['playerid']} IN (
                            `team`.`captain`,
                            `team`.`alternate`
                    )
                    AND `ladder_availability`.`id` = {$_POST['id']}");
            if ($r) {
                $return = array('success' => true);
            } else {
                $return = array('success' => false, 'error' => 'Unknown error.');
            }
        } else {
            $return = array('success' => false, 'error' => 'Missing match id.');
        }
    } else {
        $return = array('success' => false, 'error' => 'You must be logged in.');
    }
} else {



    //Set our availability/listing id to 0;
    $listing_id = 0;

    if (isset($_POST['id']) && is_numeric($_POST['id'])) {
        $db->ExecuteSQL("SELECT id, map, time, team_id as challenged, tier_id as ladder FROM ladder_availability WHERE id = {$_POST['id']}");
        if ($db->iRecords()) {
            $r = $db->ArrayResult();
            $_REQUEST['map'] = array($r['map']);
            $_REQUEST['offset'] = 0;
            $_REQUEST['ladder'] = $r['ladder'];
            $_REQUEST['challenged'] = $r['challenged'];
            $_REQUEST['time'] = date('Y-m-d H:i:s', $r['time']);
            $listing_id = $_POST['id'];
        }
    }

    if (isset($_SESSION['playerid']) && $_SESSION['playerid'] > 0) {
        if (isset($_REQUEST['map']) && isset($_REQUEST['offset']) && isset($_REQUEST['ladder']) && is_numeric($_REQUEST['ladder']) && isset($_REQUEST['challenged']) && is_numeric($_REQUEST['challenged']) && isset($_REQUEST['time'])) {
            $ladder = (int) $_REQUEST['ladder'];
            $challenged = (int) $_REQUEST['challenged'];
            $time = (int) ISOtime($_REQUEST['time']);
            $time = $time - ($_REQUEST['offset'] * 60 * 60);
            $map = implode("|", $_REQUEST['map']);


            //Check if the user is a captain of a team in the ladder
            $query = "
            SELECT 
                `team`.`id` as team_id,
                `game`.`name` AS ladder_game_name,
                `game`.`id` AS ladder_game_id,
                `game`.`hostedservers` AS ladder_hosted_servers,
                `ladder_settings`.`livedate` AS ladder_livedate,
                `ladder_settings`.`match_length` AS ladder_match_length
            FROM
                `team`
            JOIN
                `ladder_teams` ON `ladder_teams`.`team_id` = `team`.`id`
            JOIN
                `ladder_settings` ON `ladder_settings`.`id` = `ladder_teams`.`settings_id`
            JOIN
                `game` ON `game`.`id` = `ladder_settings`.`game`
            WHERE
            `ladder_teams`.`tier_id` = {$ladder} AND
            {$_SESSION['playerid']} IN (`team`.`captain`,`team`.`alternate`)";

            $db->ExecuteSQL($query);
            if ($db->iRecords()) {
                $team_data = $db->ArrayResult();
                //Check if the ladder is live
                if (strlen($team_data['ladder_livedate']) == 10 && $team_data['ladder_livedate'] < time()) {
                    $team = $team_data['team_id'];

                    if ($team != $challenged) {
                        //Verify user isn't booking too far into the future (72hr limit)
                        $plus_one_hour = time() + 3600;
                        $now = floor($plus_one_hour / 3600) * 3600; //This rounds to the next hour.

                        if ($time < ($now + 259200)) {
                            //Check for conflicting challenges
                            $query = "
                        SELECT 
                            `ladder_challenge`.`id`
                        FROM 
                            `ladder_challenge`
                        WHERE
                            `ladder_challenge`.`deleted` = 0 AND 
                            (
                                (`ladder_challenge`.`challenger` = '{$team}' AND `ladder_challenge`.`ladder` = '{$ladder}' AND `ladder_challenge`.`time` = '{$time}')
                                    OR
                                (`ladder_challenge`.`challenger` = '{$challenged}' AND `ladder_challenge`.`ladder` = '{$ladder}' AND `ladder_challenge`.`time` = '{$time}')
                            )
                        ";

                            $db->ExecuteSQL($query);
                            if (!$db->iRecords()) {

                                //If no conflicting ladder matches, check for conflicting league matches
                                $query = "
                                SELECT 
                                    `schedule`.`id`
                                FROM
                                    `schedule`
                                WHERE
                                    {$team} IN (`schedule`.`home`,`schedule`.`away`) AND `schedule`.`officialdate` = {$time}";

                                $db->ExecuteSQL($query);
                                if (!$db->iRecords()) {
                                    $reserve_server = array();
                                    //Check if IGL is hosting servers
                                    if ($team_data['ladder_hosted_servers'] == 1) {
                                        $match_servers = new matchservers();
                                        //Get common locations for each team
                                        $locations = $match_servers->getCommonLocations($team, $challenged);
                                        if ($locations['success']) {
                                            $reserve_server = $match_servers->reserveServer($locations, $team_data['ladder_game_id'], 'ladder_challenge', $time, $team_data['ladder_match_length']);
                                            if (false == $reserve_server['success'])
                                                return json_encode(array('success' => false, 'error' => 'No servers avalable for this time slot'));
                                        }
                                        else {
                                            return json_encode(array('success' => false, 'error' => $locations['error']));
                                        }
                                    }

                                    if ($db->Insert(array(
                                                'challenger' => $team,
                                                'challengee' => $challenged,
                                                'time' => $time,
                                                'ladder' => $ladder,
                                                'map' => $map,
                                                'listing_id' => $listing_id,
                                                'reservation_id' => $reserve_server['reservation_id']
                                                    ), 'ladder_challenge')) {
                                        $arrayData = array('team_name' => $challenged, 'match_time' => $time, 'ladder' => $ladder);
                                        $return = array('success' => true, 'data' => $arrayData);
                                    } else {
                                        $return = array('success' => false, 'error' => 'An unknown error occured while challenging');
                                    }
                                } else {
                                    $return = array('success' => false, 'error' => 'You already have a league match scheduled for this time.');
                                }
                            } else {
                                $return = array('success' => false, 'error' => 'You already have pending challenges for this time.');
                            }
                        } else {
                            $return = array('success' => false, 'error' => 'You cannot book more than 4 hours ahead.');
                        }
                    } else {
                        $return = array('success' => false, 'error' => 'You cannot play a match versus yourself.');
                    }
                } else {
                    $return = array('success' => false, 'error' => "Ladder is not active until " . date('Y-m-d', $team_data['ladder_livedate']));
                }
            } else {
                $return = array('success' => false, 'error' => 'You are not the team captain.');
            }
        } else {
            $return = array('success' => false, 'result' => 'Sorry, some parameters are missing.');
        }
    } else {
        $return = array('success' => false, 'error' => 'You must be logged in to challenge teams.');
    }
}
prettyJSON(json_encode($return));