<?php

/**
 * This page handles server reservation.
 * Date/time to be passed as seconds. 
 * @author Darryl Allen
 * @copyright Copyright 2012 PlayIGL.com
 */
header('Cache-Control: no-cache, must-revalidate');
header('Content-type: application/json');

require('../../includes/config.php');
require('../../model/mysql.class.php');

#$usergroups = explode(",", $_SESSION['membergroupids']);
#if(in_array(10, $usergroups) || $_SESSION['usergroupid']==6)
#{
    if(isset($_REQUEST['comp_type']) && isset($_REQUEST['time']) && is_numeric($_REQUEST['time']) && isset($_REQUEST['game']) && is_numeric($_REQUEST['game']) )
    {

        if($_REQUEST['time'] >= time())
        {
            $db = new mysql(MYSQL_DATABASE);

            $game       = (int)$_REQUEST['game'];
            $time       = (int)$_REQUEST['time'];
            $comp_type  = $_REQUEST['comp_type'];
            $matchstart = $time+1;  //need to add a second to allow matches to start at end date.
            $matchend   = $time+7199; // need to add just shy of 2 hours to allow for the start date not to exactly equal the end date

            $sql = "SELECT id FROM `matchservers` WHERE `game` = {$game} AND competition_type = {$comp_type}";

            $db->executeSQL($sql);

            if($db->iRecords())
            {

            foreach($db->ArrayResults() as $value)
            {
                $matchserver_ids[] = $value['id'];
            }

                //Check # of servers in use for this game where StartTime > `to` and EndTime < `From`
                $sql = "
                    SELECT 
                        DISTINCT(server_id) as used
                    FROM 
                        matchreserve
                    INNER JOIN 
                        `matchservers` on `matchservers`.`id` = `matchreserve`.`server_id`
                    WHERE 
                        matchservers.game = {$game} AND
                        ({$matchstart} BETWEEN matchreserve.from AND matchreserve.to OR
                        {$matchend} BETWEEN matchreserve.from AND matchreserve.to)";
                
                $db->ExecuteSQL($sql);

                foreach($db->ArrayResults() as $value)
                {
                    $server_ids[] = $value['used'];
                }
                if(count($matchserver_ids) > $db->iRecords())
                {
                    if(empty($server_ids))
                        $server_ids = array(0);

                    $sql = "
                        SELECT 
                            id, 
                            ip, 
                            port 
                        FROM 
                            matchservers 
                        WHERE 
                            game = {$game} AND id NOT IN (". join(',', $server_ids) .") 
                        ORDER BY RAND() LIMIT 1";
                            
                    $db->ExecuteSQL($sql);

                    if($db->iRecords())
                    {
                        $doc = $db->ArrayResult();
                        $server_id = $doc['id'];
                        $server_ip = $doc['ip'];
                        $server_port = $doc['port'];
                        
                        $starttime = round( $time / 3600 ) * 3600; //round start time to nearest 15 minutes. (this should have been done already)
                        $finishtime = round( $matchend / 3600 ) * 3600; //round start time to nearest 15 minutes.

                        if($db->Insert(array(
                            'server_id' => $server_id,
                            'from'      => $starttime,
                            'to'        => $finishtime
                        ), 'matchreserve'))
                        {
                            $reservationid = $db->lastId();
                            $return = array('success' => true, 'server_id'=> $server_id, 'server_ip'=> $server_ip, 'server_port'=> $server_port, 'reservation_id' => $reservationid, 'message' =>  'Match server has successfully been reserved.');
                        }
                        else
                        {
                            $return = array('success' => false, 'message' => 'Match server could not be reserved.');
                        }
                    }
                    else
                    {
                        $return = array('success' => false, 'message' => 'No match servers available for the requested time.');
                    }
                }
                else
                {
                    $return = array('success' => false, 'message' => 'No match servers available for the requested time.');
                }
            }
            else
            {
                $return = array('success' => false, 'message' => 'There are no servers available for the game requested.');
            }
        }
        else
        {
            $return = array('success' => false, 'message' => 'Sorry, you can only reserve servers in the future');
        }
    }
    else
    {
        $return = array('success' => false, 'message' => 'Sorry, it looks like some parameters are missing');
    }
    
#}
#else
#{
#    $return = array('success' => false, 'message' => $_SESSION['playerid'].'Sorry, you do not have permissions to perform this action');
#}

echo json_encode($return);
?>
