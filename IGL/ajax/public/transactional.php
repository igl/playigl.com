<?php

session_start();

header('Cache-Control: no-cache, must-revalidate');
header('Content-type: application/json');

require('../../includes/config.php');
require('../../includes/functions.php');
require('../../model/mysql.class.php');
require('../../model/tournament.class.php');
require('../../model/player.class.php');
require('../../model/activity.class.php');
require('../../model/history.class.php');
require('../../model/image.class.php');
require('../../model/league.class.php');
require('../../model/game.class.php');
require('../../model/team.class.php');
require('../../model/match.class.php');

$player = new player();
$tournament = new tournament();
$game = new game();
$match = new match();

$return['success'] = true;
$return['response'] = '';

if (isset($_SESSION['email'])) {
    if (isset($_GET['action']) && !empty($_GET['action'])) {
        switch ($_GET['action']) {
            /* *********************************************
             * Event Starting
             * *********************************************/
            case 'EventStarting':
                if (isset($_GET['eventId'])) {
                    $t = $tournament->get($_GET['eventId']);
                    $return['response'] = triggerEmail('Event Starting', $_SESSION['email'], '{"eventName":"' . $t['name'] . '"}');
                } else {
                    $return['success'] = false;
                    $return['error'] = "Variable 'eventId' undefined";
                }
                break;
            /* *********************************************
             * Match Scheduled
             * *********************************************/
            case 'MatchScheduled':
                if (isset($_GET['matchId'])) {
                    $m = $match->matchdata($_GET['matchId']);
                    $return['response'] = triggerEmail('Match Scheduled', $_SESSION['email'], '{}');
                } else {
                    $return['success'] = false;
                    $return['error'] = "Variable 'matchId' undefined";
                }
                break;
            default:
                    $return['success'] = false;
                    $return['error'] = "Specified 'action' invalid";
                break;
        }
    } else {
        $return['success'] = false;
        $return['error'] = "No 'action' sepcified";  
    }
} else {
    $return['success'] = false;
    $return['error'] = "Not Authorized";
}

function triggerEmail($eventName, $email, $deltaJson) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_USERPWD, '3cpime0e8acmyuairj02bq4h5:479tjys8vppv4bamnsh8ho88y');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($ch, CURLOPT_URL, 'https://api.userfox.com/v1/send.json');
    curl_setopt($ch, CURLOPT_POSTFIELDS, array('addr' => $email,
        'name' => $eventName,
        'delta' => $deltaJson));
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}

echo prettyJSON(json_encode($return));

?>
