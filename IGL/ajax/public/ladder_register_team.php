<?php
/*
 * @copyright 2012 PlayIGL.comT
 * @author Darryl Allen
 * This file retrieves ladder info
 * http://localhost/api/ladders
 * http://localhost/api/ladder/1 
 * http://localhost/api/ladder/teams/1
 */

header('Cache-Control: no-cache, must-revalidate');
header('Content-type: application/json');


include('../../model/mysql.class.php');
include('../../model/activity.class.php');
require('../../includes/config.php');
require('../../includes/functions.php');

if( isset($_SESSION['playerid']) && $_SESSION['playerid'] > 0 )
{
    if ( isset($_REQUEST['ladder']) && is_numeric($_REQUEST['ladder']) && isset($_REQUEST['time']) && is_numeric($_REQUEST['time']) && in_array($_REQUEST['action'], array('register','remove')) ) 
    {
        $tier_id = (int)$_REQUEST['ladder'];
        $time = (int)$_REQUEST['time'];

        $db = new mysql(MYSQL_DATABASE);
        
        
        //Check if the user is a captain of a team in the ladder
        $query = "
            SELECT 
                `active_teams`.`team_id`,
                `active_ladders`.`ladder_game_id`,
                `active_ladders`.`ladder_game_name`,
                `active_ladders`.`ladder_match_length`,
                `active_ladders`.`ladder_live_date`,
                `active_ladders`.`ladder_teamsize`
            FROM
                `active_teams`
            JOIN
                `ladder_teams` ON `ladder_teams`.`team_id` = `active_teams`.`team_id`
            JOIN
                `active_ladders` ON `active_ladders`.`ladder_id` = `ladder_teams`.`tier_id`
            WHERE
                `ladder_teams`.`tier_id` = {$tier_id} AND {$_SESSION['playerid']} IN (`team_captain_id`,`team_alternate_id`) ";

        $db->ExecuteSQL($query);
        if($db->iRecords())
        {
            $team_data = $db->ArrayResult();
            //Check if the ladder is live
            if( strlen($team_data['ladder_live_date']) == 10 && $team_data['ladder_live_date'] < time() )
            {
                $team = $team_data['team_id'];
                $ladder_name = $team_data['ladder_name'];
                $game_name = $team_data['game_name'];
                $game_id = $team_data['ladder_game_id'];

                //check if team has enough players on their roster to join
                $query = "SELECT id FROM roster WHERE team = {$team}";
                $db->ExecuteSQL($query);
                if( $db->iRecords() >= $team_data['ladder_teamsize'] )
                {
                    if($_REQUEST['action'] == 'remove')
                    {
                        $query = "SELECT map FROM ladder_pool_votes WHERE team = {$team} AND time = {$time}";
                        if( $db->ExecuteSQL($query) && $db->iRecords() )
                        {
                            $r = $db->ArrayResult();
                            $vote = "#votes_{$time}_{$r['map']}";
                        }
                        else
                        {
                            $vote = false;
                        }
                        if($db->Delete('ladder_pool', array(
                            'team' => $team,
                            'ladder' => $tier_id,
                            'time' => $time,
                        )) && $db->Delete('ladder_pool_votes',array('team' => $team, 'time' => $time)))
                        {
                            $return = array('success' => true,'vote' => $vote);
                        }
                        else
                        {
                            $return = array('success' => false, 'error' => 'Unable to remove team');
                        }
                    }
                    if($_REQUEST['action'] == 'register')
                    {
                        //Verify user isn't booking too far into the future (4hr limit)
                        $plus_one_hour = time() + 3600;
                        $now = floor($plus_one_hour / 3600) * 3600; //This rounds to the next hour.

                        if(($time < ($now+14401)) && ($time > time()))
                        {
                            //Check for conflicting ladder matches
                            $query = "
                            SELECT 
                                `ladder_pool`.`id`
                            FROM 
                                `ladder_pool`
                            WHERE
                                `ladder_pool`.`team` = '{$team}' AND `ladder_pool`.`ladder` = '{$tier_id}' AND `ladder_pool`.`time` = '{$time}'";
                            $db->ExecuteSQL($query);
                            if(!$db->iRecords())
                            {
                                //If no conflicting ladder matches, check for conflicting league matches
                                $query = "
                                SELECT 
                                    `schedule`.`id`
                                FROM
                                    `schedule`
                                WHERE
                                    `schedule`.`home` = '{$team}' OR `schedule`.away` = '{$team}' AND `schedule`.`officialdate` = {$time}";

                                //Verify there's no outstanding challenges for this time
                                $db->ExecuteSQL($query);
                                if(!$db->iRecords())
                                {
                                    $query = "
                                        SELECT
                                        `ladder_challenge`.`id`
                                        FROM
                                        `ladder`_challenge`
                                        WHERE
                                        `ladder_challenge`.`challenger` = '{$team}' AND `ladder_challenge`.`time` = '{$time}'
                                        ";
                                     $db->ExecuteSQL($query);
                                     if(!$db->iRecords())
                                     {
                                         $query = "
                                             SELECT 
                                                id
                                             FROM 
                                                `matchservers`
                                             WHERE 
                                                `matchservers`.`game` = {$game_id} 
                                                    AND `matchservers`.`competition_type` = 'ladder' 
                                                    AND `matchservers`.`deleted` = 0";
                                         $db->ExecuteSQL($query);
                                         $avail_servers = $db->iRecords();
                                         
                                         $query = "
                                             SELECT 
                                                `ladder_pool`.`id`
                                             FROM
                                                `ladder_pool`
                                             JOIN
                                                `ladder` ON `ladder`.`id` = `ladder_pool`.`ladder`
                                             WHERE
                                                `ladder`.`game` = {$game_id}";
                                         $num_teams_listed = $db->iRecords();
                                         
                                         if( floor($avail_servers*2/$team_data['ladder_match_length']) >= $num_teams_listed )
                                         {
                                            if($db->Insert(array(
                                                'ladder' => $tier_id,
                                                'team' => $team,
                                                'time' => $time,
                                                ), 'ladder_pool'))
                                                {
                                                    $activity = new activity();
                                                    $activity->addEvent("signed up for a <a href='/ladder/{$tier_id}/{$game_name}/{$ladder_name}}'>ladder</a> match", false, $team);
                                                    $return = array('success' => true);
                                                }
                                                else
                                                {
                                                    $return = array('success' => false, 'error' => 'Unable to add team to pool');
                                                }
                                         }
                                         else
                                         {
                                             $return = array('success' => false, 'error' => 'All slots for this time are filled.');
                                         }                                         
                                     }
                                     else
                                     {
                                         $return = array('success' => false, 'error' => 'You have an outstanding challenge at this time.');
                                     }                            
                                }
                                else
                                {
                                    $return = array('success' => false, 'error' => 'You already have a league match scheduled for this time');
                                }
                            }
                            else
                            {
                                $return = array('success' => false, 'error' => 'You already have a ladder match scheduled for this time');
                            }
                        }
                        else
                        {
                            $return = array('success' => false, 'error' => 'You cannot book more than 4 hours ahead.');
                        }
                    }
                }
                else
                {
                    $return = array('success' => false, 'error' => "Your team must have {$team_data['ladder_teamsize']} members to join.");
                }
            }
            else
            {
                $return = array('success' => false, 'error' => "Ladder is not active until ".date('Y-m-d', $team_data['ladder_live_date']));
            }
        }
        else
        {
            $return = array('success' => false, 'error' => 'You are not the team captain');
        }
    }
    else
    {
        $return = array('success' => false, 'result' => 'Sorry, some parameters are missing');
    }
}
else
{
    $return = array('success' => false, 'error' => 'You must be logged in to view pool times');
}
header('Content-type: application/json');
prettyJSON(json_encode($return));
