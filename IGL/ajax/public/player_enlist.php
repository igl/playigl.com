<?php
/**
 * This page handles recruit requests. Recruit or revoke via Ajax from view/freeagents.php 
 * @author Darryl Allen
 * @copyright Copyright 2012 PlayIGL.com
 */
require('../../includes/config.php');
require('../../model/mysql.class.php');
include('../../model/activity.class.php');
require('../../includes/functions.php');

$activity = new activity();

if (isset($_SESSION['playerid']) && $_SESSION['playerid'] > 0) {
  if (isset($_REQUEST['player']) && is_numeric($_REQUEST['player']) && isset($_REQUEST['game']) && is_numeric($_REQUEST['game']) && isset($_REQUEST['action']) && in_array($_REQUEST['action'], array('enlist', 'remove'))) {
    $player = (int) $_REQUEST['player'];
    $game = (int) $_REQUEST['game'];
    $db = new mysql(MYSQL_DATABASE);

    $query = "SELECT `id` FROM `game` WHERE `id` = {$game} AND steamauth = 1";
    $db->ExecuteSQL($query);
    $steam_auth = $db->iRecords();

    if (is_numeric($_SESSION['communityid']) || $steam_auth == 0) {
      // We need to make sure we're denying one of our own request and not something fishy.
      if ($_REQUEST['player'] == $_SESSION['playerid']) {
        if ($_REQUEST['action'] == 'enlist') {
          // let's make sure we're not already in a team, otherwise we can't accept this request.
          $sql = "
            SELECT 
                `freeagents`.`id` 
            FROM 
                `roster`, `freeagents`
            WHERE 
                (`freeagents`.`player` = {$player} AND `freeagents`.`game` = {$game}) 
            OR  
                (`roster`.`player` = {$player} AND `roster`.`game` = {$game}) 
            LIMIT 1";
          $db->executeSQL($sql);
          if ($db->iRecords()) {
            $return = array('success' => false, 'error' => 'Sorry, you have already enlisted, or are already on a team.');
          } else {
            $sql = "INSERT INTO freeagents (`player`, `username`, `game`) VALUES ('{$player}','{$_SESSION['username']}','{$game}')";
            if ($db->executeSQL($sql)) {
              $db->ExecuteSQL(
                "SELECT * FROM game_character_category WHERE game_id = {$game}"
              );
                
              $return = array('success' => true, 'error' => $_SESSION['username'] . ' is now a free agent.', 'roles' => $db->ArrayResults());
            } else {
              $return = array('success' => false, 'error' => 'Player could not be added as a free agent.');
            }
          }
        } else if ($_REQUEST['action'] == 'remove') {
          $db->delete('freeagents', array('player' => $player, 'game' => $game));
          $db->delete('inviterequests', array('player' => $player, 'game' => $game));
          $db->delete('game_character', array('player_id' => $player, 'game_id' => $game));
          $return = array('success' => true, 'error' => 'Free agent successfully removed.');
        }
      } else {
        $return = array('success' => false, 'error' => 'Sorry, you don\'t have access to this request.'); // this should never happen                
      }
    } else {
      $return = array('success' => false, 'error' => 'Sorry, you must first authenticate with steam.');
    }
  } else {
    $return = array('success' => false, 'error' => 'Sorry, some parameters are missing. Please try again.'); // this should never happen
  }
} else {
  $return = array('success' => false, 'error' => 'Sorry, you need to be logged in to perform this action.');
}

returnJSON($return);