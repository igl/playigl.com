<?php

/*
 * @copyright 2012 PlayIGL.comT
 * @author Darryl Allen
 * This file retrieves ladder info
 * http://localhost/api/ladders
 * http://localhost/api/ladder/1
 * http://localhost/api/ladder/teams/1
 */

header('Cache-Control: no-cache, must-revalidate');
header('Content-type: application/json');


include('../../model/mysql.class.php');
include('../../model/activity.class.php');
require('../../includes/config.php');
require('../../includes/functions.php');

$return = array('success' => false, 'error' => 'unknown');

if (isset($_SESSION['playerid']) && $_SESSION['playerid'] > 0) {
    if (isset($_REQUEST['ladder_id']) && is_numeric($_REQUEST['ladder_id'])) {
        $ladder_id = (int) $_REQUEST['ladder_id'];
        $db = new mysql(MYSQL_DATABASE);

        //get game from ladder_id
        $query = "SELECT ladder_game_id, ladder_settings_id FROM active_ladders WHERE ladder_id = {$ladder_id}";
        $db->ExecuteSQL($query);
        if ($db->iRecords()) {
            $doc = $db->ArrayResult();
            $game_id = $doc['ladder_game_id'];
            $ladder_settings_id = $doc['ladder_settings_id'];
            //Check if the user is a captain of a team
            $query = "
                SELECT
                    `active_teams`.`team_id`,
                    count(`roster`.`player`) as `team_roster_size`,
                    `active_teams`.`team_continent`
                FROM
                    `active_teams`
                LEFT JOIN
                    `roster` ON `roster`.`team` = `active_teams`.`team_id`
                WHERE
                    `team_game_id` = {$game_id} AND {$_SESSION['playerid']} IN (`team_captain_id`,`team_alternate_id`)";

            $db->ExecuteSQL($query);
            if ($db->iRecords()) {
                $doc = $db->ArrayResult();
                $team_id = $doc['team_id'];
                $team_roster_size = $doc['team_roster_size'];
                $team_continent = $doc['team_continent'];
                //Check if this team is in the ladder already...
                $query = "
                    SELECT
                        team_id
                    FROM
                        ladder_teams
                    WHERE
                        team_id = {$team_id} AND settings_id = {$ladder_settings_id}";
                $db->ExecuteSQL($query);
                if ($db->iRecords()) {
                    //Remove team from the ladder...
                    $db->Delete('ladder_teams', array('team_id' => $team_id, 'settings_id' => $ladder_settings_id));
                    $return['success'] = true;
                    $return['in_ladder'] = false;
                    $return['team_id'] = $team_id;
                    unset($return['error']);
                } else {

                    $query = "
                    SELECT
                        max(`ladder_id`) as ladder_id,
                        ladder_teamsize,
                        ladder_registration_date,
                        ladder_continent
                    FROM
                        `active_ladders`
                    WHERE
                        `ladder_id` = {$ladder_id}"; //Double check the user isn't submitting fake data
                    $db->ExecuteSQL($query);
                    if ($db->iRecords()) {
                        $ladder_data = $db->ArrayResult();
                        if (time() >= $join_ladder['ladder_registration_date']) {
                            if ($ladder_data['ladder_id'] > 0) {
                                if ($ladder_data['ladder_continent'] == $team_continent) {
                                    if ($team_roster_size >= $ladder_data['ladder_teamsize']) {
                                        //Add team to the ladder.
                                        $insert = $db->Insert(array('team_id' => $team_id, 'tier_id' => $ladder_id, 'settings_id' => $ladder_settings_id), 'ladder_teams');
                                        if ($insert) {
                                            $return['success'] = true;
                                            $return['in_ladder'] = true;
                                            $return['team_id'] = $team_id;
                                            unset($return['error']);
                                        } else {
                                            $return['error'] = "Unknown error while adding team to ladder.";
                                        }
                                    } else {
                                        $return['error'] = "Your team must have {$ladder_data['ladder_teamsize']} players to join.";
                                    }
                                } else {
                                    $return['error'] = "Only teams located in {$ladder_data['ladder_continent']} are able to register";
                                }
                            } else {
                                $return['error'] = "Unable to locate ladder id.";
                            }
                        } else {
                            $return['error'] = "Registrations are not yet open.";
                        }
                    } else {
                        $return['error'] = "Unable to locate an active ladder with ID {$ladder_id}."; //This should never happen
                    }
                }
            } else {
                $return['error'] = "You are not a team captain.";
            }
        } else {
            $return['error'] = "Unable to locate ladder.";
        }
    } else {
        $return['error'] = 'Sorry, some parameters are missing.';
    }
} else {
    $return['error'] = 'You must be logged in to view pool times.';
}
header('Content-type: application/json');
prettyJSON(json_encode($return));