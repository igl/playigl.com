<?php

/*
 * @copyright 2012 PlayIGL.comT
 * @author Darryl Allen
 * This file retrieves ladder info
 * http://localhost/api/ladders
 * http://localhost/api/ladder/1 
 * http://localhost/api/ladder/teams/1
 */

header('Cache-Control: no-cache, must-revalidate');
header('Content-type: application/json');

require('../../includes/config.php');
include('../../model/mysql.class.php');
require('../../includes/functions.php');

$db = new mysql(MYSQL_DATABASE);


//dive into multi-dimensional array to see if the user is a captain
function isCaptain($array, $key = 1, $val = 'Leader') {
    if (is_array($array)) {
        foreach ($array as $item)
            if (isset($item[$key]) && $item[$key] == $val)
                return true;
    }
    return false;
}

//Check if the user exists based on community id
function userExists($community_id) {
    global $db;
    $db->ExecuteSQL("SELECT playerid, username FROM player WHERE communityid = {$community_id}");
    if ($db->iRecords()) {
        $r = $db->ArrayResult();
        return array('exists' => true, 'username' => $r['username'], 'player_id' => $r['playerid']);
    }
    return array('exists' => false);
}

//check if a player is on a team
function onTeam($player_id, $game_id) {
    global $db;
    $db->ExecuteSQL("SELECT * FROM roster WHERE game = {$game_id} AND player = {$player_id}");
    if ($db->iRecords()) {
        return true;
    }
    return false;
}

//send invite to a player
function sendInvite($player_id, $team_id) {
    global $db;

    $db->ExecuteSQL("SELECT game FROM team WHERE id = {$team_id}");
    if ($db->iRecords()) {
        $r = $db->ArrayResult();
        if ($db->Insert(array(
                        'game' > $r['game'],
                        'team' => $team_id,
                        'player' => $player_id
                        ), 'inviterequests')) {
            return true;
        }
    }
    return false;
}

//Create an IGL user account
function createUser($username, $community_id) {
    global $db;
    if (is_numeric($community_id)) {
        
        
        $db->ExecuteSQL("SELECT username FROM player WHERE username = '{$username}'");
        if($db->iRecords()) {
            $username .= rand(0,10);
        }
        
        if ($db->Insert(array(
                    'username' => $username,
                    'communityid' => $community_id,
                    'joindate' => time()
                        ), "player")) {
            $player_id = $db->lastId();

            //Get image from Steam..
            $xml = file_get_contents("http://steamcommunity.com/profiles/{$community_id}?xml=1");
            $step_one = explode('<avatarFull>', $xml);
            $step_two = explode('<![CDATA[', $step_one[1]);
            $step_three = explode(']]></avatarFull>', $step_two[1]);
            $save = "../../img/player/{$player_id}_avatar.png";
            file_put_contents($save, file_get_contents($step_three[0]));

            $db->Update("player", array('avatar' => "/img/player/{$player_id}_avatar.png"), array('playerid' => $player_id));
            return array('success' => true, 'player_id' => $player_id, 'username' => $username);
        }
    } else {
        echo "{$community_id} is not numeric";
    }

    return array('success' => false);
}

if (isset($_REQUEST['clan_id']) && is_numeric($_REQUEST['clan_id']) && isset($_REQUEST['team_id']) && is_numeric($_REQUEST['team_id'])) {

    //import roster
    $ugc_roster = json_decode(file_get_contents("http://www.ugcleague.com/team_roster_json.cfm?clan_id={$_REQUEST['clan_id']}"), true);
    $msg = array();
    $team_id = $_REQUEST['team_id'];

    if (isCaptain($ugc_roster['DATA'])) {
        if (is_array($ugc_roster['DATA']))
            foreach ($ugc_roster['DATA'] as $p) {
                $user_exists = userExists($p[5]);
                if ($user_exists['exists']) {
                    $msg[] = "User {$p['0']} has an IGL account under the username '{$user_exists['username']}'.";
                    if (onTeam) {
                        $msg[] = "{$user_exists['username']} is already on another team.";
                    } else {
                        sendInvite($user_exists['player_id'], $team_id);
                        $msg[] = "Invite sent to {$user_exists['username']}.";
                    }
                } else {
                    $create_user = createUser($p[0], $p[5]);
                    if ($create_user['success']) {
                        $msg[] = "IGL Account created for {$create_user['username']} with Community ID {$p[5]}. User may now log into IGL using Steam.";
                        sendInvite($create_user['player_id'], $team_id);
                        $msg[] = "Invite sent to {$create_user['username']}.";
                    } else {
                        $msg[] = "Unable to create an IGL account for {$p[0]}.";
                    }
                }
            }
        $return = array('success' => true, 'messages' => $msg);
    } else {
        $return = array('success' => false, 'error' => 'You are not captain of the team being imported');
    }
} else {
    $return = array('success' => false, 'error' => 'Missing clan ID');
}

prettyJSON(json_encode($return));