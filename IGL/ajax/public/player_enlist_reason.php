<?php
/**
 * This page handles recruit requests. Recruit or revoke via Ajax from view/freeagents.php 
 * @author Darryl Allen
 * @copyright Copyright 2012 PlayIGL.com
 */
require('../../includes/config.php');
require('../../model/mysql.class.php');
include('../../model/activity.class.php');
require('../../includes/functions.php');

$activity = new activity();

if (isset($_SESSION['playerid']) && $_SESSION['playerid'] > 0) {
  if (isset($_REQUEST['player']) && is_numeric($_REQUEST['player']) && isset($_REQUEST['game']) && is_numeric($_REQUEST['game'])) {
    $player = (int) $_REQUEST['player'];
    $game = (int) $_REQUEST['game'];
    $db = new mysql(MYSQL_DATABASE);

    $query = "SELECT `id` FROM `game` WHERE `id` = {$game} AND steamauth = 1";
    $db->ExecuteSQL($query);
    $steam_auth = $db->iRecords();

    if (is_numeric($_SESSION['communityid']) || $steam_auth == 0) {

      // We need to make sure we're updating one of our own request and not something fishy.
      if ($_REQUEST['player'] == $_SESSION['playerid']) {
        if ($db->Update('freeagents', array('reason' => $_REQUEST['why']), array('player' => $player, 'game' => $game))) {
          $b = $db->Insert(array("player_id" => $player, "game_id" => $game, "game_category_id" => $_REQUEST['role']), 'game_character');
          if ($b)
            $return = array('success' => true);
          else
            $return = array('success' => false, 'error' => 'Debug me');
        } else {
          $return = array('succes' => false, 'error' => 'Error while setting recruit blurb.');
        }
      } else {
        $return = array('success' => false, 'error' => 'Sorry, you don\'t have access to this request.'); // this should never happen                
      }
    } else {
      $return = array('success' => false, 'error' => 'Sorry, you must first authenticate with steam.'); // this should never happen
    }
  } else {
    $return = array('success' => false, 'error' => 'Sorry, some parameters are missing. Please try again.');
  }
} else {
  $return = array('success' => false, 'error' => 'Sorry, you need to be logged in to perform this action.');
}

returnJSON($return);