<?php
/**
 * Fetches player data
 * @author Darryl Allen <dallen@palyigl.com>
 * @author Shaun Delaney <sdelaney@playigl.com>
 * @return JSON Following data, or fail flags
 * @copyright Copyright 2013 PlayIGL.com
 */

include_once('../../includes/config.php');
include_once('../../model/mysql.class.php');
include_once('../../includes/functions.php');
include_once('../../includes/pusher.php');
include_once('../../includes/notify.php');

//Default error message
$return = array("success" => false, "error" => "Please log in to use this feature.");

//Ensure we're logged in
if(isLoggedIn()) {
    $actor_id   = $_SESSION['playerid'];
    $actor_type = 'player';
    $subject_id = $_REQUEST['subject'];
    $subject_type = $_REQUEST['subject_type'];
    $verb = "follow";

    //Reject non-numeric IDs
    if (!is_numeric($actor_id) || !is_numeric($subject_id)) {
        $return['error'] = "Invalid data passed.";
        returnJSON($return);
    }
    
    notify($actor_id, $actor_type, $subject_id, $subject_type, $verb);

    
}

returnJSON($return, true);
