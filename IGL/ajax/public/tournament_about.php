<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
include_once('../../includes/config.php');
include_once('../../model/mysql.class.php');
include_once('../../includes/functions.php');

include('../../model/tournament.class.php');
include('../../model/game.class.php');
include('../../model/team.class.php');
include('../../model/league.class.php');
include('../../model/image.class.php');
include('../../model/history.class.php');
include('../../model/activity.class.php');
include('../../model/player.class.php');
$tournament = new tournament();
$player = new player();
$t = $tournament->get($_REQUEST['tournament_id']);
?>




<div class="info" style="font-size:14px;">
  <ul class="nav nav-tabs">
    <li class="active"><a href="#tab0" data-toggle="tab">General Information</a></li>
    <li><a href="#tab1" data-toggle="tab">Event Details</a></li>
    <li><a href="#tab2" data-toggle="tab">Rules</a></li>
  </ul>
  <div class="tab-content">
    <div class="tab-pane active" id="tab0">
        <div class="info">
                            <h4><?php echo $t['name']; ?></h4>
                            <ul class="info">
                                <li><strong>Event Start:</strong> <span class='localTime'><?php echo $t['eventstartdatetime']; ?></span></li>
                                <li><strong>Max Teams:</strong> <?php echo $t['bracket_size']; ?></li>
                                <li><strong>Wait List:</strong> <?php if ($t['wait_list_size'] == 0) { echo 'Not Enabled'; } else { echo 'Enabled'; } ?></li>
                            </ul>
                            <h4>Registration</h4>
                            <ul class="info">
                                <li><strong>Registration Opens:</strong> <span class='localTime'><?php echo $t['registrationstartdatetime']; ?></span></li>
                                <li><strong>Registration Closes:</strong> <span class='localTime'><?php echo $t['registrationenddatetime']; ?></span></li>
                            </ul>
                            <h4>Check In</h4>
                            <ul class="info">
                                <li><strong>Check In Start:</strong> <span class='localTime'><?php echo $t['checkinstartdatetime']; ?></span></li>
                                <li><strong>Check In End:</strong> <span class='localTime'><?php echo $t['checkinenddatetime']; ?></span></li>
                            </ul>
                            
        </div>
        
                         <div class="caster-wrap">
                             <h4 style="margin:30px 0px;">Commentators</h4>
                             <?php if ($t['caster1'] != '0') { ?>
                             <div class="caster">
                                 <?php $p = $player->get($t['caster1']); ?>
                                 <img style="width:75px; height:75px;" src="/img<?php echo $p['avatar']; ?>" class="pic" />
                                 <a href="#" class="user"><h3><?php echo $p['username']; ?></h3></a>
                                 <a href="#" class="twitter">@<?php echo $p['twitter']; ?></a>
                             </div>
                             <?php } ?>
                             <?php if ($t['caster2'] != '0') { ?>
                             <div class="caster">
                                 <?php $p = $player->get($t['caster2']); ?>
                                 <img style="width:75px; height:75px;" src="/img<?php echo $p['avatar']; ?>" class="pic" />
                                 <a href="#" class="user"><h3><?php echo $p['username']; ?></h3></a>
                                 <a href="#" class="twitter">@<?php echo $p['twitter']; ?></a>
                             </div>
                             <?php } ?>
                             <?php if ($t['caster3'] != '0') { ?>
                             <div class="caster">
                                 <?php $p = $player->get($t['caster3']); ?>
                                 <img style="width:75px; height:75px;" src="/img<?php echo $p['avatar']; ?>" class="pic" />
                                 <a href="#" class="user"><h3><?php echo $p['username']; ?></h3></a>
                                 <a href="#" class="twitter">@<?php echo $p['twitter']; ?></a>
                             </div>
                             <?php } ?>
                         </div>
    </div>
    <div class="tab-pane" id="tab1">
        <div class="info" style="font-size:14px;">
      <?php if ($t['details'] == "") {echo "No event details at this time."; } else {echo $t['details'];} ?>
        </div>
    </div>
    <div class="tab-pane" id="tab2">
        <div class="info" style="font-size:14px;">
      <?php if ($t['rules'] == "") {echo "No event rules at this time."; } else {echo $t['rules'];} ?>
        </div>
    </div>
  </div>
</div>
