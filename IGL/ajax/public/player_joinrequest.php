<?php

/**
 * This page handles join requests. Accepts or deny via Ajax from the dashboard 
 */
require('../../includes/config.php');
require('../../includes/functions.php');
require('../../model/mysql.class.php');
include('../../model/activity.class.php');
include('../../model/history.class.php');

$history = new history();
$activity = new activity();

if( isset($_SESSION['playerid']) && $_SESSION['playerid'] > 0 )
{
    if(isset($_REQUEST['team']) && is_numeric($_REQUEST['team']) )
    {
        $db = new mysql(MYSQL_DATABASE);

        $team = (int)$_REQUEST['team'];
        $slot = (int)$_REQUEST['slot'];
        
        $query = "SELECT id FROM team WHERE open = 1 AND id = '{$team}'";
        $db->ExecuteSQL($query);
        if($db->iRecords())
        {
            $sql = "
            SELECT 
                `id`
            FROM 
                `joinrequests`
            WHERE 
                `player` = '{$_SESSION['playerid']}' AND `team` = '{$team}'";
            $db->executeSQL($sql);
            if(!$db->iRecords())
            {
                echo "<div id='warning' width='90%'></div>";
                echo "<table class='table table-striped center'>";
                echo "<tr><td colspan='3'><h4>How would you like to join?</h4></td></tr>";
                echo "<tr><td><form action='' id='join_with_password' class='form form-horizontal' method='post' enctype='multipart/form-data'>
                    <div class='input-prepend input-append'>
                    <span class='add-on'><i class='icon-lock'></i></span>
                    <input class='span2' id='prependedInput' type='password' name='password' placeholder='Password...'>
                    <button type='button' id='join_button' class='btn' onclick='player_join_password({$team},{$_SESSION['playerid']},{$slot})'>JOIN</button>
                    </div>
                    </form>
                    </td>
                    <td><h2>OR</h2></td>
                    <td><form action='' id='join_via_request' class='form form-horizontal' method='post' enctype='multipart/form-data'><div class='input-append input-prepend'>
                    <span class='add-on'><i class='icon-envelope'></i></span><button type='button' class='btn' onclick='player_send_joinrequest({$team},{$_SESSION['playerid']})'>SEND REQUEST</button></form></td></tr>";

                echo "</table>";
            }
            else
            {
                echo "You've already requested to join this team."; // this should never happen
            }
        }
        else
        {
            echo "This team is not currently accepting join requests.";
        }
        
        
        
        
    }
    else
    {
        echo 'Sorry, some parameters are missing. Please try again.'; // this should never happen
    }
}
else
{
    echo 'Sorry, you need to be logged in to perform this action.';
}
