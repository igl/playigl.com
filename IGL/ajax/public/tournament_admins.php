<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
include_once('../../includes/config.php');
include_once('../../model/mysql.class.php');
include_once('../../includes/functions.php');

include('../../model/tournament.class.php');
include('../../model/game.class.php');
include('../../model/team.class.php');
include('../../model/league.class.php');
include('../../model/image.class.php');
include('../../model/history.class.php');
include('../../model/activity.class.php');
include('../../model/player.class.php');
$tournament = new tournament();
$player = new player();
$t = $tournament->get($_REQUEST['tournament_id']);
?>

                         <div class="caster-wrap">
                             <h2>Admins</h2>
                             <?php if ($t['admin1'] != '0') { ?>
                             <div class="caster">
                                 <?php $p = $player->get($t['admin1']); ?>
                                 <img src="/img<?php echo $p['avatar']; ?>" class="pic" />
                                 <a href="/player/<?php echo $p['playerid']; ?>" class="user"><h3><?php echo $p['username']; ?></h3></a>
                                 <a href="http://twitter.com/<?php echo $p['twitter']; ?>" class="twitter">@<?php echo $p['twitter']; ?></a>
                             </div>
                             <?php } ?>
                             <?php if ($t['admin2'] != '0') { ?>
                             <div class="caster">
                                 <?php $p = $player->get($t['admin2']); ?>
                                 <img src="/img<?php echo $p['avatar']; ?>" class="pic" />
                                 <a href="/player/<?php echo $p['playerid']; ?>" class="user"><h3><?php echo $p['username']; ?></h3></a>
                                 <a href="http://twitter.com/<?php echo $p['twitter']; ?>" class="twitter">@<?php echo $p['twitter']; ?></a>
                             </div>
                             <?php } ?>
                             <?php if ($t['admin3'] != '0') { ?>
                             <div class="caster">
                                 <?php $p = $player->get($t['admin3']); ?>
                                 <img src="/img<?php echo $p['avatar']; ?>" class="pic" />
                                 <a href="/player/<?php echo $p['playerid']; ?>" class="user"><h3><?php echo $p['username']; ?></h3></a>
                                 <a href="http://twitter.com/<?php echo $p['twitter']; ?>" class="twitter">@<?php echo $p['twitter']; ?></a>
                             </div>
                             <?php } ?>
                         </div>
