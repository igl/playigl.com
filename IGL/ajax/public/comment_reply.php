<?php
/*
 * @copyright 2012 PlayIGL.comT
 * @author Darryl Allen
 * This file retrieves ladder info
 * http://localhost/api/ladders
 * http://localhost/api/ladder/1
 * http://localhost/api/ladder/teams/1
 */

header('Cache-Control: no-cache, must-revalidate');
header('Content-type: application/json');


include('../../model/mysql.class.php');
include('../../model/activity.class.php');
require('../../includes/config.php');
require('../../includes/functions.php');

if (isset($_SESSION['playerid']) && $_SESSION['playerid'] > 0) {
  if (isset($_REQUEST['parentid']) && is_numeric($_REQUEST['parentid']) && isset($_REQUEST['reply'])) {
    $parent_id = (int) $_REQUEST['parentid'];
    $page_id = (int) $_REQUEST['page_id'];
    $db = new MySQL(MYSQL_DATABASE);
    $query = "SELECT page, pageid FROM comments WHERE id = {$parent_id}";
    $db->ExecuteSQL($query);
    $r = $db->ArrayResult();
    $page = $r['page'];
    $page_id = $r['pageid'];

    $vars = array(
      'posterid' => $_SESSION['playerid'],
      'comment' => strip_tags($_REQUEST['reply']),
      'page' => $page,
      'pageid' => $page_id,
      'parentid' => $parent_id,
      'dateposted' => time()
    );
    if ($db->Insert($vars, 'comments')) {
      $vars['avatar'] = $_SESSION['avatar'];
      $vars['username'] = $_SESSION['username'];
      $vars['postid'] = $db->lastId();
      $vars['comment'] = parseContent($vars['comment']);
      $return = array('success' => true, 'result' => $vars);
    } else {
      $return = array('success' => false, 'error' => 'Unable to submit post.');
    }
  } else {
    $return = array('success' => false, 'error' => 'Unable to submit post.');
  }
} else {
  $return = array('success' => false, 'error' => 'You must be logged in to post comments.');
}
header('Content-type: application/json');
prettyJSON(json_encode($return));
