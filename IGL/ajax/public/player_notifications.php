<?php

/**
 * This page fetch's a user's notfications
 * @author Darryl Allen
 * Copyright IGL 2013 
 */
require('../../includes/config.php');
require('../../includes/functions.php');
require('../../model/mysql.class.php');

if( isset($_SESSION['playerid']) && $_SESSION['playerid'] > 0 )
{
    $player = (int)$_REQUEST['player'];
    if($player==$_SESSION['playerid'])
    {
        $db = new mysql(MYSQL_DATABASE);
        $query = "SELECT activity.id, activity.event, activity.player, activity.team, activity.league 
            FROM activity
            JOIN activity_player ON activity_player.activityid = activity.id
            WHERE activity_player.playerid = '{$player}' ORDER BY activity.id DESC LIMIT 10";   
        $db->ExecuteSQL($query);


        if($db->iRecords())  
        {
            $response = array();
            foreach($db->ArrayResults() as $key => $value)
            {
                $response[$key]['event'] = $value['event'];

                //player or team is always the primary image
                if($value['player'] > 0)
                {
                    $sql = "SELECT avatar, username, firstname, lastname FROM player WHERE playerid = '{$value['player']}'";
                    if($db->ExecuteSQL($sql))
                    {
                        $doc = $db->ArrayResult();
                        $response[$key]['first_name'] = "<a href='/player/{$doc['username']}'>{$doc['firstname']} {$doc['username']} {$doc['lastname']}</a>";
                        $response[$key]['first_image'] = "<a href='/player/{$doc['username']}'><img class='activity img-rounded' src='/img{$doc['avatar']}'></a>";
                    }
                }
                elseif($value['team'] > 0)
                {
                    $sql = "SELECT logo, name FROM team WHERE id = '{$value['team']}'";
                    if($db->ExecuteSQL($sql))
                    {
                        $doc = $db->ArrayResult();
                        $response[$key]['first_name'] = "<a href='/team/{$value['team']}-{$doc['name']}'>{$doc['name']}</a>";
                        $response[$key]['first_image'] = "<a href='/team/{$value['team']}-{$doc['name']}'><img class='activity img-rounded' src='/img{$doc['logo']}'></a>";
                    }
                }
                elseif($value['league'] > 0)
                {
                    $sql = "SELECT logo as league_image, league.name as name FROM game
                        JOIN league ON league.game=game.id";
                    if($db->ExecuteSQL($sql))
                    {
                        $doc = $db->ArrayResult();
                        $response[$key]['first_name'] = "<a href='/league/{$value['league']}-{$doc['name']}'>{$doc['name']}</a>";
                        $response[$key]['first_image'] = "<a href='/league/{$value['league']}-{$doc['name']}'><img class='activity img-rounded' src='/img{$doc['league_image']}'></a>";
                    }
                }

                //if league is numberic, it's mostlikely the secondary. If it's empty, team is secondary.
                if($value['league'] > 0)
                {
                    $sql = "SELECT logo as league_image, league.name as name FROM game
                        JOIN league ON league.game=game.id";
                    if($db->ExecuteSQL($sql))
                    {
                        $doc = $db->ArrayResult();
                        $response[$key]['second_name'] = "<a href='/league/{$value['league']}-{$doc['name']}'>{$doc['name']}</a>";
                        $response[$key]['second_image'] = "<a href='/league/{$value['league']}-{$doc['name']}'><img class='activity img-rounded' src='/img{$doc['league_image']}'></a>";
                    }
                }
                elseif($value['team'] > 0)
                {
                    $sql = "SELECT logo, name FROM team WHERE id = '{$value['team']}'";
                    if($db->ExecuteSQL($sql))
                    {
                        $doc = $db->ArrayResult();
                        $response[$key]['second_name'] = "<a href='/team/{$value['team']}-{$doc['name']}'>{$doc['name']}</a>";
                        $response[$key]['second_image'] = "<a href='/team/{$value['team']}-{$doc['name']}'><img class='activity img-rounded' src='/img{$doc['logo']}'></a>";
                    }
                }

            }
            $sql = "SELECT count(playerid) AS new FROM activity_player WHERE playerid = '{$player}' AND viewed = '0'";
            $db->ExecuteSQL($sql);
            $doc = $db->ArrayResult();
            $return['success'] = true;
            $return['new'] = $doc['new'];
            $return['event'] = $response;
        }
        else
        {
            $return = array('success' => false, 'response' => 'Sorry, data could not be compiled');
        }
    }
    else
    {
        $return = array('success' => false, 'result' => 'Sorry, you cannot view other users\' notifications');
    }
}
else
{
    $return = array('success' => false, 'result' => 'Sorry, you must be logged in to view notifictions');
}
header('Content-type: application/json');
echo json_encode($return);
