<?php
/*
 * @copyright 2012 PlayIGL.comT
 * @author Darryl Allen
 * This file allows captains to vote for the map they want to play in a ladder pool
 */

header('Cache-Control: no-cache, must-revalidate');
header('Content-type: application/json');


include('../../model/mysql.class.php');
require('../../includes/config.php');
require('../../includes/functions.php');




//Add + 1 to the vote count for the team


//Verify user is logged in
if( isset($_SESSION['playerid']) && $_SESSION['playerid'] > 0 )
{
    if( isset($_POST['ladder']) && is_numeric($_POST['ladder']) && isset($_POST['time']) && is_numeric($_POST['time']) )
    {
        $time = (int)$_POST['time'];
        $tier_id = (int)$_POST['ladder'];
        $player_id = $_SESSION['playerid'];
        if( $time > time() )
        {
            $db = new mysql();
            //Verify the user is also a valid captain, of a team in the ladder
            $query = "SELECT `active_teams`.`team_id`, `active_teams`.`team_game_id` 
            FROM
                `active_teams`
            JOIN
                `ladder_teams` ON `ladder_teams`.`team_id` = `active_teams`.`team_id`
            WHERE
                `ladder_teams`.`tier_id` = {$tier_id} AND {$player_id} IN (`team_captain_id`,`team_alternate_id`) ";
            if( $db->ExecuteSQL($query) && $db->iRecords() )
            {
                $team = $db->ArrayResult();
                //Verify the team is registered in this pool
                $query ="SELECT id FROM ladder_pool WHERE team = {$team['team_id']} AND ladder = {$tier_id} AND time ={$time}";
                if( $db->ExecuteSQL($query) && $db->iRecords() )
                {    
                
                    //Get maps available for this ladder @ this time
                    $query = "SELECT map FROM ladder_map_schedule JOIN active_ladders ON active_ladders.ladder_game_id = ladder_map_schedule.game WHERE time = {$time} AND active_ladders.ladder_id = {$tier_id}";
                    if( $db->ExecuteSQL($query) && $db->iRecords() )
                    {
                        $available_maps = array();
                        foreach( $db->ArrayResults() as $map )
                        {
                            $available_maps[] = $map['map'];
                        }
                        //verify map requested is in the array of maps avail
                        if(in_array($_POST['map'],$available_maps))
                        {
                            //check if this team has voted. If so, update their vote. If not, add their vote
                            $query = "SELECT id FROM ladder_pool_votes WHERE team = {$team['team_id']} AND time = {$time}";

                            if( $db->ExecuteSQL($query) && $db->iRecords() ) //check if team has previously voted
                            {
                                //Update the team's vote
                                if($db->Update('ladder_pool_votes', array('map' => $_POST['map']), array('team' => $team['team_id'],'time' => $time)))
                                {
                                    $return = array('success' => true, 'mesage' => "Vote updated to {$_POST['map']}.");
                                }
                                else
                                {
                                    $return = array('success' => false, 'error' => 'Error updating vote.'); 
                                }

                            }
                            else //tema hasn't voted yet
                            {
                                //Add team's vote
                                if($db->Insert(array('game' => $team['team_game_id'], 'map' => $_POST['map'], 'team' => $team['team_id'],'time' => $time), 'ladder_pool_votes'))
                                {
                                    $return = array('success' => true, 'mesage' => "Vote added for map: {$_POST['map']}.");
                                }
                                else
                                {
                                    $return = array('success' => false, 'error' => 'Error adding vote.'); 
                                }
                            }
                        }
                        else
                        {
                            $return = array('success' => false, 'error' => 'That map does not appear to be a valid option.');
                        }
                    }
                    else
                    {
                        $return = array('success' => false, 'error' => 'There does not appear to be any available maps for ths time.');
                    }
                }
                else
                {
                    $return = array('success' => false, 'error' => 'Team is not registered for this pool.');
                }
                
            }
            else
            {
                $return = array('success' => false, 'error' => 'You do not appear to be a team captain of a team participating in this ladder.');
            }
        }
        else
        {
            $return = array('success' => false, 'error' => 'You can\'t vote for maps in the past.');
        }
    }
    else
    {
        $return = array('success' => false, 'error' => 'Some paramerters are missing.');
    }
}
else
{
    $return = array('success' => false, 'error' => 'You must be logged in to view pool times.');
}
header('Content-type: application/json');
prettyJSON(json_encode($return));