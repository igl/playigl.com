<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
include_once('../../includes/config.php');
include_once('../../model/mysql.class.php');
include_once('../../includes/functions.php');

include('../../model/tournament.class.php');
$tournament = new tournament();
$t = $tournament->get($_REQUEST['tournament_id']);
?>

<table class="table table-stats table-condensed table-bordered table-striped table-sortable">
    <thead><tr><th colspan="5">Event Schedule</th></tr><tr><th>Round</th><th>Teams</th><th>Match Times</th><th>Format</th><th>Maps</th></tr></thead>
    <tr><td>Semi Final</td><td>4</td><td>Mon 8/27/13 9:00pm EDT</td><td>bo5</td><td>SND-Carbon<br/>CTF-Dome<br/>SND-Arkaden<br/>CTF-Lockdown<br/>SND-Underground<td/></tr>
    <tr><td>Final</td><td>2</td><td>Mon 8/27/13 9:00pm EDT</td><td>bo7</td><td>SND-Carbon<br/>CTF-Dome<br/>SND-Arkaden<br/>CTF-Lockdown<br/>SND-Underground<td/></tr>

</table>
