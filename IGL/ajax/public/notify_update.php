<?php

/*
 * @copyright 2012 PlayIGL.
 * @author Darryl Allen
 */

require('../../includes/config.php');
require('../../model/mysql.class.php');

$player = (int)$_REQUEST['player'];
if( isset($_SESSION['playerid']) && $_SESSION['playerid'] > 0 )
{
    if(isset($_REQUEST['player']) && is_numeric($_REQUEST['player']))
    {
        if($_SESSION['playerid'] == $player)
        {
            $db = new mysql(MYSQL_DATABASE);
            $db->Update('activity_player', 
                    array(
                        'viewed' => 1
                    ), array(
                        'viewed' => 0,
                        'playerid' => $player
                    ));
        
            $return = array('success' => true, 'message' => 'Notifications updated.');
        }
    }
    else
    {
        $return = array('success' => false, 'message' => 'Missing paramters.');
    }
}
else 
{
    $return = array('success' => false, 'message' => 'Error: Not logged in.');    
}
header('Content-type: application/json');
header('Cache-Control: no-cache, must-revalidate');
echo json_encode($return);
?>
