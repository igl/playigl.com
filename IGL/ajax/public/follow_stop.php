<?php

/**
 * This page handles tracking requests.  
 * @author Darryl Allen
 * @copyright Copyright 2013 PlayIGL.com
 */
require('../../includes/config.php');
require('../../model/mysql.class.php');

if( isset($_SESSION['playerid']) && $_SESSION['playerid'] > 0 )
{
    if( isset($_REQUEST['follower']) && is_numeric($_REQUEST['follower']) && isset($_REQUEST['id']) && is_numeric($_REQUEST['id']) && in_array($_REQUEST['follow'], array('player','team')) )
    {
        $db = new MySQL(ACTIVITY);

        $follower = (int)$_REQUEST['follower'];
        $id   = (int)$_REQUEST['id'];
        
        if( $_REQUEST['follow'] == 'player' )
        {
            if( $id != $follower )
            {
                if( $follower == $_SESSION['playerid'] )
                {
                    if( $db->Delete('follow', array('playerid' => $id, 'followerid' => $follower)) )
                    {
                        $return = array('success' => true);           
                    }
                    else
                    {
                        $return = array('success' => false, 'error' => 'Error writing to database.'); // this should never happen                
                    }
                }
                else
                {
                    $return = array('success' => false, 'error' => 'Sorry, you cannot send requests on behalf of others.'); // this should never happen                
                }
            } 
            else
            {
                $return = array('success' => false, 'error' => 'Sorry, you cannot follow yourself.'); // this should never happen                
            }
        }
        elseif( $_REQUEST['follow'] == 'team' )
        {
            if( $db->Delete('follow', array('teamid' => $id, 'followerid' => $follower)) )
            {
                $return = array('success' => true);           
            }
            else
            {
                $return = array('success' => false, 'error' => 'Error writing to database.'); // this should never happen                
            }
        }
    }
    else
    {
        $return = array('success' => false, 'error' => 'Sorry, some parameters are missing. Please try again.'); // this should never happen
    }
}
else
{
    $return = array('success' => false, 'error' => 'Sorry, you need to be logged in to perform this action.');
}

header('Content-type: application/json');
echo json_encode($return);
