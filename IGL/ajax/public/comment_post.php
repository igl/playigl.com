<?php
/**
 * Posts a comment
 * @author Darryl Allen <dallen@palyigl.com>
 * @author Shaun Delaney <sdelaney@playigl.com>
 * @return JSON boolean success
 * @copyright Copyright 2012-13 PlayIGL.com
 */
include_once('../../includes/config.php');
include_once('../../model/mysql.class.php');
include_once('../../includes/functions.php');
include_once('../../includes/pusher.php');
include_once('../../includes/notify.php');

//Default error message
$return = array("success" => false, "error" => "You must be logged in to do this action.");

//Variables
$page_id = isset($_REQUEST['page_id']) ? $_REQUEST['page_id'] : null;
$page_name = isset($_REQUEST['page_name']) ? $_REQUEST['page_name'] : null;
$comment = isset($_REQUEST['comment']) ? $_REQUEST['comment'] : null;
$subject_username = '';

if (isLoggedIn()) {
  if (!is_null($page_id) && is_numeric($page_id) && in_array($page_name, array('player', 'team', 'article')) && !is_null($comment)) {
    $db = new MySQL(MYSQL_DATABASE);

    $vars = array(
      'posterid' => $_SESSION['playerid'],
      'comment' => $comment,
      'page' => $page_name,
      'pageid' => $page_id,
      'dateposted' => time()
    );

    if ($db->Insert($vars, 'comments')) {
      $vars['avatar'] = $_SESSION['avatar'];
      $vars['username'] = $_SESSION['username'];
      $vars['postid'] = $db->lastId();
      $vars['comment'] = parseContent($vars['comment']);
      $vars['subject_email'] = '';
      $db->ExecuteSQL("SELECT email FROM player WHERE playerid = '".$page_id."'");
        $emailResult = $db->ArrayResult();
        $vars['subject_email'] = $emailResult['email'];
      $return = array('success' => true, 'result' => $vars);
    }
    else
      $return['error'] = "Unable to submit post.";
  }
  else
    $return['error'] = "Some parameters are missing.";
}

if ($return['success']) {
  $agents = array("player-{$_SESSION['playerid']}");
  $msgs = array("You have successfully posted your comment");

  if ($page == 'player') {
    $agents[] = "player-{$page_id}";
    $msgs[] = "You have a new comment on your profile page";
  } else if ($page == 'team') {
    $msgs = array();
    $agents = array_merge($agents, getTeamChannels($page_id));
    foreach ($agents as $agent) {
      $msgs[] = "You have a new comment on your team profile page";
    }
  }

  pushMessage(
    $agents, $msgs
  );
} else {
  pushMessage(
    array("player-{$_SESSION['playerid']}"), array($return['error'])
  );
}

header('Cache-Control: no-cache, must-revalidate');
returnJSON($return);

?>