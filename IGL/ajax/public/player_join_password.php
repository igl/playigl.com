<?php

/**
 * This page handles players joining a team by password.
 */
header('Content-type: application/json');
header('Cache-Control: no-cache, must-revalidate');

//include SQL vars
require('../../includes/config.php');
//include SQL class
require('../../model/mysql.class.php');
//include acitvity - allows usto update player activity
require('../../model/activity.class.php');
//Include SendGrid
require('../../sendgrid-php/SendGrid_loader.php');

//Set our SendGrid classes
$sendgrid = new SendGrid('playigl', 'xeCr9feJ');
$mail = new SendGrid\Mail();

if( isset($_SESSION['playerid']) && $_SESSION['playerid'] > 0 )
{
    if(isset($_REQUEST['team']) && is_numeric($_REQUEST['team']) && isset($_REQUEST['player']) && is_numeric($_REQUEST['player']) && isset($_REQUEST['password']) )
    {
        
        $player = (int)$_REQUEST['player'];
        $activity   = new activity();
        $db         = new mysql(MYSQL_DATABASE);
        $team       = (int)$_REQUEST['team'];
        $password   = $_REQUEST['password'];
        
        if($player == $_SESSION['playerid'])
        {

            $query = "SELECT playerid FROM player WHERE playerid = '{$player}' AND steamid > 0";
            $db->ExecuteSQL($query);
            $player_authed = $db->iRecords();
            $query = "SELECT `steamauth` FROM `game` JOIN `team` ON `team`.`game` = `game`.`id` WHERE `team`.`id` = {$team} AND `game`.`steamauth` = ";
            $db->ExecuteSQL($query);
            $game_steam_auth = $db->iRecords();
            
            
            if( $player_authed || $game_steam_auth==0 )
            {
                $firstname  = $_SESSION['firstname'];
                $lastname   = $_SESSION['lastname'];
                $username   = $_SESSION['username'];



                $query = "SELECT `id` FROM `team` WHERE `password` = '{$password}' AND `id` = '{$team}'";

                $db->ExecuteSQL($query);
                if($db->iRecords())
                {

                    $sql = "
                            SELECT 
                                `roster`.`game`
                            FROM 
                                `roster`
                            WHERE 
                                `roster`.`player` = '{$player}' 
                            AND `roster`.`game` = (SELECT 
                                                        `team`.`game` 
                                                    FROM 
                                                        `team` 
                                                    WHERE 
                                                        `team`.`id` = '{$team}')";

                    $db->executeSQL($sql);
                    if(!$db->iRecords())
                    {

                        //Get the join request template
                        $sql = "SELECT `from`, `body`, `subject` FROM email_templates WHERE type='player_joined_team' LIMIT 1";
                        $db->ExecuteSQL($sql);
                                if($db->iRecords())
                                {
                                    $result = $db->ArrayResult();
                                    $message        = $result['body'];
                                    $subject        = $result['subject'];
                                    $from           = $result['from'];

                                }
                                else
                                {
                                    $message    = "<a href='[baseurl]dashboard'>login</a> to see who joined your team.";
                                    $subject        = "Someone joined your team!";
                                    $from           = "noreply@playigl.com";
                                }

                    $query = "
                            SELECT 
                                `team`.`id` as team_id,
                                `team`.`name` as team_name,
                                `team`.`game` as game_id,
                                `team`.`captain` as captain_id,
                                `team`.`alternate` as alternate_id,
                                `captain`.`email` as captain_email,
                                `captain`.`username` as captain_username,
                                `alternate`.`email` as alternate_email,
                                `alternate`.`username` as alternate_username
                            FROM
                                `team`
                            LEFT JOIN
                                `player` as captain ON `captain`.`playerid` = `captain`
                            LEFT JOIN
                                `player` as alternate ON `alternate`.`playerid` = `alternate`
                            WHERE 
                                `team`.`id`= '{$team}'
                            ";
                        $db->ExecuteSQL($query);
                        $doc = $db->ArrayResult();
                        if(filter_var($doc['captain_email'],FILTER_VALIDATE_EMAIL))
                        {
                            $recipient_id = array($doc['captain_id'],$doc['alternate_id']);
                            $recipient_name = array($doc['captain_username'],$doc['alternate_username']);
                            $recipient_email = array($doc['captain_email'],$doc['alternate_email']);
                            $sender_id = array($_SESSION['playerid'],$_SESSION['playerid']);
                            $sender_name = array($_SESSION['username'],$_SESSION['username']);
                            $sender_email = array($_SESSION['email'],$_SESSION['email']);
                            $team_id = array($doc['team_id'],$doc['team_id']);
                            $team_name = array($doc['team_name'],$doc['team_name']);
                            $baseurl = array(BASEURL,BASEURL);


                            //Category for SendGrid stats
                            $mail->setCategory('Join team by password');
                            //Array of users to receive email
                            $mail->setTos($recipient_email);
                            //Set From email
                            $mail->setFrom($from);
                            //Set from display name
                            $mail->setFromName('International Gaming League');
                            //set subject
                            $mail->setSubject($subject);
                            //set HTML message body
                            $mail->setHtml($message);
                            //arrays of replacemens for the message body
                            $mail->addSubstitution('[recipient_id]', $recipient_id);
                            $mail->addSubstitution('[recipient_name]', $recipient_name);
                            $mail->addSubstitution('[recipient_email]', $recipient_email);
                            $mail->addSubstitution('[sender_id]', $sender_id);
                            $mail->addSubstitution('[sender_name]', $sender_name);
                            $mail->addSubstitution('[sender_email]', $sender_email);
                            $mail->addSubstitution('[team_id]', $team_id);
                            $mail->addSubstitution('[team_name]', $team_name);
                            $mail->addSubstitution('[baseurl]', $baseurl);
                            //Send the email!
                            $sendgrid->web->send($mail);    

                            if($db->Insert(array(
                                'player' => $player,
                                'team' => $team,
                                'game' => $doc['game_id']
                            ), 'roster'))
                            {
                                $activity->addEvent('joined', $player, $team, $league, 0);
                                $return = array('success' => true, 'message' => 'Success!', 'player_id' => $player, 'username' => $username, 'firstname' => $firstname, 'lastname' => $lastname);
                            }
                            else
                            {
                                $return = array('success' => false, 'error' => 'Failed to sent invite request.');
                            }
                        }
                        else
                        {
                            $return = array('success' => false, 'error' => "The captain does not have a valid email address."); // this should never happen
                        }
                    }
                    else
                    {
                        $return = array('success' => false, 'error' => "You're already on a team for this game.");
                    }
                }
                else
                {
                    $return = array('success' => false, 'error' => "Password join disabled or the password is incorrect."); // this should never happen
                }
            }
            else 
            {
                $return = array('success' => false, 'error' => "You must first authenticate with Steam"); // this should never happen
            }
        }
        else
        {
            $return = array('success' => false, 'error' => 'You cannot join teams on behalf of other players.'); // this should never happen
        }
    }
    else
    {
        $return = array('success' => false, 'error' => 'Sorry, some parameters are missing. Please try again.'); // this should never happen
    }
}
else
{
    $return = array('success' => false, 'error' => 'Sorry, you need to be logged in to perform this action.');
}
echo json_encode($return);
