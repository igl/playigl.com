<?php
/*
 * @copyright 2012 PlayIGL.comT
 * @author Darryl Allen
 * This file sets up the next 4 hours of matches for the ladder pool. The maps are read from the 'ladder_map_schedule' table.
 * ladder_map_schedule is updated by an hourly job, ladder_set_hourly_maps.php
 */

header('Cache-Control: no-cache, must-revalidate');
header('Content-type: application/json');


include('../../model/mysql.class.php');
require('../../includes/config.php');
require('../../includes/functions.php');

if( isset($_SESSION['playerid']) && $_SESSION['playerid'] > 0 )
{
    if ( isset($_REQUEST['id']) && is_numeric($_REQUEST['id']) ) 
    {
        $db = new mysql(MYSQL_DATABASE);
        $player_id = (int)$_SESSION['playerid'];
        $tier_id = (int)$_REQUEST['id'];
        $pool = array();
        
        //Check if user is captain of a team in this ladder. If so, get the game.
        $query = "
            SELECT
                `active_teams`.`team_id`,
                `active_teams`.`team_game_id`,
                `active_teams`.`team_captain_id`,
                `active_teams`.`team_alternate_id`
            FROM
                `active_teams`
            JOIN
                `ladder_teams` ON `ladder_teams`.`team_id` = `active_teams`.`team_id`
            WHERE
                `ladder_teams`.`tier_id` = {$tier_id} AND {$player_id} IN (`team_captain_id`,`team_alternate_id`) ";

        $captain = false;
        if( $db->ExecuteSQL($query) && $db->iRecords() )
        {
            $team_info = $db->ArrayResult();
            
            if( in_array($player_id, array($team_info['team_alternate_id'],$team_info['team_captain_id'])) )
            {
                $captain = true;
                
                //Get the ladder pools and locations
                    $query = "
                        SELECT
                            continent AS server_continent,
                            region AS server_region,
                            count(`continent`) as server_available
                        FROM 
                            `matchservers` 
                        WHERE 
                            `competition_type` = 'ladder' AND `game` =  {$team_info['team_game_id']}
                        GROUP BY `continent`";
                    $db->ExecuteSQL($query);
                    $servers = $db->ArrayResults();
            }
        }
        

            $plus_one_hour = time() + 3600;
            $now = floor($plus_one_hour / 3600) * 3600; 

            $timeSlots = array(
                $now,
                $now + 3600,
                $now + 7200,
                $now + 10800
            );

            foreach( $timeSlots as $key => $time )
            {
                $query = "
                    SELECT
                        `ladder_pool`.`time` AS `ladder_time_gmt`,
                        `ladder_pool`.`time` AS `ladder_time`,
                        `ladder`.`id` AS `ladder_id`
                    FROM 
                        `ladder_pool`
                    JOIN
                        `ladder` ON `ladder`.`id` = `ladder_pool`.`ladder`
                    WHERE
                        `ladder_pool`.`ladder` = '{$tier_id}' AND
                        `ladder_pool`.`time` = '{$time}' ";

                $db->ExecuteSQL($query);
                if($db->iRecords())
                {
                    $pool[$key] = $db->ArrayResult();
                }
                else
                {
                    $pool[$key] = array();
                }
                $pool[$key]['ladder_time'] = date('g:ia',$time);
                $pool[$key]['ladder_time_unix'] = $time;

                //Grab the maps scheduled for this hour
                $query = "SELECT 
                            map
                            FROM ladder_map_schedule 
                            WHERE game = {$team_info['team_game_id']} AND time = {$time}";

                if( $db->ExecuteSQL($query) && $db->iRecords() )
                {
                    $maps = $db->ArrayResults();
                    foreach( $maps as $k => $map )
                    {
                        $pool[$key]['ladder_maps'][$k]['map'] = $map['map'];   
                        $query = "SELECT id FROM ladder_pool_votes WHERE map = '{$map['map']}' AND time = {$time}\n";
                        $db->ExecuteSQL($query);
                        $pool[$key]['ladder_maps'][$k]['votes'] = $db->iRecords();
                    }
                }


                //Find out if the team the user is captain of, is already playing at this time.
                $query = "
                    SELECT
                        `team`.`id`
                    FROM
                        `team`
                    JOIN
                        `ladder_pool`
                    ON 
                        `ladder_pool`.`team`= `team`.`id`
                    WHERE
                        `ladder_pool`.`time` = '{$time}' AND team.id = {$team_info['team_id']}";
                $db->ExecuteSQL($query);
                if( $db->iRecords() && $captain )
                {
                    $pool[$key]['ladder_registered'] = true;
                }
                else
                {
                    $pool[$key]['ladder_registered'] = false;
                }

                //Get the number of teams signed up, and list them
                $query = "
                SELECT
                    `ladder_pool`.`team` AS `team_id`,
                    `team`.`name` AS `team_name`
                FROM
                    `ladder_pool`
                JOIN
                    `team` 
                ON 
                    `ladder_pool`.`team` = `team`.`id`
                WHERE 
                    `ladder_pool`.`ladder` = '{$tier_id}' AND
                    `ladder_pool`.`time` = '{$time}'";
                $db->ExecuteSQL($query);
                if($db->iRecords())
                {
                    $pool[$key]['ladder_num_teams'] = $db->iRecords();
                    $pool[$key]['ladder_teams'] = $db->ArrayResults();
                }
                else
                {
                    $pool[$key]['ladder_num_teams'] = 0;
                    $pool[$key]['ladder_teams'] = array();;
                }
            }
            $return = array('success' => true, 'captain' => $captain, 'pool' => $pool, 'servers' => $servers);
    }
    else
    {
        $return = array('success' => false, 'error' => 'Sorry, some parameters are missing');
    }
}
else
{
    $return = array('success' => false, 'error' => 'You must be logged in to view pool times');
}
header('Content-type: application/json');
prettyJSON(json_encode($return));