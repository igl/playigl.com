<?php

/**
 * This page handles invite requests. invite or revoke via Ajax from view/freeagents.php 
 */
require('../../includes/config.php');
require('../../includes/functions.php');
require('../../model/mysql.class.php');
require('../../model/activity.class.php');

if( isset($_SESSION['playerid']) && $_SESSION['playerid'] > 0 )
{
    if( isset($_REQUEST['captain']) && is_numeric($_REQUEST['captain']) && isset($_REQUEST['player']) && is_numeric($_REQUEST['player']) )
    {
        $activity = new activity();
        $db     = new mysql(MYSQL_DATABASE);
        $captain = (int)$_REQUEST['captain'];
        $player = (int)$_REQUEST['player'];

        $data = array();
        $query = "
            SELECT 
                `team`.`id`, 
                `team`.`game`, 
                `team`.`name`, 
                `game`.`icon` as image 
            FROM 
                `team`
            JOIN
                `game` ON `game`.`id` = `team`.`game`
            WHERE (`team`.`captain` = '{$captain}' || `team`.`alternate` = '{$captain}') AND `team`.`status` = 1";
        $db->ExecuteSQL($query);
        if($db->iRecords)
        {
            $data['captain_teamids'] = array();
            $data['captain_gameids'] = array();
            $data['captain_team_names'] = array();
            foreach ( $db->ArrayResults() as $key => $captain_teams )
            {
                $data['captain_teamids'][$key] = $captain_teams['id'];
                $data['captain_gameids'][$key] = $captain_teams['game'];
                $data['captain_team_names'][$key] = $captain_teams['name'];
                $data['captain_game_image'][$key] = $captain_teams['image'];
            }
            $query = "SELECT * FROM inviterequests WHERE player = '{$player}'";
            $db->ExecuteSQL($query);
            $data['player_invites'] = array();
            foreach ( $db->ArrayResults() as $key => $player_invites )
            {
                $data['player_invites'][$key] = $player_invites['team'];
            }
            
            $query = "SELECT team, game FROM roster WHERE player = '{$player}'";
            $db->ExecuteSQL($query);
            $data['player_teamids'] = array();
            $data['player_gameids'] = array();
            foreach ( $db->ArrayResults() as $key => $player_teams )
            {
                $data['player_teamids'][$key] = $player_teams['team'];
                $data['player_gameids'][$key] = $player_teams['game'];
            }

            foreach($data['captain_teamids'] as $key => $captains_team)
            {
                if(in_array($captains_team, $data['player_teamids']) || in_array($captains_team, $data['player_invites']))
                {
                    $result[$key]['id'] = $captains_team;
                    $result[$key]['name'] = $data['captain_team_names'][$key];
                    $result[$key]['image'] = $data['captain_game_image'][$key];
                    $result[$key]['member'] = true;
                    $result[$key]['available'] = true;
                }
                elseif(in_array($data['captain_gameids'][$key], $data['player_gameids']))
                {
                    $result[$key]['id'] = $captains_team;
                    $result[$key]['name'] = $data['captain_team_names'][$key];
                    $result[$key]['image'] = $data['captain_game_image'][$key];
                    $result[$key]['member'] = false;
                    $result[$key]['available'] = false;
                }
                else
                {
                    $result[$key]['id'] = $captains_team;
                    $result[$key]['name'] = $data['captain_team_names'][$key];
                    $result[$key]['image'] = $data['captain_game_image'][$key];
                    $result[$key]['member'] = false;
                    $result[$key]['available'] = true;
                }
            }
            $return = array('success' => true, 'result' => $result);
        }
        else
        {
            $return = array('success'=> false, 'message'=> 'You are not a team captain.');
        }
        
        
    }
    else
    {
        $return = array('success' => false, 'message' => 'Sorry, some parameters are missing. Please try again.'); // this should never happen
    }
}
else
{
    $return = array('success' => false, 'message' => 'Sorry, you need to be logged in to perform this action.');
}

header('Content-type: application/json');
echo prettyJSON(json_encode($return));
