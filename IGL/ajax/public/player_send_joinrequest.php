<?php

/**
 * This page handles a player requesting to join a team.
 */
header('Content-type: application/json');
header('Cache-Control: no-cache, must-revalidate');

//include SQL vars
require('../../includes/config.php');
//include SQL class
require('../../model/mysql.class.php');
//include acitvity - allows usto update player activity
require('../../model/activity.class.php');
//Include SendGrid
require('../../sendgrid-php/SendGrid_loader.php');

//Set our SendGrid classes
$sendgrid = new SendGrid('playigl', 'xeCr9feJ');
$mail = new SendGrid\Mail();

if( isset($_SESSION['playerid']) && $_SESSION['playerid'] > 0 )
{
    if(isset($_REQUEST['team']) && is_numeric($_REQUEST['team']) && isset($_REQUEST['player']) && is_numeric($_REQUEST['player']) )
    {
        
        $player = (int)$_REQUEST['player'];
        if($player == $_SESSION['playerid'])
        {
            $db = new mysql(MYSQL_DATABASE);
            $team = (int)$_REQUEST['team'];
            
            //Verify the team is set to allow join requests
            $query = "SELECT id FROM team WHERE open = 1 AND id = '{$team}'";
            $db->ExecuteSQL($query);
            if($db->iRecords())
            {
                $sql = "
                    SELECT 
                        `id`
                    FROM 
                        `joinrequests`
                    WHERE 
                        `player` = '{$_SESSION['playerid']}' AND `team` = '{$team}'";
                //Make sure the player hasn't alreayd requested to join
                $db->executeSQL($sql);
                if(!$db->iRecords())
                {

                    //Get the join request template
                    $sql = "SELECT `from`, `body`, `subject` FROM email_templates WHERE type='player_send_joinrequest' LIMIT 1";
                    $db->ExecuteSQL($sql);
                            if($db->iRecords())
                            {
                                $result = $db->ArrayResult();
                                $message        = $result['body'];
                                $subject        = $result['subject'];
                                $from           = $result['from'];

                            }
                            else
                            {
                                $message        = "[player_username] has requesed to join your team. <a href='[baseurl]dashboard'>login</a> to accept or deny.";
                                $subject        = "[player_username] wants to join your team!";
                                $from           = "noreply@playigl.com";
                            }

                //Build data to send email to captains
                $query = "
                        SELECT 
                            `team`.`id` as team_id,
                            `team`.`name` as team_name,
                            `team`.`captain` as captain_id,
                            `team`.`alternate` as alternate_id,
                            `captain`.`email` as captain_email,
                            `captain`.`username` as captain_username,
                            `alternate`.`email` as alternate_email,
                            `alternate`.`username` as alternate_username
                        FROM
                            `team`
                        LEFT JOIN
                            `player` as captain ON `captain`.`playerid` = `captain`
                        LEFT JOIN
                            `player` as alternate ON `alternate`.`playerid` = `alternate`
                        ";
                    $db->ExecuteSQL($query);
                    $doc = $db->ArrayResult();
                    if(filter_var($doc['captain_email'],FILTER_VALIDATE_EMAIL))
                    {
                        $recipient_id = array($doc['captain_id'],$doc['alternate_id']);
                        $recipient_name = array($doc['captain_username'],$doc['alternate_username']);
                        $recipient_email = array($doc['captain_email'],$doc['alternate_email']);
                        $sender_id = array($_SESSION['playerid'],$_SESSION['playerid']);
                        $sender_name = array($_SESSION['username'],$_SESSION['username']);
                        $sender_email = array($_SESSION['email'],$_SESSION['email']);
                        $team_id = array($doc['team_id'],$doc['team_id']);
                        $team_name = array($doc['team_name'],$doc['team_name']);
                        $baseurl = array(BASEURL,BASEURL);
                        
                        //Category for SendGrid stats
                        $mail->setCategory('Player request to join team');
                        //Array of users to receive email
                        $mail->setTos($recipient_email);
                        //Set From email
                        $mail->setFrom($from);
                        //Set from display name
                        $mail->setFromName('International Gaming League');
                        //set subject
                        $mail->setSubject($subject);
                        //set HTML message body
                        $mail->setHtml($message);
                        //arrays of replacemens for the message body
                        $mail->addSubstitution('[recipient_id]', $recipient_id);
                        $mail->addSubstitution('[recipient_name]', $recipient_name);
                        $mail->addSubstitution('[recipient_email]', $recipient_email);
                        $mail->addSubstitution('[sender_id]', $sender_id);
                        $mail->addSubstitution('[sender_name]', $sender_name);
                        $mail->addSubstitution('[sender_email]', $sender_email);
                        $mail->addSubstitution('[team_id]', $team_id);
                        $mail->addSubstitution('[team_name]', $team_name);
                        $mail->addSubstitution('[baseurl]', $baseurl);
                        
                        
                        if($db->Insert(array(
                                'player' => $_SESSION['playerid'],
                                'team' => $team
                            ), 'joinrequests'))
                        {
                        //Send the email!
                        $sendgrid->web->send($mail);   
                        $return = array('success' => true, 'message' => 'Invite request sent successfully.');
                        }
                    }
                    else
                    {
                        $return = array('success' => false, 'message' => "The captain does not have a valid email address."); // this should never happen
                    }

                }
                else
                {
                    $return = array('success' => false, 'message' => "You've already requested to join this team."); // this should never happen
                }
            }
            else
            {
                $return = array('success'=> false, 'message' => "This team is not accepting applications.");
            }
        }
        else
        {
            $return = array('success' => false, 'message' => 'You cannot send requests on behalf of other players.'); // this should never happen
        }
    }
    else
    {
        $return = array('success' => false, 'message' => 'Sorry, some parameters are missing. Please try again.'); // this should never happen
    }
}
else
{
    $return = array('success' => false, 'message' => 'Sorry, you need to be logged in to perform this action.');
}

echo json_encode($return);
