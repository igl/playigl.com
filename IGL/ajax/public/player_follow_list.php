<?php

/**
 * This page handles tracking requests.  
 * @author Darryl Allen
 * @copyright Copyright 2013 PlayIGL.com
 */
require('../../includes/config.php');
require('../../includes/functions.php');
require('../../model/mysql.class.php');
include('../../model/activity.class.php');

$activity       = new activity();

if( isset($_SESSION['playerid']) && $_SESSION['playerid'] > 0 )
{
    if( isset($_REQUEST['player']) && is_numeric($_REQUEST['player']) )
    {
        $db = new mysql(MYSQL_DATABASE);
        $player = (int)$_REQUEST['player'];
        $query = "SELECT * FROM follow WHERE followerid = '{$player}'";
        $db->ExecuteSQL($query);
        if( $db->iRecords() )
        {
            $follows = $db->ArrayResults();
            foreach($follows as $follow)
            {
                $follow['playerid'];
                $get_list = json_decode(file_get_contents(BASEURL."api/player/{$follow['playerid']}"),true);
                if($get_list['success'])
                {
                    $following[] = $get_list['profile'];
                }
            }
            $return = array('success' => true, 'following' => $following);
        }
        else
        {
            $return = array('success' => false, 'error' => 'User is not following anyone.'); // this should never happen
        }

    }
    else
    {
        $return = array('success' => false, 'error' => 'Sorry, some parameters are missing. Please try again.'); // this should never happen
    }
}
else
{
    $return = array('success' => false, 'error' => 'Sorry, you need to be logged in to perform this action.');
}

header('Content-type: application/json');
echo prettyJSON(json_encode($return));
