<?php

/**
 * This page handles captains inviting players to their team. invite or revoke via Ajax from view/freeagents.php 
 */
require('../../includes/config.php');
require('../../model/mysql.class.php');
require('../../model/activity.class.php');

//Include SendGrid
require('../../sendgrid-php/SendGrid_loader.php');

//Set our SendGrid classes
$sendgrid = new SendGrid('playigl', 'xeCr9feJ');
$mail = new SendGrid\Mail();

if( isset($_SESSION['playerid']) && $_SESSION['playerid'] > 0 )
{
    if(isset($_REQUEST['player']) && is_numeric($_REQUEST['player']) && isset($_REQUEST['team']) && is_numeric($_REQUEST['team']) && isset($_REQUEST['action']) && in_array($_REQUEST['action'], array('invite','revoke')))
    {
        $activity = new activity();
        $db     = new mysql(MYSQL_DATABASE);
        $player = (int)$_REQUEST['player'];
        $team   = (int)$_REQUEST['team'];
        
        $query = "SELECT captain, alternate, game, `id` as team_id, `name` as team_name FROM `team` WHERE `id` = {$team} LIMIT 1";
        $db->executeSQL($query);
        if($db->iRecords())
        {
            $doc = $db->ArrayResult();
            // We need to make sure we're denying one of our own request and not something fishy.
            if(in_array($_SESSION['playerid'], array($doc['captain'],$doc['alternate'])))
            {
                if($_REQUEST['action'] == 'invite')
                {
                    // let's make sure we're not already in a team, otherwise we can't accept this request.
                    $query = "SELECT `inviterequests`.`id` FROM `inviterequests` WHERE `inviterequests`.`player` = {$player} AND `inviterequests`.`team` = {$team}";
                    $db->ExecuteSQL($query);
                    if(!$db->iRecords())
                    {
                        $query = "
                        SELECT `roster`.`id` FROM `roster` WHERE `roster`.`player` = {$player} AND `roster`.`team` = {$team}";
                        $db->ExecuteSQL($query);
                        if(!$db->iRecords())
                        {
                            $query = "INSERT INTO inviterequests (`game`, `team`, `player`) VALUES ('{$doc['game']}','{$team}','{$player}')";
                            if($db->executeSQL($query))
                            {
                                $query = "SELECT username, email, playerid FROM player WHERE playerid = '{$player}'";
                                $db->ExecuteSQL($query);
                                if($db->iRecords())
                                {
                                    $player_info = $db->ArrayResult();

                                    $query = "SELECT `from`, `body`, `subject` FROM email_templates WHERE type='invite' LIMIT 1";
                                    $db->ExecuteSQL($query);
                                    if($db->iRecords())
                                    {
                                        $result = $db->ArrayResult();
                                        $message        = $result['body'];
                                        $subject        = $result['subject'];
                                        $from           = $result['from'];

                                    }
                                    else
                                    {
                                        $message        = "Hello [player_username], <br>[captain] has invited you to join [team_name]! To accept or deny your invite, visit <a href='[baseurl]dashboard'>[baseurl]dashboard</a>";
                                        $subject        = "Invited to [team_name]";
                                        $from           = "noreply@playigl.com";
                                    }


                                    $to = $player_info['email'];
                                    $message = str_replace(array('[sender_email]','[sender_name]','[sender_id]','[baseurl]','[team_name]'), 
                                                        array( $_SESSION['email'], $_SESSION['username'], $_SESSION['playerid'], BASEURL,$doc['team_name']), $message);
                                   
                                    
                                    $recipient_id = array($player_info['playerid']);
                                    $recipient_name = array($player_info['username']);
                                    $recipient_email = array($player_info['email']);
                                    $sender_id = array($_SESSION['playerid']);
                                    $sender_name = array($_SESSION['username']);
                                    $sender_email = array($_SESSION['email']);
                                    $team_id = array($doc['team_id']);
                                    $team_name = array($doc['team_name']);
                                    $baseurl = array(BASEURL);


                                    //Category for SendGrid stats
                                    $mail->setCategory('Player Invites');
                                    //Array of users to receive email
                                    $mail->setTos($recipient_email);
                                    //Set From email
                                    $mail->setFrom($from);
                                    //Set from display name
                                    $mail->setFromName('International Gaming League');
                                    //set subject
                                    $mail->setSubject($subject);
                                    //set HTML message body
                                    $mail->setHtml($message);
                                    //arrays of replacemens for the message body
                                    $mail->addSubstitution('[recipient_id]', $recipient_id);
                                    $mail->addSubstitution('[recipient_name]', $recipient_name);
                                    $mail->addSubstitution('[recipient_email]', $recipient_email);
                                    $mail->addSubstitution('[sender_id]', $sender_id);
                                    $mail->addSubstitution('[sender_name]', $sender_name);
                                    $mail->addSubstitution('[sender_email]', $sender_email);
                                    $mail->addSubstitution('[team_id]', $team_id);
                                    $mail->addSubstitution('[team_name]', $team_name);
                                    $mail->addSubstitution('[baseurl]', $baseurl);
                                    //Send the email!
                                    
                                    /* REMOVED TO USE CUSTOMER.IO
                                    $sendgrid->web->send($mail);    
                                    */
                                    

                                    $activity->addEvent('invited to team', $player, $team, false, false);
                                    $return['data'] = array('recipient_id' => $recipient_id, 'recipient_name' => $recipient_name, 'recipient_email' => $recipient_email, 'sender_id' => $sender_id, 'sender_name' => $sender_name, 'sender_email' => $sender_email, 'team_id' => $team_id, 'team_name' => $team_name  );
                                    $return = array('success' => true, 'message' => 'Player invited successfully.');

                                }
                                else
                                {
                                    $return = array('success' => false, 'message' => 'Player could not be located.'); //this should never happen
                                }
                            }
                            else
                            {
                                $return = array('success' => false, 'message' => '2Invitation could not be sent.');
                            }
                        }
                        else
                        {
                            $return = array('success' => false, 'message' => 'This player is already on your roster.'); 
                        }
                    }
                    else
                    {
                        $return = array('success' => false, 'message' => 'This player already has an invite.'); 
                    }
                } 
                elseif($_REQUEST['action'] == 'revoke')
                {
                    $db->delete('inviterequests', array('player' => $player, 'team' => $team));
                    $return = array('success' => true, 'message' => 'Invitation successfully removed.');
                }
            }
            else
            {
                $return = array('success' => false, 'message' => 'Sorry, you don\'t have access to this request.'); // this should never happen                
            }     
        }
        else
        {
            $return = array('success' => false, 'message' => 'Sorry, this request doesn\'t exist.'); // this should never happen
        }
    }
    else
    {
        $return = array('success' => false, 'message' => 'Sorry, some parameters are missing. Please try again.'); // this should never happen
    }
}
else
{
    $return = array('success' => false, 'message' => 'Sorry, you need to be logged in to perform this action.');
}

header('Content-type: application/json');
echo json_encode($return);
