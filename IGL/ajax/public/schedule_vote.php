<?php
/**
 * Saves a player vote for a match
 * @author Shaun Delaney <sdelaney@playigl.com>
 * @return JSON Vote fail flags
 * @copyright Copyright 2013 PlayIGL.com
 */
include_once('../../includes/config.php');
include_once('../../model/mysql.class.php');
include_once('../../includes/functions.php');
//include_once('../../includes/pusher.php');
//include_once('../../includes/notify.php');

//Default error message
$return = array("success" => false, "code" => "01", "error" => "Please log in to use this feature.");

//Ensure we're logged in
if (isLoggedIn()) { 
  $player_id = $_SESSION['playerid'];
  $match_id = $_REQUEST['id'];
  unset($return['code']);

  //Reject non-numeric IDs
  if (!is_numeric($player_id) || !is_numeric($match_id)) {
    $return['error'] = "Invalid data passed.";
    returnJSON($return);
  }

  $db = new mysql(MYSQL_DATABASE);
  $db->ExecuteSQL(
    "SELECT * FROM schedule_vote WHERE schedule_id = {$match_id} AND player_id = {$player_id} LIMIT 1;"
  );

  if (!$db->iRecords()) {
    $arr = array(
      "player_id" => $player_id,
      "schedule_id" => $match_id
    );
    
    $b = $db->Insert($arr, 'schedule_vote');

    if ($b) {
      $return = array("success" => true, "error" => "You have succesfully voted for Match {$match_id}");
      //pushMessage(
        //array("player-{$_SESSION['playerid']}"), array($return['error'])
      //);
    } else {
      $return['error'] = "There was an error submitting your vote";
      //pushMessage(
        //array("player-{$_SESSION['playerid']}"), array($return['error'])
      //);
    }
  } else {
    $return['error'] = "You have already voted for Match {$match_id}";
    //pushMessage(
        //array("player-{$_SESSION['playerid']}"), array($return['error'])
      //);
  }
}

returnJSON($return, true);
