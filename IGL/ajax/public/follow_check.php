<?php
/**
 * This page handles tracking requests.  
 * @author Darryl Allen <dallen@palyigl.com>
 * @author Shaun Delaney <sdelaney@playigl.com>
 * @return JSON true/false, if actor follows subject
 * @copyright Copyright 2013 PlayIGL.com
 */

include_once('../../includes/config.php');
include_once('../../model/mysql.class.php');
include_once('../../includes/functions.php');

//Default error message
$return = array("success" => false, "error" => "Please log in to use this feature.");

//Ensure we're logged in
if(isLoggedIn()) {
    $actor_id   = $_SESSION['playerid'];
    $actor_type = 'player';
    $subject_id = $_REQUEST['subject'];
    $subject_type = $_REQUEST['subject_type'];

    //Reject non-numeric IDs
    if (!is_numeric($actor_id) || !is_numeric($subject_id)) {
        $return['error'] = "Invalid data passed.";
        returnJSON($return);
    }

    //Check to see if actor is following subject
    $db = new MySQL(ACTIVITY);
    $db->ExecuteSQL(
        "SELECT `id` FROM `notifications` 
        WHERE `actor_id` = {$actor_id} AND `actor_type` = '{$actor_type}'
        AND `subject_id` = {$subject_id} AND `subject_type` = '{$subject_type}'
        AND `verb` = 'follow'
        LIMIT 1;"
    );

    if ($db->iRecords())
        $return = array('success' => true);           
    else
        $return['error'] = "You do not follow this {$subject_type}";
}

returnJSON($return);