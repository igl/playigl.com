/* 
 * Used in view/freeagents.php
 *
 * @author Darryl Allen
 * @copyright Copyright 2012 PlayIGL.com
 */
//incompleted... may not be needed
function referral_send(user, email1, email2, email3, email4, email5) {
    $.getJSON('/ajax/dashboard/referral_send.php?id=' + user + '&email[]=' + email1 + '&email[]=' + email2 + '&email[]=' + email3 + '&email[]=' + email4 + '&email[]=' + email5, function(data) {
        if (data.success == undefined)
        {
            alert('Sorry, there was a problem with this request. Please try again.');
        }
        else
        {
            if (data.success)
            {
                $('#sendinvite').after('Sent!');
            }
            else
            {
                if (data.error == undefined)
                    alert('Unknown error while trying to reserve server');
                else
                    alert(data.error);
            }
        }
    });
}