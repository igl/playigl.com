/*
 * Genertaes list of teams open to recruitg
 */

function init() {
    updateOpenTeams($('#server-data').attr('data-game-id'));
}



function updateOpenTeams(game) {
    $.getJSON('/api/openteams/' + game, function(json) {

            if(json.success)
            {
                $('#openteamlist tbody').html('');
                appendrow = '';
                $.each(json.team, function(key, value) {
                    appendrow = '<tr id="team_'+value.team_id+'"><td><a href="/team/'+value['team_id']+'-'+value['team_name']+'">'+value.team_name+'</a></td><td>'+value.team_continent+' - '+value.team_region+'</td><td>'+value.team_size+'</td></tr>\n';
                    
                    $('#openteamlist tbody').append(appendrow);
                });
                    
            }
            else
            {
                $('#openteamlist tbody').html('<tr id="no_agents"><td colspan="6">'+json.error+'</td></tr>');
            }
            $('title').html('International Gaming League - '+json.game['game_name']+' - Teams Recruiting');
            $('#header_image').attr('src','/img'+json.game['game_banner'])
            $('th#title').html(json.game['game_name']+' - Teams Recruiting')
    });
}


function toggleAgent(id)
{
    $('.toggleAgent_' + id).toggle('slow');
}
