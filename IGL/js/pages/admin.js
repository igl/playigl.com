/* 
 * This file does some stuff with admin pages
 */

function featureArticle(btn, $id) {
  $btn = $(btn);
  anim = $btn.find("i");
  old_class = anim.attr("class");
  data = {
    id: $id,
    featured: anim.hasClass("icon-star")
  };

  btn.disabled = true;
  anim.attr("class", "icon-refresh icon-spin");

  jqxhr = $.post("/ajax/admin/feature_article.php", data, "json")
    .done(function(data) {
    if (data.success) {
      if (data.featured == 1)
        anim.attr("class", null).attr("class", "icon-star");
      else
        anim.attr("class", null).attr("class", "icon-star-empty");
    } else {
      anim.attr("class", null).attr("class", old_class);
    }
  })
    .fail(function() {
    anim.attr("class", null).attr("class", old_class);
  })
    .always(function() {
    $btn.attr("disabled", null);
  });
}

function competitionSelect()
{
  $('#teamGame').change(function() {
    game = $('#teamGame').val();
    //reset menus
    $('#teamLeague').html('<option value="0">---</option>');
    $('#teamLadder').html('<option value="0">---</option>');
    $.ajax({
      url: "/api/game/" + game
    }).done(function(response) { //get all games wih their respective leagues & ladders
      currentLeague = $('#currentLeague').val();
      currentLadder = $('#currentLadder').val();

      $(response).each(function(i, v) {
        if (v['game_id'] == game)
        {
          $(v['game']).each(function(i, v) { //sort through each game
            $(v['game_leagues']).each(function(i, v) //Get each league for this game
            {
              if (currentLeague == v.league_id)
              {
                $('#teamLeague').append('<option value="' + v.league_id + '" selected="selected">' + v.league_name + '</option>');
              }
              else
              {
                $('#teamLeague').append('<option value="' + v.league_id + '">' + v.league_name + '</option>');
              }
            });
            $(v['game_ladders']).each(function(i, v) //get each ladder for this game
            {
              if (currentLadder == v.ladder_id)
              {
                $('#teamLadder').append('<option value="' + v.ladder_id + '" selected="selected">' + v.ladder_name + '</option>');
              }
              else
              {
                $('#teamLadder').append('<option value="' + v.ladder_id + '">' + v.ladder_name + '</option>');
              }
            });

          });
        }
      });

      //do this for adding a new team
      if (response['game']['game_id'] == game)
      {
        $(response['game']['game_leagues']).each(function(i, v) //Get each league for this game
        {
          if (currentLeague == v.league_id)
          {
            $('#teamLeague').append('<option value="' + v.league_id + '" selected="selected">' + v.league_name + '</option>');
          }
          else
          {
            $('#teamLeague').append('<option value="' + v.league_id + '">' + v.league_name + '</option>');
          }
        });
        $(response['game']['game_ladders']).each(function(i, v) //get each ladder for this game
        {
          if (currentLadder == v.ladder_id)
          {
            $('#teamLadder').append('<option value="' + v.ladder_id + '" selected="selected">' + v.ladder_name + '</option>');
          }
          else
          {
            $('#teamLadder').append('<option value="' + v.ladder_id + '">' + v.ladder_name + '</option>');
          }
        });
      }


    });
  });
}


function remove()
{
  $('[rel=remove]').click(
    function() {
      var href = $(this).attr('data-href');

      $('#ModalLabel').text('Are you sure?');
      $('#ModalBody').text('Are you sure you want to remove this item?');
      $('#ModalButton').attr('disabled', false).unbind().text('Confirm').click(function()
      {
        window.location = href;
      });

      $('#Modal').modal({
        show: true
      });
    })
}
function toggle(id)
{
  $('.' + id).toggle();
}
function redactor()
{
  $('#rules').redactor({
    autoresize: false,
    focus: false
  });
  $('#about').redactor({
    autoresize: false,
    focus: false
  });
  $('#details').redactor({
    autoresize: false,
    focus: false
  });
}
function tooltip()
{
  $('.tooltip-on').tooltip();
}
function uploadimages()
{
  $('#avatar').click(function() {
    $('input[name=avatar]').click();
    return false;
  });
  $('#icon').click(function() {
    $('input[name=icon]').click();
    return false;
  });
  $('#logo').click(function() {
    $('input[name=logo]').click();
    return false;
  });
  $('#tile').click(function() {
    $('input[name=tile]').click();
    return false;
  });
  $('#banner').click(function() {
    $('input[name=banner]').click();
    return false;
  });
}
function datepicker()
{
  $("#time").datetimepicker(
    {
      dateFormat: "yy-mm-dd",
      stepMinute: 30,
      timeOnly: true,
      timezone: localTimezone()
    });
  $(".yymmdd").datepicker(
    {
      showSecond: false,
      dateFormat: "yy-mm-dd"
    });

  $('#startdatetime').datetimepicker(
    {
      showSecond: false,
      dateFormat: 'yy-mm-dd',
      timeFormat: 'HH:mm'
    });
  $('#checkinstartdatetime').datetimepicker(
    {
      showSecond: false,
      dateFormat: 'yy-mm-dd',
      timeFormat: 'HH:mm'
    });
  $('#checkinenddatetime').datetimepicker(
    {
      showSecond: false,
      dateFormat: 'yy-mm-dd',
      timeFormat: 'HH:mm'
    });

  $('.datetimebox').datetimepicker({
    showSecond: false,
    dateFormat: 'yy-mm-dd',
    timeFormat: 'HH:mm'
  });
}

function localTimezone()
{
  var curdate = new Date();
  var offset = curdate.getTimezoneOffset() * -60 / 3600;
  if (offset >= 0)
  {
    offset = Math.abs(offset);
    offset = '+' + offset;
  }
  $('[rel=offset]').val(offset).text(offset);
}

function initMatchservers()
{
  $('#addServer').click(function()
  {
    newrow = '';
    $.getJSON('/ajax/admin/server_locations.php', function(json) {
      newrow += '<tr>' +
        '<td><select class="span2" name="competition_type[]">' +
        '<option value="league">league</option>' +
        '<option value="ladder">ladder</option>' +
        '<option value="ladder challenge">ladder challenge</option>' +
        '<option value="tournament">tournament</option>' +
        '</select></td>' +
        '<td><input class="span2" type="textfield" name="ip[]" value="">:<input class="span1" type="textfield" name="port[]" value=""></td>' +
        '<td><input class="span2" type="textfield" name="rcon[]" value=""></td>' +
        '<td><select class="span1" name="continent[]">';
      $.each(json['continents'], function(k, v) {
        newrow += '<option value="' + v['continent'] + '">' + v['continent'] + '</option>\n';
      });
      newrow += '</select></td>' +
        '<td><select class="span2" name="region[]">';

      $.each(json['regions'], function(k, v) {
        newrow += '<option value="' + v['region'] + '">' + v['region'] + '</option>\n';
      });
      newrow += '</select></td>' +
        '<td><input type="checkbox" value="1" name="delete[]"></td>' +
        '</tr>';
      $('#serverList').append(newrow);
    });
  });
}

function addGameRole(btn, id) {
  var $btn = $(btn);
  var $role = $btn.prev(":input");

  jqxhr = $.post("/ajax/admin/editgame.php", { action: "add", game_id: id, role: $role.val() }, "json")
  .done(function(data) {
    if (data.success) {
      $("#existing_game_roles").append("<li>"+data.msg+"&nbsp;<i class='icon icon-remove red' data-id='"+id+"' onclick='removeGameRole(this, "+id+")'></i></li>");
    }
  })
  .always(function() {
    $role.val(null);
  });
}

function removeGameRole(btn, id) {
  var $btn = $(btn);
  var $role = $btn.attr("data-id");

  jqxhr = $.post("/ajax/admin/editgame.php", { action: "remove", game_id: id, id: $role }, "json")
  .done(function(data) {
    if (data.success) {
      $btn.parents("li").remove();
    }
  })
  .always(function() {
  });
}