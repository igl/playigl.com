function initForums() {
  $inputs = $(".tag-checkbox", "#new_post");
  $dates = $("span.forum_date");

  $inputs.each(function(i) {
    $a = $(this).next(".tag-link");
    $a.click(function(e) {
      e.preventDefault();
      $(this).toggleClass("active");
      $inputs.eq(i).prop("checked", !$inputs.eq(i).prop("checked"));
    });
  });

  $dates.each(function(i) {
    var $this = $(this);
    var $unix = $(this).next("input:hidden").val();
    $this.text(showLocalDate($unix) + " GMT " + localTimezone());
  });
}

function reply(btn) {
  $(btn).parents("div.show").next().toggle();
  return false;
}

function cancelReply(btn) {
  $btn = $(btn);
  $btn.parents("form").hide();
  return false;
}

function newPreview(btn) {
  $(btn).parents("form").find("[name='new_post[preview]']").val(1);
  newPost(btn);
  $(btn).parents("form").find("[name='new_post[preview]']").val(0);
  return false;
}

function newPost(btn) {
  $btn = $(btn);
  anim = $btn.find("i");

  btn.disabled = true;
  anim.prop("class", "icon-refresh icon-spin icon-large icon-white");

  //AJAX
  jqxhr = $.post("/ajax/admin/forums.php", $btn.parents("form").serialize(), "json")
    .done(function(data) {
    if (data.success) {
      if (data.preview) {
        $(".preview").html(data.preview);
      } else {
        if (data.redirect)
          window.location = data.redirect;
        else
          window.location.reload();
      }
    }
  }).fail(function() {
    alert("there was an error contacting the publishing service. Please try again later.");
  }).always(function() {
    btn.disabled = false;
    anim.prop("class", "");
  });

  return false;
}

function vote(btn, $post_id, val) {
  $btn = $(btn);
  $ref = $(btn).parents(".post, .comment");

  if ($btn.hasClass("active") || $btn.data("disabled"))
    return false;

  $btn.data("disabled", true);

  data = {
    "vote[media_id]": $post_id,
    "vote[value]": val
  };

  //AJAX
  jqxhr = $.post("/ajax/admin/forums.php", data, "json")
    .done(function(data) {
    if (data.success) {
      $btn.siblings("a").removeClass("active");
      $btn.prop("class", "active");
      $ref.find(".points").text(parseInt($ref.find(".points").text()) + parseInt(data.mod));
    } else {
      console.log(data.error);
    }
  }).fail(function() {
    alert("there was an error contacting the publishing service. Please try again later.")

  })
    .always(function() {
    $btn.data("disabled", false);
  });

  return false;
}

function deletePost(btn) {
  $btn = $(btn);
  anim = $btn.find("i");

  btn.disabled = true;
  anim.prop("class", "icon-refresh icon-spin icon-large icon-white");

  var $parent = $btn.parents(".post").length ? $btn.parents(".post") : $btn.parents(".comment");
  data = {
    "delete_post[thread_id]": $parent.attr("id").split("_")[1]
  };

  //AJAX
  jqxhr = $.post("/ajax/admin/forums.php", data, "json")
    .done(function(data) {
    if (data.redirect)
      window.location = data.redirect;
    else
      window.location.reload();
  }).fail(function() {
    alert("there was an error contacting the publishing service. Please try again later.");
  }).always(function() {
    btn.disabled = false;
    anim.prop("class", "");
  });

  return false;
}

function editPost(btn) {
  edit_txt = "Edit";
  save_txt = "Save";

  var $btn = $(btn);
  var $parent = $btn.parents(".post").length ? $btn.parents(".post") : $btn.parents(".comment");
  var $content = $parent.find(".forum-content");
  var $edit = $parent.find(".forum-edit");
  var $box = $edit.find("textarea");

  if ($btn.text().trim() == edit_txt) {
    $content.hide();
    $box.val($content.text());
    $edit.show();
    $btn.html($btn.html().replace($btn.text().trim(), save_txt));
  } else {
    var anim = $btn.find("i");
    var data = {
    };
    anim.prop("class", "icon-refresh icon-spin icon-large icon-white");

    $edit.find(":input").each(function() {
      var el = $(this);
      data[el.attr("name")] = el.val();
    });

    //AJAX
    jqxhr = $.post("/ajax/admin/forums.php", data, "json")
      .done(function(data) {
        if (data.success) {
          $edit.hide();
          $box.val(null);
          $content.html(data.txt).show();
        }
    }
    ).fail(function() {
      alert("there was an error contacting the publishing service. Please try again later.");
    }).always(function() {
      anim.prop("class", "");
      $btn.html($btn.html().replace($btn.text().trim(), edit_txt));
    });
  }

  return false;
}