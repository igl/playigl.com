/*
 * These elements are used exclusively in /view/team.php
 */

var Dashboard, dashboard, DashboardHTML, d, t, urlRoot;
var sync = true;
var hawk = _.extend({
}, Backbone.Events);
Backbone.emulateJSON = true;
$dashboard = $("#ModalBody");

function progressBar($el) {
  $el.empty().html("<div class=\"progress progress-striped active\"><div class=\"bar\" style=\"width: 100%;\"></div></div>");
}

function saveState(btn, $new_text, b) {
  $btn = $(btn);
  anim = $btn.find("i");

  if (b === false) {
    $btn.attr("disabled", null);
    anim.attr("class", null);
  } else {
    btn.disabled = true;
    anim.attr("class", "icon-refresh icon-spin icon-large icon-white");
  }

  $btn.html($btn.html().replace($btn.text().trim(), $new_text));
}

function initTeam() {
  initFollow();
  loadComments('team');

  $("#team_like").click(function() {
    mixpanel.track("User Profile Like");
  });
  $("#team_tweet").click(function() {
    mixpanel.track("User Profile Tweet");
  });
  $('#comment').limiter(130, $("#chars"));

  $('.tooltip-on').tooltip();
}

function joinTeam(team, player, slot) {
  $('#ModalLabel').text('JOIN TEAM');
  $('#ModalButton').hide();

  $('#ModalBody').html('<div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div>');
  $('#Modal').modal({
    show: true
  });
  $('#ModalBody').load('/player_joinrequest/' + team + '/' + player + '/' + slot);
}

function joinTeamSubmit(team, player) {
  $('#ModalButton').attr('disabled', true).text('WAIT...');

  $.ajax({
    url: '/player_joinrequest/' + team + '/' + player,
    success: function(data) {
      $('#ModalBody').html(data);
    }
  });

  $('#Modal').modal('hide');
}

function player_send_joinrequest(team, player) {
  $('#ModalButton').attr('disabled', true).text('SENDING...');
  elem = $('#ModalBody');
  elem.html('<div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div>');

  $.getJSON('/player_send_joinrequest/' + team + '/' + player, function(data) {
    if (data === null)
      elem.html('<div class="alert alert-error">Error. Please try again.</div>');
    else
    {
      if (data.success)
      {
        $('#Modal').modal('hide');
        alert('Invite sent!');
      }
      else
        elem.html('<span class="label label-important">Error. Please try again.</span>');
    }
  });
}

function player_join_password(team, player, slot)
{
  $('#join_button').attr('disabled', true).html('<i class="icon-time"></i> WAIT...');
  formData = $('#join_with_password').serialize();

  $.ajax({
    type: "POST",
    url: "/player_join_password/" + team + '/' + player,
    data: formData,
    dataType: "json",
    success: function(json) {
      if (json.success)
      {
        $('#Modal').modal('hide');
        $('#join_button').attr('disabled', true).text('<i class="icon-ok"></i>');
        $('#open_' + slot).html('<a href="/player/' + json.player_id + '-' + json.username + '"><p><img src="/img/roster_filled.png"></a><br>' + json.firstname + '<h5><a href="/player/' + json.player_id + '-' + json.username + '">' + json.username + '</a></h5>' + json.lastname + '</p>');
        $('.join_team').hide();
      }
      else
      {
        $('#warning').html('<div class="alert alert-error">' + json.error + '</div>');
        $('#join_button').attr('disabled', false).text('JOIN');
      }
    },
    error: function(xhr, textStatus, errorThrown) {
      alert(errorThrown);
    }
  });
}

function editTeam(id) {
  var team = new TeamBuilder({
    id: id,
    team_name: "Hoozits"
  });
  var teamHTML = new TeamBuilderHTML({
    model: team,
    hawk: hawk,
    target: $dashboard
  });
  $('#Modal').modal({
    show: true
  });
}