/* 
 * These elements are used exclusively in /view/player.php
 */
/*
function init() {
    $.fn.extend({
        limiter: function(limit, elem) {
            $(this).on("keyup focus", function() {
                setCount(this, elem);
            });
            function setCount(src, elem) {
                var chars = src.value.length;
                if (chars > limit) {
                    src.value = src.value.substr(0, limit);
                    chars = limit;
                }
                elem.html(limit - chars);
            }
            setCount($(this)[0], elem);
        }
    });
}
*/
function timeConverter(UNIX_timestamp) {
    var offset = new Date().getTimezoneOffset();
    var a = new Date((UNIX_timestamp + (offset * 60)) * 1000);
    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    var year = a.getFullYear();
    var month = months[a.getMonth()];
    var date = a.getDate();
    var hour = a.getHours();
    var min = a.getMinutes();
    var sec = a.getSeconds();
    var time = month + ' ' + date + ',' + ' ' + hour + ':' + min + ':' + sec;
    return time;
}


/*
function updateFreeagents(game, player) {
  $.getJSON('/api/freeagents/' + game, function(json) {

    if (player > 0)
    {
      if (json.on_team) //dont allow enlisting
      {
        $('#button_enlist').show().text('Enlist').attr('title', 'You are already on a team').addClass('tooltip-on');
      }
      else if (json.is_agent) //show remove option
      {
        $('#button_enlist').show().text('Remove').attr('title', 'Remove from free agents').addClass('btn-red tooltip-on').attr('onclick', 'enlist(' + player + ',' + game + ',\"remove\")');
      }
      else //show enlist option
      {
        $('#button_enlist').show().text('Enlist').attr('title', 'Register as a free agent').addClass('tooltip-on').attr('onclick', 'enlist(' + player + ',' + game + ',\"enlist\")');
      }
    }
    else
    {
      $('#button_enlist').hide();
    }

    if (json.success)
    {
      appendrow = '<tr class="hide"><td colspan="5"></td></tr>';
      $.each(json.freeagents, function(key, value) {
        joindate = timeConverter(value.player_joindate);
        appendrow += '<tr class="player_' + value.player_id + '"><td><img src="' + value.player_flag + '"></td><td><a title="View details" class="tooltip-on" onclick="toggleAgent(' + value.player_id + ')">' + value.player_firstname + ' "' + value.player_username + '" ' + value.player_lastname + '</a></td><td>' + value.player_city + ' ' + value.player_state + '</td>';

        if (json.game_data['game_steamauth'] == 1)
        {
          appendrow += '<td><a title="View steam profile" class="tooltip-on" href="http://steamcommunity.com/profiles/' + value.player_community_id + '" target="_blank">' + value.player_steam_id + '</a></td>';
          colspan = 6;
        }
        else
        {
          colspan = 5;
        }

        appendrow += '<td>' + joindate + '</td></tr>\n';

        if (value.player_invited)
        {
          appendrow += '<tr class="player_' + value.player_id + ' hide toggleAgent_' + value.player_id + '"><td colspan="' + colspan + '" class="pad10"><a href="/player/' + value.player_username + '"><img src="/img' + value.player_avatar + '" class="img-rounded loading feed" alt="Avatar"></a><div style="margin-left:6px; display:inline-block; width:480px" class="well well-small"><p><i class="icon-quote-left pull-left"></i> ' + value.player_reason + ' <i class="icon-quote-right pull-right"></i></p></div></td></tr>';
          appendrow += '<tr class="player_' + value.player_id + ' hide toggleAgent_' + value.player_id + '"><td colspan="' + colspan + '" class="pad10"><div><div class="pull-right"><button class="btn" id="invite_' + value.player_id + '" onclick="invite(' + value.player_id + ',' + json.captains_team + ',\'revoke\')">Revoke</button> <a href="/player/' + value.player_username + '" type="button" class="btn">View Profile</a> <button type="button" class="btn btn-red" onclick="toggleAgent(' + value.player_id + ')">Close</button></div></div></td></tr>';
        }
        else
        {
          appendrow += '<tr class="player_' + value.player_id + ' hide toggleAgent_' + value.player_id + '"><td colspan="' + colspan + '" class="pad10"><a href="/player/' + value.player_username + '"><img src="/img' + value.player_avatar + '" class="img-rounded loading feed" alt="Avatar"></a><div style="margin-left:6px; display:inline-block; width:480px" class="well well-small"><p><i class="icon-quote-left pull-left"></i> ' + value.player_reason + ' <i class="icon-quote-right pull-right"></i></p></div></td></tr>';
          appendrow += '<tr class="player_' + value.player_id + ' hide toggleAgent_' + value.player_id + '"><td colspan="' + colspan + '" class="pad10"><div><div class="pull-right"><button class="btn" id="invite_' + value.player_id + '" onclick="invite(' + value.player_id + ',' + json.captains_team + ',\'invite\')">Invite</button> <a href="/player/' + value.player_username + '" type="button" class="btn">View Profile</a> <button type="button" class="btn btn-red" onclick="toggleAgent(' + value.player_id + ')">Close</button></div></div></td></tr>';
        }


      });
      $('#freeagentlist tbody').html(appendrow);
    }
    else
    {
      $('#freeagentlist tbody').html('<tr id="no_agents"><td colspan="5">No free agents available.</td></tr>');
    }
    $('.tooltip-on').tooltip();
  });
} */

function enlist(player, game, action) {
    $('#button_enlist').html('Doin\' Stuff <i class="icon-spin icon-refresh"></i>').attr('disabled', true);
    $.getJSON('/enlist/' + player + '/' + game + '/' + action, function(data) {

        if (typeof data.success == undefined)
        {
            alert('Sorry, there was a problem with this request. Please try again.');
        }
        else
        {
            if (data.success)
            {
                if (action == 'enlist')
                {
                    var roles_container = "Desired Role: <select name='role'>:roles</select>";
                    var roles = new Array();
                    for(var i = 0; i < data.roles.length; i++) {
                        roles.push("<option value='"+data.roles[i].id+"'>"+data.roles[i].category+"</option>");
                    }
                    if (roles.length) {
                        roles_container = roles_container.replace(":roles", roles.join(""));
                    } else {
                        roles_container = "";
                    }
          
                    $('#freeagentModalLabel').text('Tell us about yourself...');
                    $('#freeagentModalBody').html('<form id="whyRecruit"><textarea id="why" name="why" style="width:518px; height:100px"></textarea><br><small>You have <span id="chars"></span> characters left.</small><br>'+roles_container+'</form>');
                    $('#freeagentModal').modal({
                        show: true
                    })

                    $('.close').click(function() {
                        $('#button_enlist').html('Enlist');
                        enlist(player, game, 'remove');
                        updateFreeagents(game, player);
                    });

                    $('#freeagentModalButton').attr('disabled', false).unbind().text('SAVE').click(function() {

                        $('#freeagentModalButton').attr('disabled', true).unbind().text('SAVING...');
                        formData = $('#whyRecruit').serialize();
                        $.ajax({
                            type: "POST",
                            url: "/enlist/reason/" + player + '/' + game,
                            data: formData,
                            dataType: "json",
                            success: function(json) {
                                if (json.success)
                                {
                                    updateFreeagents(game, player);
                                    $('#freeagentModal').modal('hide');
                                    $('#no_agents').hide();
                                }
                                else
                                {
                                    alert(json.error);
                                    $('#button_enlist').html('Enlist');
                                    $('#freeagentModalButton').attr('disabled', false).unbind().text('SAVE');
                                }
                            },
                            error: function(xhr, textStatus, errorThrown) {
                                alert(errorThrown);
                            }
                        });


                    });
                /*
          var elem = $("#chars");
          $('#why').limiter(140, elem); 
       */


                }
                if (action == 'remove')
                {
                    $('#freeagentModal').modal('hide');
                }
                document.location.reload(true);
            }
            else
            {
                if (data.error == undefined)
                {
                    alert('Unknown error while trying to ' + action);
                    $('#button_enlist').html('Enlist');
                    updateFreeagents(game, player);
                }
                else
                {
                    $('#button_enlist').html('Enlist');
                    alert(data.error);
                }

            }
        }
        $('#button_enlist').attr('disabled', false);
    });
}

function toggleAgent(id)
{
    $('.toggleAgent_' + id).toggle('slow');
}
