var Dashboard, dashboard, DashboardHTML, d, t, urlRoot;
var sync = true;
var hawk = _.extend({
}, Backbone.Events);
Backbone.emulateJSON = true;
$dashboard = $("#dashboardresults");

switchView(current_view);

function progressBar($el) {
  $el.empty().html("<div class=\"progress progress-striped active\"><div class=\"bar\" style=\"width: 100%;\"></div></div>");
}

function saveState(btn, $new_text, b) {
  $btn = $(btn);
  anim = $btn.find("i");

  if (b === false) {
    $btn.attr("disabled", null);
    anim.attr("class", null);
  } else {
    btn.disabled = true;
    anim.attr("class", "icon-refresh icon-spin icon-large icon-white");
  }

  $btn.html($btn.html().replace($btn.text().trim(), $new_text));
}

function switchView(new_view) {
  if (!sync && new_view === current_view)
    return false;

  current_view = new_view;

  d = getNewData();

  progressBar($dashboard);

  switch (current_view) {
    case 'profile':
      switchToProfile();
      break;
    case 'teams':
      switchToTeams();
      break;
    case 'matches':
      switchToMatches();
      break;
    case 'referrals':
      switchToReferrals();
      break;
    case 'following':
      switchToFollowing();
      break;
  }

  if (sync)
    sync = false;

  return false;
}

function getNewData() {
  if (sync)
    return false;

  return true;
}

function checkDOM(section, $el, id) {
  if ($("#" + section + id, $el).length)
    return false;

  if ($el.parent().children("tr[id$='_" + id + "']").length)
    $el.parent().children("tr[id$='_" + id + "']").remove();

  $("<tr id='" + section + id + "'><td colspan='3'></td></tr>").insertAfter($el);

  return true;
}

function checkTable(section, $el, id, $target) {
  if ($("#" + section + id, $el).length)
    return false;

  if ($el.find("tbody").children("tr[id^='" + section + "']").length)
    $el.find("tbody").children("tr[id^='" + section + "']").remove();

  $("<tr id='" + section + id + "'><td colspan='99'></td></tr>").insertAfter($target);

  return true;
}

function switchToProfile() {
  var Profile = Backbone.Model.extend({
    modelName: "profile",
    url: "/backbone/profile/"
  });

  var ProfileHTML = Backbone.View.extend({
    tagName: "form",
    className: "form form-horizontal",
    template: _.template($("#profile_template").html()),
    avatar_template: _.template($("#avatar_template").html()),
    initialize: function(opt) {
      this.target = $dashboard;

      if (!d) {
        var that = this;
        _.each(data, function(value, key, list) {
          that.model.set(key, value);
        });
        this.addToDOM();
      } else {
        this.model.on("sync", this.addToDOM, this);
        this.model.fetch();
      }
    },
    events: {
      "click button[name='save']": "saveProfile",
      "change input:file[name='avatar']": "uploadAvatar"
    },
    addToDOM: function() {
      this.target.empty();
      this.target.html(this.render().el);
    },
    render: function() {
      this.$el.html(this.template(this.model.attributes));
      this.$el.find("select[name='sex']").val(this.model.get("sex").toUpperCase());
      this.$el.find("select[name='country']").val(this.model.get("country").toUpperCase());
      this.$el.find("select[name='time_zone_offset']").val(this.model.get("time_zone_offset"));
      this.$el.append(this.avatar_template({ id: this.model.get("id"), field_name: "avatar", avatar: this.model.get("avatar"), media_type: "player"}));
      return this;
    },
    uploadAvatar: function(e) {
      var that = this;
      var $input = $(e.target);
      var $modal = $input.parents(".modal");
      var $form = $modal.find("form[target]");
      var $avatar = $modal.find("img");

      $avatar.fadeTo('slow', 0.5);
      $form.submit();

      $modal.find("iframe").load(function() {
        $(this).unbind('load');
        var success = $.trim($(this).contents().text());

        if (success !== "false") {
          var src = success+ '?' + Math.random();
          $avatar.attr("src", src);
          $avatar.load(function() {
            $avatar.fadeTo('slow', 1);
            that.updateAvatar(success, src);
          });
        } else {
          $avatar.fadeTo('slow', 1);
          console.log("An error occured uploading a avatar");
        }
      });
    },
    updateAvatar: function(id, src) {
      this.model.set("avatar", id);
      this.$el.find(".dashboard-avatar").attr("src", src);
    },
    saveProfile: function(e) {
      e.preventDefault();
      saveState(e.target, "SAVING");

      var formData = {
        ingame_name : {
        }
      };
      var that = this;

      //get form data
      $(e.target).parents("form").find(":input").not(":file").not("button").each(function() {
        var el = $(this);
        if (el.attr("name") == "game_label") {
          formData["ingame_name"][el.attr("data-id")] = el.val();
        } else {
          formData[el.attr("name")] = el.val();
        }
      });
      
      formData['birthday'] = formData['birthday-year'] + "-" + formData['birthday-month'] + "-" + formData['birthday-day'];

      //Custom fields for Avatar AJAX
      delete formData.id;
      delete formData.field;
      delete formData.media_type;
      delete formData['birthday-year'];
      delete formData['birthday-month'];
      delete formData['birthday-day'];

      formData["ingame_name"] = JSON.stringify(formData["ingame_name"]);

      this.model.save(formData, {
        success: function() {
          that.model.set("ingame_name", JSON.parse(formData["ingame_name"]));
          that.render();
        }
      });
    }
  });

  Dashboard = new Profile();
  dashboard = new ProfileHTML({
    model: Dashboard
  });
}

function switchToTeams() {
  urlRoot = "/backbone/team/";

  var Team = Backbone.Model.extend({
    modelName: "team",
    urlRoot: urlRoot
  });

  var TeamCreate = Backbone.Model.extend({
    modelName: "team_create",
    urlRoot: urlRoot
  });

  var TeamAdmin = Backbone.Model.extend({
    modelName: "team_admin",
    urlRoot: urlRoot
  });

  var TeamDetails = Backbone.Model.extend({
    modelName: "team_details",
    urlRoot: urlRoot
  });

  var TeamEdit = Backbone.Model.extend({
    modelName: "team_edit",
    urlRoot: urlRoot
  });

  var TeamPassword = Backbone.Model.extend({
    modelName: "team_password",
    urlRoot: urlRoot
  });

  var TeamLeave = Backbone.Model.extend({
    modelName: "team_leave",
    urlRoot: urlRoot
  });

  var TeamCreateHTML = Backbone.View.extend({
    tagName: "form",
    className: "form form-horizontal",
    template: _.template($("#team_create_template").html()),
    initialize: function(opt) {
      this.hawk = opt.hawk;
      this.target = $(".create-team-form");
      this.model.set("action", "create new team");
      this.model.set("status", 1);
      this.addToDOM();
    },
    events: {
      "click button[name='save']": "save",
      "click button[name='close']": "dispose"
    },
    addToDOM: function() {
      this.target.empty();
      this.target.hide();
      this.target.html(this.render().el);
      this.target.slideDown();
    },
    render: function() {
      this.$el.html(this.template(this.model.attributes));
      return this;
    },
    dispose: function() {
      $("button[name='create']").show();
      this.$el.parent().hide();
      this.$el.remove();
      this.remove();
    },
    save: function(e) {
      saveState(e.target, "Creating Team");

      var formData = {
      };
      var that = this;

      //get form data
      $(e.target).parents("form").find(":input").not(":file").not("button").each(function() {
        var el = $(this);
        formData[el.attr("name")] = el.val();
      });

      formData.all_games = null;
      formData.disallowed_games = null;

      this.model.save(formData, {
        success: function() {
          that.dispose();
          dashboard.model.fetch({
            data: $.param({
              action: "get all player teams and invites"
            })
          });
        }
      });
    }
  });

  var TeamDetailsHTML = Backbone.View.extend({
    tagName: "form",
    className: "form form-horizontal",
    template: _.template($("#team_details_template").html()),
    avatar_template: _.template($("#avatar_template").html()),
    initialize: function(opt) {
      this.hawk = opt.hawk;
      this.target = $("#team_details_" + this.model.get("id") + " td");
      this.model.on("sync", this.addToDOM, this);
      this.model.fetch({
        data: $.param({
          action: "get team data"
        })
      });

      progressBar(this.target);
    },
    events: {
      "click button[name='save']": "save",
      "click button[name='cancel']": "dispose",
      "change input:file[name='avatar']": "uploadAvatar"
    },
    addToDOM: function() {
      this.target.empty();
      this.target.html(this.render().el);
    },
    render: function() {
      this.$el.html(this.template(this.model.attributes));
      this.$el.find("select[name='location']").val(this.model.get("location"));
      console.log(this.$el.find("select[name='location']"));
      this.$el.append(this.avatar_template({ id: this.model.get("id"), field_name: "logo", avatar: this.model.get("avatar"), media_type: "team"}));
      return this;
    },
    dispose: function() {
      this.$el.parent().remove();
      this.remove();
    },
    uploadAvatar: function(e) {
      var that = this;
      var $input = $(e.target);
      var $modal = $input.parents(".modal");
      var $form = $modal.find("form[target]");
      var $avatar = $modal.find("img");

      $avatar.fadeTo('slow', 0.5);
      $form.submit();

      $modal.find("iframe").load(function() {
        $(this).unbind('load');
        var success = $.trim($(this).contents().text());

        if (success !== "false") {
          var src = "/img" + success + '?' + Math.random();
          $avatar.attr("src", src);
          $avatar.load(function() {
            $avatar.fadeTo('slow', 1);
            that.updateAvatar(success, src);
          });
        } else {
          $avatar.fadeTo('slow', 1);
          console.log("An error occured uploading a avatar");
        }
      });
    },
    updateAvatar: function(id, src) {
      this.model.set("avatar", id);
      this.$el.find(".dashboard-avatar").attr("src", src);
    },
    save: function(e) {
      saveState(e.target, "Saving Changes");

      var formData = {
      };
      var that = this;

      //get form data
      $(e.target).parents("form").find(":input").not(":file").not("button").each(function() {
        var el = $(this);
        formData[el.attr("name")] = el.val();
      });

      //Custom fields for Avatar AJAX
      delete formData.id;
      delete formData.field;
      delete formData.media_type;

      this.model.set("action", "save team details");
      this.model.save(formData, {
        success: function() {
          that.hawk.trigger("updateTeamData", that.model.get("id"));
          that.dispose();
        }
      });
    }
  });

  var TeamPasswordHTML = Backbone.View.extend({
    tagName: "form",
    className: "form form-horizontal text-right",
    template: _.template($("#team_password_template").html()),
    initialize: function(opt) {
      this.hawk = opt.hawk;
      this.target = $("#team_password_" + this.model.get("id") + " td");
      this.model.on("sync", this.addToDOM, this);
      this.model.fetch({
        data: $.param({
          action: "get team password"
        })
      });

      progressBar(this.target);
    },
    events: {
      "click button[name='save']": "save",
      "click button[name='remove']": "save",
      "click button[name='close']": "dispose"
    },
    addToDOM: function() {
      this.target.empty();
      this.target.html(this.render().el);
    },
    render: function() {
      this.$el.html(this.template(this.model.attributes));
      return this;
    },
    dispose: function() {
      this.$el.parent().remove();
      this.remove();
    },
    save: function(e) {
      if ($(e.target).prop("name") === 'save')
        saveState(e.target, "Saving");
      else
        saveState(e.target, "Removing");

      var formData = {
      };
      var that = this;

      //get form data
      $(e.target).parents("form").find(":input").not("button").each(function() {
        var el = $(this);
        formData[el.attr("name")] = el.val();
      });

      this.model.set("action", "save team password");
      this.model.save(formData, {
        success: function() {
          that.hawk.trigger("updateTeamData", that.model.get("id"));
          that.dispose();
        }
      });
    }
  });

  var TeamLeaveHTML = Backbone.View.extend({
    tagName: "form",
    className: "form form-horizontal text-right",
    template: _.template($("#team_leave_template").html()),
    initialize: function(opt) {
      this.hawk = opt.hawk;
      this.target = $("#team_leave_" + this.model.get("id") + " td");
      this.listenTo(this.model, "destroy", this.dispose);

      progressBar(this.target);
      this.addToDOM();
    },
    events: {
      "click button[name='save']": "save",
      "click button[name='close']": "dispose"
    },
    addToDOM: function() {
      this.target.empty();
      this.target.html(this.render().el);
    },
    render: function() {
      this.$el.html(this.template(this.model.attributes));
      return this;
    },
    dispose: function() {
      this.$el.parent().remove();
      this.remove();
    },
    save: function(e) {
      saveState(e.target, "Leaving");

      var that = this;

      this.model.set("action", "leave team");
      this.model.save({
      }, {
        success: function() {
          that.model.destroy({
            wait: true,
            success: function() {
              that.hawk.trigger("removeTeamData", that.model.get("id"));
              that.dispose();
            }
          });
        }
      });
    }
  });

  var TeamHTML = Backbone.View.extend({
    template: _.template($("#teams_template").html()),
    initialize: function(opt) {
      this.hawk = opt.hawk;
      this.target = $dashboard;

      _.bindAll(this, "updateTeamData");
      _.bindAll(this, "removeTeamData");
      opt.hawk.bind("updateTeamData", this.updateTeamData);
      opt.hawk.bind("removeTeamData", this.removeTeamData);

      this.model.on("sync", this.parseData, this);

      if (!d) {
        var that = this;
        _.each(data, function(value, key, list) {
          that.model.set(key, value);
        });
        this.parseData();
      } else {
        this.model.fetch({
          data: $.param({
            action: "get all player teams and invites"
          })
        });
      }
    },
    events: {
      "click button[name='create']": "createTeam",
      "click button.accept": "acceptInvite",
      "click button.reject": "rejectInvite",
      "click button.followers": "viewFollowers",
      "click button.details": "detailsTeam",
      "click button.edit": "editTeam",
      "click button.password": "setPassword",
      "click button.build": "buildTeam",
      "click button.leave": "leaveTeam"
    },
    addToDOM: function() {
      this.target.empty();
      this.target.html(this.render().el);
    },
    render: function() {
      this.$el.html(this.template(this.model.attributes));
      this.delegateEvents();
      return this;
    },
    dispose: function() {
      this.$el.parent().remove();
      this.remove();
    },
    parseData: function() {
      var Collection = Backbone.Collection.extend({
        model: TeamAdmin,
        hawk: hawk
      });

      this.model.set("teams", new Collection(this.model.get("teams")));
      this.model.set("invites", new Collection(this.model.get("invites")));
      this.addToDOM();
    },
    updateTeamData: function(id) {
      if (_.contains(this.model.get("teams").pluck("id"), id)) {
        var model = this.model.get("teams").get(id);

        model.on("sync", this.addToDOM, this);
        model.fetch({
          data: $.param({
            action: "get player team"
          })
        });
      }
    },
    removeTeamData: function(id) {
      if (_.contains(this.model.get("teams").pluck("id"), id)) {
        var collection = this.model.get("teams");
        var model = collection.get(id);

        collection.once("remove", this.addToDOM, this);
        collection.remove(model);
      }
    },
    createTeam: function(e) {
      e.preventDefault();
      $(e.target).hide();

      var team = new TeamCreate({
        all_games: all_games,
        disallowed_games: this.model.get("teams").pluck("game")
      });
      var teamHTML = new TeamCreateHTML({
        model: team,
        hawk: hawk
      });
    },
    acceptInvite: function(e) {
      e.preventDefault();
      saveState(e.target, "Accepting Invite");

      var that = this;
      var invite = this.model.get("invites").get($(e.target).attr("data-id")).clone();

      invite.set("action", "accept team invite");
      
      invite.save({
      }, {
        success: function() {
          that.model.fetch({
          data: $.param({
            action: "get all player teams and invites"
          })
        });
        }
      });
      
    }, 
    rejectInvite: function(e) {
      e.preventDefault();
      saveState(e.target, "Rejecting Invite");
      
      var that = this;     
      var invite = this.model.get("invites").get($(e.target).attr("data-id")).clone();
      invite.clear();

      invite.set("action", "reject team invite");
      invite.set("id", $(e.target).attr("data-id"));
      this.listenToOnce(this.model.get("invites"), "remove", this.addToDOM);

      invite.save({
      }, {
        success: function() {
          that.model.get("invites").remove(invite);
        }
      });
    },
    viewFollowers: function(e) {
      e.preventDefault();
      viewFollowers("team", $(e.target).attr("data-id"));
    },
    detailsTeam: function(e) {
      if (checkDOM("team_details_", $(e.target).parents("tr"), $(e.target).attr("data-id"))) {
        var team = new TeamDetails({
          id: $(e.target).attr("data-id")
        });
        var teamHTML = new TeamDetailsHTML({
          model: team,
          hawk: hawk
        });
      }
    },
    editTeam: function(e) {
      if (checkDOM("team_edit_", $(e.target).parents("tr"), $(e.target).attr("data-id"))) {
        var team = new TeamEdit({
          id: $(e.target).attr("data-id")
        });
        var teamHTML = new TeamEditHTML({
          model: team,
          hawk: hawk
        });
      }
    },
    setPassword: function(e) {
      if (checkDOM("team_password_", $(e.target).parents("tr"), $(e.target).attr("data-id"))) {
        var team = new TeamPassword({
          id: $(e.target).attr("data-id")
        });
        var teamHTML = new TeamPasswordHTML({
          model: team,
          hawk: hawk
        });
      }
    },
    buildTeam: function(e) {
      if (checkDOM("team_builder_", $(e.target).parents("tr"), $(e.target).attr("data-id"))) {
        var team = new TeamBuilder({
          id: $(e.target).attr("data-id"),
          team_name: this.model.get("teams").get($(e.target).attr("data-id")).get("name")
        });
        var teamHTML = new TeamBuilderHTML({
          model: team,
          hawk: hawk
        });
      }
    },
    leaveTeam: function(e) {
      if (checkDOM("team_leave_", $(e.target).parents("tr"), $(e.target).attr("data-id"))) {
        var team = new TeamLeave({
          id: $(e.target).attr("data-id")
        });
        var teamHTML = new TeamLeaveHTML({
          model: team,
          hawk: hawk
        });
      }
    }
  });

  Dashboard = new Team();
  dashboard = new TeamHTML({
    model: Dashboard,
    hawk: hawk
  });
}

function switchToReferrals() {
  var Invite = Backbone.Model.extend({
    modelName: "invite"
  });

  var Referral = Backbone.Model.extend({
    modelName: "referral",
    url: "/backbone/referrals/"
  });

  var ReferralHTML = Backbone.View.extend({
    tagName: "div",
    className: "row-fluid",
    template: _.template($("#referral_template").html()),
    initialize: function(opt) {
      this.target = $dashboard;

      if (!d) {
        var that = this;
        _.each(data, function(value, key, list) {
          that.model.set(key, value);
        });
        this.parseData();
      } else {
        this.model.once("sync", this.parseData, this);
        this.model.fetch();
      }
    },
    addToDOM: function() {
      this.target.empty();
      this.target.html(this.render().el);
    },
    render: function() {
      this.$el.html(this.template(this.model.attributes));
      this.delegateEvents();
      return this;
    },
    events: {
      "click button[name='save']": "sendInvite",
      "click .icon-remove": "revokeInvite"
    },
    validate: function(attrs, opt) {
      if (!attrs.email.indexOf("@") || !attrs.email.length)
        return "You must specify an email address";
      if (_.contains(this.model.get("invites").pluck("email"), attrs.email))
        return "You have already emailed that party";
      return true;
    },
    parseData: function() {
      var Invites = Backbone.Collection.extend({
        model: Invite,
        url: "/backbone/referrals/"
      });

      this.model.set("invites", new Invites(this.model.get("invites")));
      this.addToDOM();
    },
    calculateConversion: function() {
      this.model.set("progress_percent", 100 * Math.round(this.model.get("progress") / this.model.get("invites").length));
      this.addToDOM();
    },
    showError: function(error) {
      $dashboard.prepend("<div class=\"alert alert-error\"><button data-dismiss=\"alert\" class=\"close\" type=\"button\"><i class=\"icon-remove\"></i></button>" + error + "</div>");
    },
    sendInvite: function(e) {
      e.preventDefault();
      saveState(e.target, "SENDING");
      $(".alert", $dashboard).remove();

      var formData = {
      };
      var that = this;
      var model = this.model.clone();
      model.clear();

      //get form data
      $(e.target).parents("form").find(":input").not("button").each(function() {
        var el = $(this);
        formData[el.attr("name")] = el.val();
      });

      error = this.validate(formData);
      if (error !== true) {
        this.showError(error);
        saveState(e.target, "SEND", false);
      } else {
        this.listenToOnce(this.model, "sync", this.parseData);
        model.save(formData, {
          success: function() {
            that.model.clear();
            that.model.fetch();
          }
        });
      }
    },
    revokeInvite: function(e) {
      e.preventDefault();

      saveState(e.target, "");

      var that = this;
      var id = $(e.target).attr("data-id");
      var referral = this.model.get("invites").get(id);
      referral.clear();

      referral.set("action", "revoke referral");
      referral.set("id", id);
      this.listenToOnce(this.model.get("invites"), "remove", this.calculateConversion);

      referral.save({
      }, {
        success: function() {
          referral.destroy();
        }
      });
    }
  });

  Dashboard = new Referral();
  dashboard = new ReferralHTML({
    model: Dashboard
  });
}

function switchToFollowing() {
  var Followee = Backbone.Model.extend({
    modelName: "followee",
    url: "/backbone/followee/"
  });

  var FolloweeHTML = Backbone.View.extend({
    tagName: "div",
    className: "row-fluid",
    template: _.template($("#followee_template").html()),
    initialize: function(opt) {
      this.target = $dashboard;

      if (!d) {
        var that = this;
        _.each(data, function(value, key, list) {
          that.model.set(key, value);
        });
        this.parseData();
      } else {
        this.model.once("sync", this.parseData, this);
        this.model.fetch();
      }
    },
    addToDOM: function() {
      this.target.empty();
      this.target.html(this.render().el);
    },
    render: function() {
      this.$el.html(this.template(this.model.attributes));
      this.delegateEvents();
      return this;
    },
    events: {
      "click button[name='save']": "sendInvite",
      "click .icon-remove": "revokeInvite"
    },
    parseData: function() {
      var Follows = Backbone.Collection.extend({
        model: Followee,
        url: "/backbone/followee/"
      });

      this.model.set("follows", new Follows(this.model.get("follows")));
      this.addToDOM();
    }
  });

  Dashboard = new Followee();
  dashboard = new FolloweeHTML({
    model: Dashboard
  });
}

function switchToMatches() {
  urlRoot = "/backbone/match/";

  var Match = Backbone.Model.extend({
    modelName: "match",
    urlRoot: urlRoot
  });

  var Score = Backbone.Model.extend({
    modelName: "score",
    urlRoot: urlRoot
  });

  var MatchesHTML = Backbone.View.extend({
    template: _.template($("#matches_template").html()),
    initialize: function(opt) {
      this.target = $dashboard;
      this.hawk = opt.hawk;
      _.bindAll(this, "updateMatches");
      opt.hawk.bind("updateMatches", this.updateMatches);

      if (!d) {
        var that = this;
        _.each(data, function(value, key, list) {
          that.model.set(key, value);
        });
        this.parseData();
      } else {
        this.model.on("sync", this.parseData, this);
        this.model.fetch({
          data: $.param({
            action: "get all matches for player"
          })
        });
      }
    },
    events: {
      "click .score": "submitForm",
      "click .accept": "acceptChallenge",
      "click .reject": "rejectChallenge",
      "click .withdraw": "rejectChallenge",
      "click .matchurl": "saveMatchURL"
    },
    addToDOM: function() {
      this.target.empty();
      this.target.html(this.render().el);
    },
    render: function() {
      this.$el.html(this.template(this.model.attributes));
      this.delegateEvents();
      return this;
    },
    dispose: function() {
      this.$el.parent().remove();
      this.remove();
    },
    updateMatches: function(args) {
      var that = this;
      _.each(this.model.get(args.collection).models, function(item) {
        if (item.get("id") == args.id) {
          that.listenToOnce(that.model, "sync", that.parseData);
          that.model.fetch({
            data: $.param({
              action: "get all matches for player"
            })
          });
        }
      }, this);
    },
    parseData: function() {
      var Matches = Backbone.Collection.extend({
        model: Match,
        url: urlRoot
      });

      this.model.set("challenge", new Matches(this.model.get("challenge")));
      this.model.set("upcoming", new Matches(this.model.get("upcoming")));
      this.model.set("completed", new Matches(this.model.get("completed")));

      this.addToDOM();
    },
    acceptChallenge: function(e) {
      e.preventDefault();
      saveState(e.target, "Accepting");

      var that = this;
      var challenge = this.model.get("challenge").get($(e.target).attr("data-id")).clone();

      challenge.set("action", "accept challenge");
      challenge.save({
      }, {
        success: function() {
          that.hawk.trigger("updateMatches", {
            collection: "challenge",
            id: challenge.get("id")
          });
          challenge.destroy();
        }
      });
    },
    rejectChallenge: function(e) {
      e.preventDefault();

      if ($(e.target).hasClass("reject"))
        saveState(e.target, "Rejecting");
      else
        saveState(e.target, "Withdrawing");

      var that = this;
      var challenge = this.model.get("challenge").get($(e.target).attr("data-id")).clone();
      challenge.clear();

      challenge.set("action", "reject challenge");
      challenge.set("id", $(e.target).attr("data-id"));
      this.listenToOnce(this.model.get("challenge"), "remove", this.addToDOM);

      challenge.save({
      }, {
        success: function() {
          challenge.destroy();
        }
      });
    },
    saveMatchURL: function(e) {
      e.preventDefault();

      saveState(e.target, "");

      var that = this;
      var matchurl = this.model.get("upcoming").get($(e.target).attr("data-id")).clone();
      matchurl.clear();

      matchurl.set("action", "submit match url");
      matchurl.set("id", $(e.target).attr("data-id"));
      matchurl.set("match_url", $(e.target).parents("td").find(":input").val());

      matchurl.save({
      }, {
        success: function() {
          that.hawk.trigger("updateMatches", {
            collection: "upcoming",
            id: matchurl.get("id")
          });
          matchurl.destroy();
        },
        error: function(model, xhr, options) {
          console.log(model, xhr, options);
        }
      });
    },
    submitForm: function(e) {
      e.preventDefault();
      if (checkTable("score_", $(e.target).parents("table"), $(e.target).attr("data-id"), $(e.target).parents("tr"))) {
        model = this.model.get("upcoming").get($(e.target).attr("data-id"));

        var score = new Score({
          id: model.get("id"),
          home_id: model.get("home_id"),
          home_name: model.get("home_name"),
          away_id: model.get("away_id"),
          away_name: model.get("away_name")
        });

        var scoreHTML = new ScoreHTML({
          model: score,
          hawk: hawk
        });
      }
    }
  });

  var ScoreHTML = Backbone.View.extend({
    tagName: "form",
    className: "form form-horizontal",
    template: _.template($("#score_template").html()),
    initialize: function(opt) {
      this.hawk = opt.hawk;
      this.target = $("#score_" + this.model.get("id") + " td");
      this.model.on("sync", this.addToDOM, this);
      this.model.fetch({
        data: $.param({
          action: "get team data for score submission"
        })
      });

      progressBar(this.target);
    },
    events: {
      "click button[name='save']": "save",
      "click button[name='cancel']": "dispose",
    },
    addToDOM: function() {
      this.target.empty();
      this.target.html(this.render().el);
    },
    render: function() {
      this.$el.html(this.template(this.model.attributes));
      this.delegateEvents();
      return this;
    },
    dispose: function() {
      this.$el.parent().remove();
      this.remove();
    },
    save: function(e) {
      e.preventDefault();
      saveState(e.target, "Submitting");

      var formData = {
        home_score: new Array(),
        away_score: new Array()
      };
      var that = this;

      //get form data
      $(e.target).parents("form").find(":input").not(":file").not("button").not(":reset").each(function() {
        var el = $(this);
        if ((/^home/).test(el.attr("name")))
          formData["home_score"].push(el.val());
        else if ((/^away/).test(el.attr("name")))
          formData["away_score"].push(el.val());
      });

      this.model.set("action", "submit match score");
      this.model.save(formData, {
        success: function() {
            PlayIGL.Analytics.track('match_completed', formData);
          that.hawk.trigger("updateMatches", {
            collection: "upcoming",
            id: this.model.get("id")
          });
          that.dispose();
        }
      });
      return false;
    }
  });
  Dashboard = new Match();
  dashboard = new MatchesHTML({
    model: Dashboard,
    hawk: hawk
  });
}