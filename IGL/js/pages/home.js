/*
 * These elements are used exclusively in /view/home.php
 */

function initCarousel() {
  $("#myCarousel").carousel();
}

function initVote() {
  $(".match-card-upvote").click(function(e) {
    e.preventDefault();
    
    var $this = $(this);
    $this.removeClass("icon-heart").addClass("icon-refresh icon-spin");
    
    jqxhr = $.post("/ajax/public/schedule_vote.php", { id: parseInt($this.attr("data-id")) }, "json")
    .done(function(data) {
      if (data.success) {
        $vote = parseInt($this.next("span").text());
        $vote+=5;
        $this.next("span").text($vote);
        $this.attr("style", "color:#981e1d;");
      } else {
        if (data.code)
          alert(data.error);
      }
    })
    .always(function(data) {
      $this.unbind().click(function(e) { e.preventDefault(); });
      $this.removeClass("icon-refresh icon-spin tooltip-on").addClass("icon-heart");
      if ($this.next(".tooltip"))
        $this.next(".tooltip").remove();
    });
  });
}