/* 
 * These elements are used exclusively in /view/player.php
 */

function invite_button() {
    player = $('#page_id').val();
    viewer = $('#viewer_id').val();
    if(viewer > 0 && player!= '')
    {
        $.getJSON('/invite/player_invite_check/' + player + '/' + viewer, function(json) {
        if(json.success)
        {
           teamlist = '';
            $.each(json.result, function(key, value) {
                if(value.member == true && value.available == true)
                {
                    teamlist = teamlist + '<li><a id="handleInvite_'+value.id+'" onclick="invite('+value.id+', \'revoke\')"><div><img class="pull-left" style="width:18px; height:18px" src="/img'+value.image+'"><div class="pull-left" style="width:108px;text-align:left">' + value.name + '</div><div class="pull-right"><i id="state_'+value.id+'" class="icon-check"></i></div></div></a></li>';
                }
                else if(value.member == false && value.available == false)
                {
                    teamlist = teamlist + '<li><div><img class="pull-left" style="width:18px; height:18px" src="/img'+value.image+'"><div class="pull-left" style="width:108px;text-align:left"></div><div class="pull-right"><i id="state_'+value.id+'" class="icon-ban-circle"></i></div></div></li>';                        
                }
                else
                {
                    teamlist = teamlist + '<li><a id="handleInvite_'+value.id+'" onclick="invite('+value.id+', \'invite\')"><div><img class="pull-left" style="width:18px; height:18px" src="/img'+value.image+'"><div class="pull-left" style="width:108px;text-align:left">' + value.name + '</div><div class="pull-right"><i id="state_'+value.id+'" class="icon-check-empty"></i></div></div></a></li>';                                                
                }

            });
                $('#invite_button_' + player).html('<button class="btn dropdown-toggle" data-toggle="dropdown" href="#"><i id="inviteIcon_'+player+'" class="icon-envelope"></i> Team Invite<span class="caret"></span></button> <ul id="teamList" class="dropdown-menu" style="width:300px"></ul>');
                $('#teamList').append(teamlist);
        }
        else
        {
            $('#invite_button_' + player).hide();
        }
    });
    }
    
}