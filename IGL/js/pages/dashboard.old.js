/* 
 * These elements are used exclusively in /view/dashboard.php
 */

var mmToMonth = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
var playerid = $("input[name='playerID']:hidden").val();

function initDashboard() {
    var view = $("input[name='view']:hidden").val();
       
    switch(view)
    {
        case 'profile': updateProfile(playerid); break;
        case 'matches': updateMatchList(playerid); break;
        case 'teams': updateTeamList(playerid); break;
        case 'referrals': updateReferrals(playerid); break;
        case 'following': updateFollowing(playerid); break;
        default: updateProfile(playerid); break;
    }
    
    activityFeed(playerid);
}

function showLocalDate(timestamp)
{
    var dt = new Date(timestamp * 1000);
    var mm = mmToMonth[dt.getMonth()];
    minutes = dt.getMinutes();
    if(minutes < 10)
        minutes = '0' + minutes;

    if(dt.getHours() > 11)
    {
        hour = dt.getHours() - 12;
        t = 'pm'
    }
    else
    {
        hour = dt.getHours();
        t = 'am'
    }
    return  mm + " " + dt.getDate() + "," + " " + hour + ":" + minutes + t;
}

function localTimezone()
{
    var cur_date = new Date();
    var offset = cur_date.getTimezoneOffset()*-60 / 3600;
    if(offset >= 0)
    {
        offset = Math.abs(offset);
        offset = '+' + offset;
    }
    return offset;
}

function setSelectedIndex(s, valsearch)
{
    // Loop through all the items in drop down list
    for (i = 0; i< s.options.length; i++)
    {
        if (s.options[i].value == valsearch)
        {
            // Item is found. Set its property and exit
            s.options[i].selected = true;
            break;
        }
    }
    return;
}

function handleChallenge(id,action,player)
{
    $('.tooltip').hide();
    $.getJSON('/dashboard/challenge/handle/' + id + '/' +action, function(data) {
        if(data.success)
        {
            $('#challenge_'+id).remove();
            updateMatchList(player);
        }
        else
            alert(data.error);
    });
}

function hideAll()
{
    $('#dashboardFollowing, #dashboardMatchList, #dashboardReferrals, #dashboardTeamData, #player_profile, #dashboardJoinRequests, #dashboardInviteRequests').hide();
}

function challenges(captain)
{
    $('#dashboardMatchList').append('<div id="dashboardChallenges"></div>');
    $('#dashboardChallenges').html('<div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div>');
    
    $.getJSON('/dashboard/challenge/list/' + captain, function(data) {
        if(data == null)
            $('#dashboardChallenges').html('');
        else
        {
            if(data.success)
            {
                table = '<table id="inbound" class="table table-striped table-bordered table-stats">'+
                        '<thead>' +
                        '<tr><th colspan="7">Challenges - <small>Outbound</small></th></tr>'+
                        '<tr><th colspan="3">Ladder</th><th>Team</th><th>Map</th><th>Time (GMT '+localTimezone()+')</th><th>Action</th>' +
                        '</thead>' +
                        '<tbody></tbody></table>' +
                        '<table id="outbound" class="table table-striped table-bordered table-stats">'+
                        '<thead>' +
                        '<tr><th colspan="7">Challenges - <small>Inbound</small></th></tr>'+
                        '<tr><th colspan="3">Ladder</th><th>Team</th><th>Map</th><th>Time (GMT '+localTimezone()+')</th><th>Action</th>' +
                        '</thead>' +
                        '<tbody></tbody></table>'; 
                $('#dashboardChallenges').html(table);
                outbound ='';
                inbound = '';
                              
                $.each(data.challenge, function(key, value){
                    time = showLocalDate(value.time);
                    if(value.captain == captain || value.alternate == captain)
                        inbound += '<tr id="challenge_'+value.id+'"><td width="40px"><a href="/ladder/'+value.ladder_id+'/'+value.ladder_name+'" class="tooltip-on" title="'+value.ladder_name+'"><img class="img-rounded" src="/img'+value.game_avatar+'.png"></a></td><td width="40px"><a href="/team/'+value.challenger_id+'/'+value.challenger_name+'" class="tooltip-on" title="'+value.challenger_name+'"><img class="img-rounded" src="/img'+value.challenger_avatar+'.png"></a></td><td><div>'+value.ladder_name+'</div></td><td><a href="/team/'+value.challenger_id+'/'+value.challenger_name+'" class="tooltip-on" title="'+value.challenger_name+'">'+value.challenger_tag+'</a></td><td width="80px">'+value.map+'</td><td width="90px" wrap="nowrap">'+time+'</td><td width="64px"><button onclick="handleChallenge('+value.id+',\'accept\','+captain+')" type="button" class="btn btn-mini btn-action tooltip-on" title="Accept challenge"><i class="icon-ok"></i></button> <button onclick="handleChallenge('+value.id+',\'deny\','+captain+')" type="button" class="btn btn-mini btn-red tooltip-on" title="Deny challenge"><i class="icon-remove"></i></button></td></tr>';
                    else
                        outbound += '<tr id="challenge_'+value.id+'"><td width="40px"><a href="/ladder/'+value.ladder_id+'/'+value.ladder_name+'" class="tooltip-on" title="'+value.ladder_name+'"><img class="img-rounded" src="/img'+value.game_avatar+'.png"></a></td><td width="40px"><a href="/team/'+value.challengee_id+'/'+value.challengee_name+'" class="tooltip-on" title="'+value.challengee_name+'"><img class="img-rounded" src="/img'+value.challengee_avatar+'.png"></a></td><td><div>'+value.ladder_name+'</div></td><td><a href="/team/'+value.challengee_id+'/'+value.challengee_name+'" class="tooltip-on" title="'+value.challengee_name+'">'+value.challengee_tag+'</a></td><td width="80px">'+value.map+'</td><td width="90px" wrap="nowrap">'+time+'</td><td width="32px"><button onclick="handleChallenge('+value.id+',\'cancel\','+captain+')" type="button" class="btn btn-mini btn-red tooltip-on" title="Cancel challenge"><i class="icon-remove"></i></button></td></tr>';                    
                });
                
                $('#inbound tbody').append(outbound);
                $('#outbound tbody').append(inbound);
                $('.tooltip-on').tooltip();                
            }
            else if(data.error)
                $('#dashboardChallenges').html('<div class="alert alert-error">'+data.error+'</div>');
            else
                $('#dashboardChallenges').html('<div class="alert alert-warning">You do not have any challenges</div>');
        }
    });
}

function activityFeed(player)
{
    $('.notifications').html('<div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div>');
    
    $.getJSON('/dashboard/feed', function(feed) {
        if(feed.success==true)
        {
            $('.notifications').html('<table id="feed" class="table table-condensed" style="width:300px"><tbody></tbody></table>');
            $.each(feed['events'], function(key, value) {
                
                if(value['following'] == "player")
                    var image = value['image1'];
                else
                    var image = value['image2'];

                $('#feed tbody').append('<tr><td><div class="feed_container"><div id="feed_content_' + value['id'] + '" class="feed_content"><img alt="Avatar" class="img-rounded loading feed" src="/img' + image + '.png"><a class="followed" href="'+value['link1']+'">'+value['name1']+'</a> ' + value['event'] + ' </div><div class="feed_time"><small>' + value['when'] + '</small></div></div></td></tr>');

                if(value['name2'] && value['name2'].length > 0)
                    $('#feed_content_' + value['id']).append('<a href="'+ value['link2'] + '">' + value['name2'] + '</a>');
            });
        }
        else
            $('.notifications').html('Error loading notifications.');
    });
}

function handleInvite(id, action, player)
{
    $.getJSON('/dashboard/invite/' + action + '/' + id, function(data) {
        if (data.success == undefined)
            alert('Sorry, there was a problem with this invite. Please try again.');
        else
        {
            if (data.success)
            {
                $('#inviterequest_' +id).hide('slow');
                if(action == 'accept')
                {
                    updateTeamList(player);
                    mixpanel.track('Team Joined');
                }
            }
            else
                alert(data.message);
        }
    });
}

function handleRequest(id, action)
{
    $.getJSON('/dashboard/request/' + action + '/' + id, function(data) {
        if (data.success == undefined)
            alert('Sorry, there was a problem with this request. Please try again.');
        else
        {
            if (data.success)
            {
                $('#joinrequest_' +id).hide('slow');
            }
            else
                alert(data.message);
        }
    });
}

function showCreateTeamForm()
{
    $('#ModalLabel').text('CREATE TEAM');
    $('#ModalButton').attr('disabled', false).unbind().text('CREATE').click( function() { createTeam(); })
    $('#ModalBody').html('<div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div>');
    $('#Modal').modal({ show: true })
    
    $.ajax({
        url: '/team_create',
        success: function(data) {
            $('#ModalBody').html(data);
        }
    });
}

function createTeam()
{    
    if($('#game').val() != 0 && $('#name').val() != '' && $('#tag').val() != '' && $('#location').val() != '')
    {
        $('#ModalButton').html('CREATE <i class="icon-refresh icon-spin"></i>').attr('disabled', true).unbind();
        formData = $('#createTeamForm').serialize();
        $.ajax({
            type: "POST",
            url: "/dashboard/team_create",
            data: formData,
            dataType: "json",
            success: function(team){
                if(team.success)
                {
                    $('#Modal').modal('hide');
                    updateTeamList(team['captain']);
                    mixpanel.track('Team Created');
                    mixpanel.track('Team Joined');
                }
                else
                {
                    $('#ModalButton').html('CREATE').attr('disabled', false);
                    alert(team.error);
                }
                    
            },
            error: function(xhr, textStatus, errorThrown){
                alert(errorThrown);
            }
        });

    }
    else
        {
            $('#ModalButton').html('CREATE').attr('disabled', false);
            alert('Please ensure all fields are completed before creating your team.');   
        }
}

function leaveLeague(team, league)
{
    $('#ModalLabel').text('LEAVE LEAGUE');
    $('#ModalButton').attr('disabled', false).unbind().text('LEAVE').click( function() {
        $(this).attr('disabled', true).text('LEAVING...');
        $.getJSON('/dashboard/team/' + team + '/league/leave/' + league, function(json) {
            if(json == null)
            {
                alert('Unknown error trying to leave league. Please try again later.');
                button.attr('disabled', false).text('LEAVE');
            }
            else
            {
                if(json.success)
                {
                    updateTeamList(playerid);
                    $('#Modal').modal('hide');
                }
                else
                {
                    if(json.error)
                        alert(json.error);
                    else
                        alert("Unable to leave league. Please try again later.");
                }
            }
        });
    });
    $('#ModalBody').html('If you leave this league, your current rankings will be lost. Are you sure?');
    $('#Modal').modal({ show: true })
}


//modified version of what's in ladder.js
function joinButton(btn)
{
    btn.click(function(){
        
    game_id = $(this).attr('data-game');
    action = $(this).attr('data-action');
    team_id = $(this).attr('data-team');

    $(this).html('Wait <i class="icon-spin icon-refresh"></i>');
    $.ajax({
        type: "POST",
        url: "/ladder/join/"+game_id,
        dataType: "json",
        success: function(json){
            if(json.success)
            {
                if(json.in_ladder)
                {
                    $('#ladder_'+team_id).text('Leave Ladder').addClass('btn-red').attr('data-action', 'leave');
                }
                else
                {
                    $('#ladder_'+team_id).text('Join Ladder').removeClass('btn-red').attr('data-action', 'join');
                }
            }
            else
            {
                $('#messages').prepend("<div class='alert alert-error'>"+json.error+"<a class='close' data-dismiss='alert'><i class='icon-remove'></i></a></div>");
                if(action == 'join')
                {
                    $('#ladder_'+team_id).html('Join Ladder');
                }
                else
                {
                    $('#ladder_'+team_id).html('Leave Ladder');
                }
            }
        },
        error: function(xhr, textStatus, errorThrown){
            alert(errorThrown);
        }
    });
    })
}

function joinLeague(team, game)
{
    $('#ModalLabel').text('JOIN LEAGUE');
    $('#ModalButton').attr('disabled', true).unbind().text('JOIN').click( function() {
        var button = $(this);
        var league = $('#selectedLeague').val();            
        if(league)
        {
            $(this).attr('disabled', true).text('JOINING...');
            $.getJSON('/dashboard/team/' + team + '/league/join/' + league, function(json) {
                if(json == null)
                {
                    alert('Unknown error trying to join league. Please try again later.');
                    button.attr('disabled', false).text('JOIN');
                }
                else
                {
                    if(json.success)
                    {
                        updateTeamList(playerid);
                        $('#Modal').modal('hide');
                    }
                    else
                    {
                        if(json.error)
                            alert(json.error);
                        else
                            alert('Unknown error trying to join league. Please try again later.');
                        
                        button.attr('disabled', false).text('JOIN');                            
                    }
                }
            });
        }
    })
    $('#ModalBody').html('<div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div>');
    $('#Modal').modal({ show: true })
    $.getJSON('/dashboard/leagues/list/' + game, function(json) {
        if(json == null)
              $('#ModalBody').html('Unknown error loading leagues. Please try again.');          
        else
        {
            if(json.success)
            {
                var html = "Select league to join: <select id='selectedLeague'>";
                $.each(json.leagues, function(index, league) {
                    html = html + "<option value="+ league.id+">"+ league.name +" - "+ league.skill +"</option>";
                })
                html = html + "</select>";
                $('#ModalBody').html(html);
                $('#ModalButton').attr('disabled', false);                    
            }
            else
            {
                if(json.error)
                    $('#ModalBody').html(json.error);
                else
                    $('#ModalBody').html('Unknown error loading leagues. Please try again.');
            }
        }
    });    
}

function updateTeamList(player)
{
    hideAll();
    $('#dashboardTeamList').html('<div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div>');
    $('#dashboardTeamData').show();
    $('#dashboardTeamData').append('<div id="dashboardInviteRequests" style="margin-top:8px"></div>');
    $('#dashboardTeamData').append('<div id="dashboardJoinRequests" style="margin-top:8px"></div>');
    $('#triggers').html('<a type="button" class="btn btn-action" onclick="showCreateTeamForm()">NEW TEAM</a>');

    $.getJSON('/dashboard/team/list/' + player, function(data) {
        if(data == null)
            $('#dashboardTeamList').html('Unknown error loading teams, please try again.');
        else
        {
            $('#dashboardTeamList').html(''); // empty the list
            if(data.success)
            {
                if(data.teams)
                {
                    $('#dashboardTeamList').append('<table style="width:630px" class="table table-bordered table-condensed table-striped" id="dashboardTeamListTable"><tbody></tbody></table>');
                    $.each(data.teams, function(index, team) {
                        appendrow = '';
                        appendrow +=  
                            '<tr id="teamlist_' + team.id +'">';
                        
                        if(team.league == null || team.league == 0)
                            appendrow +=  
                                '<td width="40px"><img alt="Game" class="img-rounded img-profile-small" src="/img'+ team.game_logo +'.png"></td>';
                        else
                            appendrow +=  
                                '<td width="40px"><a href="/league/'+ team.league +'-'+ team.league_name +'"><img alt="Game" class="img-rounded img-profile-small" src="/img'+ team.game_logo +'.png"></a></td>';
                        
                        appendrow +=  
                            '<td width="40px"><a href="/team/'+ team.id +'"><img alt="'+ team.name +'" class="img-rounded img-profile-small" src="/img'+ team.logo +'.png"></a></td>' +
                            '<td><a href="/team/'+ team.id +'-' + team.name + '">' + team.name +'</a><br/>';
                        
                        if(team.num_followers > 0)
                            appendrow += '<button onclick="viewFollowers(\'team\','+team.id+','+player+')" class="btn btn-mini btn-info tooltip-on" title="'+team.num_followers+' Followers"><i class="icon-star"></i> '+team.num_followers+'</button>';
                        else
                            appendrow += '<button class="btn btn-mini btn-info tooltip-on" title="You don\'t have any Followers"><i class="icon-star-empty"></i></button>';

                        if((team.captain == player) || (team.alternate == player))
                        {
                            appendrow += 
                                ' <button class="btn btn-mini tooltip-on" title="Manage Team" onclick="manageTeam('+ team.id +', 1)"><i class="icon-pencil"></i></button>' +
                                ' <button class="btn btn-mini tooltip-on" title="Edit Roster" onclick="editRoster('+ team.id +', 1)"><i class="icon-group"></i></button>';
                            
                            if(team['password'])
                                appendrow +=  ' <button id="setPassword" onclick="toggleTeamPIN('+ team.id +',1)" class="btn btn-mini tooltip-on" title="Set join password"><i id="teamLocked_' + team.id + '" class="icon-lock tooltip-on" title="Allow users to join via password"></i></button>';
                            else
                                appendrow +=  ' <button id="setPassword" onclick="toggleTeamPIN('+ team.id +',1)" class="btn btn-mini tooltip-on" title="Set join password"><i id="teamLocked_' + team.id + '" class="icon-key"></i></button>';
                            appendrow += ' <button class="btn btn-mini" onclick="teamBuilder('+ team.id +')">Team Builder</button>';
                            
                            if(team.league == null || team.league == 0)
                            {
                                if(team.totalplayers >= team.league_team_size)
                                    appendrow +=  ' <button class="btn btn-mini" onclick="joinLeague('+ team.id +', '+ team.game +')">Join League</button>';
                                else
                                    appendrow +=  ' <button class="btn btn-mini tooltip-on" title="You need '+ team.league_team_size +' players before you can join a league." disabled>Join League</button>';
                            }
                            else
                                appendrow +=  ' <button class="btn btn-mini btn-red" onclick="leaveLeague('+ team.id +', '+ team.league +')">Leave League</button>';

                            if(team.ladder == null || team.ladder == 0)
                            {
                                if(team.totalplayers >= team.ladder_team_size)
                                    appendrow +=  ' <button id="ladder_'+team.id+'" data-game="'+team.game+'" data-team="'+team.id+'" data-action="join" class="join_ladder btn btn-mini">Join Ladder</button>';
                                else
                                    appendrow +=  ' <button class="btn btn-mini tooltip-on" title="You need '+ team.ladder_team_size +' players before you can join a ladder." disabled>Join Ladder</button>';
                            }
                            else
                                appendrow +=  ' <button id="ladder_'+team.id+'" data-game="'+team.game+'" data-team="'+team.id+'" data-action="leave" class="join_ladder btn btn-mini btn-red">Leave Ladder</button>';   
                        }
                        appendrow += 
                            ' <button class="btn btn-mini btn-red" onclick="leaveTeam(' + team.id  + ')">Leave Team</button>' +
                            '</td>' +
                            '</tr>' +
                            '<tr id="teamlist_'+ team.id +'_edit" style="display:none"><td>test</td>' +
                            '</tr>';                        
                        
                        $('#dashboardTeamListTable').append(appendrow);
                        joinButton($('#ladder_'+team.id));
                        $('.tooltip-on').tooltip();
                    });
                }
                else
                    $('#dashboardTeamList').append('<div class="alert alert-warning">No teams found</div>');
            }
            else
                $('#dashboardTeamList').append('<div class="alert alert-error">Error loading teams : ' + data.error + '</div>');
       
            updateInviteRequests(player);
        }
    });
}

function toggleTeamPIN(id,show)
{
    elem = $('#teamlist_'+ id + '_edit');
    if(show == 0)
        elem.hide();
    else
    {
        appendrow = '<td colspan="3">' +
                    '<form action="" id="teamPIN_' + id + '">' +
                    '<div class="input-append offset2">' +
                    '<input style="height:20px" id="appendedInputButtons" maxlength="4" type="text" name="pin">' +
                    '<button type="button" class="btn" onclick="setTeamPIN(' + id + ',1)">Save</button>' +
                    '<button type="button" class="btn" onclick="setTeamPIN(' + id + ',0)">Remove</button>' +
                    '<button type="button" class="btn btn-danger" onclick="toggleTeamPIN(' + id + ',0)">Close</button>' +
                    '</div>' +
                    '</form>' +
                    '</td>';
        elem.html(appendrow);
        elem.show();
    }
}

function manageTeam(id, show)
{
    elem = $('#teamlist_'+ id + '_edit');
    if(!show)
        elem.hide();
    else
    {
        elem.html('<td colspan=3><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></td></tr>');
        elem.show();
        
        $.getJSON('/dashboard/team/get/' + id, function(data) {
            if(data == null)
                elem.html('<td colspan=3><div class="alert alert-error">Problem loading team information. Please try again.</div></td></tr>');
            else
            {
                if(data.success)
                {
                    appendrow = '';
                    appendrow +=  '<td colspan="3">' +
                        '<form id="profile_'+ data.team.id +'" action="/avatar.php" method="post" target="avatar-upload-frame" class="form form-horizontal" enctype="multipart/form-data">' +
                        '<input type="file" accept="image/jpg,image/png,image/jpeg" id="lefile" name="logo" style="display:none" onChange="uploadAvatar(' + data.team.id + ', ' + data.team.logo + ')">' + 
                        '<input type="hidden" name="id" value="'+ data.team.id +'" />' +
                        '<input type="hidden" name="media_type" value="team" />' +
                        '<input type="hidden" name="field" value="logo" />' +
                        '<input type="hidden" name="avatar_player" value="' + data.team.logo + '" />' +
                        '<legend>Editing '+ data.team.name +'</legend>'+
                        '<fieldset class="span3 pull-left">'+
                        '<a><img onclick="$(\'input[id=lefile]\').click();" alt="' + data.team.name +'" src="/img' + data.team.logo +'.png" class="img-rounded img-profile dashboard-avatar"></a>' +
                        '<label class="control-label" for="inputSteamGroup">Steam Group</label>' +
                        '<input type="text" id="inputSteamGroup" name="steamgroup" placeholder="ex: playigl" value="'+ data.team.steamgroup +'">' +
                        '</fieldset>' +
                        '<fieldset class="span4 pull-right">' +
                        '<label class="control-label" for="inputTag">Tag</label>' +
                         '<input type="text" id="inputTag" name="tag" placeholder="" value="'+ data.team.tag +'">' +
                        '<label class="control-label" for="inputWebsite">Website</label>' +
                        '<input type="text" id="inputWebsite" name="website" placeholder="ex: http://www.playigl.com/" value="'+ data.team.website +'">';
 
                    // only show captain edit if we're captain or alternate
                    if(data.team.captain == playerid)
                    {
                        appendrow +=  
                            '<label class="control-label" for="selectCaptain">Captain</label>' +
                            '<select name="captain" id="selectCaptain">';

                        $.each(data.team.players, function(index, player) {
                            if(player.playerid == data.team.captain)
                                appendrow +=  '<option value=' + player.playerid +' selected>' + player.username + '</option>';
                            else
                                appendrow +=  '<option value=' + player.playerid +'>' + player.username + '</option>';
                        });

                        appendrow += 
                            '</select>';                         
                    }
                    
                    appendrow += 
                        '<label class="control-label" for="selectAlternate">Alternate</label>' +
                        '<select name="alternate" id="selectAlternate">' +
                        '<option value=0>No alternate captain</option>';
                    
                    $.each(data.team.players, function(index, player) {
                        if(player.playerid != data.team.captain)
                        {
                            if(player.playerid == data.team.alternate)
                                appendrow +=  '<option value=' + player.playerid +' selected>' + player.username + '</option>';
                            else
                                appendrow +=  '<option value=' + player.playerid +'>' + player.username + '</option>';                                
                        }
                    });
                    
                    appendrow += 
                        '</select>' +
                        '<label class="control-label" for="allowApplicants">Allow Applicants</label>' +
                        '<select name="open" id="allowApplicants">';
                    
                    if(data.team.open == 1)
                    {
                        appendrow +=  ' <option value="1" selected>Yes</option>' +
                        ' <option value="0">No</option>';
                    }
                    else
                    {
                        appendrow +=  ' <option value="1">Yes</option>' +
                        ' <option value="0" selected>No</option>';
                    }
                    
                    appendrow = appendrow  + '</select>' +
                    '</fieldset>' +
                    '<div class="form-actions clear">' +
                    ' <button id="saveTeamButton_'+ id +'" type="button" class="btn btn-primary" onclick="saveTeam('+id+')">Save Changes</button>' +
                    ' <button type="button" class="btn" onclick="manageTeam('+id+', 0)">Cancel</button>' +
                    '</div>' +
                    '</form>' +
                    '</td>';
                    appendrow +=  '<iframe id="avatar-upload-frame" style="display:none;" name="avatar-upload-frame"></iframe>';                
                    elem.html(appendrow);
                }
                else
                    elem.html('<td colspan=3><div class="alert alert-error">Error: ' + data.error +'</div>></td></tr>');
            }
        });
    }
}

function saveTeam(id)
{
    $('#saveTeamButton_' + id).attr('disabled',true).text('Saving...');
    formData = $('#profile_'+ id).serialize();
    
    $.ajax({
        type: "POST",
        url: "/dashboard/team/save/" + id,
        data: formData,
        dataType: "json",
        success: function(json){
            if(json.success)
            {
                manageTeam(id, 0);
                updateTeamList(playerid);
            }
            else
            {
                alert(json.error);
                $('#saveTeamButton_' + id).attr('disabled',false).text('Save Changes');
            }
        },
        error: function(xhr, textStatus, errorThrown){
            alert(errorThrown);
        }
    });
}

function editRoster(id, show)
{
    elem = $('#teamlist_'+ id + '_edit');
    if(!show)
        elem.hide();
    else
    {
        elem.html('<td colspan=3><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></td></tr>');
        elem.show();
        $.getJSON('/dashboard/team/get/' + id, function(data) {
            if(data == null)
                elem.html('<td colspan=3><span class="label label-important">Problem loading team information. Please try again.</span></td></tr>');
            else
            {
                if(data.success)
                {
                    appendrow = '';
                    appendrow +=  '<td colspan=3>' +
                        '<form action="" id="rosterInformation_'+ data.team.id +'" class="form form-horizontal">' +
                        '<legend>You are editing '+ data.team.name +'</legend>' +
                        '<table class="table table-bordered table-condensed" width="100%">' +
                        '<tr><th>Player</th><th>Remove</th></tr>';
                    
                    $.each(data.team.players, function(index, player) {
                        if(player.playerid != data.team.captain && player.playerid != playerid)
                        {
                            appendrow +=  
                                '<tr>' +
                                '<td>' + player.username + '</td>' +
                                '<td><input type="checkbox" value="1" name="remove['+ player.playerid +']"></td>' +
                                '</tr>';
                        }
                        else
                        {
                            appendrow +=  
                                '<tr>' +
                                '<td>' + player.username + '</td>' +
                                '<td></td>' +
                                '</tr>';
                        }
                    });
                    
                    appendrow +=  '</table>';
                    appendrow += 
                        '<div class="form-actions">' +
                        ' <button id="saveRosterButton_' + id + '" type="button" class="btn btn-primary" onclick="saveRoster('+id+')">Save Changes</button>' +
                        ' <button type="button" class="btn" onclick="editRoster('+id+', 0)">Cancel</button>' +
                        '</div>' +
                        '</form>' +
                        '</td>';
                                        
                    elem.html(appendrow);
                }
                else
                    elem.html('<td colspan=3><span class="label label-important">Error: ' + data.error +'</span></td></tr>');
            }
        });
    }
}

function saveRoster(id)
{
    $('#saveRosterButton_' + id).attr('disabled',true).text('Saving...');
    formData = $('#rosterInformation_'+ id).serialize();
    
    $.ajax({
        type: "POST",
        url: "/dashboard/roster/save/" + id,
        data: formData,
        dataType: "json",
        success: function(json){
            if(json.success)
                editRoster(id, 0);
            else
            {
                alert(json.error);
                $('#saveRosterButton_' + id).attr('disabled',false).text('Save Changes');
            }
        },
        error: function(xhr, textStatus, errorThrown){
            alert(errorThrown);
        }
    });
}

function updateInviteRequests(player)
{
    $('#dashboardInviteRequests').html('<div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div>').show();
    $.getJSON('/dashboard/inviterequest/list/' + player, function(data) {
        if(data == null)
            $('#dashboardInviteRequests').html('Unknown error loading teams, please try again.');
        else
        {
            $('#dashboardInviteRequests').html(''); // empty the list
            if(data.success)
            {
                if(data.requests)
                {
                    $('#dashboardInviteRequests').append('<table style="width:630px" class="table table-bordered table-condensed table-striped" id="dashboardInviteRequestsTable"><tbody></tbody></table>');
                    $.each(data.requests, function(index, request) {
                        appendrow = '';
                        appendrow +=  
                            '<tr id="inviterequest_' + request.id +'">' +
                            '<td width="32px"><a href="/league/'+ request.league +'-'+ request.league_name +'"><img alt="Game" class="img-rounded" src="/img'+ request.game_logo +'.png"></a></td>' +
                            '<td width="32px"><a href="/team/'+ request.team_id +'"><img alt="'+ request.name +'" width="40px" height="40px" class="img-rounded" src="/img'+ request.logo +'.png"></a></td>' +
                            '<td><a href="/team/'+ request.team_id +'"> ' + request.name +'</a></td>' +
                            '<td width="100px">' +
                            ' <button type="button" class="btn btn-mini btn-red" onclick="handleInvite(' + request.id +', \'deny\', ' + player + ')"><i class="icon icon-white icon-remove tooltip-on" title="DENY"></i></button>' +
                            ' <button type="button" class="btn btn-mini btn-action" onclick="handleInvite(' + request.id +', \'accept\', ' + player + ')"><i class="icon icon-white icon-ok tooltip-on" title="ACCEPT"></i></button>' +
                            '</td>' +
                            '</tr>';
                        $('#dashboardInviteRequestsTable').append(appendrow);
                    });
                }
                else
                     $('#dashboardInviteRequests').html('');
            }
            else
                $('#dashboardInviteRequests').append('<span class="label label-important">Error loading join requests : ' + data.error + '</span>');
            
            updateJoinRequests(player);
       }
    });
}

function updateJoinRequests(player) {
    $('#dashboardJoinRequests').html('<div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div>').show();
    $.getJSON('/dashboard/joinrequest/list/' + player, function(data) {
        if(data == null)
            $('#dashboardJoinRequests').html('Unknown error loading teams, please try again.');
        else
        {
            $('#dashboardJoinRequests').html(''); // empty the list
            if(data.success)
            {
                if(data.requests)
                {
                    $('#dashboardJoinRequests').append('<table style="width:630px" class="table table-bordered table-condensed table-striped" id="dashboardJoinRequestsTable"><tbody></tbody></table>');
                    $.each(data.requests, function(index, request) {
                        appendrow = '';
                        appendrow +=  
                            '<tr id="joinrequest_' + request.request_id +'">' +
                            '<td width="40px"><img alt="Game" width="40px" height="40px" class="img-rounded" src="/img'+ request.game_logo +'.png"></td>' +
                            '<td width="40px"><a href="/player/'+ request.player_id +'"><img alt="avatar" width="40px" height="40px" class="img-rounded" src="/img'+ request.avatar +'.png"></a></td>' +
                            '<td><a href="/team/'+ request.team_id +'">'+ request.team_tag +' ' + request.team_name +'</a></td>' +
                            '<td><a href="/player/'+ request.player_id +'">'+ request.username + '</a></td>' +
                            '<td width="100px">' +
                            ' <button type="button" class="btn btn-mini btn-red" onclick="handleRequest(' + request.request_id +', \'deny\')"><i class="icon icon-white icon-remove tooltip-on" title="DENY"></i></button>' +
                            ' <button type="button" class="btn btn-mini btn-action" onclick="handleRequest(' + request.request_id +', \'accept\')"><i class="icon icon-white icon-ok tooltip-on" title="ACCEPT"></i></button>' +
                            '</td>' +
                            '</tr>';
                        $('#dashboardJoinRequestsTable').append(appendrow);
                    });
                }
                else
                    $('#dashboardJoinRequests').html('');
            }
            else
                $('#dashboardJoinRequests').append('<span class="label label-important">Error loading join requests : ' + data.error + '</span>');
       }
    });
}

function updateMatchList(player) {
    hideAll();
    $('#dashboardMatchList').html('<div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div>');
    $('#dashboardMatchList').show();    
    
    $.getJSON('/dashboard/matches/list/' + player, function(data) {
        if(data == null)
            $('#dashboardMatchList').html('Unknown error loading matches, please try again.');
        else
        {
            $('#dashboardMatchList').html(''); // empty the list
            
            if(data.success)
            {
                if(data.matches.next)
                {                    
                    if(data.matches.next.officialdate > 0)
                        date = showLocalDate(data.matches.next.officialdate);
                    else
                        date = showLocalDate(data.matches.next.scheduleddate);
                    
                    $('#dashboardMatchList').append(
                        '<table class="table table-condensed table-striped table-bordered">' +
                        '<tr><th class="row-header" colspan="5"><h5>NEXT MATCH</h5></th></tr>' +
                        '<tr><th>Date & Time (GMT '+localTimezone()+')</th><th>Home</th><th></th><th>Away</th><th>Join Match</th></tr>' +
                        '<tr><th>' + date +'</th><td><a href="/team/'+ data.matches.next.home_id +'">' + data.matches.next.home_tag + '</a> ('+ data.matches.next.home_wins + '-' + data.matches.next.home_losses +')</a></td><td>vs</td><td><a href="/team/'+ data.matches.next.away_id +'">' + data.matches.next.away_tag + '</a> (' + data.matches.next.away_wins + '-' + data.matches.next.away_losses +')</td><td id="play_match"><a onclick="playmatch()" class="btn btn-action" href="steam://connect/' + data.matches.next.server_ip +':' + data.matches.next.server_port +'/' + data.matches.next.sv_password +'">Play <i class="icon-headphones icon-white"></i></a></td></tr>' +
                        '</table>'
                     );
                    if(data.matches.next.home_captain == 1 || data.matches.next.away_captain == 1 || data.matches.next.home_alternate == 1 || data.matches.next.away_alternate == 1 )
                        $('#play_match').append(' <button type="buton" class="btn btn-info tooltip-on" title="Submit score" onclick="submitScoreForm('+data.matches.next.id+','+player+')"><i class="icon-check"></i></button>');
                }
                $('#dashboardMatchList').append('<table class="table table-bordered table-condensed table-striped" id="dashboardMatchListTable"><tbody></tbody></table>');
                $('#dashboardMatchListTable tbody').append(
                    '<tr><th colspan=5 class="row-header"><h5>Upcoming Matches</h5></th></tr>' +
                    '<tr><th>Date & Time (GMT '+localTimezone()+')</th><th>Action</th><th>Home</th><th>&nbsp;</th><th>Away</th></tr>'
                );

                if(data.matches.upcoming)
                {                    
                    $.each(data.matches.upcoming, function(index, match) {
                        if(match.officialdate > 0)
                            date = showLocalDate(match.officialdate);
                        else
                            date = showLocalDate(match.scheduleddate);
                        
                        appendrow = '';
                        appendrow +=  '<tr>' +
                            '<th><a href="/match/' + match.id +'">' + date +'</a></th>' +
                            '<td><img alt="Live" src="/img/live.png">';
                        
                        if(match.home_captain == playerid || match.away_captain == playerid)
                            appendrow +=  '<img alt="Calendar" src="/img/calendar.png">';

                        appendrow +=  '</td>' +
                            '<td><a href="/team/' + match.home_id +'">' + match.home_tag + '</a></td>' +
                            '<td align="center">vs.</td>' +
                            '<td><a href="/team/' + match.away_id +'">' + match.away_tag + '</a></td>' +
                            '</tr>';

                        $('#dashboardMatchListTable tbody').append(appendrow);
                    });
                }
                else
                    $('#dashboardMatchListTable tbody').append('<tr><td colspan=5><div class="alert alert-warning">You do not have any matches scheduled.</div></td></tr>');
                
                if(data.matches.completed)
                {
                    $('#dashboardMatchList').append('<table class="table table-bordered table-condensed table-striped" id="dashboardMatchListPlayed"><tbody></tbody></table>');
                    $('#dashboardMatchListPlayed tbody').append('<tr><th colspan="5" class="row-header"><h5>Last matches played</h5></th></tr>');

                    $.each(data.matches.completed, function(index, match) {
                        if(match.officialdate > 0)
                            date = showLocalDate(match.officialdate);
                        else
                            date = showLocalDate(match.scheduleddate);
                        
                        appendrow = '';
                        appendrow +=  
                            '<tr>' +
                            '<th><a href="/match/' + match.id +'">' + date +' HR</a></th>' +
                            '<td></td>' +
                            '<td>';
                        
                        if(match.home_id == match.winner_id)
                            appendrow +=  '<span class="label label-success pull-right label-fixed">W</span>';
                        else
                            appendrow +=  '<span class="label label-important pull-right label-fixed">L</span>';
                            
                        appendrow +=  
                            ' <a href="team/' + match.home_id +'">' + match.home_tag + '</a></td>' +
                            '<td align="center">vs.</td>' +
                            '<td>';

                        if(match.away_id == match.winner_id)
                            appendrow +=  '<span class="label label-success pull-right label-fixed">W</span>';
                        else
                            appendrow +=  '<span class="label label-important pull-right label-fixed">L</span>';

                        appendrow +=  '<a href="/team/' + match.away_id +'">' + match.away_tag + '</a></td>' +
                            '</tr>';
                        $('#dashboardMatchListPlayed tbody').append(appendrow);
                    }); 
                }
                challenges(player);
            }
            else
                $('#dashboardMatchList').append('<div class="alert alert-error">Error loading matches : ' + data.error + '</div>');
        }
    });
}

function playmatch() {
        mixpanel.track('Play Match');
}

function submitScoreForm(matchid,player) {
    $('#ModalLabel').text('SUBMIT SCORE');
    $('#ModalButton').attr('disabled', false).text('SUBMIT').click(function(){ submitScore(matchid,player); });
    $('#Modal').modal('show');
    
    $.getJSON('/dashboard/submitscore/' + matchid, function(data) {
        if(data.success)
        {
            $('#ModalBody').html('<form id="submitScoreForm"><table id="submitScore" class="table table-striped"><tbody></tbody></table></form>');
            row = '<tr><th>Round</th><th>'+data['home_name']+'<input type="hidden" name="home_id" value="'+data['home_id']+'"></th><th>'+data['away_name']+'<input type="hidden" name="away_id" value="'+data['away_id']+'"></th></tr>';
            count = 1
            while( data['rounds'] > count )
            {
                row += '<tr><td>'+count+'</td><td><input type="text" name="home_score[]"></td><td><input type="text" name="away_score[]"></td></tr>';
                count++;
            }
            row += '<tr><td>Overtime</td><td><input type="text" name="home_score[]"></td><td><input type="text" name="away_score[]"></td></tr>';
            $('#submitScore tbody').append(row);
        }
        else
            alert(data.error);        
    });
}

function submitScore(matchid,player)
{
    $('#ModalButton').attr('disabled', true).html('SUBMIT <i class="icon-refresh icon-spin"></i>');
    formData = $('#submitScoreForm').serialize();
    
    $.ajax({
        type: "POST",
        url: "/dashboard/submitscoresave/" + matchid,
        data: formData,
        dataType: "json",
        success: function(data){
            if(data.success)
            {
                $('#Modal').modal('hide');
                updateMatchList(player);
            }
            else
            {
                alert(data.error);
                $('#ModalButton').attr('disabled', false).html('SUBMIT');
            }
        }
    });
}

function leaveTeam(id)
{
    $('#ModalLabel').text('LEAVE TEAM');
    $('#ModalButton').attr('disabled', false).unbind().text('CONFIRM').click( function() {
        $('#ModalButton').attr('disabled', true).text('PLEASE WAIT...');
        
        $.getJSON('/dashboard/team/leave/' + id, function(data) {
            $('#Modal').modal('hide')
            if (data.success == undefined)
                alert('Sorry, there was a problem with this request. Please try again.');
            else
            {
                if (data.success)
                {
                    $('#teamlist_'+id).hide();
                    $('#teamlist_'+id+'_edit').remove();
                }
                else
                {
                    if (data.error == undefined)
                        alert('Unknown error while trying to leave team.');
                    else
                        alert(data.error);
                }
            }
        });
    });
    
    $('#ModalBody').html('Are you sure you want to leave this team?');
    $('#Modal').modal('show');
}

function toggle_visibility(id)
{
    top.visible_div_id = 'right';
    var old_e = document.getElementById(top.visible_div_id);
    var new_e = document.getElementById(id);
    
    if(old_e)
        old_e.style.display = 'none';
    
    new_e.style.display = 'block';   
    top.visible_div_id = id;          
}

function teamBuilder(id)
{
    $('#ModalLabel').text('TEAM BUILDER');
    $('#ModalButton').attr('disabled', false).unbind().text('INVITE TO TEAM').addClass('btn-red').click( function() { saveTeamBuilder(id); });
    $('#ModalBody').html('<div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div>');
    $('#Modal').addClass('modal-wide');
    $('#Modal').modal({ show: true });
    
    $.ajax({
        url: '/team_builder/'+id,
        success: function(data) {
            $('#ModalBody').html(data);
        }
    });
}

function saveTeamBuilder(id)
{
    $('#ModalButton').attr('disabled',true).text('SENDING...');
    formData = $('#teambuilder').serializeArray();
    
    $.each(formData, function(key, field) {
     $.getJSON('/invite/' + field.value + '/' + id + '/invite', function(data) {
        if (data.success == undefined)
            alert('Sorry, there was a problem with this request. Please try again.');
        });
    });
 
    $('#ModalBody').html('<div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div>');
    $('#Modal').modal({ show: true });
    
    $.ajax({
        url: '/team_builder/'+id,
        success: function(data) {
            $('#ModalBody').html(data);
            $('#ModalButton').attr('disabled',false).text('INVITE');
        }
    });
}

function removePlayer(player,team)
{
    $.getJSON('/dashboard/roster/remove/' + player + '/' + team, function(data) {
        if (data.success == undefined)
            alert('Sorry, there was a problem with this request. Please try again.');
        else
        {
            $('#player_' + player).remove();
            $('#Modal').modal({ show: true });

            $.getJSON('/api/player/' + player, function(data) {
                if (data.success == true)
                    $('#sortable2').prepend('<li class="ui-state-default"><input name="invite[]" type="hidden" value="' + player + '"><img alt="' + data.player.player_country + '" src="/img/flags/' + data.player.player_country + '.png"> <a target="_blank" href="/player/' + player + '-' + data.player.player_username + '">' + data.player.player_username + '</a><br>' + data.player.player_firstname + ' ' + data.player.player_lastname + '<br>' + data.player.player_city + ' ' + data.player.state + ' <br> <a target="_blank" href="http://steamcommunity.com/profiles/' + data.player.player_community_id + '">' + data.player.player_steam_id + '</a></li>');
            });
        }
    });
}

function removeReferral(key,player,referral)
{
    $('.tooltip').hide();
    $('#referral_' + key).html('<td>Removing...</td><td><i class="close icon-time"></i></td>');
    
    $.getJSON('/dashboard/referral/remove/' + player + '/' + referral, function(data) {
        if (data.success == undefined)
        {
            alert('Sorry, there was a problem with this request. Please try again.');
            $('#referral_' + key).html('<td>' + referral + '</td><td><i class="close icon-remove" onclick="removeReferral(' + key + ',' + player + ',\'' + referal + '\'")"></i></td>');
        }
        else
        {
            if(data.success==true)
                $('#referral_'+key).hide('slow');
            else
            {
                alert(data.message);
                $('#referral_' + key).html('<td>' + referral + '</td><td><i class="close icon-remove" onclick="removeReferral(' + key + ',' + player + ',\'' + referal + '\')"></i></td>');
            }
        }    
    });
}

function uploadAvatar(media, avatar)
{
    $('.dashboard-avatar').fadeTo('slow', 0.5);
    $('.dashboard-avatar').parent().append('<p class="dashboard-avatar-loader"><img src="/img/avatar-loader.gif" /></p>');
    $('#profile_' + media).submit();
    
    $('#avatar-upload-frame').load(function() {
       $(this).unbind('load');
       var success = $.trim($('#avatar-upload-frame').contents().text());
       console.log(success);
       
       if (success != "false") {
           var src = "/img"+success+".png"+'?'+Math.random();
           $('.dashboard-avatar').attr("src", src);
           $('.dashboard-avatar-loader').remove();
           $('.dashboard-avatar').load(function() {
              $('.dashboard-avatar').fadeTo('slow', 1); 
           });
       } else
           console.log("An error occured uploading a avatar");
    });
}

/*
 * OAuth for Twitter, starts the auth process
 */
function twitterOAuth() {
    window.open('/twitter.php', '', 'width=980,height=600');
}
/*
 * OAuth for Twitter, callback for successful auth
 */
function twitterOAuth_Complete(response, win) {
    win.close();
    var twitterParent = $('.twitter-element').parent();
    $('.twitter-element').remove();
    $(twitterParent).append(twitterProfileElement(response.screen_name)); 
    $('.twitter-element').hide().fadeIn();
    $('#profile_save').click();
}

/*
 * Returns the Twitter Profile Element for the profile page
 */
function twitterProfileElement(twitter) {
    if(twitter)
        return '<div class="input-prepend twitter-element"><span class="add-on" style="height:24px; padding-top:10px; ">@</span><input class="social" name="twitter" value="' + twitter + '" type="text" placeholder="ex. PlayIGL" style="width:180;"></div>';
    return '<div onclick="twitterOAuth()" class="twitter-element"><img src="https://dev.twitter.com/sites/default/files/images_documentation/sign-in-with-twitter-gray.png" alt="Sign in with Twitter" style="margin:6px 0;"/></div>';
}

function updateProfile(player)
{
    hideAll();
    $('#player_profile').html('<div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div>').show();
    
    $.getJSON('/api/player/'+ player , function(data) {
        if (data.success == undefined)
            $('#player_profile').html('<div class="alert alert-error">Error loading player profile</div>');
        else
        {
            if(data.success)
            {
                appendrow = '';
                appendrow +=  '<form action="/avatar.php" method="post" target="avatar-upload-frame" class="form form-horizontal" enctype="multipart/form-data" id="profile_' + player + '">' +
                '<input type="file" accept="image/jpg,image/png,image/jpeg" id="lefile" name="avatar" style="display:none" onChange="uploadAvatar(' + player + ', ' + data.player.player_avatar + ')">' + 
                '<input type="hidden" name="media_type" value="player" />' +
                '<input type="hidden" name="field" value="avatar" />' +
                '<input type="hidden" name="id" value="' + player + '" />' +
                '<h3>' + data.player.player_username + ' <img class="dashboard-flag" alt="' + data.player.player_country + '" src="/img/flags/' + data.player.player_country + '.png"></h3>' +
                '<table class="table table-striped table-condensed">' +
                '<tr><td style="padding:0;">' +
                '<table>' +
                '<tr><td style="border-top:none"><h4>FIRST NAME</h4>' +
                '<input id="loginfileld" type="text" name="firstname" value="' + data.player.player_firstname + '" placeholder="Ex. John"></td></tr>' +

                '<tr><td><h4>LAST NAME</h4>' +
                '<input class="loginfield" type="text" name="lastname" value="' + data.player.player_lastname +'" placeholder="ex. Smith"></td></tr>' +
                '</table>' +
                '</td><td>';

                if(data.player.player_avatar > 0)
                    appendrow +=  '<a style="display:block; position:relative;" onclick="$(\'input[id=lefile]\').click();"><img alt="Avatar" class="img-rounded dashboard-avatar" width="184" height="184" src="/img' + data.player.player_avatar + '.png"></a>';
                else
                    appendrow += '<a style="display:block; position:relative;" onclick="$(\'input[id=lefile]\').click();"><img alt="Missing" class="img-rounded dashboard-avatar" width="184" height="184" src="/img/missing.png"></a>';

                // Upload iFrame
                appendrow +=  '<iframe id="avatar-upload-frame" style="display:none;" name="avatar-upload-frame"></iframe>';                
                appendrow +=  '</td></tr>' +
                '<tr><td><h4>SEX</h4>' +
                '<select name="sex" style="height:40px;padding-top:7px;">' +
                '<option>' + data.player.player_sex + '</option>' +
                '<option>MALE</option>' +
                '<option>FEMALE</option>' +
                '</select></td><td><h4 class="pull-right">Age</h4><h4 class="pull-left">DATE OF BIRTH</h4>' +
                '<input class="pull-left" style="clear:left; width:150px;" id="dob" name="birthday" value="' + data.player.player_birthday + '" type="text" placeholder="MM-DD-YY"><input class="pull-right" style="width:40px;" id="age" name="age" value="" type="text" placeholder=""></td></tr>' +
                '<tr><td><h4>COUNTRY</h4>' +
                '<select id="country_list" style="height:40px;padding-top:7px;" name="country" data-placeholder="Choose a Country..." class="chzn-select">';

                appendrow +=  '</select>' + 
                    '</td><td><h4>TIME ZONE</h4>' +
                    '<select id="timezoneoffset" name="timezoneoffset"  style="height:40px;padding-top:7px;">' +
                      '<option value="-12">(GMT -12:00) Eniwetok, Kwajalein</option>' +
                      '<option value="-11">(GMT -11:00) Midway Island, Samoa</option>' +
                      '<option value="-10">(GMT -10:00) Hawaii</option>' +
                      '<option value="-9">(GMT -9:00) Alaska</option>' +
                      '<option value="-8">(GMT -8:00) Pacific Time (US & Canada)</option>' +
                      '<option value="-7">(GMT -7:00) Mountain Time (US & Canada)</option>' +
                      '<option value="-6">(GMT -6:00) Central Time (US & Canada), Mexico City</option>' +
                      '<option value="-5">(GMT -5:00) Eastern Time (US & Canada), Bogota, Lima</option>' +
                      '<option value="-4">(GMT -4:00) Atlantic Time (Canada), Caracas, La Paz</option>' +
                      '<option value="-3.5">(GMT -3:30) Newfoundland</option>' +
                      '<option value="-3">(GMT -3:00) Brazil, Buenos Aires, Georgetown</option>' +
                      '<option value="-2">(GMT -2:00) Mid-Atlantic</option>' +
                      '<option value="-1">(GMT -1:00 hour) Azores, Cape Verde Islands</option>' +
                      '<option value="0">(GMT) Western Europe Time, London, Lisbon, Casablanca</option>' +
                      '<option value="1">(GMT +1:00 hour) Brussels, Copenhagen, Madrid, Paris</option>' +
                      '<option value="2">(GMT +2:00) Kaliningrad, South Africa</option>' +
                      '<option value="3">(GMT +3:00) Baghdad, Riyadh, Moscow, St. Petersburg</option>' +
                      '<option value="3.5">(GMT +3:30) Tehran</option>' +
                      '<option value="4">(GMT +4:00) Abu Dhabi, Muscat, Baku, Tbilisi</option>' +
                      '<option value="4.5">(GMT +4:30) Kabul</option>' +
                      '<option value="5">(GMT +5:00) Ekaterinburg, Islamabad, Karachi, Tashkent</option>' +
                      '<option value="5.5">(GMT +5:30) Bombay, Calcutta, Madras, New Delhi</option>' +
                      '<option value="6">(GMT +6:00) Almaty, Dhaka, Colombo</option>' +
                      '<option value="7">(GMT +7:00) Bangkok, Hanoi, Jakarta</option>' +
                      '<option value="8">(GMT +8:00) Beijing, Perth, Singapore, Hong Kong</option>' +
                      '<option value="9">(GMT +9:00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk</option>' +
                      '<option value="9.5">(GMT +9:30) Adelaide, Darwin</option>' +
                      '<option value="10">(GMT +10:00) Eastern Australia, Guam, Vladivostok</option>' +
                      '<option value="11">(GMT +11:00) Magadan, Solomon Islands, New Caledonia</option>' +
                      '<option value="12">(GMT +12:00) Auckland, Wellington, Fiji, Kamchatka</option>' +
                '</select>' +
                '</td></tr>' +
                '<tr><td><h4>STATE/PROVINCE</h4>' +
                '<input class="loginfield" name="state" value="' + data.player.player_state + '" type="text" placeholder="ex. New York"></td><td><h4>CITY/TOWN</h4>' +
                '<input class="loginfield" name="city" value="' + data.player.player_city + '" type="text" placeholder="ex. New York City"></td></tr>' +
                '<tr><td><h4>TWITTER</h4>';

                appendrow +=  twitterProfileElement(data.player.player_twitter);

                appendrow +=  '</td>' +
                '<td><h4>FACEBOOK</h4>' +
                '<div class="input-prepend"><span class="add-on" style="height:24px; padding-top:10px; ">F</span><input class="social" name="facebook" value="' + data.player.player_facebook + '" type="text" placeholder="ex. www.facebook.com/playigl"></div></td></tr>' +
                '<tr><td><h4>WEBSITE</h4>' +
                '<input class="loginfield" name="website" value="' + data.player.player_website + '" type="text" placeholder="ex. www.playigl.com"></td><td>' +
                '<h4>STEAM ID</h4>';

                if(data.player.player_community_id)
                    appendrow +=  '<span class="uneditable-input steamid">' + data.player.player_community_id + '</span>';
                else
                    appendrow +=  '<a href="https://steamcommunity.com/openid/login?openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&openid.mode=checkid_setup&openid.return_to=http://' + window.location.hostname + '/dashboard&openid.realm=http://' + window.location.hostname + '&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select"><img alt="Steam Auth" src="/img/steam_auth.png" /></a>';

                appendrow +=  '</td></tr>' +
                '<tr><td><h4>CHANGE PASSWORD</h4>' +
                '<div class="input-prepend"><span class="add-on" style="height:24px; padding-top:10px; "><i class="icon-lock"></i></span><input class="social" name="password" type="text"></div></td><td><h4>APPLY CHANGES</h4><button type="button" id="profile_save" class="btn btn-action" onclick="saveProfile(' + player + ')">SAVE</button></td></tr>' +
                '<tr><td></td><td></td></tr>' +
                '</table></form>';

                $('#player_profile').html(appendrow);

                //DOB Selector
                $("#dob" ).datepicker({ dateFormat: "mm-dd-yy" });

                // User Age
                $('#dob').change(function() {
                    var dob = new Date($('#dob').val());
                    var today = new Date();
                    var age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
                    $('#age').val(age);
                });

                $('#dob').change();

                //List of countries
                $.getJSON('/api/country/', function(countries) {
                    if (countries.success)
                    {
                        appendcountry = '';
                        $.each(countries.country_list,function (key, value) 
                        {
                            appendcountry = appendcountry + '<option value=' + value.iso;
                                if(value.iso == data.player.player_country.toUpperCase())
                                appendcountry = appendcountry + ' selected="selected"';

                                appendcountry = appendcountry + '>' + value.printable_name + '</option>';
                        });
                        $('#country_list').append(appendcountry);
                    }
                    else
                        alert(countries.error);
                });

                setSelectedIndex(document.getElementById("timezoneoffset"),data.player.player_timezoneoffset);
            }
            else
                $('#player_profile').html('<div class="alert alert-error">' + data.result + '</div>');
        }
    });
}

function saveProfile(player)
{
    $('#profile_save').attr('disabled',true).html('<i class="icon-time icon-white"></i> SAVING...');
    formData = $('#profile_'+ player).serialize();
    
    $.ajax({
        type: "POST",
        url: "/dashboard/profile/save/" + player,
        data: formData,
        dataType: "json",
        success: function(json){
            if(json.success)
                $('#profile_save').attr('disabled',false).text('SAVE');
            else
            {
                alert(json.error);
                $('#profile_save').attr('disabled',false).text('SAVE');
            }
        },
        error: function(xhr, textStatus, errorThrown){
            alert(errorThrown);
        }
    }); 
}

function updateReferrals(player)
{
    hideAll();
    $('#dashboardReferrals').html('<div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div>');
    $('#dashboardReferrals').show();
    
    $.getJSON('/dashboard/referral/list/' + player, function(data) {
        if (data.success == undefined)
            alert('Sorry, there was a problem with this request. Please try again.');
        else
        {
            if(data.success)
            {
                appendrow = '';
                appendrow +=  '<div class="row-fluid"><div class="span6">' +
                    '<form id="referrals_' + player + '" action="ajax/dashboard/referral_send.php">' +
                    '<table class="table table-condensed">' +
                    '<tr><th class="row-header white"><h5>INVITE FRIENDS!</h5></th></tr>';

                referrals_show = 5;
                while(referrals_show > 0) 
                {
                    appendrow += 
                        '<tr><td><input type="text" class="loginfield" name="email[]" placeholder="Email..."></td></tr>';
                    referrals_show--;
                } 



                appendrow +=  '<tr><td><button id="button_referralSend" type="button" class="btn btn-action" onclick="sendReferrals(' + player + ')">SEND</button></td></tr>' +
                    '</table>' +
                    '</form>' +
                    '</div>';

                appendrow +=  '<div class="span6">' +
                    '<table class="table table-condensed">' +
                    '<tr><th colspan=2 class="row-header white"><h5>STATS</h5></th></tr>' +
                    '<tr><td><h6>Conversion Rate</h6></td><td><div class="progress"><div class="bar tooltip-on" title="Activated: ' + data.referrals.activated + ' (' + data.referrals.convert_rate + '%)" style="width:' + data.referrals.convert_rate + '%">' + data.referrals.convert_rate + '%</div></div></td></tr>';

                $.each(data.referrals.sent,function (key, value) 
                {
                    if(value.activated==1)
                        appendrow +=  '<tr><td>' + value.referee + '</td><td><img alt="Accepted" src="/img/check.gif" class="tooltip-on" title="Invite Accepted"></i> <a href="/player/' + value.refereeId + '-' + value.username + '" class="tooltip-on" title="' + value.username + '">' + value.username + '</a></td></tr>';
                    else
                        appendrow +=  '<tr id="referral_' + key + '"><td>' + value.referee + '</td><td><i class="close icon-remove tooltip-on" title="Revoke Invite" onclick="removeReferral(' + key + ',' + player + ',\'' + value.referee + '\')"></i></td></tr>';
                });

                appendrow +=  '</table>' + '</div></div>';

                $('#dashboardReferrals').html(appendrow);
                $('#dashboardReferrals .tooltip-on').tooltip();
            }
            else
            {
                if(referrals.error)
                    alert(referrals.error);
                else
                    alert(referrals.warning);
            }
        }
    });
}

function sendReferrals(player)
{
    $('#button_referralSend').attr('disabled',false).html('<i class="icon-envelope icon-white"></i> SENDING...');
    formData = $('#referrals_'+ player).serialize();
    
    $.ajax({
        type: "POST",
        url: "/dashboard/referral/send/" + player,
        data: formData,
        dataType: "json",
        success: function(json){
            if(json.success)
            {
                if(json.error)
                    $('#dashboardresults').append('<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>' + json.error + '</div>');
                if(json.warning)
                    $('#dashboardresults').append('<div class="alert alert-warning"><button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>' + json.warning + '</div>');

                $('#dashboardresults').append('<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>' + json.message + '</div>');
                updateReferrals(player);
            }
            else
            {
                alert(json.error);
                $('#button_referralSend' + player).attr('disabled',false).text('SEND');
            }
        },
        error: function(xhr, textStatus, errorThrown){
            alert(errorThrown);
        }
    });
}

function updateFollowing(player,action) 
{
    hideAll();
    $('#dashboardFollowing').html('<div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div>');
    $('#dashboardFollowing').show();
    
    $.ajax({
        type: "POST",
        url: "/follow/" + action + "/" + player,
        data: "follower=" + player,
        dataType: "json",
        success: function(data){
            if(data.success)
            {
                $('#dashboardFollowing').html('');
                $('#dashboardFollowing').html('<table id="follow_list" class="table table-condensed table-striped"><tbody><tr><td><button id="button_following" class="btn pull-right" onclick="updateFollowing(' + player +',\'list\')"><i class="icon-pushpin"></i> Following ('+data.num_following+')</button></td><td><button id="button_followers" class="btn" onclick="updateFollowing(' + player +',\'me\')"><i class="icon-star"></i> Followers ('+data.num_followers+')</button></td></tr></tbody></table>');
                $.each(data.following,function (key, value) 
                {
                    if(value.follow == 'player')
                    {
                        if(value.following == true)
                            $('#follow_list tbody').append('<tr><td width="460px"><div><img alt="Avatar" class="img-rounded pull-left smallthumb" style="margin-right: 6px;" src="/img' + value.player_avatar + '.png"><h6> <img alt="' + value.player_country + '" src="/img/flags/' + value.player_country + '.png"> <a href="/player/' + value.player_id + '-' + value.player_username + '">' + value.player_firstname + ' ' + value.player_username + ' ' + value.player_lastname + '</a></h6></div></td><td><button id="follow_' + value.follow + '_' + value.player_id + '" type="button" class="btn btn-info btn-follow" onclick="follow_stop(' + value.player_id + ',' + player + ',\'' + value.follow + '\')"><i class="icon-check icon-large icon-white"></i> Following</button></td></tr>');
                        else
                        {
                            if(action == 'list')
                                $('#follow_list tbody').append('<tr><td width="460px"><div><img alt="Avatar" class="img-rounded pull-left smallthumb" style="margin-right: 6px;" src="/img' + value.player_avatar + '.png"><h6> <img alt="' + value.player_country + '" src="/img/flags/' + value.player_country + '.png"> <a href="/player/' + value.player_id + '-' + value.player_username + '">' + value.player_firstname + ' ' + value.player_username + ' ' + value.player_lastname + '</a></h6></div></td><td><button id="follow_' + value.follow + '_' + value.player_id + '" type="button" class="btn btn-info btn-follow" onclick="follow_stop(' + value.player_id + ',' + player + ',\'' + value.follow + '\')"><i class="icon-check icon-large icon-white"></i> Following</button></td></tr>');
                            else
                                $('#follow_list tbody').append('<tr><td width="460px"><div><img alt="Avatar" class="img-rounded pull-left smallthumb" style="margin-right: 6px;" src="/img' + value.player_avatar + '.png"><h6> <img alt="' + value.player_country + '" src="/img/flags/' + value.player_country + '.png"> <a href="/player/' + value.player_id + '-' + value.player_username + '">' + value.player_firstname + ' ' + value.player_username + ' ' + value.player_lastname + '</a></h6></div></td><td><button id="follow_' + value.follow + '_' + value.player_id + '" type="button" class="btn btn-info btn-follow" onclick="follow(' + value.player_id + ',' + player + ',\'' + value.follow + '\')"><i class="icon-check-empty icon-large icon-white"></i> Follow</button></td></tr>');
                        }
                        
                    }
                    else
                    {
                        if(value.follow = 'team')
                            $('#follow_list tbody').append('<tr><td width="460px"><div><img alt="' + value.team + '" class="img-rounded pull-left smallthumb" style="margin-right: 6px;" src="/img' + value.logo + '.png"><h6> <a href="/team/' + value.team_id + '-' + value.team + '">' + value.team + '</a></h6></div></td><td><button id="follow_' + value.follow + '_' + value.team_id + '" type="button" class="btn btn-info btn-follow" onclick="follow_stop(' + value.team_id + ',' + player + ',\'' + value.follow + '\')"><i class="icon-check icon-large icon-white"></i> Following</button></td></tr>');
                    }

                });
            }
            else
                $('#dashboardFollowing').html('<div class="alert alert-warning">' + data.message + '</div>');
        }
    });
}
    

function setTeamPIN(teamid,action)
{
    formData = $('#teamPIN_' + teamid).serialize();
    $.ajax({
        type: "POST",
        url: "/dashboard/team/pin/" + action + '/' + teamid,
        data: formData,
        dataType: "json",
        success: function(json){
            if(json.success)
            {
                $('#setPassword').popover('hide');
                if(action == 1)
                {
                    $('#teamLocked_' + teamid).removeClass('icon-key');
                    $('#teamLocked_' + teamid).addClass('icon-lock');
                }
                else
                {
                    $('#teamLocked_' + teamid).removeClass('icon-lock');
                    $('#teamLocked_' + teamid).addClass('icon-key');
                }
                $('#teamlist_'+ teamid + '_edit').hide; 
            }
            else
                alert(json.error);
        },
        error: function(xhr, textStatus, errorThrown){
            alert(errorThrown);
        }
    });
}