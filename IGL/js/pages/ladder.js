/* 
 * These elements are used exclusively in /view/team.php
 */
function initLadder() {
    joinButton($('#join_ladder'));
}

function removeChallenge(id) {
    $('#listing_'+id).remove();
    $.ajax({
        type: "POST",
        url: "/ladder/challenge",
        data: {
            id: id, 
            remove: true
        },
        dataType: "json",
        success: function(data) {
            PlayIGL.Analytics.track('team_challenged_removed', {team_id:data.team_name, match_time:data.match_time, game_type:data.ladder});
            if(false==data['success']) {
                alert(data['error']);
            }
        }
    });
}

function doChallenge(id) {
    $.ajax({
        type: "POST",
        url: "/ladder/challenge",
        data: {
            id: id, 
            offset: localTimezone()
        },
        dataType: "json",
        success: function(data) {
            PlayIGL.Analytics.track('team_challenged', {team_id:data.data.team_name, match_time:data.data.match_time, game_type:data.data.ladder});
            if(data['success']) {
                $('#listing_'+id).html('Challenged <i class="icon-check"></i>');
            } else {
                alert(data['error']);
            }
        }
    });
}

function saveAvailability() {
    $('#ladderModalButton').attr('disabled', true).html('Save <i class="icon-spinner icon-spin"></i>');
    
    $.ajax({
        type: "POST",
        url: "/ajax/public/ladder_availability.php",
        data: $("#availability").serialize(),
        dataType: "json",
        success: function(data) {
            if(data['success']) {
                $('#ladderModal').modal('hide');
                document.location.reload(true);
            } else {
                $('#ladderModalButton').attr('disabled', false).text('Save');
                $('#ladderModalBody').append('<div class="alert alert-error"><b>Error:</b> '+data['error']+'.</div>');
            }
        }
    });
}

function availability(tier_id) {
    //set our time frames
    var time = new Date();
    currenttime = time.getTime();
    currenttime = (Math.floor(currenttime / 1800000) * 1800000) + 1800000;
    plus3dys = currenttime + 259200000;
    
       
    $('#ladderModalLabel').text('Set Availability');
    $('#ladderModalButton').attr('disabled', false).unbind().text('Save').click( function() {
        saveAvailability();
    });

    $('#ladderModalBody').html('<div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div>');
    $('#ladderModal').modal({
        show: true
    });
    $('#ladderModalBody').load('/view/game/ladder/availability.php?tier_id='+tier_id+'&offset='+localTimezone(),function() {
        $('.dateTimePicker').datetimepicker(
        {
            minDate: new Date(currenttime),
            maxDate: new Date(plus3dys),
            minute: 0,
            stepMinute: 15,
            dateFormat: "yy-mm-dd",
            currentText: 'Next Available'
        });
    });
    
    
       
}

function localHour(timestamp)
{
    var dt = new Date(timestamp * 1000);
    minutes = dt.getMinutes();
    if(minutes < 10)
    {
        minutes = '0' + minutes;
    }
    if(dt.getHours() > 11)
    {
        hour = dt.getHours() - 12;
        t = 'pm'
    }
    else
    {
        hour = dt.getHours();
        t = 'am'
    }
    if(hour==0)
    {
        hour = 12;
    }
    return  hour + ":" + minutes + t;
}

function challengeTimes(ladder,challenged) {
    var time = new Date();
    currenttime = time.getTime();
    currenttime = (Math.floor(currenttime / 3600000) * 3600000) + 3600000;
    plus3dys = currenttime + 259200000;
    $('#ladderModalLabel').text('CHALLENGE TEAM');
    $('#ladderModalButton').attr('disabled', false).unbind().text('Challenge').click( function() {
        submitChallenge();
    });

    $('#ladderModalBody').html('<div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div>');
    $('#ladderModal').modal({
        show: true
    });
    
    
    $.getJSON('/api/ladder/'+ladder, function(data) {
        if(data.success)
        {
            datePicker = '';
            datePicker +=   '<form id="challengeForm" class="">' +
            '<fieldset class="pull-left span6">' +
            '<h4>Propose Match <small>Must be within 72hrs. Times are GMT'+localTimezone()+'</small></h4>' +
            '<table><tr><td><label><h5>When?</h5><input style="height:30px !important; margin-top:10px" id="challengeTime" type="text" name="time" placeholder="Date/Time"></label></td>' +
            '<td>';
                            

            round = 1;
            while(data['ladder']['ladder_rounds']>=round)
            {
                datePicker +=   '<label><h5>Map ' + round +
                '</h5><select id="map" name="map[]" style="height:36px !important;">';
                $.each(data['ladder']['ladder_maps'], function(i,v){
                    datePicker += '<option value="'+v['map']+'">'+v['map']+'</option>';
                })
                datePicker +=   '</select></label>';
                round = round + 1;
            }

            datePicker +=   '</td></tr></table>' +
            '<input type="hidden" id="offset" name="offset" value="'+localTimezone()+'">' +
            '<input type="hidden" id="ladder" name="ladder" value="'+ladder+'">' +
            '<input type="hidden" id="challenged" name="challenged" value="'+challenged+'">' +
            '<div id="challengeResult"></div>' +
            '</fieldset>' +
            '</form>';
            $('#ladderModalBody').html(datePicker);
            $('#challengeTime').datetimepicker(
            {
                minDate: new Date(currenttime),
                maxDate: new Date(plus3dys),
                minuteMax: 0,
                dateFormat: "yy-mm-dd",
                currentText: 'Next Available'
            });
        }
        else
        {
            $('#ladderModalBody').html('<h3>Error loading challenge form.</h3>');
        }
    });    
}

function submitChallenge()
{
    if($('#challengeTime').val() == '')
    {
        alert('All fields must be completed');
    }
    else
    {
        $('#ladderModalButton').attr('disabled', true).html('Challenge <i class="icon-spin icon-refresh"></i>');
        formData = $('#challengeForm').serialize();
        $.ajax({
            type: "POST",
            url: "/ladder/challenge/",
            data: formData,
            dataType: "json",
            success: function(json){
                if(json.success)
                {
                    $('#ladderModal').modal('hide');
                    $('#mesages').prepend('Challenge Successful!');
                }
                else
                {
                    $('#challengeResult').html('<div class="alert alert-error">'+json.error+'</div>');
                    $('#ladderModalButton').attr('disabled', false).text('Challenge');
                }
            },
            error: function(xhr, textStatus, errorThrown){
                alert(errorThrown);
                $('#ladderModalButton').attr('disabled', false).text('Challenge');
            }
        });
    }
}

function createTeam(btn)
{
    btn.click(function(){
        window.location = '/dashboard/teams'; 
    });
}

function joinButton(btn)
{
    btn.click(function(){
        tier_id = $('#tier_id').val();    
        $('#join_ladder').attr('disabled', true).html('Wait <i class="icon-spin icon-refresh"></i>');
        $.ajax({
            type: "POST",
            url: "/ladder/join/"+tier_id,
            dataType: "json",
            success: function(json){
                if(json.success)
                {
                    document.location.reload(true);
                }
                else
                {
                    alert(json.error);
                    $('#join_ladder').attr('disabled', false);
                }
            },
            error: function(xhr, textStatus, errorThrown){
                alert(errorThrown);
                $('#join_ladder').html('Join Ladder').attr('disabled', false);
            }
        });
    })
}
/*
function updateStandings()
{
    ladder = $('#page_id').val();
    $('#ladder_standings tbody').html('<tr><td colspan="6"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></td></tr>');
    $.getJSON('/api/ladder/' + ladder, function(data) {
        if(data == null)
        {
            alert('Error loading ladder times.');
        }
        else
        {
            if( data.success )
            {
                if( data['ladder']['ladder_name'] )
                {
                    $('#ladder_name').html(data['ladder']['ladder_game_name']+" - "+data['ladder']['ladder_format']+" - "+data['ladder']['ladder_name']);
                }
                
                
                if( data['ladder']['ladder_participating'] ) //if team is participating in the ladder
                {
                    $('#ladder_action').html("<button id='join_ladder' type='button' class='pull-right btn btn-mini btn-default'>Leave Ladder</button>");
                }
                else
                {
                    if( data['ladder']['ladder_entry_level'] && data['ladder']['ladder_captain'] ) //If ladder being viewed is entry level
                    {
                        $('#ladder_action').html("<button id='join_ladder' type='button' class='pull-right btn btn-mini'>Join Ladder</button>");                
                    }
                    else if( !data['ladder']['ladder_captain'] && data['ladder']['ladder_entry_level'] )
                    {
                        $('#ladder_action').html("<button id='create_team' type='button' class='pull-right btn btn-mini'>Create Team</button>");                
                    }
                }
                
                tiers = '';
                $.each( data['tiers'], function(i,v){
                    tier = i+1;
                    tiers += '<a class="btn btn-mini btn-default" href="/ladder/'+v['ladder_id']+'/'+v['ladder_game_name']+'/'+v['ladder_name']+'">Tier '+tier+'</a>&nbsp;';
                });
                        
                $('#ladder_tiers').html(tiers);
                
                
                
                $('#ladder_image').attr("src","/img"+data['ladder']['ladder_game_banner']+"")
                game_id = data['ladder']['ladder_game_id'];
                joinButton($('#join_ladder'));
                createTeam($('#create_team'));
                
                appendrow ='';
                $('#ladder_standings tbody').html('');
                $.each(data['ladder']['ladder_teams'], function(key, value){
                    rank = 1 + key;
                    appendrow += '<tr id="ladder_team_'+value['team_id']+'">';
                    appendrow += '<td>'+rank+'</td>';
                    appendrow += '<td><a href="/team/'+value['team_id']+'-'+value['team_name']+'">'+value['team_tag']+'</a></td>';
                    appendrow += '<td><a href="/team/'+value['team_id']+'-'+value['team_name']+'">'+value['team_name']+'</a> ('+value['team_ladder_wins']+' - '+value['team_ladder_loses']+')';
                    if( data['ladder']['ladder_captain'] && data['ladder']['ladder_participating'] ) 
                    {
                        appendrow += '<div class="pull-right"><a onclick="challengeTimes('+data.ladder['ladder_id']+','+value['team_id']+')"><i class="icon-trophy tooltip-on" title="Challenge"></i></a> <a href="/team/results/'+value['team_id']+'"><i class="icon-bar-chart tooltip-on" title="View History"></i></a></div>';
           
                    }
                    else
                    {
                        appendrow += '<div class="pull-right"><a href="/team/results/'+value['team_id']+'"><i class="icon-bar-chart tooltip-on" title="View History"></i></a></div>';
                    }
                        
                    appendrow += '</td>';
                    appendrow += '<td>'+value['team_points']+'</td>';
                    appendrow += '<td>'+value['team_highest_points']+'</td>';
                    appendrow += '</tr>';
                });
                $('#ladder_standings tbody').append(appendrow);
            }
            else
            {
                $('#ladder_standings tbody').html('<tr><td colspan="6">Error while loading teams</td></tr>');
            }
        }
    });
} 

function poolTimes() {
    ladder = $('#page_id').val();
    $('.tooltip').hide();
    $('#refresh').addClass('icon-spin');
    $.getJSON('/ladder/pool/' + ladder, function(data) {
        if(data == null)
        {
            alert('Error loading ladder times.');
        }
        else
        {
            if(data.success)
            {
                $('#poolTimes').html('');
                $.each(data.pool, function(key, value){

                    pooltime = localHour(value.ladder_time_unix);
                        
                    time = '<li class="dropdown">' +
                    '<a class="dropdown-toggle" data-toggle="dropdown"><span class="tooltip-on" id="'+value.ladder_time_unix+'_registered" title="'+value.ladder_num_teams+' teams signed up">'+pooltime+' (<span id="'+value.ladder_time_unix+'">'+value.ladder_num_teams+'</span>)</span></a>';
                             
                    if(data.captain)
                    {
                        time += '<ul id="pool_'+value.ladder_time_unix+'" class="dropdown-menu">';
                                    
                        if(value.ladder_registered)
                        {
                            time += '<li><a id="'+value.ladder_time_unix+'_register" onclick="registerTeam('+ladder+','+value.ladder_time_unix+',\'remove\')">Registered <i class="icon-check"></i></a></li>';
                        }
                        else
                        {
                            time += '<li><a id="'+value.ladder_time_unix+'_register" onclick="registerTeam('+ladder+','+value.ladder_time_unix+',\'register\')">Register <i class="icon-check-empty"></i></a></li>';
                        }
                    }
                    $('#poolTimes').append(time);
                                
                                
                    if(value.ladder_maps)
                    {
                        $('#pool_'+value.ladder_time_unix).append('<li class="divider"></li>');
                        $.each(value.ladder_maps, function(k,v){
                            map_list = '<li><a id="vote_'+k+'_'+value.ladder_time_unix+'" data-time="'+value.ladder_time_unix+'" data-map="'+v['map']+'" data-ladder="'+ladder+'">'+v['map']+' (<span id="votes_'+value.ladder_time_unix+'_'+v['map']+'">'+v['votes']+'</span>)</a></li>';
                            $('#pool_'+value.ladder_time_unix).append(map_list);
                            //bind vote function
                            vote($('#vote_'+k+'_'+value.ladder_time_unix));
                        });
                    }
                                
                                
                    time = '</li>' +
                    '<li class="divider-vertical"></li>';
                    $('#poolTimes').append(time);
                })
                time = '<li><a onclick="poolTimes()"><i id="refresh" class="icon-refresh"></i></a></li>';
                $('#poolTimes').append(time);
                
                
                $('#refresh').removeClass('icon-spin');
            }
        }
            
    });
}


function matchTime()
{
    dates = $('#ladder_schedule').find('td[data-time]');
    $.each(dates, function(k,v) {
        $(this).text(showLocalDate($(this).attr('data-time')));
    });
    $('#timezone').text('Date (GMT '+localTimezone()+')');
}

function vote(btn)
{
    btn.click(function(){
        
        //Get voting variables
        ladder = $(this).attr('data-ladder');
        map = $(this).attr('data-map');
        time = $(this).attr('data-time');
    
        $.ajax({
            type: "POST",
            url: "/ladder/vote/",
            data: 'ladder='+ladder+'&map='+map+'&time='+time,
            dataType: "json",
            success: function(data){
                if(data.success) {
                    poolTimes();

                } else {

                    alert(data.error);
                }
            }
        })
    
    
    });
}
*/
function registerTeam(ladder,time,action) {
    num_registered = $('#'+time).text();
    $('#'+time).html('<i class="icon-spin icon-refresh"></i>');
    $.getJSON('/ladder/register/team/' + ladder + '/' + time + '/' + action, function(data) {
        if(action=='register')
        {
            num_regd = parseInt(num_registered,10) + 1;
            attr = 'registerTeam('+ladder+','+time+',\'remove\')';
            text = 'Registered <i class="icon-check"></i>';
        }
        else if(action == 'remove')
        {
            $(data.vote).text(parseInt($(data.vote).text(),10) -1);
            num_regd = parseInt(num_registered,10) - 1;
            attr = 'registerTeam('+ladder+','+time+',\'register\')';
            text = 'Register <i class="icon-check-empty"></i>';
        }
        else
        {
            num_regd = 0;
        }
        
        
        if(data.success)
        {
            $('#'+time).text(num_regd);
            $('#'+time+'_registered').attr('data-original-title',num_regd+' Teams Registered');
            $('#'+time+'_register').attr('onclick',attr).html(text);  
        }
        else
        {
            $('#'+time).text(num_registered);
            alert(data.error);
        }
    });
}