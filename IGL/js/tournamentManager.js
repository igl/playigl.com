var tournamentManager = {
    loader: '/img/tournamentLoader.gif',
    actions: {
        list_events: '/ajax/admin/tournament_manager/list_events.php',
        new_event: '/ajax/admin/tournament_manager/new_event.php',
        save_event: '/ajax/admin/tournament_manager/save_event.php',
        edit_event: '/ajax/admin/tournament_manager/edit_event.php',
        edit_round: '/ajax/admin/tournament_manager/edit_round.php',
        update_event: '/ajax/admin/tournament_manager/update_event.php',
        delete_event: '/ajax/admin/tournament_manager/delete_event.php',
        delete_member: '/ajax/admin/tournament_manager/delete_member.php',
        delete_round: '/ajax/admin/tournament_manager/delete_round.php',
        edit_event_members: '/ajax/admin/tournament_manager/edit_event_members.php',
        update_event_members: '/ajax/admin/tournament_manager/update_event_members.php',
        update_event_match: '/ajax/admin/tournament_manager/update_event_match.php',
        edit_match: '/ajax/admin/tournament_manager/edit_match.php',
        add_event_members: '/ajax/admin/tournament_manager/add_event_members.php',
        add_event_schedule: '/ajax/admin/tournament_manager/add_event_schedule.php',
        edit_event_schedule: '/ajax/admin/tournament_manager/edit_event_schedule.php',
        update_event_schedule: '/ajax/admin/tournament_manager/update_event_schedule.php',
        upload_event_banner: '/ajax/admin/tournament_manager/upload_event_banner.php'
    },
    call: function(action, params, callback) {
        $('#tournamentManagerInner').html('<div style="margin:0px auto; padding-top:20px; width:20px;"><img src="'+tournamentManager.loader+'" /></div>');
        $.ajax({ 
            type: "POST",
            url: action,
            cache: false,
            data: params
        }).done(function(response) {
            $('#tournamentManagerInner').html(response);
            PlayIGL.viewChange(true);
            tournamentManager.bindButtons();
            if (callback && typeof(callback) === "function") {
                callback();
            }
        });
    },
    bindButtons: function() {
        $('#newEventBtn').unbind('click').click(function() {
            tournamentManager.call(tournamentManager.actions.new_event, {}, function() { $('#newEventBtn').hide(); $('#saveEventBtn').show(); new_eventCallback(); }); 
        });
        $('#backBtn').unbind('click').click(function() {
            tournamentManager.call(tournamentManager.actions.list_events, {}, function() { $('#newEventBtn').show(); $('#saveEventBtn').hide(); $('#updateEventBtn').hide(); $('#updateMembersBtn').hide(); $('#updateScheduleBtn').hide(); $('#updateMatchBtn').hide(); }); 
        });
        $('#saveEventBtn').unbind('click').click(function() {
            tournamentManager.call(tournamentManager.actions.save_event, $('#newEventForm').serializeArray(), null);  
        });
        $('#updateEventBtn').unbind('click').click(function() {
            tournamentManager.call(tournamentManager.actions.update_event, $('#newEventForm').serializeArray(), null);  
        });
        $('#updateMembersBtn').unbind('click').click(function() {
            tournamentManager.call(tournamentManager.actions.update_event_members, $('#newEventForm').serializeArray(), null);  
        });
        $('#updateScheduleBtn').unbind('click').click(function() {
            tournamentManager.call(tournamentManager.actions.update_event_schedule, $('#newEventForm').serializeArray(), null);  
        });
        $('#updateRoundBtn').unbind('click').click(function() {
            tournamentManager.call(tournamentManager.actions.update_event_round, $('#newEventForm').serializeArray(), null);  
        });
        $('#updateMatchBtn').unbind('click').click(function() {
            tournamentManager.call(tournamentManager.actions.update_event_match, $('#newEventForm').serializeArray(), null);  
        });
        $('.editEventBtn').unbind('click').click(function() {
            tournamentManager.call(tournamentManager.actions.edit_event, { id: $(this).parent().parent().find('.id').html() }, function() { $('#newEventBtn').hide(); $('#updateEventBtn').show(); edit_eventCallback(); });  
        });
        $('.editRoundBtn').unbind('click').click(function() {
            console.log($(this).parent().parent().data('round-id'));
            tournamentManager.call(tournamentManager.actions.edit_round, { id: $(this).parent().parent().data('round-id') }, function() { $('#updateScheduleBtn').show(); edit_roundCallback(); });  
        });
        $('.editMatchBtn').unbind('click').click(function() {
            tournamentManager.call(tournamentManager.actions.edit_match, { id: $(this).parent().parent().find('.id').html() }, function() { $('#updateMatchBtn').show(); edit_matchCallback(); });  
        });
        $('.membersBtn').unbind('click').click(function() {
            tournamentManager.call(tournamentManager.actions.edit_event_members, { id: $(this).parent().parent().find('.id').html() }, function() { $('#newEventBtn').hide(); $('#updateMembersBtn').show(); members_eventCallback(); });  
        });
        $('.bannerBtn').unbind('click').click(function() {
            tournamentManager.call(tournamentManager.actions.upload_event_banner, { id: $(this).parent().parent().find('.id').html() }, function() { $('#newEventBtn').hide(); banner_eventCallback(); });  
        });
        $('.addTeamBtn').unbind('click').click(function() {
            tournamentManager.call(tournamentManager.actions.add_event_members, { id: $('#memberList').val(), tid: $('input[name="id"]').val() }, function() { $('#updateMembersBtn').hide(); $('#updateScheduleBtn').hide();} );  
        });
        $('.addRoundBtn').unbind('click').click(function() {
            tournamentManager.call(tournamentManager.actions.add_event_schedule, { id: $('#sizeList').val(), tid: $('input[name="id"]').val() }, function() {$('#updateMembersBtn').hide(); $('#updateScheduleBtn').hide();} );  
        });
        $('.scheduleBtn').unbind('click').click(function() {
            tournamentManager.call(tournamentManager.actions.edit_event_schedule, { id: $(this).parent().parent().find('.id').html() }, function() { $('#newEventBtn').hide(); schedule_eventCallback(); });  
        });
        $('.deleteRoundBtn').unbind('click').click(function() {
            console.log($(this).parent().parent());
            if (confirm('Are you sure you wish to delete this round?')==true) {
                tournamentManager.call(tournamentManager.actions.delete_round, { id: $(this).parent().parent().find('.id').html() }, null); 
            }
        });
        $('.deleteEventBtn').unbind('click').click(function() {
            console.log($(this).parent().parent());
            if (confirm('Are you sure you wish to delete this event?')==true) {
                tournamentManager.call(tournamentManager.actions.delete_event, { id: $(this).parent().parent().find('.id').html() }, null); 
            }
        });
        $('.deleteMemberBtn').unbind('click').click(function() {
            console.log($(this).parent().parent());
            if (confirm('Are you sure you wish to delete this member?')==true) {
                tournamentManager.call(tournamentManager.actions.delete_member, { id: $(this).parent().parent().find('.id').html() }, null); 
            }
        });
    }
}

window.onload = function() {
    tournamentManager.call(tournamentManager.actions.list_events, {}, null);
    tournamentManager.bindButtons();
}







