function aboutImport() {
    $('#ModalLabel').text('Importing your UGC roster.');
    $('#ModalBody').html('<ol><li>You must be both a UGC team captain & an IGL team captain</li><li>Your UGC clan ID can be taken from the web address.</li><ul><li>http://www.ugcleague.com/team_page.cfm?clan_id=<b>1234</b></li></ul><li>Importing your roster creates IGL user accounts for each player based on their Steam ID.</li><li>Invites are sent to each valid user. Users are <b>not</b> automatically added to your roster</li><li>Users must log into their IGL account using Steam, at www.playigl.com</li></ol>');
    $('#Modal').modal({
        show: true
    });
}

var Player = Backbone.Model.extend({
  modelName: "player",
  urlRoot: "/backbone/player"
});

var TeamBuilder = Backbone.Model.extend({
  modelName: "team_builder",
  urlRoot: "/backbone/team/"
});

var TeamBuilderHTML = Backbone.View.extend({
  tagName: "form",
  className: "form form-horizontal",
  template: _.template($("#team_builder_template").html()),
  initialize: function(opt) {
    this.hawk = opt.hawk;
    this.target = opt.target || $("#team_builder_" + this.model.get("id") + " td");
    this.model.set("target", typeof opt.target === "undefined" ? false : true);
    this.model.on("sync", this.parseData, this);
    this.model.fetch({
      data: $.param({
        action: "get team recruitment"
      })
    });

    progressBar(this.target);
  },
  events: {
    "click button[name='close']": "dispose",
    "click button.import-ugc": "importUGCRoster",
    "click button.promote-captain": "promoteCaptain",
    "click button.promote-alternate": "promoteAlternate",
    "click button.demote-alternate": "demoteAlternate",
    "click button.remove-player": "removePlayerFromTeam",
    "click button.cancel-invite": "cancelPendingInvite",
    "click button.accept-join": "acceptJoinRequest",
    "click button.reject-join": "rejectJoinRequest",
    "click button.invite-player": "invitePlayerToTeam",
    "click button.invite-email": "invitePlayerToIGL"
  },
  addToDOM: function() {
    this.target.empty();
    this.target.html(this.render().el);
  },
  render: function() {
    this.$el.html(this.template(this.model.attributes));
    this.delegateEvents();
    return this;
  },
  dispose: function() {
    this.$el.parent().remove();
    this.remove();
  },
  parseData: function() {
    var Players = Backbone.Collection.extend({
      model: Player,
      url: "/backbone/player/"
    });

    this.model.set("roster", new Players(this.model.get("roster")));
    this.model.set("invites", new Players(this.model.get("invites")));
    this.model.set("joins", new Players(this.model.get("joins")));
    this.model.set("free_agents", new Players(this.model.get("free_agents")));
    this.addToDOM();
  },
  importUGCRoster: function(e) {
    e.preventDefault();

    saveState(e.target, "");

    var that = this;
    var id = $(e.target).attr("data-id");
    var player = new Player();

    player.set("action", "import roster from ugc");
    player.set("team_id", this.model.get("id"));
    player.set("clan_id", $("#clan_id").val());

    player.save({
    }, {
      success: function() {
        that.hawk.trigger("updateTeamData", that.model.get("id"));
        player.destroy();
      }
    });
  },
  promoteCaptain: function(e) {
    e.preventDefault();

    saveState(e.target, "");

    var that = this;
    var id = $(e.target).attr("data-id");
    var player = this.model.get("roster").get(id);

    player.set("action", "promote player to captain");
    player.set("alternate_id", this.model.get("alternate_id"));
    player.set("team_id", this.model.get("id"));
    this.listenToOnce(player, "sync", this.addToDOM);

    player.save({
    }, {
      success: function() {
        that.hawk.trigger("updateTeamData", that.model.get("id"));
        that.dispose();
      }
    });
  },
  demoteAlternate: function(e) {
    e.preventDefault();

    saveState(e.target, "");

    var that = this;
    var id = $(e.target).attr("data-id");
    var player = this.model.get("roster").get(id);

    player.set("action", "demote player from alternate captain");
    player.set("alternate_id", 0);
    player.set("team_id", this.model.get("id"));
    this.listenToOnce(player, "sync", this.addToDOM);

    player.save({
    }, {
      success: function() {
        that.hawk.trigger("updateTeamData", that.model.get("id"));
        that.dispose();
      }
    });
  },
  promoteAlternate: function(e) {
    e.preventDefault();

    saveState(e.target, "");

    var that = this;
    var id = $(e.target).attr("data-id");
    var player = this.model.get("roster").get(id);

    player.set("action", "promote player to alternate captain");
    player.set("team_id", this.model.get("id"));
    player.set("team_name", this.model.get("team_name"));
    this.listenToOnce(player, "sync", this.addToDOM);

    player.save({
    }, {
      success: function() {
        that.model.set("alternate_id", player.get("id"));
      }
    });
  },
  removePlayerFromTeam: function(e) {
    e.preventDefault();

    saveState(e.target, "");

    var that = this;
    var id = $(e.target).attr("data-id");
    var player = this.model.get("roster").get(id);

    player.set("action", "remove player from team");
    player.set("alternate_id", this.model.get("alternate_id"));
    player.set("team_id", this.model.get("id"));
    player.set("team_name", this.model.get("team_name"));
    this.listenToOnce(this.model.get("roster"), "remove", this.addToDOM);

    player.save({
    }, {
      success: function() {
        if (that.model.get("roster").length - 1 == 1)
          that.hawk.trigger("updateTeamData", that.model.get("id"));

        player.destroy();
      }
    });
  },
  cancelPendingInvite: function(e) {
    e.preventDefault();

    saveState(e.target, "");

    var id = $(e.target).attr("data-id");
    var player = this.model.get("invites").get(id);

    player.set("action", "cancel invite to team");
    player.set("team_id", this.model.get("id"));
    player.set("team_name", this.model.get("team_name"));
    this.listenToOnce(this.model.get("invites"), "remove", this.addToDOM);

    player.save({
    }, {
      success: function() {
        player.destroy();
      }
    });
  },
  acceptJoinRequest: function(e) {
    e.preventDefault();

    saveState(e.target, "");

    var that = this;
    var id = $(e.target).attr("data-id");
    var player = this.model.get("joins").get(id);
    var record = player.clone();

    player.set("action", "accept team join request");
    player.set("game_id", this.model.get("game_id"));
    player.set("team_id", this.model.get("id"));
    player.set("team_name", this.model.get("team_name"));
    this.listenToOnce(this.model.get("roster"), "add", this.addToDOM);

    player.save({
    }, {
      success: function() {
        that.model.get("joins").remove(player);
        that.model.get("roster").add(record);

        if (that.model.get("roster").length == 2)
          that.hawk.trigger("updateTeamData", that.model.get("id"));
      }
    });
  },
  rejectJoinRequest: function(e) {
    e.preventDefault();

    saveState(e.target, "");

    var id = $(e.target).attr("data-id");
    var player = this.model.get("joins").get(id);

    player.set("action", "reject team join request");
    player.set("game_id", this.model.get("game_id"));
    player.set("team_id", this.model.get("id"));
    player.set("team_name", this.model.get("team_name"));
    this.listenToOnce(this.model.get("joins"), "remove", this.addToDOM);

    player.save({
    }, {
      success: function() {
        player.destroy();
      }
    });
  },
  invitePlayerToTeam: function(e) {
    e.preventDefault();

    saveState(e.target, "Invite");

    var that = this;
    var id = $(e.target).attr("data-id");
    var player = this.model.get("free_agents").get(id);
    var record = player.clone();

    player.set("action", "invite player to join team");
    player.set("game_id", this.model.get("game_id"));
    player.set("team_id", this.model.get("id"));
    player.set("team_name", this.model.get("team_name"));
    this.listenToOnce(this.model.get("invites"), "add", this.addToDOM);

    player.save({
    }, {
      success: function() {
        that.model.get("free_agents").remove(player);
        that.model.get("invites").add(record);
      }
    });
  },
  invitePlayerToIGL: function(e) {
    e.preventDefault();

    saveState(e.target, "Invite");
    
    var that = this;
    var id = $(e.target).attr("data-id");
    var player = new Player();
    
    player.set("action", "invite email to join team");
    player.set("team_id", this.model.get("id"));
    player.set("email", this.target.find(".email-addr").val());

    player.save({
    }, {
      success: function() {
        that.target.find(".email-addr").val(null);
        saveState(e.target, "Invite", false);
      }
    });
  }
});