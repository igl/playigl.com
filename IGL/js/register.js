// ***************************************
// Registration Class
// Copyright PlayIGL.com
// Author: Kevin Langlois*
// ***************************************
Registration = {
    info: {},
// ***************************************
// (V2) Start Registration Process
// ***************************************
    start: function() {
        // Default Values for Facebook Populated Fields
        Registration.info.username = "";
        Registration.info.email = "";
        Registration.info.password = "";
        Registration.info.firstname = "";
        Registration.info.lastname = "";
        Registration.info.communityid = "";
    },
// ***************************************
// (V2) Check Information
// ***************************************
    checkInformation: function() {
        $('#register-username').attr('readonly', true);
        $('#register-email').attr('readonly', true);
        $('#register-password').attr('readonly', true);
        $.ajax({
                url: '/ajax/admin/register.php?action=checkinfo&register-username='+$('#register-username').val()+'&register-email='+$('#register-email').val()+'&register-password='+$('#register-password').val(),
                success: function(data) {
                    if (data.success == false) {
                        $('#register-error-1').html('<i class="icon-warning-sign"></i> '+data.error);
                        $('#register-error-1').show();
                        $('#register-username').attr('readonly', false);
                        $('#register-email').attr('readonly', false);
                        $('#register-password').attr('readonly', false);
                    } else {
                        Registration.info.username = $('#register-username').val();
                        Registration.info.email = $('#register-email').val();
                        Registration.info.password = $('#register-password').val();
                        Registration.submitProfile();
                    }
                }
            });
    },
// ***************************************
// (V2) Check Detailed Information
// ***************************************
    checkDetailedInformation: function() {
        $('#register-username2').attr('readonly', true);
        $('#register-email2').attr('readonly', true);
        $('#register-password2').attr('readonly', true);
        $('#register-firstname').attr('readonly', true);
        $('#register-lastname').attr('readonly', true);
        $.ajax({
                url: '/ajax/admin/register.php?action=checkinfo&register-username='+$('#register-username2').val()+'&register-email='+$('#register-email2').val()+'&register-password='+$('#register-password2').val()+'&register-steam='+Registration.info.communityid,
                success: function(data) {
                    if (data.success == false) {
                        $('#register-error-2').html('<i class="icon-warning-sign"></i> '+data.error);
                        $('#register-error-2').show();
                        $('#register-username2').attr('readonly', false);
                        $('#register-email2').attr('readonly', false);
                        $('#register-password2').attr('readonly', false);
                        $('#register-firstname').attr('readonly', false);
                        $('#register-lastname').attr('readonly', false);
                    } else {
                        Registration.info.username = $('#register-username2').val();
                        Registration.info.email = $('#register-email2').val();
                        Registration.info.password = $('#register-password2').val();
                        Registration.info.firstname = $('#register-firstname').val();
                        Registration.info.lastname = $('#register-lastname').val();
                        Registration.submitProfile();
                    }
                }
            });
    },
// ***************************************
// Check Username
// ***************************************
/*
    checkUsername: function(nextStep) {
        if ($('#usernameInput').val().length > 0) {
            $('.username-taken').hide();
            $('.username-avail').hide();
            var username = $('#usernameInput').val();
            $.ajax({
                url: '/ajax/admin/register.php?action=checkname&username=' + username,
                success: function(data) {
                    if (data.available == false) {
                        $('.username-taken').html('The username "' + username + '" is <span>taken</span>.');
                        $('.username-taken').show()
                    } else {
                        $('.username-avail').html('The username "' + username + '" is <span>available</span>.');
                        $('.username-avail').show();
                        if (nextStep) {
                            Registration.registerUsername(username);
                        }
                    }
                }
            });
        } else {
            $('#usernameInput').focus();
        }
        PlayIGL.Analytics.track('Checked Username Availability', {'referer': document.referrer});
    },
        */
// ***************************************
// Register Username
// ***************************************
/*
    registerUsername: function(username) {
        Registration.info.username = username;
        register1();
    },
        */
// ***************************************
// Connect Steam
// ***************************************
    connectSteam: function(username) {
        var steamWindow = window.open('https://steamcommunity.com/openid/login?openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&openid.mode=checkid_setup&openid.return_to=http://'+document.domain+'/ajax/admin/steamCallback.php&openid.realm=http://'+document.domain+'&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select', 'Steam Community Connect', 'width=980,height=' + $(window).height());

    },
    steamCallback: function(steam_id, swindow) {
        swindow.close();
        if (window.location.href.indexOf('dashboard') > -1) {
            $('#community_id').val(steam_id);
        } else {
            Registration.info.communityid = steam_id;     
        }
        $('.steami').removeClass('icon-question-sign').addClass('icon-ok');
        $('.steam span').text('Connected');
        $('#Registration-Step-1').hide(400, function() {
           $('#Registration-Step-2').show(); 
        });
    },
// ***************************************
// Connect Twitch
// ***************************************
    connectTwitch: function(username) {
        var twitchWindow = window.open('https://api.twitch.tv/kraken/oauth2/authorize?response_type=code&client_id=sybsuxgjrk6hatwwlgabrcy0315kuyu&redirect_uri=http://www.playigl.com/ajax/admin/twitchCallback.php&scope=user_read+channel_read', 'Twitch Connect', 'width=980,height=' + $(window).height());
    },
    twitchCallback: function(twitch_id, swindow) {
        swindow.close();
        if (window.location.href.indexOf('dashboard') > -1) {
            $('#twitch').val(twitch_id);
        } else {

            Registration.info.twitch = twitch_id;
        }
        console.log(Registration.info.twitch);
        $('.twitchi').removeClass('icon-question-sign').addClass('icon-ok');
        $('.twitch span').text('Connected');
        $('#Registration-Step-1').hide(400, function() {
           $('#Registration-Step-2').show(); 
        });
    },
// ***************************************
// Connect Facebook
// ***************************************
    connectFacebook: function(username) {
        var facebookWindow = window.open('http://www.facebook.com/dialog/oauth/?client_id=190820527646793&redirect_uri=' + escape("http://www.playigl.com/ajax/admin/facebookCallback.php") + '&state=igl&display=popup', 'Facebook Connect', 'width=980,height=' + $(window).height());

    },
    facebookCallback: function(facebook_token, facebook_name, facebook_user, swindow) {
        swindow.close();
        facebook_user = jQuery.parseJSON(facebook_user);
        console.log(facebook_user);
        if (window.location.href.indexOf('dashboard') > -1) {
            $('#facebook_token').val(facebook_token);
        } else {
            Registration.info.facebook_token = facebook_token;
            Registration.info.facebook = facebook_name;
            if (facebook_user.first_name) {
                Registration.info.firstname = facebook_user.first_name;
            }
            if (facebook_user.last_name) {
                Registration.info.lastname = facebook_user.last_name;
            }
        }
        $('.facebooki').removeClass('icon-question-sign').addClass('icon-ok');
        $('.facebook span').text(Registration.info.facebook);
        $('#Registration-Step-1').hide(400, function() {
           $('#Registration-Step-2').show(); 
        });
    },
// ***************************************
// Connect Twitter
// ***************************************
    connectTwitter: function(username) {
        var twitterWindow = window.open('/twitterCallback.php', '', 'width=980,height=600');

    },
    twitterCallback: function(twitter_token, twitter_info, swindow) {
        swindow.close();
        if (window.location.href.indexOf('dashboard') > -1) {
            $('#twitter_token').val(twitter_token);
        } else {
            Registration.info.twitter_token = twitter_token;
            Registration.info.twitter = twitter_info;
        }
        $('.twitteri').removeClass('icon-question-sign').addClass('icon-ok');
        $('.twitter span').text(Registration.info.twitter);
        $('#Registration-Step-1').hide(400, function() {
           $('#Registration-Step-2').show(); 
        });
    },
// ***************************************
// Personal Information
// ***************************************
/*
    registerUserInfo: function() {
        if ($('#email').val() != "" && $('#password').val() != "" && $('#password_confirm').val() != "") {
            if ($('#password').val() == $('#password_confirm').val()) {
                var personalInfo = $('#personalInfoForm').serializeArray();
                $(personalInfo).each(function(i, v) {
                    Registration.info[v.name] = v.value;
                });
                register3();
            } else {
                alert('Password does not match');
            }
        } else {
            alert('Please complete all required fields.');
        }
        PlayIGL.Analytics.track('Registered Personal Informaton', {'referer': document.referrer});
    },
    */
// ***************************************
// Personal Information
// ***************************************
    submitProfile: function() {
        console.log();
        $.ajax({
            url: '/ajax/admin/register.php?action=register',
            data: Registration.info,
            success: function(response) {
                if (response.success) {
                    if (Registration.info.twitter) {
                        var tweetUser = 'http://www.playigl.com/tweetUser.php?username=' + Registration.info.username;
                        $.ajax({url: tweetUser, success: function(r) {
                                console.log(r);
                                document.location = "/thanks-for-signing-up";
                            }});
                    } else {
                        document.location = "/thanks-for-signing-up";
                    }
                    var d = new Date();
                    var n = d.getTime()/1000;
                    console.log(response);
                    analytics.identify({id: response.user_id, email : response.email, created_at: n, username: response.username});
                    PlayIGL.Analytics.track('account_created', {id: response.user_id, email : response.email, username: response.username});
                } else {
                    console.log(response);
                    $('#register-error-0').html('Ooops! Looks like something went wrong. Please contact support@playigl.com to follow up.').show();
                    done();
                }
            }
        });
    },
// ***************************************
// Personal Information
// ***************************************
    done: function() {
        document.location = "/login";
    }

};
/*
function register(going_back) {

    if (going_back) {
        $('#register-step-2').fadeOut(function() {
            $('#register-step-1').fadeIn();
        });
    }

    $('#register-steps div').removeClass('active-step');
    $('#step1').addClass('active-step');

    $('#usernameInput').keypress(function(e) {
        if (e.which == 13) {
            Registration.checkUsername(true);
            return false;
        }
    });

    if (Registration.info.username) {
        $('#usernameInput').val(Registration.info.username);
    }

    PlayIGL.Analytics.track('Registration Step 1', {'referer': document.referrer});
}

function register1(going_back) {

    if (going_back) {
        $('#register-step-3').fadeOut(function() {
            $('#register-step-2').fadeIn();
        });
    } else {
        $('#register-step-1').fadeOut(function() {
            $('#register-step-2').fadeIn();
        });
    }

    $('#register-steps div').removeClass('active-step');
    $('#step1').addClass('active-step');
    $('#step2').addClass('active-step');


    $('.tooltip-on').tooltip();

    if (Registration.info.twitter) {
        $('.twitteri').removeClass('icon-question-sign').addClass('icon-ok');
        $('.twitter span').text(Registration.info.twitter);
    }
    if (Registration.info.facebook) {
        $('.facebooki').removeClass('icon-question-sign').addClass('icon-ok');
        $('.facebook span').text(Registration.info.facebook);
    }
    if (Registration.info.twitch) {
        $('.twitchi').removeClass('icon-question-sign').addClass('icon-ok');
        $('.twitch span').text('Connected');
    }
    if (Registration.info.communityid) {
        $('.steami').removeClass('icon-question-sign').addClass('icon-ok');
        $('.steam span').text('Connected');
    }

    PlayIGL.Analytics.track('Registration Step 2', {'referer': document.referrer});
}

function register2(going_back) {

    if (going_back) {
        $('#register-step-4').fadeOut(function() {
            $('#register-step-3').fadeIn();
        });
    } else {
        $('#register-step-2').fadeOut(function() {
            $('#register-step-3').fadeIn();
        });
    }

    $('#register-steps div').removeClass('active-step');
    $('#step1').addClass('active-step');
    $('#step2').addClass('active-step');
    $('#step3').addClass('active-step');

    $('#password_confirm').keypress(function(e) {
        if (e.which == 13) {
            Registration.registerUserInfo();
        }
    });

   PlayIGL.Analytics.track('Registration Step 3', {'referer': document.referrer});

}

function register3() {

    $('#register-step-3').fadeOut(function() {
        $('#register-step-4').fadeIn();
    });

    $('#register-steps div').removeClass('active-step');
    $('#step1').addClass('active-step');
    $('#step2').addClass('active-step');
    $('#step3').addClass('active-step');
    $('#step4').addClass('active-step');

    $('#register-step-4 .username').text(Registration.info.username);
    $('#register-step-4 .email').text(Registration.info.email);

    if (Registration.info.steam) {
        $('#register-step-4 .steam span').html('Connected');
    }
    if (Registration.info.twitter) {
        $('#register-step-4 .twitter span').html('Connected');
    }
    if (Registration.info.facebook) {
        $('#register-step-4 .facebook span').html('Connected');
    }
    if (Registration.info.twitch) {
        $('#register-step-4 .twitch span').html('Connected');
    }

    PlayIGL.Analytics.track('Registration Step 4', {'referer': document.referrer});
}
*/
