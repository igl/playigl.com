// ***************************************
// Tournament Class
// Copyright PlayIGL.com
// Author: Kevin Langlois
// ***************************************

var Tournament = { 
    admin: {},
    tournament_id: 0,
    player_id: '',
    team_array: new Array(),
    data: 0,
// ***************************************
// Load Initial Data, Set Banner and Registration Type
// ***************************************
    init: function() { 
        console.log($('#tournament_content'));
        Tournament.tournament_id = $('#tournament_content').attr('data-tournament-id');
        Tournament.player_id = $('#tournament_content').attr('data-player-id');
        Tournament.activity(true);
        $.ajax({ url: "/api/tournament.php?id="+Tournament.tournament_id }).done(function(response) {
            Tournament.data = response;
            //Tournament.setBanner(response.tournament.tournament_game_banner);       
            Tournament.setProgress();
            Tournament.setTeamCount($(response.tournament.tournament_teams).length);
            Tournament.setTeamArray(response.tournament.tournament_teams);
            Tournament.activity(false);
            Tournament.navAbout($('#about'));
        });
    },
// ***************************************
// Load Initial Data, Set Banner and Registration Type
// ***************************************
    reload: function(func) { 
        $.ajax({ url: "/api/tournament.php?id="+Tournament.tournament_id }).done(function(response) {
            Tournament.data = response;
            Tournament.setTeamCount($(response.tournament.tournament_teams).length);
            Tournament.setTeamArray(response.tournament.tournament_teams);
            func();
        });
    },
// ***************************************
// Set Banner
// ***************************************
    setBanner: function(banner) {
        $('#tournament_game_banner').attr('src', '/img'+banner);
    },
// ***************************************
// Set Banner
// ***************************************
    setTeamCount: function(c) {
        $('#team_nav_count').html("Participants <small>("+c+")</small>");
    },
// ***************************************
// Update Page Content Below Navigation
// ***************************************
    setPageContent: function(content) {
        $('#tournament_content').html(content);
        PlayIGL.viewChange();
    },
// ***************************************
// Populate Team Array
// ***************************************
    setTeamArray: function(tournament_teams) {
        Tournament.team_array.length = 0;
        $(tournament_teams).each(function(i, v) {
            Tournament.team_array.push(v.team_id);
        });
        console.log(Tournament.team_array);
    },
// ***************************************
// Update Tournament Progress
// *************************************** 
    setProgress: function() {
        console.log(Tournament.data.tournament);
        if (Tournament.data.tournament.currentdatetime > Tournament.data.tournament.tournament_registrationstart && Tournament.data.tournament.currentdatetime < Tournament.data.tournament.tournament_registrationend) {
            $("#event-progress-bar .bar").css('width', '25%');
            $("#event-progress-bar .text").text('Status: Registration Open');
            $('#event-action-btn').text('Sign Up Now').click(function() { Tournament.navRegistration("Open"); });
        } else if (Tournament.data.tournament.currentdatetime > Tournament.data.tournament.tournament_checkinstart && Tournament.data.tournament.currentdatetime < Tournament.data.tournament.tournament_checkinend) {
            $("#event-progress-bar .bar").css('width', '50%');
            $("#event-progress-bar .text").text('Status: Participant Check In');
            $('#event-action-btn').text('Check In').click(function() { Tournament.navCheckin(); });
        } else if (Tournament.data.tournament.currentdatetime > Tournament.data.tournament.tournament_checkinend && Tournament.data.tournament.currentdatetime < Tournament.data.tournament.tournament_end) {
            $("#event-progress-bar .bar").css('width', '75%');
            $("#event-progress-bar .text").text('Status: Matches In Progress');
            $('#event-action-btn').text('View Brackets').click(function() { window.open('/brackets/'+Tournament.data.tournament.tournament_id); });
        } else if(Tournament.data.tournament.currentdatetime > Tournament.data.tournament.tournament_end) {
            $("#event-progress-bar").removeClass('progress-danger').addClass('progress-success');
            $("#event-progress-bar .bar").css('width', '100%');
            $("#event-progress-bar .text").text('Status: Tournament Completed');
            $('#event-action-btn').text('View Brackets').click(function() { window.open('/brackets/'+Tournament.data.tournament.tournament_id); });
        } else {
            $("#event-progress-bar .bar").css('width', '0%');
            $("#event-progress-bar .text").text('Coming Soon');
            $('#event-action-btn').text('Closed');
        }
    },
// ***************************************
// Update Registration button (1 - Open, 2 - Closed, 0 - Limited)
// *************************************** 
    setRegistrationType: function(tournament_registration_type) {
        if (Tournament.data.tournament.currentdatetime > Tournament.data.tournament.tournament_registrationstart && Tournament.data.tournament.currentdatetime < Tournament.data.tournament.tournament_registrationend) {
            $("#event-progress-bar .bar").css('width', '25%');
            $("#event-progress-bar .text").text('Status: Registration Open');
            if (tournament_registration_type == "1") {
                $('#event-action-btn').text('Sign Up Now').click(function() { Tournament.navRegistration("Open"); });
            }
            if (tournament_registration_type == "0") {
                $('#event-action-btn').text('Limited').click(function() { Tournament.navRegistration("Limited"); });
            }
            if (tournament_registration_type == "2") {
                $('#event-action-btn').text('Closed').click(function() { Tournament.navRegistration("Closed"); });
            }
        } else if (Tournament.data.tournament.currentdatetime > Tournament.data.tournament.tournament_checkinstart && Tournament.data.tournament.currentdatetime < Tournament.data.tournament.tournament_checkinend) {
            $("#event-progress-bar .bar").css('width', '50%');
            $("#event-progress-bar .text").text('Status: Check-In Open');
            $('#event-action-btn').text('Check In').click(function() { Tournament.navCheckin(); });
            
            $('#calltoaction').append('<div class="button"><a onclick="Tournament.navCheckin()" class="btn btn-large btn-action">Check In</a></div>');
        } else {
            $("#event-progress-bar .bar").css('width', '2%');
            $("#event-progress-bar .text").text('Coming Soon');
            $('#event-action-btn').text('Closed');
        }
    },
// ***************************************
// Show or Hide the activity progress bar
// ***************************************   
    activity: function(active) {
        if (active) {
            $('#tournament_content').hide();
            $('#tournament_activity').show();     
        } else {
           $('#tournament_content').fadeIn();
            $('#tournament_activity').hide();      
        }
    },
// ***************************************
// Show Tournament Details
// ***************************************   
    navAbout: function(e) {
        $('.nav li').removeClass('active'); $(e).parent().addClass('active');
        Tournament.activity(true);
            response = Tournament.data;
            console.log(response);  
            $.ajax({ url: "/ajax/public/tournament_about.php?tournament_id=" + Tournament.tournament_id }).done(function(response) {
                //var box = '<div class="box"><h1>'+response.tournament.tournament_name+' '+(new Date(response.tournament.currentdatetime * 1000)).toLocaleString()+'</h1><div class="inner_box" style="margin-top:14px;"><i class="icon-calendar"></i> <strong>Start: </strong>'+(new Date(response.tournament.tournament_start * 1000)).toLocaleString()+' <i class="icon-paste" style="margin-left:20px;"></i> <strong>Registration: </strong>'+(new Date(response.tournament.tournament_registrationstart * 1000)).toLocaleString()+'<i class="icon-signin" style="margin-left:20px;"></i> <strong>Check In: </strong>'+(new Date(response.tournament.tournament_checkinstart * 1000)).toLocaleString()+'</div><div class="inner_box" style="margin-top:14px;">'+response.tournament.tournament_details+'</div><div class="inner_box" style="margin-top:14px;">'+response.tournament.tournament_rules+'</div></div>';
                Tournament.setPageContent(response);
                Tournament.activity(false);
            });
    },
// ***************************************
// Show Tournament Schedule
// ***************************************   
    navSchedule: function(e) {
        $('.nav li').removeClass('active'); $(e).parent().addClass('active');
        Tournament.activity(true);
            response = Tournament.data;
            console.log(response); 
            var box = '<table class="table table-stats table-condensed table-bordered table-striped table-sortable"><thead><tr><th colspan="5">Event Schedule</th></tr><tr><th>Round</th><th>Match Times</th><th>Format</th><th>Maps</th></tr></thead>';
            $.ajax({ url: "/ajax/admin/tournaments.php?action=getSchedule&tournament_id="+Tournament.tournament_id }).done(function(response) { 
                if (response.schedule) {
                    $(response.schedule.rounds).each(function(i, d) {
                        if (!d.startdatetime) { d.startdatetime = "Undetermined "; }
                        if (!d.format) { d.format = "Undetermined "; }
                        if (!d.map_pool) { d.map_pool = "Undetermined "; }
                        box += '<tr><td>'+d.name+'</td><td><span class="localTime">'+d.startdatetime+'</span></td><td>'+d.format+'</td><td>'+d.map_pool+'</td></tr>';
                    });
                }
                box += '</table>';
                Tournament.setPageContent(box);
                Tournament.activity(false);
            });
    },
// ***************************************
// Show Tournament Admins
// ***************************************   
    navAdmins: function(e) {
        $('.nav li').removeClass('active'); $(e).parent().addClass('active');
        Tournament.activity(true);
            response = Tournament.data;
            console.log(response);  
            $.ajax({ url: "/ajax/public/tournament_admins.php?tournament_id=" + Tournament.tournament_id }).done(function(response) {
                //var box = '<div class="box"><h1>'+response.tournament.tournament_name+' '+(new Date(response.tournament.currentdatetime * 1000)).toLocaleString()+'</h1><div class="inner_box" style="margin-top:14px;"><i class="icon-calendar"></i> <strong>Start: </strong>'+(new Date(response.tournament.tournament_start * 1000)).toLocaleString()+' <i class="icon-paste" style="margin-left:20px;"></i> <strong>Registration: </strong>'+(new Date(response.tournament.tournament_registrationstart * 1000)).toLocaleString()+'<i class="icon-signin" style="margin-left:20px;"></i> <strong>Check In: </strong>'+(new Date(response.tournament.tournament_checkinstart * 1000)).toLocaleString()+'</div><div class="inner_box" style="margin-top:14px;">'+response.tournament.tournament_details+'</div><div class="inner_box" style="margin-top:14px;">'+response.tournament.tournament_rules+'</div></div>';
                Tournament.setPageContent(response);
                Tournament.activity(false);
            });
    },
// ***************************************
// Show Tournament Matches
// ***************************************   
    navBrackets: function(e) {
        $('.nav li').removeClass('active'); $(e).parent().addClass('active');
        Tournament.activity(true);
            response = Tournament.data;
            console.log(response);  
            $.ajax({ url: "/ajax/public/tournament_brackets.php?tournament_id=" + Tournament.tournament_id }).done(function(response) {
                //var box = '<div class="box"><h1>'+response.tournament.tournament_name+' '+(new Date(response.tournament.currentdatetime * 1000)).toLocaleString()+'</h1><div class="inner_box" style="margin-top:14px;"><i class="icon-calendar"></i> <strong>Start: </strong>'+(new Date(response.tournament.tournament_start * 1000)).toLocaleString()+' <i class="icon-paste" style="margin-left:20px;"></i> <strong>Registration: </strong>'+(new Date(response.tournament.tournament_registrationstart * 1000)).toLocaleString()+'<i class="icon-signin" style="margin-left:20px;"></i> <strong>Check In: </strong>'+(new Date(response.tournament.tournament_checkinstart * 1000)).toLocaleString()+'</div><div class="inner_box" style="margin-top:14px;">'+response.tournament.tournament_details+'</div><div class="inner_box" style="margin-top:14px;">'+response.tournament.tournament_rules+'</div></div>';
                Tournament.setPageContent(response);
                Tournament.activity(false);
            });
    },
// ***************************************
// Show Tournament Teams
// ***************************************   
    navTeams: function(e) {
        $('.nav li').removeClass('active'); $(e).parent().addClass('active');
        Tournament.activity(true);
        response = Tournament.data; 
            var box = '';
            box += '<table class="table table-stats table-condensed table-bordered table-striped table-sortable"><thead><tr><th colspan="4">Registered Participants</th></tr><tr><th style="width:96px;">Tag</th><th style="width:196px;">Name</th><th style="width:153px;">Captain</th><th style="width:134px;">Status</th></tr></thead>';
            $(response.tournament.tournament_teams).each(function(i, team) {
                   if (team.waitlist_flag != true) {
                   box += '<tr><td>'+team.team_tag+'</td><td>'+team.team_name+'</td><td><a href="#" class="tooltip-player">'+team.team_captain_name+'</a></td><td>';
                        if (team.tstatus == 1) {
                            box += '<span class="label label-success">Checked In</span>';
                        }
                   box += '</td></tr>';
                   }
            });
            box += '</table>';
            if (response.tournament.wait_list_size > 0) {
            box += '<table class="table table-stats table-condensed table-bordered table-striped table-sortable"><thead><tr><th colspan="4">Waitlisted</th></tr><tr><th style="width:96px;">Tag</th><th style="width:196px;">Name</th><th style="width:153px;">Captain</th><th style="width:134px;">RPI</th></tr></thead>';
            $(response.tournament.tournament_teams).each(function(i, team) {
                   if (team.waitlist_flag == true && team.checkedin_flag != true) {
                   box += '<tr><td>'+team.team_tag+'</td><td>'+team.team_name+'</td><td>'+team.team_captain_name+'</td><td>'+team.team_rpi+'</td></tr>';
                   }
            });
            box += '</table>';
            }
            Tournament.setPageContent(box);
            Tournament.activity(false);
    },
// ***************************************
// Show Tournament Standings
// ***************************************   
    navMatches: function(e) {
        $('.nav li').removeClass('active'); $(e).parent().addClass('active');
        Tournament.activity(true);
        response = Tournament.data
            console.log(response);     
            var box = '<div id="matchNavbar" class="nav-wrap" style="margin-bottom:0px; padding:2px 2px 2px 4px;">';
            $.ajax({ url: "/ajax/admin/tournaments.php?action=getMatches&tournament_id="+Tournament.tournament_id }).done(function(response) {
                console.log(response); 
                box += '<ul class="nav nav-tabs nav-pills">';
                $(response.matches).each(function(i, v) {
                    box += '<li><a onclick="Tournament.displayMatches('+v.id+')">'+v.name+'</a></li>';
                });
                box += '</ul>';
                box += '</div>';
                box += '<div id="bracketSystem"></div>';
                Tournament.setPageContent(box);
                Tournament.displayMatches(response.matches[0].id);
                Tournament.activity(false);
            });
    },
// ***************************************
// Show Tournament Standings FULLSCREEN
// ***************************************   
    navFullscreenStandings: function() {
        var standingsWindow = window.open('/brackets/'+Tournament.tournament_id, 'PlayIGL - Brackets', 'width='+$(window).width()+', height='+$(window).height());
    },
// ***************************************
// Show Open Registration
// ***************************************   
    navRegistration: function() {
        $('.nav li').removeClass('active');
        var eligible = false;
        Tournament.activity(true);
        $.ajax({ url: "/api/playerteams/" + Tournament.player_id }).done(function(response) {
            console.log(response);    
            var box = '<table class="table table-stats table-condensed table-bordered table-striped table-sortable"><thead><tr><th colspan="4">Eligible Teams</th></tr><tr><th>Name</th><th style="width:70px;"></th></tr></thead>';           
            $(response.teams).each(function (i, t) {
                //if (t.league != 0) {
                if (t.is_captain) {
                    eligible = true;
                    box += "<tr><td>"+t.team_name+"</td><td>";
                    if ($.inArray(t.team_id, Tournament.team_array) < 0) {
                        box += "<div onclick='Tournament.enrollTeam(\""+t.team_id+"\")' class='btn btn-action'>Enroll</div>";
                    } else {
                        box += "<div onclick='Tournament.withdrawTeam(\""+t.team_id+"\")' class='btn btn-red'>Withdraw</div>";
                    }
                    box += "</td>";
                }
                //}
            });
            box += '</table>';
            if (!eligible) {
                box += '<div class="alert alert-warning"><i class="icon-warning-sign"></i> You do not have any eligibe teams for this tournament.<a class="btn pull-right" style="margin-top:-5px; margin-right:-26px;" href="/dashboard/profile/">Create A Team</a></div>';
            }
            Tournament.setPageContent(box);
            Tournament.activity(false);  
        });
    },
// ***************************************
// Show Check In
// ***************************************   
    navCheckin: function() {
        $('.nav li').removeClass('active');
        var eligible = false;
        Tournament.activity(true);
            var box = '<table class="table table-stats table-condensed table-bordered table-striped table-sortable"><thead><tr><th colspan="4">Eligible Teams</th></tr><tr><th>Tag</th><th>Name</th><th style="width:70px;"></th></tr></thead>';           
            $(Tournament.data.tournament.tournament_teams).each(function (i, t) {
                   console.log("TEAM:"+t);
                //if (t.league != 0) {
                if (Tournament.data.tournament.active_session) {
                //if (t.league == Tournament.data.tournament.tournament_division) {
                if (t.team_captain_id == Tournament.data.tournament.active_session || t.team_alternate_id == Tournament.data.tournament.active_session) {
                    eligible = true;
                    box += "<tr><td>"+t.team_tag+"</td><td>"+t.team_name+"</td><td>";
                    if (t.checkedin_flag != 1) {
                        box += "<div onclick='Tournament.checkIn(\""+t.team_id+"\")' class='btn btn-action'>Check In</div>";
                    } else {
                        box += "<span class='label label-success'><i class='icon-ok-sign'></i> Checked In</span>";
                    }
                    box += "</td>";
                }
                //}
                }
                //}
            });
            box += '</table>';
            if (!eligible) {
                box += '<div class="alert alert-warning"><i class="icon-warning-sign"></i> You are not a registered participant of this tournament.</div>';
            }
            Tournament.setPageContent(box);
            Tournament.activity(false);  
    },
// ***************************************
// Enroll a Team
// ***************************************   
    enrollTeam: function(team_id, regType) {
        Tournament.activity(true);
        $.ajax({ url: "/ajax/admin/tournaments.php?action=enrollTeam&tournament_id="+Tournament.tournament_id+"&team_id="+team_id }).done(function(response) {
            console.log(response); 
            Tournament.reload(Tournament.navRegistration);
           //var box = "<p>None of your teams are eligible for this tournament.</p>";
            //Tournament.setPageContent(box);
        });
    },
// ***************************************
// Enroll a Team
// ***************************************   
    withdrawTeam: function(team_id, regType) {
        Tournament.activity(true);
        $.ajax({ url: "/ajax/admin/tournaments.php?action=withdrawTeam&tournament_id="+Tournament.tournament_id+"&team_id="+team_id }).done(function(response) {
            console.log(response);  
            Tournament.reload(Tournament.navRegistration);
           //var box = "<p>None of your teams are eligible for this tournament.</p>";
            //Tournament.setPageContent(box);
        });
    },
// ***************************************
// Check In
// ***************************************   
    checkIn: function(team_id, regType) {
        Tournament.activity(true);
        $.ajax({ url: "/ajax/admin/tournaments.php?action=checkIn&tournament_id="+Tournament.tournament_id+"&team_id="+team_id }).done(function(response) {
            console.log(response); 
            Tournament.reload(Tournament.navCheckin);
           //var box = "<p>None of your teams are eligible for this tournament.</p>";
            //Tournament.setPageContent(box);
        });
    },
// ***************************************
// Get Match Graphic
// ***************************************   
    displayMatches: function(round_id) {
        $('#bracketSystem').html('');
        $.ajax({ url: "/ajax/admin/tournaments.php?action=getMatches&tournament_id="+Tournament.tournament_id }).done(function(response) {
            console.log(response); 
            $(response.matches).each(function(i, v) {
                if (v.id == round_id) {
                    var bg = 0;
                    var bg_color = 'eee';
                    $(v.matches).each(function(ii, vv) {
                        if (bg % 2 == 0) {bg_color = 'grey';} else {bg_color = '';   }
                        $('#bracketSystem').append('<div class="match_row '+bg_color+'"><div class="matchbox"><div class="team team1"><div class="score">'+vv.teams[0].score+'</div><img src="/img'+vv.teams[0].logo+'" /><h4>'+vv.teams[0].name+'</h4><span>'+vv.teams[0].captain.username+'</span></div><div class="team team2"><div class="score">'+vv.teams[1].score+'</div><img src="/img'+vv.teams[1].logo+'" /><h4>'+vv.teams[1].name+'</h4><span>'+vv.teams[1].captain.username+'</span></div></div><div class="match_details"><span class="localTime">'+vv.match_time+'</span></div></div>');
                        bg ++;
                    });
                }
            });
            $('#bracketSystem').append('<div style="clear:both;"></div>');
            PlayIGL.viewChange();
        });
    }
}


// ***************************************
// Tournament Admin Class
// Copyright PlayIGL.com
// Author: Kevin Langlois
// ***************************************


Tournament.admin = {
// ***************************************
// Load Initial Data Teams
// ***************************************
    initTeams: function(id) { 

        Tournament.tournament_id = id;
        Tournament.activity(true);
        $.ajax({ url: "../api/tournament.php?id="+Tournament.tournament_id }).done(function(response) {
            console.log(response);
            Tournament.data = response;
            Tournament.setTeamArray(response.tournament.tournament_teams);
            Tournament.activity(false);
            Tournament.admin.manageTeams(id);
        });
    },
// ***************************************
// Load Initial Data Schedules
// ***************************************
    initSchedule: function(id) { 

        Tournament.tournament_id = id;
        Tournament.activity(true);
        $.ajax({ url: "../api/tournament.php?id="+Tournament.tournament_id }).done(function(response) {
            console.log(response);
            Tournament.data = response;
            Tournament.setTeamArray(response.tournament.tournament_teams);
            Tournament.activity(false);
            Tournament.admin.manageSchedule(id);
        });
    },
// ***************************************
// Load Initial Data Rounds
// ***************************************
    initMatches: function(id) { 

        Tournament.tournament_id = id;
        Tournament.activity(true);
        $.ajax({ url: "../api/tournament.php?id="+Tournament.tournament_id }).done(function(response) {
            console.log(response);
            Tournament.data = response;
            Tournament.setTeamArray(response.tournament.tournament_teams);
            Tournament.activity(false);
            Tournament.admin.manageMatches(id);
        });
    },
// ***************************************
// Manage Matches
// ***************************************
    manageMatches: function(tournament_id) {
        Tournament.activity(true);
        $.ajax({ url: "../ajax/admin/tournaments.php?action=getMatches&tournament_id="+Tournament.tournament_id }).done(function(response) {
            console.log(response);
            var schedule = false;
            var html = '';
            $(response.matches).each(function(i,v) {
                console.log(v);
                schedule = true;
                html += '<table class="table table-stats table-condensed table-bordered table-striped table-sortable"><thead><tr><th colspan="4">'+v.name+'</th></tr><tr><th>Match Scheduling Id</th><th>Team 1</th><th>team 2</th><th>Match Time</th><th>Status</th><th>Action</th></tr></thead>';
                $(v.matches).each(function(i, v) {
                    if (v.status == 0) { v.status = '<span class="label label-warning">Incomplete</span>'; } else { v.status = '<span class="label label-success">Completed</span>'; }
                   html += '<tr><td>'+v.id+'</td><td>'+v.teams[0].name+'</td><td>'+v.teams[1].name+'</td><td>'+v.match_time+'</td><td>'+v.status+'</th><th><div class="btn btn-mini" onclick="Tournament.admin.editMatch('+v.id+')"><i class="icon-pencil"></i> Edit</div></th></tr>';
            });
                html += '</table>';
            });
            if (!schedule) {
                html = '<div class="alert"><strong>No Schedule Available!</strong> A schedule has not been generated for this tournament. </div>';
            }
            Tournament.setPageContent(html);
            Tournament.activity(false);
        });
    },
// ***************************************
// Edit Schedule
// ***************************************
    editSchedule: function(round_id) {
        if (round_id != null) {
            Tournament.activity(true);
            $.ajax({ url: "../ajax/admin/tournaments.php?action=getRound&round_id="+round_id }).done(function(response) {
                var box = '<div class="box"><h1>Edit Round</h1><div class="inner_box" style="margin-top:14px;"><form id="editRoundForm">';
                box += '<label>Round Name</label><input id="name" name="name" value="'+response.schedule.round.name+'" />';
                box += '<label>Start Date Time</label><input id="startdatetime" name="startdatetime" value="'+response.schedule.round.startdatetime+'" />';
                box += '<label>Format</label><select id="format" name="format"></select>';
                box += '<label>Map Pool</label><select id="map_pool" name="map_pool"></select>';
                box += '</select><div class="btn pull-right" onclick="Tournament.admin.saveSchedule(\''+response.schedule.round.id+'\')"><i class="icon-disk"/> Save</div></form></div></div>'; 
                Tournament.setPageContent(box);
                Tournament.activity(false);
            });
        } else {
             Tournament.setPageContent('');
        }
    },
         
 // ***************************************
// Save Schedule
// ***************************************
    saveSchedule: function(round_id) {
        Tournament.activity(true);
        $.ajax({ url: "../ajax/admin/tournaments.php?action=updateRound&round_id="+round_id, data: $('#editRoundForm').serializeArray()  }).done(function(response) {
            console.log(response);
            Tournament.activity(false);
            Tournament.admin.manageSchedule(tournament_id);
        });
    },
// ***************************************
// Generate Match
// ***************************************
    generateMatches: function() {
        Tournament.activity(true);
        $.ajax({ url: "../ajax/admin/tournaments.php?action=generateMatches&tournament_id="+Tournament.tournament_id  }).done(function(response) {
            console.log(response);
            Tournament.activity(false);
            Tournament.admin.manageMatches(tournament_id);
        });
    },
            
// ***************************************
// Edit Match
// ***************************************
    editMatch: function(match_id) {
        if (match_id != null) {
            Tournament.activity(true);
            $.ajax({ url: "../ajax/admin/tournaments.php?action=getMatch&match_id="+match_id }).done(function(response) {
                var box = '<div class="box"><h1>Edit Match</h1><div class="inner_box" style="margin-top:14px;"><form id="editMatch" name="editMatch">';
                box += '<label>Match Time</label><input id="match_time" name="match_time" value="'+response.match.match_time+'" />';
                box += '<label>Match Team1</label><input id="team1_id" name="team1_id" value="'+response.match.team1_id+'" />';
                box += '<label>Match Team2</label><input id="team2_id" name="team2_id" value="'+response.match.team2_id+'" />';
                box += '<label>Match Status</label><select id="status" name="status"><option value="0">Incomplete</option><option value="1">Completed</option></select>';
                box += '</form><div class="btn pull-right" onclick="Tournament.admin.saveMatch('+match_id+')"><i class="icon-disk"/> Save</div></div></div>'; 
                Tournament.setPageContent(box);
                $('#status').val(response.match.status);
                Tournament.activity(false);
            });
        } else {
             Tournament.setPageContent('');
        }
    },
// ***************************************
// Save Match
// ***************************************
    saveMatch: function(match_id) {
        Tournament.activity(true);
        $.ajax({ url: "../ajax/admin/tournaments.php?action=updateMatch&match_id="+match_id, data: $('#editMatch').serializeArray()  }).done(function(response) {
            console.log(response);
            Tournament.activity(false);
            Tournament.admin.manageMatches(tournament_id);
        });
    },
// ***************************************
// Delete Round
// ***************************************
    deleteMatch: function(tournament_id) {
        if ($('#round_id').val() == $('select option:last').val()) {
            Tournament.activity(true);
            $.ajax({ url: "../ajax/admin/tournaments.php?action=deleteMatch&tournament_id="+Tournament.tournament_id }).done(function(response) {
                console.log(response);
                    Tournament.activity(false);
            Tournament.admin.manageRounds(tournament_id);
            });
        } else {
            alert('You can only remove the last generated round.')
        }
    },
// ***************************************
// Manage Teams
// ***************************************
    manageTeams: function(tournament_id) {
        Tournament.activity(true);
        response = Tournament.data; 
            var box = '';
            box += '<table class="table table-stats table-condensed table-bordered table-striped table-sortable"><thead><tr><th colspan="4">Registered Teams<div class="btn btn-mini pull-right" onclick="Tournament.admin.selectTeam()"><i class="icon-plus" /> Add</div></th></tr><tr><th>Tag</th><th>Name</th><th>Captain</th><th width="16">Action</th></tr></thead>';
            $(response.tournament.tournament_teams).each(function(i, team) {
                    console.log(team);
                   box += '<tr><td>'+team.team_tag+'</td><td>'+team.team_name+'</td><td>'+team.team_captain_name+'</td><td><div class="btn btn-mini" onclick="Tournament.admin.withdrawTeam(\''+team.team_id+'\')"><i class="icon-remove" /> Drop</div></td></tr>';
            });
            box += '</table>';
            Tournament.setPageContent(box);
            Tournament.activity(false);
    },
// ***************************************
// Select a Team
// ***************************************   
    selectTeam: function() {
        Tournament.activity(true);
        $.ajax({ url: "../api/game/"+Tournament.data.tournament.tournament_game_id+"/teams" }).done(function(response) {
            console.log(response);  
            var box = '<div class="box"><h1>Add Team</h1><div class="inner_box" style="margin-top:14px;"><label>Select Team</label><select id="selectTeamDropdown">';
            $(response.teams).each(function(i, t) {
                box += '<option value="'+t.id+'">'+t.name+'</option>';
            });
            box += '</select><div class="btn pull-right" onclick="Tournament.admin.enrollTeam($(\'#selectTeamDropdown\').val())"><i class="icon-plus"/> Add</div></div></div>'; 
            Tournament.setPageContent(box);
            Tournament.activity(false);
        });   
    },
// ***************************************
// Enroll a Team
// ***************************************   
    enrollTeam: function(team_id) {
        Tournament.activity(true);
        $.ajax({ url: "/ajax/admin/tournaments.php?action=enrollTeam&tournament_id="+Tournament.tournament_id+"&team_id="+team_id }).done(function(response) {
            console.log(response);  
            Tournament.admin.initTeams(Tournament.tournament_id);
            Tournament.activity(false);
        });
    },
// ***************************************
// Withdraw a Team
// ***************************************   
    withdrawTeam: function(team_id) {
        Tournament.activity(true);
         $.ajax({ url: "/ajax/admin/tournaments.php?action=withdrawTeam&tournament_id="+Tournament.tournament_id+"&team_id="+team_id }).done(function(response) {
           console.log(response);  
            Tournament.admin.initTeams(Tournament.tournament_id);
            Tournament.activity(false);
        });
    },
// ***************************************
// Manage Schedule
// ***************************************
    manageSchedule: function(tournament_id) {
        Tournament.activity(true);
        var box = '';
        box += '<table class="table table-stats table-condensed table-bordered table-striped table-sortable"><thead><tr><th colspan="4">Tournament Schedule</th></tr><tr><th>Round</th><th>Teams</th><th>Match Times</th><th>Format</th><th>Map Pool</th><th width="16">Action</th></tr></thead>';    
        $.ajax({ url: "/ajax/admin/tournaments.php?action=getSchedule&tournament_id="+Tournament.tournament_id }).done(function(response) { 
            if (response.schedule) {
                $(response.schedule.rounds).each(function(i, d) {
                   box += '<tr><td>'+d.name+'</td><td>'+d.team_count+'</td><td>'+d.startdatetime+'</td><td>'+d.format+'</td><td>'+d.map_pool+'</td><td><div class="btn btn-mini" onclick="Tournament.admin.editSchedule(\''+d.id+'\')"><i class="icon-pencil" /> Edit</div></td></tr>';
                });
            }
            box += '</table>';
            Tournament.setPageContent(box);
            Tournament.activity(false);
        });
    }
}
