function streamStatus(){
    $.getJSON('/api/stream.php', function(data) {
        if(data == null)
        {
            $('#twitchStatus').removeClass('badge-success');
            $('#twitchStatus').text('Offline').removeAttr('data-original-title');
            $('#twitchStatus').show();
        }
        else
        {
            if(data.live == 1)
            {
                $('#twitchStatus').addClass('badge-success');
                $('#twitchStatus').html('Live').attr('data-original-title', data.status );
                $('#twitchStatus').show();
            }
            else
            {
                $('#twitchStatus').removeClass('badge-success');
                $('#twitchStatus').text('Offline').removeAttr('data-original-title');
                $('#twitchStatus').show();
            }
        } 
        });
        setTimeout(streamStatus, 120000);
    }

$(function() {
        $('.accordion').on('show hide', function(e) {
            $(e.target).siblings(".accordion-heading").find("i").toggleClass('icon-caret-down icon-caret-up');
        });
        
    });