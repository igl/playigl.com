Brackets = {
    teams: ['team1', 'team2', 'team3', 'team4', 'team5', 'team6', 'team7', 'team8', 'team9', 'team10', 'team11', 'team12', 'team13', 'team14', 'team15', 'team16','team1', 'team2', 'team3', 'team4', 'team5', 'team6', 'team7', 'team8', 'team9', 'team10', 'team11', 'team12', 'team13', 'team14', 'team15', 'team16'],
    results: [
        [[1,2], [2,1], [1,1], [0,0],[1,2], [2,1], [1,1], [0,0],[1,2], [2,1], [1,1], [0,0],[1,2], [2,1], [1,1], [0,0]],
        [[1,2], [2,1], [1,1], [0,0],[1,2], [2,1], [1,1], [0,0]],
        [[1,1], [3,4],[1,1], [3,4]],
        [[1,0],[1,0]],
        [[1,0]]
      ],
    rounds:0,
    create: function() {
        $('body').append('<table id="bracketTable"></table>');
        Brackets.rounds = $(Brackets.results).length,
        console.log(Brackets.rounds);
        for(var y=0;y<($(Brackets.teams).length*2);y++) {
            var column = "";
            column += '<tr>';
            for(var x=0;x<Brackets.rounds;x++) {
                column += '<td></td>';
            }
            column += '</tr>';
            $('#bracketTable').append(column)
        }
        var r = 0; var t = 0;
        while(r<Brackets.rounds) {
            var rslt = 0;
            var row = Math.pow(2, r+1)-2;
            while(rslt<$(Brackets.results[r]).length) {
                $('#bracketTable tr:eq('+row+') td:eq('+r+')').html('<div class="team">'+Brackets.results[r][rslt][0]+'</div>');
                row++;
                $('#bracketTable tr:eq('+row+') td:eq('+r+')').html('<div class="team">'+Brackets.results[r][rslt][1]+'</div>');
                row++;
                for(var i=1;i<=Math.pow(2, r+2)-2;i++) {
                    row++;
                }
                rslt++;
            }
            r++;
        }
    },
    create2: function() {
        var vIndex = 0;
        var grid = new Array();
        $('.roundColumn').each(function(c, cElm) {
            grid[c] = new Array();
            $(cElm).children().each( function(g, gElm) {
                var y1 = $(gElm).offset().top;
                grid[c][g] = y1;
            });
        });
        console.log(grid);
        for(var y=1;y<$(grid).length;y++) {
            for (var x=0;x<$(grid[y]).length;x++) {
                grid[y][x] = grid[y][x] + (grid[y-1][x+1] - grid[y-1][x]);
                alert(grid[y][x]);
            }
        }
        console.log(grid);
    }
};
