/* 
 * These elements are used to follow teams & players
 */

//Grab the follower data
function viewFollowers() {
    //Variables needed
    subject_type = $('#page_name').val();
    subject_id = $('#page_id').val();
    
    viewFollowers(subject_type, subject_id);
}

//Grab the follower data
function viewFollowers(subject_type, subject_id) {
    //actor_id = $('#viewer_id').val();
    if(subject_id > 0 && subject_type != '')
    {
        $('#ModalBody').html('<div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div>');
        $('#ModalLabel').text('FOLLOWERS');
        $('#ModalButton').hide();
        $('#Modal').modal('show');

        jqxhr = $.post("/followers/" + subject_type + '/' + subject_id, "json")
        .done(function(data) {
            if(data.success) {
                $('#ModalBody').html('<table id="follow_list" class="table table-striped table-condensed"><tbody></tbody></table>');
                appendrow ='';
                $.each(data.followers,function(key,value){
                    $('#follow_list tbody').append('<tr><td width="460px"><div><img alt="Avatar" class="img-rounded pull-left smallthumb" style="margin-right: 6px;" src="/img' + value.player_avatar + '"><h6> <img alt="' + value.player_country + '" src="/img/flags/' + value.player_country + '"> <a href="/player/' + value.player_username + '">' + value.player_firstname + ' ' + value.player_username + ' ' + value.player_lastname + '</a></h6></div></td><td>&nbsp;</td></tr>');
                    /*if(actor_id == value.player_id)
                        $('#follow_list tbody').append('<tr><td width="460px"><div><img alt="Avatar" class="img-rounded pull-left smallthumb" style="margin-right: 6px;" src="/img' + value.player_avatar + '"><h6> <img alt="' + value.player_country + '" src="/img/flags/' + value.player_country + '"> <a href="/player/' + value.player_id + '-' + value.player_username + '">' + value.player_firstname + ' ' + value.player_username + ' ' + value.player_lastname + '</a></h6></div></td><td>&nbsp;</td></tr>');
                    else
                    {
                        if(value.isFollowing == true)
                            $('#follow_list tbody').append('<tr><td width="460px"><div><img alt="Avatar" class="img-rounded pull-left smallthumb" style="margin-right: 6px;" src="/img' + value.player_avatar + '"><h6> <img alt="' + value.player_country + '" src="/img/flags/' + value.player_country + '"> <a href="/player/' + value.player_id + '-' + value.player_username + '">' + value.player_firstname + ' ' + value.player_username + ' ' + value.player_lastname + '</a></h6></div></td><td><button id="follow_player_' + value.player_id + '" type="button" class="btn btn-info btn-follow"><i class="icon-check icon-large icon-white"></i> Following</button></td></tr>');
                        else
                            $('#follow_list tbody').append('<tr><td width="460px"><div><img alt="Avatar" class="img-rounded pull-left smallthumb" style="margin-right: 6px;" src="/img' + value.player_avatar + '"><h6> <img alt="' + value.player_country + '" src="/img/flags/' + value.player_country + '"> <a href="/player/' + value.player_id + '-' + value.player_username + '">' + value.player_firstname + ' ' + value.player_username + ' ' + value.player_lastname + '</a></h6></div></td><td><button id="follow_player_' + value.player_id + '" type="button" class="btn btn-info btn-follow"><i class="icon-check-empty icon-large icon-white"></i> Follow</button></td></tr>');
                    }       */             
                });

                $('#thisFollowers').append(appendrow);
            } else {
                $('#Modal').modal('hide');
                    alert(data.error);
            }
        });
    }
}

//Follow logic
function initFollow(){
    //Variables needed
    subject_type = $('#page_name').val();
    subject_id = $('#page_id').val();

    //AJAX
    if(subject_id > 0 && subject_type != '')
    {   
        jqxhr = $.post("/follow/check/" + subject_type + '/' + subject_id, "json")
        .done(function(data) {
            if(data.success) {
                check = "icon-check";
                txt = "Following";
            } else {
                check = "icon-check-empty";
                txt = "Follow";
            }

            //Create the button
            $('#follow_button_' + subject_id).html('<button id="follow_' + subject_type + '_' + subject_id + '" type="button" class="btn btn-info btn-follow"><i class="' + check + ' icon-large icon-white"></i> ' + txt + '</button>');
        })
        .always(function() {
            //Save the button reference
            $btn = $('#follow_'+subject_type+'_'+subject_id);

            $btn.click(function() {
                //Save the DOM reference
                btn = this;

                //Prevent button spam, and animate
                btn.disabled = true;
                anim = $btn.find("i");
                anim.attr("class", "icon-refresh icon-spin icon-large icon-white");

                //AJAX
                jqxhr = $.post("/follow/" + subject_type + '/' + subject_id, "json")
                .done(function(data) {
                    if(data.success) {
                        if(data.following) {
                            btn.disabled = false;
                            $btn.html('<i class="icon-check icon-large icon-white"></i> Following');
                            if (subject_type == 'player') {
                                PlayIGL.Analytics.track('follow_player', {actor: data.actor, actor_username: data.actor_username, subject: data.subject, subject_username:data.subject_username, subject_email:data.subject_email});
                            } else {
                                PlayIGL.Analytics.track('follow_team', {actor: data.actor, actor_username: data.actor_username, subject: data.subject, subject_username:data.subject_username, subject_email:data.subject_email});
                            }
                        } else {
                            btn.disabled = false;
                            $btn.html('<i class="icon-check-empty icon-large icon-white"></i> Follow');
                        }
                    } else {
                        btn.disabled = false;
                        $btn.html('<i class="icon-check-empty icon-large icon-white"></i> Follow');
                    }
                });
            });
        });
    }
}