/* 
  *
 * @author Darryl Allen
 * @copyright Copyright 2012 PlayIGL.com
 */
//incompleted... may not be needed
function server_reserve(game, date) {
    $.getJSON('/server/reserve/' + game + '/' + date, function(data) {
        if (data.success == undefined)
        {
            alert('Sorry, there was a problem with this request. Please try again.');
        }
        else
        {
            if (data.success)
            {
                window.location.href = "blah.php?name=" + data.server_id;
            }
            else
            {
                if (data.error == undefined)
                    alert('Unknown error while trying to reserve server');
                else
                    alert(data.error);
            }
        }
    });
}