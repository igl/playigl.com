/* 
 * These elements are used exclusively on the landing page
 */


function games_list()
{
        $.getJSON('/api/game/', function(data) {
        if(data.success)
        {
            game = '<div class="row">';
            i=1;
            $.each(data.game, function(key,value){
                if( i%4 == 0 )
                {
                    game += '<div class="row">';
                }
                
                game += '<div class="span3">' +
                        '<a href="#" class="game">' +
                        '<img class="game-img" src="/img'+value['game_tile']+'" />' +
                        '<h4>'+value['game_name']+'</h4>' +
                        '<span class="players">'+value['game_num_players']+' players</span>' +
                        '</a>' +
                        '</div>';
                if( i%4 == 0 )
                {
                    game += '</div>';
                }    
                    
                i++;
            })
            game += '</div>';
            
            $('#num_teams').html(data['stats']['num_teams']);
            $('#num_games').html(data['stats']['num_games']);
            //$('#num_divisions').html(data['stats']['num_divisions']);
            
            $('.games-list').append(game);
            
            $({countNum: 0}).animate({countNum: data['stats']['num_users']}, {
            duration: 2000,
            easing:'linear',
            step: function() {
              $('#num_players').html(Math.floor(this.countNum));
            },
            complete: function() {
              console.log('finished');
            }
            });
        }
        });
}


function teams_list()
{
        $.getJSON('/api/featuredteams/', function(data) {
        if(data.success)
        {
            team =  '<div class="row">' +
                    '<div class="span12"><a name="games"></a><h3>Participating at IGL</h3></div>' +
                    '</div><div class="row">';
            i=1;
            $.each(data.teams, function(key,value){
                if( i%6 == 0 )
                {
                    team += '<div class="row">';
                }
                
                team += '<div class="span2">' +
                        '<a href="/team/'+value['team_id']+'/'+value['team_name']+'" class="team tooltip-on" title="'+value['team_name']+'"><img class="game-img" src="/img'+value['team_logo']+'" /></a>' +
                        '</div>';
                if( i%6 == 0 )
                {
                    team += '</div>';
                }    
                    
                i++;
            })
            team += '</div>';
            
            $('.teams-list').append(team);
            $('.tooltip-on').tooltip();
        }
        });
        
}