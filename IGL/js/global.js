/* 
 * These are functions used on more than 90% of all pages
 */

var PUSHER_API_KEY = 'c13a7a22e9ba95b8fdf8';
var _ufq = _ufq || [];

$('body').tooltip({
  html:true,
  selector: '.tooltip-on'
});

$('body').popover({
  html:true,
  selector: '.popover-on',
  trigger: 'hover',
});

function initPusher() {
    var pusher = new Pusher(PUSHER_API_KEY);
    var channels = [];
    
    //AJAX
    var jqxhr = $.post("/ajax/dashboard/channels.php", "json")
    .done(function(data) {
        if(data.success) {
            $.each(data.channels, function(key,value) {
                channels.push(value);
            });
        }
    })
    .fail(function() {
        channels.push('global');
    })
    .always(function() {
        var notifier = new PusherNotifier(pusher.subscribe(channels[0]), { title: 'From PlayIGL', image: 'img/gritter/global.png' });
        for (var i = 1; i < channels.length; i++)
            var notifier = new PusherNotifier(pusher.subscribe(channels[i]));
    });
}

function initNav() {
    $('.accordion').on('show hide', function(e) {
        $(e.target).siblings(".accordion-heading").find("i").toggleClass('icon-caret-down icon-caret-up');
    });
    $('#accordion2').siblings('.accordion-toggle').slideUp();
}

function initStream() {
    $twitch_status = $("#twitchStatus");
    
    if (!$twitch_status.length)
        return false;
    
    $.getJSON('/api/stream.php', function(data) {
        if(!data.sucess)
        {
            $twitch_status.removeClass('badge-success');
            $twitch_status.text('Offline').removeAttr('data-original-title');
            $twitch_status.show();
        }
        else
        {
            if(data.stream.live == 1)
            {
                $twitch_status.addClass('badge-success');
                $twitch_status.html('Live').attr('data-original-title', data.status );
                $twitch_status.show();
            }
            else
            {
                $twitch_status.removeClass('badge-success');
                $twitch_status.text('Offline').removeAttr('data-original-title');
                $twitch_status.show();
            }
        } 
    });
    setTimeout(initStream, 120000);
}

function initSearch() {
    $(".icon").click(function() {
	var icon = $(this),
			input = icon.parent().find("#search"),
			submit = icon.parent().find(".submit"),
			is_submit_clicked = false;
	
	// Animate the input field
	input.animate({
		"width": "230px",
		"padding": "10px",
		"opacity": 1
	}, 300, function() {
		input.focus();
	});
	
	submit.mousedown(function() {
		is_submit_clicked = true;
	});
	
	// Now, we need to hide the icon too
	icon.fadeOut(300);
	
	// Looks great, but what about hiding the input when it loses focus and doesnt contain any value? Lets do that too
	input.blur(function() {
		if(!input.val() && !is_submit_clicked) {
			input.animate({
				"width": "0",
				"padding": "0",
				"opacity": 0
			}, 200);
			
			// Get the icon back
			icon.fadeIn(200);
		};
	});
});

    $('header [name=q]').liveSearch({url: '/api/search.php?q='});
}

/*
function initGoogle() {
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-33973481-1']);
  _gaq.push(['_setDomainName', 'playigl.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
}
*/

function initUserVoice() {
  var uv = document.createElement('script'); uv.type = 'text/javascript'; uv.async = true;
  uv.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'widget.uservoice.com/F1CJ1oW4vQLhfAn0KTOOg.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(uv, s);
}

function videoPopout ()
{
         window.open('igltv.php', 'IGL Livestream', 'width=' + screen.availWidth + ', height=' + screen.availHeight + ', status=no, toolbar=no, menubar=no');
}


function showLocalDate(timestamp)
{
    var mmToMonth = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
    var dt = new Date(timestamp * 1000);
    var mm = mmToMonth[dt.getMonth()];
    minutes = dt.getMinutes();
    if(minutes < 10)
        minutes = '0' + minutes;

    if(dt.getHours() > 11)
    {
        hour = dt.getHours() - 12;
        t = 'pm'
    }
    else
    {
        hour = dt.getHours();
        t = 'am'
    }
    return  mm + " " + dt.getDate() + "," + " " + hour + ":" + minutes + t;
}

function localTimezone()
{
    var cur_date = new Date();
    var offset = cur_date.getTimezoneOffset()*-60 / 3600;
    if(offset >= 0)
    {
        offset = Math.abs(offset);
        offset = '+' + offset;
    }
    return offset;
}

// PlayIGL Global Function Class
var PlayIGL = {
    viewChange: function(dev) {
      //PlayIGL.initTooltip();  
      $('.localTime').each(function(i,v) {
          if ($(v).text().length == 10) {
            $(v).text(PlayIGL.formatTimestamp($(v).text(), dev));
          }
      })
      console.log('datetimepicker');
      $('.dateTimePicker').datetimepicker({ pick12HourFormat: false });
    },
    initTooltip: function() {
        console.log('Init Tooltip');
        $(".tooltip-player").simpletip({
            baseClass: 'largeTooltip',
            activeClass: 'largeTooltipActive',
            offset: [-10, -100],
            position: 'top',
            content: 'Retrieving info...',
            boundryCheck: false,
            onShow: function(){
                thisTooltip = this;
                $.ajax({ url: "/ajax/public/tooltipinfo.php?player_id=3"}).done(function(response) {
                    thisTooltip.update(response);
                });
            },
            onBeforeHide: function() {
                this.update('');
            }
            
        });
    },
    getLocalTimezone: function() {
        var cur_date = new Date();
        var offset = cur_date.getTimezoneOffset()*-60 / 3600;
        if(offset >= 0) {
            offset = Math.abs(offset);
            offset = '+' + offset;
        }
        return offset;
    },
    getLocalTimezoneOffset: function() {
        var cur_date = new Date();
        var offset = cur_date.getTimezoneOffset()*-60;
        if(offset >= 0) {
            offset = Math.abs(offset);
            offset = '+' + offset;
        }
        return offset;
    },
    formatTimestamp: function(timestamp, dev) {
        // String Arrays
        var weekday=new Array(7);weekday[0]="Sunday";weekday[1]="Monday";weekday[2]="Tuesday";weekday[3]="Wednesday";weekday[4]="Thursday";weekday[5]="Friday";weekday[6]="Saturday";
        var month=new Array();month[0]="January";month[1]="February";month[2]="March";month[3]="April";month[4]="May";month[5]="June";month[6]="July";month[7]="August";month[8]="September";month[9]="October";month[10]="November";month[11]="December";
        // Get Date
        var a = new Date(timestamp*1000);
        var year = a.getFullYear();
        var month = month[a.getMonth()];
        var date = ('0'+a.getDate()).slice(-2);
        var hour = a.getHours();
        if (hour < 12) {a_p = "AM";} else {a_p = "PM";}
        if (hour == 0) { hour = 12; }
        if (hour > 12) { hour = hour - 12;}
        var min = ('0'+a.getMinutes()).slice(-2);
        var sec = ('0'+a.getSeconds()).slice(-2);
        var day = weekday[a.getDay()];
        var split = new Date().toString().split(" ");
        if (dev == null) {
            var time = day+' '+month+' '+date+', '+year+' '+hour+':'+min+' '+a_p+' (GMT' + localTimezone()+')';
        } else {
            var time = date+'-'+('0'+(a.getMonth()+1)).slice(-2)+'-'+year+' '+('0'+a.getHours()).slice(-2)+':'+min+':'+sec;
        }
        return time;
    },
    getLocalTime: function() {
        now = new Date();
        now = now.getTime();
        return now;
    },
    getLocalTimeOffset: function() {
        var d = new Date();
        var tz = d.toString().split("GMT")[1].split(" (")[0];
        return tz;
    },
    getNowInUTC: function() {
        d = new Date();
        utc = PlayIGL.getLocalTime() + (d.getTimezoneOffset() * 60000);
        return utc;
    },
    getLocalTimeUnix: function (localTimeString) {
       localTime = Math.round(Date.parse(localTimeString)/1000);
       return localTime; 
    },
    getLocalTimeInUTC: function (localTimeString) {
       localTime = Math.round(Date.parse(localTimeString)/1000);
       return localTime; 
    },
            
     Analytics: {
        track: function(action, data) {
            analytics.track(action, data);
        }
     },
            
     Forms: {
        createGameDropdown: function(ddElement, defaultValue) {
            $.ajax({dataType: "json", url: "/api/game/", success: function(response) {
                    $(ddElement).html('<option value="0">-----</option>');
                    $(response.game).each(function(i, v) {
                        $(ddElement).append('<option value="'+v.game_id+'">'+v.game_name+'</option>');
                    })
                    if (typeof defaultValue != 'undefined') {
                         $(ddElement).val(defaultValue);
                    }
            }});
        },
        createDivisionDropdown: function(ddElement, gameId, defaultValue) {
            if (typeof gameId == 'undefined') { gameId = ""; }
            $.ajax({dataType: "json", url: "/api/game/"+gameId, success: function(response) {
                    $(ddElement).html('<option value="0">-----</option>');
                    $(response.game).each(function(i, g) {
                        $(g.game_leagues).each(function(i, v) {
                            $(ddElement).append('<option value="'+v.league_id+'">'+v.league_name+'</option>');
                        });
                    });
                    if (typeof defaultValue != 'undefined') {
                         $(ddElement).val(defaultValue);
                    }
            }});
        }
     }
};