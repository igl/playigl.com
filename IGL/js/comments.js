/*
 * This script loads comments & rpelies for team & player profiles
 */

var mmToMonth = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");

function showLocalDate(timestamp)
{
    var dt = new Date(timestamp * 1000);
    var mm = mmToMonth[dt.getMonth()];
    minutes = dt.getMinutes();
    if(minutes < 10)
    {
        minutes = '0' + minutes;
    }
    if(dt.getHours() > 11)
    {
        hour = dt.getHours() - 12;
        t = 'pm'
    }
    else
    {
        hour = dt.getHours();
        t = 'am'
    }
  return  mm + " " + dt.getDate() + "," + " " + hour + ":" + minutes + t;
}


//Character counter
(function($) {
    $.fn.extend( {
        limiter: function(limit, elem) {
            $(this).on("keyup focus", function() {
                setCount(this, elem);
            });
            function setCount(src, elem) {
                var chars = src.value.length;
                if (chars > limit) {
                    src.value = src.value.substr(0, limit);
                    chars = limit;
                }
                elem.html( limit - chars );
            }
            setCount($(this)[0], elem);
        }
    });
    })(jQuery);
    

function loadComments()
{
    subject_id = $('#page_id').val();
    subject_type = $('#page_name').val();
    if(subject_id > 0 && subject_type != '')
    {
        $.getJSON('/api/'+subject_type+'/' + subject_id, function(json) {
            if(json.success)
                {
                   comments = '';
                   url = document.URL;

                    $.each(json.comments, function(key, value) {
                        time = showLocalDate(value['post_date']);
                        comments += '<li class="media comments">' +
                                        '<div class="pull-left">' +
                                          '<a href="/player/'+value['poster_username']+'" class="avatar"><img src="/img'+value['poster_avatar']+'" class="media-object"></a>' +
                                          '<a onclick="showReply('+value['post_id']+')" class="reply">Reply</a>' +
                                        '</div>' +

                                        '<div class="media-body" id="comment_'+value['post_id']+'">' +
                                          '<blockquote>' +
                                            '<div><div class="pull-right">' +
                                            '<a href="https://twitter.com/share" class="twitter-share-button" data-text="' + value['poster_comment'] + '" data-via="playIGL" data-count="none">Tweet</a>' +
                                            '<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?\'http\':\'https\';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+\'://platform.twitter.com/widgets.js\';fjs.parentNode.insertBefore(js,fjs);}}(document, \'script\', \'twitter-wjs\');</script>' +
                                            '</div><a href="/player/'+value['poster_username']+'">'+value['poster_username']+'</a><br>' +
                                            '<span>'+time+'</span>' +
                                            '<p>'+value['poster_comment']+'</p>' +

                                          '</blockquote>' +
                                          '<ul class="clear media-list" id="replies_'+value['post_id']+'">';

                        $.each(value['post_replies'], function(key, value) {
                            time = showLocalDate(value['post_date']);
                            comments += '<li class="media comments"><a href="/player/'+value['poster_username']+'" class="avatar pull-left"><img src="/img'+value['poster_avatar']+'" class="media-object"></a>' +
                                            '<div class="media-body">' +
                                              '<blockquote>' +
                                                '<div class="player"><a href="/player/'+value['poster_username']+'">'+value['poster_username']+'</a></div>' +
                                                '<div class="date">'+time+'</div>' +
                                                '<p>'+value['poster_comment']+'</p>' +
                                            '</div>' +
                                          '</li>';
                        });
                        comments += '</ul><div id="comment_reply_'+value['post_id']+'"></div></div></li>';      
                    });
                    $('#'+subject_type+'_comments').html(comments);  
                }
                else
                {
                    $('#'+subject_type+'_comments').html('<li>Error loading comments.</li>');  
                }
        });   
        
    }
}

function commentPost() {
    id = $('#page_id').val();
    page = $('#page_name').val();
    if(id > 0 && page != '' && $("#comment").val().length)
    {
        $('#button_post').html('Post Comment <i class="icon-refresh icon-spin"></i>').attr('disabled',true);
        formData = $('#comment_form').serialize();
        
         $.ajax({
            type: "POST",
            url: "/comment/post/",
            data: formData,
            dataType: "json",
            success: function(json){
                if(json.success)
                {
                    time = showLocalDate(json.result['dateposted']);
                    url = document.URL;
                    comment = '<li class="media comments">' +
                                '<div class="pull-left">' +
                                  '<a href="/player/'+json.result['posterid']+'-'+json.result['username']+'" class="avatar"><img src="/img'+json.result['avatar']+'" class="media-object"></a>' +
                                  '<a class="reply" onclick="showReply('+json.result['postid']+')">Reply</a>' +
                                '</div>' +

                                '<div class="media-body" id="comment_'+json.result['postid']+'">' +
                                  '<blockquote>' +
                                    '<div class="player"><a href="/player/'+json.result['posterid']+'-'+json.result['username']+'">'+json.result['username']+'</a></div>' +
                                    '<div class="date">'+time+'</div>' +
                                    '<p>'+json.result['comment']+'</p>' +

                                  '</blockquote>' +
                                  '<ul class="clear media-list" id="replies_'+json.result['postid']+'">' +
                                  '</ul>' +
                                  '<div id="comment_reply_'+json.result['postid']+'"></div>' +
                                '</div>' +
                              '</li>';

                    $('#'+page+'_comments').prepend(comment).on();
                    $('#button_post').text('Post Comment').attr('disabled',false);
                    $('#comment').val('');
                    PlayIGL.Analytics.track('comment_player', { actor_username: json.result['username'], subject_username: json.result['username'], subject_email: json.result['subject_email']})
                }
                else
                {
                    alert(json.error);
                    $('#button_post').text('Post Comment').attr('disabled',false);
                }
            }
        });
    } else {
      alert("Please enter a comment");
    }
}
function showReply(commentid)
{
    page_id = $('#page_id').val();
    viewer = $('#viewer_id').val();
    if(viewer > 0 && page_id > 0)
    {
        $('.reply_form').remove();
        reply = '<form id="reply_'+commentid+'" class="reply_form">' +
                    '<input type="hidden" name="parentid" value="'+commentid+'">' +
                    '<input type="hidden" name="page_id" value="'+page_id+'">' +
                    '<textarea id="reply" name="reply" style="margin-left: 6px;width:482px;height:60px"></textarea><button type="button" class="close reply-close" data-dismiss="alert"><i class="icon-remove"></i></button>' +
                    '<div class="full">' +
                    '<small class="pull-left">You have <span id="chars_reply"></span> characters left.</small>' +
                    '<button id="button_reply" type="button" onclick="commentReply('+commentid+')" class="btn btn-info pull-right">Reply</button>' +
                    '</div>' +
                '</form>';
            $('#comment_reply_'+commentid).html(reply);
            var elem = $("#chars_reply");
            $('#reply').limiter(130, elem);
    }
}

function hideReply(commentid)
{
    $('.reply_form').remove();
    $('#reply_'+commentid).remove();
}

function commentReply(commentid)
{
    page_id = $('#page_id').val();
    viewer = $('#viewer_id').val();
    
    $form = $('#reply_'+commentid);
    
    if(viewer > 0 && page_id > 0 && $form.find("#reply").val().length)
    {
        $('#button_reply').html('Reply <i class="icon-refresh icon-spin"></i>').attr('disabled',true);
        formData = $form.serialize();
         $.ajax({
            type: "POST",
            url: "/comment/reply/",
            data: formData,
            dataType: "json",
            success: function(json){
                if(json.success)
                {
                    time = showLocalDate(json.result['dateposted']);
                    url = document.URL;
                    comment = '<li class="media comments"><a href="/player/'+json.result['posterid']+'-'+json.result['username']+'" class="avatar pull-left"><img src="/img'+json.result['avatar']+'" class="media-object"></a>' +
                      '<div class="media-body">' +
                        '<blockquote>' +
                          '<div class="player"><a href="/player/'+json.result['posterid']+'-'+json.result['username']+'">'+json.result['username']+'</a></div>'+
                          '<div class="date">'+time+'</div>' +
                          '<p>'+json.result['comment']+'</p>' +
                      '</div>' +
                    '</li>';
                    $('#replies_'+commentid).append(comment);
                    $('#button_reply').text('Reply').attr('disabled',false);
                    hideReply(commentid);
                }
                else
                {
                    alert(json.error);
                    $('#button_reply').text('Reply').attr('disabled',false);
                }
            }
        });  
        var elem = $("#chars");
        $('#comment').limiter(130, elem);
    } else {
      alert("Please enter a comment");
    }
}