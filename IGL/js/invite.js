/* 
 * Used in view/freeagents.php & view/player.php
 */

function invite(team, action) {
    player = $('#page_id').val();
    $('#inviteIcon_'+player).removeClass().addClass('icon-refresh icon-spin');
    $('#invite_' + player).html('<i class="icon-refresh icon-spin"></i> Wait...');

    
    $.getJSON('/invite/' + player + '/' + team + '/' + action, function(data) {
        if (data.success == undefined)
        {
            alert('Sorry, there was a problem with this request. Please try again.');
        }
        else
        {
            if (data.success)
            {
                if(action == 'invite')
                {
                    //freeagent.php items
                    $('#invite_' + player).text('Revoke');
                    $('#invite_' + player).attr('onclick','invite('+player+','+team+',\'revoke\')');
                    
                    //player.php items
                    $('#state_'+team).removeClass().addClass('icon-check');
                    $('#handleInvite_'+team).attr('onclick','invite('+player+','+team+',\'revoke\')');
                    $('#inviteIcon_'+player).removeClass().addClass('icon-envelope');
                }

                if(action == 'revoke')
                {
                    //freeagent.php items
                    $('#invite_' + player).text('Invite');
                    $('#invite_' + player).attr('onclick','invite('+player+','+team+',\'invite\')');
                    
                    //player.php items
                    $('#state_'+team).removeClass().addClass('icon-check-empty');
                    $('#handleInvite_'+team).attr('onclick','invite('+player+','+team+',\'invite\')');
                    $('#inviteIcon_'+player).removeClass().addClass('icon-envelope');
                }
            }
            else
            {
                if (data.message == undefined)
                    alert('Unknown error while trying to ' + action);
                else
                    alert(data.message);
            }
        }
    });
}