// ***************************************
// Registration Class
// Copyright PlayIGL.com
// Author: Kevin Langlois*
// ***************************************
Authentication = {
    info: {},
    redirect: '',
    root: '',
// ***************************************
// Start Authentication Process
// ***************************************
    start: function(r) {   
        /*
        var_html = '<style>.tooltip-on { margin-left:10px; position:relative; top:4px; }</style>';
        var_html += '<div class="modal hide" id="Modal" tabindex="-1" role="dialog" aria-labelledby="teamModalLabel" aria-hide="true">';
        var_html += '<div class="modal-header">';
        var_html += '<button type="button" class="close" data-dismiss="modal" aria-hide="true"><i class="icon-remove"></i></button>'
        var_html += '<h3 id="ModalLabel">LOADING</h3>'
        var_html += '</div>'
        var_html += '<div class="modal-body" id="ModalBody">'
        var_html += '<div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div>'
        var_html += '</div>'
        $('body').append(var_html);
        */
        Authentication.redirect = $('#redirect').data('referer');
        Authentication.root = $("#redirect").data('root');
        login();
    },
// ***************************************
// Login Steam
// ***************************************
    loginSteam: function(username) { 
         var steamWindow = window.open('https://steamcommunity.com/openid/login?openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&openid.mode=checkid_setup&openid.return_to=http://'+document.domain+'/ajax/admin/steamLogin.php&openid.realm=http://'+document.domain+'&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select', 'Steam Community Connect', 'width=980,height='+$(window).height()); 
    },
    steamCallback: function(steam_id, swindow) {
        swindow.close();
        Authentication.info.steam = steam_id;
        console.log(Authentication.info);
        Authentication.getCredentials('communityid',  Authentication.info.steam);
     },
// ***************************************
// Login Twitch
// ***************************************
    loginTwitch: function(username) { 
         var twitchWindow = window.open('https://api.twitch.tv/kraken/oauth2/authorize?response_type=code&client_id=jxhlriwned9k6uz43fzi08vcc6zcggv&redirect_uri=http://www.playigl.com/ajax/admin/twitchLogin.php&scope=user_read+channel_read', 'Twitch Connect', 'width=980,height='+$(window).height());
    },
    twitchCallback: function(twitch_id, swindow) {
        swindow.close();
        Authentication.info.twitch = twitch_id;
        console.log(Authentication.info);
        Authentication.getCredentials('twitch',  Authentication.info.twitch);
    },
// ***************************************
// Login Facebook
// ***************************************
    loginFacebook: function(username) { 
        var facebookWindow = window.open('http://www.facebook.com/dialog/oauth/?client_id=190820527646793&redirect_uri='+escape("http://www.playigl.com/ajax/admin/facebookLogin.php") + '&state=igl&display=popup', 'Facebook Connect', 'width=980,height='+$(window).height());
  
    },
    facebookCallback: function(facebook_token, facebook_name, facebook_user, swindow) {
        swindow.close();
        Authentication.info.facebook_token = facebook_token;
        Authentication.info.facebook = facebook_name;
        console.log(Authentication.info);
        Authentication.getCredentials('facebook_token',  Authentication.info.facebook_token);
    },
// ***************************************
// Login Twitter
// ***************************************
    loginTwitter: function(username) { 
       var twitterWindow = window.open('/twitterLogin.php', '', 'width=980,height=600');
      
    },
    twitterCallback: function(twitter_token, twitter_info, swindow) {
        swindow.close();
        Authentication.info.twitter_token = twitter_token;
        Authentication.info.twitter = twitter_info;
        console.log(Authentication.info);
        Authentication.getCredentials('twitter_token', Authentication.info.twitter_token);
    },
// ***************************************
// Get Credentials
// ***************************************
    getCredentials: function (check, value) {
        console.log(check + ": " + value);
        if (value != "") {
            $.ajax({ url: "/ajax/admin/socialLogin.php?check="+check+"&value="+value }).done(function(response) {
                Authentication.socialCallback(response.user, response.hash);
            });          
        } else {
            alert('There was an error contacting the service. Please use your IGL credentials.')
        }
    },
// ***************************************
// Social Callback
// ***************************************
    socialCallback: function(user, hash) {
        if (user != "undefined" && hash != "undefined") {
            $('#login_username').val(user);
            $('#login_password').val('');
            $('#login_md5password').val(hash)
            Authentication.authenticateUser();
        } else {
            Alert('Please connect your social account via the PlayIGL Dashboard.')
        }
    },
// ***************************************
// Set Remember Me Cookie
// ***************************************
    rememberMe: function(c_name,value) {
        $.cookie(c_name, value, { expires: 14 });
    },
// ***************************************
// Get Remember Me Cookie
// ***************************************
    getRememberMe: function(c_name) {
        return $.cookie(c_name);
    },
// ***************************************
// Check Remember Me Cookie
// ***************************************
    checkRememberMe: function() {
        var username = $.cookie('username');
        var hash = $.cookie('hash');
        if (username !=null && username !="" && hash !=null && hash !="") {
            return true;
        }
    },
// ***************************************
// Perform Authentication
// ***************************************
    authenticateUser: function() {
        if ($('#login_username').val() != "") {
            if ($('#login_password').val() != "") {
                $("#login_md5password").val(md5($("#login_password").val()));
            }
            $.ajax({ url: "/ajax/admin/authenticate.php?user="+$('#login_username').val()+"&hash="+$("#login_md5password").val()+"&offset="+PlayIGL.getLocalTimezoneOffset() }).done(function(response) {
                console.log(response.success);
                if (response.success) {
                    console.log(response);
                    if ($('#rememberme').is(':checked')) {
                        Authentication.rememberMe('username', response.userinfo.username);
                        Authentication.rememberMe('hash', $('#login_password').val());
                    } else {
                        Authentication.rememberMe('username', "");
                        Authentication.rememberMe('hash', "");
                    }
                    
                    if (Authentication.redirect == Authentication.root || Authentication.redirect == (Authentication.root + 'thanks-for-signing-up') || Authentication.redirect == (Authentication.root + 'register') || Authentication.redirect == (Authentication.root + 'login')) {
                        document.location = "/home";
                    } else {
                        document.location = Authentication.redirect;
                    }
                    analytics.identify({id: response.userinfo.playerid, email : response.userinfo.email, created_at: response.userinfo.joindate, username: response.userinfo.username});
                    PlayIGL.Analytics.track('logged_in');
                } else {
                    $('.alert').text(response.error).fadeIn();
                    if (response.error == "Username does not exist") {
                        $('#login_username').val('').focus();
                        $('#login_password').val('');    
                    } else {
                        $('#login_password').val('').focus(); 
                    }
                }
            });
        } else {

        }
    }
    
};

function login() {

    $('#ModalBody').html('<div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div>');

    var_html = '<div class="header-img step5 login-header"><h2>USER LOGIN</h2></div>';

    
    var_html += '<div class="alert alert-error" style="display:none;"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Warning!</strong> The username or password you entered was incorrect.</div>';

    var_html += '<form id="loginform" class="loginform clearfix"><div class="left">';
    var_html += '<h3>Login with your credentials</h3>';
    var_html += '<input id="login_username" name="login_md5username" type="text" placeholder="Username" class="full">';
    var_html += '<input id="login_password" name="login_md5password" type="password" placeholder="●●●●●●●●" class="full">';
    var_html += '<input id="login_md5password" name="login_md5password" type="hidden" />';
    var_html += '<input type="button" class="btn btn-login" value="LOGIN" onclick="Authentication.authenticateUser()" />';
    var_html += '<div class="remember"><input type="checkbox" id="rememberme" name="rememberme" value="remember"> Remember Me</div>';
    var_html += '<div class="small-links"><a href="/reset/" class="forgot-pass">Forgot Password?</a><br /><a href="/register">Dont have an account?</a></div>';

    var_html += '</div><div class="right">';
    var_html += '<h3>Login with a social account</h3>';
    var_html += '<div class="social-connect loginver">';
    var_html += '<a href="#" class="connect-btn steam" onclick="Authentication.loginSteam()"><div class="icon-wrap"><img class="icon" src="../img/register/icon-steam.png"></div> <span>With Steam</span></a>';
    var_html += '<a href="#" class="connect-btn twitch" onclick="Authentication.loginTwitch()"><div class="icon-wrap"><img class="icon" src="../img/register/icon-twitch.png"></div> <span>With Twitch</span></a>';
    var_html += '<a href="#" class="connect-btn facebook" onclick="Authentication.loginFacebook()"><div class="icon-wrap"><img class="icon" src="../img/register/icon-facebook.png"></div> <span>With Facebook</span></a>';
    var_html += '<a href="#" class="connect-btn twitter last" onclick="Authentication.loginTwitter()"><div class="icon-wrap"><img class="icon" src="../img/register/icon-twitter.png"></div> <span>With Twitter</span></a>';
    var_html += '</div>';

    var_html += '</div></form>';

    $('#ModalBody').html(var_html); 
    $('#login_password').keypress(function (e) {
        if (e.which == 13) {
            Authentication.authenticateUser();
            return false;
        }
    });
    if (Authentication.checkRememberMe() == true) {
        $('#login_password').val(Authentication.getRememberMe("hash"))
        $('#login_username').val(Authentication.getRememberMe("username"));
        $('#rememberme').prop('checked', 'checked');
        Authentication.authenticateUser();
    }

    $('#username').focus();   
}
