<?php

//include some configs
require('includes/config.php');
//include our mySQL class
require('model/mysql.class.php');
//include some functions
require('includes/functions.php');

$db = new MySQL;

//Get 120 days ago
$then = time() - (60 * 60 * 24 * 150);

//Set our starting date, rounded to the day.
$start = strtotime(date('Y-m-d', $then));
$start = 1356998400;

//Get starting month
$month = date('F',$start);

//Print our starting month
echo "<h2>{$month}</h2>";

//Set our days to 0
$days = 1;

//Set running total & monthly total regd to 0
$total = 0;
$monthly_total = 0;


while( $start <= time() )
{
    //Set the last second of the day
    $end = $start + (60*60*24) - 1;
    
    //Query the player table
    $query = "SELECT playerid FROM player WHERE joindate >= {$start} AND joindate < {$end}";
    $db->ExecuteSQL($query);
    
    //Set num regid
    $regd = $db->iRecords();
    
    //Update our running total
    $total = $total + $regd;
    
    //Update our monthly running total
    $monthly_total = $monthly_total + $regd;
    
    
    //Check if we're reading a new monh
    if( $month != date('F',$start) )
    {
        echo "<h4>{$month} Daily Average: ".round(($monthly_total/$days),2)."</h4>";
        echo "<h4>{$month} Total: {$monthly_total}";
        echo "<h2>".date('F',$start)."</h2>";
        
        //Set the new current month
        $month = date('F',$start);
        
        //Reset our monthly total
        $monthly_total = 0;
        
        //Reset our days with a new month
        $days = 1;
    }
    
    //Print the date & num users reg'd
    echo date('F d, Y',$start)." - ".$regd."<br />";
    
    //Go to the next day...
    $start = $start + 86400;
    
    //Add another day to our counter
    $days++;
}

//Print stuff for the last month / iteration
echo "<h4>{$month} Daily Average: ".round(($monthly_total/$days),2)."</h4>";
echo "<h4>{$month} Total: {$monthly_total}</h4>";
echo "-------------------------------------------------";
echo "<h4>Total Reg'd users last 120 days: {$total}</h4>";