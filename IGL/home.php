<?php
define('IGL_PAGE', true);

include_once('./includes/config.php');
include_once('./model/mysql.class.php');
include_once('./includes/functions.php');

$db = new mysql(MYSQL_DATABASE);

//Set up our default Page
if (!isset($_GET['page']))
    $_GET['page'] = "home";

//Get Page Vars
$query = "SELECT * FROM `page_vars` WHERE `page` = '{$_GET['page']}'";
$b = $db->ExecuteSQL($query);
$page_vars = array();

//Check DB Results
if ($b && $db->iRecords())
    $page_vars = $db->ArrayResult();

//If no page entry exists, redirect
if (empty($page_vars))
    header('location: ' . BASEURL . '404/');

//Generate PHP Includes
if (isset($page_vars) && !empty($page_vars['objects']) && !is_null($page_vars['objects'])) {
    $php_files = explode(";", $page_vars['objects']);
    foreach ($php_files as $php) {
        include_once($php);
    }
}

//If logged in, bring in permissions
//if (isset($_SESSION['playerid']) && $_SESSION['playerid'] > 0)
include_once('includes/permissions.php');

//Scope variables
$content = false;

//Do this for tournament specific pages
if (in_array($_GET['page'], array('tournament'))) {
    $id = (int) $_GET['id'];
    $tournament = json_decode(file_get_contents(BASEURL . "api/tournament/{$id}"), true);
    $game = $tournament['tournament']['tournament_game_id'];
    $game_image = $tournament['tournament']['tournament_game_banner'];
}

//Check which page to load
switch ($_GET['page']) {

    case "me":
        if (isset($_SESSION['playerid']) && is_numeric($_SESSION['playerid'])) {
            header('location: ' . BASEURL . 'player/' . $_SESSION['username']);
        }
        else
            header('location: ' . BASEURL . 'login');
        break;
    case "404":
        $content = 'view/404.php';
        break;

    case "admin":
        if (isset($_SESSION['playerid']) && is_numeric($_SESSION['playerid']) && PRESS) {
            if (isset($_GET['view']) && in_array($_GET['view'], array(
                        "allconfigs",
                        "allgames",
                        "allleagues",
                        "allladders",
                        "alltournaments",
                        "allarticles",
                        "allnews",
                        "allteams",
                        "allplayers",
                        "addconfig",
                        "addgame",
                        "addladder",
                        "addleague",
                        "addtournament",
                        "addarticle",
                        "addnews",
                        "addteam",
                        "addplayer",
                        "addmappool",
                        "deleteconfig",
                        "deletegame",
                        "deleteleague",
                        "deleteladder",
                        "deletetournament",
                        "deletearticle",
                        "deletenews",
                        "deleteteam",
                        "deleteplayer",
                        "deletemappool",
                        "editconfig",
                        "editgame",
                        "editleague",
                        "editladder",
                        "edittournament",
                        "editarticle",
                        "editnews",
                        "editteam",
                        "editplayer",
                        "editmappool",
                        "mappools",
                        "uploadmap",
                        "managetournament",
                        "manageschedule",
                        "matchservers",
                        "allshowmatches",
                        "addshowmatch",
                        "editshowmatch",
                        "deleteshowmatch",
                        "managematches",
                        "ads",
                        "deletemap",
                        "minievent",
                        "tournaments"
                            )
            )) {
                $content = "view/admin/" . $_GET['view'] . ".php";
            } else {
                $content = "view/admin.php";
            }
        } else {
            header('location: ' . BASEURL . '404');
        }
        break;

    case "contestrules":
        $content = 'view/contestrules.php';
        break;

    case "privacypolicy":
        $content = 'view/privacypolicy.php';
        break;

    case "twitter":
        $include_navi = false;
        $content = 'view/twitter.php';
        break;

    case "faq":
        $content = 'view/faq.php';
        break;

    case "about":
        $content = 'view/about.php';
        break;

    case "contact":
        $content = 'view/contact.php';
        break;

    case "login":
        $content = 'view/login.php';
        break;

    case "register":
        if (isset($_SESSION['playerid'])) {
            $content = 'view/404.php';
        } else {
            $content = 'view/register.php';
        }
        break;

    case "thankyou":
        $content = 'view/thankyou.php';
        break;

    case "reset":
        $content = 'view/reset.php';
        break;

    case "search":
        $q = $_REQUEST['q'];
        echo file_get_contents(BASEURL . "/api/search/{$q}");
        break;

    case "game":
        $game = new game();
        $content_navigation = 'view/game/navigation.php';
        $content_footer = 'view/game/footer.php';

        $game_name = urldecode($_GET['name']);
        
        $db->ExecuteSQL("SELECT id, name FROM game WHERE name = '{$game_name}' ");
        if ($db->iRecords())
            $r = $db->ArrayResult();
        else
            header('location: ' . BASEURL . '404');

        //Re-Set game name as stored in DB
        $game_name = $r['name'];
        $game_id = $r['id'];


        $navigation = new navigation();
        $comp_navi = $navigation->competitions($r['id']);
        $game_navi = $navigation->game($r['id']);

        function cmp($a, $b) {
            if ($a['sponsored'] || $a['featured'])
                return -1;
            if ($b['sponsored'] || $b['featured'])
                return 1;
            if ($a['popularity'] == $b['popularity'])
                return 0;
            return ($a['popularity'] > $b['popularity']) ? -1 : 1;
        }

        $match_cards = array();
        $youtube_vods = array();

        $db->ExecuteSQL(
                "SELECT * FROM match_cards LIMIT 0,5"
        );
        if ($db->iRecords()) {
            $match_cards = $db->ArrayResults();

            foreach ($match_cards as $key => $card) {
                $db->ExecuteSQL(
                        "SELECT * FROM match_popularity WHERE id = {$card['id']} LIMIT 1;"
                );
                if ($db->iRecords()) {
                    $pop = $db->ArrayResult();
                    $match_cards[$key]['popularity'] = $pop['follow_popularity'] + (5 * $pop['vote_popularity']);
                }
            }
            usort($match_cards, "cmp");
        }

        if (count($match_cards) < 8) {
            $youtube = $db->ExecuteSQL(
                    "SELECT * FROM youtube"
            );

            $feed = $db->ArrayResults();
            if (count($feed)) {
                $needed_vods = 9 - count($match_cards);
                $used_index = array();
                do {
                    $ndex = rand(0, count($feed) - 1);

                    if (!isset($used_ndex[$ndex])) {
                        $youtube_vods[] = array(
                            "thumbnail" => $feed[$ndex]['thumbnail'],
                            "link" => $feed[$ndex]['link'],
                            "title" => $feed[$ndex]['title']
                        );

                        $used_ndex[$ndex] = true;
                        $needed_vods--;
                    }
                } while ($needed_vods > 0);
            }
        }

        $forums = new Forums();
        $top_posters = $forums->getCustomHTML('top players');
        $new_threads = $forums->getCustomHTML('newest threads');

        $view = strtolower($_GET['view']);

        $p = $_GET['p'];
        if (empty($p)) {
            $p = 1;
            $_GET['p'] = 1;
        }

        switch ($view) {
            
            case "teams":
                $on_game_team = $game->onTeam($game_id, $_SESSION['playerid']);
                $teams = $game->getTeams($game_id);
                $content = 'view/game/teams.php';
                break;

            case "players":
                $players = $game->getPlayers($game_id, $p);
                $content = 'view/game/players.php';
                break;

            case "free agents":
                $on_game_team = $game->onTeam($game_id, $_SESSION['playerid']);
                $is_agent = $game->isAgent($game_id, $_SESSION['playerid']);
                $players = $game->getFreeagents($game_id, $p);
                $content = 'view/game/freeagents.php';
                break;

            case "ladder":
                $ladder = new ladder();

                //Make sure the vars being passed exist in the db. Helps prevent SQL injection.
                $db->ExecuteSQL("SELECT name FROM ladder_settings");
                $p = array();
                if($db->iRecords()) {
                    foreach($db->ArrayResults() as $n) {
                        $p[] = strtolower($n['name']);
                    }
                }
                
                $continent = strtolower($_GET['continent']);
                $sub_page = strtolower($_GET['sub_page']);
                if(!in_array($sub_page, $p)) {
                    header('location: ' . BASEURL . '404');
                }
                //Lets gather our ladder info here
                $db->ExecuteSQL("SELECT ladder.id as tier_id, ladder_settings.name as ladder_name FROM ladder JOIN ladder_settings ON ladder_settings.id = ladder.settings WHERE ladder_settings.game = {$game_id} AND ladder_settings.name = '$sub_page' AND ladder_settings.continent = '{$continent}' ORDER BY ladder.id ASC");
                if ($db->iRecords())
                    $tiers = $db->ArrayResults();
                else
                    header('location: ' . BASEURL . '404');

                //Subtract 1 from our division to get the index of the tier from the array
                $tier_id = $tiers[($_GET['division'] - 1)]['tier_id'];

                //Get global ladder info
                $ladder_info = $ladder->get($tier_id);

                $sub_view = strtolower($_GET['sub_view']);
                
                $is_ladder_captain = $ladder->isCaptain($tier_id, $_SESSION['playerid']);
                $is_game_captain = $game->isCaptain($game_id, $_SESSION['playerid']);
                $on_game_team = $game->onTeam($game_id, $_SESSION['playerid']);
                $on_ladder_team = $ladder->onTeam($tier_id, $_SESSION['playerid']);

                switch ($sub_view) {
                    
                    case "standings":
                        $standings = $ladder->getStandings($tier_id);
                        $content = 'view/game/ladder/standings.php';
                        break;

                    case "faq":
                        $faq = $ladder_info['faq'];
                        $content = 'view/game/ladder/faq.php';
                        break;

                    case "rules":
                        $rules = $ladder_info['rules'];
                        $content = 'view/game/ladder/rules.php';
                        break;

                    case "results":
                        $results = $ladder->getResults($tier_id);
                        $content = 'view/game/ladder/results.php';
                        break;

                    case "schedule":
                        $schedule = $ladder->getSchedule($tier_id);
                        $content = 'view/game/ladder/schedule.php';
                        break;

                    default :
                        $match_listings = $ladder->getMatchListings($tier_id);
                        $content = 'view/game/ladder/match_listings.php';
                        break;
                }
                break;
            
            //Handle Tournaments
            case "tournament":
                $tournament = new tournament();

                //Make sure the vars being passed exist in the db. Helps prevent SQL injection.
                $db->ExecuteSQL("SELECT name FROM tournaments");
                
                $p = array();
                if($db->iRecords()) {
                    $r = $db->ArrayResults();
                    foreach($r as $n) {
                        $p[] = strtolower($n['name']);
                    }
                }
                $continent = strtolower($_GET['continent']);
                $sub_page = strtolower($_GET['sub_page']);

                if(!in_array($sub_page, $p)) {
                    header('location: ' . BASEURL . '404');
                }
                
                //Lets gather our ladder info here
                $db->ExecuteSQL("SELECT id AS tournament_id, name as tournament_name, format as tournament_format FROM tournaments WHERE game = {$game_id} AND name = '{$sub_page}' AND continent = '{$continent}' ORDER BY id ASC");
                if ($db->iRecords())
                    $r = $db->ArrayResult();
                else
                    header('location: ' . BASEURL . '404');
                $t = $tournament->get($r['tournament_id']);

                $content = 'view/game/tournament/tournament.php';
                break;
                
                case "event":
                    //Make sure the vars being passed exist in the db. Helps prevent SQL injection.
                    $db->ExecuteSQL("SELECT name FROM ".MYSQL_DATABASE.".showmatches");

                    $p = array();
                    if($db->iRecords()) {
                        $r = $db->ArrayResults();
                        foreach($r as $n) {
                            $p[] = strtolower($n['name']);
                        }
                    }
                    $sub_page = strtolower($_GET['sub_page']);

                    if(!in_array($sub_page, $p)) {
                        header('location: ' . BASEURL . '404');
                    }
                    
                    $db->ExecuteSQL("SELECT id as event_id FROM ".MYSQL_DATABASE.".showmatches WHERE name = '{$sub_page}' AND game_id = {$game_id}");

                    if ($db->iRecords())
                        $r = $db->ArrayResult();
                    else
                        header('location: ' . BASEURL . '404');
                    
                    $showmatch = new showmatch();
                    $match = $showmatch->get($r['event_id']);

                    $content = 'view/game/event/event.php';
                    break;

            default:
                $_GET['view'] = 'all schedules';
                $schedule = $game->getSchedule($game_id, $p);
                $content = 'view/game/schedule.php';
                break;
        }


        break;
/*
    case "ladder":
        $id = (int) $_GET['id'];
        $ladder = new ladder();
        $ladder_info = json_decode(file_get_contents(BASEURL . "api/ladder/{$id}"), true);
        #$ladder_standings = $ladder->getStandings($id);
        //Get %of teams this ladder sports for the game
        if ($ladder_info['ladder']['ladder_num_teams_global'] > 0)
            $weight = $ladder_info['ladder']['ladder_num_teams'] / $ladder_info['ladder']['ladder_num_teams_global'];
        else
            $weight = 1;

        //Dedicate the right % of servers/team slots to this particular ladder tier
        //2 slots per server, so we double up, but since servers get booked in 2hr intervals, and matches occur every hour, our servers get cut inhalf again.
        $ladder_info['ladder']['ladder_slots'] = floor((floor($weight * $ladder_info['ladder']['ladder_num_servers_global'])) / 2) * 2; //showing *2/2 for formality

        switch ($_GET['view']) {
            case "schedule":
                $ladder_schedule = $ladder->getSchedule($id);
                $content = 'view/ladder/schedule.php';
                break;
            case "results":
                $ladder_results = $ladder->getResults($id);
                $content = 'view/ladder/results.php';
                break;
            case "rules":
                $content = 'view/ladder/rules.php';
                break;
            case "faq":
                $content = 'view/ladder/faq.php';
                break;
            default:
                $content = 'view/ladder/standings.php';
                break;
        }
        break;

    case "tournament":
        $content = 'view/tournament.php';
        break;
*/
    case "showmatch":
        $content = 'view/showmatch.php';
        break;

    case "minievent":
        $content = 'view/minievent.php';
        break;
/*
    case "freeagents":
        $game = (int) $_GET['id'];
        $free_agent_list = json_decode(file_get_contents(BASEURL . "api/freeagents/{$game}"), true);
        $navigation = json_decode(file_get_contents(BASEURL . "api/navigation"), true);
        $content = 'view/freeagents.php';
        break;

    case "openteams":
        $game = (int) $_GET['id'];
        $content = 'view/openteams.php';
        break;

    case "playerstats":
        $id = (int) $_GET['id'];
        $player = (int) $_REQUEST['player'];
        $stats = json_decode(file_get_contents(BASEURL . "api/playerstats/{$id}/{$player}"), true);
        $content = 'view/playerstats.php';
        break;

    case "matchstats":
        $id = (int) $_GET['id'];
        $stats = json_decode(file_get_contents(BASEURL . "api/matchstats/{$id}"), true);
        $content = 'view/matchstats.php';
        break;

    case "stats":
        $stats = true;
        break;
*/
    case "team":
        $id = $_GET['id'];
        include_once("./model/team.class.php");

        switch ($_GET['view']) {

            case "results":
                $team = new team();
                $team_record = $team->getWinRecord($id);
                $team_results = $team->getResults($id);
                $last_10 = $team->getLast10Record($id);
                #$league_record = $team->getLeagueRecord($id);
                $ladder_record = $team->getLadderRecord($id);
                $tournament_record = $team->getTournamentRecord($id);
                $team = json_decode(file_get_contents(BASEURL . "api/team/{$id}"), true);
                $content = 'view/team/results.php';
                break;


            default:
                $team = new team();
                $team_results = $team->getResults($id, null, 5);
                // Compile team data
                $team = json_decode(file_get_contents(BASEURL . "api/team/{$id}"), true);

                if ($team['success']) {
                    $db->ExecuteSQL("SELECT * FROM inviterequests WHERE team = {$id}");
                    $team['team']['invites'] = $db->ArrayResults();

                    $history = json_decode(file_get_contents(BASEURL . "api/history/team/{$id}"), true);
                    if ($history['success'])
                        $team['history'] = $history['history'];

                    $team_info = array('team_name', 'team_tag', 'team_game_name', 'team_continent', 'team_region', 'team_rpi', 'team_followers', 'team_created', 'team_captain_name', 'team_alternate_name', 'team_website', 'team_facebook', 'team_twitter');
                    $team['team']['team_created'] = date('Y-m-d', ISOtime($team['team']['team_created']));
                    $team_html = array();
                    $team_ladders = array();

                    foreach ($team['team']['team_ladders'] as $l)
                        $team_ladders[] = "Ladder: <a href='/ladder/{$l['ladder_id']}/standings'><span class='tag'>{$l['ladder_name']}</span></a>";

                    foreach ($team_info as $key) {

                        if ($team['team'][$key]) {
                            switch ($key) {
                                case 'team_name': $team_html[$key] = "<h2>{$team['team'][$key]}</h2>";
                                    break;

                                case 'team_tag':
                                case 'team_game_name':
                                case 'team_continent':
                                case 'team_region':
                                case 'team_created':
                                case 'team_ladder_id':
                                    $title = explode('_', $key);
                                    $title = ucfirst($title[1]);

                                    if ($key == 'team_ladder_id')
                                        $key = 'team_ladder_name';

                                    $team_html[$key] = "{$title}: <span class='tag'>{$team['team'][$key]}</span>";
                                    break;

                                case 'team_followers':
                                    $team_html[$key] = "Followers: <span class='tag'><a onclick=\"viewFollowers('team',{$id},{$_SESSION['playerid']})\">{$team['team'][$key]}</a></span>";
                                    break;

                                case 'team_league_id':
                                    $team_html[$key] = "League: <a href='/league/{$team['team']['team_league']}/{$team['team']['team_game']}/{$team['team']['team_league_name']}'><span class='tag'>{$team['team']['team_league']}</span></a> ({$team['team']['team_wins']} - {$team['team']['team_loses']})";
                                    break;

                                case 'team_captain_name':
                                    $team_html[$key] = "Captain: <a href='/player/{$team['team']['team_captain_name']}'><span class='tag'>{$team['team']['team_captain_name']}</span></a>";
                                    break;

                                case 'team_alternate_name':
                                    $team_html[$key] = "Alternate: <a href='/player/{$team['team']['team_alternate_name']}'><span class='tag'>{$team['team']['team_alternate_name']}</span></a>";
                                    break;

                                case 'team_website':
                                    $team_html[$key] = "Website: <a href='http://{$team['team']['team_website']}'><span class='tag'>{$team['team']['team_website']}</span></a>";
                                    break;

                                case 'team_facebook':
                                    $team_html[$key] = "<div class='fb-subscribe' data-href='http://www.facebook.com/{$team['team']['team_facebook']}' data-layout='button_count' data-show-faces='false' data-width='50'></div>";
                                    break;

                                case 'team_twitter':
                                    $team_html[$key] = "<a href='http://twitter.com/{$team['team']['team_twitter']}' class='twitter-follow-button' data-show-count='false'>Follow @{$team['team']['team_twitter']}</a><script type='text/javascript'>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src='//platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document,'script','twitter-wjs');</script>";
                                    break;
                            }
                        }
                    }
                }

                include_once("./model/backbone.class.php");
                $backbone = new Backbone($db);
                $team_builder_template = $backbone->getTemplate("dashboard", "team_builder");
                $content = 'view/team/profile.php';
                break;
        }
        break;


    case "player":
        //Compile player data
        if (isset($_REQUEST['player_username']))
            $id = urlencode($_REQUEST['player_username']);
        else
            $id = urlencode($_REQUEST['id']);

        // Compile player data 
        $player = json_decode(file_get_contents(BASEURL . "api/player/{$id}"), true);

        if (false == $player['success'])
            header('location: ' . BASEURL . '404');

        $page_vars['title'] = str_replace('[page_title]', $player['player']['player_username'], $page_vars['title']);

        $history = json_decode(file_get_contents(BASEURL . "api/history/player/{$player['player']['player_id']}"), true);

        if ($history['success'])
            $player['player_history'] = $history['history'];

        $id = $player['player']['player_id'];
        $twitch = new twitch();
        $channels = $twitch->getChannels($id);


        $content = 'view/player.php';
        break;

    case "article":
        $id = explode('-', $_GET['id']);
        $article_info = json_decode(file_get_contents(BASEURL . "api/article/{$id[0]}"), true);
        $article = $article_info['article'];
        $page_vars['title'] = $article['title'];
        $page_vars['meta'] .= (!is_null($page_vars['meta']) ? "," : "") . $article['meta'];
        $content = 'view/article.php';
        break;
/*
    case "news":
        $id = explode('-', $_GET['id']);
        $news_info = json_decode(file_get_contents(BASEURL . "api/news/{$id[0]}"), true);
        $news = $news_info['post'];
        $content = 'view/news.php';
        break;

    case "view":
        $content = 'view/view.php';
        break;
*/
    case "forums":
        $content = 'view/view.php';
        $forums = new Forums();
        $return = $forums->getForums();
        $content = $return['view'];
        break;

    case "match":
        
        
        $match = new Match();
        $team = new team();
        $navigation = new navigation();
        
        $thismatch = $match->matchdata($_REQUEST['id']);
        $_GET['name'] = $thismatch['game']['name'];
        $content_navigation = 'view/game/navigation.php';
        $game_name = $thismatch['game']['name'];
        $comp_navi = $navigation->competitions($thismatch['game']['id']);
        $game_navi = $navigation->game($thismatch['game']['id']);
        

        $team_stats = array();
        $team_stats['home']['L10'] = $team->getLast10Record($thismatch['home']['id']);
        $team_stats['home']['record'] = $team->getWinRecord($thismatch['home']['id']);


        $team_stats['away']['L10'] = $team->getLast10Record($thismatch['away']['id']);
        $team_stats['away']['record'] = $team->getWinRecord($thismatch['away']['id']);


        if (!empty($thismatch['ladder'])) {
            $thismatch['format'] = $thismatch['ladder']['format'];
        }
        if (!empty($thismatch['league'])) {
            $thismatch['format'] = $thismatch['league']['format'];
        }
        if (!empty($thismatch['tournament'])) {
            $thismatch['format'] = $thismatch['tournament']['format'];
        }

        if (!empty($thismatch['ladder'])) {
            $thismatch['format-name'] = $thismatch['ladder']['name'];
        }
        if (!empty($thismatch['league'])) {
            $thismatch['format-name'] = $thismatch['league']['name'];
        }
        if (!empty($thismatch['tournament'])) {
            $thismatch['format-name'] = $thismatch['tournament']['name'];
        }
        $content = 'view/match.php';
        break;

    case "dashboard":
        //Scope variables
        if (!is_null($_SESSION) && !empty($_SESSION) && $_SESSION['playerid'] >= 0) {
            include_once("./model/backbone.class.php");
            include_once("./model/steamlogin.class.php");
            $backbone = new Backbone($db);
            $SteamSignIn = new SteamSignIn();
            $current_view = isset($_GET['view']) ? $_GET['view'] : "profile";

            $steam_login_verify = SteamSignIn::validate();
            if (!empty($steam_login_verify))
                $db->update('player', array('communityid' => $steam_login_verify), array('playerid' => $_SESSION['playerid']));

            switch ($current_view) {
                case "teams":
                    $backbone->getData("get all player teams and invites", $_GET['page'], $current_view, array("id" => $_SESSION['playerid'], "team_id" => NULL));
                    $data = $backbone->safe_data;
                    break;
                case "referrals":
                    $backbone->getData("referrals", $_GET['page'], $current_view, NULL);
                    $data = $backbone->safe_data;
                    break;
                case "following":
                    $backbone->getData("followee", $_GET['page'], $current_view, NULL);
                    $data = $backbone->safe_data;
                    break;
                case "matches":
                    $backbone->getData("get all matches for player", $_GET['page'], $current_view, NULL);
                    $data = $backbone->safe_data;
                    break;
                default:
                    $backbone->getData("player", $_GET['page'], $current_view, array("id" => $_SESSION['playerid']));
                    $data = $backbone->safe_data;
                    break;
            }

            $backbone->getData("games");
            $all_games = $backbone->safe_data;

            $avatar_template = $backbone->getTemplate("dashboard", "avatar_modal");
            $profile_template = $backbone->getTemplate("dashboard", "profile");
            $team_create_template = $backbone->getTemplate("dashboard", "create_team");
            $teams_template = $backbone->getTemplate("dashboard", "teams");
            $team_admin_template = $backbone->getTemplate("dashboard", "team_admin");
            $team_details_template = $backbone->getTemplate("dashboard", "team_details");
            $team_edit_template = $backbone->getTemplate("dashboard", "team_edit");
            $team_password_template = $backbone->getTemplate("dashboard", "team_password");
            $team_builder_template = $backbone->getTemplate("dashboard", "team_builder");
            $team_leave_template = $backbone->getTemplate("dashboard", "team_leave");
            $matches_template = $backbone->getTemplate("dashboard", "match_list");
            $referral_template = $backbone->getTemplate("dashboard", "referrals");
            $followee_template = $backbone->getTemplate("dashboard", "followee");
            $score_template = $backbone->getTemplate("dashboard", "score_report");

            $content = './view/dashboard.php';
        } else {
            header('Location: ' . BASEURL . 'login');
        }
        break;

    default:

        function cmp($a, $b) {
            if ($a['sponsored'] || $a['featured'])
                return -1;
            if ($b['sponsored'] || $b['featured'])
                return 1;
            if ($a['popularity'] == $b['popularity'])
                return 0;
            return ($a['popularity'] > $b['popularity']) ? -1 : 1;
        }

        $article = new Article();
        $articles = $article->carousel(NULL);

        $match_cards = array();
        $youtube_vods = array();

        $db->ExecuteSQL(
                "SELECT * FROM match_cards LIMIT 0,5"
        );
        if ($db->iRecords()) {
            $match_cards = $db->ArrayResults();

            foreach ($match_cards as $key => $card) {
                $db->ExecuteSQL(
                        "SELECT * FROM match_popularity WHERE id = {$card['id']} LIMIT 1;"
                );
                if ($db->iRecords()) {
                    $pop = $db->ArrayResult();
                    $match_cards[$key]['popularity'] = $pop['follow_popularity'] + (5 * $pop['vote_popularity']);
                }
            }
            usort($match_cards, "cmp");
        }

        if (count($match_cards) < 9) {
            $youtube = $db->ExecuteSQL(
                    "SELECT * FROM youtube"
            );

            $feed = $db->ArrayResults();
            if (count($feed)) {
                $needed_vods = 9 - count($match_cards);
                $used_index = array();
                do {
                    $ndex = rand(0, count($feed) - 1);

                    if (!isset($used_ndex[$ndex])) {
                        $youtube_vods[] = array(
                            "thumbnail" => $feed[$ndex]['thumbnail'],
                            "link" => $feed[$ndex]['link'],
                            "title" => $feed[$ndex]['title']
                        );

                        $used_ndex[$ndex] = true;
                        $needed_vods--;
                    }
                } while ($needed_vods > 0);
            }
        }

        $forums = new Forums();
        $top_posters = $forums->getCustomHTML('top players');
        $new_threads = $forums->getCustomHTML('newest threads');

        $content = 'view/home.php';
        break;
}

//Generate Meta Keywords
$meta_keywords = "";
if (isset($page_vars) && !empty($page_vars['meta']) && !is_null($page_vars['meta'])) {
    $meta_keywords = "<meta name=\"keywords\" content=\"{$page_vars['meta']}\">";
}

//Generate CSS Includes
$css_includes = "";
if (isset($page_vars) && !empty($page_vars['css']) && !is_null($page_vars['css'])) {
    $css_files = explode(";", $page_vars['css']);
    foreach ($css_files as $css) {
        $cache = filemtime(getcwd()."/css/".$css);
        $css_includes .= "<link href=\"/css/{$css}?{$cache}\" rel=\"stylesheet\" type=\"text/css\" />";
    }
}

//Generate JS Includes
$js_includes = "";
if (isset($page_vars) && !empty($page_vars['js']) && !is_null($page_vars['js'])) {
    $js_files = explode(";", $page_vars['js']);
    foreach ($js_files as $js) {
        
        
        if (strpos($js, "//") !== false) {
            $cache = filemtime(getcwd()."/".$js);
            $js_includes .= "<script type=\"text/javascript\" src=\"{$js}?{$cache}\"></script>\n";
        }
        else{
            $cache = filemtime(getcwd()."/js/".$js);
            $js_includes .= "<script type=\"text/javascript\" src=\"/js/{$js}?{$cache}\"></script>\n";
        }
    }
}
//Generate JS Functions
$js_functions = (isset($page_vars) && !empty($page_vars['functions']) && !is_null($page_vars['functions'])) ? $page_vars['functions'] : "";




$db->ExecuteSQL("SELECT id, name, logo, (SELECT count(team_id) FROM ".MYSQL_DATABASE.".active_teams WHERE team_game_id = game.id) as num_teams FROM  ".MYSQL_DATABASE.".game WHERE status = 1 AND deleted = 0");
if($db->iRecords()) {
    $navi = $db->ArrayResults();
}

//Footer links
$query = "SELECT * FROM pages";
$db = new MySQL(MYSQL_DATABASE);
$db->ExecuteSQL($query);
$pages = $db->ArrayResults();
foreach ($pages as $k => $l)
    $pages[$k]['link'] = strtolower($l['title']);

//Include HTML
include_once('./templates/header.php');

if (isset($content_navigation)) {
    include_once($content_navigation);
}
include_once($content);
if (isset($content_footer)) {
    include_once($content_footer);
}

if (isset($page_vars) && $page_vars['nav']) {
    include_once('./templates/navigation.php');
}

include_once('./templates/footer.php');
?>
