<?php
/**
 * Fetches the schedule
 * @author Darryl Allen <dallen@palyigl.com>
 * @author Shaun Delaney <sdelaney@playigl.com>
 * @return JSON schedule list
 * @copyright Copyright 2013 PlayIGL.com
 */

include_once('../includes/config.php');
include_once('../model/mysql.class.php');
include_once('../includes/functions.php');

//Default error message
$return = array("success" => false, "error" => "Invalid League Request.");

//Variables
$id         = isset($_REQUEST['id']) ? $_REQUEST['id'] : null;
$game_id    = isset($_REQUEST['game']) ? strtolower($_REQUEST['game']) : null;

$db = new mysql(MYSQL_DATABASE);

if (!is_null($id) && is_numeric($id)) {
    $db->ExecuteSQL(
        "SELECT 
                `schedule`.`id`,
                `schedule`.`scheduleddate`,
                `schedule`.`officialdate`,
                `schedule`.`home`,
                `schedule`.`away`,
                `matchservers`.`ip` AS `server_ip`,
                `matchservers`.`port` AS `server_port`,
                `team_home`.`name` as `home_name`,
                `team_home`.`tag` as `home_tag`,
                `team_home`.`id` as `home_id`,
                `team_home`.`logo` as `home_logo`,
                `team_home`.`wins` as `home_wins`,
                `team_home`.`losses` as `home_losses`,
                `team_home`.`captain` as `home_captain`,
                `team_away`.`name` as `away_name`,
                `team_away`.`tag` as `away_tag`,
                `team_away`.`id` as `away_id`,
                `team_away`.`logo` as `away_logo`,
                `team_away`.`wins` as `away_wins`,
                `team_away`.`losses` as `away_losses`,
                `team_away`.`captain` as `away_captain`,
                `broadcaster`.`username` as `broadcaster_name`,
                `broadcaster`.`playerid` as `broadcaster_id`
            FROM 
                `schedule`
                LEFT JOIN `matchreserve` ON `matchreserve`.`id` = `schedule`.`reservation_id`
                LEFT JOIN `matchservers` ON `matchservers`.`id` = `matchreserve`.`server_id`
                LEFT JOIN `team` as `team_home` ON `schedule`.`home` = `team_home`.`id`
                LEFT JOIN `team` as `team_away` ON `schedule`.`away` = `team_away`.`id`
                LEFT JOIN `player` as `broadcaster` ON `broadcaster`.`playerid` = `schedule`.`stream`
            WHERE 
            `schedule`.`id` = {$id}"
    );

    if ($db->iRecords())
        $return = array('success' => true, 'match' => $db->ArrayResult());
    else
        $return['error'] = "Sorry, that match could not be found.";
} elseif (!is_null($game_id) && is_numeric($game_id)) {
    $now = time();
    $db->ExecuteSQL(
        "SELECT 
            `schedule`.`id`,
            `schedule`.`scheduleddate`,
            `schedule`.`home`,
            `schedule`.`away`,
            `schedule`.`server_ip`,
            `schedule`.`server_port`,
            `team_home`.`name` as `home_name`,
            `team_home`.`tag` as `home_tag`,
            `team_home`.`id` as `home_id`,
            `team_home`.`logo` as `home_logo`,
            `team_home`.`wins` as `home_wins`,
            `team_home`.`losses` as `home_losses`,
            `team_home`.`captain` as `home_captain`,
            `team_away`.`name` as `away_name`,
            `team_away`.`tag` as `away_tag`,
            `team_away`.`id` as `away_id`,
            `team_away`.`logo` as `away_logo`,
            `team_away`.`wins` as `away_wins`,
            `team_away`.`losses` as `away_losses`,
            `team_away`.`captain` as `away_captain`,
            `broadcaster`.`username` as `broadcaster_name`,
            `broadcaster`.`playerid` as `broadcaster_id`,
        FROM 
            `schedule` 
            LEFT JOIN `team` as `team_home` ON `schedule`.`home` = `team_home`.`id`
            LEFT JOIN `team` as `team_away` ON `schedule`.`away` = `team_away`.`id`
            LEFT JOIN `player` as `broadcaster` ON `broadcaster`.`playerid` = `schedule`.`stream`
        WHERE 
            `schedule`.`completed` = 0 AND `schedule`.`officialdate` > {$now} AND `schedule`.`game` = {$game}"
    );

    if ($db->iRecords()) {
        $offset = (3600 * $_SESSION['timezoneoffset']);
        $dateTime = new DateTime();
        $dateTime->setTimeZone(new DateTimeZone(timezone_name_from_abbr("", $offset,0)));

        foreach ($db->ArrayResults() as $match) {
            if (!$match['home_logo']) $match['home_logo'] = 0;
            if (!$match['away_logo']) $match['away_logo'] = 0;

            $match['time'] = date('g:ia',$match['scheduleddate']+$offset);
            $match['timezone'] = $dateTime->format('T'); 
            $match['offset'] = $offset;
            $match['date'] = date('l, M dS',$match['scheduleddate']+$offset);

            $return[] = $match;
        }

        $return = array('success' => true, 'matches' => $return);
    }
}
else
    $return['error'] = "Missing parameters";

header('Cache-Control: no-cache, must-revalidate');
returnJSON($return);
?>