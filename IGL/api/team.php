<?php
/**
 * Fetches team details
 * @author Darryl Allen <dallen@palyigl.com>
 * @author Shaun Delaney <sdelaney@playigl.com>
 * @return JSON team list
 * @copyright Copyright 2013 PlayIGL.com
 */

include_once('../includes/config.php');
include_once('../model/mysql.class.php');
include_once('../includes/functions.php');

//Default error message
$return = array("success" => false, "error" => "Invalid Request.");

//Variables
$id         = isset($_REQUEST['id']) ? $_REQUEST['id'] : null;

$db = new mysql(MYSQL_DATABASE);

if (!is_null($id) && is_numeric($id)) {
    $db = new mysql(MYSQL_DATABASE);

    $db->ExecuteSQL(
        "SELECT * FROM active_teams WHERE `team_id` = {$id}"
    );

    if ($db->iRecords()) {
        $result = $db->ArrayResult();
        $result['team_ladders'] = array();
        
        $db->ExecuteSQL("
                SELECT      
                    ladder_id,
                    ladder_name
                FROM 
                    active_ladders 
                LEFT JOIN 
                    `ladder_teams` ON `ladder_teams`.`tier_id` = `active_ladders`.`ladder_id` 
                WHERE 
                    `ladder_teams`.`team_id` = {$id}");
        if($db->iRecords())             
            $result['team_ladders'] = $db->ArrayResults();
    
        
        //Get # of followers for this team
        $db->ExecuteSQL(
            "SELECT id FROM ".ACTIVITY.".notifications WHERE subject_type = 'team' and subject_id = {$id} and actor_type IN ('player', 'team');"
        );
        $result['team_followers'] = $db->iRecords();

        //Query for the team roster
        $db->ExecuteSQL(
            "SELECT 
                `roster`.`player` as player_id,
                `player`.`username` AS player_username, 
                `player`.`firstname` AS player_firstname, 
                `player`.`lastname` AS player_lastname, 
                `player`.`communityid` AS player_community_id,
                `player`.`city` AS player_city, 
                `player`.`state` AS player_state, 
                `player`.`country` AS player_country, 
                `roster`.`joindate` AS player_joindate 
            FROM 
                `roster`
            JOIN 
                `player` ON `player`.`playerid` = `roster`.`player`
            WHERE 
                `roster`.`team` = {$id}"
        );
                
        $result['team_roster'] = array();

        if ($db->iRecords()) {
            $result['team_roster'] = $db->ArrayResults();

            //also display the steam ID for the roster players if community ID is available 
            foreach ($result['team_roster'] as $key => $value) {
                if ($value['player_community_id'])
                    $result['team_roster'][$key]['player_steam_id'] = convert64to32($value['player_community_id']);
            }
        }

        //If viewer is team captain, display the invites. (used in team builder)
        if(isLoggedIn() && in_array($_SESSION['playerid'],array($result['team_captain_id'],$result['team_alternate_id']))) {
            $db->ExecuteSQL(
                "SELECT
                    `inviterequests`.`player` as player_id,
                    `player`.`username` AS player_username, 
                    `player`.`firstname` AS player_firstname, 
                    `player`.`lastname` AS player_lastname, 
                    `player`.`communityid` AS player_community_id,
                    `player`.`city` AS player_city, 
                    `player`.`state` AS player_state, 
                    `player`.`country` AS player_country
                FROM 
                    `inviterequests`
                LEFT JOIN 
                    `player` ON `player`.`playerid` = `inviterequests`.`player`
                WHERE 
                    `inviterequests`.`team` = {$id}"
            );

            if ($db->iRecords())
                $result['team_invites'] = $db->ArrayResults();
        }
            
        //Gather comments for this team's profile
        $db->ExecuteSQL(
            "SELECT 
                `comments`.`id` as post_id, 
                `comments`.`dateposted` as post_date,
                `comments`.`posterid` as poster_id,
                `comments`.`comment` as poster_comment, 
                `player`.`avatar` as poster_avatar, 
                `player`.`username` as poster_username
            FROM 
                `comments`
            JOIN
                `player` ON `player`.`playerid` = `comments`.`posterid`
            WHERE 
                `comments`.`page` = 'team' AND `comments`.`pageid` = {$id} AND `comments`.`parentid` = 0 ORDER BY `comments`.`id` DESC"
        );

        $comments = array();

        if ($db->iRecords()) {
            $comments = $db->ArrayResults();

            foreach ($comments as $key => $comment) {
                $db->ExecuteSQL(
                    "SELECT 
                    `comments`.`id` as post_id, 
                    `comments`.`dateposted` as post_date,
                    `comments`.`posterid` as poster_id,
                    `comments`.`comment` as poster_comment, 
                    `player`.`avatar` as poster_avatar, 
                    `player`.`username` as poster_username 
                    FROM 
                        `comments`
                    JOIN
                        `player` ON `player`.`playerid` = `comments`.`posterid`
                    WHERE 
                        `comments`.`page` = 'team' AND `comments`.`pageid` = {$id} AND `comments`.`parentid` = {$comment['post_id']} ORDER BY `comments`.`id` ASC"
                );
                
                $comments[$key]['poster_comment'] = parseContent($comments[$key]['poster_comment']);
                $comments[$key]['post_replies'] = $db->ArrayResults();
                
                if (is_array($comments[$key]['post_replies'])) {
                  foreach($comments[$key]['post_replies'] as $reply) {
                    $reply['poster_comment'] = parseContent($reply['poster_comment']);
                  }
                }
            }
        }
        
        $return = array('success' => true, 'team' => $result, 'comments' => $comments);
    }
    else 
        $return['error'] = "Team not found";
}

header('Cache-Control: no-cache, must-revalidate');
returnJSON($return);
?>