<?php
/**
 * Fetches player data
 * @author Darryl Allen <dallen@palyigl.com>
 * @author Shaun Delaney <sdelaney@playigl.com>
 * @return JSON List of users
 * This file runs periodically to copy the playerid & Username from the IGL users DB to the IGL stats DB.
 * @copyright Copyright 2012-2013 PlayIGL.com
 */

include_once('../includes/config.php');
include_once('../model/mysql.class.php');
include_once('../includes/functions.php');

date_default_timezone_set('UTC');

$time = time() - (3600*24*2); // To speed up our query, we only gather users who have been online in the last 2 days

$db = new mysql(MYSQL_DATABASE);
$db->ExecuteSQL(
    "SELECT playerid, username, steamid, country FROM player WHERE lastactivity > {$time}"
);

header('Cache-Control: no-cache, must-revalidate');
returnJSON($db->ArrayResults());
?>