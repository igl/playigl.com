<?php
set_time_limit(60);
/*
 * @copyright 2012 PlayIGL.com
 * @author Darryl Allen
 * This file scans the HLStats DB for CS:GO stats for
 */

header('Cache-Control: no-cache, must-revalidate');
header('Content-type: application/json');

/*
define ("MYSQL_HOST", "stats-dev.cimukvvq20st.us-east-1.rds.amazonaws.com"); // MySQL Hostname
define ("MYSQL_USER", "stats_dev");	// MySQL Username
define ("MYSQL_PASS", "g3tRAg5D");	// MySQL Password
define ("MYSQL_DATABASE", "stats");	// MySQL Database
*/

define ("MYSQL_HOST", "localhost"); // MySQL Hostname
define ("MYSQL_USER", "root");	// MySQL Username
define ("MYSQL_PASS", "");	// MySQL Password
define ("MYSQL_DATABASE", "stats");	// MySQL Database

define ("BASEURL", "http://localhost/"); #Include trailing slash

require('../includes/functions.php');
require('../model/mysql.class.php');
date_default_timezone_set('UTC');

if(isset($_REQUEST['id']) && is_numeric($_REQUEST['id']) && isset($_REQUEST['playerid']) && is_numeric($_REQUEST['playerid']))
{
    $matchid = (int)$_REQUEST['id'];
    $playerid = (int)$_REQUEST['playerid'];
    $schedule = json_decode(file_get_contents(BASEURL."api/schedule/{$matchid}"),true);

    $db = new mysql(MYSQL_DATABASE);
    $date = $schedule['match']['officialdate'];
    $ip = $schedule['match']['ip'];
    $port = $schedule['match']['port'];
    
    $sql = "SELECT serverId FROM hlstats_servers WHERE address = '{$ip}' AND port = '{$port}' LIMIT 1";
    $db->ExecuteSQL($sql);
    $doc = $db->ArrayResult();
    $server_id = $doc['serverId'];
    
    $match_start = date('Y-m-d H:i:s',$date);
    $match_finish = date('Y-m-d H:i:s',$date + 7200);
    
    $sql = "SELECT eventTime FROM hlstats_events_playeractions WHERE eventTime > '{$match_start}' AND eventTime < '{$match_finish}' LIMIT 1";
   
    
    $db->ExecuteSQL($sql); //check if matches for this time exist
    if($db->iRecords())
    {
        //Include the scheudle in the output
        $round_results['schedule'] = $schedule['match'];
        
        //Get times each player was on a team.
        $sql = "SELECT eventTime, COUNT(eventTime) AS halftime FROM hlstats_events_changeteam WHERE eventTime > '{$match_start}' AND eventTime < '{$match_finish}' AND serverId = '{$server_id}' GROUP BY eventTime ORDER BY eventTime DESC LIMIT 1";
        $db->ExecuteSQL($sql); 
        $doc = $db->ArrayResult();
        $halftime = $doc['eventTime'];
        
        
        $half = array(
            'Round 1' => "eventTime > '{$match_start}' AND eventTime <= '{$halftime}' AND serverId = '{$server_id}'",
            'Round 2' => "eventTime > '{$halftime}' AND eventTime <= '{$match_finish}' AND serverId = '{$server_id}'"
            );

                 
        //Look-up IGL player info using community ID
        $player_details = json_decode(file_get_contents(BASEURL."api/player/{$playerid}"),true);
        if($player_details['success'])
        foreach($player_details['profile'] as $info => $data)
        {
            $player_data[$info] = $data;
        }
        //Get HLStats playerid from SteamID
        $sql = "SELECT playerId FROM hlstats_playeruniqueids WHERE uniqueId = '{$player_data['steam_id']}' LIMIT 1";
        $db->ExecuteSQL($sql);
        $doc = $db->ArrayResult();
        $player_data['playerId'] = $doc['playerId'];
        
        
        $result = $player_data;
        $sql = "SELECT playerid, lastName, username, country FROM hlstats_players WHERE playerId = '{$player_data['playerId']}' LIMIT 1";
        $db->ExecuteSQL($sql);
        if($db->iRecords())
        {
            $stat   = $db->ArrayResult();
        }

        $round_results['player'] = array_merge($player_data,$stat);
        
        
        foreach($half as $round => $filter)
        {
            unset($result); //Clear current results
            $result = array();
            
            //Get array of events (Bomb Plants / Defuses, Kill Streaks)
            $sql = "SELECT count(hlstats_events_playeractions.playerId) as count, description
            FROM hlstats_actions
            JOIN hlstats_events_playeractions ON hlstats_events_playeractions.actionId = hlstats_actions.id
            JOIN hlstats_players ON hlstats_players.playerId=hlstats_events_playeractions.playerId
            WHERE hlstats_events_playeractions.playerId = '{$player_data['playerId']}' AND {$filter}
            GROUP BY description, hlstats_events_playeractions.playerId";
            $db->ExecuteSQL($sql);
            $doc = $db->ArrayResults();
            $result['events'] = array();
            foreach($doc as $v)
            {
                $result['events'][$v['description']] = $v['count'];
            } 

            $sql = "SELECT eventtime, hlstats_players.username, hlstats_players.lastName as ingame_name, hlstats_players.playerid, hlstats_players.country, hlstats_playeruniqueids.uniqueId as steam_id, hlstats_weapons.code as weapon_code, hlstats_weapons.name as weapon_name, headshot
                    FROM hlstats_events_frags
                    JOIN hlstats_weapons 
                            ON hlstats_events_frags.weapon=hlstats_weapons.code
                    JOIN hlstats_playeruniqueids 
                            ON hlstats_events_frags.victimId = hlstats_playeruniqueids.playerId
                    JOIN hlstats_players
                            ON hlstats_players.playerId =  hlstats_playeruniqueids.playerId
                    WHERE hlstats_weapons.game='csgo' AND killerId = '{$player_data['playerId']}' AND {$filter}";
            $db->ExecuteSQL($sql);
            $result['kills'] = array();
            if($db->iRecords())
                $result['kills'] = $db->ArrayResults();
                


            $sql = "SELECT eventtime, hlstats_players.username, hlstats_players.lastName as ingame_name, hlstats_players.playerid, hlstats_players.country, hlstats_playeruniqueids.uniqueId as steam_id, hlstats_weapons.code as weapon_code, hlstats_weapons.name as weapon_name, headshot
                    FROM hlstats_events_frags
                    JOIN hlstats_weapons 
                            ON hlstats_events_frags.weapon=hlstats_weapons.code
                    JOIN hlstats_playeruniqueids 
                            ON hlstats_events_frags.killerId = hlstats_playeruniqueids.playerId
                    JOIN hlstats_players
                            ON hlstats_players.playerId =  hlstats_playeruniqueids.playerId
                    WHERE hlstats_weapons.game='csgo' AND victimId = '{$player_data['playerId']}' AND {$filter}";    

            $db->ExecuteSQL($sql);
            $result['deaths'] = array();
            if($db->iRecords())
                $result['deaths'] = $db->ArrayResults();
            
            $round_results['round'][$round] = $result;
        }
    }
    else
    {
        $round_results = array('success' => false, 'message' => "That match could not be found.");
    }
}
else
{
    $round_results = array('success' => false, 'message' => "Some parameters appear to be missing.");
}

prettyJSON(json_encode($round_results));
?>