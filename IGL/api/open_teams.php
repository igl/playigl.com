<?php
/**
 * Fetches actively recruiting teams
 * @author Darryl Allen <dallen@palyigl.com>
 * @author Shaun Delaney <sdelaney@playigl.com>
 * @return JSON teams list*
 * @copyright Copyright 2013 PlayIGL.com
 */

include_once('../includes/config.php');
include_once('../model/mysql.class.php');
include_once('../includes/functions.php');

//Default error message
$return = array("success" => false, "error" => "Invalid Request.");

//Variables
$id         = isset($_REQUEST['id']) ? $_REQUEST['id'] : null;

$db = new mysql(MYSQL_DATABASE);

if (!is_null($id) && is_numeric($id)) {
    $db->ExecuteSQL(
        "SELECT
            `game`.`id` as game_id,
            `game`.`name` as game_name,
            `game`.`banner` as game_banner
        FROM
            `game`
        WHERE
            `game`.`id` = {$id}"
    );

    if ($db->iRecords()) {
        $game = $db->ArrayResult();

        $db->ExecuteSQL(
            "SELECT
                team_id,
                team_game_id,
                team_name,
                team_continent,
                team_region,
                team_logo
            FROM
                `active_teams`
            WHERE 
                `team_open` = 1 AND `team_game_id` = {$id}"
        );

        if ($db->iRecords()) {
            $openTeams = $db->ArrayResults();
            
            foreach ($openTeams as $k => $t) {
                $db->ExecuteSQL(
                    "SELECT `id` FROM `roster` WHERE `roster`.`team` = {$t['team_id']}"
                );

                $openTeams[$k]['team_size'] = $db->iRecords();
            }

            $return = array('success'=> true, 'game' => $game, 'team' => $openTeams);
        }
        else
            $return = array('success' => false, 'game' => $game, 'error' => 'There are no teams looking for players.');
    }
    else
        $return['error'] = "Game not found.";
}

header('Cache-Control: no-cache, must-revalidate');
returnJSON($return);
?>