<?php
/**
 * Fetches search results for players/teams
 * @author Darryl Allen <dallen@palyigl.com>
 * @author Shaun Delaney <sdelaney@playigl.com>
 * @return HTML results
 * @copyright Copyright 2013 PlayIGL.com
 */

include_once('../includes/config.php');
include_once('../model/mysql.class.php');
include_once('../includes/functions.php');

//Variables
$q         = isset($_REQUEST['q']) ? $_REQUEST['q'] : null;

if (!is_null($q)) {
    if (preg_match('/[a-zA-Z0-9 ]/', $q)) {
        $db = new mysql(MYSQL_DATABASE);

        $player_results = array();
        $team_results = array();
        $league_results = array();
        $clean_query = mysql_real_escape_string($q);

        echo "<table class='table table-condensed table-striped table-hover'>";

        //#Add news & article search support (+forum topics)
        $db->ExecuteSQL(
            "SELECT avatar, playerid as player_id, username as player_username, firstname, lastname, country, city, state FROM player WHERE username LIKE '%{$clean_query}%' OR firstname LIKE '%{$clean_query}%' OR lastname LIKE '%{$clean_query}%' ORDER BY username ASC LIMIT 25"
        );

        if ($db->iRecords()) {
            echo "<tr><td><h4>Players</h4></td></tr>";
            
            foreach ($db->ArrayResults() as $key => $value) {
                if($value['country']=='')
                    $value['country']='missing';

                echo "<tr><td><a href='/player/{$value['player_username']}'><div><img width='40px' height='40px' class='img-rounded pull-left' style='margin-right: 6px;' src='/img{$value['avatar']}'><h6> <img src='/img/flags/{$value['country']}.png'> {$value['firstname']} {$value['player_username']} {$value['lastname']}<br>
                    <small>{$value['city']} {$value['state']}</small></h6></div></a></td></tr>";
            }
        }

        $db->ExecuteSQL(
            "SELECT game.id AS game_id, game.name as game_name, game.logo as game_logo, league.name as league_name, league.id as league_id FROM league JOIN game on league.game=game.id WHERE game.name LIKE '%{$clean_query}%' OR league.name LIKE '%{$clean_query}%' ORDER BY game.name ASC LIMIT 25"
        );

        if ($db->iRecords()) {
            echo "<tr><td><h4>Leagues</h4></td></tr>";

            foreach ($db->ArrayResults() as $key => $value) {
                echo "<tr><td><a href='/league/{$value['league_id']}-{$value['league_name']}'><div><img width='40px' height='40px' class='img-rounded pull-left' style='margin-right: 6px;' src='/img{$value['game_logo']}'> <h6>{$value['league_name']}<br>
                    <small>{$value['game_name']}</small></h6></div></a></td></tr>";
            }
        }

        $db->ExecuteSQL(
            "SELECT 
                game.id as game_id, 
                game.name as game_name,
                league.id as league_id,
                league.name as league_name,
                team.id as team_id,
                team.name as team_name,
                team.logo as team_logo,
                roster.player as player_id,
                player.username as player_username

            FROM team 
                JOIN roster ON team.id  =  roster.team
                JOIN player ON player.playerid = roster.player
                JOIN game ON game.id = roster.game
                LEFT JOIN league ON league.game = game.id

            WHERE
                team.deleted = 0 AND
                team.id > 0 AND
                (
                team.name LIKE '%{$clean_query}%' OR
                player.username LIKE '%{$clean_query}%' OR
                game.name LIKE '%{$clean_query}%' OR
                league.name LIKE '%{$clean_query}%'
                )
                GROUP BY team.name ASC LIMIT 25"
        );

        if ($db->iRecords()) {
            echo "<tr><td><h4>Teams</h4></td></tr>";

            foreach ($db->ArrayResults() as $key => $value) {
                echo "<tr><td><a href='/team/{$value['team_id']}-{$value['team_name']}'><div><img width='40px' height='40px' class='img-rounded pull-left' style='margin-right: 6px;' src='/img{$value['team_logo']}'> <h6>{$value['team_name']}<br>
                    <small>{$value['league_name']}<br>
                    {$value['game_name']}</small></h6></div></a></td></tr>";
            }
        }

        $db->ExecuteSQL(
            "SELECT news.title as news_title, news.id as news_id FROM news
            WHERE title LIKE '%{$clean_query}%' OR content LIKE '%{$clean_query}%'"
        );

        if ($db->iRecords()) {
            echo "<tr><td><h4>News</h4></td></tr>";
            
            foreach ($db->ArrayResults() as $key => $value) {
                $title = truncate($value['news_title'], 50);
                echo "<tr><td><a href='/news/{$value['news_id']}-{$value['news_title']}'><div><h6>{$title}</h6></div></a></td></tr>";
            }
        }

        $db->ExecuteSQL(
            "SELECT article.banner as image, article.title as article_title, article.id as article_id FROM article
            WHERE title LIKE '%{$clean_query}%' OR content LIKE '%{$clean_query}%'"
        );

        if($db->iRecords()) {
            echo "<tr><td><h4>Articles</h4></td></tr>";
            
            foreach ($db->ArrayResults() as $key => $value)
                echo "<tr><td><a href='/article/{$value['article_id']}-{$value['article_title']}'><div><img width='272px' height='80px' class='img-rounded' src='/img{$value['image']}'><br /><h6>{$value['article_title']}</h6></div></a></td></tr>";
        }
        
        echo "</table>";
    }
    else
        echo 'Your search contains invalid characters.';  
}
else
    echo 'If you search for notding, you will always find it.';
?>