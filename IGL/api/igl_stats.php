<?php
/**
 * Fetches player or team history
 * @author Darryl Allen <dallen@palyigl.com>
 * @author Shaun Delaney <sdelaney@playigl.com>
 * @return JSON history list
 * @copyright Copyright 2013 PlayIGL.com
 */

include_once('../includes/config.php');
include_once('../model/mysql.class.php');
include_once('../includes/functions.php');

function getStatCount(&$db, $sql) {
    $db->ExecuteSQL($sql);
    return $db->iRecords();
}

$db = new mysql(MYSQL_DATABASE);

//# Global Stats
$return = array();
$return['num_leagues'] = 0;
$return['num_freeagents'] = 0;
$return['num_teams'] = 0;
$return['num_players'] = 0;

$return['num_registered'] = getStatCount($db, "SELECT playerid FROM player;");  //# Registered Users
$return['num_referrals'] = getStatCount($db, "SELECT id FROM referral;");   //# Referrals Sent
$return['num_referrals_converted'] = getStatCount($db, "SELECT id FROM referral WHERE activated = 1;"); //# Referrals Converted
$return['num_new24h'] = getStatCount($db, "SELECT playerid FROM player WHERE joindate > '".(time() - (60*60*24))."'");  //# New users last 24hrs
$return['num_new7d'] = getStatCount($db, "SELECT playerid FROM player WHERE joindate > '".(time() - (60*60*24*7))."'"); //# New users last 7 days
$return['num_new30d'] = getStatCount($db, "SELECT playerid FROM player WHERE joindate > '".(time() - (60*60*24*30))."'");   //# New users last 30 days
$return['num_online'] = getStatCount($db, "SELECT playerid FROM player WHERE lastactivity > '".(time() - (60*60*24))."'");  //# Online last 24hrs

//Per game stats
$db->ExecuteSQL(
    "SELECT id as game_id, name as game_name, logo as logo FROM game WHERE status = 1"
);

if ($db->iRecords()) {
    $games = $db->ArrayResults();
    foreach ($games as $key => $value) {
        $game_id = $value['game_id'];
        $return['game'][$game_id] = $value;
        $return['game'][$game_id]['num_teams'] = 0; //initialize value
        $return['game'][$game_id]['num_players'] = 0; //initialize value
        
        $db->ExecuteSQL(
            "SELECT league.id as league_id, league.name as league_name, game.icon as icon FROM league JOIN game ON game.id = league.game WHERE league.game = '{$game_id}' AND league.status = 1 ORDER BY league.order"
        );
            
        if($db->iRecords()) {
            $leagues = $db->ArrayResults();
            $return['game'][$game_id]['num_leagues'] = count($leagues);  //total of all leagues for a game
            $return['num_leagues'] = $return['num_leagues'] + count($leagues); //running total of all leagues for all games
            
            $db->ExecuteSQL(
                "SELECT COUNT(player) as freeagents FROM freeagents WHERE game='$game_id'"
            );
            
            $agents = $db->ArrayResult();
            $return['game'][$game_id]['num_freeagents'] = $agents['freeagents'];  //total # free agents for a game
            $return['num_freeagents'] = $return['num_freeagents'] + $agents['freeagents'];
            
            foreach ($leagues as $key => $value) {
                $league_id = $value['league_id'];
                $return['game'][$game_id]['leagues'][$league_id] = $value;
                
                $db->ExecuteSQL(
                    "SELECT id as team_id, name as team_name, wins as team_wins, losses as team_losses FROM team WHERE league = '{$league_id}' AND status = 1"
                );
                
                if ($db->iRecords()) {
                    $teams = $db->ArrayResults();
                    foreach ($teams as $key => $value) {
                        $team_id = $value['team_id'];
                        $return['game'][$game_id]['leagues'][$league_id]['teams'][$team_id] = $value;
                        $return['game'][$game_id]['leagues'][$league_id]['num_players'] = 0; //initialize value
                        
                        $db->ExecuteSQL(
                            "SELECT roster.player as player_id, player.username as player_username FROM roster JOIN player ON player.playerid=roster.player WHERE team = '{$team_id}'"      
                        );
                        
                        if ($db->iRecords()) {
                            $roster = $db->ArrayResults();
                            $return['game'][$game_id]['leagues'][$league_id]['teams'][$team_id]['num_players'] = count($roster); //Total players on a team
                            $return['game'][$game_id]['leagues'][$league_id]['num_players'] = $return['game'][$game_id]['leagues'][$league_id]['num_players'] + count($roster); //Total players in a league
                            $return['game'][$game_id]['num_players'] = $return['game'][$game_id]['num_players'] + count($roster); //Total players for a game
                            $return['game'][$game_id]['leagues'][$league_id]['teams'][$team_id]['roster'] = $roster; //List of players on a team
                            $return['num_players'] = $return['num_players'] + count($roster); //Running total of all players on a roster for all games
                        }
                    }
                    
                    $return['game'][$game_id]['leagues'][$league_id]['num_teams'] = count($teams); //Total teams in a league
                    $return['game'][$game_id]['num_teams'] = $return['game'][$game_id]['num_teams'] + count($teams); //Total teams in a game
                    $return['num_teams'] = $return['num_teams'] + count($teams); //Running total of all teams for all games
                }
            }
        }
    }
    
    $return['num_games'] = count($games); // Total number of games
}

header('Cache-Control: no-cache, must-revalidate');
returnJSON($return);
?>