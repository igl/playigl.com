<?php
/**
 * Fetches ladder details
 * @author Darryl Allen <dallen@palyigl.com>
 * @author Shaun Delaney <sdelaney@playigl.com>
 * @return JSON ladder list
 * @copyright Copyright 2013 PlayIGL.com
 */

include_once('../includes/config.php');
include_once('../model/mysql.class.php');
include_once('../includes/functions.php');

//Default error message
$return = array("success" => false, "error" => "Invalid Ladder Request.");

//Variables
$id         = isset($_REQUEST['id']) ? $_REQUEST['id'] : null;
$fetch      = isset($_REQUEST['fetch']) ? strtolower($_REQUEST['fetch']) : null;

$db = new mysql(MYSQL_DATABASE);

if (!is_null($fetch) && in_array($fetch, array('all'))) {
    $db->ExecuteSQL("SELECT * FROM active_ladders");

    if ($db->iRecords())
        $return = array('success' => true, 'ladders' => $db->ArrayResults());
    else
        $return['error'] = "There are no active ladders.";
}
elseif (!is_null($id) && is_numeric($id)) {
    $db->ExecuteSQL("SELECT * FROM active_ladders WHERE ladder_id = {$id}");

    if ($db->iRecords()) {
        $ladder = $db->ArrayResult();
        /*
         
        $ladder['ladder_maps'] = array();

        //Get the maps for this ladder
        $db->ExecuteSQL("SELECT poolarray FROM mappools WHERE id = {$ladder['ladder_mappool']}");

        if ($db->iRecords()) {
            $map_list = $db->ArrayResult();
            $pool_array = rtrim($map_list['poolarray'],',');

            $db->ExecuteSQL(
                "SELECT map FROM maplist WHERE id IN ({$pool_array})"
            );
            
            if($db->iRecords())
                $ladder['ladder_maps'] = $db->ArrayResults();
        }
         */

        $db->ExecuteSQL(
            "SELECT
                `active_teams`.`team_id`, 
                `active_teams`.`team_name`, 
                `active_teams`.`team_continent`,
                `active_teams`.`team_region`,
                `active_teams`.`team_tag`, 
                `active_teams`.`team_rpi`, 
                `ladder_teams`.`current_points` AS team_points, 
                `ladder_teams`.`highest_points` AS team_highest_points, 
                `active_teams`.`team_facebook`, 
                `active_teams`.`team_twitter`, 
                `active_teams`.`team_steamgroup`, 
                `active_teams`.`team_website`, 
                `active_teams`.`team_captain_id`, 
                `active_teams`.`team_captain_name`, 
                `active_teams`.`team_alternate_id`, 
                `active_teams`.`team_alternate_name`, 
                `active_teams`.`team_logo`, 
                `active_teams`.`team_created`, 
                `active_teams`.`team_biography`,
                (SELECT count(id) FROM `schedule` s WHERE `ladder` = {$id} AND `winner` = `active_teams`.`team_id`) as team_ladder_wins,
                (SELECT count(id) FROM `schedule` s WHERE `ladder` = {$id} AND `loser` =  `active_teams`.`team_id`) as team_ladder_loses
            FROM
                `active_teams`
            JOIN 
                `ladder_teams` ON `ladder_teams`.`team_id` = `active_teams`.`team_id`
            WHERE
                `ladder_teams`.`tier_id` = {$id}
            ORDER BY team_points DESC"
        );

        $ladder['ladder_teams'] = $db->ArrayResults();

        //Check if this is the lowest ranked ladder. Teams can only join the lowest ranked ladder
        $db->ExecuteSQL(
            "SELECT max(`id`) as lowest_ladder FROM ladder WHERE ladder.status = 1 AND ladder.settings = (SELECT settings FROM ladder WHERE id = {$id})"
        );

        $r = $db->ArrayResult();
        $ladder['ladder_entry_level'] = ($r['lowest_ladder'] == $ladder['ladder_id']) ? true : false;

        if (isLoggedIn()) {
            //check if the user is a team captain for a team for a specific game
            $db->ExecuteSQL(
                "SELECT 
                    `team_id` 
                FROM 
                    `active_teams`
                WHERE
                    `team_game_id` = {$ladder['ladder_game_id']}
                AND {$_SESSION['playerid']} IN (`team_captain_id`,`team_alternate_id`)"
            );

            $ladder['ladder_captain'] = $db->iRecords() ? true : false;

            $ladder['ladder_participating'] = false;
            // Check if the user viewing, is the team captain of any participating team
            foreach ($ladder['ladder_teams'] as $i => $team) {
                //set flags
                if( substr($ladder['ladder_teams']['team_continent'], 0,2) == 'EU' )
                    $ladder['ladder_teams'][$i]['team_flag'] = 'europeanunion.png';
                else
                    $ladder['ladder_teams'][$i]['team_flag'] = 'us.png';
                if(in_array($_SESSION['playerid'], array($team['team_captain_id'],$team['team_alternate_id'])))
                    $ladder['ladder_participating'] = true;
            }
        }
        
        //Get sister tiers for this ladder
        $db->ExecuteSQL("
            SELECT 
            ladder_id,
            ladder_game_name,
            ladder_name
            FROM 
            active_ladders
            WHERE 
		ladder_format = (SELECT ladder_format FROM active_ladders WHERE ladder_id = {$id}) AND
		ladder_game_id = (SELECT ladder_game_id FROM active_ladders WHERE ladder_id = {$id})
            ");
            $tiers = array();
            if( $db->iRecords() )
                $tiers = $db->ArrayResults();
        
        $return = array('success' => true, 'ladder' => $ladder, 'tiers' => $tiers);
    }
    else
        $return['error'] = "That ladder either does not exist, or is not active.";
}

header('Cache-Control: no-cache, must-revalidate');
returnJSON($return);
?>