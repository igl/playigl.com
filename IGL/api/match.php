<?php

/**
 * Fetches ladder details
 * @author Darryl Allen <dallen@palyigl.com>
 * @author Shaun Delaney <sdelaney@playigl.com>
 * @return JSON ladder list
 * @copyright Copyright 2013 PlayIGL.com
 */
include_once('../includes/config.php');
include_once('../model/mysql.class.php');
include_once('../includes/functions.php');

$db = new MySQL();

if (isset($_REQUEST['match_id']) && is_numeric($_REQUEST['match_id']) && isset($_REQUEST['hash']) && isset($_REQUEST['username'])) {
    //Verify user is legit
    $db->ExecuteSQL("SELECT salt FROM player WHERE username = '{$_REQUEST['username']}'");
    if ($db->iRecords()) {
        $r = $db->ArrayResult();
        $hash = md5($_REQUEST['match_id'] . $r['salt']);
        if ($hash == $_REQUEST['hash']) {
            $db->ExecuteSQL("SELECT
	`game`.`name` AS game_name,
	`ip` AS server_ip,
	`port` AS server_port
        FROM
                `schedule`
        LEFT JOIN `game` ON `game`.`id` = `schedule`.game
        LEFT JOIN `matchreserve` ON `matchreserve`.`id` = `schedule`.`reservation_id`
        LEFT JOIN `matchservers` ON `matchservers`.`id` = `matchreserve`.`server_id`
        WHERE `schedule`.`id` = 46");
            if ($db->iRecords()) {
                $return = array('success' => true);
            } else {
                $return = array('success' => false, 'error' => 'Unable to locate match.');
            }
        } else {
            $return = array('success' => false, 'error' => 'Hash does not match');
        }
    } else {
        $return = array('success' => false, 'error' => 'Unable to locate user');
    }
} else {
    $return = array('success' => false, 'error' => 'Match ID is missing or invalid.');
}

header('Cache-Control: no-cache, must-revalidate');
returnJSON($return);
?>