<?php
/**
 * Fetches search results for players/teams
 * @author Darryl Allen <dallen@palyigl.com>
 * @author Shaun Delaney <sdelaney@playigl.com>
 * @return HTML results
 * @copyright Copyright 2013 PlayIGL.com
 */

include_once('../includes/config.php');
include_once('../model/mysql.class.php');
include_once('../includes/functions.php');

//Variables
$q         = isset($_REQUEST['q']) ? $_REQUEST['q'] : null;

if (!is_null($q)) {
    if(preg_match('/[a-zA-Z0-9 ]/', $q)) {
        $db = new mysql(MYSQL_DATABASE);
        $player_results = array();
        $team_results = array();
        $league_results = array();
        $clean_query = mysql_real_escape_string($q);
        
        echo "<ul id='sortable2' class='ui-sortable'>";

        $db->ExecuteSQL(
            "SELECT playerid as player_id, username as player_username, firstname as player_firstname, lastname as player_lastname, country, city, state, communityid as community_id FROM player WHERE username LIKE '%{$clean_query}%' OR firstname LIKE '%{$clean_query}%' OR lastname LIKE '%{$clean_query}%' ORDER BY username ASC LIMIT 25"
        );

        if ($db->iRecords()) {
            foreach ($db->ArrayResults() as $key => $player) {
                $player['steam_id'] = convert64to32($player['community_id']);

                if($player['country']=='')
                    $player['country']='missing';

                echo "<li class='ui-state-default'><input name='invite[]' type='hidden' value='{$player['player_id']}'><img src='/img/flags/{$player['country']}.png'> <a target='_blank' href='/player/{$player['player_username']}'>{$player['player_username']}</a><br>{$player['player_firstname']} {$player['player_lastname']}<br>{$player['city']} {$player['state']} <br> <a target='_blank' href='http://steamcommunity.com/profiles/{$player['community_id']}'>{$player['steam_id']}</a></li>";
            }
        }
        
        echo "</ul>";
    }
    else
        echo 'Your search contains invalid characters.';
}
else
    echo 'If you search for notding, you will always find it.';
?>