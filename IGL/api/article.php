<?php
/**
 * Fetches articles
 * @author Darryl Allen <dallen@palyigl.com>
 * @author Shaun Delaney <sdelaney@playigl.com>
 * @return JSON List of articles
 * @copyright Copyright 2012-2013 PlayIGL.com
 */

include_once('../includes/config.php');
include_once('../model/mysql.class.php');
include_once('../includes/functions.php');

$return = array("success" => false, "error" => "Generic error.");
$db = new mysql(MYSQL_DATABASE);

if(isset($_REQUEST['id']) && is_numeric($_REQUEST['id'])) {    
    $article_id = $_REQUEST['id'];
    $db->ExecuteSQL(
        "SELECT a.*, p.username AS author FROM article a JOIN player p ON p.playerid = a.author_id WHERE a.id = '{$article_id}'"
    );
    
    if ($db->iRecords())
    {
        $data = $db->ArrayResult();
        //Grab comment data
        $query = "SELECT 
                `comments`.`id` as post_id, 
                `comments`.`dateposted` as post_date,
                `comments`.`posterid` as poster_id,
                `comments`.`comment` as poster_comment, 
                `player`.`avatar` as poster_avatar, 
                `player`.`username` as poster_username
            FROM 
                `comments`
            JOIN
                `player` ON `player`.`playerid` = `comments`.`posterid`
            WHERE 
                `comments`.`page` = 'article' AND `comments`.`pageid` = {$article_id} AND `comments`.`parentid` = 0 ORDER BY `comments`.`id` DESC";

        $comments = array();
        if( $db->ExecuteSQL($query) && $db->iRecords() )
        {
            $comments = $db->ArrayResults();
            foreach ($comments as $key => $comment)
            {
                $query = "SELECT 
                        `comments`.`id` as post_id, 
                        `comments`.`dateposted` as post_date,
                        `comments`.`posterid` as poster_id,
                        `comments`.`comment` as poster_comment, 
                        `player`.`avatar` as poster_avatar, 
                        `player`.`username` as poster_username 
                    FROM 
                        `comments`
                    JOIN
                        `player` ON `player`.`playerid` = `comments`.`posterid`
                    WHERE 
                        `comments`.`page` = 'article' AND `comments`.`pageid` = {$article_id} AND `comments`.`parentid` = {$comment['post_id']} ORDER BY `comments`.`id` ASC";
                $db->ExecuteSQL($query);
                
                $comments[$key]['poster_comment'] = parseContent($comments[$key]['poster_comment']);
                $comments[$key]['post_replies'] = $db->ArrayResults();

                if (is_array($comments[$key]['post_replies'])) {
                  foreach($comments[$key]['post_replies'] as $reply) {
                    $reply['poster_comment'] = parseContent($reply['poster_comment']);
                  }
                }
            }
        }
        $return = array('success' => true, 'article' => $data, 'comments' => $comments);
    }
    else
        $return['error'] = "Sorry, data could not be compiled";
} else {
    $db->ExecuteSQL(
        "SELECT * FROM article"
    );
    $data = $db->ArrayResults();
    
    
    
    
    
    if ($db->iRecords())
        $return = array('success' => true, 'article' => $data);
    else
        $return['error'] = "Sorry, data could not be compiled";
}

header('Cache-Control: no-cache, must-revalidate');
returnJSON($return);