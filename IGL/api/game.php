<?php
/**
 * Fetches game and league data
 * @author Darryl Allen <dallen@palyigl.com>
 * @author Shaun Delaney <sdelaney@playigl.com>
 * @return JSON game and league lists
 * @copyright Copyright 2013 PlayIGL.com
 */

include_once('../includes/config.php');
include_once('../model/mysql.class.php');
include_once('../includes/functions.php');

//Default error message
$return = array("success" => false, "error" => "Invalid Game.");

//Variables
$game_id    = isset($_REQUEST['id']) ? $_REQUEST['id'] : null;
$fetch      = isset($_REQUEST['fetch']) ? $_REQUEST['fetch'] : null;

$db = new mysql(MYSQL_DATABASE);

if (!is_null($game_id) && is_numeric($game_id)) {
    if (!is_null($fetch) && $fetch == "teams") {        
        $db->ExecuteSQL(
            "SELECT id, name FROM `team` WHERE `status` = 1 AND `game` = {$game_id}"
        );
        
        if ($db->iRecords())
            $return = array('success' => true, 'teams' => $db->ArrayResults());
    } else {
        $db->ExecuteSQL(
            "SELECT * FROM `active_games` WHERE `game_id` = {$game_id}"
        );
            
        if ($db->iRecords()) {
            $games = $db->ArrayResult();
            $games['game_leagues'] = array();
            $games['game_ladders'] = array();

            $db->ExecuteSQL(
                "SELECT * FROM `active_leagues` WHERE `league_game_id` = {$games['game_id']}"
            );

            if ($db->iRecords())
                $games['game_leagues'] = $db->ArrayResults();

            $db->ExecuteSQL(
                "SELECT * FROM `active_ladders` WHERE `ladder_game_id` = {$games['game_id']}"
            );

            if ($db->iRecords())
                $games['game_ladders'] = $db->ArrayResults();
        
            $return = array('success' => true, 'game' => $games);
        } else
            $return = array('success' => true, 'error' => "No active games found.");
    }
} else {
    $num = array();
    $db->ExecuteSQL(
            "SELECT count(`playerid`) AS num_users FROM player WHERE deleted = 0"
        );
        
        $r = $db->ArrayResult();
        $num['num_users'] = $r['num_users'];
        
    $db->ExecuteSQL(
        "SELECT * FROM `active_games`"
    );
    
    if ($db->iRecords()) {
        $games = $db->ArrayResults();

        $num['num_games'] = $db->iRecords();
        $num['num_teams'] = 0;
        $num['num_players'] = 0;
        
        

        $db->ExecuteSQL(
            "SELECT 
                count(DISTINCT(league.id)) + count(DISTINCT(ladder_settings.id)) + count(DISTINCT(tournaments.id)) as divisions
            FROM 
                `tournaments`, 
                `league`,
                `ladder_settings`
            WHERE
                league.status = 1 AND
                league.deleted = 0 AND
                ladder_settings.status = 1 AND
                ladder_settings.deleted = 0 AND
                tournaments.status = 1"
        );
        
        $d = $db->ArrayResult();
        $num['num_divisions'] = $d['divisions'];

        foreach ($games as $k => $g)
        {
            //Get all the leagues for a specific game
            $db->ExecuteSQL(
              "SELECT * FROM active_leagues WHERE league_game_id = {$g['game_id']}"      
            );

            $game_leagues = $db->ArrayResults();

            foreach ($game_leagues as $i => $league_data) {
                //Get teams for each league/division
                $db->ExecuteSQL(
                    "SELECT * FROM active_teams WHERE `team_league_id` = {$league_data['league_id']} ORDER BY `team_league_name` DESC"
                );

                $game_leagues[$i]['league_teams'] = $db->ArrayResults();
            }

            //Get all the ladders for a specific game
            $db->ExecuteSQL(
                "SELECT * FROM active_ladders WHERE ladder_game_id = {$g['game_id']}"
            );
                
            $game_ladders = $db->ArrayResults();

            foreach ($game_ladders as $i => $ladder_data) {
                //Get teams for each league/divisionS
                $db->ExecuteSQL(
                    "SELECT * FROM active_teams JOIN ladder_teams ON ladder_teams.team_id = active_teams.team_id WHERE ladder_teams.tier_id = {$ladder_data['ladder_id']} "
                );

                $game_ladder[$i]['ladder_teams'] = $db->ArrayResults();
            }

            //Get number of players registered in a game
            $db->ExecuteSQL(
                "SELECT count(`roster`.`id`) as players FROM `roster` WHERE `game` = {$g['game_id']}"
            );
                
            $r = $db->ArrayResult();
            
            $games[$k]['game_num_players'] = number_format($r['players']);
            $num['num_players'] = number_format($num['num_players'] + $games[$k]['game_num_players']);

            $db->ExecuteSQL("SELECT `id` FROM `team` WHERE `status` = 1 AND `game` = {$g['game_id']}");
            $games[$k]['game_num_teams'] = number_format($db->iRecords());
            $games[$k]['game_num_players'] = $r['players'];
            $num['num_teams'] = number_format($num['num_teams'] + $games[$k]['game_num_teams']);

            $games[$k]['game_leagues'] = $game_leagues;
            $games[$k]['game_ladders'] = $game_ladders;
        }
        
        $return = array('success' => true, 'stats' => $num, 'game' => $games);
    }
}

header('Cache-Control: no-cache, must-revalidate');
returnJSON($return);
?>