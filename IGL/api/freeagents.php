<?php
/**
 * Fetches free agents
 * @author Darryl Allen <dallen@palyigl.com>
 * @author Shaun Delaney <sdelaney@playigl.com>
 * @return JSON free agent list
 * @copyright Copyright 2013 PlayIGL.com
 */

include_once('../includes/config.php');
include_once('../model/mysql.class.php');
include_once('../includes/functions.php');

//Default error message
$return = array("success" => false, "error" => "Invalid Game.");

//Variables
$game_id    = $_REQUEST['game'];
$game_data  = false;
$team_data  = false;
$on_game_team    = false;
$is_agent   = false;

//Reject non-numeric IDs
if (is_numeric($game_id)) {
    $db = new mysql(MYSQL_DATABASE);

    //Save Game Data
    $db->ExecuteSQL(
        "SELECT 
            `game`.`id` as game_id,
            `game`.`name` as game_name,
            `game`.`steamauth` as game_name_steamauth,
            `game`.`about` as game_about,
            `game`.`teamsize` as game_teamsize,
            `game`.`icon` as game_icon,
            `game`.`logo` as game_logo,
            `game`.`banner` as game_banner
        FROM 
            `game`
        WHERE 
            `game`.`id` = {$game_id}"
    );    

    $game_data = $db->ArrayResult();

    //If logged in, save team data
    if (isLoggedIn()) {
        $db->ExecuteSQL(
            "SELECT 
                `team`.`id` as team_id
            FROM 
                `team` 
            WHERE 
                `team`.`game`= {$game_id} 
            AND (`team`.`captain` = {$_SESSION['playerid']} OR `team`.`alternate` = {$_SESSION['playerid']} )"
        );

        $team_data = $db->iRecords() ? $db->ArrayResult() : false;
    }

    //Grab Free Agent data
    $db->ExecuteSQL(
        "SELECT 
            player.playerid as player_id,
            player.avatar as player_avatar,
            freeagents.reason as player_reason,
            player.username as player_username, 
            player.firstname as player_firstname, 
            player.lastname as player_lastname, 
            player.city as player_city, 
            player.state as player_state, 
            player.country as player_country, 
            player.communityid as player_community_id,
            freeagents.reason as player_reason,
            freeagents.joindate as player_joindate 
        FROM 
            freeagents 
        JOIN
            player 
        ON 
            player.playerid=freeagents.player 
        WHERE 
            freeagents.game = {$game_id}
            ORDER BY freeagents.id DESC"
    );
    
    if ($db->iRecords()) {
        $doc = $db->ArrayResults();

        //Go through all agents
        foreach($doc as $key => $value)
        {
            if (is_array($team_data)) {
                //Check if you have sent this player an invite
                $db->ExecuteSQL(
                    "SELECT `id` FROM `inviterequests` 
                    WHERE `player` = {$doc[$key]['player_id']} AND team = {$team_data['team_id']}"
                );

                $doc[$key]['player_invited'] = $db->iRecords() ? true : false;
            }

            $doc[$key]['player_steam_id'] = convert64to32($value['player_community_id']);
            
            if ($value['player_country'])
                $doc[$key]['player_flag'] = "/img/flags/".strtolower($value['player_country']).".png";
            else
                $doc[$key]['player_flag'] = "/img/flags/missing.png";

            $doc[$key]['player_joindate'] = strtotime($value['player_joindate']);
        }
        
        //Grab team data if logged in
        if (isLoggedIn()) {
            $db->ExecuteSQL(
                "SELECT
                    `roster`.`id`
                FROM
                    `roster`
                WHERE
                    `roster`.`player` = {$_SESSION['playerid']}
                AND
                    `roster`.`game` = {$game_id}"
            );

            $on_game_team = $db->iRecords() ? true : false;

            $db->ExecuteSQL(
                "SELECT
                    `freeagents`.`id`
                FROM
                    `freeagents`
                WHERE
                    `freeagents`.`player` = {$_SESSION['playerid']}
                AND
                    `freeagents`.`game` = {$game_id}"
            );

            $is_agent = $db->iRecords() ? true : false;

            $return = array('success' => true, 'game_data' => $game_data, 'captains_team' => $team_data['team_id'], 'is_agent' => $is_agent, 'on_team' => $on_game_team, 'freeagents' => $doc);
        }
        else
            $return = array('success' => true, 'game_data' => $game_data, 'freeagents' => $doc);
    }
    else
        $return = array('success' => false, 'game_data' => $game_data, 'is_agent' => $is_agent, 'on_team' => $on_game_team, 'message' => 'No free agents listed.');
}

header('Cache-Control: no-cache, must-revalidate');
returnJSON($return);
?>
