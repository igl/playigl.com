<?php
/**
 * Fetches team details
 * @author Darryl Allen <dallen@palyigl.com>
 * @author Shaun Delaney <sdelaney@playigl.com>
 * @return JSON team list
 * @copyright Copyright 2013 PlayIGL.com
 */

include_once('../includes/config.php');
include_once('../model/mysql.class.php');
include_once('../includes/functions.php');

//Default error message
$return = array("success" => false, "error" => "No teams found.");

$db = new mysql(MYSQL_DATABASE);

$db->ExecuteSQL(
    "SELECT
    `team`.`id` AS team_id,
    `team`.`name` AS team_name,
    `team`.`logo` AS team_logo
    FROM
    `team`
    WHERE
    `team`.`featured` = 1 AND `team`.`status` = 1"
);

if ($db->iRecords()) 
    $return = array('success' => true, 'teams' => $db->ArrayResults());

header('Cache-Control: no-cache, must-revalidate');
returnJSON($return);
?>