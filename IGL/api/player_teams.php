<?php
/**
 * Fetches player data
 * @author Darryl Allen <dallen@palyigl.com>
 * @author Shaun Delaney <sdelaney@playigl.com>
 * @return JSON Player and Comments Objects
 * @copyright Copyright 2013 PlayIGL.com
 */

include_once('../includes/config.php');
include_once('../model/mysql.class.php');
include_once('../model/player.class.php');
include_once('../model/activity.class.php');
include_once('../model/history.class.php');
include_once('../model/image.class.php');
include_once('../model/league.class.php');
include_once('../model/game.class.php');
include_once('../model/team.class.php');
include_once('../includes/functions.php');

$player = new player();
$teams = $player->getTeams($_GET['id']);

//Default error message
$return = array("teams" => $teams);


returnJSON($return, true);
