<?php
/**
 * Fetches follower data
 * @author Darryl Allen <dallen@palyigl.com>
 * @author Shaun Delaney <sdelaney@playigl.com>
 * @return JSON Followers data
 * @copyright Copyright 2013 PlayIGL.com
 */

include_once('../includes/config.php');
include_once('../model/mysql.class.php');
include_once('../includes/functions.php');

$return = array("success" => false, "error" => "Please log in to use this feature.");

if (isLoggedIn()) {
    $actor_id   = $_SESSION['playerid'];
    $subject_id = $_REQUEST['subject'];
    $subject_type = $_REQUEST['subject_type'];
    $verb = 'follow';
    
    //Reject non-numeric IDs
    if (!is_numeric($actor_id) || !is_numeric($subject_id)) {
        $return['error'] = "Invalid data passed.";
        returnJSON($return);
    }

    $db = new MySQL(MYSQL_DATABASE);
    $db->ExecuteSQL(
        "SELECT actor_id FROM `".ACTIVITY."`.`notifications` 
        WHERE subject_type = '{$subject_type}' and subject_id = {$subject_id} and actor_type IN ('player', 'team');"
    );

    if($db->iRecords()) {
        $followers = $db->ArrayResults();
        
        foreach($followers as $key => $follower) {
            $db->ExecuteSQL(
                "SELECT firstname, lastname, username, avatar, country FROM player 
                WHERE playerid = '{$follower['actor_id']}'"
            );
                
            $profile = $db->ArrayResult();
                
            $followers[$key]['player_id']       = $follower['actor_id'];
            $followers[$key]['player_username'] = $profile['username'];
            $followers[$key]['player_firstname']= $profile['firstname'];
            $followers[$key]['player_lastname'] = $profile['lastname'];
            $followers[$key]['player_avatar']   = $profile['avatar'];
            $followers[$key]['player_country']  = $profile['country'];
            $followers[$key]['player_avatar']   = $profile['avatar'];

            $db->ExecuteSQL(
                "SELECT id FROM `".ACTIVITY."`.`notifications` 
                WHERE actor_type = 'player' AND actor_id = {$subject_id} AND actor_type='player' 
                AND subject_id = {$follower['actor_id']} AND subject_type = 'player';"
            );
                
            if ($db->iRecords())
                $followers[$key]['isFollowing'] = true;
            else
                $followers[$key]['isFollowing'] = false;
        }
        
        $return = array('success' => true, 'followers' => $followers);
    } else {
        $return = array('success' => true, 'followers' => array());
    }
}

returnJSON($return);
