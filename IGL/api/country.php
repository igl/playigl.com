<?php
/**
 * Fetches countries
 * @author Darryl Allen <dallen@palyigl.com>
 * @author Shaun Delaney <sdelaney@playigl.com>
 * @return JSON List of countries
 * @copyright Copyright 2012-2013 PlayIGL.com
 */

include_once('../includes/config.php');
include_once('../model/mysql.class.php');
include_once('../includes/functions.php');

$db = new mysql(MYSQL_DATABASE);
$db->ExecuteSQL(
    "SELECT * FROM country"
);

if($db->iRecords())
    $return = array('success' => true, 'country_list' => $db->ArrayResults());
else
    $return = array('success' => false, 'error' => 'Country list could not be compiled.');

returnJSON($return);