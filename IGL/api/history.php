<?php
/**
 * Fetches player or team history
 * @author Darryl Allen <dallen@palyigl.com>
 * @author Shaun Delaney <sdelaney@playigl.com>
 * @return JSON history list
 * @copyright Copyright 2013 PlayIGL.com
 */

include_once('../includes/config.php');
include_once('../model/mysql.class.php');
include_once('../includes/functions.php');

//Default error message
$return = array("success" => false, "error" => "Invalid Player or Team.");

//Variables
$id         = isset($_REQUEST['id']) ? $_REQUEST['id'] : null;
$fetch      = isset($_REQUEST['fetch']) ? strtolower($_REQUEST['fetch']) : null;

if (!is_null($id) && is_numeric($id) && in_array($fetch, array('player','team'))) {
    $db = new mysql(MYSQL_DATABASE);
    $db->ExecuteSQL(
        "SELECT 
            player.playerid as player_id, 
            player.username as player_username, 
            team.id as team_id, 
            team.name as team_name,
            game.id as game_id,
            game.name as game_name,
            history.joindate,
            history.leavedate
        FROM history
        JOIN team ON team.id = history.team
        JOIN game ON game.id = history.game
        JOIN player ON player.playerid = history.player
        WHERE history.{$fetch} = {$id}"
    );

    if ($db->iRecords) {
        $history = $db->ArrayResults();
        foreach ($history as $key => $value) {
            $history[$key]['joindate'] = date('M, Y',$value['joindate']);

            if($value['leavedate'])
                $history[$key]['leavedate'] = date('M, Y',$value['leavedate']);
            else
                $history[$key]['leavedate'] = "Current";
        }
        $return = array('success' => true, 'history' => $history);
    }
    else
        $return['error'] = "No history found";
}

returnJSON($return);