<?php
/**
 * Fetches the stream
 * @author Darryl Allen <dallen@palyigl.com>
 * @author Shaun Delaney <sdelaney@playigl.com>
 * @return JSON Stream Data
 * @copyright Copyright 2013 PlayIGL.com
 */

include_once('../includes/config.php');
include_once('../model/mysql.class.php');
include_once('../includes/functions.php');

//Default error message
$return = array("success" => false, "error" => "Invalid Request.");

$db = new MySQL(MYSQL_DATABASE);
$db->ExecuteSQL(
    "SELECT * FROM `stream` LIMIT 1;"
);

if ($db->iRecords())
    $return = array("success" => true, "stream" => $db->ArrayResult());

header('Cache-Control: no-cache, must-revalidate');
returnJSON($return);
?>