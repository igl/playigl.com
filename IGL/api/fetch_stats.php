<?php

/*
 * @copyright 2012 PlayIGL.com
 * @author Darryl Allen
 * This file scans the HLStats DB for CS:GO stats for
 */

header('Cache-Control: no-cache, must-revalidate');
header('Content-type: application/json');

/*
define ("MYSQL_HOST", "stats-dev.cimukvvq20st.us-east-1.rds.amazonaws.com"); // MySQL Hostname
define ("MYSQL_USER", "stats_dev");	// MySQL Username
define ("MYSQL_PASS", "g3tRAg5D");	// MySQL Password
define ("MYSQL_DATABASE", "stats");	// MySQL Database
*/

define ("MYSQL_HOST", "localhost"); // MySQL Hostname
define ("MYSQL_USER", "root");	// MySQL Username
define ("MYSQL_PASS", "");	// MySQL Password
define ("MYSQL_DATABASE", "stats");	// MySQL Database

require('../includes/functions.php');
require('../model/mysql.class.php');
date_default_timezone_set('UTC');

if(isset($_REQUEST['date']) && is_numeric($_REQUEST['date']) && isset($_REQUEST['server']) && is_numeric($_REQUEST['server']) && in_array($_REQUEST['get'], array('player','team','match')))
{
    $db = new mysql(MYSQL_DATABASE);
    $date = (int)$_REQUEST['date']; //date from schedule in unix time
    $server_id = (int)$_REQUEST['server']; //serverid
    $get = $_REQUEST['get']; //Get individual player data, team data, or match data
    
    
    ###Test variables
    $date = strtotime("2012-12-17 21:00:00");
    $server_id = 2;
    
    
    $match_start = date('Y-m-d H:i:s',$date);
    $match_finish = date('Y-m-d H:i:s',$date + 7200);
    
    $sql = "SELECT eventTime FROM hlstats_events_playeractions WHERE eventTime > '{$match_start}' AND eventTime < '{$match_finish}' LIMIT 1";
   
    $db->ExecuteSQL($sql); //check if matches for this time exist
    if($db->iRecords())
    {
        //Get times each player was on a team.
        $sql = "SELECT eventTime, COUNT(eventTime) AS halftime FROM hlstats_events_changeteam WHERE eventTime > '{$match_start}' AND eventTime < '{$match_finish}' AND serverId = '{$server_id}' GROUP BY eventTime ORDER BY eventTime DESC LIMIT 1";
        $db->ExecuteSQL($sql); 
        $doc = $db->ArrayResult();
        $halftime = $doc['eventTime'];
        
        
        $sql = "SELECT playerId FROM hlstats_events_changeteam WHERE eventTime > '{$match_start}' AND eventTime <= '{$halftime}' AND serverId = '{$server_id}' AND team = 'TERRORIST'";
        $db->ExecuteSQL($sql);         
        $team1 = array_flatten_recursive($db->ArrayResults());
        
        $sql = "SELECT playerId FROM hlstats_events_changeteam WHERE eventTime >= '{$halftime}' AND eventTime < '{$match_finish}' AND serverId = '{$server_id}' AND team = 'CT'";
        $db->ExecuteSQL($sql);

        //Flatten the array & remove any stragglers from team changes.
        //Working on assumptions, we use the teams from after half time to assume assignment. Then trim them up.
        $team2 = array_flatten_recursive($db->ArrayResults());   //CT round 2
        if(count($team1)>5)
        {
            $team1 = array_diff($team1, $team2);
        }
        if(count($team2)>5)
        {
            $team2 = array_diff($team2, $team1);
        }

//Round 1 CT = Team 1
//Round 1 T  = Team 2
        
//Round 2 CT = Team 2
//Round 2 T  = Team 1

        switch($get)
        {
            case "match":
                
                foreach($team1 as $key => $value)
                {
                
                    $sql = "SELECT hlstats_events_playeractions.playerId, hlstats_players.lastname, count(hlstats_events_playeractions.playerId) as count, description
                            FROM hlstats_actions
                            JOIN hlstats_events_playeractions ON hlstats_events_playeractions.actionId = hlstats_actions.id
                            JOIN hlstats_players ON hlstats_players.playerId=hlstats_events_playeractions.playerId
                            GROUP BY description, hlstats_events_playeractions.playerId";
                    
                    
                    
                    $db->ExecuteSQL($sql);
                    $actions = $db->ArrayResults();
                    
                    foreach($actions['id'] as $key => $value)
                    {
                        $sql = "SELECT * FROM hlstats_events_playeractions WHERE actionID = '{$value}' ";
                    }
                    
                    //Set SteamID & Community ID
                    $sql = "SELECT uniqueId FROM hlstats_playeruniqueids WHERE playerId = '{$value}'";
                    $db->ExecuteSQL($sql);
                    $stat   = $db->ArrayResult();
                    $steam_id = $stat['uniqueId'];
                    $split_id = explode(':',$steam_id);
                    $community_id = bcadd(bcadd(bcmul($split_id[1],2), '76561197960265728'), $split_id[1]);
                    $stats['team1']['CT'][$key]['SteamID'] = $steam_id;
                    $stats['team1']['CT'][$key]['CommunityID'] = $community_id;
                    $stats['team1']['TERRORIST'][$key]['SteamID'] = $steam_id;
                    $stats['team1']['TERRORIST'][$key]['CommunityID'] = $community_id;
                
                    //Team 1 CT
                    $sql = "SELECT count(killerId) as kills FROM hlstats_events_frags WHERE 
                        eventTime > '{$match_start}' AND eventTime <= '{$halftime}' AND serverId = '{$server_id}' AND killerId = '{$value}' ";
                    $db->ExecuteSQL($sql);
                    $stat   = $db->ArrayResult();
                    $kills  = $stat['kills'];
                    $stats['team1']['CT'][$key]['kills']= $kills; //Update stats for [player][round][kills]
                    
                    $sql = "SELECT count(victimId) as deaths FROM hlstats_events_frags WHERE 
                        eventTime > '{$match_start}' AND eventTime <= '{$halftime}' AND serverId = '{$server_id}' AND victimId = '{$value}'";
                    $db->ExecuteSQL($sql);
                    $stat   = $db->ArrayResult();
                    $deaths = $stat['deaths'];
                    $stats['team1']['CT'][$key]['deaths']= $deaths; //Update stats for [player][round][deaths]
                    $stats['team1']['CT'][$key]['KDR']= number_format(($kills/$deaths),3); //Update stats for [player][round][deaths]
                    
                    //Bomb Defusal Stats
                    $sql = "SELECT count(actionId) as success_bomb_defuse FROM hlstats_events_playeractions WHERE actionId = '4' AND 
                        eventTime > '{$match_start}' AND eventTime <= '{$halftime}' AND serverId = '{$server_id}' AND playerId = '{$value}'";
                    $db->ExecuteSQL($sql);
                    $stat   = $db->ArrayResult();
                    $success_bomb_defuse = $stat['success_bomb_defuse'];
                    $stats['team1']['CT'][$key]['success_bomb_defuse'] = $success_bomb_defuse;
                    
                    $sql = "SELECT count(actionId) as start_bomb_defuse FROM hlstats_events_playeractions WHERE actionId = '1' AND 
                        eventTime > '{$match_start}' AND eventTime <= '{$halftime}' AND serverId = '{$server_id}' AND playerId = '{$value}'";
                    $db->ExecuteSQL($sql);
                    $stat   = $db->ArrayResult();
                    $start_bomb_defuse = $stat['start_bomb_defuse'];
                    $stats['team1']['CT'][$key]['start_bomb_defuse'] = $start_bomb_defuse;
                    
                    $sql = "SELECT count(actionId) as start_bomb_defuse_kit FROM hlstats_events_playeractions WHERE actionId = '2' AND 
                        eventTime > '{$match_start}' AND eventTime <= '{$halftime}' AND serverId = '{$server_id}' AND playerId = '{$value}'";
                    $db->ExecuteSQL($sql);
                    $stat   = $db->ArrayResult();
                    $start_bomb_defuse_kit = $stat['start_bomb_defuse_kit'];
                    $stats['team1']['CT'][$key]['start_bomb_defuse_kit'] = $start_bomb_defuse_kit;
                    
                    //
                    //HALF TIME - SWITCH SIDES
                    //
                    
                    //Team 1 Terrorist
                    $sql = "SELECT count(killerId) as kills FROM hlstats_events_frags WHERE 
                        eventTime > '{$halftime}' AND eventTime <= '{$match_finish}' AND serverId = '{$server_id}' AND killerId = '{$value}' ";
                    $db->ExecuteSQL($sql);
                    $stat   = $db->ArrayResult();
                    $kills  = $stat['kills'];
                    $stats['team1']['TERRORIST'][$key]['kills']= $kills; //Update stats for [player][round][kills]
                    
                    $sql = "SELECT count(victimId) as deaths FROM hlstats_events_frags WHERE 
                        eventTime > '{$halftime}' AND eventTime <= '{$match_finish}' AND serverId = '{$server_id}' AND victimId = '{$value}'                        ";
                    $db->ExecuteSQL($sql);
                    $stat   = $db->ArrayResult();
                    $deaths = $stat['deaths'];
                    $stats['team1']['TERRORIST'][$key]['deaths']= $deaths; //Update stats for [player][round][deaths]
                    $stats['team1']['TERRORIST'][$key]['KDR']= number_format(($kills/$deaths),3); //Update stats for [player][round][deaths]
                    
                    //Bomb Plants
                    $sql = "SELECT count(actionId) as success_bomb_plant FROM hlstats_events_playeractions WHERE actionId = '3' AND 
                        eventTime > '{$halftime}' AND eventTime <= '{$match_finish}' AND serverId = '{$server_id}' AND playerId = '{$value}'";
                    $db->ExecuteSQL($sql);
                    $stat   = $db->ArrayResult();
                    $success_bomb_plant = $stat['success_bomb_plant'];
                    $stats['team1']['TERRORIST'][$key]['success_bomb_plant'] = $success_bomb_plant;
                    
                }
                
                foreach($team2 as $key => $value)
                {
                    //Set SteamID
                    $sql = "SELECT uniqueId FROM hlstats_playerUniqueids WHERE playerId = '{$value}'";
                    $db->ExecuteSQL($sql);
                    $stat   = $db->ArrayResult();
                    $steam_id = $stat['uniqueId'];
                    $split_id = explode(':',$steam_id);
                    $community_id = bcadd(bcadd(bcmul($split_id[1], '2'), '76561197960265728'), $split_id[1]);
                    $stats['team2']['CT'][$key]['SteamID'] = $steam_id;
                    $stats['team2']['CT'][$key]['CommunityID'] = $community_id;
                    $stats['team2']['TERRORIST'][$key]['SteamID'] = $steam_id;
                    $stats['team2']['TERRORIST'][$key]['CommunityID'] = $community_id;

                    //Team 2 Round 1 events
                    $sql = "SELECT count(killerId) as kills FROM hlstats_events_frags WHERE 
                        eventTime > '{$match_start}' AND eventTime <= '{$halftime}' AND serverId = '{$server_id}' AND killerId = '{$value}' ";
                    $db->ExecuteSQL($sql);
                    $stat   = $db->ArrayResult();
                    $kills  = $stat['kills'];
                    $stats['team2']['TERRORIST'][$key]['kills']= $kills; //Update stats for [player][round][kills]
                    
                    $sql = "SELECT count(victimId) as deaths FROM hlstats_events_frags WHERE 
                        eventTime > '{$match_start}' AND eventTime <= '{$halftime}' AND serverId = '{$server_id}' AND victimId = '{$value}'                        ";
                    $db->ExecuteSQL($sql);
                    $stat   = $db->ArrayResult();
                    $deaths = $stat['deaths'];
                    $stats['team2']['TERRORIST'][$key]['deaths']= $deaths; //Update stats for [player][round][deaths]
                    $stats['team2']['TERRORIST'][$key]['KDR']= number_format(($kills/$deaths),3); //Update stats for [player][round][deaths]
                    
                    //Bomb Plants
                    $sql = "SELECT count(actionId) as success_bomb_plant FROM hlstats_events_playeractions WHERE actionId = '3' AND 
                        eventTime > '{$match_start}' AND eventTime <= '{$halftime}' AND serverId = '{$server_id}' AND playerId = '{$value}'";
                    $db->ExecuteSQL($sql);
                    $stat   = $db->ArrayResult();
                    $success_bomb_plant = $stat['success_bomb_plant'];
                    $stats['team2']['CT'][$key]['success_bomb_plant'] = $success_bomb_plant;
                    

                    
                    //
                    //HALF TIME - SWITCH SIDES
                    //
                    //
                    //
                    //Team 1 Round 2
                    $sql = "SELECT count(killerId) as kills FROM hlstats_events_frags WHERE 
                        eventTime > '{$halftime}' AND eventTime <= '{$match_finish}' AND serverId = '{$server_id}' AND killerId = '{$value}' ";
                    $db->ExecuteSQL($sql);
                    $stat   = $db->ArrayResult();
                    $kills  = $stat['kills'];
                    $stats['team2']['CT'][$key]['kills']= $kills; //Update stats for [player][round][kills]
                    
                    $sql = "SELECT count(victimId) as deaths FROM hlstats_events_frags WHERE 
                        eventTime > '{$halftime}' AND eventTime <= '{$match_finish}' AND serverId = '{$server_id}' AND victimId = '{$value}'                        ";
                    $db->ExecuteSQL($sql);
                    $stat   = $db->ArrayResult();
                    $deaths = $stat['deaths'];
                    $stats['team2']['CT'][$key]['deaths']= $deaths; //Update stats for [player][round][deaths]
                    $stats['team2']['CT'][$key]['KDR']= number_format(($kills/$deaths),3); //Update stats for [player][round][deaths]
                    
                    //Bomb Defusal Stats
                    $sql = "SELECT count(actionId) as success_bomb_defuse FROM hlstats_events_playeractions WHERE actionId = '4' AND 
                        eventTime > '{$halftime}' AND eventTime <= '{$match_finish}' AND serverId = '{$server_id}' AND playerId = '{$value}'";
                    $db->ExecuteSQL($sql);
                    $stat   = $db->ArrayResult();
                    $success_bomb_defuse = $stat['success_bomb_defuse'];
                    $stats['team2']['CT'][$key]['success_bomb_defuse'] = $success_bomb_defuse;
                    
                    $sql = "SELECT count(actionId) as start_bomb_defuse FROM hlstats_events_playeractions WHERE actionId = '1' AND 
                        eventTime > '{$halftime}' AND eventTime <= '{$match_finish}' AND serverId = '{$server_id}' AND playerId = '{$value}'";
                    $db->ExecuteSQL($sql);
                    $stat   = $db->ArrayResult();
                    $start_bomb_defuse = $stat['start_bomb_defuse'];
                    $stats['team2']['CT'][$key]['start_bomb_defuse'] = $start_bomb_defuse;
                    
                    $sql = "SELECT count(actionId) as start_bomb_defuse_kit FROM hlstats_events_playeractions WHERE actionId = '2' AND 
                        eventTime > '{$halftime}' AND eventTime <= '{$match_finish}' AND serverId = '{$server_id}' AND playerId = '{$value}'";
                    $db->ExecuteSQL($sql);
                    $stat   = $db->ArrayResult();
                    $start_bomb_defuse_kit = $stat['start_bomb_defuse_kit'];
                    $stats['team2']['CT'][$key]['start_bomb_defuse_kit'] = $start_bomb_defuse_kit;
                    
                    //Return stats
                    $result = array('success'=> true, 'stats' => $stats);
                }
                
                break;
             case "player";
                 if(isset($_REQUEST['steamid']) && is_numeric($_REQUEST['steamid']) && strlen($_REQUEST['steamid']) == 17)
                 {
                     $steam_64 = $_REQUEST['steamid'];
                     $steam_32 = convert64to32($steam_64);
                     $result = array('success' => true, 'stats' => $steam_32);
                 }
                 else
                 {
                     $result = array('success' => false, 'message' => 'Please specify a valid 64bit SteamID');
                 }
                 break;
            
        }
    }
    else
    {
        $result = array('success' => false, 'message' => "That match could not be found.");
    }
}
else
{
    $result = array('success' => false, 'message' => "Some parameters appear to be missing.");
}

prettyJSON(json_encode($result));
?>