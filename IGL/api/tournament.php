<?php
/**
 * Fetches tournament details
 * @author Darryl Allen <dallen@palyigl.com>
 * @author Shaun Delaney <sdelaney@playigl.com>
 * @return JSON tournament list
 * @copyright Copyright 2013 PlayIGL.com
 */

include_once('../includes/config.php');
include_once('../model/mysql.class.php');
include_once('../includes/functions.php');

//Default error message
$return = array("success" => false, "error" => "Invalid Request.");

//Variables
$id         = isset($_REQUEST['id']) ? $_REQUEST['id'] : null;
$fetch      = isset($_REQUEST['fetch']) ? $_REQUEST['fetch'] : null;
$set        = isset($_REQUEST['set']) ? $_REQUEST['set'] : null;
$team_id    = isset($_REQUEST['teamid']) ? $_REQUEST['teamid'] : null;
$tournament_id = isset($_REQUEST['tournamentid']) ? $_REQUEST['tournamentid'] : null;

$db = new mysql(MYSQL_DATABASE);

if (!is_null($fetch) && in_array($_REQUEST['fetch'], array('all'))) {
    $db->ExecuteSQL(
        "SELECT 
            `game`.`id` AS `tournament_game_id`, 
            `game`.`name` AS `tournament_game_name`, 
            `tournaments`.`id` AS `tournament_id`, 
            `tournaments`.`name` AS `tournament_name`, 
            `tournaments`.`registration_type` AS `tournament_registration_type`, 
            `game`.`about` AS `tournament_game_about`,
            `game`.`banner` AS `tournament_game_banner`,
            `game`.`forum` AS `tournament_game_forum`,
            `tournaments`.`details` AS `tournament_details`,
            `tournaments`.`rules` AS `tournament_rules`,
            `tournaments`.`checkinstartdatetime` AS `tournament_checkinstart`,
            `tournaments`.`checkinenddatetime` AS `tournament_checkinend`,
            `tournaments`.`startdatetime` AS `tournament_start`,
            `tournaments`.`enddatetime` AS `tournament_end`
        FROM 
            `tournaments` 
        JOIN 
            `game` ON `game`.`id` = `tournaments`.`game`"
    );
    
    if ($db->iRecords())
        $return = array('success' => true, 'tournaments' => $db->ArrayResults());
    else
        $return['error'] = "Sorry, no tournaments are running.";
} elseif (!is_null($set) && !is_null($team_id) && !is_null($tournament_id)) {    
    if ($db->Update('team', array('tournament'=>$tournament_id), array('id'=>$team_id) ))
       $return = array('success' => true); 
} elseif (!is_null($id) && is_numeric($id)) {

    $db->ExecuteSQL(
        "SELECT 
            `game`.`id` AS `tournament_game_id`, 
            `game`.`name` AS `tournament_game_name`, 
            `tournaments`.`id` AS `tournament_id`, 
            `tournaments`.`name` AS `tournament_name`, 
            `tournaments`.`registration_type` AS `tournament_registration_type`, 
            `game`.`about` AS `tournament_game_about`,
            `game`.`banner` AS `tournament_game_banner`,
            `tournaments`.`details` AS `tournament_details`,
            `tournaments`.`rules` AS `tournament_rules`,
            `tournaments`.`checkinstartdatetime` AS `tournament_checkinstart`,
            `tournaments`.`checkinenddatetime` AS `tournament_checkinend`,
            `tournaments`.`eventstartdatetime` AS `tournament_start`,
            `tournaments`.`eventenddatetime` AS `tournament_end`,
            `tournaments`.`registrationstartdatetime` AS `tournament_registrationstart`,
            `tournaments`.`registrationenddatetime` AS `tournament_registrationend`
        FROM 
            `tournaments` 
        JOIN 
            `game` ON `game`.`id` = `tournaments`.`game`
        WHERE 
            `tournaments`.`id` = {$id}"
    );
    
    if ($db->iRecords()) {
        $tournament = $db->ArrayResult();

        $db->ExecuteSQL(
            "SELECT
                `team`.`id` AS team_id, 
                `team`.`name` AS team_name, 
                `team`.`tag` AS team_tag, 
                `team`.`rpi` AS team_rpi, 
                `team`.`points` AS team_points, 
                `team`.`points` AS team_highest_points, 
                `team`.`captain` AS team_captain_id, 
                `team`.`alternate` AS team_alternate_id, 
                `team`.`logo` AS team_logo,
                `tournament_teams`.`waitlist_flag`,
                `tournament_teams`.`checkedin_flag`,
                `tournament_teams`.`tstatus`,
                `player`.`username` as team_captain_name
            FROM
                `tournament_teams`
            LEFT JOIN
                `team` ON `team`.`id` = `tournament_teams`.`team_id`
            LEFT JOIN
                `player` ON `player`.`playerid` = `team`.`captain`
            WHERE
                `tournament_teams`.`tournament_id` = {$id}
            ORDER BY `tournament_teams`.`id` DESC"
        );
                
        $tournament['tournament_teams'] = $db->ArrayResults();
        $tournament['active_session'] = $_SESSION['playerid'];
        
        $tournament['currentdatetime'] = mktime();
        $return = array('success' => true, 'tournament' => $tournament);
    }
    else
        $return['error'] = "Sorry, that tournament is not running.";
        $return['sqlerror'] = $db->sLastError;
}

header('Cache-Control: no-cache, must-revalidate');
returnJSON($return);
?>