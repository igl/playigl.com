<?php

/**
 * Fetches all competition types & divisions on a per game basis info
 * @author Darryl Allen <dallen@palyigl.com>
 * @author Shaun Delaney <sdelaney@playigl.com>
 * @return JSON navigation list
 * @copyright Copyright 2013 PlayIGL.com
 */
include_once('../includes/config.php');
include_once('../model/mysql.class.php');
include_once('../includes/functions.php');

//Default error message
$return = array();

$db = new mysql(MYSQL_DATABASE);

$db->ExecuteSQL(
        "SELECT * FROM active_games"
);

$games = $db->ArrayResults();
$return['navigation']['league'] = $games;
$return['navigation']['league']['num_games'] = count($games);
$return['navigation']['ladder'] = $games;
$return['navigation']['tournament'] = $games;

// Get ads
$db->ExecuteSQL('SELECT * FROM ads LIMIT 1');
if( $db->iRecords() )
{
    $ad = $db->ArrayResult();
    $return['ad'] = $ad;
}
 else
     $return['ad'] = array();


//Do all this stuff here to get all leagues
foreach ($games as $key => $game) {
    //Get the number of teams
    $db->ExecuteSQL(
            "SELECT id FROM team WHERE game = {$game['game_id']} AND status = 1 AND deleted = 0"
    );

    $return['navigation']['league'][$key]['game_teams'] = $db->iRecords();
    $return['navigation']['ladder'][$key]['game_teams'] = $db->iRecords();
    $return['navigation']['tournament'][$key]['game_teams'] = $db->iRecords();

    //Get the free agent count for each game & competition type
    $db->ExecuteSQL(
            "SELECT id FROM freeagents WHERE game = {$game['game_id']}"
    );

    $return['navigation']['league'][$key]['game_freeagents'] = $db->iRecords();
    $return['navigation']['ladder'][$key]['game_freeagents'] = $db->iRecords();
    $return['navigation']['tournament'][$key]['game_freeagents'] = $db->iRecords();

    //Get the open team count for each game
    $db->ExecuteSQL(
            "SELECT team_id FROM active_teams WHERE team_game_id = {$game['game_id']}"
    );

    $return['navigation']['league'][$key]['game_openteams'] = $db->iRecords();
    $return['navigation']['ladder'][$key]['game_openteams'] = $db->iRecords();
    $return['navigation']['tournament'][$key]['game_openteams'] = $db->iRecords();

    $db->ExecuteSQL(
            "SELECT
            `team`.`id` AS `team_id`
        FROM
            `team`
        JOIN
            `league` ON `league`.`id` = `team`.`league`
        WHERE
            `league`.`game` = {$game['game_id']} AND `league`.`status` = 1"
    );

    $return['navigation']['league'][$key]['league_num_teams'] = $db->iRecords();

    $db->ExecuteSQL(
            "SELECT
            `id` AS `league_id`,
            `name` AS `league_name`
        FROM
            `league`
        WHERE
            `game` = {$game['game_id']}  AND `league`.`status` = 1
        ORDER BY `order` ASC"
    );

    $return['navigation']['league'][$key]['league_num_divisions'] = $db->iRecords();
    $return['navigation']['league'][$key]['league_divisions'] = $db->ArrayResults();

    foreach ($return['navigation']['league'][$key]['league_divisions'] as $index => $league) {
        $db->ExecuteSQL(
                "SELECT
                `team`.`id` AS `team_id`
            FROM
                `team`
            JOIN
               `league` ON `team`.`league` = `league`.`id`
            WHERE
                `team`.`league` = {$league['league_id']} AND `league`.`status` = 1 AND `team`.`status` = 1"
        );

        $return['navigation']['league'][$key]['league_divisions'][$index]['league_active_teams'] = $db->iRecords();
    }
}

///Do all this stuff here to get all ladders
foreach ($return['navigation']['ladder'] as $key => $game) {
    $db->ExecuteSQL(
            "SELECT
                `active_teams`.`team_id`
            FROM
                `active_teams`
            JOIN
                `ladder_teams` ON `ladder_teams`.`team_id` = `active_teams`.`team_id`
            WHERE
                `active_teams`.`team_game_id` =  {$game['game_id']}"
    );
    $return['navigation']['ladder'][$key]['ladder_num_teams'] = $db->iRecords();

    $db->ExecuteSQL(
            "SELECT
                `ladder_id`,
                `ladder_name`,
                `ladder_format`
            FROM
                `active_ladders`
            WHERE
                `active_ladders`.`ladder_game_id` = {$game['game_id']}
            ORDER BY `active_ladders`.`ladder_format`, `active_ladders`.`ladder_id` ASC"
    );

    $return['navigation']['ladder'][$key]['ladder_num_divisions'] = $db->iRecords();
    $return['navigation']['ladder'][$key]['ladder_divisions'] = $db->ArrayResults();

    foreach ($return['navigation']['ladder'][$key]['ladder_divisions'] as $index => $ladder) {
        //Needs to be updated to get list of teams in a tournament
        $db->ExecuteSQL(
                "SELECT
                `active_teams`.`team_id`
            FROM
                `active_teams`
            LEFT JOIN ladder_teams ON ladder_teams.team_id = active_teams.team_id
            WHERE
                `ladder_teams`.`tier_id` = {$ladder['ladder_id']} "
        );

        $return['navigation']['ladder'][$key]['ladder_divisions'][$index]['ladder_active_teams'] = $db->iRecords();
    }
}

///Do all this stuff here to get all tournaments
foreach ($return['navigation']['tournament'] as $key => $game) {
    $db->ExecuteSQL(
            "SELECT
            `tournament_teams`.`id`
        FROM
            `tournament_teams`
        JOIN
            `tournaments` ON `tournaments`.`id` = `tournament_teams`.`tournament_id`
        WHERE
            `tournaments`.`game` = {$game['game_id']} AND `tournaments`.`status` = 1 AND `tournaments`.`is_published` = 1"
    );

    $return['navigation']['tournament'][$key]['tournament_num_teams'] = $db->iRecords();

    $db->ExecuteSQL(
            "SELECT
            `id` AS `tournament_id`,
            `name` AS `tournament_name`,
             `bracket_size` AS `tournament_size`
        FROM
            `tournaments`
        WHERE
            `game` = {$game['game_id']} AND `tournaments`.`status` = 1 AND `tournaments`.`is_published` = 1"
    );

    $return['navigation']['tournament'][$key]['tournament_num_divisions'] = $db->iRecords();
    $return['navigation']['tournament'][$key]['tournament_divisions'] = $db->ArrayResults();

    //Requires updating to get list of teams pariticipating in a tournament
    foreach ($return['navigation']['tournament'][$key]['tournament_divisions'] as $index => $tournament) {
        $db->ExecuteSQL(
                "SELECT
                `tournament_teams`.`team_id` AS `team_id`
            FROM
                `tournament_teams`
            JOIN
                `tournaments` ON `tournaments`.`id` = `tournament_teams`.`tournament_id`
            WHERE
                `tournament_teams`.`tournament_id` = {$tournament['tournament_id']} AND `tournaments`.`status` = 1 AND `tournaments`.`is_published` = 1"
        );

        $return['navigation']['tournament'][$key]['tournament_divisions'][$index]['tournament_active_teams'] = $db->iRecords();
    }
}


header('Cache-Control: no-cache, must-revalidate');
returnJSON($return);
?>