<?php
/**
 * Fetches player data
 * @author Darryl Allen <dallen@palyigl.com>
 * @author Shaun Delaney <sdelaney@playigl.com>
 * @return JSON Player and Comments Objects
 * @copyright Copyright 2013 PlayIGL.com
 */

include_once('../includes/config.php');
include_once('../model/mysql.class.php');
include_once('../includes/functions.php');

//Default error message
$return = array("success" => false, "error" => "Sorry, data for this player could not be compiled.");

//Save the player ID and table column
$player = $_REQUEST['id'];


if(is_numeric($player))
{
    if(strlen($player) == 17)
    {
        $column = "`communityid`";
    }
    else
    {
        $column = "`playerid`";
    }
}
else
{
    $column = "`username`";
    $player = urldecode($player);
}

//Create the query, depending on who is viewing this player
if(isLoggedIn() && in_array($player,array($_SESSION['playerid'],$_SESSION['username'],$_SESSION['communityid']))) {
    $query = "
        SELECT
            playerid as player_id,
            username  as player_username, 
            firstname  as player_firstname, 
            lastname  as player_lastname, 
            ingame_name as player_ingame_name,
            country  as player_country, 
            city  as player_city, 
            state as player_state, 
            email as player_email, 
            communityid as player_community_id, 
            twitch as player_twitch, 
            facebook as player_facebook, 
            facebook_token as player_facebook_token, 
            twitter as player_twitter, 
            twitter_token as player_twitter_token, 
            posts as player_posts, 
            joindate as player_joindate, 
            lastactivity as player_lastactivity, 
            avatar as player_avatar, 
            birthday as player_birthday,
            sex as player_sex,
            website as player_website,
            timezoneoffset as player_timezoneoffset
        FROM 
            `player` 
        WHERE {$column} = '{$player}'";
} else {
    $query = "
        SELECT 
            playerid as player_id, 
            username as player_username, 
            firstname as player_firstname, 
            lastname as player_lastname, 
            ingame_name as player_ingame_name,
            country as player_country, 
            city as player_city, 
            state as player_state, 
            communityid as player_community_id, 
            facebook as player_facebook, 
            twitter as player_twitter, 
            posts as player_posts, 
            joindate as player_joindate, 
            lastactivity as player_lastactivity, 
            avatar as player_avatar, 
            birthday as player_birthday
        FROM 
            `player` 
        WHERE {$column} = '{$player}'";
}

//Connect to the db
$db = new mysql(MYSQL_DATABASE);
$db->ExecuteSQL($query);

//If theres a return
if($db->iRecords()) {
    $result = $db->ArrayResult();
    $player_id = $result['player_id'];

    //decode ingame names
    if($result['player_ingame_name']) {
        $result['player_ingame_name'] = json_decode($result['player_ingame_name'], true);
        foreach($result['player_ingame_name'] as $i => $v)
        {
            $db->ExecuteSQL("SELECT name FROM game WHERE id = {$i} AND status = 1 AND deleted = 0");
            if($db->iRecords())
            {
                $doc = $db->ArrayResult();
                $result['player_ingame_name'][$doc['name']] =  $v;
            }
            unset($result['player_ingame_name'][$i]);
        }
    }
    else
        $result['player_ingame_name'] = array();
    
    //Create birthday
    if($result['player_birthday']) {
        list($y,$m,$d) = explode('-', $result['player_birthday']);
        if (($m = (date('m') - $m)) < 0) {
            $y++;
        } elseif ($m == 0 && date('d') - $d < 0) {
            $y++;
        } 
        $result['player_age'] = date('Y') - $y;
    }
    $result['player_lastactivity'] = ago($result['player_lastactivity']);

    //Create steam ID
    $result['player_steam_id'] = convert64to32($result['player_community_id']);
    $result['player_generated'] = date('Y-m-d H:i:s',time());

    //Create followers
    $db_activity = new mysql(ACTIVITY);
    $query = "SELECT id FROM ".ACTIVITY.".notifications WHERE subject_type = 'player' and subject_id = {$player_id} and actor_type IN ('player', 'team');";

    $db_activity->ExecuteSQL($query);
    $result['player_followers'] = $db_activity->iRecords();

    //Create followee's
    $query = "SELECT id FROM ".ACTIVITY.".notifications WHERE actor_type = 'player' and actor_id = {$player_id} and subject_type IN ('player', 'team');";
    $db_activity->ExecuteSQL($query);
    $result['player_following'] = $db_activity->iRecords();

    $db = new mysql(MYSQL_DATABASE);
    //Grab the teams this player is on
    $query = "SELECT 
            team_id,
            team_tag,
            team_name,
            team_captain_id,
            team_alternate_id,
            team_game_id,
            team_game_name,
            `team_logo` as team_avatar,
            team_rpi
        FROM 
            `roster` 
            LEFT JOIN `active_teams` on `roster`.`team` = `team_id`
            LEFT JOIN `game` on `active_teams`.`team_game_id` = `game`.`id`
        WHERE 
            `roster`.`player` = {$player_id}";

    $db->ExecuteSQL($query);

    //If there are teams, save the data
    $teams = array();
    if($db->iRecords())
        $teams = $db->ArrayResults();
    foreach($teams as $i => $t) {
        $db->ExecuteSQL("
                SELECT      
                    ladder_id,
                    ladder_name
                FROM 
                    active_ladders 
                LEFT JOIN 
                    `ladder_teams` ON `ladder_teams`.`tier_id` = `active_ladders`.`ladder_id` 
                WHERE 
                    `ladder_teams`.`team_id` = {$t['team_id']}");
        if($db->iRecords())             
            $teams[$i]['team_ladders'] = $db->ArrayResults();
        else
            $teams[$i]['team_ladders'] = array();
    }
    $result['player_teams'] = $teams;
    
    
    $db = new mysql(MYSQL_DATABASE);
    //Grab the teams this player is on
    $db->ExecuteSQL("SELECT 
                `team`.`id` as team_id,
                `team`.`tag` as team_tag,
                `team`.`name` as team_name,
                `game`.`name` as team_game_name,
                `team`.`logo` as team_avatar,
                `team`.`rpi` as team_rpi,
                `history`.`joindate` as team_join_date,
                `history`.`leavedate` as team_leave_date
            FROM 
                `history` 
                LEFT JOIN `team` on `history`.`team` = `team`.`id`
                LEFT JOIN `game` on `game`.`id` = `team`.`game`
            WHERE 
                `history`.`leavedate` > 0 AND
                `team`.`status` = 1 AND
                `history`.`player` =  {$player_id}");

    //If there are teams, save the data
    if($db->iRecords())
        $result['player_previous_teams'] = $db->ArrayResults();
    else
        $result['player_previous_teams'] = array();
    
    
    //Grab comment data
    $query = "SELECT 
            `comments`.`id` as post_id, 
            `comments`.`dateposted` as post_date,
            `comments`.`posterid` as poster_id,
            `comments`.`comment` as poster_comment, 
            `player`.`avatar` as poster_avatar, 
            `player`.`username` as poster_username
        FROM 
            `comments`
        JOIN
            `player` ON `player`.`playerid` = `comments`.`posterid`
        WHERE 
            `comments`.`page` = 'player' AND `comments`.`pageid` = {$player_id} AND `comments`.`parentid` = 0 ORDER BY `comments`.`id` DESC";
    $db->ExecuteSQL($query);
            
    $comments = $db->ArrayResults();
    
    foreach ($comments as $key => $comment)
    {
        $query = "SELECT 
                `comments`.`id` as post_id, 
                `comments`.`dateposted` as post_date,
                `comments`.`posterid` as poster_id,
                `comments`.`comment` as poster_comment, 
                `player`.`avatar` as poster_avatar, 
                `player`.`username` as poster_username 
            FROM 
                `comments`
            JOIN
                `player` ON `player`.`playerid` = `comments`.`posterid`
            WHERE 
                `comments`.`page` = 'player' AND `comments`.`pageid` = {$player_id} AND `comments`.`parentid` = {$comment['post_id']} ORDER BY `comments`.`id` ASC";
        $db->ExecuteSQL($query);
                
        $comments[$key]['poster_comment'] = parseContent($comments[$key]['poster_comment']);
        $comments[$key]['post_replies'] = $db->ArrayResults();
        
        if (is_array($comments[$key]['post_replies'])) {
          foreach($comments[$key]['post_replies'] as $reply) {
            $reply['poster_comment'] = parseContent($reply['poster_comment']);
          }
        }
    }
    
    $return = array('success' => true, 'player' => $result, 'comments' => $comments);
}
else
{
    $return['error'] = "Sorry, data for this player could not be compiled.";
}

returnJSON($return, true);
