<?php

/*
 * @copyright 2012 PlayIGL.com
 * @author Darryl Allen
 * This file scans the HLStats DB for CS:GO stats for
 */

header('Cache-Control: no-cache, must-revalidate');
header('Content-type: application/json');

/*
define ("MYSQL_HOST", "stats-dev.cimukvvq20st.us-east-1.rds.amazonaws.com"); // MySQL Hostname
define ("MYSQL_USER", "stats_dev");	// MySQL Username
define ("MYSQL_PASS", "g3tRAg5D");	// MySQL Password
define ("MYSQL_DATABASE", "stats");	// MySQL Database
*/

define ("MYSQL_HOST", "localhost"); // MySQL Hostname
define ("MYSQL_USER", "root");	// MySQL Username
define ("MYSQL_PASS", "");	// MySQL Password
define ("MYSQL_DATABASE", "stats");	// MySQL Database

define ("BASEURL", "http://localhost/"); #Include trailing slash

require('../includes/functions.php');
require('../model/mysql.class.php');
date_default_timezone_set('UTC');

if(isset($_REQUEST['id']) && is_numeric($_REQUEST['id']))
{
    $matchid = (int)$_REQUEST['id'];
    $schedule = json_decode(file_get_contents(BASEURL."api/schedule/{$matchid}"),true);

    $db = new mysql(MYSQL_DATABASE);
    $date = $schedule['match']['officialdate'];
    $ip = $schedule['match']['ip'];
    $port = $schedule['match']['port'];
    
    $sql = "SELECT serverId FROM hlstats_servers WHERE address = '{$ip}' AND port = '{$port}' LIMIT 1";
    $db->ExecuteSQL($sql);
    $doc = $db->ArrayResult();
    $server_id = $doc['serverId'];
    
    $match_start = date('Y-m-d H:i:s',$date);
    $match_finish = date('Y-m-d H:i:s',$date + 7200);
    
    $sql = "SELECT eventTime FROM hlstats_events_playeractions WHERE eventTime > '{$match_start}' AND eventTime < '{$match_finish}' LIMIT 1";
   
    
    $db->ExecuteSQL($sql); //check if matches for this time exist
    if($db->iRecords())
    {
        //Get times each player was on a team.
        $sql = "SELECT eventTime, COUNT(eventTime) AS halftime FROM hlstats_events_changeteam WHERE eventTime > '{$match_start}' AND eventTime < '{$match_finish}' AND serverId = '{$server_id}' GROUP BY eventTime ORDER BY eventTime DESC LIMIT 1";
        $db->ExecuteSQL($sql); 
        $doc = $db->ArrayResult();
        $halftime = $doc['eventTime'];
        
        
        $sql = "SELECT playerId FROM hlstats_events_changeteam WHERE eventTime >= '{$halftime}' AND eventTime < '{$match_finish}' AND serverId = '{$server_id}' AND team = 'TERRORIST'";
        $db->ExecuteSQL($sql);         
        $team1 = array_flatten_recursive($db->ArrayResults()); //CT Rnd 1, Terrorist Rnd2
        
        $sql = "SELECT playerId FROM hlstats_events_changeteam WHERE eventTime >= '{$halftime}' AND eventTime < '{$match_finish}' AND serverId = '{$server_id}' AND team = 'CT'";
        $db->ExecuteSQL($sql);

        //Flatten the array & remove any stragglers from team changes.
        //Working on assumptions, we use the teams from after half time to assume assignment. Then trim them up.
        $team2 = array_flatten_recursive($db->ArrayResults());   //Terrorist Rnd1, CT round 2
        if(count($team1)>5)
        {
            $team1 = array_diff($team1, $team2);
        }
        if(count($team2)>5)
        {
            $team2 = array_diff($team2, $team1);
        }
        
        $teams = array(
            $schedule['match']['home_name'] => $team1, //CT Rnd 1, Terrorist Rnd2
            $schedule['match']['away_name'] => $team2  //Terrorist Rnd1, CT round 2
            );
        
        $player_details = array();
        //Round 1 CT = Team 1
        //Round 1 T  = Team 2

        //Round 2 CT = Team 2
        //Round 2 T  = Team 1

        $half = array(
            'Round 1' => "eventTime > '{$match_start}' AND eventTime <= '{$halftime}' AND serverId = '{$server_id}'",
            'Round 2' => "eventTime > '{$halftime}' AND eventTime <= '{$match_finish}' AND serverId = '{$server_id}'"
            );

        foreach($half as $round => $filter)
        {
            foreach($teams as $team => $players)
            {
                foreach($players as $key => $playerId)
                {
                    unset($result); //Clear current results
                    $sql = "SELECT name FROM hlstats_playernames WHERE playerID = '{$playerId}'";
                    $db->ExecuteSQL($sql);
                    if($db->iRecords())
                    {
                        //Set ingame name - used for pop-over
                        $stat   = $db->ArrayResult();
                        $result['ingame_name'] = $stat['name'];

                        //Set SteamID & Community ID
                        $sql = "SELECT uniqueId FROM hlstats_playeruniqueids WHERE playerId = '{$playerId}'";
                        $db->ExecuteSQL($sql);
                        $stat   = $db->ArrayResult();
                        $community_id = convert32to64("STEAM_0:".$stat['uniqueId']);
                        $result['steam_id'] = $stat['uniqueId'];
                        $result['community_id'] = $community_id;

                        //Look-up IGL player info using community ID
                        $player_details[$playerId] = json_decode(file_get_contents(BASEURL."api/player/{$community_id}"),true);
                        if($player_details[$playerId]['success'])
                        foreach($player_details[$playerId]['profile'] as $info => $data)
                        {
                            $result[$info] = $data;
                        }


                    }


                    $sql = "SELECT count(killerId) as kills
                    FROM hlstats_events_frags
                    WHERE killerId = '{$playerId}' AND {$filter}
                    GROUP BY killerId";                      
                    $db->ExecuteSQL($sql);
                    if($db->iRecords())
                    {
                        $doc = $db->ArrayResult();
                        $result['kills'] = $doc['kills'];
                    }
                    else
                        $result['kills'] = 0;

                    $sql = "SELECT count(victimId) as deaths
                    FROM hlstats_events_frags
                    WHERE victimId = '{$playerId}' AND {$filter}
                    GROUP BY victimId";
                    $db->ExecuteSQL($sql);
                    if($db->iRecords())
                    {
                        $doc = $db->ArrayResult();
                        $result['deaths'] = $doc['deaths'];
                    }
                    else
                        $result['deaths'] = 0;


                    $sql = "SELECT count(hlstats_events_playeractions.playerId) as count, description, team
                    FROM hlstats_actions
                    JOIN hlstats_events_playeractions ON hlstats_events_playeractions.actionId = hlstats_actions.id
                    JOIN hlstats_players ON hlstats_players.playerId=hlstats_events_playeractions.playerId
                    WHERE hlstats_events_playeractions.playerId = '{$playerId}' AND {$filter}
                    GROUP BY description, hlstats_events_playeractions.playerId";
                    $db->ExecuteSQL($sql);
                    $doc = $db->ArrayResults();
                    foreach($doc as $v)
                    {
                        $result[$v['description']] = $v['count'];
                    } 
                    $player_results[$key] = $result;
                }
                $team_results[$team] = $player_results;
            }
            $round_results[$round] = $team_results;
        }
    }
    else
    {
        $round_results = array('success' => false, 'message' => "That match could not be found.");
    }
}
else
{
    $round_results = array('success' => false, 'message' => "Some parameters appear to be missing.");
}

prettyJSON(json_encode($round_results));
?>