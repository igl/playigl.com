<?php

/**
 * Registration Script for Public Website
 * @author Kevin Langlois*
 * @copyright Copyright 2012 PlayIGL.com
 */

header('Cache-Control: no-cache, must-revalidate');
header('Content-type: application/json');

require('../includes/config.php');
require('../includes/functions.php');
require('../model/mysql.class.php');


$return = array();

$db = new mysql(MYSQL_DATABASE);

        if (!empty($_GET['username'])) {
            $query = "SELECT `username` FROM `".MYSQL_DATABASE."`.`player` WHERE `username` = '".$_GET['username']."'";
            $db->ExecuteSQL($query);
            if($db->iRecords()) {
                $data = $db->ArrayResult();
                $return['is_taken'] = true;
                $return['username'] = $_GET['username'];
            } else {
                $return['is_taken'] = false;
                $return['username'] = $_GET['username'];
            }
        } else {
            $return['error'] = "No username provided.";
        }

echo $_GET['callback'].'(';
echo prettyJSON(json_encode($return));
echo ')';

