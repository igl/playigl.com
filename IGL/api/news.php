<?php
/**
 * Fetches news posts
 * @author Darryl Allen <dallen@palyigl.com>
 * @author Shaun Delaney <sdelaney@playigl.com>
 * @return JSON news list
 * @copyright Copyright 2013 PlayIGL.com
 */

include_once('../includes/config.php');
include_once('../model/mysql.class.php');
include_once('../includes/functions.php');

//Default error message
$return = array("success" => false, "error" => "Invalid Request.");

//Variables
$id         = isset($_REQUEST['id']) ? $_REQUEST['id'] : null;

$db = new mysql(MYSQL_DATABASE);

if (!is_null($id) && is_numeric($id)) {
    $db->ExecuteSQL(
        "SELECT * FROM news WHERE id = {$id}"
    );
        
    if($db->iRecords())
        $return = array('success' => true, 'post' => $db->ArrayResult());
    else
        $return['error'] = "No data retrieved from news.";
} else {
    $db->ExecuteSQL(
        "SELECT * FROM news"
    );

    if($db->iRecords())
        $return = array('success' => true, 'post' => $db->ArrayResults());
    else
        $return['error'] = "Sorry, data could not be compiled";
}

header('Cache-Control: no-cache, must-revalidate');
returnJSON($return);
?>