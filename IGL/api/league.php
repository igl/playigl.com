<?php
/**
 * Fetches league details
 * @author Darryl Allen <dallen@palyigl.com>
 * @author Shaun Delaney <sdelaney@playigl.com>
 * @return JSON league list
 * @copyright Copyright 2013 PlayIGL.com
 */

include_once('../includes/config.php');
include_once('../model/mysql.class.php');
include_once('../includes/functions.php');

//Default error message
$return = array("success" => false, "error" => "Invalid League Request.");

//Variables
$id         = isset($_REQUEST['id']) ? $_REQUEST['id'] : null;
$fetch      = isset($_REQUEST['fetch']) ? strtolower($_REQUEST['fetch']) : null;

$db = new mysql(MYSQL_DATABASE);

if (!is_null($id) && is_numeric($id)) {
    $db->ExecuteSQL(
        "SELECT 
            game.id as game_id, 
            game.name as game_name, 
            league.id as league_id, 
            league.name as league_name, 
            game.about as about,
            game.banner as banner,
            league.order, 
            league.format, 
            league.rounds, 
            league.day, 
            league.time, 
            league.currentseason,
            league.currentweek,
            league.totalweeks,
            league.livedate,
            league.rules,
            league.status
        FROM 
            league 
        JOIN 
            game ON game.id=league.game 
        WHERE 
            league.id = {$id} AND league.deleted = 0"
    );
            
    if ($db->iRecords())
        $return = array('success' => true, 'league' => $db->ArrayResult());
    else
        $return['error'] = "That league does not exist.";
}
elseif (!is_null($fetch) && in_array($fetch, array('all'))) {
    $db->ExecuteSQL(
        "SELECT 
            `league`.`id` as league_id, 
            `league`.`name` as league_name, 
            `league`.`game` as game_id, 
            `game`.`name` as game_name, 
            `league`.`status`
        FROM 
            `league` 
        JOIN 
            `game` ON `game`.`id` = `league`.`game`
        WHERE
            `league`.`status` = 1 AND `league`.`deleted` = 0
        ORDER BY `league`.`status` DESC"
    );
    
    if ($db->iRecords())
        $return = array('success' => true, 'leagues' => $db->ArrayResults());
    else
        $return['error'] = "No leagues are presently active.";
}

header('Cache-Control: no-cache, must-revalidate');
returnJSON($return);
?>