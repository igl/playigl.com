<?php
/*
 * @copyright 2012 PlayIGL.comT
 * @author Darryl Allen
 * This file retrieves ladder info
 * http://localhost/api/ladders
 * http://localhost/api/ladder/1 
 * http://localhost/api/ladder/teams/1
 */

header('Cache-Control: no-cache, must-revalidate');
header('Content-type: application/json');


include('./model/mysql.class.php');
include('./model/mysql.sync.php');
require('./includes/config.php');
require('./includes/functions.php');

$db = new MySQL();

$db->ExecuteSQL("SELECT * FROM matchhistory");
if($db->iRecords()) {
    $r = $db->ArrayResults();
    foreach($r as $h) {
        print_r($h);
        $db->Update('schedule', array(
            'winner' => $h['winner'],
            'loser' => $h['loser'],
            'home_score' => $h['home_results'],
            'away_score' => $h['away_results']
        ), array('id' => $h['scheduleid']));
    }
}


?>