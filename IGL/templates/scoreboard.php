<?
if(IGL_PAGE!=true)die();
?>
<div id="headerwrap">
 
 <div class="row">
  
 <? foreach($scoreboard['matches'] as $key => $value)if ($key < 6) { ?>
  <div class="span2 scoreboard<? if($key < 1)echo "-front"; ?>">
      <a href="#" class="">
      <div id="score-button" class="scorebutton shadow"> 
      <div id="score-logo" class="pull-left"><? echo $value['icon']; ?></div><div id="score-date"><? echo date('M jS - G:i',strtotime($value['officialdate'])); ?></div>
       <div id="score-line"><img src="./img/line-scoreboard.png"></div>        
          <div id="score-team-wrap">
           <div id="score-team" class="pull-left"><? echo $value['homename']; ?></div>
           <div id="score-number" class="pull-right">0</div>
         </div>
         <div id="score-team-wrap">
          <div id="score-team" class="pull-left"><? echo $value['awayname']; ?></div> 
          <div id="score-number" class="pull-right">0</div>
         </div>
         <div id="score-level-status-wrap">
          <div id="score-level" class="pull-left"><? echo $value['map']; ?></div>
          <div id="score-level" class="pull-right"><!---Second Half ---></div>
         </div> 
      </div></a>
  </div>
<? }?> 
  
   <div class="span1 scoreboard-end">
       <a href="#" class="score-button-end">
       <div id="score-button-end" class=" shadow pull-left">
           
           <div id="score-number-matches"><? echo count($scoreboard['matches']); ?></div>
           
           <div id="score-total">total</div>
           
       </div></a></div>
  

 
  
</div>
</div>