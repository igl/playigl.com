<?php
if(IGL_PAGE!=true) die();
?>
<!DOCTYPE HTML>
<html>
<head>
    <title>International Gaming League - <?php echo $page_vars['title'] ?></title>
    <meta name="description" content="Your destination for Amateur eSports. Divisional leagues, tiered ladders and weekend tournaments. Join today!" />
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
    <?php echo $meta_keywords; ?>
    <link href="/favicon.ico" rel="Shortcut Icon"  type="image/x-icon" />
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400" rel="stylesheet" type="text/css" />
    <link href="http://fonts.googleapis.com/css?family=Headland+One" rel="stylesheet" type="text/css" />
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet" type="text/css" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
    <link href="/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="/css/jquery.liveSearch.css" rel="stylesheet" type="text/css" />
    <link href="/css/jquery.gritter.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="http://tarruda.github.com/bootstrap-datetimepicker/assets/css/bootstrap-datetimepicker.min.css">
    <?php echo $css_includes; ?> 
    <!-- Segment.io Analytics Engine -->
    <?php require('includes/tracking.php'); ?>
    <script type="text/javascript">var analytics=analytics||[];(function(){var e=["identify","track","trackLink","trackForm","trackClick","trackSubmit","page","pageview","ab","alias","ready","group"],t=function(e){return function(){analytics.push([e].concat(Array.prototype.slice.call(arguments,0)))}};for(var n=0;n<e.length;n++)analytics[e[n]]=t(e[n])})(),analytics.load=function(e){var t=document.createElement("script");t.type="text/javascript",t.async=!0,t.src=("https:"===document.location.protocol?"https://":"http://")+"d2dq2ahtl5zl1z.cloudfront.net/analytics.js/v1/"+e+"/analytics.min.js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(t,n)};analytics.load("ia4hmptjr2");</script>
    <link rel="stylesheet" href="/css/stylesheet.css" />
</head>
<body>
    <?php include_once('./templates/msg_bar.php'); ?>
    <header>
        <div class="wrap">
            <div class="row" style="margin-bottom:14px;">
                <div class="span6"><a href="<?php echo BASEURL; ?>"><img src="<?php echo BASEURL; ?>img/igl_logo.png" alt="International Gaming League" /></a></div>
                <div class="span6">
                    <form id="searchBox" action="" class="search">
	<!-- We'll have a button that'll make the search input appear, a submit button and the input -->
	
	<!-- Alos, a label for it so that we can style it however we want it-->
	<input id="submit" value="" type="submit">
	<label for="submit" class="submit"><i style="width:17px !important; height:18px !important;" class="icon-search"></i></label>
	
	<!-- trigger button and input -->
	<a href="javascript: void(0)" class="icon"><i class="icon-search"></i></a>
	<input type="search" name="q" id="search" placeholder="Search Players, Teams & Articles">
        
</form>
                    <div style="margin-top:-40px;" id="jquery-live-search"></div>
                </div>
            </div>

            <div class="row">
                <div class="span8">
                    <div class="btn-group pull-left">
                        <a href="/home" class="btn btn-large">HOME</a> 
                        <a class="btn btn-large dropdown-toggle" data-toggle="dropdown">GAMES</a>
                         <ul class="dropdown-menu">
                             <?php foreach($navi as $g) { 
                             echo "<li><a href='/game/{$g['name']}'>{$g['name']}</a></li>";
                             }
                             ?>
                         </ul>
                        <a href="/forums" class="btn btn-large">FORUMS</a> <a href="http://support.playigl.com" class="btn btn-large">SUPPORT</a>
                    </div>
                </div>
                <div class="span4">
                    <div class="btn-toolbar">
                        <?php if(isset($_SESSION['playerid']) && $_SESSION['playerid'] > 0) { ?>
                        
                        <div class="btn-group pull-right" style="text-align: left">
                            <a href="<?php echo "/player/{$_SESSION['username']}"; ?>" class="btn btn-large"><?php echo $_SESSION['username']; ?></a>
                            <a class="btn btn-large dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="/dashboard/profile/"><i class="icon-user"></i> My Profile</a></li>
                                <li><a href="/dashboard/teams/"><i class="icon-group"></i> My Teams</a></li>
                                <li><a href="/dashboard/matches/"><i class="icon-calendar"></i> My Matches</a></li>
                                <li><a href="/dashboard/matches/"><i class="icon-share-alt"></i> My Referrals</a></li>
                                <li><a href="/dashboard/matches/"><i class="icon-check"></i> My Followers</a></li>
                                <?php
                                    $usergroups = explode(",", $_SESSION['membergroupids']);
                                    if((bool) array_intersect(array(6,10,12), $usergroups) || $_SESSION['usergroupid']==6) {
                                ?>
                                <li><a href='/admin/'><i class='icon-cogs'></i> Admin Panel</a></li>
                                <li class="divider"></li>
                                <?php }
                                $user_teams = json_decode(file_get_contents(BASEURL."api/player/{$_SESSION['playerid']}"),true);
                                if(isset($user_teams['teams']) && is_array($user_teams['teams']))
                                {
                                    foreach($user_teams['teams'] as $value) 
                                    {
                                        echo "<li id='teamlist_{$value['id']}'><a href='/team/{$value['id']}-{$value['name']}'><i class='icon-group'></i> {$value['name']}</a></li>";
                                    }
                                    echo '<li class="divider"></li>';
                                } ?>
                                <li><a href="/logout.php"><i class="icon-signout"></i> Logout</a></li>
                            </ul>
                        </div>
                        <div class="btn-group pull-right" style="display:none; margin-right:10px; text-align: left">
                            <div class="btn btn-red btn-large">Tour</div>
                            <div class="btn btn-red btn-large" style="padding:9px 5px;"><i class="icon-remove"></i></div>
                        </div>
                        <?php } else { ?>
                        <ul class="nav pull-right">
                            <li class="dropdown" style="float:left;">
                                <a class="btn btn-large" href="/register">Register</a>
                            </li>
                            <li class="dropdown" style="float:left; padding-left: 12px;">
                                <a class="btn btn-large btn-red" href="/login">Login</a>
                            </li>
                        </ul>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </header>        
    <div class="wrap">            
            <?php if($_GET['page']!='dashboard' && !basename(getcwd())=="admin") include('scoreboard.php'); ?>
            <div class="row">