
</div>
</div><!-- Close wrapper -->
<div class="footer">
    <div class="headerwrap">
        <div class="row">
            <!-- Column 1 -->
            <div class="span4">
                <div id="footer-logo"><a href="/"><img alt="IGL Logo" src="<?php echo BASEURL; ?>img/igl_logo_footer.png" /></a></div>
                <div id="footer-social" class="pull-left">
                    <a href=" http://www.facebook.com/playigl"><img alt="Facebook" src="<?php echo BASEURL; ?>img/facebook.png" /></a>
                    <a href="http://www.twitter.com/playigl"><img alt="Twitter" src="<?php echo BASEURL; ?>img/twitter.png" /></a>
                    <a href="http://steamcommunity.com/groups/playigl"><img alt="Stea," src="<?php echo BASEURL; ?>img/steam.png" /></a>
                    <a style="padding-top:5px;" href="http://mixpanel.com/f/partner"><img src="http://mixpanel.com/site_media/images/partner/badge_blue.png" alt="Mobile Analytics" /></a>
                </div>
                <div id="social-registered" class="pull-left">
                    <a style="display:block;" href="/online">0 Registered Users Online</a>
                </div>
            </div>
            <!--/ Column 1 -->

            <!-- Column 2 -->
            <div class="span4">
                <div id="footer-middle">
                    <h4 class="who">Who are you?  What is this?</h4>
                    <p class="who_info">International Gaming League is your amateur destination for competitive eSports. Elevate your game in league matches or watch your favourite teams duke it out live. All within a context backed up by comprehensive game statistics.</p>
                </div>
            </div>
            <!--/ Column 2 -->

            <!-- Column 3 -->
            <div class="span4">
                <div class="row">
                    <!-- Nested Column 1-->
                    <div class="span1"><img alt="Line" src="<?php echo BASEURL; ?>img/footer-list-line.png" width="2" height="68" /></div>
                    <!--/ Nested Column 1-->

                    <!-- Nested Column 2-->
                    <div class="span1">
                        <ul class="unstyled">
                            <li><a href="/about" class="footerlink">About</a></li>
                            <li><a href="/press" class="footerlink">Press</a></li>
                            <li><a href="mailto:info@playigl.com" class="footerlink">Contact</a></li>
                            <li><a href="/partners-faq/" class="footerlink">FAQ</a></li>
                            <li><a href="/careers" class="footerlink">Careers</a></li>
                            <li><a href="/developers" class="footerlink">Developers</a></li>
                            <li><a href="/investors" class="footerlink">Investors</a></li>
                        </ul>
                    </div>
                    <!--/ Nested Column 2-->

                    <!-- Nested Column 3-->
                    <div class="span2">
                        <ul class="unstyled">
                            <li><a class="footerlink" href="/partners">Partners</a></li>
                            <li><a class="footerlink" href="/advertise">Advertise</a></li>
                            <li><a class="footerlink" href="/tos">Terms of Service</a></li>
                            <li><a class="footerlink" href="/legal">Legal</a></li>
                            <li><a class="footerlink" href="/help">Help</a></li>
                            <li><a class="footerlink" href="/premium">Premium</a></li>
                            <li><a class="footerlink" href="/staff">Staff</a></li>
                        </ul>
                    </div>
                    <!--/ Nested Column 3-->
                </div>
            </div>
            <!--/ Column 3 -->
        </div>
    </div>
</div>
<div id="userInfo" data-email="<?php echo $_SESSION['email']; ?>" data-username="<?php echo $_SESSION['username']; ?>" data-country="<?php echo $_SESSION['country']; ?>" data-gender="<?php echo $_SESSION['sex']; ?>" data-birthday="<?php echo $_SESSION['birthday']; ?>" data-joindate="<?php echo $_SESSION['joindate']; ?>" data-facebook="<?php echo $_SESSION['facebook']; ?>" data-twitter="<?php echo $_SESSION['twitter']; ?>" data-steam="<?php echo $_SESSION['steam']; ?>" data-twitch="<?php echo $_SESSION['twitch']; ?>"></div>

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js"></script>
<script type="text/javascript" src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="//js.pusher.com/1.12/pusher.min.js"></script>

<?php
    
    $scripts = array(
'/js/jquery.liveSearch.js',
'/js/register.js',
'/js/cookie.js',
'/js/authenticate.js',
'/js/md5.js',

'/js/notifications/jquery.gritter.min.js',
'/js/notifications/pushernotifier.js',
'/js/global.js',
'/js/ganalytics.js',
'/js/seo.js',
'/js/simpletip.min.js',
            );
foreach($scripts as $js) {
    $cache = filemtime(getcwd().$js);
    echo "<script type=\"text/javascript\" src=\"{$js}?{$cache}\"></script>\n";
}
    ?>
<script type="text/javascript" src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.min.js"></script>
<?php echo $js_includes; ?>
<script type="text/javascript">
    $("document").ready(function() {
        //initUserFox();
        initNav();
        initStream();
        initSearch();
        //initGoogle();
        initUserVoice();
        //PlayIGL.initTooltip();
        initPusher();
        <?php echo $js_functions; ?>
<?php
if (isset($_SESSION['playerid'])) {
    echo "analytics.identify({id: '".$_SESSION['playerid']."', email : '".$_SESSION['email']."', created_at : '".$_SESSION['joindate']."', username: '".$_SESSION['username']."'});";
}
?>
});
</script>
<?php 
include('./includes/activity.php'); 
?>
</body>
</html>
