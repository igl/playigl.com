<?php
if(IGL_PAGE!=true)die();
?>
<div>
<div class="box pull-right" style="width:280px"><h1>CONTROL PANEL</h1>
<div class="accordion" id="accordion2">
<!-- accordion group 1 -->

<div class="accordion-group">
<div class="accordion-heading">
<div class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#manage-games">
 <div class="thumbnail gameicon pull-left"><img src='../img/igl_icon.png'></div>
 <div class="accordion-title">MANAGE GAMES</div>
</div>
</div>
<div id="manage-games" class="accordion-body collapse">
<div class="accordion-inner">
<ul class="unstyled">
    <li><div class="accordion-division"><div class="accordion-division-highlight"><a href="?page=addgame">Add New</a></div></div></li>
    <li><div class="accordion-division"><div class="accordion-division-highlight"><a href="?page=allgames">All Games</a></div></div></li>
    <li><div class="accordion-division"><div class="accordion-division-highlight"><a href="?page=editservers">Match Servers</a></div></div></li>
    <li><div class="accordion-division"><div class="accordion-division-highlight"><a href="?page=addconfigs">Add Match Configs</a></div></div></li>
    <li><div class="accordion-division"><div class="accordion-division-highlight"><a href="?page=editconfigs">Edit Match Configs</a></div></div></li>
</ul>
</div>
</div>
</div>

<div class="accordion-group">
<div class="accordion-heading">
<div class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#manage-leagues">
 <div class="thumbnail gameicon pull-left"><img src='../img/igl_icon.png'></div>
 <div class="accordion-title">MANAGE LEAGUES</div>
</div>
</div>
<div id="manage-leagues" class="accordion-body collapse">
<div class="accordion-inner">
<ul class="unstyled">
    <li><div class="accordion-division"><div class="accordion-division-highlight"><a href="?page=addleague">Add League</a></div></div></li>
    <li><div class="accordion-division"><div class="accordion-division-highlight"><a href="?page=editleague">Edit Existing Leagues</a></div></div></li>
    <li><div class="accordion-division"><div class="accordion-division-highlight"><a href="?page=matchtypes">Edit Match Types</a></div></div></li>
    <li><div class="accordion-division"><div class="accordion-division-highlight"><a href="?page=editmaps">Edit League Maps</a></div></div></li>
    <li><div class="accordion-division"><div class="accordion-division-highlight"><a href="?page=createschedule">Create & Post League Schedule</a></div></div></li>
    <li><div class="accordion-division"><div class="accordion-division-highlight"><a href="?page=editschedule">Edit Existing Schedule</a></div></div></li>
    <li><div class="accordion-division"><div class="accordion-division-highlight"><a href="?page=addmatchresult">Add Match Results</a></div></div></li>
    <li><div class="accordion-division"><div class="accordion-division-highlight"><a href="?page=editmatchresult">Edit Existing Match Results</a></div></div></li>     
    <li><div class="accordion-division"><div class="accordion-division-highlight"><a href="?page=addban">Ban a Player</a></div></div></li>
    <li><div class="accordion-division"><div class="accordion-division-highlight"><a href="?page=editbans">Edit Current League Bans</a></div></div></li>
</ul>
</div>
</div>
</div>

<div class="accordion-group">
<div class="accordion-heading">
<div class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#manage-ladders">
 <div class="thumbnail gameicon pull-left"><img src='../img/cup.jpg'></div>
 <div class="accordion-title">MANAGE LADDERS</div>
</div>
</div>
<div id="manage-ladders" class="accordion-body collapse">
<div class="accordion-inner">
<ul class="unstyled">
    <li><div class="accordion-division"><div class="accordion-division-highlight"><a href="?page=createladder">Create New Ladder</a></div></div></li>
    <li><div class="accordion-division"><div class="accordion-division-highlight"><a href="?page=allladders">View All Ladders</a></div></div></li>
</ul>
</div>
</div>
</div>

<div class="accordion-group">
<div class="accordion-heading">
<div class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#manage-tournaments">
 <div class="thumbnail gameicon pull-left"><img src='../img/cup.jpg'></div>
 <div class="accordion-title">MANAGE TOURNAMENTS</div>
</div>
</div>
<div id="manage-tournaments" class="accordion-body collapse">
<div class="accordion-inner">
<ul class="unstyled">
    <li><div class="accordion-division"><div class="accordion-division-highlight"><a href="?page=createtournament">Create New Tournament</a></div></div></li>
    <li><div class="accordion-division"><div class="accordion-division-highlight"><a href="?page=alltournaments">View All Tournament</a></div></div></li>
    <li><div class="accordion-division"><div class="accordion-division-highlight"><a href="?page=managetournament">Manage Teams</a></div></div></li>
    <li><div class="accordion-division"><div class="accordion-division-highlight"><a href="?page=tournamentresults">Manage Results</a></div></div></li> 
</ul>
</div>
</div>
</div>

             
<div class="accordion-group">
<div class="accordion-heading">
<div class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse3">
<div class="thumbnail gameicon pull-left"><img src='../img/team_icon.png'></div>
<div class="accordion-title">MANAGE PLAYERS & TEAMS</div>
</div>
</div>
<div id="collapse3" class="accordion-body collapse">
<div class="accordion-inner">
<ul class="unstyled">
    <li><div class="accordion-division"><div class="accordion-division-highlight"><a href="?page=addteam">Create a New Team</a></div></div></li>
    <li><div class="accordion-division"><div class="accordion-division-highlight"><a href="?page=editteam">Edit Existing Teams</a></div></div></li>
    <li><div class="accordion-division"><div class="accordion-division-highlight"><a href="?page=createuser">Create Player/User Account</a></div></div></li>
    <li><div class="accordion-division"><div class="accordion-division-highlight"><a href="?page=addfreeagent">Add Existing User to Free Agents</a></div></div></li>
    <li><div class="accordion-division"><div class="accordion-division-highlight"><a href="?page=editfreeagent">Edit Existing Free Agents</a></div></div></li>
  </ul>
</div>
</div>
</div>


<div class="accordion-group">
<div class="accordion-heading">
<div class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse4">
<div class="thumbnail gameicon pull-left"><img src='../img/news_icon.png'></div>
<div class="accordion-title">MANAGE NEWS & ARTICLES</div>
</div>
</div>
<div id="collapse4" class="accordion-body collapse">
<div class="accordion-inner">
<ul class="unstyled">
    <li><div class="accordion-division"><div class="accordion-division-highlight"><a href="?page=addnews">Write a News Post</a></div></div></li>
    <li><div class="accordion-division"><div class="accordion-division-highlight"><a href="?page=editnews">Edit Existing News</a></div></div></li>
    <li><div class="accordion-division"><div class="accordion-division-highlight"><a href="?page=addarticle">Write an Article</a></div></div></li>
    <li><div class="accordion-division"><div class="accordion-division-highlight"><a href="?page=editarticle">Edit Existing Articles</a></div></div></li>
</ul>
</div>
</div>
</div>

<div class="accordion-group">
<div class="accordion-heading">
<div class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse5">
 <div class="thumbnail gameicon pull-left"><img src='../img/joystick.png'></div>
 <div class="accordion-title">EMAIL TEMPLATES</div>
</div>
</div>
<div id="collapse5" class="accordion-body collapse">
<div class="accordion-inner">
<ul class="unstyled">
    <li><div class="accordion-division"><div class="accordion-division-highlight"><a href="?page=emailtemplates">Manage Email Templates</a></div></div></li>
</ul>
</div>
</div>
</div>

<div class="accordion-group accordion-last">
<div class="accordion-heading">
<div class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse6">
 <div class="thumbnail gameicon pull-left"><img src='../img/joystick.png'></div>
 <div class="accordion-title">BROADCASTS</div>
</div>
</div>
<div id="collapse6" class="accordion-body collapse">
<div class="accordion-inner">
<ul class="unstyled">
    <li><div class="accordion-division"><div class="accordion-division-highlight"><a href="?page=selectbroadcasts">Select Broadcasts</a></div></div></li>
</ul>
</div>
</div>
</div>


<div class="accordion-group">
<div class="accordion-heading">
<div class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse7">
 <div class="thumbnail gameicon pull-left"><img src='../img/joystick.png'></div>
 <div class="accordion-title">Footer Pages</div>
</div>
</div>
<div id="collapse7" class="accordion-body collapse">
<div class="accordion-inner">
<ul class="unstyled">
    <li><div class="accordion-division"><div class="accordion-division-highlight"><a href="?page=managefooterpages">Manage Footer Pages</a></div></div></li>
</ul>
</div>
</div>
</div>
         
</div>
</div>
    </div>
        
<script type="text/javascript">
    $(function() {
        $('#accordion2').siblings('.accordion-toggle').slideUp();
    });
</script>