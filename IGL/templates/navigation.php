<?php
if (IGL_PAGE != true)
    die();
$navigation = new navigation();
$advert = $navigation->ads();
?>
<div class="span4">
    <h2 class="bar">GAMES <i class='icon-globe'></i></h2>
    <?php
    if (isset($advert) && is_array($advert)) {
        #echo "<a href='" . $advert['href'] . "'><img src='" . $advert['image_data'] . "' /></a><br>";
    }
    ?>
    <div class="box">
        <div class="accordion">
            <?php
            if (is_array($navi))
                foreach ($navi as $g) {
                    ?>
                    <a href="/game/<?php echo $g['name']; ?>"><div class='accordion-group'>
                            <div class='accordion-heading'>
                                <div class='thumbnail gameicon pull-left'><img src='/img<?php echo $g['logo']; ?>'></div> 
                                <div class='accordion-title'><?php echo $g['name']; ?></div>
                                <div class='accordion-teams'><?php echo $g['num_teams']; ?> Teams</div>
                            </div>
                        </div></a>
                    <?php
                }
            ?>
        </div>     
    </div>        

    <!-- VIDEO --> 
    <h2 class="bar">IGL TV
        <a style="position:relative; left:160px; bottom:3px" onclick='videoPopout()'>
            <span id="twitchStatus" style="display:none" class="label label-badge">Offline</span>
        </a>
    </h2>
    <div class="box">
        <object type="application/x-shockwave-flash" height="184" width="302" id="live_embed_player_flash" data="http://www.twitch.tv/widgets/live_embed_player.swf?channel=igl1" bgcolor="#000000"><param name="allowFullScreen" value="true" /><param name="allowScriptAccess" value="always" /><param name="allowNetworking" value="all" /><param name="movie" value="http://www.twitch.tv/widgets/live_embed_player.swf" /><param name="flashvars" value="hostname=www.twitch.tv&channel=igl1&auto_play=true&start_volume=25" /></object>
    </div>
</div>