<?php include '../templates/landing/header.php'; ?>
<?php include '../templates/landing/subnav.php'; ?>


<div class="content-wrap">
	<div class="container">
		<div class="row">
			<div class="span3 sidebar">
				<ul class="sub-menu">
					<li><a href="/about">About IGL</a></li>
					<li><a href="/staff">Staff</a></li>
					<li><a href="/inthenews">In The News</a></li>
					<li><a href="/press">Press Releases</a></li>
					<li><a href="/press-kit" class="active">Press Kit</a></li>
				</ul>
				<?php include '../templates/landing/sidebar-bottom.php'; ?>
			</div>
			<div class="span9 content press-kit">
				<h1>IGL Press Kit</h1>
				<h4 class="oath">
					<div>
						Please raise your right hand and repeat after me...
					</div>
					<div>
						“I, (insert your name), agree to respect the assets of International Gaming League and abide by the rules below prior to downloading. If I have any questions, I shall contact IGL at <a href="mailto:press@playigl.com">press@playigl.com</a> for clarification. If I ignore or refuse to follow these rules, IGL can revoke my right to use these at any time.”
					</div>
					Thank you. You may now proceed...
				</h4>
				<div class="clearfix">
					<div class="span-left">
						<img src="/img/landing/press-kit/igl-logo-light.png" />
						<span class="download"><a href="/img/landing/press-kit/igl_vector_white_mar2013.eps">Download EPS</a></span>
						<h4>Please respect our logo by..</h4>
						<ul>
							<li>Using this logo on light or white backgrounds.</li>
							<li>Not removing any elements of the logo or modifying the font.</li>
							<li>Making sure there is enough contrast between our logo and the background.</li>
							<li>Never rotating or using it vertically.</li>
							<li>Giving our logo enough breathing room.</li>
						</ul>
					</div>
					<div class="span-right">
						<img src="/img/landing/press-kit/igl-logo-dark.png" />
						<span class="download"><a href="/img/landing/press-kit/igl_vector_dark_mar2013.eps">Download EPS</a></span>
						<h4>Dark Version</h4>
						<ul>
							<li>Use this logo on dark backgrounds.</li>
							<li>Please make sure there is enough contrast between our logo and the background.</li>
							<li>In a circumstance where both logos work, please use the light background version.</li>
						</ul>
					</div>
				</div>
				<a class="download-all btn btn-large btn-red" href="/img/landing/press-kit/igl-logos-vector.zip">Download All IGL Logos</a>
				<p>&nbsp;</p>
				<h4>IGL Brand Colors</h4>
				<ul class="brand-colors">
					<li>
						<div class="red-dark color-box">&nbsp;</div>
						IGL Red<br />
						HEX: #6e0000
					</li>
					<li>
						<div class="red-light color-box">&nbsp;</div>
						IGL Light Red<br />
						HEX: #ba1a1a
					</li>
					<li>
						<div class="black color-box">&nbsp;</div>
						IGL Black<br />
						HEX: #000000
					</li>
					<li class="last">
						<div class="white color-box">&nbsp;</div>
						IGL White<br />
						HEX: #FFFFFF
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>

<?php include '../templates/landing/footer.php'; ?>