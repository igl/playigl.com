<?php include '../templates/landing/header.php'; ?>
<?php include '../templates/landing/subnav.php'; ?>


<div class="content-wrap">
	<div class="container">
		<div class="row">
			<div class="span3 sidebar">
				<ul class="sub-menu">
					<li><a href="/faq">Legal</a></li>
					<li><a href="/privacypolicy" class="active">Privacy Policy</a></li>
				</ul>
				<?php include '../templates/landing/sidebar-bottom.php'; ?>
			</div>
			<div class="span9 content">
				<h1>Privacy Policy</h1>
<p>This   Privacy Policy governs the manner in which International Gaming League   collects, uses, maintains and discloses information collected from users   (each, a &quot;User&quot;) of the www.playigl.com website (&quot;Site&quot;). This privacy   policy applies to the Site and all products and services offered by   International Gaming League.</p>
<p>1.3i Personal identification information</p>
<p>We   may collect personal identification information from Users in a variety   of ways, including, but not limited to, when Users visit our site,   register on the site, place an order, and in connection with other   activities, services, features or resources we make available on our   Site. Users may be asked for, as appropriate, name, email address,   mailing address, phone number. Users may, however, visit our Site   anonymously. We will collect personal identification information from   Users only if they voluntarily submit such information to us. Users can   always refuse to supply personally identification information, except   that it may prevent them from engaging in certain Site related   activities. </p>
<p>1.3ii Non-personal identification information </p>
<p>We   may collect non-personal identification information about Users   whenever they interact with our Site. Non-personal identification   information may include the browser name, the type of computer and   technical information about Users means of connection to our Site, such   as the operating system and the Internet service providers utilized and   other similar information.</p>
<p>1.3iii Web browser cookies</p>
<p>Our   Site may use &quot;cookies&quot; to enhance User experience. User's web browser   places cookies on their hard drive for record-keeping purposes and   sometimes to track information about them. User may choose to set their   web browser to refuse cookies, or to alert you when cookies are being   sent. If they do so, note that some parts of the Site may not function   properly.</p>
<p>1.3iv How we use collected information</p>
<p>International Gaming League collects and uses Users personal information for the following purposes:</p>
<p>    - To personalize user experience</p>
<p>      We may use information in the aggregate to understand how our Users as a   group use the services and resources provided on our Site.</p>
<p>    - To improve our Site</p>
<p>    We continually strive to improve our website offerings based on the information and feedback we receive from you.</p>
<p>    - To improve customer service</p>
<p>    Your information helps us to more effectively respond to your customer service requests and support needs.</p>
<p>    - To process transactions</p>
<p>      We may use the information Users provide about themselves when placing   an order only to provide service to that order. We do not share this   information with outside parties except to the extent necessary to   provide the service.</p>
<p>    - To administer a content, promotion, survey or other Site feature</p>
<p>    To send Users information they agreed to receive about topics we think will be of interest to them.</p>
<p>    - To send periodic emails</p>
<p>      The email address Users provide for order processing, will only be used   to send them information and updates pertaining to their order. It may   also be used to respond to their inquiries, and/or other requests or   questions.</p>
<p>How we protect your information</p>
<p>We   adopt appropriate data collection, storage and processing practices and   security measures to protect against unauthorized access, alteration,   disclosure or destruction of your personal information, username,   password, transaction information and data stored on our Site.</p>
<p>1.3v Sharing your personal information</p>
<p>We   do not sell, trade, or rent Users personal identification information   to others. We may share generic aggregated demographic information not   linked to any personal identification information regarding visitors and   users with our business partners, trusted affiliates and advertisers   for the purposes outlined above.</p>
<p>1.3vi Third party websites</p>
<p>Users   may find advertising or other content on our Site that link to the   sites and services of our partners, suppliers, advertisers, sponsors,   licensors and other third parties. We do not control the content or   links that appear on these sites and are not responsible for the   practices employed by websites linked to or from our Site. In addition,   these sites or services, including their content and links, may be   constantly changing. These sites and services may have their own privacy   policies and customer service policies. Browsing and interaction on any   other website, including websites which have a link to our Site, is   subject to that website's own terms and policies.</p>
<p>1.3vii Advertising</p>
<p>Ads   appearing on our site may be delivered to Users by advertising   partners, who may set cookies. These cookies allow the ad server to   recognize your computer each time they send you an online advertisement   to compile non personal identification information about you or others   who use your computer. This information allows ad networks to, among   other things, deliver targeted advertisements that they believe will be   of most interest to you. This privacy policy does not cover the use of   cookies by any advertisers.</p>
<p>1.3viii Google Adsense</p>
<p>Some   of the ads may be served by Google. Google's use of the DART cookie   enables it to serve ads to Users based on their visit to our Site and   other sites on the Internet. DART uses &quot;non personally identifiable   information&quot; and does NOT track personal information about you, such as   your name, email address, physical address, etc. You may opt out of the   use of the DART cookie by visiting the Google ad and content network   privacy policy at http://www.google.com/privacy_ads.html</p>
<p>1.3ix Compliance with children's online privacy protection act</p>
<p>Protecting   the privacy of the very young is especially important. For that reason,   we never collect or maintain information at our Site from those we   actually know are under 13, and no part of our website is structured to   attract anyone under 13.</p>
<p>1.3x Changes to this privacy policy</p>
<p>International   Gaming League has the discretion to update this privacy policy at any   time. When we do, we will revise the updated date at the bottom of this   page. We encourage Users to frequently check this page for any changes   to stay informed about how we are helping to protect the personal   information we collect. You acknowledge and agree that it is your   responsibility to review this privacy policy periodically and become   aware of modifications.</p>
<p>1.3xi Your acceptance of these terms</p>
<p>By   using this Site, you signify your acceptance of this policy. If you do   not agree to this policy, please do not use our Site. Your continued use   of the Site following the posting of changes to this policy will be   deemed your acceptance of those changes.</p>
<p>1.3xii Contacting us</p>
<p>If   you have any questions about this Privacy Policy, the practices of this   site, or your dealings with this site, please contact us at:</p>
<p>By telephone: 650-752-9445 (650-PLA-YIGL)<br />
By email: <a href="mailto:support@playigl.com">support@playigl.com</a></p>

			</div>
		</div>
	</div>
</div>

<?php include '../templates/landing/footer.php'; ?>