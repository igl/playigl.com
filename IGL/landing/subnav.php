<div class="subnav">
	<div class="container">
		<div class="row">
			<div class="span12">
				<ul class="menu nav nav-pills">
					<li><a href="/about">About</a></li>
					<!-- <li><a href="#">Blog</a></li> -->
					<li><a href="/premium">Premium</a></li>
					<li><a href="/advertise">Advertise</a></li>
					<li><a href="/developers">Developers</a></li>
					<li><a href="/partners">Partners</a></li>
					<li><a href="/careers">Careers</a></li>
					<li><a href="http://support.playigl.com">Help</a></li>
					<li><a href="/legal">Legal</a></li>
					<li><a href="/investors">Investors</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>