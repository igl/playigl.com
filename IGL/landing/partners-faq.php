<?php include '../templates/landing/header.php'; ?>
<?php include '../templates/landing/subnav.php'; ?>


<div class="content-wrap">
	<div class="container">
		<div class="row">
			<div class="span3 sidebar">
				<ul class="sub-menu">
					<li><a href="/partners">Partners</a></li>
					<li><a href="/partners/signup">Sign Up</a></li>
					<li><a href="/partners/faq" class="active">FAQ</a></li>
				</ul>
				<?php include '../templates/landing/sidebar-bottom.php'; ?>
			</div>
			<div class="span9 content">
				<h1>Partners FAQ</h1>
				<p>Coming soon. </p>
			</div>
		</div>
	</div>
</div>

<?php include '../templates/landing/footer.php'; ?>