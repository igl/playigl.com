<?php include '../templates/landing/header.php'; ?>
<?php include '../templates/landing/subnav.php'; ?>


<div class="content-wrap">
	<div class="container">
		<div class="row">
			<div class="span3 sidebar">
				<ul class="sub-menu">
					<li><a href="/about">About IGL</a></li>
					<li><a href="/staff" class="active">Staff</a></li>
					<li><a href="/inthenews">In The News</a></li>
					<li><a href="/press">Press Releases</a></li>
					<li><a href="/press-kit">Press Kit</a></li>
				</ul>
				<?php include '../templates/landing/sidebar-bottom.php'; ?>
			</div>
			<div class="span9 content">
				<h1>IGL Staff</h1>
				<p>IGL creates communities around the games you love. The best game publishers partner with International Gaming League to deliver the full eSports experience for the best games on the planet. We believe in growting eSports, so if it’s an up-and-coming title, you’ll find it here.</p>
				<ul class="staff clearfix">
					<li>
						<img class="staff-img" src="/img/landing/staff/eyal.png" />
						<h4>Eyal Toledano</h4>
						<span class="position">Founder &amp; CEO</span>
						<div class="twitter"><i class="icon-twitter"></i> <a href="http://twitter.com/eyaltoledano" target="_blank">@eyaltoledano</a></div>
						<div class="linkedin"><i class="icon-linkedin"></i> <a href="http://ca.linkedin.com/in/eyaltoledano/" target="_blank">LinkedIn</a></div>
					</li>
					<li>
						<img class="staff-img" src="/img/landing/staff/darryl.png" />
						<h4>Darryl Allen</h4>
						<span class="position">Founder &amp; CTO</span>
						<div class="twitter"><i class="icon-twitter"></i> <a href="http://twitter.com/iglabsolute" target="_blank">@iglabsolute</a></div>
						<div class="linkedin"><i class="icon-linkedin"></i> <a href="http://ca.linkedin.com/in/darrylbryanallen" target="_blank">LinkedIn</a></div>
					</li>
					<li class="last">
						<img class="staff-img" src="/img/landing/staff/john.png" />
						<h4>John Clark</h4>
						<span class="position">Marketing &amp; Tournaments Director</span>
						<div class="twitter"><i class="icon-twitter"></i> <a href="http://twitter.com/es_johnclark" target="_blank">@es_johnclark</a></div>
						<div class="linkedin"><i class="icon-linkedin"></i> <a href="http://www.linkedin.com/pub/john-clark/4/900/4b4" target="_blank">LinkedIn</a></div>
					</li>
					<li>
						<img class="staff-img" src="/img/landing/staff/kevin.png" />
						<h4>Kevin Langlois</h4>
						<span class="position">Senior Web Developer</span>
						<div class="twitter"><i class="icon-twitter"></i> <a href="http://twitter.com/mixedbykevin" target="_blank">@mixedbykevin</a></div>
						<div class="linkedin"><i class="icon-linkedin"></i> <a href="http://ca.linkedin.com/in/kevinlanglois" target="_blank">LinkedIn</a></div>
					</li>
					<li>
						<img class="staff-img" src="/img/landing/staff/jassi.png" />
						<h4>Jassi Bacha</h4>
						<span class="position">UI/UX Designer &amp; Front-End Developer</span>
						<div class="twitter"><i class="icon-twitter"></i> <a href="http://twitter.com/jassibacha" target="_blank">@jassibacha</a></div>
						<div class="linkedin"><i class="icon-linkedin"></i> <a href="http://www.linkedin.com/pub/jassi-bacha/31/8b0/512" target="_blank">LinkedIn</a></div>
					</li>
					<li class="last">
						<img class="staff-img" src="/img/landing/staff/shaun.png" />
						<h4>Shaun Delaney</h4>
						<span class="position">Web Developer, Shoutcaster Extraordinaire</span>
						<div class="twitter"><i class="icon-twitter"></i> <a href="http://twitter.com/fourcourtjester" target="_blank">@fourcourtjester</a></div>
						<div class="linkedin"><i class="icon-linkedin"></i> <a href="http://ca.linkedin.com/pub/shaun-delaney/3a/786/569" target="_blank">LinkedIn</a></div>
					</li>
					<li>
						<img class="staff-img" src="/img/landing/staff/hugo.png" />
						<h4>Hugo Twaalfhoven</h4>
						<span class="position">Media Director</span>
						<div class="twitter"><i class="icon-twitter"></i> <a href="http://twitter.com/fpsintelligence" target="_blank">@fpsintelligence</a></div>
						<div class="linkedin"><i class="icon-linkedin"></i> <a href="http://nl.linkedin.com/in/grasplag" target="_blank">LinkedIn</a></div>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>

<?php include '../templates/landing/footer.php'; ?>