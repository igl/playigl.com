<?php include '../templates/landing/header.php'; ?>
<?php include '../templates/landing/subnav.php'; ?>


<div class="content-wrap">
	<div class="container">
		<div class="row">
			<div class="span3 sidebar">
				<ul class="sub-menu">
					<li><a href="/developers" class="active">Developers</a></li>
					<li><a href="/developer-tos">Terms of Service</a></li>
				</ul>
				<?php include '../templates/landing/sidebar-bottom.php'; ?>
			</div>
			<div class="span9 content">
				<h1>Tap into the League API</h1>
				<p>Our developers are working to create a RESTful league API for creating new features and adding value to your current eSports and gaming websites. From getting access to a player profile card to a full season-tracking suite, the IGL API will soon be available to developers worldwide.</p>
			</div>
		</div>
	</div>
</div>

<?php include '../templates/landing/footer.php'; ?>