<?php include '../templates/landing/header.php'; ?>
<?php include '../templates/landing/subnav.php'; ?>


<div class="content-wrap">
	<div class="container">
		<div class="row">
			<div class="span3 sidebar">
				<ul class="sub-menu">
					<li><a href="/faq" class="active">Legal</a></li>
					<li><a href="/privacypolicy">Privacy Policy</a></li>
				</ul>
				<?php include '../templates/landing/sidebar-bottom.php'; ?>
			</div>
			<div class="span9 content">
				<h1>Legal</h1>
				<p>Coming Soon.</p>
			</div>
		</div>
	</div>
</div>

<?php include '../templates/landing/footer.php'; ?>