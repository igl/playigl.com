<?php include '../templates/landing/header.php'; ?>
<?php include '../templates/landing/subnav.php'; ?>
<div class="content-wrap">
	<div class="container">
		<div class="row">
			<div class="span3 sidebar">
				<ul class="sub-menu">
					<li><a href="/about">About IGL</a></li>
					<li><a href="/staff">Staff</a></li>
					<li><a href="/inthenews" class="active">In The News</a></li>
					<li><a href="/press">Press Releases</a></li>
					<li><a href="/press-kit">Press Kit</a></li>
				</ul>
				<?php include '../templates/landing/sidebar-bottom.php'; ?>
			</div>
			<div class="span9 content">
				<h1>IGL in the News</h1>
				<div class="news-wrap">
					<ul class="nav nav-pills" id="news-tab">
						<li><a href="#highlights" data-toggle="tab">Highlights</a></li>
						<!-- <li><a href="#2013" data-toggle="tab">2013</a></li>
						<li><a href="#2012" data-toggle="tab">2012</a></li> -->
					</ul>
					 
					<div class="tab-content">
						<div class="tab-pane" id="highlights">
							<ul>
								<li>
									<a href="http://venturebeat.com/2012/11/09/anatomy-of-a-demo-day-founderfuels-fall-2012-cohort-graduates-in-pictures/" target="_blank" class="news-img"><img src="/img/landing/news/venturebeat.png"></a>
									<p><a href="http://venturebeat.com/2012/11/09/anatomy-of-a-demo-day-founderfuels-fall-2012-cohort-graduates-in-pictures/" target="_blank">Anatomy of a Demo Day: FounderFuel’s fall 2012 cohort graduates (in pictures)</a></p>
						            <span>John Koetsier, VentureBeat</span>
						        </li>
								<li>
									<a href="http://venturebeat.com/2012/11/08/mycustomizer-if-mass-customization-is-the-future-heres-the-tool-to-create-it/" target="_blank" class="news-img"><img src="/img/landing/news/venturebeat.png"></a>
									<p><a href="http://venturebeat.com/2012/11/08/mycustomizer-if-mass-customization-is-the-future-heres-the-tool-to-create-it/" target="_blank">MyCustomizer: If mass customization is the future, here’s the tool to create it</a></p>
						            <span>John Koetsier, VentureBeat</span>
						        </li>
								<li>
									<a href="http://venturebeat.com/2012/11/08/fastgrab-quick-service-food-ordering-app-plans-to-compete-with-grubhub-and-opentable-by-not/#s:photo-1-21" target="_blank" class="news-img"><img src="/img/landing/news/venturebeat.png"></a>
									<p><a href="http://venturebeat.com/2012/11/08/fastgrab-quick-service-food-ordering-app-plans-to-compete-with-grubhub-and-opentable-by-not/#s:photo-1-21" target="_blank">FastGrab: quick service food ordering app plans to compete with GrubHub and OpenTable by not</a></p>
						            <span>John Koetsier, VentureBeat</span>
						        </li>
								<li>
									<a href="http://venturebeat.com/2012/11/08/founderfuel-demo-day-today-here-are-the-startups/" target="_blank" class="news-img"><img src="/img/landing/news/venturebeat.png"></a>
									<p><a href="http://venturebeat.com/2012/11/08/founderfuel-demo-day-today-here-are-the-startups/" target="_blank">FounderFuel Demo Day today: here are the startups</a></p>
						            <span>John Koetsier, VentureBeat</span>
						        </li>
								<li>
									<a href="http://venturebeat.com/2012/11/07/openera-launches-passbook-for-files-find-any-files-in-email-box-google-drive-evernote-and-more-in-2-clicks/" target="_blank" class="news-img"><img src="/img/landing/news/venturebeat.png"></a>
									<p><a href="http://venturebeat.com/2012/11/07/openera-launches-passbook-for-files-find-any-files-in-email-box-google-drive-evernote-and-more-in-2-clicks/" target="_blank">Openera launches “Passbox” for files: find any files in email, Box, Google Drive, Evernote and more in 2 clicks</a></p>
						            <span>John Koetsier, VentureBeat</span>
						        </li>
								<li>
									<a href="http://www.itbusiness.ca/it/client/en/home/News.asp?id=69309" target="_blank" class="news-img"><img src="/img/landing/news/itbusiness.png"></a>
									<p><a href="http://www.itbusiness.ca/it/client/en/home/News.asp?id=69309" target="_blank">FounderFuel launches 8 international startups at demo day</a></p>
						            <span>Brian Jackson, IT Business</span>
						        </li>
								<li>
									<a href="http://www.fundica.com/blog/274664edca9fe788/Montreal_startup_accelerator_FounderFuel_unveils_latest_cohort_of_businesses.html" target="_blank" class="news-img"><img src="/img/landing/news/fundica.png"></a>
									<p><a href="http://www.fundica.com/blog/274664edca9fe788/Montreal_startup_accelerator_FounderFuel_unveils_latest_cohort_of_businesses.html" target="_blank">Montreal startup accelerator FounderFuel unveils latest cohort of businesses</a></p>
						            <span>Fundica Blog</span>
						        </li>
								<li>
									<a href="http://venturebeat.com/2012/11/08/reelyactive-wants-to-create-the-internet-of-things-for-the-little-guy/" target="_blank" class="news-img"><img src="/img/landing/news/venturebeat.png"></a>
									<p><a href="http://venturebeat.com/2012/11/08/reelyactive-wants-to-create-the-internet-of-things-for-the-little-guy/" target="_blank">ReelyActive wants to create the Internet of things for the little guy</a></p>
						            <span>John Koetsier, VentureBeat</span>
						        </li>
								<li>
									<a href="http://blog.davender.com/2012/11/founderfuel-demoday-fall-2012-first-impressions/" target="_blank" class="news-img"><img src="/img/landing/news/founderfuel.png"></a>
									<p><a href="http://blog.davender.com/2012/11/founderfuel-demoday-fall-2012-first-impressions/" target="_blank">FounderFuel DemoDay Fall 2012 – First impressions</a></p>
						            <span>Coach Davender Gupta, Business At The Speed Of People</span>
						        </li>
								<li>
									<a href="http://howtowriteabusinessplan.com/2012/11/press-release-founderfuels-2012-graduating-class/" target="_blank" class="news-img"><img src="/img/landing/news/founderfuel.png"></a>
									<p><a href="http://howtowriteabusinessplan.com/2012/11/press-release-founderfuels-2012-graduating-class/" target="_blank">FounderFuel’s 2012 graduating class!</a></p>
						            <span>Luke, How To Write A Business Plan</span>
						        </li>
								<li>
									<a href="http://expertise.hec.ca/pleiade_capital/evenements/founderfuel-fall-2012-demo-day/" target="_blank" class="news-img"><img src="/img/landing/news/founderfuel.png"></a>
									<p><a href="http://expertise.hec.ca/pleiade_capital/evenements/founderfuel-fall-2012-demo-day/" target="_blank">FounderFuel Fall 2012 Demo Day</a></p>
						            <span>Daniel Del Balso, Pléiade Capital</span>
						        </li>
								<li>
									<a href="http://acceleratorgazette.com/content/founderfuel-goes-global-big-launch-event" target="_blank" class="news-img"><img src="/img/landing/news/gazette.png"></a>
									<p><a href="http://acceleratorgazette.com/content/founderfuel-goes-global-big-launch-event" target="_blank">FounderFuel Goes Global With Big Launch Event</a></p>
						            <span>The Accelerator Gazette</span>
						        </li>
								<li>
									<a href="http://thelinknewspaper.ca/article/3586" target="_blank" class="news-img"><img src="/img/landing/news/thelink.png"></a>
									<p><a href="http://thelinknewspaper.ca/article/3586" target="_blank">Start It, Grow It, Pitch It, Fund It</a></p>
						            <span>Vivien Leung, The Link</span>
						        </li>
								<li>
									<a href="http://www.techvibes.com/blog/founderfuel-cohort-graduates-2012-11-08" target="_blank" class="news-img"><img src="/img/landing/news/techvibes.png"></a>
									<p><a href="http://www.techvibes.com/blog/founderfuel-cohort-graduates-2012-11-08" target="_blank">FounderFuel Graduates Latest Cohort of Canadian Startups</a></p>
						            <span>Joseph Czikk, Techvibes</span>
						        </li>
								<li>
									<a href="http://fullclipfinance.com/founderfuel-goes-international-and-launches-8-startups-before-800-people/" target="_blank" class="news-img"><img src="/img/landing/news/fullclip.png"></a>
									<p><a href="http://fullclipfinance.com/founderfuel-goes-international-and-launches-8-startups-before-800-people/" target="_blank">FounderFuel goes international, launches 8 startups</a></p>
						            <span>Cameron Stockman, Full Clip Finance</span>
						        </li>
								<!-- <li>
									<a href="#" target="_blank" class="news-img"><img src="http://s.jtvnw.net/jtv_user_pictures/hosted_images/ForbesLogo.jpeg"></a>
									<p><a href="#" target="_blank">Anatomy of a Demo Day: FounderFuel’s fall 2012 cohort graduates (in pictures)</a></p>
						            <span>John Koetsier, VentureBeat</span>
						        </li> -->
							</ul>
						</div>
						<div class="tab-pane" id="2013">
							...
						</div>
						<div class="tab-pane" id="2012">...</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$(function () {
$('#news-tab a:first').tab('show');
})
</script>
<?php include '../templates/landing/footer.php'; ?>