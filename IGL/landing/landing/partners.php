<?php include '../templates/landing/header.php'; ?>
<?php include '../templates/landing/subnav.php'; ?>


<div class="content-wrap">
	<div class="container">
		<div class="row">
			<div class="span3 sidebar">
				<ul class="sub-menu">
					<li><a href="/partners" class="active">Partners</a></li>
					<li><a href="/partners/signup">Sign Up</a></li>
					<li><a href="/partners/faq">FAQ</a></li>
				</ul>
				<?php include '../templates/landing/sidebar-bottom.php'; ?>
			</div>
			<div class="span9 content">
				<h1>Partners</h1>
				<p><strong>The IGL Partners is an exclusive group of game developers & publishers, consumer brands, sponsors and teams that converge here to elevate your game. If you are a gaming brand looking to reach the eSports market by way of new and innovative programs, leverage the engagement power of eSports with us today.</strong></p>
				<p>Creating relationships with the right game developers and publishers represents one of the core business practices at International Gaming League. We work hard to support the games you love, and that is made possible through collaborations with the game development community in an effort to add value to your league experience.</p>
			</div>
		</div>
	</div>
</div>

<?php include '../templates/landing/footer.php'; ?>