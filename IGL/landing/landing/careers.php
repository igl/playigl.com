<?php include '../templates/landing/header.php'; ?>
<?php include '../templates/landing/subnav.php'; ?>


<div class="content-wrap">
	<div class="container">
		<div class="row">
			<div class="span3 sidebar">
				<ul class="sub-menu">
					<li><a href="/careers" class="active">Careers</a></li>
					<li><a href="/benefits">Benefits</a></li>
				</ul>
				<?php include '../templates/landing/sidebar-bottom.php'; ?>
			</div>
			<div class="span9 content">
				<h1>Careers at International Gaming League</h1>
				<h4>International Gaming League is a Canadian startup building the next big thing in eSports. Join our team to bridge the gap between Amateur and Pro eSports. Let’s grow our favorite pastime together!</h4>
				<p>The IGL team has grown tremendously <a href="http://founderfuel.com/fall-2012-demo-day-pitches/" target="_blank">since we graduated from our accelerator</a> from a team of two cofounders to a staff of almost 30 people, between engineers, content creators and league administrators.</p>
				<h2>Who you'll work with</h2>
				<p>We have an unyielding belief in giving ownership of initiatives. Everyone at IGL is an expert in something. Working with us means being given the responsibility of your own vision when doing what you love without compromising on your side projects. Just ask Steve, who created <a href="https://backpack.tf" target="_blank">Backpack.tf</a> or Shaun (<a href="https://twitter.com/FourCourtJester" target="_blank">@FourtCourtJester</a>), who continues to bring you some of the best casting on on his <a href="https://twitter.com/FourCourtJester" target="_blank">YouTube channel</a>.</p>
				<h2>Where you’ll be working</h2>
				<p>We strongly believe in giving you the option of working remotely or at the office. We’re situated in the heart of downtown Montreal (<a href="http://notman.org/en/" target="_blank">the Home of the Web, actually</a>), so we’re close some of the biggest names in the gaming industry, including EA, Ubisoft, A2M and Eidos.</p>
				<h2>How we work</h2>
				<p>We’re a small, very passionate team that loves to work in an Agile environment. We iterate on our products very quickly and love trying out new stuff to push it to the world the night of. The best part is taking user feedback and answering it less than one week later.</p>
		</div>
	</div>
</div>

<?php include '../templates/landing/footer.php'; ?>