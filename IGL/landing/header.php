<?php
/*********************
 * IGL Landing Page
 * *******************/
    //session_start();
    // Send authorized users to the /home page
   // if (!is_null($_SESSION) && $_SESSION['playerid'] > 0) {
    //    header('Location: /home');
    //}
?>
<html>
  <head>
    <title>International Gaming League - Elevate Your Game</title>
    <meta name="description" content="The online destination for Amateur Competitive Video Gaming">
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <link rel="Shortcut Icon" href="/favicon.ico" type="image/x-icon" />
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Headland+One' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
    
    <link type="text/css" href="/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/font-awesome.css">
    <link type="text/css" href="/css/jquery-ui.css" rel="stylesheet">
    
    <link rel="stylesheet" href="/css/stylesheet.css" />
    <link rel="stylesheet" href="/css/tablecloth.css" />
    <link rel="stylesheet" href="/css/prettify.css" />


    <link rel="stylesheet" href="/css/landing.css" />

    <script type="text/javascript" src="/js/mixpanel.js"></script>
    <script type="text/javascript" src="/js/jquery-1.8.2.js"></script>
    <script type="text/javascript" src="/js/jquery-ui.js"></script>
    <script type="text/javascript" src="/js/bootstrap.js"></script>
    <script type="text/javascript" src="/js/register.js"></script>
    <script type="text/javascript" src="/js/authenticate.js"></script>
    <script type="text/javascript" src="/js/index.js"></script>

    <script type="text/javascript">
        var analytics=analytics||[];(function(){var e=["identify","track","trackLink","trackForm","trackClick","trackSubmit","page","pageview","ab","alias","ready","group"],t=function(e){return function(){analytics.push([e].concat(Array.prototype.slice.call(arguments,0)))}};for(var n=0;n<e.length;n++)analytics[e[n]]=t(e[n])})(),analytics.load=function(e){var t=document.createElement("script");t.type="text/javascript",t.async=!0,t.src=("https:"===document.location.protocol?"https://":"http://")+"d2dq2ahtl5zl1z.cloudfront.net/analytics.js/v1/"+e+"/analytics.min.js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(t,n)};
        analytics.load("ia4hmptjr2");
    </script>

  </head>
  <body class="landing">
    <div class="wrap">
    <?php include_once('../templates/msg_bar.php'); ?>
      <header>
        <div class="headerwrap">
            <a class="logo" href="/">
              <img alt="International Gaming League" src="/img/igl_logo_1.png">
            </a>
            <ul class="menu">
              <!--<li><a href="#">Tour</a></li>-->
              <li><a href="/login">Log In</a></li>
              <li><a href="/register">Sign Up</a></li>
            </ul>
        </div>
      </header>