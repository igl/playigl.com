    </div> <!-- wrap div -->
    <footer>
        <div class="container">
          <div class="row">
            <div class="span10">
              <img class="logo" src="/img/igl_logo-footer.png">
              <ul class="menu">
                <li><a href="/about">About</a></li>
                <!-- <li><a href="#">Blog</a></li> -->
                <li><a href="/premium">Premium</a></li>
                <li><a href="/advertise">Advertise</a></li>
                <li><a href="/developers">Developers</a></li>
                <li><a href="/partners">Partners</a></li>
                <li><a href="/careers">Careers</a></li>
                <li><a href="http://support.playigl.com">Help</a></li>
                <li><a href="/legal">Legal</a></li>
              </ul>
            </div>
            <div class="span2">
              <a class="what-is" href="/what-is-esports">What is eSports?</a>
            </div>
          </div>
        </div>
    </footer>
    <script type="text/javascript" src="/js/global.js"></script>
    <script type="text/javascript">
    $("document").ready(function() { initGoogle(); } );
    </script>
    <?php
if (isset($_SESSION['playerid'])) {
    echo '<script type="text/javascript">';
    
    echo "analytics.identify('".$_SESSION['playerid']."',";
    echo "{email : '".$_SESSION['email']."'});";
  echo "</script>";
} else {
    echo '<script type="text/javascript">';
    
    echo "analytics.identify('Anonymous');";
  echo "</script>";
}
?>
  </body>
</html>
