<?php include '../templates/landing/header.php'; ?>
<?php include '../templates/landing/subnav.php'; ?>


<div class="content-wrap">
	<div class="container">
		<div class="row">
			<div class="span3 sidebar">
				<ul class="sub-menu">
					<li><a href="/about" class="active">About IGL</a></li>
					<li><a href="/staff">Staff</a></li>
					<li><a href="/inthenews">In The News</a></li>
					<li><a href="/press">Press Releases</a></li>
					<li><a href="/press-kit">Press Kit</a></li>
				</ul>
				<?php include '../templates/landing/sidebar-bottom.php'; ?>
			</div>
			<div class="span9 content">
				<h1>About International Gaming League</h1>
				<p><strong>International Gaming League is your destination for Amateur eSports. We allow you to elevate your game by delivering the full eSports experience to everyone--Not just professional players.</strong></p>
				<h4>The Best Games</h4>
				<p>IGL creates communities around the games you love. The best game publishers partner with International Gaming League to deliver the full eSports experience for the best games on the planet. We believe in growing eSports, so if it’s an up-and-coming title, you’ll find it here.</p>
				<h4>Skill Level Matters</h4>
				<p>IGL is home to thousands of free agents across the world who compete across a variety of video games. Best of all? The IGL league structure allows players of any skill level to compete and to experience the eSports experience.</p>
				<h4>The Best Events</h4>
				<p>IGL organizes and partners with the best events. They’re all here: international tournaments, charity events, professional show matches for your viewing pleasure, amateur tournaments every week. And you can be a part of all of them.</p>
				<h4>Actually Elevate Your Game</h4>
				<p>This company was built by gamers that have a thirst for competition. We’ve figured out, through our own experiences as amateurs, semi-professionals and professionals what it takes to take you from being interested in that awesome new game to competing with the best players.</p>
				<div class="row">
					<div class="span4">
						<h4>Follow Us</h4>
						<a class="social-icon" href="#"><i class="icon-facebook"></i></a> &nbsp;&nbsp; <a class="social-icon" href="#"><i class="icon-twitter"></i></a>
					</div>
					<div class="span5">
						<h4>Find us here in Montreal</h4>
						<iframe width="380" height="330" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.ca/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=51+Sherbrooke+St+W,+Montreal,+QC+H2X+1X2&amp;aq=&amp;sll=45.511324,-73.565226&amp;sspn=0.012887,0.033023&amp;ie=UTF8&amp;hq=&amp;hnear=51+Sherbrooke+St+W,+Montreal,+Quebec+H2X+1X2&amp;t=m&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe><br />
						<a href="https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=405+Rue+Bonsecours+Montreal,+QC+H2Y+3C3,+Canada&amp;aq=&amp;sll=45.505926,-73.560076&amp;sspn=0.051551,0.132093&amp;ie=UTF8&amp;hq=&amp;hnear=405+Rue+Bonsecours,+Montr%C3%A9al,+Communaut%C3%A9-Urbaine-de-Montr%C3%A9al,+Qu%C3%A9bec+H2Y+1H4,+Canada&amp;t=m&amp;ll=45.517775,-73.549776&amp;spn=0.019846,0.032616&amp;z=14&amp;iwloc=A">View on Google Maps</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include '../templates/landing/footer.php'; ?>