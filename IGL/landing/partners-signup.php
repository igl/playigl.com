<?php include '../templates/landing/header.php'; ?>
<?php include '../templates/landing/subnav.php'; ?>


<div class="content-wrap">
	<div class="container">
		<div class="row">
			<div class="span3 sidebar">
				<ul class="sub-menu">
					<li><a href="/partners">Partners</a></li>
					<li><a href="/partners/signup" class="active">Sign Up</a></li>
					<li><a href="/partners/faq">FAQ</a></li>
				</ul>
				<?php include '../templates/landing/sidebar-bottom.php'; ?>
			</div>
			<div class="span9 content">
				<h1>Partners Sign-Up</h1>
				<p><strong>The IGL Partners is an exclusive group of game developers & publishers, consumer brands, sponsors and teams that converge here to elevate your game. If you are a gaming brand looking to reach the eSports market by way of new and innovative programs, leverage the engagement power of eSports with us today.</strong></p>
				<p>If you’d like to run events with us or would like to purchase title sponsorships for our leagues, ladders and sponsored events, please get in contact us.</p>
				<a class="btn btn-large btn-red" href="mailto:sales@playigl.com">Contact League Sales</a>
			</div>
		</div>
	</div>
</div>

<?php include '../templates/landing/footer.php'; ?>