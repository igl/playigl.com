<?php include '../templates/landing/header.php'; ?>
<?php include '../templates/landing/subnav.php'; ?>


<div class="content-wrap">
	<div class="container">
		<div class="row">
			<div class="span3 sidebar">
				<ul class="sub-menu">
					<li><a href="/investors" class="active">Investors</a></li>
				</ul>
				<?php include '../templates/landing/sidebar-bottom.php'; ?>
			</div>
			<div class="span9 content">
				<h1>Investors</h1>
				<h3>International Gaming League is a FounderFuel alumni. We graduated as part of the Fall 2012 Cohort in Montreal, on November 9th, 2012.</h3>
				<ul class="staff clearfix">
					<li>
						<a href="http://founderfuel.com/" target="_blank"><img src="/img/landing/investors/founderfuel-logo.png" /></a>
					</li>
					<li>
						<a href="http://www.bdc.ca/EN/solutions/venture_capital/strategic_approach/Pages/it_venture_fund.aspx#.UUp8JFeJ3kh" target="_blank"><img src="/img/landing/investors/bdc-logo.png" /></a>
						
					</li>
					<li class="last">
						<a href="http://realventures.com/en/" target="_blank"><img src="/img/landing/investors/realventures-logo.png" /></a>
					</li>
				</ul>
				<a class="btn btn-large btn-red" href="https://angel.co/international-gaming-league" target="_blank">Visit AngelList Profile</a>
			</div>
		</div>
	</div>
</div>

<?php include '../templates/landing/footer.php'; ?>