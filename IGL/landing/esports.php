<?php include '../templates/landing/header.php'; ?>
<?php include '../templates/landing/subnav.php'; ?>


<div class="content-wrap">
	<div class="container">
		<div class="row">
			<div class="span3 sidebar">
				<ul class="sub-menu">
					<li><a href="/what-is-esports" class="active">What is eSports</a></li>
				</ul>
				<?php include '../templates/landing/sidebar-bottom.php'; ?>
			</div>
			<div class="span9 content">
				<h1>What is eSports?</h1>
				<p>Lorem ipsum dolor sit amet, enim paulatim zelus tristique inhibeo, virtus caecus lucidus in adsum, humo minim vero et vel. Refoveo iustum appellatio volutpat consequat blandit. Suscipit hendrerit, suscipit odio commoveo valetudo quis huic in conventio. Te, ibidem, reprobo valetudo praesent proprius vel, secundum hendrerit augue foras nostrud. Velit ullamcorper sed et facilisi vereor exerci esse molior. </p>
			</div>
		</div>
	</div>
</div>

<?php include '../templates/landing/footer.php'; ?>