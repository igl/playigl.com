<?php include '../templates/landing/header.php'; ?>
<?php include '../templates/landing/subnav.php'; ?>


<div class="content-wrap">
	<div class="container">
		<div class="row">
			<div class="span3 sidebar">
				<ul class="sub-menu">
					<li><a href="/about">About IGL</a></li>
					<li><a href="/staff">Staff</a></li>
					<li><a href="/inthenews">In The News</a></li>
					<li><a href="/press" class="active">Press Releases</a></li>
					<li><a href="/press-kit">Press Kit</a></li>
				</ul>
				<?php include '../templates/landing/sidebar-bottom.php'; ?>
			</div>
			<div class="span9 content">
				<h1>Press Releases</h1>
				Coming Soon
				<!-- <ul class="press-release">
					<li>
						<a href="#">Twitch Partners with Red 5 Studios to Bring One-Click Broadcasting to Firefall</a><br />
						<span>February 13, 2013</span>
					</li>
				</ul> -->
			</div>
		</div>
	</div>
</div>

<?php include '../templates/landing/footer.php'; ?>