<div class="row">
    <div class="span8 event match-details">

        <div class="navbar">
            <div class="navbar-inner">
                <img src="/img<?php echo $thismatch['game']['banner']; ?>" class="img-header">
            </div>
        </div>


        <div class="tab-content">
            <div class="info clearfix">

                <h4 class="pull-left"><?php echo $thismatch['game']['name']; ?> - <?php echo $thismatch['format']; ?> - <?php echo $thismatch['format-name']; ?></h4>
                <div class="status pull-right">
                    Status: 
                    <? if ($thismatch['completed'] == 1) { ?>
                        <span class="green">Completed <i class="icon-ok"></i></span>
                    <? } else { ?>
                        <span class="red">Incomplete</span>
                    <? } ?>
                </div>
            </div>
            <div class="match">
                <div class="social">
                    <a href="https://twitter.com/share"  data-via="playigl" data-related="<?php echo "{$thismatch['home']['twitter']}, {$thismatch['away']['twitter']}"; ?>" data-url="www.playigl.com" data-counturl="<?php echo BASEURL . "match/{$thismatch['id']}"; ?>" data-text="<?php echo "{$thismatch['home']['name']} vs {$thismatch['away']['name']}";
                    echo " " . BASEURL . "match/{$thismatch['id']}"; ?>" class="twitter-share-button">Tweet Match</a>
                    <script>
                            !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');
                    </script>
                </div>
                <div class="date"><span class="localTime"><?php echo $thismatch['date']; ?></span></div>
                <div class="team">
                    <img class="team-img" src="/img<?php echo $thismatch['home']['logo']; ?>" />
                    <div class="name"><img class="flag" src="/img/flags/ca.png" /> <?php echo $thismatch['home']['name']; ?></div>
                    <div class="team-stats">RPI: <strong><?php echo $thismatch['home']['rpi']; ?></strong> &nbsp;&nbsp;&nbsp; L10: <strong><?php echo $team_stats['home']['L10']['wins']; ?>-<?php echo $team_stats['home']['L10']['losses']; ?></strong> &nbsp;&nbsp;&nbsp; PCT: <strong><?php echo $team_stats['home']['record']['pct']; ?></strong></div>
                    <div class="roster">
                        <table class="table table-condensed table-striped table-bordered table-stats">
                            <thead>
                                <tr>
                                    <th colspan="2">Roster</th>
                                </tr>
                            </thead>    
                            <tbody> 
                                <?php
                                $i = 1;
                                foreach ($thismatch['home']['roster'] as $r) {
                                    echo '<tr><td>' . $i . '</td>';
                                    echo '<td><img class="flag" src="/img/flags/' . $r["country"] . '.png" /> ' . $r["username"] . '</td>';
                                    echo '</tr>';
                                    $i++;
                                }
                                ?>
                            </tbody>    
                        </table>
                    </div>
                </div>
                <div class="score">
                    <div class="vs">VS</div>
                    <div class="<?php if (round($thismatch['home_points']) > round($thismatch['away_points'])) {
                                    echo "win";
                                } else {
                                    echo "loss";
                                } ?>"><?php echo array_sum(explode("|", $thismatch['home_score'])); ?></div>
                    <div class="<?php if (round($thismatch['home_points']) < round($thismatch['away_points'])) {
                                    echo "win";
                                } else {
                                    echo "loss";
                                } ?>"><?php echo array_sum(explode("|", $thismatch['away_score'])); ?></div>
                </div>
                <div class="team">
                    <img class="team-img" src="/img<?php echo $thismatch['away']['logo']; ?>" />
                    <div class="name"><img class="flag" src="/img/flags/ca.png" /> <?php echo $thismatch['away']['name']; ?></div>
                    <div class="team-stats">RPI: <strong><?php echo $thismatch['away']['rpi']; ?></strong> &nbsp;&nbsp;&nbsp; L10: <strong><?php echo $team_stats['away']['L10']['wins']; ?>-<?php echo $team_stats['away']['L10']['losses']; ?></strong> &nbsp;&nbsp;&nbsp; PCT: <strong><?php echo $team_stats['away']['record']['pct']; ?></strong></div>
                    <div class="roster">
                        <table class="table table-condensed table-striped table-bordered table-stats">
                            <thead>
                                <tr>
                                    <th colspan="2">Roster</th>
                                </tr>
                            </thead>    
                            <tbody> 
<?php
$i = 1;
foreach ($thismatch['away']['roster'] as $r) {
    echo '<tr><td>' . $i . '</td>';
    echo '<td><img class="flag" src="/img/flags/' . $r["country"] . '.png" /> ' . $r["username"] . '</td>';
    echo '</tr>';
    $i++;
}
?>
                            </tbody>    
                        </table>
                    </div>
                </div>
                <div class="match-rounds">
                    <table class="table table-condensed table-striped table-bordered table-stats">
                        <thead>
                            <tr>
                                <th colspan="2">Map</th>
                                <th>Home</th>
                                <th>Away</th>
                            </tr>
                        </thead>    
                        <tbody> 
                            <?php
                            $i = 1;
                            foreach ($thismatch['maprounds'] as $i => $r) {
                                echo '<tr><td>' . $r['id'] . '</td>';
                                echo '<td>' . $r['map'] . '</td>';
                                echo '<td>' . $r['home_score'] . '</td>';
                                echo '<td>' . $r['away_score'] . '</td>';
                                echo '</tr>';
                                $i++;
                            }
                            ?>
                        </tbody>    
                    </table>
                </div>
            </div>
            <div class="content">

            </div>
        </div>
        <div class="comments tab-content">
            <h3>Match Discussion</h3>
            <div class="inner_box">
                <form id="comment_form">
                    <input type="hidden" value="472" name="viewer_id" id="viewer_id">
                    <input type="hidden" value="472" name="page_id" id="page_id">
                    <input type="hidden" value="player" name="page_name" id="page_name">
                    <textarea id="comment" name="comment" style="height: 60px; width: 545px;"></textarea>
                    <div class="full" style="height:30px">
                        <small class="pull-left">You have <span id="chars">130</span> characters left.</small>
                        <button id="button_post" type="button" onclick="commentPost();" class="btn btn-primary pull-right">Post Comment</button>
                    </div>
                </form>


                <ul id="player_comments" class="clear media-list"></ul>
            </div>
        </div>
    </div> 
    <div class="span4">
        <div id="accordion" class="box">
            <h1>Similar Matches</h1>
            <ul class="similar-matches">
                <?php
                foreach ($thismatch['similar'] as $s) {
                    $url = urlencode("{$s['home']['name']}-vs-{$s['away']['name']}");
                    echo "<li><a href='/match/{$s['id']}-{$url}'>";
                    echo "<img src='/img{$s['home']['logo']}' class='avatar team2' />";
                    echo "<img src='/img{$s['away']['logo']}' class='avatar team1' />";
                    echo '<h4>' . $s['home']['tag'] . ' vs ' . $s['away']['tag'] . '</h4>';
                    echo '2v2 Clan Arena Ladder<br/>';
                    echo date('F jS Y', $s['date']);
                    echo '</a></li>';
                }
                ?>
            </ul>
        </div>
    </div>
</div>