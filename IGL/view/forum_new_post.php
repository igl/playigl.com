<div class="span12">
  <form method="post" id="new_post" class="post-form form-stacked" accept-charset="UTF-8">
    <h2>New Post</h2>
    <p>At this time, no HTML or BBCode is allowed, and only links will be activated.</p><br>
    <p>
      <label for="post_title">Title</label>
      <input type="text" size="30" name="new_post[title]" id="post_title" class="span12">
    </p>
    <p>
      <label for="post_text">Text</label>
      <textarea rows="20" name="new_post[content]" id="post_text" cols="40" class="span12"></textarea>
    </p>
    <p>
      <label for="new_post_tag_ids">Tags</label>
      <?php echo $forums->getTagsHTML($forums->tags, true); ?>
    </p>
    <input type="hidden" name="new_post[preview]" value="0">
    <hr/>
    <h2>Preview</h2>
    <div class="preview">
      <div class="well">No preview.</div>
    </div>
    <div class="form-actions">
      <button class="btn btn-primary" type="button" onclick="newPost(this);"><i></i> Submit</button>
      <button class="btn btn-primary" type="button" onclick="newPreview(this);"><i></i> Preview</button>
      <a class="btn" href="/forums/">Cancel</a>
    </div>
  </form>
</div>