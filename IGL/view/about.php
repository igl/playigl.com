<div class="box full">
    <h1 class="center">About International Gaming League</h1>
    <div class="inner_box" style="margin-top: 12px">
        <div class="lead center">Your destination for amateur eSports.</div>
        <div class="row-fluid">
            <div class="span7">
                <h3>Why an Amateur League?</h3>
                <p>Professional eSports have been growing at breakneck speeds over the last couple of years. Ten years ago, the idea of playing video games competitively as a career choice seemed like nothing but imagination. <strong>In 2011, more than $6 Million had been won by eSports teams</strong> in games like Starcraft 2, League of Legends, Dota 2 and others.</p>
                <p>In the face of a growing pro segment, a new demand is born where people of different skill levels now want to participate in eSports as both players and viewers. But barriers of entry in eSports are considerable and it's difficult to introduce new gamers to the sport. This is how International Gaming League came to be, to become an amateur destination for competitive eSports: a place for people of all skill levels to enjoy player or spectator experiences for the best competitive titles.</p>
                
                <select name="slider" id="flip-a" data-role="slider">
                    <option value="off">Off</option>
                    <option value="on">On</option>
                </select>
            </div>
            
            <div class="span5" style="padding-top: 60px;">
                <img src="img/igl-logo.png">
                <blockquote><h4>A professional-grade league tailored for an immense eSports amateur audience.</h4></blockquote>
            </div>
        </div>

        <br>

        <h3>The IGL Philosophy</h3>
        <p>We're driven by an unshakable belief that by providing a proper backbone to the Amateur eSports community, that the sport will benefit from a centralized pool of talent, from which teams of International affiliation can recruit and build long-lasting relationships, cumulating in a healthy and growing eSports industry.</p>
        <p>Our mission is to <strong>elevate your game</strong>. To become the de facto online destination for amateur eSports players and spectators, we've come up with a set of rules we dubbed the IGL Manifesto. It represents our core beliefs and everything we strive to achieve.</p>

        <hr>

        <h3>Built in Montreal, Canada</h3>
        <p>International Gaming League is proud to be a <a href="http://founderfuel.com/en/alumni/fall-2012/">FounderFuel Alumni</a>. By taking part in one of North America's most renowned web accelerators, IGL gained access to countless mentors, perks and professional relationships that helped build who and what we are today. </p>
        <p>FounderFuel brought together an IGL team that was scattered throughout North America and solified the position of the Canadian company in a city that is recognized as one of the North American epicenters for the video game industry.</p>
    </div>
</div>