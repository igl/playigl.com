<div class="span8">
    <?php
        $summary = truncate(str_replace(array("\r\n", "\n", "\r"),'',strip_tags($article['content'])),200);
        $date = date('F jS, Y',strtotime($article['rts']));
        $social = array(
            "Facebook" => "igl_icon.png",
            "Twitter" => "igl_icon.png",
            "LinkedIn" => "igl_icon.png",
            "Pinterest" => "igl_icon.png"
        );
    ?>
    <img src='/img<?php echo $article['banner']; ?>'>
    
    <div class='box' style='margin-top:12px;'>
        <h1><?php echo $article['title']; ?></h1>
        <div style='padding:6px'>
            <div class='inner_box'>
                <p class='posted'><strong>Posted: </strong><?php echo $date; ?> by <a href='/player/<?php echo $article['author']; ?>'><?php echo $article['author']; ?></a></p>
                <div class='articlecontent'><?php echo $article['content']; ?></div>
                <?php
                    foreach($social as $key => $val) {
                        echo "<span class='st_".strtolower($key)."_vcount' st_image='".BASEURL."/img/{$val}' st_title='{$article['title']}' st_summary='{$summary}' displayText='{$key}'></span>";
                    }
                ?>
            </div>
        </div>
        
        <div class="inner_box">
            <form id="comment_form">
                <input type="hidden" id="viewer_id" name="viewer_id" value="<?php echo $_SESSION['playerid'];?>">
                <input type="hidden" id="page_id" name="page_id" value="<?php echo $article['id'];?>">
                <input type="hidden" id="page_name" name="page_name" value="article">
                <?php
                if( isset($_SESSION['playerid']) && $_SESSION['playerid'] > 0 )
                {
                ?>
                <textarea style="width:562px;height:60px" name="comment" id="comment"></textarea>
                <div style="height:30px" class="full">
                    <small class="pull-left">You have <span id="chars">130</span> characters left.</small>
                    <button class="btn btn-primary pull-right" onclick="commentPost();" type="button" id="button_post">Post Comment</button>
                </div>
                <?php
                }
                ?>
            </form>
            <ul class="clear media-list" id="article_comments"></ul>
        </div>
    </div>
    
    
</div>