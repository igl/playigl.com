<?php if(IGL_PAGE!=true)die(); ?>
<div class="span8">
    <div class="navbar navbar-division">
        <div class="navbar-inner">
            <img id='header_image' class='img-header'>
        </div>
    </div>
    
    <table style='margin-top:12px' class='table table-condensed table-striped table-bordered table-stats' id='openteamlist'>
        <thead>
            <tr><th colspan='6' id='title'>Teams Recruiting</tr>
            <tr><th>Name</th><th>Location</th><th>Players</th></tr>
        </thead>
        <tbody><tr><td colspan="3"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></tr></tbody>
    </table>

    <div style="display:none;" id="server-data" data-game-id="<?php echo $game; ?>"></div>
</div>