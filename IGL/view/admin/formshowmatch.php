
    
<form id="adminForm" action="" enctype="multipart/form-data" method="POST">
    
<div class="box">
        <h1>Showmatch</h1>
        <div class="inner_box" style="margin-top:10px">
            <div class="row" style="margin-left:0px;">
                <fieldset class="pull-left span5">
                    <label>Event Title</label>
                    <input id="name" name="name" type="text" value="<?php if($mode == "edit") { echo $match_info['name']; } ?>">
                    <span class="help-block">Example: DOTA 2 - Select Divison Playoffs</span>
                    <label>Game</label>
                    <select id="game_id" name="game_id" ></select>
                    <input type="hidden" id="action" name="action" value="create"/>
                    <input type="hidden" id="id" name="id" value="<?php if($mode == "edit") { echo $match_info['id']; } ?>"/>
                </fieldset>
                <fieldset class="span5">
                    
                    <label>Start Date / Time</label>
                    <div class="input-append dateTimePicker">
        <input name="startdatetime" id="statedatetime" data-format="dd-MM-yyyy hh:mm:ss" type="text" value="<?php if($mode == "edit") { echo $match_info['startdatetime']; } ?>"></input>
        <span class="add-on">
            <i data-time-icon="icon-time" data-date-icon="icon-calendar">
        </i>
    </span>
    </div>
                    <label>End Date / Time</label>
                                        <div class="input-append dateTimePicker">
        <input name="enddatetime" id="enddatetime" data-format="dd-MM-yyyy hh:mm:ss" type="text" value="<?php if($mode == "edit") { echo $match_info['enddatetime']; } ?>"></input>
        <span class="add-on">
            <i data-time-icon="icon-time" data-date-icon="icon-calendar">
        </i>
    </span>
                </fieldset>
                <fieldset class="span10">
                    <label>Banner Image</label>
                    <input type="file" name="banner_image" id="banner_image" />
                    <label>Event Info Image</label>
                    <input type="file" name="info_image" id="info_image" />
                    <label>Results Image</label>
                    <input type="file" name="results_image" id="results_image" />
                </fieldset>
            </div>
        </div>
</div>
     
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">
window.onload = function() {   
    
    PlayIGL.Forms.createGameDropdown($('#game_id') <?php if($mode == "edit") { echo ", '".$match_info['game_id']."'"; } ?> );

    
    $('#startdatetime').val(PlayIGL.formatTimestamp($('#startdatetime').val(), true));
    $('#enddatetime').val(PlayIGL.formatTimestamp($('#enddatetime').val(), true));
    
    PlayIGL.viewChange(true);

};
</script>    
   
</form>

