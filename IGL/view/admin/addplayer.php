<div class="span12">
<?php
include('./view/admin/subnavigation.php');
if( SUPERUSER==true )
{
    //classes
    $player = new player();
    
    echo '<div onclick="$(\'#playerForm\').submit();" style="margin-top:-56px; margin-right:5px;" class="btn btn-action pull-right">Save</div>';
    if($_POST)
    {
        $result = $player->add();
        if($result['success'])
        {
            ?>
            <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">x</button>Player added successfully!</div>
            <?php
        }
        else
        {
            echo "<div class='alert alert-error'><button type='button' class='close' data-dismiss='alert'>x</button>{$result['error']}</div>";
        }
    }

    $mode = 'add';
    include('formplayer.php');
}
else
{
    echo "You do not have access to this page.";
}
?>
</div>