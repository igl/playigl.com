<?php
if( SUPERUSER )
{
    if (isset($_REQUEST['id'])) {
    
    $id = (int)$_REQUEST['id'];
     
     // Classes
     $league = new league();
     
     // Actions
         if ($league->delete($id)) {
             echo '<div class="alert span12">League deleted.</div>';
         } else {
             echo '<div class="alert span12 alert-error"><strong>Error.</strong> The league could not be deleted.</div>';
         }
     
    } else {
        echo '<div class="alert alert-error"><strong>Error.</strong> No league id provided.</div>';
    }

    $leagues = $league->all();
    include('allleagues.php');
}
else
{
    echo "You do not have access to this page.";
}
?>