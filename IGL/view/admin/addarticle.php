<div class="span12">
<?php
include('./view/admin/subnavigation.php');
if( SUPERUSER==true )
{
    echo '<div onclick="$(\'#action\').val(\'create\'); $(\'#articleForm\').submit();" style="margin-top:-55px; margin-right:5px" class="btn btn-action pull-right">Create</div>';
    $article   = new article();

    if($_POST)
    {
        $result = $article->add();

        if( $result['success'] == true )
        {
            ?>
            <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">x</button>Article added successfully!</div>
            <?php
        }
        else
        {
            echo "<div class='alert alert-error'><button type='button' class='close' data-dismiss='alert'>x</button>{$result['message']}</div>";
        }
    }
    $mode = 'add';
    include('formarticle.php');
}
else
{
    echo "You do not have access to this page.";
}
?>
</div>