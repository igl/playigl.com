<div class="span12">
<?php
include('./view/admin/subnavigation.php');
if( SUPERUSER==true )
{
    $team   = new team();
    $id = (int)$_REQUEST['id'];

    echo '<div onclick="$(\'#teamForm\').submit();" style="margin-top:-56px; margin-right:5px;" class="btn btn-action pull-right">Save</div>';
    if($_POST)
    {
        if($team->save())
        {
            ?>
            <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">x</button>Team saved successfully!</div>
            <?php
        }
        else
        {
            ?>
            <div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">x</button>Error saving team.</div>
            <?php
        }
    }
    
    $game = new game();
    $games = $game->all();  
    
    $team_data = $team->get($id);
    $game_data = json_decode(file_get_contents(BASEURL.'api/game/'.$team_data['game']),true);
    
    if( $game_data['success'] )
    {
        $leagues = $game_data['game']['game_leagues'];
        $ladders = $game_data['game']['game_ladders'];
    }
    $mode = 'edit';
    include('formteam.php');
}
else
{
    echo "You do not have access to this page.";
}
?>
</div>