<div class="span12">
<?php

include('./view/admin/subnavigation.php');

if( LEAGUEADMIN )
{
    
    if (isset($_REQUEST['id'])) {
    
    $mode = "edit";
    $showmatchId = $_REQUEST['id'];
    // Includes
     require_once('./model/showmatch.class.php');
     
     // Classes
     $game = new game();
     $league = new league();
     $showmatch = new showmatch();
    
     
    // Option Buttons
    echo '<div onclick="$(\'#action\').val(\'save\'); $(\'#adminForm\').submit();" style="margin-top:-55px; margin-right:5px;" class="btn btn-action pull-right">Save Showmatch</div>';
    $match_info = $showmatch->get($showmatchId); 

     // Actions
     if (isset($_POST['action'])) {
         if ($showmatch->save($_POST['id'])) {
             $match_info = $showmatch->get($showmatchId); // Reload tournament info
             $error = false;
             if ($_FILES["banner_image"]["tmp_name"] && $_FILES["banner_image"]["tmp_name"] != "") {
             if (move_uploaded_file($_FILES["banner_image"]["tmp_name"], "./img/ss2_banner.png")) {
                 $error = false;
             } else {
                 $error = true;
             }
             }
             if ($_FILES["info_image"]["tmp_name"] && $_FILES["info_image"]["tmp_name"] != "") {
             if (move_uploaded_file($_FILES["info_image"]["tmp_name"], "./img/ss2_info.png")) {
                  $error = false;
             } else {
                 $error = true;
             }
             }
             if ($_FILES["results_image"]["tmp_name"] && $_FILES["results_image"]["tmp_name"] != "") {
             if (move_uploaded_file($_FILES["results_image"]["tmp_name"], "./img/ss2_results.png")) {
                  $error = false;
             } else {
                  $error = true;
             }
             }
             if (!$error) {
                  echo '<div class="alert alert-success">Showmatch updated. <a href="http://www.playigl.com/minievent/'.$_REQUEST['id'].'" target="_blank"><i class="icon-page"></i> View Page<a/></div>';
             } else {
                  echo '<div class="alert alert-success">Showmatch updated. Some images could not be uploaded. <a href="http://www.playigl.com/minievent/'.$_REQUEST['id'].'" target="_blank"><i class="icon-page"></i> View Page<a/></div>';  
             }
     }
     }
     
     include('./view/admin/formshowmatch.php');
     
     echo '<div onclick="$(\'#action\').val(\'delete\'); $(\'#adminForm\').attr(\'action\', \'/admin/deleteshowmatch\'); $(\'#adminForm\').submit();" style="margin-bottom:14px;" class="btn btn-danger pull-right">Delete Showmatch</div>';
    
    } else {
        echo '<div class="alert alert-error"><strong>Error.</strong> No showmatch id provided.</div>';
    }
    

}
else
{
    echo "You do not have access to this page.";
}
?>
</div>