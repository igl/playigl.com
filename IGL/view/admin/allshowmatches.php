<div class="span12">
<?php
include('./view/admin/subnavigation.php');

if( LEAGUEADMIN )
{
    include('./model/showmatch.class.php');
    $showmatch = new showmatch();
    $matches = $showmatch->all();
?>
<style>

    .showmatchList tbody tr {
        cursor:pointer;
    }
</style>
<button type="button" onclick="document.location = '/admin/addshowmatch'" style="margin-top:-55px; margin-right:5px;" class="btn btn-action pull-right">Add New</button>
<div class="box">
        <h1>All Show Matches</h1>
        <div class="inner_box" style="margin-top:10px">
            <div class="row" style="margin-left:0px;">
                <table class="table table-condensed table-striped table-hover showmatchList">
                    <thead>
                        <tr>
                            <th>Event Id</th>
                            <th>Event Title</th>
                            <th>Game</th>   
                            <th>Start Date</th>     
                            <th>End Date</th>     
                    </thead>
                    <tbody>
                        <?php
                        foreach($matches as $t) {
                            echo "<tr onclick='document.location = \"editshowmatch/{$t['id']}\"'><td>{$t['id']}</td><td><a href='/admin/editshowmatch/{$t['id']}'>{$t['name']}</a></td><td>{$t['game_id']}</td><td>".date("Y-m-d H:i:s",$t['startdatetime'])."</td><td>".date("Y-m-d H:i:s",$t['enddatetime'])."</td></tr>";
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
</div>
<?php
}
else
{
    echo "You do not have access to this page.";
}
?>
</div>