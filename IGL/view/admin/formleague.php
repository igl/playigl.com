<form id="leagueForm" method="post" enctype='multipart/form-data'>
    <div class='box'>
    <h1>League Settings</h1>
    <div stye='margin-top:12px' class='inner_box'>
            <div class="row">
                <div class="span5 pull-left">
                    <h3 class="tooltip-on">Game <a><i class="icon-question-sign tooltip-on" title="Select the game affiliated with the league."></i></a></h3>
                    <select name="game" class="span3">
                    <?php
                    foreach( $games as $g )
                    {
                        if( $g['id'] == $league_data['game'] )
                        {
                            echo "<option value='{$g['id']}' selected='selected'>{$g['name']}</option>";
                        }
                        else
                        {
                            echo "<option value='{$g['id']}'>{$g['name']}</option>";
                        }
                    }

                    ?>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="span6 pull-left"><h3>Division name <a><i class="icon-question-sign tooltip-on" title="Displayed as a 'division'"></i></a></h3>
                    <select name="name">
                        <?php
                        $divisions = array(
                            'Open' => 'Open',
                            'Main' => 'Main',
                            'Select' => 'Select',
                            'Premier' => 'Premier',
                            );
                        foreach( $divisions as $i => $d )
                        {
                            if( $i == $league_data['name'] )
                            {
                                echo "<option value='{$i}' selected='selected'>{$i}</option>";
                            }
                            else
                                echo "<option value='{$i}'>{$i}</option>";
                        }
                        
                        ?>
                    </select>
                </div>
                <div class="span5 pull-left"><h3>Status <a><i class="icon-question-sign tooltip-on" title="Inative leagues are hidden"></i></a></h3>
                    <select class='loginfield' name='status'>
                <?php
                if($league_data['status'] == 1)
                {
                    echo " <option value='1' selected='selected'>Active</option>";
                    echo " <option value='0'>Inactive</option>";
                }
                else
                {
                    echo " <option value='1'>Active</option>";
                    echo " <option value='0' selected='selected'>Inactive</option>";
                }
                ?>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class='span6 pull-left'>
                    <h3>Team Size <a><i class="icon-question-sign tooltip-on" title="Number of players required to register"></i></a></h3>
                    <input class='span3' type='textfield' name='teamsize' value='<?php if( $mode=='edit' ){ echo  $league_data['teamsize']; } ?>'>
                </div>
                <div class='span5 pull-left'>
                    <h3>Rounds <a><i class="icon-question-sign tooltip-on" title="How many times do the teams play to determine a match"></i></a></h3>
                    <input class='span3' type='textfield' name='rounds' value='<?php if( $mode=='edit' ){ echo  $league_data['rounds']; } ?>'>
                </div>
            </div>
        
        
        <div class="row">
                <div class='span6 pull-left'>
                    <h3>Weight <a><i class="icon-question-sign tooltip-on" title="Used to calculate RPI. Starting at 1.0, increment by .1 every skill tier."></i></a></h3>
                    <input class='span3' type='textfield' name='weight' value='<?php if( $mode=='edit' ){ echo  $league_data['weight']; } ?>'>
                </div>
                <div class="span5 pull-left">
                    <h3>Format <a><i class="icon-question-sign tooltip-on" title="Only used for visual reference. Typically 5 vs 5."></i></a></h3>
                    <input class='span3' type='textfield' name='format' value='<?php if( $mode=='edit' ){ echo  $league_data['format']; } ?>'>
                </div>
            </div>
            


        </div>

    <input id='id' type='hidden' name='id' value='<?php if( $mode=='edit' ){ echo  $_REQUEST['id']; } ?>' >
    </div>



    <div class='box'>
    <h1>Scheduling</h1>
    <div class="inner_box">
    <div class="row">
        <div class='span6 pull-left'>
            <h3>Day of the week <a><i class="icon-question-sign tooltip-on" title="Matches will be pre-set for this day of the week."></i></a></h3>
            <select name="day" class="span3">
            <?php
            $day = array(
                'Sunday',
                'Monday',
                'Tuesday',
                'Wednesday',
                'Thursday',
                'Friday',
                'Saturday'
            );
            foreach( $day as $d )
            {
                if( $d == $league_data['day'] )
                {
                    echo "<option selected='selected'>{$d}</option>";
                }
                else
                {
                    echo "<option>{$d}</option>";
                }
            }
                   
            ?>
            </select>
        </div>
        <div class="span5 pull-left">
            <h3>Default Time <small>GMT <span rel="offset"></span></small> <a><i class="icon-question-sign tooltip-on" title="Default time matches are scheduled for. Teams may reschedule."></i></a></h3>
            <input id="time" class='span3' type='textfield' name='time' value='<?php if( $mode=='edit' ){ echo  date('H:i',$league_data['time']); } ?>'>
            <input id="offset" rel="offset" type='hidden' name='offset' value=''>
        </div>
    </div>
    <div class="clear well-small">All leagues/divisions for a specific game should be scheduled for the same day & time be default.</div>
    <div class="row">
        <div class='span6 pull-left'>
            <h3>Total weeks <a><i class="icon-question-sign tooltip-on" title="Total weeks in a season, not including playoffs."></i></a></h3>
            <input class='span3' type='textfield' name='totalweeks' value='<?php if( $mode=='edit' ){ echo  $league_data['totalweeks']; } ?>'>
        </div>
        <div class='span5 pull-left'>
            <h3>Current week <a><i class="icon-question-sign tooltip-on" title="Current week the season is on. Starts at 1."></i></a></h3>
            <input class='span3' type='textfield' name='currentweek' value='<?php if( $mode=='edit' ){ echo  $league_data['currentweek']; } ?>'>
        </div>
    </div>
   
   <div class="row">
        <div class="span6 pull-left">
            <h3>Live date <a><i class="icon-question-sign tooltip-on" title="League matches begin on this day."></i></a></h3>
            <input id="livedate" class='span3' type='textfield' name='livedate' value='<?php if( $mode=='edit' ){ echo  date('Y-m-d',$league_data['livedate']); } ?>'>
        </div>
        <div class="span5 pull-left">
            <h3>Current season <a><i class="icon-question-sign tooltip-on" title="Current season of the league. Starts at 1."></i></a></h3>
            <input id="livedate" class='span3' type='textfield' name='currentseason' value='<?php if( $mode=='edit' ){ echo  $league_data['currentseason']; } ?>'>
        </div>
    </div>
    
    </div>
    </div>
    
    <div class='box'>
    <h1>Images</h1>
    <div stye='margin-top:12px' class='inner_box'>
        <img id="icon" style="margin:2px;" data-src="/js/holder.js/18x18" class='img-polaroid tooltip-on' title='18 x 18' <?php if($mode=='edit' && $league_data['icon']) echo "src='/img{$league_data['icon']}'"; ?>><input style='visibility:hidden;' type='file' name='icon'><br />
        <img id="logo" style="margin:2px;" data-src="/js/holder.js/40x40" class='img-polaroid tooltip-on' title='40 x 40' <?php if($mode=='edit' && $league_data['logo']) echo "src='/img{$league_data['logo']}'"; ?>><input style='visibility:hidden;' type='file' name='logo'><br />
    </div>
    </div>
    
    <div class='box'>      
    <h1>Rules</h1>
    <div stye='margin-top:12px' class='inner_box'>
        <textarea style='height: 600px;' id='rules' name='rules'><?php if( $mode=='edit' ){ echo  $league_data['rules']; } ?></textarea>
    </div>
    </div>
    <input type="hidden" name="id" value="<?php if( $mode=='edit' ){ echo  $league_data['id']; } ?>">
    </form>