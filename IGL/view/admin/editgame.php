<div class="span12">
<?php
include('./view/admin/subnavigation.php');
if( SUPERUSER==true )
{
    echo '<div onclick="$(\'#gameForm\').submit();" style="margin-top:-56px; margin-right:5px;" class="btn btn-action pull-right">Save</div>';
    $game   = new game();

    if($_POST)
    {
        if($game->save())
        {
            ?>
            <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">x</button>Game updated successfully!</div>
            <?php
        }
        else
        {
            ?>
            <div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">x</button>Error updating game.</div>
            <?php
        }
    }

    
    if(isset($_REQUEST['id']) && is_numeric($_REQUEST['id']))
    {
        $game_id = (int)$_REQUEST['id'];
        $game_data = $game->get($game_id);

        $mode = 'edit';
        include('formgame.php');

    }
    else
    {
        ?>
        <div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">x</button>Invalid game specified.</div>
        <?php
    }
}
else
{
    echo "You do not have access to this page.";
}
?>
</div>