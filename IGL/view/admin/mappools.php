<div class="span12">
<?php
include('./view/admin/subnavigation.php');
if( LEAGUEADMIN )
{    
     // Classes
     $game = new game();
     $game_name = $game->get($_REQUEST['id']);
     $game_name = $game_name['name'];

     $maps = new maps();
     $mappools = $maps->getMapPools($_REQUEST['id']);
     ?>
        <button type="button" style="margin-top:-55px; margin-right:105px;" onclick="document.location = '/admin/uploadmap/<?php echo $_REQUEST['id']; ?>'" class="btn btn-action pull-right">Manage Maps</button>

    <button type="button" style="margin-top:-55px; margin-right:5px;" onclick="document.location = '/admin/addmappool/<?php echo $_REQUEST['id']; ?>'" class="btn btn-action pull-right">New Pool</button>
<div class="box">
        <h1>Map Pools - <?php echo $game_name; ?></h1>
        <div class="inner_box" style="margin-top:10px">
            <div class="row" style="margin-left:0px;">
                <table class="table table-condensed table-striped table-hover gameList">
                    <thead>
                        <tr>
                            <th>Map Pool ID</th>
                            <th>Map Pool Name</th>  
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($mappools)) {
                        foreach($mappools as $mp) {
                            echo "<tr><td>{$mp['id']}</td><td>{$mp['name']}</td><td><a href='/admin/editmappool/{$mp['id']}' class='btn tooltip-on' title='Edit Pool'><i class='icon-pencil'></i></a> <a href='/admin/deletemappool/{$mp['id']}/' class='btn btn-danger tooltip-on' title='Remove Pool'><i class='icon-remove'></i></a></td></tr>";
                        }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
</div>
<?php
}
else
{
    echo "You do not have access to this page.";
}
?>
</div>