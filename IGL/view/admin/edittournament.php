<div class="span12">
<?php

include('./view/admin/subnavigation.php');

if( LEAGUEADMIN )
{
    
    if (isset($_REQUEST['id'])) {
    
    $mode = "edit";
    $tournamentId = $_REQUEST['id'];
    // Includes
     require_once('./model/tournament.class.php');
     
     // Classes
     $game = new game();
     $league = new league();
     $tournament = new tournament();
     $tournament = new tournament();
     $maps = new maps();
     
    // Option Buttons
    echo '<div onclick="$(\'#action\').val(\'save\'); $(\'#adminForm\').submit();" style="margin-top:-55px; margin-right:5px;" class="btn btn-action pull-right">Save Tournament</div>';
    $tour_info = $tournament->get($tournamentId); 

     // Actions
     if (isset($_POST['action'])) {
         if ($tournament->save($_POST['id'])) {
             $tour_info = $tournament->get($tournamentId); // Reload tournament info
             echo '<div class="alert alert-success">Tournament updated.</div>';
         } else {
             echo '<div class="alert alert-error"><strong>Error.</strong> The tournament could not be updated.</div>';
         }
     }
     
     include('./view/admin/formtournament.php');
     
     echo '<div onclick="$(\'#action\').val(\'delete\'); $(\'#adminForm\').attr(\'action\', \'../deletetournament/\'); $(\'#adminForm\').submit();" style="margin-bottom:14px;" class="btn btn-danger pull-right">Delete Tournament</div>';
    
    } else {
        echo '<div class="alert alert-error"><strong>Error.</strong> No tournament id provided.</div>';
    }

}
else
{
    echo "You do not have access to this page.";
}
?>
</div>