<?
if(IGL_PAGE!=true || PRESS == false)
die();
?>
<div class="navbar">
    <div class="navbar-inner">
   
    <ul class="nav" style="padding: 0px 0px 0px 8px">
    <li>
        <div class="dropdown">
            <a data-toggle="dropdown">Articles</a>
            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
            <li><a tabindex="-1" href="/admin/allarticles">All Articles</a></li>
            <li><a tabindex="-1" href="/admin/addarticle">Add Articles</a></li>
            </ul>
        </div>
    </li>
    <li class="divider-vertical"></li>
    <li>
        <div class="dropdown">
            <a data-toggle="dropdown">News</a>
            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
            <li><a tabindex="-1" href="/admin/allnews">All News</a></li>
            <li><a tabindex="-1" href="/admin/addnews">Add News</a></li>
            </ul>
        </div>
    </li>
        <li class="divider-vertical"></li>
    <li>
        <div class="dropdown">
            <a data-toggle="dropdown">Ads</a>
            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
            <li><a tabindex="-1" href="/admin/ads">All Ads</a></li>
            </ul>
        </div>
    </li>
    <li class="divider-vertical"></li>
    <li>
        <div class="dropdown">
            <a href="#" data-toggle="dropdown">Games</a>
            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
            <li><a tabindex="-1" href="/admin/allgames">All Games</a></li>
            <li><a tabindex="-1" href="/admin/addgame">Add Game</a></li>
            </ul>
        </div>
    </li>
    <li class="divider-vertical"></li>
    <li>
        <div class="dropdown">
            <a href="#" data-toggle="dropdown">Configs</a>
            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
            <li><a tabindex="-1" href="/admin/allconfigs">All Configs</a></li>
            <li><a tabindex="-1" href="/admin/addconfig">Add Config</a></li>
            </ul>
        </div>
    </li>
    <li class="divider-vertical"></li>
    <li>
        <div class="dropdown">
            <a href="#" data-toggle="dropdown">Competitions</a>
            <ul class="dropdown-menu" style="padding-right:10px;" role="menu" aria-labelledby="dLabel">
                <li class="dropdown-submenu">
                    <a tabindex="-1" href="#">Leagues</a>
                    <ul class="dropdown-menu">
                        <li><a tabindex="-1" href="/admin/allleagues">All Leagues</a></li>
                        <li><a tabindex="-1" href="/admin/addleague">Add League</a></li>
                    </ul>
                </li>
                <li class="dropdown-submenu">
                    <a tabindex="-1" href="#">Ladders</a>
                    <ul class="dropdown-menu">
                         <li><a tabindex="-1" href="/admin/allladders">All Ladders</a></li>
                         <li><a tabindex="-1" href="/admin/addladder">Add Ladder</a></li>
                    </ul>
                </li>
                <li class="dropdown-submenu">
                    <a tabindex="-1" href="#">Tournaments</a>
                    <ul class="dropdown-menu">
                        <li><a tabindex="-1" href="/admin/tournaments">Tournament Manager</a></li>
                        <li><a tabindex="-1" href="/admin/minievent">Mini Events</a></li>
                    </ul>
                </li>
                <li class="dropdown-submenu">
                    <a tabindex="-1" href="#">Show Matches</a>
                    <ul class="dropdown-menu">
                         <li><a tabindex="-1" href="/admin/allshowmatches">All Show Matches</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </li>
    <li class="divider-vertical"></li>
    <li>
        <div class="dropdown">
            <a data-toggle="dropdown">Teams</a>
            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
            <li><a tabindex="-1" href="/admin/allteams">All Teams</a></li>
            <li><a tabindex="-1" href="/admin/addteam">Add Team</a></li>
            </ul>
        </div>
    </li>
    <li class="divider-vertical"></li>
    <li>
        <div class="dropdown">
            <a data-toggle="dropdown">Players</a>
            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
            <li><a tabindex="-1" href="/admin/allplayers">All Players</a></li>
            <li><a tabindex="-1" href="/admin/addplayer">Add Player</a></li>
            </ul>
        </div>
    </li>
    </ul>
    </div>
    </div>



    