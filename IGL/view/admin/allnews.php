<div class="span12">
<?php
include('./view/admin/subnavigation.php');
if( PRESS )
{    
     // Classes
     $news = new news();
     $news_data = $news->all();
     echo '<div onclick="document.location = \'/admin/addnews\'" style="margin-top:-55px; margin-right:5px" class="btn btn-action pull-right">Add New</div>';
     ?>
<div class="box" style="margin-top:12px">
        <h1>All news</h1>
        <div class="inner_box" style="margin-top:10px">
            <div class="row" style="margin-left:0px;">
                <table class="table table-condensed table-striped table-hover newsList">
                    <thead>
                        <tr>
                            <th>News Id</th>
                            <th>News Game</th>
                            <th>News Date</th>  
                            <th>News Title</th>  
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach($news_data as $n) {
                            echo "<tr><td>{$n['id']}</td><td>{$n['game_name']}</td><td>".date('l M jS, Y',$n['dateposted'])."</td><td>{$n['title']}</td><td><a href='/admin/editnews/{$n['id']}' class='btn tooltip-on' title='Edit news'><i class='icon-pencil'></i></a> <a data-href='/admin/deletenews/{$n['id']}' rel='remove' class='btn btn-danger tooltip-on' title='Remove news'><i class='icon-remove'></i></a></td></tr>";
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
<div class="modal hide" id="Modal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hide="true">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hide="true"><i class='icon-remove'></i></button>
<h3 id="ModalLabel">LOADING</h3>
</div>
<div class="modal-body" id="ModalBody">
<div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div>
</div>
<div class="modal-footer">
<button id="ModalButtonClose" class="btn" data-dismiss="modal" aria-hide="true">CLOSE</button>
<button id="ModalButton" class="btn btn-primary">SUBMIT</button>
</div>
</div>
</div>
<?php
}
else
{
    echo "You do not have access to this page.";
}
?>
</div>