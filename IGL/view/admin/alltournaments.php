<div class="span12">
<?php
include('./view/admin/subnavigation.php');

if( LEAGUEADMIN )
{
     // Classes
     $game = new game();
     $tournament = new tournament();
     $tournaments = $tournament->all();
?>
<style>

    .tournamentList tbody tr {
        cursor:pointer;
    }
</style>
<button type="button" onclick="document.location = '/admin/addtournament'" style="margin-top:-55px; margin-right:5px;" class="btn btn-action pull-right">Add New</button>
<div class="box">
        <h1>All Tournaments</h1>
        <div class="inner_box" style="margin-top:10px">
            <div class="row" style="margin-left:0px;">
                <table class="table table-condensed table-striped table-hover tournamentList">
                    <thead>
                        <tr>
                            <th>Event Id</th>
                            <th>Event Title</th>
                            <th>Game</th>   
                            <th>Start Date</th>     
                            <th>End Date</th>     
                    </thead>
                    <tbody>
                        <?php
                        foreach($tournaments as $t) {
                            if ($t['tournament_status'] != 0) {
                                echo "<tr onclick='document.location = \"edittournament/{$t['tournament_id']}\"'><td>{$t['tournament_id']}</td><td><a href='/admin/edittournament/{$t['tournament_id']}'>{$t['tournament_name']}</a></td><td>{$t['tournament_game_name']}</td><td>".date("Y-m-d H:i:s",$t['tournament_startdatetime'])."</td><td>".date("Y-m-d H:i:s",$t['tournament_enddatetime'])."</td></tr>";
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
</div>
<?php
}
else
{
    echo "You do not have access to this page.";
}
?>
</div>