<div class="span12">
<?php
include('./view/admin/subnavigation.php');
if( SUPERUSER==true )
{
    //classes
    $team = new team();
    $game = new game();
    $player = new player();
    
    //method calls
    $games = $game->all(); 
    $players = $player->all();

    echo '<div onclick="$(\'#teamForm\').submit();" style="margin-top:-56px; margin-right:5px;" class="btn btn-action pull-right">Save</div>';
    if($_POST)
    {
        $result = $team->add();
        if($result['success'])
        {
            ?>
            <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">x</button>Team added successfully!</div>
            <?php
        }
        else
        {
            echo "<div class='alert alert-error'><button type='button' class='close' data-dismiss='alert'>x</button>{$result['error']}</div>";
        }
    }

    //get game data
    $game_data = json_decode(file_get_contents(BASEURL.'api/game/'),true);
    
    
    $mode = 'add';
    include('formteam.php');
}
else
{
    echo "You do not have access to this page.";
}
?>
</div>