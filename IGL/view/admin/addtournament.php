<?php
include('./view/admin/subnavigation.php');

if( LEAGUEADMIN )
{

     
     // Classes
     $game = new game();
     $league = new league();
     $tournament = new tournament();
     $maps = new maps();
     
     //Deletion Check
     $deleted = false;
     
     $mode = "create";
     
    // Option Buttons
    echo '<div onclick="$(\'#action\').val(\'create\'); $(\'#adminForm\').submit();" style="margin-top:-56px; margin-right:5px;" class="btn btn-action pull-right">Create</div>';

     
     // Actions
     if ($_POST['action'] == "create") {
         if ($tournament->create()) {
             echo '<div class="alert alert-success">Tournament created.</div>';
         } else {
             echo '<div class="alert alert-error"><strong>Error.</strong> The tournament could not be created.</div>';
         }
     }
    
     include('formtournament.php');

}
else
{
    echo "You do not have access to this page.";
}
?>