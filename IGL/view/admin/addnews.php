<div class="span12">
<?php
include('./view/admin/subnavigation.php');
if( SUPERUSER==true )
{
    echo '<div onclick="$(\'#action\').val(\'create\'); $(\'#newsForm\').submit();" style="margin-top:-55px; margin-right:5px" class="btn btn-action pull-right">Create</div>';
    $news   = new news();

    if($_POST)
    {
        $result = $news->add();
        if( $result == true )
        {
            ?>
            <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">x</button>News added successfully!</div>
            <?php
        }
        else
        {
            echo "<div class='alert alert-error'><button type='button' class='close' data-dismiss='alert'>x</button>{$result}.</div>";
        }
    }
    $mode = 'add';
    $game = new game();
    $games = $game->all();  
    include('formnews.php');
}
else
{
    echo "You do not have access to this page.";
}
?>
</div>