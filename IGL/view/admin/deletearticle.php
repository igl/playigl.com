<?php
if( PRESS )
{
    if (isset($_REQUEST['id'])) {
    
    $id = (int)$_REQUEST['id'];
     
     // Classes
     $article = new article();
     
     // Actions
         if ($article->delete($id)) {
             echo '<div class="alert span12">article deleted.</div>';
         } else {
             echo '<div class="alert span12 alert-error"><strong>Error.</strong> The article could not be deleted.</div>';
         }
     
    } else {
        echo '<div class="alert alert-error"><strong>Error.</strong> No article id provided.</div>';
    }

    $articles = $article->all();
    include('allarticles.php');
}
else
{
    echo "You do not have access to this page.";
}
?>