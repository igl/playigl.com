<?php
include('./view/admin/subnavigation.php');
if (LEAGUEADMIN) {
?>
<div id="tournamentManager" style="box-shadow: 0px 0px 5px #888; margin-bottom:20px;">
<div class="navbar navbar-inverse" style="margin-bottom:0;">
  <div class="navbar-inner">
    <a class="brand" href="#"><i class="icon-trophy" style="margin-right:10px;"></i> Tournament Manager</a>
    <ul class="nav">
    </ul>
    <ul class="nav pull-right" style="margin-right:5px;">
        <li id='backBtn' class="btn btn-small btn-inverse" style='margin-right:5px;'><i class="icon-chevron-left"></i> Back</li>
        <li id='newEventBtn' class="btn btn-small btn-inverse" style='margin-right:5px;'><i class="icon-plus"></i> New Event</li>
        <li id='saveEventBtn' class='btn btn-success btn-small pull-right' style='display:none;'><i class='icon-ok-sign'></i> Save</li>
        <li id='updateEventBtn' class='btn btn-success btn-small pull-right' style='display:none;'><i class='icon-ok-sign'></i> Update</li> 
        <li id='updateMembersBtn' class='btn btn-success btn-small pull-right' style='display:none;'><i class='icon-ok-sign'></i> Update</li> 
        <li id='updateScheduleBtn' class='btn btn-success btn-small pull-right' style='display:none;'><i class='icon-ok-sign'></i> Update</li> 
        <li id='updateMatchBtn' class='btn btn-success btn-small pull-right' style='display:none;'><i class='icon-ok-sign'></i> Update</li> 
    </ul>
  </div>
</div>
<div id='tournamentManagerInner' style="min-height:300px; background:#eee;">
    <div style="margin:0px auto; padding-top:20px; width:20px;"><img src="/img/tournamentLoader.gif" /></div>
</div>
</div>
<script type='text/javascript' src='/js/tournamentManager.js'></script>
<?php
} else {
    echo "You do not have access to this page.";
}
?>
