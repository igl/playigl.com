<form id="playerForm" method="post" enctype='multipart/form-data'>
    <div class='box'>
    <h1>Player details</h1>
        <div stye='margin-top:12px' class='inner_box'>
        <div class="row">
            <div class="span6">
            <img id="avatar" width="184px" class="img-polaroid" src="<?php if( $mode=='edit' && isset($player_data['avatar'])) echo "/img{$player_data['avatar']}"; else echo "/js/holder.js/184x184"; ?>">
            <input type="file" name="avatar" style="visibility:hidden;">
            </div>
        </div>
            
        <div class="row">
            <div class='span6 pull-left'>
                <h3>Username <a><i class="icon-question-sign tooltip-on" title="Players's alias/display name."></i></a></h3>
                <input id="username" class='span3' type='textfield' name='username' value='<?php if( $mode=='edit' ){ echo  $player_data['username']; }  ?>'>
            </div>
            <div class='span5 pull-left'>
                <h3>Password <a><i class="icon-question-sign tooltip-on" title="Reset the user password."></i></a></h3>
                <input id="password" class='span3' type='textfield' name='password' value=''>
            </div>
        </div>
        <div class="row">
            <div class='span6 pull-left'>
                <h3>First name <a><i class="icon-question-sign tooltip-on" title="Player's real first name."></i></a></h3>
                <input id="firstname" class='span3' type='textfield' name='firstname' value='<?php if( $mode=='edit' ){ echo  $player_data['firstname']; }  ?>'>
            </div>
            <div class='span5 pull-left'>
                <h3>Last name <a><i class="icon-question-sign tooltip-on" title="Player's real last name."></i></a></h3>
                <input id="lastname" class='span3' type='textfield' name='lastname' value='<?php if( $mode=='edit' ){ echo  $player_data['lastname']; }  ?>'>
            </div>
         </div>
        <div class="row">   
            <div class='span6 pull-left'>
                <h3>City <a><i class="icon-question-sign tooltip-on" title="Player's local city."></i></a></h3>
                <input id="city" class='span3' type='textfield' name='city' value='<?php if( $mode=='edit' ){ echo  $player_data['city']; }  ?>'>
            </div>
            <div class='span5 pull-left'>
                <h3>State <a><i class="icon-question-sign tooltip-on" title="Player's local state."></i></a></h3>
                <input id="state" class='span3' type='textfield' name='state' value='<?php if( $mode=='edit' ){ echo  $player_data['state']; }  ?>'>
            </div>
        </div>
        <div class="row">
            <div class='span6 pull-left'>
                <h3>Country <a><i class="icon-question-sign tooltip-on" title="Player's country of residence."></i></a></h3>
                <input id="country" class='span3' type='textfield' name='country' value='<?php if( $mode=='edit' ){ echo  $player_data['country']; }  ?>'>
            </div>
            <div class='span5 pull-left'>
                <h3>Gender <a><i class="icon-question-sign tooltip-on" title="Male / Female."></i></a></h3>
                <input id="sex" class='span3' type='textfield' name='sex' value='<?php if( $mode=='edit' ){ echo  $player_data['sex']; }  ?>'>
            </div>
        </div>
        <div class="row"> 
            <div class='span6 pull-left'>
                <h3>Community ID <a><i class="icon-question-sign tooltip-on" title="Steam community ID."></i></a></h3>
                <input id="communityid" class='span3' type='textfield' name='communityid' value='<?php if( $mode=='edit' ){ echo  $player_data['communityid']; }  ?>'>
            </div>
            <div class='span5 pull-left'>
                <h3>Email <a><i class="icon-question-sign tooltip-on" title="Player's personal email."></i></a></h3>
                <input id="email" class='span3' type='textfield' name='email' value='<?php if( $mode=='edit' ){ echo  $player_data['email']; }  ?>'>
            </div>
        </div>
        <div class="row">
            <div class='span6 pull-left'>
                <h3>Facebook <a><i class="icon-question-sign tooltip-on" title="Player's personal facebook URL."></i></a></h3>
                <input id="facebook" class='span3' type='textfield' name='facebook' value='<?php if( $mode=='edit' ){ echo  $player_data['facebook']; }  ?>'>
            </div>
            <div class='span5 pull-left'>
                <h3>Twitter <a><i class="icon-question-sign tooltip-on" title="Player's personal twitter handle. Do not include @."></i></a></h3>
                <input id="twitter" class='span3' type='textfield' name='twitter' value='<?php if( $mode=='edit' ){ echo  $player_data['twitter']; }  ?>'>
            </div>
        </div>
        <div class="row">
            <div class='span6 pull-left'>
                <h3>Twitch <a><i class="icon-question-sign tooltip-on" title="Player's twitch user handle."></i></a></h3>
                <input id="twitch" class='span3' type='textfield' name='twitch' value='<?php if( $mode=='edit' ){ echo  $player_data['twitch']; }  ?>'>
            </div>
            <div class='span5 pull-left'>
                <h3>Website <a><i class="icon-question-sign tooltip-on" title="Player's personal website."></i></a></h3>
                <input id="name" class='span3' type='textfield' name='website' value='<?php if( $mode=='edit' ){ echo  $player_data['website']; }  ?>'>
            </div>
        </div>
            
        <div class="row">
            <div class='span6 pull-left'>
                <h3>Permissions <a><i class="icon-question-sign tooltip-on" title="Determine a player's access levels."></i></a></h3>
                <select id="usergroupid" name="usergroupid">
                    <?php
                    $permissions = array(
                        'Standard User' => 2,
                        'Press' => 12,
                        'League Admin' => 10,
                        'Superuser' => 6,
                        
                    );
                    foreach( $permissions as $i => $p )
                    {
                        if( $p == $player_data['usergroupid'])
                        {
                            echo "<option value='{$p}' selected='selected'>{$i}</option>";
                        }
                        else
                            echo "<option value='{$p}'>{$i}</option>";
                    }
                    ?>
                </select>
                
                
            </div>
            <div class='span5 pull-left'>
                <h3>Deleted <a><i class="icon-question-sign tooltip-on" title="Whether a user's account is active."></i></a></h3>
                <select id="deleted" name="deleted">
                    <?php 
                    if($player_data['deleted'] == 1)
                    {
                        echo " <option value='0'>Acive</option>";
                        echo " <option value='1' selected='selected'>Deleted</option>";
                    }
                    else
                    {
                        echo " <option value='0' selected='selected'>Acive</option>";
                        echo " <option value='1'>Deleted</option>";
                    }
                    ?>
                </select>
            </div>
        </div>
        
            
        
        
        </div>
        </div>

    
        

    <input id='id' type='hidden' name='id' value='<?php if( $mode=='edit' ){ echo  $_REQUEST['id']; } ?>' >
</form>