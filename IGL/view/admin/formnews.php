<form id="newsForm" method="post" enctype='multipart/form-data'>
    <div class='box'>
    <h1>News details</h1>
        <div stye='margin-top:12px' class='inner_box'>
        <div class="row">
            <div class="span11">
            <h3>Game <a><i class="icon-question-sign tooltip-on" title="News wil be displayed under pages of the game selected."></i></a></h3>
            <select name="game" class="span3">
            <?php
            print_r($games);
            foreach( $games as $g )
            {
                if( $g['id'] == $news_data['game'] )
                {
                    echo "<option value='{$g['id']}' selected='selected'>{$g['name']}</option>";
                }
                else
                {
                    echo "<option value='{$g['id']}'>{$g['name']}</option>";
                }
            }

            ?>
            </select>
            </div>
        </div>
        <div class="clear">
        <h3>Title <a><i class="icon-question-sign tooltip-on" title="Title of the news. No HTML."></i></a></h3> 
        <input class='span11' type='textfield' name='title' value='<?php if( $mode=='edit' ){ echo  $news_data['title']; } ?>'>
        </div>
        <div class="clear">
        <h3>Content <a><i class="icon-question-sign tooltip-on" title="Body of the news. Accepts HTML mark-up"></i></a></h3>
        <textarea id="details" name="content" style="width:550px; height:150px;"><?php if($mode == "edit") { echo $news_data['content']; } ?></textarea>  
        </div>
        <div class="row">
            <div class='span6 pull-left'>
                <h3>Author <a><i class="icon-question-sign tooltip-on" title="User cited for news creation."></i></a></h3>
                <input class='span3' type='textfield' name='poster' value='<?php if( $mode=='edit' ){ echo  $news_data['poster']; } else echo $_SESSION['username']; ?>'>
            </div>
            <div class="span5 pull-left">
                <h3>Date <a><i class="icon-question-sign tooltip-on" title="Date the news was published."></i></a></h3>
                <input id="date" class='span3' type='textfield' name='dateposted' value='<?php if( $mode=='edit' ){ echo  date('Y-m-d',$news_data['dateposted']); } else echo date('Y-m-d',time()); ?>'>
            </div>
        </div>
        </div>
    </div>

    <input id='id' type='hidden' name='id' value='<?php if( $mode=='edit' ){ echo  $_REQUEST['id']; } ?>' >
</form>