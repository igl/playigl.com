<div class="span12">
<?php
include('./view/admin/subnavigation.php');
if( SUPERUSER==true )
{
    $server_config   = new serverConfig();

    echo '<div onclick="$(\'#server_configForm\').submit();" style="margin-top:-56px; margin-right:5px;" class="btn btn-action pull-right">Save</div>';
    if($_POST)
    {
        if($server_config->save())
        {
            ?>
            <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">x</button>Config saved successfully!</div>
            <?php
        }
        else
        {
            ?>
            <div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">x</button>Error saving config.</div>
            <?php
        }
    }
    
    $game = new game();
    $games = $game->all();  
    
    $mode = 'edit';
    $data = $server_config->get();
    include('formserver_config.php');
}
else
{
    echo "You do not have access to this page.";
}
?>
</div>