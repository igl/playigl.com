<div class="span12">
<?php
include('./view/admin/subnavigation.php');
if( SUPERUSER==true )
{
    $mode = 'add';
    echo '<div onclick="$(\'#action\').val(\'create\'); $(\'#server_configForm\').submit();" style="margin-top:-55px; margin-right:5px" class="btn btn-action pull-right">Save</div>';
    $server_config   = new serverConfig();

    if($_POST)
    {
        if( $server_config->add() )
        {
            ?>
            <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">x</button>Match config added successfully!</div>
            <?php
        }
        else
        {
            echo "<div class='alert alert-error'><button type='button' class='close' data-dismiss='alert'>x</button>Error adding config</div>";
        }
    }
    $game = new game();
    $games = $game->all(); 

    include('formserver_config.php');
}
else
{
    echo "You do not have access to this page.";
}
?>
</div>