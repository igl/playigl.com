<div class="span12">
<?php
include('./view/admin/subnavigation.php');
if( SUPERUSER )
{    
     // Classes
     $team = new team();
     $teams = $team->all();
     
     echo '<div onclick="document.location = \'/admin/addteam\'" style="margin-top:-56px; margin-right:5px;" class="btn btn-action pull-right">Add new</div>';
     ?>
<div class="box" style="margin-top:12px">
    <h1>All teams</h1>
    <div class="inner_box" style="margin-top:10px">
        <div class="row" style="margin-left:0px;">
            <table class="table table-condensed table-striped table-hover teamList">
                <thead>
                    <tr>
                        <th>Team Id</th>
                        <th>Team Tag</th>
                        <th>Team Name</th>
                        <th>Team Captain</th>
                        <th>Game Name</th>  
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach($teams as $t) {
                        echo "<tr><td>{$t['team_id']}</td><td>{$t['team_tag']}</td><td>{$t['team_name']}</td><td>{$t['team_captain_name']}</td><td>{$t['team_game_name']}</td><td><a href='/admin/editteam/{$t['team_id']}' class='btn tooltip-on' title='Edit team'><i class='icon-pencil'></i></a> <a data-href='/admin/deleteteam/{$t['team_id']}' rel='remove' class='btn btn-danger tooltip-on' title='Remove team'><i class='icon-remove'></i></a></td></tr>";
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
<div class="modal hide" id="Modal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hide="true">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hide="true"><i class='icon-remove'></i></button>
<h3 id="ModalLabel">LOADING</h3>
</div>
<div class="modal-body" id="ModalBody">
<div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div>
</div>
<div class="modal-footer">
<button id="ModalButtonClose" class="btn" data-dismiss="modal" aria-hide="true">CLOSE</button>
<button id="ModalButton" class="btn btn-primary">SUBMIT</button>
</div>
</div>    
</div>
<?php
}
else
{
    echo "You do not have access to this page.";
}
?>
</div>