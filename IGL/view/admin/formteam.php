<form id="teamForm" method="post" enctype='multipart/form-data'>
    <div class='box'>
    <h1>Team details</h1>
        <div stye='margin-top:12px' class='inner_box'>
        <div class="row">
            <div class='span6 pull-left'>
                <h3>Name <a><i class="icon-question-sign tooltip-on" title="Team display name."></i></a></h3>
                <input id="name" type='textfield' name='name' value='<?php if( $mode=='edit' ){ echo  $team_data['name']; }  ?>'>
            </div>
            <div class='span5 pull-left'>
                <h3>Featured <a><i class="icon-question-sign tooltip-on" title="Display team on landing page."></i></a></h3>
                <select id="featured" name="featured">
                <?php
                $options = array('No','Yes');
                    foreach( $options as $i => $o )
                    {
                        if( $i == $team_data['featured'] )
                        {
                            echo "<option value='{$i}' selected='selected'>{$o}</option>";
                        }
                        else
                        {
                            echo "<option value='{$i}'>{$o}</option>";
                        }
                    }
                ?>
            </select>
            </div>
        </div>
        <div class="row">
            <div class="span6 pull-left">
                <h3>Tag <a><i class="icon-question-sign tooltip-on" title="Team short name. Often used in-game."></i></a></h3>
                <input id="tag" class='span3' type='textfield' name='tag' value='<?php if( $mode=='edit' ){ echo  $team_data['tag']; }  ?>'>
            </div>
            <div class="span5">
            <h3>Captain <a><i class="icon-question-sign tooltip-on" title="This player will have complete control over a teams actions."></i></a></h3>
            <select id="captain" name="captain" class="span3">
                <?php
                if( isset($team_data) && is_array($team_data['team_roster']) )
                {
                    foreach( $team_data['team_roster'] as $p )
                    {
                        if( $p['player_id'] == $team_data['captain'] )
                        {
                            echo "<option value='{$p['player_id']}' selected='selected'>{$p['player_username']}</option>";
                        }
                        else
                        {
                            echo "<option value='{$p['player_id']}'>{$p['player_username']}</option>";
                        }
                    }
                }
                else 
                {
                    foreach( $players as $p )
                    {
                        echo "<option value='{$p['player_id']}'>{$p['player_username']}</option>";
                    }
                }
                ?>
            </select>
            </div>
        </div>
            
        <div class="row">
            <div class='span6 pull-left'>
                <h3>Open <a><i class="icon-question-sign tooltip-on" title="Whether a team is open to applications."></i></a></h3>
                <select id="open" name="open" class="span3">
                <?php
                    if( isset($team_data['open']) && $team_data['open'] == 0 )
                    {
                        echo "<option value='0' selected='selected'>Closed</option>";
                        echo "<option value='1'>Open</option>";
                    }
                    else
                    {
                        echo "<option value='0'>Closed</option>";
                        echo "<option value='1' selected='selected'>Open</option>";
                    }
                ?>
                </select>
            </div>
            <div class="span5 pull-left">
                <h3>Password <a><i class="icon-question-sign tooltip-on" title="Password for players to join the team. 4 digits."></i></a></h3>
                <input id="password" class='span3' type='textfield' name='password' value='<?php if( $mode=='edit' ){ echo  $team_data['password']; } ?>'>
            </div>
        </div>
            
        <div class="row">
            <div class='span6 pull-left'>
                <h3>Website <a><i class="icon-question-sign tooltip-on" title="Team's web URL."></i></a></h3>
                <input id="website" class='span3' type='textfield' name='website' value='<?php if( $mode=='edit' ){ echo  $team_data['website']; } ?>'>
            </div>
            <div class="span5 pull-left">
                <h3>Steamgroup <a><i class="icon-question-sign tooltip-on" title="Steamgroup."></i></a></h3>
                <input id="steamgroup" class='span3' type='textfield' name='steamgroup' value='<?php if( $mode=='edit' ){ echo  $team_data['steamgroup']; } ?>'>
            </div>
        </div>
        <div class="row">
            <div class='span6 pull-left'>
                <h3>Facebook <a><i class="icon-question-sign tooltip-on" title="Team's Facebook page URL."></i></a></h3>
                <input id="facebook" class='span3' type='textfield' name='facebook' value='<?php if( $mode=='edit' ){ echo  $team_data['facebook']; } ?>'>
            </div>
            <div class="span5 pull-left">
                <h3>Twitter <a><i class="icon-question-sign tooltip-on" title="Team's Twitter handle. Not including @"></i></a></h3>
                <input id="twitter" class='span3' type='textfield' name='twitter' value='<?php if( $mode=='edit' ){ echo  $team_data['twitter']; } ?>'>
            </div>
        </div>
        <div class="row">
            <div class='span6 pull-left'>
                <h3>Continent <a><i class="icon-question-sign tooltip-on" title="Continent - Abbreviated."></i></a></h3>
                <input id="continent" class='span3' type='textfield' name='continent' value='<?php if( $mode=='edit' ){ echo  $team_data['continent']; } ?>'>
            </div>
            <div class="span5 pull-left">
                <h3>Region <a><i class="icon-question-sign tooltip-on" title="Region - North/South, East/Central/West"></i></a></h3>
                <input id="region" class='span3' type='textfield' name='region' value='<?php if( $mode=='edit' ){ echo  $team_data['region']; } ?>'>
            </div>
        </div>
        </div>
        </div>

    
        <div class='box'>
        <h1>Competition details</h1>
        <div stye='margin-top:12px' class='inner_box'>
            
            
        <div class="row">
            <div class="span11">
            <h3>Game <a><i class="icon-question-sign tooltip-on" title="Select the game the team will participate in. Required."></i></a></h3>
            <select id="teamGame" name="game" class="span9">
            <?php
            if($mode=='add')
                    echo "<option value='0'>---</option>";
            foreach( $games as $g )
            {
                if( isset($team_data['game']) && $g['id'] == $team_data['game'] )
                {
                    echo "<option value='{$g['id']}' selected='selected'>{$g['name']}</option>";
                }
                else
                {
                    echo "<option value='{$g['id']}'>{$g['name']}</option>";
                }
            }

            ?>
            </select>
            </div>
        </div>

        
        </div>
    </div>

    <input id='id' type='hidden' name='id' value='<?php if( $mode=='edit' ){ echo  $_REQUEST['id']; } ?>' >
</form>