<div class="span12">
<?php
include('./view/admin/subnavigation.php');
if( PRESS )
{
    echo '<div onclick="$(\'#newsForm\').submit();" style="margin-top:-56px; margin-right:5px;" class="btn btn-action pull-right">Save</div>';
    $news   = new news();

    if($_POST)
    {
        if($news->save())
        {
            ?>
            <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">x</button>News updated successfully!</div>
            <?php
        }
        else
        {
            ?>
            <div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">x</button>Error updating news.</div>
            <?php
        }
    }

    
    if(isset($_REQUEST['id']) && is_numeric($_REQUEST['id']))
    {
        $mode = 'edit';
        $game = new game();
        $games = $game->all();  
        $news_data = $news->get($_REQUEST['id']);
        include('formnews.php');
    }
    else
    {
        ?>
        <div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">x</button>Invalid news specified.</div>
        <?php
    }
}
else
{
    echo "You do not have access to this page.";
}
?>
</div>