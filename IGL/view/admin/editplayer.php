<div class="span12">
<?php
include('./view/admin/subnavigation.php');
if( SUPERUSER==true )
{
    $player   = new player();
    $id = (int)$_REQUEST['id'];

    echo '<div onclick="$(\'#playerForm\').submit();" style="margin-top:-56px; margin-right:5px;" class="btn btn-action pull-right">Save</div>';
    if($_POST)
    {
        $result = $player->save();
        if($result['success'])
        {
            ?>
            <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">x</button>Player saved successfully!</div>
            <?php
        }
        else
        {
            echo "<div class='alert alert-error'><button type='button' class='close' data-dismiss='alert'>x</button>{$result['error']}</div>";
        }
    }
    
    $game = new game();
    $games = $game->all();  
    
    $player_data = $player->get($id);
    $mode = 'edit';
    include('formplayer.php');
}
else
{
    echo "You do not have access to this page.";
}
?>
</div>