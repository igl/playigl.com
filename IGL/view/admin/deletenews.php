<?php
if( PRESS )
{
    if (isset($_REQUEST['id'])) {
    
    $id = (int)$_REQUEST['id'];
     
     // Classes
     $news = new news();
     
     // Actions
         if ($news->delete($id)) {
             echo '<div class="alert span12">news deleted.</div>';
         } else {
             echo '<div class="alert span12 alert-error"><strong>Error.</strong> The news could not be deleted.</div>';
         }
     
    } else {
        echo '<div class="alert alert-error"><strong>Error.</strong> No news id provided.</div>';
    }

    $news_data = $news->all();
    include('allnews.php');
}
else
{
    echo "You do not have access to this page.";
}
?>