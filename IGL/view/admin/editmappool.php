<div class="span12">
<?php
include('./view/admin/subnavigation.php');
if( LEAGUEADMIN )
{
   
     $maps = new maps();
     $mappool = $maps->getMapPool($_REQUEST['id']);
     $maplist = $maps->getAll($mappool['game']);
    ?>
   <button type="button" style="margin-top:-55px; margin-right:5px;" onclick="savePool()" class="btn btn-action pull-right">Save</button>

   <div style="display:none;" class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><h4>Done!</h4>Map pool saved.</div>
   <div style="display:none;" class="alert alert-block"><button type="button" class="close" data-dismiss="alert">&times;</button><h4>Error!</h4>Could not save map pool</div>
   
    <div class="box">
        <h1>Edit Map Pool</h1>
        <div class="inner_box" style="margin-top:10px">
            <div class="row" style="margin-left:0px;">
                <fieldset>
                    <label>Map Pool Name</label>
                    <input id="name" name="name" type="text" style="width:550px;" value="<?php echo $mappool['name']; ?>">
                    <input type="hidden" name="game" id="game" value="<?php echo $mappool['game']; ?>" />
                </fieldset>
            </div>
        </div>
    </div>
    
     <div class="box">
        <h1>Maps</h1>
        <div class="inner_box" style="margin-top:10px">
            <div class="row" style="margin-left:0px;">
                <fieldset>
                    <label>Game Maps</label>
                    <select id="map_select">
                        <?php
                        if (!empty($maplist)) {
                        foreach($maplist as $ml) {
                            $ml['id'] = addslashes($ml['id']);
                            echo "<option value='{$ml['id']}'>{$ml['map']}</option>";
                        }
                        }
                        ?>
                    </select>
                    <div id="add_map" class="btn btn-success">Add Map</div>
                    
                    <table class="table table-condensed table-striped table-hover gameList">
                    <thead>
                        <tr>
                            <th>Map ID</th>
                            <th>Game</th>  
                            <th>Map Name</th> 
                            <th>Action</th> 
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $edit_maps = rtrim($mappool['poolarray'],',');    
                            $map_array = explode(',', $edit_maps);
                            foreach($map_array as $m) {
                                $m = str_replace("'", "", $m);
                                $this_map = $maps->getMap($m);
                                if ($this_map) {
                                    echo "<tr><td>".$this_map['id']."</td><td>".$this_map['game_name']."</td><td>".$this_map['map']."</td><td><div onclick='$(this).parent().parent().remove()' class='btn btn-danger tooltip-on' title='Remove Pool'><i class='icon-remove'></i></div></td></tr>";
                                }
                            }
                        ?>
                    </tbody>
                    </table>
                
                </fieldset>
            </div>
        </div>
    </div>
    
    <script type="text/javascript">
        window.onload = function() {
        var map_array = new Array();
         <?php
           if (!empty($maplist)) {
              foreach($maplist as $ml) {
                   echo "map_array[{$ml['id']}] = new Array('{$ml['id']}', '{$ml['game_name']}', '{$ml['map']}');";
              }
          }
        ?>
        $('#add_map').click(function() {
           var select_id = $('#map_select').val();
            $('tbody').append("<tr><td>"+map_array[select_id][0]+"</td><td>"+map_array[select_id][1]+"</td><td>"+map_array[select_id][2]+"</td><td><div onclick='$(this).parent().parent().remove()' class='btn btn-danger tooltip-on' title='Remove Pool'><i class='icon-remove'></i></div></td></tr>");
        });
        };
        
        function savePool() {
            var id = <?php echo $_REQUEST['id']; ?>;
            var game = "<?php echo $mappool['game']; ?>";
            var name = $('#name').val();
            var map_pool = "";
            $('tbody tr').each(function(i, v) {
                map_pool += $(v).children(":first").text()+",";
            });
            $.ajax({ url: "/ajax/admin/mappools.php?action=updatePool&id="+id+"&game="+game+"&name="+name+"&mappool="+map_pool }).done(function(response) {
                if (response.error == null) {
                    document.location = "/admin/mappools/"+game;
                } else {
                    $('.alert-block').fadeIn();
                }
            });
        }
    </script>
    
    <?php
}
else
{
    echo "You do not have access to this page.";
}
?>
</div>