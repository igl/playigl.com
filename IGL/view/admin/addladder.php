<div class="span12">
<?php
include('./view/admin/subnavigation.php');

if( SUPERUSER )
{
    $mode = "add";
    $ladderId = $_REQUEST['id'];

     // Classes
     $game = new game();
     $ladder = new ladder();

    // Option Buttons
    echo '<div onclick="$(\'#action\').val(\'save\'); $(\'#ladderForm\').submit();" style="margin-top:-55px; margin-right:5px" class="btn btn-action pull-right">Save Ladder</div>';


    // Actions
    if (isset($_POST['action']) && $_POST['action'] == "save") 
    {
        $action = $ladder->add();
        if( $action['success'] ) 
        {
            $mode ='edit';
            $ladder_info = $ladder->get($action['ladder_id']); // Reload ladder info
            echo '<div class="alert alert-success">Ladder updated.</div>';
        } 
        else 
        {
            echo "<div class='alert alert-error'><strong>Error.</strong>{$action['error']}</div>";
        }
    }

    $maps = new maps();
    $map_pools = $maps->getMapPools($ladder_info['game']);
    
    $configs = new serverConfig();
    $match_configs = $configs->all('match');
    
    $game = new game();
    $games = $game->all();
    
    include('formladder.php');

    echo '<div onclick="$(\'#action\').val(\'delete\'); $(\'#adminForm\').attr(\'action\', \'../admin/deleteladder\'); $(\'#adminForm\').submit();" style="margin-bottom:14px;" class="btn btn-danger pull-right">Delete Ladder</div>';
}
else
{
    echo "You do not have access to this page.";
}
?>
</div>