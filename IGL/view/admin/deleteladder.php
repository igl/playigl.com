<?php
if( SUPERUSER )
{
    if (isset($_REQUEST['id'])) {
    
    $id = (int)$_REQUEST['id'];
     
     // Classes
     $ladder = new ladder();
     
     // Actions
         if ($ladder->delete($id)) {
             echo '<div class="alert span12">Ladder deleted.</div>';
         } else {
             echo '<div class="alert span12 alert-error"><strong>Error.</strong> The ladder could not be deleted.</div>';
         }
     
    } else {
        echo '<div class="alert alert-error"><strong>Error.</strong> No ladder id provided.</div>';
    }

    $ladders = $ladder->all();
    include('allladders.php');
}
else
{
    echo "You do not have access to this page.";
}
?>