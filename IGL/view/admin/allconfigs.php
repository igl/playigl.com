<div class="span12">
<?php
include('./view/admin/subnavigation.php');
if( SUPERUSER )
{    
     // Classes
     $server_config   = new serverConfig();
     $configs = $server_config->all();
     
     echo '<div onclick="document.location = \'/admin/addconfig\'" style="margin-top:-56px; margin-right:5px;" class="btn btn-action pull-right">Add Config</div>';
     ?>
<div class="box" style="margin-top:12px">
    <h1>All Configs</h1>
    <div class="inner_box" style="margin-top:10px">
        <div class="row" style="margin-left:0px;">
            <table class="table table-condensed table-striped table-hover playerList">
                <thead>
                    <tr>
                        <th>Config Id</th>
                        <th>Game</th>
                        <th>Config Type</th>
                        <th>Name</th>
                        <th>Action</th>
                </thead>
                <tbody>
                    <?php
                    foreach($configs as $c) {
                        echo "<tr><td>{$c['id']}</td><td>{$c['game_name']}</td><td>{$c['type']}</td><td>{$c['name']}</td><td><a href='/admin/editconfig/{$c['id']}' class='btn tooltip-on' title='Edit player'><i class='icon-pencil'></i></a> <a data-href='/admin/deleteconfig/{$c['id']}' rel='remove' class='btn btn-danger tooltip-on' title='Remove player'><i class='icon-remove'></i></a></td></tr>";
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>  
<div class="modal hide" id="Modal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hide="true">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hide="true"><i class='icon-remove'></i></button>
<h3 id="ModalLabel">LOADING</h3>
</div>
<div class="modal-body" id="ModalBody">
<div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div>
</div>
<div class="modal-footer">
<button id="ModalButtonClose" class="btn" data-dismiss="modal" aria-hide="true">CLOSE</button>
<button id="ModalButton" class="btn btn-primary">SUBMIT</button>
</div>
</div> 
</div>
<?php
}
else
{
    echo "You do not have access to this page.";
}
?>
</div>