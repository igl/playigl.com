<div class="span12">
<?php
include('./view/admin/subnavigation.php');

if( LEAGUEADMIN )
{    
     // Classes
     $league = new league();
     $leagues = $league->all();
     
     echo '<div onclick="document.location = \'/admin/addleague\'" style="margin-top:-56px; margin-right:5px;" class="btn btn-action pull-right">Add new</div>';
     ?>
<div class="box" style="margin-top:12px">
    <h1>All leagues</h1>
    <div class="inner_box" style="margin-top:10px">
        <div class="row" style="margin-left:0px;">
            <table class="table table-condensed table-striped table-hover leagueList">
                <thead>
                    <tr>
                        <th>League Id</th>
                        <th>League Name</th>  
                        <th>Game Name</th>  
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach($leagues as $l) {
                        echo "<tr><td>{$l['league_id']}</td><td>{$l['league_name']}</td><td>{$l['league_game_name']}</td><td><a href='/admin/editleague/{$l['league_id']}' class='btn tooltip-on' title='Edit league'><i class='icon-pencil'></i></a> <a data-href='/admin/deleteleague/{$l['league_id']}' rel='remove' class='btn btn-danger tooltip-on' title='Remove league'><i class='icon-remove'></i></a></td></tr>";
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
<div class="modal hide" id="Modal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hide="true">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hide="true"><i class='icon-remove'></i></button>
<h3 id="ModalLabel">LOADING</h3>
</div>
<div class="modal-body" id="ModalBody">
<div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div>
</div>
<div class="modal-footer">
<button id="ModalButtonClose" class="btn" data-dismiss="modal" aria-hide="true">CLOSE</button>
<button id="ModalButton" class="btn btn-primary">SUBMIT</button>
</div>
</div>    
</div>
<?php
}
else
{
    echo "You do not have access to this page.";
}
?>
</div>