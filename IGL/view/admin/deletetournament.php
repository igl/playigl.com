<?php

include('./view/admin/subnavigation.php');

if( LEAGUEADMIN )
{
    
    if (isset($_REQUEST['id'])) {
    
    $tournamentId = $_REQUEST['id'];
    
    // Includes
     require_once('./model/tournament.class.php');
     
     // Classes
     $game = new game();
     $tournament = new tournament();
     
     // Actions
         if ($tournament->delete($_POST['id'])) {
             echo '<div class="alert">Tournament deleted.</div>';
         } else {
             echo '<div class="alert alert-error"><strong>Error.</strong> The tournament could not be deleted.</div>';
         }
     
    } else {
        echo '<div class="alert alert-error"><strong>Error.</strong> No tournament id provided.</div>';
    }
?>

<script type="text/javascript">
(function($) {        
    $('#startdatetime').datepicker();
    $('#enddatetime').datepicker();
    $('#checkinstartdatetime').datepicker();
    $('#checkinenddatetime').datepicker();
})(jQuery);
</script>

<?php
}
else
{
    echo "You do not have access to this page.";
}
?>
