<div class="span12">
<?php
include('./view/admin/subnavigation.php');

if( LEAGUEADMIN )
{
    
    if(isset($_POST['save'])) {
        if($_POST['ad_id'] == "new") {
            if ($_FILES['file']['tmp_name'] != "") {
                move_uploaded_file($_FILES["file"]["tmp_name"], "./img/ads/" . $_FILES["file"]["name"]);
                $data = "/img/ads/" . $_FILES["file"]["name"];
            } else {
                $data = "";
            }
            if ($db->Insert(array('name'=>$_POST['name'], 'href'=>$_POST['href'], 'image_data'=>$data, 'is_global'=>$_POST['is_global']), 'ads')) {
                echo '<div class="alert alert-success">Ad created successfuly</div>';
            } else {
                echo '<div class="alert alert-error">Ad could not be created.</div>';
            }
        } else {
            $ad_id = $_POST['ad_id'];
            if ($_FILES['file']['tmp_name'] != "") {
                move_uploaded_file($_FILES["file"]["tmp_name"], "./img/ads/" . $_FILES["file"]["name"]);
                $data = "/img/ads/" . $_FILES["file"]["name"];
            } else {
                $data = "";
            }
            if ($db->Update('ads', array('name'=>$_POST['name'], 'href'=>$_POST['href'], 'image_data'=>$data, 'is_global'=>$_POST['is_global']), array('id'=>$ad_id))) {
                echo '<div class="alert alert-success">Ad updated successfuly</div>';
            } else {
                echo '<div class="alert alert-error">Ad could not be updated.</div>';
            }
        }
    }
    
    if (isset($_POST['delete'])) {
        if ($db->delete('ads', array('id'=>$_POST['ad_id']))) {
                echo '<div class="alert alert-success">Ad deleted successfuly</div>';
            } else {
                echo '<div class="alert alert-error">Ad could not be deleted.</div>';
            }
    }

?>
<div class="box">
        <h1>All Ads</h1>
        <div class="inner_box" style="margin-top:10px">
            <div class="row" style="margin-left:0px;">
                <form action="" method="post" enctype="multipart/form-data">
                <label>Select Ad</label>
                <select id="ad_id" name='ad_id'>
                    <option value="new">New Ad...</option>
                        <?php
                        $query = "SELECT * FROM ads";
                        $db->ExecuteSQL($query);
                        if ($db->iRecords()) {
                            $ads = $db->ArrayResults();
                            foreach($ads as $a) {
                                echo "<option value='".$a['id']."'>".$a['name']."</option>";
                            }
                        }
                        ?>
                </select>
                <label>Name</label>
                <input id="name" name="name" />
                <label>HREF</label>
                <input id="href" name="href"/> 
                <br/><br/>
                <img id="image_data" src="" />
                <br/><br/>
                <label >Image:</label>
                <input type="file" name="file" id="file"><br>
                <br/>
                <input type="checkbox" value="1" name="is_global" id="is_global"/> Is Global<br/><br/>
                <input type="submit" name="save" value="Save">
                <input type="submit" name="delete" value="Delete">
                </form>
            </div>
        </div>
</div>
<?php
}
else
{
    echo "You do not have access to this page.";
}
?>
</div>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>
    window.onload = function() {
        $('#ad_id').change(function() {
            $.ajax({url: "/ajax/admin/ads.php?id="+$('#ad_id').val()}).done(function(data) {
                console.log(data);
               $('#name').val(data.ad.name);
               $('#href').val(data.ad.href);
               $('#is_global').val(data.ad.is_global);
               $('#image_data').attr('src', data.ad.image_data);
            });
        });
    }
</script>