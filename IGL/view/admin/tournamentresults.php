<?php

if( LEAGUEADMIN )
{

    // Includes
     require_once('../model/tournament.class.php');
     
     // Classes
     $game = new game();
     $tournament = new tournament();
     $tournaments = $tournament->getAll();   
?>
<style>

    .tournamentList tbody tr {
        cursor:pointer;
    }
</style>
<div class="box" style="margin-top:12px">
        <h1>Manage Tournament Results</h1>
        <div class="inner_box" style="margin-top:10px">
            <div class="row" style="margin-left:0px;">
                <label>Tournament</label>
                <select id="tournament_id" name="tournament_id">
                        <?php
                        foreach($tournaments as $t) {
                            echo "<option value='".$t['id']."'>".$t['name']."</option>";
                        }
                        ?>
                </select>
                <label>Round</label>
                <select id="round_id" name="round_id">
                </select>
                <div onclick="Tournament.admin.deleteRound()" style="margin-top:-5px; margin-left:10px;" class="btn btn-danger">Delete</div>
                <div onclick="Tournament.admin.generateRound()" style="margin-top:-5px; margin-left:10px;" class="btn btn-success">Generate Next</div>
            </div>
        </div>
</div>

<div id="tournament_activity" style="display:none; text-align:center;"><img src="/img/tournament-loader.gif"/></div>              
<div id="tournament_content" style="margin-top:12px"></div>

<script type="text/javascript" src="../js/tournament.js"></script>
<script type="text/javascript">
(function($) {   
    Tournament.admin.initRounds($('#tournament_id').val());
    $('#tournament_id').change(function() {
        Tournament.tournament_id = $(this).val();
        Tournament.admin.initRounds($(this).val());
    });
    $('#round_id').change(function() {
            Tournament.admin.editRound($(this).val());
    });
})(jQuery);
</script>


<?php
}
else
{
    echo "You do not have access to this page.";
}
?>