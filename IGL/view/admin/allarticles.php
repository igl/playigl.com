<div class="span12">
  <?php
  include('./view/admin/subnavigation.php');
  if (PRESS) {
    // Classes
    $article = new article();
    $articles = $article->all();
    echo '<div onclick="document.location = \'/admin/addarticle\'" style="margin-top:-55px; margin-right:5px" class="btn btn-action pull-right">Add New</div>';

    ?>
    <div class="box" style="margin-top:12px">
      <h1>All articles</h1>
      <div class="inner_box" style="margin-top:10px">
        <div class="row" style="margin-left:0px;">
          <table class="table table-condensed table-striped table-hover articleList">
            <thead>
              <tr>
                <th>Article Id</th>
                <th>Article Date</th>
                <th>Article Title</th>
              </tr>
            </thead>
            <tbody>
              <?php
              foreach ($articles as $a) {
                $date = date('l M jS, Y', strtotime($a['rts']));
                $featured = $a['featured'] ? "icon-star" : "icon-star-empty";
                echo <<< A
                  <tr>
                    <td>{$a['id']}</td>
                    <td>{$date}</td>
                    <td>{$a['title']}</td>
                    <td>
                      <a href='/admin/editarticle/{$a['id']}' class='btn tooltip-on' title='Edit article'><i class='icon-pencil'></i></a>
                      <a class="btn tooltip-on feature-article" title="Front Page Status" onclick="featureArticle(this, {$a['id']})"><i class="{$featured}"></i></a>
                      <a data-href='/admin/deletearticle/{$a['id']}' rel='remove' class='btn btn-danger tooltip-on' title='Remove article'><i class='icon-remove'></i></a>
                      </td>
                  </tr>
A;
              }

              ?>
            </tbody>
          </table>
        </div>
      </div>
      <div class="modal hide" id="Modal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hide="true">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hide="true"><i class='icon-remove'></i></button>
          <h3 id="ModalLabel">LOADING</h3>
        </div>
        <div class="modal-body" id="ModalBody">
          <div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div>
        </div>
        <div class="modal-footer">
          <button id="ModalButtonClose" class="btn" data-dismiss="modal" aria-hide="true">CLOSE</button>
          <button id="ModalButton" class="btn btn-primary">SUBMIT</button>
        </div>
      </div>
    </div>
    <?php
  } else {
    echo "You do not have access to this page.";
  }

  ?>
</div>