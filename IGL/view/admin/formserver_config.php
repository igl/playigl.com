<div class="alert alert-info"><b><i class="icon-info-sign"></i> Tip:</b> Match configs are executed before matches begin on IGL hosted servers. Match configs are executed first.</div>
<form id="server_configForm" method='post' enctype='multipart/form-data'>
    <div class='box'>
        <h1>Match Server Details</h1>
        <div class="inner_box">
            <fieldset>
                <label>Game <br /><select name="game">
                        <?php
                        foreach ($games as $g) {
                            if ($mode == 'edit') {
                                if ($data['game'] == $g['id']) {
                                    echo "<option value='{$g['id']}' selected='selected'>{$g['name']}</option>";
                                } else {
                                    echo "<option value='{$g['id']}'>{$g['name']}</option>";
                                }
                            } else {
                                echo "<option value='{$g['id']}'>{$g['name']}</option>";
                            }
                        }
                        ?></select></label>
                <label>Config Type/Level <br /><select name="type"><option value="map">Map</option><option value="match">Match</option></select></label>
                <label>Name <br /><input name="name" type="text" class="span6" value="<?php if ($mode == 'edit') echo $data['name']; ?>"></label>
                <label>Settings <br /><textarea name="settings" class="span11" rows="500" style="height:500px!important"><?php if ($mode == 'edit') echo $data['settings']; ?></textarea></label>
            </fieldset>
        </div>

    </div>
</form>