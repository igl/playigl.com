<div class="span12">
<?php
include('./view/admin/subnavigation.php');
if( SUPERUSER==true )
{
    $league   = new league();

    echo '<div onclick="$(\'#leagueForm\').submit();" style="margin-top:-56px; margin-right:5px;" class="btn btn-action pull-right">Create</div>';
    if($_POST)
    {
        if($league->add())
        {
            ?>
            <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">x</button>League added successfully!</div>
            <?php
        }
        else
        {
            ?>
            <div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">x</button>Error adding league.</div>
            <?php
        }
    }
    $game = new game();
    $games = $game->all();
    $mode = 'add';
    include('formleague.php');
}
else
{
    echo "You do not have access to this page.";
}
?>
</div>