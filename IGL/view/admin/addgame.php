<div class="span12">
<?php
include('./view/admin/subnavigation.php');
if( SUPERUSER==true )
{
    echo '<div onclick="$(\'#action\').val(\'create\'); $(\'#gameForm\').submit();" style="margin-top:-55px; margin-right:5px" class="btn btn-action pull-right">Create</div>';
    $game   = new game();

    if($_POST)
    {
        if($game->add())
        {
            ?>
            <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">x</button>Game added successfully!</div>
            <?php
        }
        else
        {
            ?>
            <div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">x</button>Error adding game.</div>
            <?php
        }
    }
    $mode = 'add';
    include('formgame.php');
}
else
{
    echo "You do not have access to this page.";
}
?>
</div>