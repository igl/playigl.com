<?php

if( LEAGUEADMIN )
{
    include('./view/admin/subnavigation.php');
    
    if (isset($_POST['id'])) {
    
    $showmatchId = $_POST['id'];
    
    // Includes
     require_once('./model/showmatch.class.php');
     
     // Classes
     $game = new game();
     $showmatch = new showmatch();
     
     // Actions
         if ($showmatch->delete($_POST['id'])) {
             echo '<div class="alert">Showmatch deleted.</div>';
         } else {
             echo '<div class="alert alert-error"><strong>Error.</strong> The showmatch could not be deleted.</div>';
         }
     
    } else {
        echo '<div class="alert alert-error"><strong>Error.</strong> No showmatch id provided.</div>';
    }

}
else
{
    echo "You do not have access to this page.";
}
?>
