<form id="ladderForm" action="" method="POST">
<div class="box" style="margin-top:12px">
    <h1>Ladder</h1>
    <div class="inner_box" style="margin-top:10px">
        <div class="row" style="margin-left:0px;">
            <fieldset class="span6 pull-left">
                <label>Game</label>
                <select name="game" class="span3">
                    <?php
                    foreach( $games as $g )
                    {
                        if( $g['id'] == $ladder_info['game'] )
                        {
                            echo "<option value='{$g['id']}' selected='selected'>{$g['name']}</option>";
                        }
                        else
                        {
                            echo "<option value='{$g['id']}'>{$g['name']}</option>";
                        }
                    }

                    ?>
                    </select>
                <span class="help-block">Assign the ladder a game</span>
            </fieldset>

            <fieldset class="span5">
                <label>Title</label>
                <input id="name" name="name" type="text" value="<?php if($mode == "edit") { echo $ladder_info['name']; } ?>">
                <span class="help-block">Appended to Tier level. Example: Tier 1 Red Bull Ladder</span>
                <input type="hidden" id="action" name="action"/>
                <input type="hidden" id="id" name="id" value="<?php if($mode == "edit") { echo $_REQUEST['id']; } ?>"/>
            </fieldset>
            
            
            <fieldset class="span6 pull-left">
                <label>Game Type</label>
                <input id="format" name="format" type="text" value="<?php if( $mode=='edit' ){ echo $ladder_info['format'];  } ?>">
                <span class="help-block">The format for this ladder. Examples: S&D, 5v5, CTF, Highlander</span>
            </fieldset>
            
            <?php
            $locations = json_decode(file_get_contents(BASEURL . 'ajax/admin/server_locations.php'), true);
            ?>
            <fieldset class="span5">
                <label>Continent / Region</label>
                <select name="continent" class="span3">
                    <?php
                    foreach( $locations['continents'] as $i => $c )
                    {
                        if( $c['continent'] == $ladder_info['continent'] )
                        {
                            echo "<option value='{$c['continent']}' selected='selected'>{$c['continent']}</option>";
                        }
                        else
                        {
                            echo "<option value='{$c['continent']}'>{$c['continent']}</option>";
                        }
                    }

                    ?>
                    <option value="Global">Global</option>
                    </select>
                <span class="help-block">Lock the ladder to a specific region</span>
            </fieldset>
            
            <?php
            $livedate = $ladder_info['livedate'];
            $registration_date = $ladder_info['registration_date'];
            if ( empty($ladder_info['livedate']) )
                $livedate = strtotime("+7 days");
            if ( empty($ladder_info['registration_date']) )
                $registration_date = strtotime("+1 day");
            ?>
            <fieldset class="span6 pull-left">
                <label>Registration Date</label>
                <input id="registration_date" class="yymmdd" name="registration_date" type="text" value="<?php if( $mode=='edit' ){ echo  date('Y-m-d',$registration_date); } ?>">
                <span class="help-block">Date teams can start registering for matches</span>
            </fieldset>
            
            <fieldset class="span5">
                <label>Live Date</label>
                <input id="livedate" name="livedate" class="yymmdd" type="text" value="<?php if( $mode=='edit' ){ echo  date('Y-m-d',$livedate); } ?>">
                <span class="help-block">Date ladder matches begin</span>
            </fieldset>
        </div>
    </div>
</div>

<div class="box" style="margin-top:12px">
    <h1>General Information</h1>
    <div class="inner_box" style="margin-top:10px">
        <div class="row" style="margin-left:0px;">

             <fieldset class="span5">
                <legend>Ladder Information</legend>
                <label>Active</label>
                <select id="status" name="status" >
                <?php
                    $options = array('No','Yes');
                    foreach($options as $i => $o) 
                    {
                        if( $i == $ladder_info['status'] )
                        {
                            echo "<option value='{$i}' selected='selected'>{$o}</option>";
                        }
                        else
                        {
                            echo "<option value='{$i}'>{$o}</option>";
                        }
                    }
                ?>
                </select>
                <label>Max Teams per Ladder <br /><small>(Ladders will auto-split when 150% of this value is reached)</small></label>
                <input id="maxteams" name="maxteams"  type="text" value="<?php if($mode == "edit") { echo $ladder_info['maxteams']; }else{ echo "50";} ?>">
                
                <label>Map Pool</label>
                <select name="mappool">
                    <option value="0">---Required---</option>
                    <?php
                    foreach( $map_pools as $pool )
                    {
                        if( $pool['id'] == $ladder_info['mappool'])
                            echo "<option value='{$pool['id']}' selected='selected'>{$pool['name']}</option>";
                        else
                            echo "<option value='{$pool['id']}'>{$pool['name']}</option>";
                    }
                    ?>
                </select>
                <label>Match Config</label>
                <select name="config_id">
                    <option value="">---NA---</option>
                    <?php
                    foreach( $match_configs as $c )
                    {
                        if( $c['id'] == $ladder_info['config_id'])
                            echo "<option value='{$c['id']}' selected='selected'>{$c['name']}</option>";
                        else
                            echo "<option value='{$c['id']}'>{$c['name']}</option>";
                    }
                    ?>
                </select>
            </fieldset> 

            <fieldset class="pull-right span6">
                <legend>Match Information</legend>
                <label>Min Players <small>Teams must have this many players to participate.</small></label>
                <input id="teamsize" name="teamsize"  type="text" value="<?php if($mode == "edit") { echo $ladder_info['teamsize']; } ?>">
                <label>Match Length <small>(hours)</small></label>
                <select id="match_length" name="match_length">
                <?php
                $options = array(.25,.5,.75,1,1.5,2,2.5,3,4);
                foreach( $options as $o )
                    if( $o == $ladder_info['match_length'] )
                        echo "<option selected='selected'>{$o}</option>";
                    else
                        echo "<option>{$o}</option>";
                        ?>
                </select>
                <label># of Games/Maps Per Match</label>
                <select id="rounds" name="rounds">
                <?php
                $rounds = array(1,3,5,7,9);
                foreach( $rounds as $r )
                    if( $r == $ladder_info['rounds'] )
                        echo "<option selected='selected'>{$r}</option>";
                    else
                        echo "<option>{$r}</option>";
                        ?>
                </select>
                <span class="help-block">The total number of maps teams play to determine the winner of the match.</span>
                <span class="help-block">Examples:<br>
                Counter-Strike has 15 rounds per map. A match is only 1 map. Therefore the number of rounds is 1.<br>
                Quake Live has 10 rounds per map. A match is 3 maps. Therefore the number of rounds is 3.
                </span>
            </fieldset>  
        </div>
    </div>
</div>
    
    
<div class="box" style="margin-top:12px">
    <h1>Reporting Match Scores</h1>
    <div class="inner_box" style="margin-top:10px">
        <div class="row" style="margin-left:0px;">
             <fieldset class="span5">
                <legend>Scoring Method</legend>
                <select id="scoring_method" name="scoring_method" >
                <?php
                    $options = array('Total Score','Round Scores');
                    foreach($options as $i => $o) 
                    {
                        if( $i == $ladder_info['scoring_method'] )
                        {
                            echo "<option value='{$i}' selected='selected'>{$o}</option>";
                        }
                        else
                        {
                            echo "<option value='{$i}'>{$o}</option>";
                        }
                    }
                ?>
                </select>
                <span class="help-block">Round score requires the captains to submit scores for each round played, per team.<br>Example:<br>6-5<br>6-4<br>6-5<br><br>Total score requires 1 total score for each team.<br>Example:<br>18-14</span>
            </fieldset> 

            <fieldset class="pull-right span6">
                <legend>Round Details</legend>
                <select id="round_details" name="round_details" >
                <?php
                    $options = array('Simple','Detailed');
                    foreach($options as $i => $o) 
                    {
                        if( $i == $ladder_info['round_details'] )
                        {
                            echo "<option value='{$i}' selected='selected'>{$o}</option>";
                        }
                        else
                        {
                            echo "<option value='{$i}'>{$o}</option>";
                        }
                    }
                ?>
                </select>
                <span class="help-block">Simple round scoring requires the captain to select the winner for each round from a check box. <br>Detailed scoring requires exact score per team for each round.<br> Example: 6-5</span>
            </fieldset>
        </div>
        <span class="help-block">*For quick & simple score submission, use  Total Score & Simple. For detailed scoring (best for historic archives) use Round Scores & Detailed.</span>
    </div>
</div>    
 
<div class="box" style="margin-top:12px">
    <h1>Ladder Rules</h1>
    <div class="inner_box" style="margin-top:10px">
        <textarea id="rules" name="rules" style="width:550px; height:150px;"><?php if($mode == "edit") { echo $ladder_info['rules']; } ?></textarea>  
    </div>
</div>
<div class="box" style="margin-top:12px">
    <h1>Ladder Details</h1>
    <div class="inner_box" style="margin-top:10px">
        <textarea id="details" name="details" style="width:550px; height:150px;"><?php if($mode == "edit") { echo $ladder_info['details']; } ?></textarea>  
    </div>
</div>
    

  

<script type="text/javascript">
(function($) {   
    
    <?php
        if($mode == "edit") {
            echo "$('#game').val('".$ladder_info['game']."');";
            echo "$('#mappool').val('".$ladder_info['mappool']."');";
        }
        else
        {
    ?>
    $('#game').change(function() {
        var game = $(this).val();
        $.ajax({ url: "http://localhost/api/game/"+game }).done(function(result) {
                   $('#details').setCode(result.data.about);
                   $('#rules').setCode(result.data.rules);
        });
    }).change();
    <?php
        }
        ?>
})(jQuery);
</script>    
   
</form>