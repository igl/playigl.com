<form id="articleForm" method="post" enctype='multipart/form-data'>
  <div class='box'>
    <h1>Article details</h1>
    <div stye='margin-top:12px' class='inner_box'>
      <div class="row">
        <div class="span11">
          <h3>Header image <a><i class="icon-question-sign tooltip-on" title="Displayed on the front page slider & top of an article."></i></a></h3>
          <img id="banner" style="margin:2px;" data-src="/js/holder.js/640x240" class='img-polaroid tooltip-on' title='640 x 240' <?php if ($mode == 'edit' && isset($article_data['banner'])) echo "src='/img{$article_data['banner']}'"; ?>><input style='visibility:hidden;' type='file' name='banner'><br />
        </div>
      </div>
      <div class="clear">
        <h3>Title <a><i class="icon-question-sign tooltip-on" title="Title of the article. No HTML."></i></a></h3>
        <input class='span11' type='textfield' name='title' value='<?php if ($mode == 'edit') {
  echo $article_data['title'];
} ?>'>
      </div>
      <div class="clear">
        <h3>Preview <a><i class="icon-question-sign tooltip-on" title="Preview of the article. No HTML. 120 chars max."></i></a></h3>
        <input class='span11' type='textfield' name='description' value='<?php if ($mode == 'edit') {
  echo $article_data['description'];
} ?>' maxlength="120">
      </div>
      <div class="clear">
        <h3>Keywords <a><i class="icon-question-sign tooltip-on" title="Important keywords to help find the article. Comma delimited."></i></a></h3>
        <input class='span11' type='textfield' name='meta' value='<?php if ($mode == 'edit') {
  echo $article_data['meta'];
} ?>'>
      </div>
      <div class="clear">
        <h3>Content <a><i class="icon-question-sign tooltip-on" title="Body of the article. Accepts HTML mark-up."></i></a></h3>
        <textarea id="details" name="content" style="width:100%; min-height:450px;"><?php if ($mode == "edit") {
  echo $article_data['content'];
} ?></textarea>
      </div>
      <div class="row">
        <div class='span6 pull-left'>
          <h3>Author <a><i class="icon-question-sign tooltip-on" title="User cited for article creation."></i></a></h3>
          <input type="hidden" name="author_id" value="<?php if ($mode == 'edit') {
  echo $article_data['author_id'];
}
else echo $_SESSION['playerid']; ?>">
          <input class='span3' type='textfield' value='<?php if ($mode == 'edit') {
  echo $article_data['author_name'];
}
else echo $_SESSION['username']; ?>' disabled>
        </div>
        <div class="span5 pull-left">
          <h3>Date <a><i class="icon-question-sign tooltip-on" title="Date the article was published."></i></a></h3>
          <input type="hidden" name="rts" value="<?php if ($mode == 'edit') {
  echo $article_data['rts'];
}
else echo date('Y-m-d', time()); ?>">
          <input id="date" class='span3 yymmdd' type='textfield' name='rts' value='<?php if ($mode == 'edit') {
  echo date('Y-m-d', strtotime($article_data['rts']));
}
else echo date('Y-m-d', time()); ?>' disabled>
        </div>
      </div>
    </div>
  </div>

  <input id='id' type='hidden' name='id' value='<?php if ($mode == 'edit') {
  echo $_REQUEST['id'];
} ?>' >
</form>