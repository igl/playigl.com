
<form id="adminForm" action="" method="POST">

<div class="box">
        <h1>Tournament</h1>
        <div class="inner_box" style="margin-top:10px">
            <div class="row" style="margin-left:0px;">
                <fieldset class="pull-left span5">
                    <label>Event Title</label>
                    <input id="name" name="name" type="text" value="<?php if($mode == "edit") { echo $tour_info['name']; } ?>">
                    <span class="help-block">Example: DOTA 2 - Select Divison Playoffs</span>
                    <label>Registration Style</label>
                    <select id="registrationstyle" name="registrationstyle" >
                        <option value="0">Division Limited</option>
                        <option value="1">Open</option>
                        <option value="2">Custom</option>
                    </select>
                    <input type="hidden" id="action" name="action" value="create"/>
                    <input type="hidden" id="id" name="id" value="<?php if($mode == "edit") { echo $tour_info['id']; } ?>"/>
                </fieldset>
                <fieldset class="span5">
                    <label>Start Date / Time</label>
                    <input class="datetimebox" type="text" name="startdatetime" id="startdatetime" value="<?php if($mode == "edit") { echo $tour_info['startdatetime']; } ?>">
                    <label>End Date / Time</label>
                    <input class="datetimebox" type="text" name="enddatetime" id="enddatetime" value="<?php if($mode == "edit") { echo $tour_info['enddatetime']; } ?>">
                    <label>Published</label>
                    <select id="is_published" name="is_published" >
                        <option value="0">No</option>
                        <option value="1">Yes</option>
                    </select>
                    <div id="starttimeAlert" style="display:none;" class="alert alert-error"></div>
                </fieldset>
            </div>
        </div>
</div>
    
<div class="box" style="margin-top:12px">
        <h1>General Information</h1>
        <div class="inner_box" style="margin-top:10px">
            <div class="row" style="margin-left:0px;">

                 <fieldset class="span5">
                    <legend>General Information</legend>
                    <label># of Participants</label>
                    <input id="numberofparticipants" name="numberofparticipants"  type="text" value="<?php if($mode == "edit") { echo $tour_info['numberofparticipants']; } ?>">
                    <label># of Waitlisted Participants</label>
                    <input id="numberofwaitlist" name="numberofwaitlist"  type="text" value="<?php if($mode == "edit") { echo $tour_info['numberofwaitlist']; } ?>">
                    <label>Game</label>
                    <select id="game" name="game" ></select>
                    <legend>Registration Date & Time</legend>
                    <div id="registrationAlert" style="display:none;" class="alert alert-error"></div>
                    <label>Registration Start Date / Time</label>
                    <input class="datetimebox" type="text" name="registrationstartdatetime" id="registrationstartdatetime" value="<?php if($mode == "edit") { echo $tour_info['registrationstartdatetime']; } ?>">
                    <label>Registration End Date / Time</label>
                    <input class="datetimebox" type="text" name="registrationenddatetime" id="registrationenddatetime" value="<?php if($mode == "edit") { echo $tour_info['registrationenddatetime']; } ?>">
                    <label>Scoring Method</label>
                    <select id="scoring_method" name="scoring_method">
                        <option value="0">Total Score</option>
                        <option value ="1">Round Scores</option>
                    </select>
                 </fieldset>  
               
                               
                <fieldset class="pull-right span5">
                    <legend>Bracket Information</legend>
                    <label>Bracket Type</label>
                    <select id="brackettype" name="brackettype">
                        <option>Single Elimination</option>
                        <option>Double Elimination</option>
                    </select>
                    <label>Bracket Size</label>
                    <select id="bracketsize" name="bracketsize">
                        <option>4</option>
                        <option>8</option>
                        <option>16</option>
                        <option>32</option>
                        <option>64</option>
                        <option>128</option>
                        <option>256</option>
                        <option>512</option>
                    </select>
                    <label>Map Pool</label>
                    <select id='mappool' name='mappool'>
                        <option value=''>--------</option>
                        <?php
                        $maplist = $maps->getMapPools($tour_info['game']);
                        if (!empty($maplist)) {
                        foreach($maplist as $ml) {
                            echo "<option value='{$ml['id']}'>{$ml['name']}</option>";
                        }
                        }
                        ?>
                    </select>
                    <div id="mappool-error" class='alert' style="display:none;"><i class="icon-warning-sign"></i> No Map Pool Selected.</div>
                    
                    <legend>Check In Date & Time</legend>
                    <div id="checkinAlert" style="display:none;" class="alert alert-error"></div>
                    <label>Check In Start Date / Time</label>
                    <input class="datetimebox" type="text" name="checkinstartdatetime" id="checkinstartdatetime" value="<?php if($mode == "edit") { echo $tour_info['checkinstartdatetime']; } ?>">
                    <label>Check In End Date / Time</label>
                    <input class="datetimebox" type="text" name="checkinenddatetime" id="checkinenddatetime" value="<?php if($mode == "edit") { echo $tour_info['checkinenddatetime']; } ?>">
                </fieldset>
                
            </div>
        </div>
</div>
    
<div class="box" style="margin-top:12px">
        <h1>Event Details</h1>
        <div class="inner_box" style="margin-top:10px">
            <div class="row" style="margin-left:0px;">
                <fieldset>
                    <textarea id="form_details" name="details" style="width:700px; height:150px;"><?php if($mode == "edit") { echo $tour_info['details']; } ?></textarea>  
                </fieldset>
            </div>
        </div>
</div>
    
<div class="box" style="margin-top:12px">
        <h1>Event Rules</h1>
        <div class="inner_box" style="margin-top:10px">
            <div class="row" style="margin-left:0px;">
                <fieldset>
                    <textarea id="form_rules" name="rules" style="width:700px; height:150px;"><?php if($mode == "edit") { echo $tour_info['rules']; } ?></textarea>  
                </fieldset>
            </div>
        </div>
</div>
    
<div class="box" style="margin-top:12px">
        <h1>Admins and Casters</h1>
        <div class="inner_box" style="margin-top:10px">
            <div class="row" style="margin-left:0px;">
                <fieldset class="span5">
                    <legend>Event Admins</legend>
                    <label>Admin 1</label>
                    <select id="admin1" name="admin1">
                        <option value=''>--------</option>
                        <?php
                            $query = "SELECT playerid, username FROM player WHERE usergroupid = 6";
                            $db->ExecuteSQL($query);
                            if ($db->iRecords()) {
                                $admins = $db->ArrayResults();
                                foreach($admins as $a) {
                                    echo "<option value='".$a['playerid']."'>".$a['username']."</option>";
                                }
                            }
                            echo $db->sLastError;
                        ?>
                    </select>
                    <label>Admin 2</label>
                    <select id="admin2" name="admin2"></select>
                    <label>Admin 3</label>
                    <select id="admin3" name="admin3"></select>
                </fieldset>
                <fieldset class="span5 pull-right">
                    <legend>Event Casters</legend>
                    <label>Caster 1</label>
                    <select id="caster1" name="caster1">
                        <option value=''>--------</option>
                        <?php
                            $query = "SELECT playerid, username FROM player WHERE usergroupid = 6";
                            $db->ExecuteSQL($query);
                            if ($db->iRecords()) {
                                $casters = $db->ArrayResults();
                                foreach($casters as $c) {
                                    echo "<option value='".$c['playerid']."'>".$c['username']."</option>";
                                }
                            }
                            echo $db->sLastError;
                        ?>
                    </select>
                    <label>Caster 2</label>
                    <select id="caster2" name="caster2"></select>
                    <label>Caster 3</label>
                    <select id="caster3" name="caster3"></select>
                </fieldset>     
            </div>
        </div>
</div> 
  
<div class="box" style="margin-top:12px">
        <h1>Prize Information</h1>
        <div class="inner_box" style="margin-top:10px">
            <div class="row" style="margin-left:0px;">
                <fieldset>
                    <legend>Prize Information</legend>
                    <label>Prize Distribution</label>
                    <select id="prizedistribution" name="prizedistribution"></select>
                    <label>Total Cash Prizes</label>
                    <input name="totalcashprize" type="text" value="<?php if($mode == "edit") { echo $tour_info['totalcashprize']; } ?>">
                    <label>Total Product Prize Value</label>
                    <input name="totalproductprizevalue" type="text" value="<?php if($mode == "edit") { echo $tour_info['totalproductprizevalue']; } ?>">
                </fieldset>
            </div>
        </div>
</div>    
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">
window.onload = function() {
    
   PlayIGL.Forms.createGameDropdown($('#game') <?php if($mode == "edit") { echo ", '".$tour_info['game']."'"; } ?>);
   
    $('#adminForm').submit(function(e) {
        $('#startdatetime').val(PlayIGL.getLocalTimeInUTC($('#startdatetime').datepicker( "getDate" )));
        $('#enddatetime').val(PlayIGL.getLocalTimeInUTC($('#enddatetime').datepicker( "getDate" )));
        $('#checkinstartdatetime').val(PlayIGL.getLocalTimeInUTC($('#checkinstartdatetime').datepicker( "getDate" )));
        $('#checkinenddatetime').val(PlayIGL.getLocalTimeInUTC($('#checkinenddatetime').datepicker( "getDate" )));
        $('#registrationstartdatetime').val(PlayIGL.getLocalTimeInUTC($('#registrationstartdatetime').datepicker( "getDate" )));
        $('#registrationenddatetime').val(PlayIGL.getLocalTimeInUTC($('#registrationenddatetime').datepicker( "getDate" )));
    });
    
    $('#mappool').change(function() {
       if ($('#mappool').val() == '') {
           $('#mappool-error').show();
       } else {
           $('#mappool-error').hide();
       }
    });
    
    $('#startdatetime').val(PlayIGL.formatTimestamp($('#startdatetime').val(), true));
    $('#enddatetime').val(PlayIGL.formatTimestamp($('#enddatetime').val(), true));
    $('#checkinstartdatetime').val(PlayIGL.formatTimestamp($('#checkinstartdatetime').val(), true));
    $('#checkinenddatetime').val(PlayIGL.formatTimestamp($('#checkinenddatetime').val(), true));
    $('#registrationstartdatetime').val(PlayIGL.formatTimestamp($('#registrationstartdatetime').val(), true));
    $('#registrationenddatetime').val(PlayIGL.formatTimestamp($('#registrationenddatetime').val(), true));
    
    $('#form_rules').redactor();
    $('#form_details').redactor();
    
    $('#admin2').html($('#admin1').html());
    $('#admin3').html($('#admin1').html());
    $('#caster2').html($('#caster1').html());
    $('#caster3').html($('#caster1').html());
    
    $('#startdatetime').change(function() {
        if (PlayIGL.getLocalTimeUnix($('#startdatetime').val()) > PlayIGL.getLocalTimeUnix($('#enddatetime').val())) {
            $('#starttimeAlert').text('End Time is before Start Time').show();
        } else {
           $('#starttimeAlert').hide(); 
        }
    });
    $('#enddatetime').change(function() {
        if (PlayIGL.getLocalTimeUnix($('#startdatetime').val()) > PlayIGL.getLocalTimeUnix($('#enddatetime').val())) {
            $('#starttimeAlert').text('End Time is before Start Time').show();
        } else {
           $('#starttimeAlert').hide(); 
        }
    });
    $('#checkinstartdatetime').change(function() {
        if (PlayIGL.getLocalTimeUnix($('#checkinstartdatetime').val()) > PlayIGL.getLocalTimeUnix($('#checkinenddatetime').val())) {
            $('#checkinAlert').text('End Time is before Start Time').show();
        } else {
           $('#checkinAlert').hide(); 
        }
    });
    $('#checkinenddatetime').change(function() {
        if (PlayIGL.getLocalTimeUnix($('#checkinstartdatetime').val()) > PlayIGL.getLocalTimeUnix($('#checkinenddatetime').val())) {
            $('#checkinAlert').text('End Time is before Start Time').show();
        } else {
           $('#checkinAlert').hide(); 
        }
    });
    $('#registrationstartdatetime').change(function() {
        if (PlayIGL.getLocalTimeUnix($('#registrationstartdatetime').val()) > PlayIGL.getLocalTimeUnix($('#registrationenddatetime').val())) {
            $('#registrationAlert').text('End Time is before Start Time').show();
        } else {
           $('#registrationAlert').hide(); 
        }
    });
    $('#registrationenddatetime').change(function() {
        if (PlayIGL.getLocalTimeUnix($('#registrationstartdatetime').val()) > PlayIGL.getLocalTimeUnix($('#registrationenddatetime').val())) {
            $('#registrationAlert').text('End Time is before Start Time').show();
        } else {
           $('#registrationAlert').hide(); 
        }
    });
    
    <?php
    if (isset($mode)) {
        if($mode == "edit") {
            echo "$('#registrationstyle').val('".$tour_info['registrationstyle']."');";
            echo "$('#is_published').val('".$tour_info['is_published']."');";
            echo "$('#brackettype').val('".$tour_info['brackettype']."');";
            echo "$('#game').val('".$tour_info['game']."');";
            echo "$('#bracketsize').val('".$tour_info['bracketsize']."');";
            echo "$('#mappool').val('".$tour_info['mappool']."');";
            echo "$('#admin1').val('".$tour_info['admin1']."');";
            echo "$('#admin2').val('".$tour_info['admin2']."');";
            echo "$('#admin3').val('".$tour_info['admin3']."');";
            echo "$('#caster1').val('".$tour_info['caster1']."');";
            echo "$('#caster2').val('".$tour_info['caster2']."');";
            echo "$('#caster3').val('".$tour_info['caster3']."');";
            echo "$('#prizedistribution').val('".$tour_info['prizedistribution']."');";
             echo "$('#scoring_method').val('".$tour_info['scoring_method']."');";
        }
    }
    ?>
};
</script>    
   
</form>

