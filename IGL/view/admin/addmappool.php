<div class="span12">
<?php
include('./view/admin/subnavigation.php');

if( LEAGUEADMIN )
{
     $maps = new maps();
     $maplist = $maps->getAll($_REQUEST['id']);
    ?>
   <button type="button" style="margin-top:-55px; margin-right:5px;" onclick="savePool()" class="btn btn-action pull-right">Save</button>

   <div style="display:none;" class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><h4>Done!</h4>Map pool saved.</div>
   <div style="display:none;" class="alert alert-block"><button type="button" class="close" data-dismiss="alert">&times;</button><h4>Error!</h4>Could not save map pool</div>
   
    <div class="box">
        <h1>New Map Pool</h1>
        <div class="inner_box" style="margin-top:10px">
            <div class="row" style="margin-left:0px;">
                <fieldset>
                    <label>Map Pool Name</label>
                    <input id="name" name="name" type="text" style="width:550px;" value="<?php if($mode == "edit") { echo $tour_info['name']; } ?>">
                    <input type="hidden" name="game" id="game" value="<?php echo $_REQUEST['id']; ?>" />
                </fieldset>
            </div>
        </div>
    </div>
    
     <div class="box">
        <h1>Maps</h1>
        <div class="inner_box" style="margin-top:10px">
            <div class="row" style="margin-left:0px;">
                <fieldset>
                    <label>Game Maps</label>
                    <select id="map_select">
                        <?php
                        if (!empty($maplist)) {
                        foreach($maplist as $ml) {
                            echo "<option value='{$ml['id']}'>{$ml['map']}</option>";
                        }
                        }
                        ?>
                    </select>
                    <div id="add_map" class="btn btn-success">Add Map</div>
                    
                    <table class="table table-condensed table-striped table-hover gameList">
                    <thead>
                        <tr>
                            <th>Map ID</th>
                            <th>League</th>  
                            <th>Season</th> 
                            <th>Week</th> 
                            <th>Map Name</th> 
                            <th>Action</th> 
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    </table>
                
                </fieldset>
            </div>
        </div>
    </div>
    
    <script type="text/javascript">
        window.onload = function() {
        var map_array = new Array();
         <?php
           if (!empty($maplist)) {
              foreach($maplist as $ml) {
                   echo "map_array[{$ml['id']}] = new Array('{$ml['id']}', '{$ml['league']}', '{$ml['season']}', '{$ml['week']}', '{$ml['map']}');";
              }
          }
        ?>
        $('#add_map').click(function() {
           var select_id = $('#map_select').val();
            $('tbody').append("<tr><td>"+map_array[select_id][0]+"</td><td>"+map_array[select_id][1]+"</td><td>"+map_array[select_id][2]+"</td><td>"+map_array[select_id][3]+"</td><td>"+map_array[select_id][4]+"</td><td><div onclick='$(this).parent().parent().remove()' class='btn btn-danger tooltip-on' title='Remove Pool'><i class='icon-remove'></i></div></td></tr>");
        });
        };
        
        function savePool() {
            var game = "<?php echo $_REQUEST['id']; ?>";
            var name = $('#name').val();
            var map_pool = "[";
            $('tbody tr').each(function(i, v) {
                map_pool += $(v).children(":first").text()+",";
            });
            map_pool += "]";
            $.ajax({ url: "/ajax/admin/mappools.php?action=savePool&game="+game+"&name="+name+"&mappool="+map_pool }).done(function(response) {
                if (response.error == null) {
                    document.location = "/admin/mappools/"+game;
                } else {
                    $('.alert-block').fadeIn();
                }
            });
        }
    </script>
    
    <?php
}
else
{
    echo "You do not have access to this page.";
}
?>
</div>