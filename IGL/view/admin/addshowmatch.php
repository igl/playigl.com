<?php
include('./view/admin/subnavigation.php');

if( LEAGUEADMIN )
{
require_once('./model/showmatch.class.php');
     
     // Classes
     $game = new game();
     $league = new league();
     $showmatch = new showmatch();
     
     //Deletion Check
     $deleted = false;
     
     $mode = "create";
     
    // Option Buttons
    echo '<div onclick="$(\'#action\').val(\'create\'); $(\'#adminForm\').submit();" style="margin-top:-56px; margin-right:5px;" class="btn btn-action pull-right">Create</div>';

     
     // Actions
     if ($_POST['action'] == "create") {
         if ($showmatch->create()) {
             echo '<div class="alert alert-success">Showmatch created.</div>';
         } else {
             echo '<div class="alert alert-error"><strong>Error.</strong> The showmatch could not be created.</div>';
         }
     }
    
     include('formshowmatch.php');

}
else
{
    echo "You do not have access to this page.";
}
?>