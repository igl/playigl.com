<?php
if( SUPERUSER )
{
    if (isset($_REQUEST['id'])) {
    
    $id = (int)$_REQUEST['id'];
     
     // Classes
     $game = new game();
     
     // Actions
         if ($game->delete($id)) {
             echo '<div class="alert span12">Game deleted.</div>';
         } else {
             echo '<div class="alert span12 alert-error"><strong>Error.</strong> The game could not be deleted.</div>';
         }
     
    } else {
        echo '<div class="alert alert-error"><strong>Error.</strong> No game id provided.</div>';
    }

    $games = $game->all();
    include('allgames.php');
}
else
{
    echo "You do not have access to this page.";
}
?>