<?php
if( SUPERUSER )
{
    if (isset($_REQUEST['id'])) {
    
    $id = (int)$_REQUEST['id'];
     
     // Classes
     $server_config = new serverConfig();
     
     // Actions
         if ($server_config->delete($id)) {
             echo '<div class="alert span12">Config deleted.</div>';
         } else {
             echo '<div class="alert span12 alert-error"><strong>Error.</strong> The config could not be deleted.</div>';
         }
     
    } else {
        echo '<div class="alert alert-error"><strong>Error.</strong> No config id provided.</div>';
    }

    $server_configs = $server_config->all();
    include('allconfigs.php');
}
else
{
    echo "You do not have access to this page.";
}
?>