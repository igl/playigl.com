<div class="span12">
<?php
include('./view/admin/subnavigation.php');
if( SUPERUSER )
{    
     // Classes
     $ladder = new ladder();
     $ladders = $ladder->all();
     
     echo '<div onclick="document.location = \'/admin/addladder\'" style="margin-top:-56px; margin-right:5px;" class="btn btn-action pull-right">Add new</div>';
     ?>
<div class="box" style="margin-top:12px">
        <h1>All ladders</h1>
        <div class="inner_box" style="margin-top:10px">
            <div class="row" style="margin-left:0px;">
                <table class="table table-condensed table-striped table-hover ladderList">
                    <thead>
                        <tr>
                            <th style="width:10%">Ladder Id</th>
                            <th>Game Name</th>
                            <th style="width:20%">Continent</th>
                            <th>Ladder Name</th>
                            <th>Game Type</th>
                            <th style="text-align: right">Manage Ladder</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach($ladders as $l) {
                            echo "<tr><td>{$l['id']}</td><td>{$l['game_name']}</td><td>{$l['continent']}</td><td>{$l['name']}</td><td>{$l['format']}</td><td style='text-align: right'><a href='/admin/editladder/{$l['id']}' class='btn tooltip-on' title='Manage Ladder'><i class='icon-pencil'></i></a></td></tr>";
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
<div class="modal hide" id="Modal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hide="true">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hide="true"><i class='icon-remove'></i></button>
<h3 id="ModalLabel">LOADING</h3>
</div>
<div class="modal-body" id="ModalBody">
<div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div>
</div>
<div class="modal-footer">
<button id="ModalButtonClose" class="btn" data-dismiss="modal" aria-hide="true">CLOSE</button>
<button id="ModalButton" class="btn btn-primary">SUBMIT</button>
</div>
</div> 
</div>
<?php
}
else
{
    echo "You do not have access to this page.";
}
?>
</div>