<?php
include('./view/admin/subnavigation.php');

if( LEAGUEADMIN )
{

    // Includes
     require_once('./model/tournament.class.php');
     
     // Classes
     $game = new game();
     $tournament = new tournament();
     $tournaments = $tournament->all();
     
     echo '<div onclick="document.location = \'../admin/?page=createtournament\'" style="margin-top:-40px;" class="btn btn-action pull-right">Create New</div>';

?>
<style>

    .tournamentList tbody tr {
        cursor:pointer;
    }
</style>
<div class="box" style="margin-top:12px">
        <h1>Manage Tournament Matches</h1>
        <div class="inner_box" style="margin-top:10px">
            <div class="row" style="margin-left:0px;">
                <label>Tournament</label>
                <select id="tournament_id" name="tournament_id">
                        <?php
                        foreach($tournaments as $t) {
                            if ($t['tournament_status'] != 0) {
                                echo "<option value='".$t['tournament_id']."'>".$t['tournament_name']."</option>";
                            }
                        }
                        ?>
                </select>
                <div onclick="Tournament.admin.generateMatches();" class="btn pull-right">Generate Next Round</div>
            </div>
        </div>
</div>

<div id="tournament_activity" style="display:none; text-align:center;"><img src="/img/tournament-loader.gif"/></div>              
<div id="tournament_content" style="margin-top:12px"></div>

<script type="text/javascript" src="../js/tournament.js"></script>
<script type="text/javascript" src="../js/tournament.js"></script>
<script type="text/javascript">
window.onload = function() {  
    Tournament.admin.initMatches($('#tournament_id').val());
    $('#tournament_id').change(function() {
        Tournament.tournament_id = $(this).val();
        Tournament.admin.initMatches($(this).val());
    });
};
</script>


<?php
}
else
{
    echo "You do not have access to this page.";
}
?>