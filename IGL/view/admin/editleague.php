<div class="span12">
<?php
include('./view/admin/subnavigation.php');
if( SUPERUSER==true )
{
    $league   = new league();
    $id = (int)$_REQUEST['id'];

    echo '<div onclick="$(\'#leagueForm\').submit();" style="margin-top:-56px; margin-right:5px;" class="btn btn-action pull-right">Save</div>';
    if($_POST)
    {
        if($league->save())
        {
            ?>
            <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">x</button>League saved successfully!</div>
            <?php
        }
        else
        {
            ?>
            <div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">x</button>Error saving league.</div>
            <?php
        }
    }
    
    $game = new game();
    $games = $game->all();    
    $league_data = $league->get($id);
    $mode = 'edit';
    include('formleague.php');
}
else
{
    echo "You do not have access to this page.";
}
?>
</div>