<form id="matchserverForm" method='post' enctype='multipart/form-data'>
    <div class='box'>
    <h1>Match Server Details</h1>

        <div stye='margin-top:12px' class='inner_box'>
            <div class="row">
                <div class="span11 pull-left"><h3>Servers</h3>
                    <table id="serverList" class="table table-striped table-condensed">
                        <tr>
                            <th><h5>Competition <a><i class="icon-question-sign tooltip-on" title="Servers are assigned per competition type. This ensures the server is not unexpectedly used elsewhere."></i></a></h5></th><th><h5>Connection Details <a><i class="icon-question-sign tooltip-on" title="IP:Port of a match servers."></i></a></h5></th><th><h5>Rcon Password <a><i class="icon-question-sign tooltip-on" title="Secure password used to manage servers. IGL scripts use this password to set maps & various server data for matches."></i></a></h5></th><th colspan="2"><h5>Location <a><i class="icon-question-sign tooltip-on" title="Select the continent. Choose the region relative to the centre of the continent."></i></a></h5></th><th><h5>Delete <a><i class="icon-question-sign tooltip-on" title="Servers must be decomissioned 7 days in advance in order to prevent matches not occuring."></i></a></h5></th>
                        </tr>
                    <?php
                    $options = array('league','ladder','ladder challenge','tournament');
                    foreach( $server_details['servers'] as $i => $s)
                    {
                        ?>
                        <tr class="control-group">
                            <td><div class="controls"><select class="span2" name="competition_type[<?php echo $i; ?>]">
                                    <?php foreach($options as $o)
                                    {
                                        if( $o == $s['competition_type'] )
                                            echo "<option value='{$o}' selected='selected'>{$o}</option>";
                                        else
                                            echo "<option value='{$o}'>{$o}</option>";
                                    }
                                        
                                        ?>
                                    
                                </select></div></td>
                            <td><div class="controls"><input class='span2' type='textfield' name='ip[<?php echo $i; ?>]' value='<?php echo $s['ip']; ?>'>:<input class='span1' type='textfield' name='port[<?php echo $i; ?>]' value='<?php echo $s['port']; ?>'></div></td>
                            <td><div class="controls"><input class='span2' type='textfield' name='rcon[<?php echo $i; ?>]' value='<?php echo $s['rcon']; ?>'></div></td>
                            <td><div class="controls">
                                    <select class="span1" name="continent[]">
                                        <?php foreach($server_locations['continents'] as $l)
                                        {
                                            if( $l['continent'] == $s['continent'] )
                                            echo "<option value='{$l['continent']}' selected='selected'>{$l['continent']}</option>";
                                        else
                                            echo "<option value='{$l['continent']}'>{$l['continent']}</option>";
                                        }
                                        ?>
                                    </select>
                            </td>
                            <td><div class="controls">
                                    <select class="span2" name="region[]">
                                        <?php
                                        foreach($server_locations['regions'] as $l)
                                        {
                                            if( $l['region'] == $s['region'] )
                                            echo "<option value='{$l['region']}' selected='selected'>{$l['region']}</option>";
                                        else
                                            echo "<option value='{$l['region']}'>{$l['region']}</option>";
                                        }
                                        ?>
                                    </select>
                            </td>
                            <td><div class="controls"><input type='checkbox' value='1' name='delete[]'><input type='hidden' name='server_id[]' value='<?php echo $s['id']; ?>'></div></td>
                        </tr>
                        <?php
                    }
                    ?>
                    </table>
                    <button id="addServer" type="button" class="btn btn-action pull-right">Add Server</button> 
            </div>
        </div>
    </div>
    </div>
    </form>