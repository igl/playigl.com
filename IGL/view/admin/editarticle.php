<div class="span12">
  <?php
  include('./view/admin/subnavigation.php');
  if (PRESS) {
    echo '<div onclick="$(\'#articleForm\').submit();" style="margin-top:-56px; margin-right:5px;" class="btn btn-action pull-right">Save</div>';
    $article = new article();

    if ($_POST) {
      if ($article->save()) {

        ?>
        <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">x</button>News updated successfully!</div>
        <?php
      } else {

        ?>
        <div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">x</button>Error updating article.</div>
        <?php
      }
    }

    if (isset($_REQUEST['id']) && is_numeric($_REQUEST['id'])) {
      $mode = 'edit';
      $article_data = $article->get($_REQUEST['id']);
      include('formarticle.php');
    } else {

      ?>
      <div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">x</button>Invalid article specified.</div>
      <?php
    }
  } else {
    echo "You do not have access to this page.";
  }

  ?>
</div>