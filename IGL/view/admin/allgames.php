<div class="span12">
<?php
include('./view/admin/subnavigation.php');
if( SUPERUSER )
{    
     // Classes
     $game = new game();
     $games = $game->all();
     echo '<div onclick="document.location = \'/admin/addgame\'" style="margin-top:-55px; margin-right:5px" class="btn btn-action pull-right">Add New</div>';
     ?>
<div class="box" style="margin-top:12px">
        <h1>All Games</h1>
        <div class="inner_box" style="margin-top:10px">
            <div class="row" style="margin-left:0px;">
                <table class="table table-condensed table-striped table-hover gameList">
                    <thead>
                        <tr>
                            <th width="10%">Game Id</th>
                            <th width="65%">Game Name</th>  
                            <th>Manage</th>  
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach($games as $g) {
                                echo "<tr><td>{$g['id']}</td><td>{$g['name']}</td><td style='text-align:right'>";
                                if($g['hostedservers']) {
                                    echo "<a href='/admin/matchservers/{$g['id']}' class='btn tooltip-on' title='Match Servers'><i class='icon-cloud'></i></a> ";
                                }
                                echo "<a href='/admin/mappools/{$g['id']}' class='btn tooltip-on' title='Map Pools'><i class='icon-globe'></i></a> <a href='/admin/editgame/{$g['id']}' class='btn tooltip-on' title='Edit Game'><i class='icon-pencil'></i></a> <a data-href='/admin/deletegame/{$g['id']}' rel='remove' class='btn btn-danger tooltip-on' title='Remove Game'><i class='icon-remove'></i></a></td></tr>";
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
<div class="modal hide" id="Modal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hide="true">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hide="true"><i class='icon-remove'></i></button>
<h3 id="ModalLabel">LOADING</h3>
</div>
<div class="modal-body" id="ModalBody">
<div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div>
</div>
<div class="modal-footer">
<button id="ModalButtonClose" class="btn" data-dismiss="modal" aria-hide="true">CLOSE</button>
<button id="ModalButton" class="btn btn-primary">SUBMIT</button>
</div>
</div>
</div>
<?php
}
else
{
    echo "You do not have access to this page.";
}
?>
</div>