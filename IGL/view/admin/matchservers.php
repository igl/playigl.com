<div class="span12">
<?php
include('./view/admin/subnavigation.php');
if( SUPERUSER==true )
{
    echo '<div onclick="$(\'#matchserverForm\').submit();" style="margin-top:-55px; margin-right:5px" class="btn btn-action pull-right">Save</div>';
    $match_servers   = new matchservers();

    if($_POST)
    {
        $save = $match_servers->save();
        if($save['success'])
        {
            ?>
            <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">x</button>Servers updated successfully!</div>
            <?php
        }
        else
        {
            ?>
            <div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">x</button><?php echo $save['error']; ?></div>
            <?php
        }
    }
    $id = (int)$_REQUEST['id'];
    $server_details = $match_servers->get($id);
    $server_locations = $match_servers->locations();
    
    
    $mode = 'edit';
    if($server_details['success'])
    include('formmatchservers.php');
    else
       echo "<div class='alert alert-error'><button type='button' class='close' data-dismiss='alert'>x</button>{$server_details['error']}</div>";
}
else
{
    echo "You do not have access to this page.";
}
?>
</div>