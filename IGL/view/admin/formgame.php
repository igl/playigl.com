<form id="gameForm" method='post' enctype='multipart/form-data'>
    <div class='box'>
    <h1>Global Settings</h1>

        <div style='margin-top:12px' class='inner_box'>
            <div class="row">
                <div class="span6 pull-left"><h3>Name<a><i class="icon-question-sign tooltip-on" title="Title of the game."></i></a></h3><input class='span' type='textfield' name='name' value='<?php if( $mode =='edit') echo $game_data['name']; ?>'></div>
                <div class="span5 pull-left"><h3>Status<a><i class="icon-question-sign tooltip-on" title="Games set in inactive are hidden from public view."></i></a></h3>
                    <select class='loginfield' name='status'>
                <?php
                $options = array('Inactive','Active');
                foreach( $options as $i => $o)
                {
                    if($game_data['status'] == $i)
                        echo " <option value='{$i}' selected='selected'>{$o}</option>";
                    else
                        echo " <option value='{$i}'>{$o}</option>";       
                }
                ?>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="span5 pull-left"><h3>Require Steam Auth<a><i class="icon-question-sign tooltip-on" title="If steam auth is required, users must authenticate with steam before playing."></i></a></h3>
                    <select class='loginfield' name='steamauth'>
                <?php
                $options = array('No','Yes');
                foreach( $options as $i => $o)
                {
                    if($game_data['steamauth'] == $i)
                        echo " <option value='{$i}' selected='selected'>{$o}</option>";
                    else
                        echo " <option value='{$i}'>{$o}</option>";       
                }
                ?>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="span6 pull-left"><h3>Match Servers<a><i class="icon-question-sign tooltip-on" title="Select whether games require IGL supplied dedicated servers."></i></a></h3>
                <select class='loginfield' name='hostedservers'>
                <?php
                $options = array('Developer Hosted','IGL Hosted');
                foreach( $options as $i => $o)
                {
                    if($game_data['hostedservers'] == $i)
                        echo " <option value='{$i}' selected='selected'>{$o}</option>";
                    else
                        echo " <option value='{$i}'>{$o}</option>";       
                }
                ?>
                </select>
                
                </div>
                <div class="span5 pull-left"><h3>Game launched by URL<a><i class="icon-question-sign tooltip-on" title="Games like QuakeLive can be launched by providing a URL to the match."></i></a></h3>
                    <select class='loginfield' name='match_url'>
                <?php
                $options = array('No','Yes');
                foreach( $options as $i => $o)
                {
                    if($game_data['match_url'] == $i)
                        echo " <option value='{$i}' selected='selected'>{$o}</option>";
                    else
                        echo " <option value='{$i}'>{$o}</option>";       
                }
                ?>
                    </select>
                </div>
            </div>
            


        </div>

    <input id='id' type='hidden' name='id' value='<?php if( $mode =='edit') echo $_REQUEST['id']; ?>' >
    </div>
  
    <div class='box'>
      <h1>Game Roles</h1>
      <div style='margin-top:12px' class='inner_box row'>
        <div class="span6">
          <h3>Existing Roles Defined</h3>
          <ul id="existing_game_roles">
            <?php
              if (count($game_data['categories'])) {
                foreach($game_data['categories'] as $c) {
                  echo "<li>{$c['category']}&nbsp;<i class='icon icon-remove red' data-id='{$c['id']}' onclick='removeGameRole(this, {$game_id})'></i></li>";
                }
              } else {
                echo "<li>No Roles defined.</li>";
              }
            ?>
          </ul>
        </div>
        <div class="span5">
          <h3>Add a New Role</h3>
          <input type="text" name="game_role" value="">
          <button id="add_new_role" type="button" onclick="addGameRole(this, <?php echo $game_id; ?>);"><i></i>Add</button>
        </div>
      </div>
    </div>

    <div class='box'>
    <h1>Images</h1>
    <div style='margin-top:12px' class='inner_box'>
        <img id="icon" style="margin:2px;" data-src="/js/holder.js/18x18" class='img-polaroid tooltip-on' title='18 x 18' <?php if($mode == 'edit' && $game_data['icon']) echo "src='/img{$game_data['icon']}'"; ?>><input style='visibility:hidden;' type='file' name='icon'><br />
        <img id="logo" style="margin:2px;" data-src="/js/holder.js/40x40" class='img-polaroid tooltip-on' title='40 x 40' <?php if($mode == 'edit' && $game_data['logo']) echo "src='/img{$game_data['logo']}'"; ?>><input style='visibility:hidden;' type='file' name='logo'><br />
        <img id="tile" style="margin:2px;" data-src="/js/holder.js/220x220" class='img-polaroid tooltip-on' title='220 x 220' <?php if($mode == 'edit' && $game_data['tile']) echo "src='/img{$game_data['tile']}'"; ?>><input style='visibility:hidden;' type='file' name='tile'><br />
        <img id="banner" style="margin:2px;" data-src="/js/holder.js/620x50" class='img-polaroid tooltip-on' title='620 x 50' <?php if($mode == 'edit' && $game_data['banner']) echo "src='/img{$game_data['banner']}'"; ?>><input style='visibility:hidden;' type='file' name='banner'>
    </div>
    </div>

    <div class='box'>
    <h1>About<a><i class="icon-question-sign tooltip-on" title="A brief write-up about the game."></i></a></h1>
    <div stye='margin-top:12px' class='inner_box'>
        <textarea style='height: 600px;' id='about' name='about'><?php if( $mode =='edit') echo $game_data['about']; ?></textarea>
    </div>
    </div>

    </form>