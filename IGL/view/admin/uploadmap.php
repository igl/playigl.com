<div class="span12">
<?php
include('./view/admin/subnavigation.php');

if( LEAGUEADMIN )
{    
     // Classes
     $game = new game();
     $game_data = $game->get($_REQUEST['id']);
     
     $maps = new maps();
     $game_maps = $maps->getAll($_REQUEST['id']);
     
     $server_config = new serverConfig();
     $configs = $server_config->getAll();

     ?>
<div class="box">
        <h1>Upload Map - <?php echo $game_data['name']; ?></h1>
        <div class="inner_box" style="margin-top:10px">
            <div class="row" style="margin-left:0px;">
                <form action="/ajax/admin/map_upload.php" method="post" target="upload_frame" enctype="multipart/form-data">
                    <label for="file">Map Name:</label>
                    <input type="text" name="name" id="name" /><br>
                    <label><span class="tooltip-on" title="Not required">File Upload:</span></label>
<input type="file" name="file" id="file"><br>
<input type="hidden" name="game" id="game" value="<?php echo $_REQUEST['id']; ?>">
<label><span class="tooltip-on" title="Required for games where each map requires a different setting. Example: TF2">Associated Config</span></label>
<select name="config_id">
    <option value="">Not Required</option>
    <?php
    foreach($configs as $c)
        echo "<option value='{$c['id']}'>{$c['name']}</option>";
    ?>
</select><br>
<input type="submit" name="submit" value="Submit">
</form>
                <iframe src="" border="0" id="upload_frame" name="upload_frame"></iframe>
            </div>
        </div>
</div>

<div class="box">
        <h1>Maps - <?php echo $game_data['name']; ?></h1>
        <div class="inner_box" style="margin-top:10px">
            <div class="row" style="margin-left:0px;">
                 <table class="table table-condensed table-striped table-hover gameList">
                    <thead>
                        <tr>
                            <th>Map Name</th>
                            <th>Config Settings</th>
                            <th>Action</th>  
                        </tr>
                    </thead>
                    <tbody>
                 <?php
                        if (!empty($game_maps)) {
                        foreach($game_maps as $mp) {
                            echo "<tr><td>{$mp['map']}</td><td>{$mp['config_name']}</td><td><a href='/admin/deletemap/{$mp['id']}' class='btn btn-danger tooltip-on' title='Remove Map'><i class='icon-remove'></i></a></td></tr>";
                        }
                        }
                        ?>
                    </tbody>
                 </table>
            </div>
        </div>
</div>
<?php
}
else
{
    echo "You do not have access to this page.";
}
?>
</div>