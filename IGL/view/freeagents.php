<?php
    if(IGL_PAGE!=true)die();
?>
<div class="span8">
    <div class="navbar navbar-division">
        <div class="navbar-inner">
            <ul class="nav">
                <li><a>Game Selection</a></li>
            </ul>
            <ul class="nav pull-right">
            <?php 
                if (isset($navigation)) {
                    foreach($navigation['navigation']['league'] as $g)
                    {
                        echo "<li><a style='padding:0px' href='/freeagents/{$g['game_id']}-{$g['game_name']}'><img src='/img{$g['game_logo']}'></a></li>";
                        echo "<li class='divider'></li>";
                    }
                }
            ?>
            </ul>
        </div>
    </div>

    <table style='margin-top:12px' class='table table-condensed table-striped table-bordered table-stats' id='freeagentlist'>
        <thead>
            <tr><th colspan='5'><?php echo $free_agent_list['game_data']['game_name']; ?> FREE AGENTS <button id='button_enlist' class='hide btn btn-mini pull-right'>Enlist</button></tr>
            <tr><th colspan='2'>Player</th><th>Location</th><?php if($free_agent_list['game_data']['game_steamauth']) { echo "<th>SteamID</th>"; }?><th>Date Listed</th></tr>
        </thead>
        <tbody></tbody>
    </table>

    <div class="modal hide" id="freeagentModal" tabindex="-1" role="dialog" aria-labelledby="freeagentModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class='icon-remove'></i></button>
            <h3 id="freeagentModalLabel">LOADING</h3>
        </div>
        <div class="modal-body" id="freeagentModalBody">
            <div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div>
        </div>
        <div class="modal-footer">
            <button id="freeagentModalButton" class="btn btn-primary">SAVE</button>
        </div>
    </div>
    
    <div style="display:none;" id="server-data" data-game-id="<?php echo $game; ?>" data-player-id="<?php echo $_SESSION['playerid']; ?>"></div>
</div>