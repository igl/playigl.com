<div class="span8">
    <div class="profile-banner">
        <div class="avatar"><img src="/img<?php echo $player['player']['player_avatar']; ?>" /></div>
        <div class="overlay-wrap">
            <div class="left">
                <h2><?php echo $player['player']['player_username']; ?></h2>
                <div class="name"><?php echo "{$player['player']['player_firstname']} {$player['player']['player_lastname']}";?> <img src="/img/flags/<?php echo $player['player']['player_country']; ?>.png"></div>
            </div>
            <div class="amount">
                <div class="number"><?php echo $player['player']['player_following']; ?></div>
                <div class="type">following</div>
            </div>
            <div class="amount">
              <div class="number"><a onclick="viewFollowers('player',<?php echo $id; ?>,0<?php echo $_SESSION['playerid']; ?>);"><?php echo $player['player']['player_followers']; ?></a></div>
              <div class="type">followers</div>
            </div>
        </div>
        <div class="banner"><img src="/img/igl-profile-cover.jpg" /></div>
    </div>
    <div class="box">
        <div class="inner_box">
            <div class="top_buttons">
                <?php
                if($_SESSION['playerid'] != $id) {?>
                <div id="follow_button_<?php echo $id ?>" class="pull-right" style="width:118px; text-align:right"></div>
                <?php } ?>
                <div id="invite_button_<?php echo $id ?>" class="pull-right btn-group" style="margin-right:6px; width:138px; text-align:right"></div>
            </div>
            <div class="info clearfix">
                <div class="left">
                    <strong><?php echo "{$player['player']['player_city']}, {$player['player']['player_state']}"; ?></strong><br />
                    <?php echo age($player['player']['player_birthday']); ?> Years Old<br />Member Since <?php echo date('F jS, Y ',$player['player']['player_joindate']); ?><br />
                </div>
                <div class="right">
                    <?php if( strlen($player['player']['player_steam_id']) > 0 )
                    {
                        echo "<div class='steam-id'>Steam ID: <a href='http://steamcommunity.com/profiles/{$player['player']['player_community_id']}'>{$player['player']['player_steam_id']}</a> <a href='steam://friends/add/{$player['player']['player_community_id']}'><i class='icon-plus tooltip-on' title='Add to Steam friends'></i></a></div>";
                    }
                    if( $player['player']['player_facebook'] ) 
                    {
                        ?><!--
                        <div data-width="50" data-show-faces="false" data-layout="button_count" data-href="https://www.facebook.com/<?php echo $player['player']['player_facebook']; ?>" class="fb-subscribe fb_iframe_widget" fb-xfbml-state="rendered">
                            <span style="width: 83px; height: 20px;">
                                <iframe width="50px" scrolling="no" height="1000px" frameborder="0" name="f17c78d91d3b7c" allowtransparency="true" style="border: medium none; width: 83px; height: 20px;" src="http://www.facebook.com/plugins/subscribe.php?href=https%3A%2F%2Fwww.facebook.com%2F<?php echo $player['player']['player_facebook']; ?>&amp;layout=button_count&amp;show_faces=false&amp;width=50&amp;app_id=190820527646793&amp;locale=en_US&amp;sdk=joey&amp;channel=http%3A%2F%2Fstatic.ak.facebook.com%2Fconnect%2Fxd_arbiter.php%3Fversion%3D18%23cb%3Df51d44514b0ca2%26origin%3Dhttp%253A%252F%252F<?php echo BASEURL; ?>f1dae9244c7c39c%26domain%3D<?php echo BASEURL; ?>%26relation%3Dparent.parent" class=""></iframe>
                            </span>
                        </div>-->
                        <?php
                    }

                    if( $player['player']['player_twitter'] )
                    {
                        ?>
                        <iframe scrolling="no" frameborder="0" allowtransparency="true" src="http://platform.twitter.com/widgets/follow_button.1362008198.html#_=1362539894638&amp;id=twitter-widget-0&amp;lang=en&amp;screen_name=<?php echo $player['player']['player_twitter']; ?>&amp;show_count=false&amp;show_screen_name=true&amp;size=m" class="twitter-follow-button twitter-follow-button" style="width: 58px; height: 20px;" title="Twitter Follow Button" data-twttr-rendered="true"></iframe>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <div class="more-info separator"><div class="last-active">Last Active: <?php echo $player['player']['player_lastactivity']; ?></div></div>
        </div>
        <div class="profile-tabs">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#teams">Teams</a></li>
                <li class="aliases"><a data-toggle="tab" href="#aliases">Aliases</a></li>
                <li class=""><a data-toggle="tab" href="#streams">Streams</a></li>
                <li class=""><a data-toggle="tab" href="#former-teams">Former Teams</a></li>
            </ul>
            <div class="tab-content inner_box profile-tab-content">
                <div id="teams" class="tab-pane fade active in">
                    <ul class="teams-list">
                        <?php 
                        foreach( $player['player']['player_teams'] as $i => $t )
                        {
                            $num = $i+1;
                            $img = "http://placehold.it/184x184";
                            if($t['team_avatar']>0)
                                $img = "/img{$t['team_avatar']}";
                                ?>
                        <li <?php if ($num % 3 == 0) echo "class='last'" ?>>
                            <a href="/team/<?php echo "{$t['team_id']}-{$t['team_name']}"; ?>"><img src="<?php echo $img; ?>" class="team-img" /></a>
                            <h3><a href="/team/<?php echo "{$t['team_id']}-{$t['team_name']}"; ?>"><?php echo $t['team_name']; ?></a></h3>
                            <span class="game"><?php echo $t['team_game_name']; ?></span>
                            <span class="tag"><?php echo $t['team_tag']; ?></span>
                            <!--<span class="winloss"><span class="win">3</span> - <span class="loss">0</span></span>-->
                            <span class="rpi">RPI: <a href="#"><?php echo $t['team_rpi']; ?></a></span>
                        </li>
                        <?php } ?>
                    </ul>
                </div>
                <div id="aliases" class="tab-pane fade">
                    <table class="table table-striped table-condensed table-bordered">
                        <tbody>
                            <?php
                            foreach($player['player']['player_ingame_name'] as $i => $n)
                                echo "<tr><td>{$i}</td><td>{$n}</td></tr>";
                            ?>
                        </tbody>
                    </table>
                </div>
                <div id="streams" class="tab-pane fade">
                        <?php
                        echo "<ul class='match-cards'>";
                        if($channels) {
                            foreach($channels->videos as $vod) {
                                $num = $i +1;
                                echo "<li style='width:183px!important;'"; if ($num % 2 == 0) echo "class='last'"; echo  ">";

                            echo "
                              <div class='inner' style='height:100%; background:#000'>
                                <div class='image' style='height:100%;'><a href='{$vod->url}' target='_blank'><img src='{$vod->preview}' style='height:100%;' /></a></div>
                                <div class='title'>
                                  <h4><a href='{$vod->url}' target='_blank'>{$vod->title}</a></h4>
                                </div>
                                <div class='badge-top' style='width:174px!important'>
                                  <i class='icon-play-circle'></i> ".date('M jS, Y',strtotime($vod->recorded_at))." <div class='pull-right'>{$vod->views} views</div>
                                </div>
                              </div>
                            </li>";
                            }
                            echo "</ul>";
                        } else {
                            //User does not have a linke twitch account
                            if($_SESSION['playerid'] == $id) {
                                ?>
                                <div class='social-connect'><a class="connect-btn twitch" href="/dashboard/profile"><div class="icon-wrap"><img class="icon" src="/img/register/icon-twitch.png"></div> <span>Connect With Twitch</span></a></div>
                                <?php
                            } else {
                                echo "<div class='alert alert-info'>User has no recordings, or has not connected their twitch account.</div>";
                            }
                        }
                        ?>
                    
                </div>
                <div id="former-teams" class="tab-pane fade">
                    <ul class="teams-list">
                        <?php 
                        foreach( $player['player']['player_previous_teams'] as $i => $t )
                        {
                            $num = $i+1;
                            $img = "http://placehold.it/184x184";
                            if($t['team_avatar']>0)
                                $img = "/img{$t['team_avatar']}";
                                ?>
                            <li <?php if ($num % 3 == 0) echo "class='last'" ?>>
                            <a href="/team/<?php echo "{$t['team_id']}-{$t['team_name']}"; ?>"><img src="<?php echo $img; ?>" class="team-img" /></a>
                            <h3><a href="/team/<?php echo "{$t['team_id']}-{$t['team_name']}"; ?>"><?php echo $t['team_name']; ?></a></h3>
                            <span class="game"><?php echo $t['team_game_name']; ?></span>
                            <span class="tag"><?php echo $t['team_tag']; ?></span>
                            <span class="period"><?php echo date('M Y',$t['team_join_date']). " - ". date('M Y',$t['team_leave_date']); ?></span>
                        </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="inner_box">
        
            <form id="comment_form">
                <input type="hidden" id="viewer_id" name="viewer_id" value="<?php echo $_SESSION['playerid'];?>">
                <input type="hidden" id="page_id" name="page_id" value="<?php echo $player['player']['player_id'];?>">
                <input type="hidden" id="page_name" name="page_name" value="player">
                <?php
                if( isset($_SESSION['playerid']) && $_SESSION['playerid'] > 0 )
                {
                ?>
                <textarea style="width:562px;height:60px" name="comment" id="comment"></textarea>
                <div style="height:30px" class="full">
                    <small class="pull-left">You have <span id="chars">130</span> characters left.</small>
                    <button class="btn btn-primary pull-right" onclick="commentPost();" type="button" id="button_post">Post Comment</button>
                </div>
                <?php
                }
                ?>
            </form>
        
            
            <ul class="clear media-list" id="player_comments"></ul>
        </div>
    </div>
    <!-- /box -->
    <div aria-hide="true" aria-labelledby="teamModalLabel" role="dialog" tabindex="-1" id="Modal" class="modal hide">
        <div class="modal-header">
            <button aria-hide="true" data-dismiss="modal" class="close" type="button"><i class="icon-remove"></i></button>
            <h3 id="ModalLabel">LOADING</h3>
        </div>
        <div id="ModalBody" class="modal-body">
            <div class="progress progress-striped active"><div style="width: 100%;" class="bar"></div></div>
        </div>
        <div class="modal-footer">
            <button aria-hide="true" data-dismiss="modal" class="btn" id="ModalButtonClose">CLOSE</button>
            <button class="btn btn-primary" id="ModalButton">SUBMIT</button>
        </div>
    </div>
</div>
  <!-- /content --> 