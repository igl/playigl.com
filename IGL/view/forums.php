<div class="span12">
    <?php if (isLoggedIn()) { ?><a class="btn btn-primary pull-right" href="/forums/post/new">New Post</a><?php } ?>
    <p class="pull-right" style="line-height:30px; margin-right:8px;">These forums are in <span class="label label-warning">BETA</span>.</p>
    <form method="get" class="form-inline" action="/forums/search/" accept-charset="UTF-8">
        <label for="s">Search:</label>
        <input type="text" name="s" id="search">
        <label for="tag">in</label>
        <select name="tag">
            <option value="">All Forums</option>
            <?php
            if(is_array($forums->tags))
                foreach($forums->tags as $tag) {
                    $tag_id = urlencode($tag['name']);
                    echo <<< TAG
                        <option value="{$tag_id}">{$tag['name']}</option>
TAG;
                }
            ?>
        </select>
        <input type="submit" class="btn" value="Go">
    </form>
    
    <?php
        if (!empty($return['html'])) {
            echo <<< TABLE
                {$return['search']}
                <table class="table table-striped">
                    {$return['html']}
                </table>
                {$return['pagination']}
                {$return['viewers']}
TABLE;
        }
    ?>
</div>