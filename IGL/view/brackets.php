<?php
/*
 * @copyright 2013 PlayIGL.com
 * @Author Kevin Langlois
 */
require('../includes/config.php');
require('../model/mysql.class.php');
$db = new MySQL();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>International Gaming League - Tournament Bracket Generator</title>
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet" type="text/css" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
        <link href="/css/bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="/css/font-awesome.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="/css/stylesheet.css" />
        <link rel="stylesheet" href="/css/brackets.css" />
        <script src="/js/jquery-1.8.2.js"></script>
        <script>
            window.onload = function() {
                $('.team').hover(function() {
                    var team = $(this).find('span').text();
                    $('#brackets').find('span:contains("' + team + '")').each(function(i, v) {
                        $(v).parent().parent().addClass('highlight');
                    });
                }, function() {
                    var team = $(this).find('span').text();
                    $('#brackets').find('span:contains("' + team + '")').each(function(i, v) {
                        $(v).parent().parent().removeClass('highlight');
                    });
                });
            }
        </script>
    </head>
    <body>

        <header style="height:96px;">
            <div class="wrap">
                <div class="row">
                    <div class="span6"><a href="http://www.playigl.com/"><img src="http://www.playigl.com/img/igl_logo.png" alt="International Gaming League"></a></div>
                </div>
            </div>
        </div>
    </header>

    <div id="bracket-container">

        <?php
        error_reporting(E_ALL);

        /*********************************
         * Templates
        /********************************/
        $match_left = '<tr><td><div class="match"><div class="team team1"><div class="score [team1_winlose] left">[team1_score]</div><div class="num left">[team1_num]</div><div class="name"><span class="tooltip-on">[team1]</span></div></div><div class="team team2"><div class="score [team2_winlose] left">[team2_score]</div><div class="num left">[team2_num]</div><div class="name"><span class="tooltip-on">[team2]</span></div></div></div></td></tr>';
        $match_right = '<tr><td><div class="match"><div class="team team1"><div class="score [team1_winlose] right">[team1_score]</div><div class="num right">[team1_num]</div><div class="name"><span class="tooltip-on">[team1]</span></div></div><div class="team team2"><div class="score [team2_winlose] right">[team2_score]</div><div class="num right">[team2_num]</div><div class="name"><span class="tooltip-on">[team2]</span></div></div></div></td></tr>';
        $match_single = '<tr><td><div class="match"><div class="team team1"><div class="num left">[team1_num]</div><div class="name"><span class="tooltip-on">[team1]</span></div></div></td></tr>';
        $match_champion = '<tr><td><div class="match match-large"><div class="team team-large team1"><div class="num num-large left">[team1_num]</div><div class="name name-large"><span class="tooltip-on">[team1]</span></div></div></td></tr>';
        $round_start = ' <td><table style="height:100%; width:100%;">';
        $round_end = '</table></td>';
        $round_details = '<tr><td class="round_details"></td><tr/>';
        $line_start = '<td class="match-line-col"><table style="height:100%;">';
        $line_inner = '<tr><td class="match-line-not"></td></tr><tr><td class="match-line"></td></tr><tr><td class="match-line"></td></tr><tr><td class="match-line-not"></td></tr>';
        $line_end = '</table></td>';
        $line_inner_middle = '<tr><td class="match-line-not"></td></tr><tr><td class="match-line-not"></td></tr><tr><td class="match-line"></td></tr><tr><td class="match-line-not"></td></tr><tr><td class="match-line-not"></td></tr>';
           
        /*********************************
         * Variables
        /********************************/
        $round_count = 0;
        $outter_bracket_flag = false;
        $champion_round = null;

        /*********************************
         * Print Matches Function
        /********************************/
        function printMatch($template, $team1, $team1_num, $team1_score, $team2 = '', $team2_num = '', $team2_score = '') {

            $html = $template;
            $html = str_replace('[team1]', $team1, $html);
            $html = str_replace('[team1_score]', $team1_score, $html);
            $html = str_replace('[team1_num]', $team1_num, $html);
            $html = str_replace('[team2]', $team2, $html);
            $html = str_replace('[team2_score]', $team2_score, $html);
            $html = str_replace('[team2_num]', $team2_num, $html);
            if ($team1_score > $team2_score) {
                $html = str_replace('[team1_winlose]', 'win', $html);
                $html = str_replace('[team2_winlose]', 'lose', $html);
            } else if ($team1_score < $team2_score) {
                $html = str_replace('[team2_winlose]', 'win', $html);
                $html = str_replace('[team1_winlose]', 'lose', $html);
            } else {
                $html = str_replace('[team2_winlose]', '', $html);
                $html = str_replace('[team1_winlose]', '', $html);
            }
            return $html;
        }

        /*********************************
         * Retrieve Rounds and Matches
        /********************************/
        $query = "SELECT * FROM tournament_schedule WHERE tournament_id = " . $_REQUEST['tournament_id'] . " ORDER BY round_number DESC";
        $db->ExecuteSQL($query);
        if ($db->iRecords()) {
            $rounds = $db->ArrayResults();
            $c = 0;
            foreach ($rounds as $r) {
                $round_count++;
                $query = "SELECT * FROM tournament_matches JOIN schedule ON tournament_matches.submission_id = schedule.id WHERE tournament_matches.schedule_id = " . $r['id'];
                $db->ExecuteSQL($query);
                if ($db->iRecords()) {
                    $r['matches'] = $db->ArrayResults();
                    $rounds[$c]['matches'] = $r['matches'];
                    $c++;
                }
            }
        }
        
        /*********************************
         * Determine If the bracket needs to be split in half
        /********************************/
        if ($round_count < 5) {
            $width = ($round_count * 110) + (($round_count - 1) * 16);
        } else {
            $width = 976;
            $outter_bracket_flag = true;
            $outter_bracket = $round_count * 2;
            $champion_round = $rounds[$round_count - 1];
            unset($rounds[$round_count - 1]);
            for ($i = $round_count - 2; $i > -1; $i--) {
                $rounds[$outter_bracket] = $rounds[$i];
                $left_bracket = array_slice($rounds[$i]['matches'], 0, count($rounds[$i]['matches']) / 2);
                $right_bracket = array_slice($rounds[$i]['matches'], count($rounds[$i]['matches']) / 2);
                $rounds[$i]['matches'] = $left_bracket;
                $rounds[$outter_bracket]['matches'] = $right_bracket;
                $outter_bracket--;
            }
        }
        
        /*********************************
         * Render Brackets Left to Right
        /********************************/
        $result = '';
        $result .= '<div id="brackets" style="width:' . $width . 'px;">';
        
        if ($champion_round != null) {
            $result .= '<table style="position:absolute; top:90px; left:412px; width:110px; height:55px;">';
            $result .= '<tr><td class="champion-text">CHAMPION</td></tr>';
            $result .= printMatch($match_champion, '', '', '');
            $result .= '</table>';
            $result .= '<table style="position:absolute; top:170px; left:430px; width:110px; height:55px;">';
            $result .= printMatch($match_left, '', '', '', '', '', '');
            $result .= '</table>';
        }
        
        $result .= '<table class="bracket-table" style=""><tr>';
        
        $round_counter = 0;
        foreach ($rounds as $r) {
            $roundMatches = '';
            $roundMatches .= $round_start;
            $l = 1;
            foreach ($r['matches'] as $m) {
                $query = 'SELECT * FROM tournament_teams join team on tournament_teams.team_id = team.id where tournament_teams.team_id = '.$m['team1_id'].' AND tournament_id = '.$_REQUEST['tournament_id'];
                $db->ExecuteSQL($query);
                $m['team1_data'] = $db->ArrayResult();
                $query = 'SELECT * FROM tournament_teams join team on tournament_teams.team_id = team.id where tournament_teams.team_id = '.$m['team2_id'].' AND tournament_id = '.$_REQUEST['tournament_id'];
                $db->ExecuteSQL($query);
                $m['team2_data'] = $db->ArrayResult();
                if ($outter_bracket_flag) {
                if ($round_counter < (count($rounds) / 2)) {
                    $roundMatches .= printMatch($match_left, $m['team1_data']['tag'], $m['team1_data']['seed'], $m['home_score'], $m['team2_data']['tag'], $m['team2_data']['seed'], $m['away_score']);
                } else {
                    $roundMatches .= printMatch($match_right, $m['team1_data']['tag'], $m['team1_data']['seed'], $m['home_score'], $m['team2_data']['tag'], $m['team2_data']['seed'], $m['away_score']);
                }
                } else {
                   $roundMatches .= printMatch($match_left, $m['team1_data']['tag'], $m['team1_data']['seed'], $m['home_score'], $m['team2_data']['tag'], $m['team2_data']['seed'], $m['away_score']); 
                }
                $l++;
            }
            $roundMatches .= $round_end;

            if ($outter_bracket_flag) {
                if ($round_counter < (count($rounds) / 2)) {
                    if ($round_counter == (count($rounds) / 2) - 1) {
                        $result .= $roundMatches;
                        $result .= $line_start;
                        $result .= $line_inner_middle;
                        $result .= $line_end;
                    } else {
                        $result .= $roundMatches;
                        $result .= $line_start;
                        for ($i = 1; $i < ($l / 2); $i++) {
                            $result .= $line_inner;
                        }
                        $result .= $line_end;
                    }
                } else {
                    if ($round_counter == (count($rounds) / 2)) {
                        $result .= $roundMatches;
                    } else {

                        $result .= $line_start;
                        for ($i = 1; $i < ($l / 2); $i++) {
                            $result .= $line_inner;
                        }
                        $result .= $line_end;
                        $result .= $roundMatches;
                    }
                }
            } else {
                if ($round_counter == count($rounds)-1) {
                $result .= $roundMatches;
                } else {
                $result .= $roundMatches;
                $result .= $line_start;
                for ($i = 1; $i < ($l / 2); $i++) {
                    $result .= $line_inner;
                }
                $result .= $line_end;
                }
            }

            $round_counter++;
        }
        $result .= "</tr></table></div>";

        echo $result;
        ?>
    </div>


    <div id="brackets1" style="display:none;">
        <table style="position:absolute; top:90px; left:412px; width:110px; height:55px;">
            <tr><td class="champion-text">CHAMPION</td></tr>
            <?php printMatch($match_champion, '', '', '');
            ;
            ?>
        </table>
        <table style="position:absolute; top:170px; left:430px; width:110px; height:55px;">
<?php printMatch($match_left, '', '', '', '', '', ''); ?>
        </table>
        <table style="position:absolute; top:360px; left:430px; width:110px; height:55px;">
            <tr><td class="winner-text">3rd Place Match</td></tr>
<?php printMatch($match_left, '', '', '', '', '', ''); ?>
        </table>
        <table style="position:absolute; top:450px; left:430px; width:110px; height:55px;">
            <tr><td class="winner-text">2nd Place Winner</td></tr>
<?php printMatch($match_single, '', '', ''); ?>
        </table>
        <table style="position:absolute; top:510px; left:430px; width:110px; height:55px;">
            <tr><td class="winner-text">3rd Place Winner</td></tr>
<?php echo printMatch($match_single, '', '', ''); ?>
        </table>
    </div>
</body>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js"></script>
<script type="text/javascript" src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/jquery.bracket.js"></script>
<link rel="stylesheet" type="text/css" href="/css/jquery.bracket.css" />
<script type="text/javascript" src="/js/brackets.js"></script>
<script type="text/javascript">
    //$('.tooltip-on').tooltip();

</script>
</html>

