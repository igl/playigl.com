<?php
/*
 * @copyright 2012 PlayIGL.com
 * @author Darryl Allen
 */
echo "<div class='box'>";
echo "<h1 style='margin-bottom:12px;'>MATCH STATS</h1>";
foreach($stats as $round => $round_results)
{
    echo "<div class='inner_box'>";
    echo "<h3>$round</h3>";
    
    foreach($round_results as $team => $player)
    {
        echo "<table class='table table-condensed table-striped'>";
        echo "<tr><td colspan='7'><h4>$team</h4></td></tr>";
        
        //Headings
        echo "<tr><td width='450px'>Name</td><td><a rel=tooltip title='Kills'>K</a></td><td><a rel=tooltip title='Deaths'>D</a></td><td><a rel=tooltip title='Headshots'>H</a></td><td><a rel=tooltip title='2 Kill Streaks'>2K</a></td><td><a rel=tooltip title='3 Kill Streaks'>3K</a></td><td><a rel=tooltip title='4 Kill Streaks'>4K</a></td></tr>";
        
        foreach($player as $stat)
            echo "<tr><td>{$stat['firstname']} <a href='/playerstats/{$id}/{$stat['id']}' rel='tooltip' title ='{$stat['ingame_name']}'>{$stat['username']}</a> {$stat['lastname']}</td><td>{$stat['kills']}</td><td>{$stat['deaths']}</td><td>{$stat['Headshot']}</td><td>{$stat['Double Kill (2 kills)']}</td><td>{$stat['Triple Kill (3 kills)']}</td><td>{$stat['Domination (4 kills)']}</td></tr>";

        echo "</table>";
    }
    echo "</div>";
}
echo "</div>";
#debug($stats);
?>