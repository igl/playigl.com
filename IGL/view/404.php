<div class="box full">
    <h1>404 - Page Not Found</h1>
    <div class="inner_box" style="margin-top: 12px">
        <div class="row-fluid">
            <div class="span7">
                <h3>Ooops! That page doesn't exist.</h3>
                <h5><i class="icon-warning-sign"></i> We are really sorry but the page you requested cannot be found.</h5>
                <p>We think that the best thing to do is to start again from the <a href="/home">home page</a>. Feel free to <a href="mailto:info@playigl.com">contact us</a> if the problem persists or if you definitely can't find what you are looking for. Thank you very much.</p>

            </div>
        </div>
    </div>
</div>