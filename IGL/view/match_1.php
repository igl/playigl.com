<?php
if(IGL_PAGE!=true)die();
echo "<div class='span8'>";
include('./view/league/subnavigation.php');
echo "<div class='box' style='margin-top:12px;'>";
echo "<h1>{$match['teams']['home']['tag']} vs {$match['teams']['away']['tag']} - {$this->league->name($match['data']['league'],false)}</h1>";
echo "<table class='table'>";
echo "<tr>
<td style='padding-top:10px'><img class='img-rounded' src='".BASEURL."img{$this->team->logo($value['home'])}' width='184' height='184'>
<ul class='unstyled center'>
    <li><h4 class='igl-red'>{$match['teams']['home']['name']}</h4></li>
    <li><h6>{$match['teams']['home']['wins']}-{$match['teams']['home']['losses']}</h6></li>
</ul>   
        </td>
<td style='padding-top:10px;' width='100%'>

<ul class='unstyled center'>
    <li><h3 class='igl-red'>{$match_date}</h3></li>
    <li><h4>{$match_time}</h4></li>
    <li><h5>{$this->match->type($match['data']['matchtype'])}</h5></li>
    <li> SEASON <span class='igl-red'>&nbsp;{$match['data']['season']}</span></li>
    <li> WEEK <span class='igl-red'>&nbsp;{$match['data']['week']}</span></li>
    <li><span class='igl-red'>&nbsp;{$match['data']['map']}</span></li>
</ul>
</td>
<td style='padding-top:10px'><img class='img-rounded' src='".BASEURL."img{$this->team->logo($value['away'])}'  width='184' height='184'>
<ul class='unstyled center'>
    <li><h4 class='igl-red'>{$match['teams']['away']['name']}</h4></li>
    <li><h6>{$match['teams']['away']['wins']}-{$match['teams']['away']['losses']}</h6></li>
</ul>
        </td>
</tr>";
echo "</table>";
echo "</div>";
echo "</div>";

/*
$stats = json_decode(file_get_contents(BASEURL.'api/fetch_stats.php?date=1353002400&server=1&get=match'),true);
if($stats['success'])
{

    echo "<div class='box' style='margin-top:12px;'>";
    echo "<h1>{$match['teams']['home']['tag']} vs {$match['teams']['away']['tag']} - Round 1 - {$this->league->name($match['data']['league'],false)}</h1>";
    echo "<h4>Counter Terrorist - Team 1</h4>";
    echo "<table class='table table-condensed table-striped'>";
    echo "<tr><td width='200'><b>Player</b></td><td width='30'><b>Kills</b></td><td width='30'><b>Deaths</b></td><td><b>KDR</b></td><td><b>Defuse Attempts</b></td><td><b>Defuses</b></td></tr>";

    foreach($stats['stats']['CT']['team1'] as $key => $value)
    {
            $defuse_attempts = ($value['start_bomb_defuse'] + $value['start_bomb_defuse_kit']);
            $player_info = player_from_communityID($value['CommunityID']);
            echo "<tr><td>{$player_info['username']}</td><td>{$value['kills']}</td><td>{$value['deaths']}</td><td>{$value['KDR']}</td><td>{$defuse_attempts}</td><td>{$value['success_bomb_defuse']}</td></tr>";
    }
    echo "</table>";

    echo "<h4>Terrorist - Team 2</h4>";
    echo "<table class='table table-condensed table-striped'>";
    echo "<tr><td width='200'><b>Player</b></td><td width='30'><b>Kills</b></td><td width='30'><b>Deaths</b></td><td><b>KDR</b></td><td><b>Bomb Plants</b></td></tr>";

    foreach($stats['stats']['TERRORIST']['team2'] as $key => $value)
    {
            $player_info = player_from_communityID($value['CommunityID']);
            echo "<tr><td>{$player_info['username']}</td><td>{$value['kills']}</td><td>{$value['deaths']}</td><td>{$value['KDR']}</td><td>{$value['success_bomb_plant']}</td></tr>";
    }
    echo "</table>";

    echo "</div>";
    
    
    //Round 2

    echo "<div class='box' style='margin-top:12px;'>";
    echo "<h1>{$match['teams']['home']['tag']} vs {$match['teams']['away']['tag']} - Round 1 - {$this->league->name($match['data']['league'],false)}</h1>";
    echo "<h4>Counter Terrorist - Team 2</h4>";
    echo "<table class='table table-condensed table-striped'>";
    echo "<tr><td width='200'><b>Player</b></td><td width='30'><b>Kills</b></td><td width='30'><b>Deaths</b></td><td><b>KDR</b></td><td><b>Defuse Attempts</b></td><td><b>Defuses</b></td></tr>";

    foreach($stats['stats']['CT']['team2'] as $key => $value)
    {
            $defuse_attempts = ($value['start_bomb_defuse'] + $value['start_bomb_defuse_kit']);
            $player_info = player_from_communityID($value['CommunityID']);
            echo "<tr><td>{$player_info['username']}</td><td>{$value['kills']}</td><td>{$value['deaths']}</td><td>{$value['KDR']}</td><td>{$defuse_attempts}</td><td>{$value['success_bomb_defuse']}</td></tr>";
    }
    echo "</table>";

    echo "<h4>Terrorist - Team 1</h4>";
    echo "<table class='table table-condensed table-striped'>";
    echo "<tr><td width='200'><b>Player</b></td><td width='30'><b>Kills</b></td><td width='30'><b>Deaths</b></td><td><b>KDR</b></td><td><b>Bomb Plants</b></td></tr>";

    foreach($stats['stats']['TERRORIST']['team1'] as $key => $value)
    {
            $player_info = player_from_communityID($value['CommunityID']);
            echo "<tr><td>{$player_info['username']}</td><td>{$value['kills']}</td><td>{$value['deaths']}</td><td>{$value['KDR']}</td><td>{$value['success_bomb_plant']}</td></tr>";
    }
    echo "</table>";

    echo "</div>";
}
else
{
    echo $stats['message'];
}
*/
?>