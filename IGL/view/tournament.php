<?php
/*
 * @copyright 2013 PlayIGL.com
 * @Author Kevin Langlois
 */

include_once('./includes/config.php');
include_once('./model/mysql.class.php');
include_once('./includes/functions.php');

include('./model/tournament.class.php');
$tournament = new tournament();
$t = $tournament->get($_REQUEST['id']);
?>
<style>
    #bracketSystem {
        width:618px; margin-left:-19px; margin-top:-21px;
    }
    #bracketSystem .team {
        width:290px; height:80px;
        background-color: #444; 
        color:#fff; font-size:16px; font-weight:bold;
    } 
    #bracketSystem .team span { font-size:12px; font-weight:normal; }
    #bracketSystem .team img {
        height: 64px; width:64px; float:left; margin:7px; border:1px solid #333;
    }
    #bracketSystem h4 { color:#fff; font-size:14px; margin:8px 0 0 0; font-weight:normal; padding:0; }
    #bracketSystem .team1 { float:left; text-align: left;}
    #bracketSystem .team2 { float:right; text-align: right; }
    #bracketSystem .team1 .score { float:right; margin-right:2px; }
    #bracketSystem .team2 .score { float:left; margin-left:2px; }
    #bracketSystem .team1 img { float:left; }
    #bracketSystem .team2 img { float:right; }

    #bracketSystem .score {
        width:50px; height:80px; float:right;
        background-image: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#aa3f3f), to(#981e1d));
        background-image: -webkit-linear-gradient(top, #aa3f3f, #981e1d); 
        background-image:    -moz-linear-gradient(top, #aa3f3f, #981e1d);
        background-image:     -ms-linear-gradient(top, #aa3f3f, #981e1d);
        background-image:      -o-linear-gradient(top, #aa3f3f, #981e1d); 
        color:#fff; font-size:22px; font-weight:bold;
        line-height:80px; text-align: center;
        position:relative; top:0px;
    }  
    #bracketSystem .matchbox {
        width:580px; height:80px;
        margin-bottom:0px;
        -moz-box-shadow:    2px 2px 2px 1px #666;
        -webkit-box-shadow: 2px 2px 2px 1px #666;
        box-shadow:         2px 2px 2px 1px #666;
    } 
    #bracketSystem .match_row {
        padding:19px;
    }
    #bracketSystem .grey {
        background:#E5E5E5;
    }
    .match_details {
        width:350px; height:30px;
        margin:0 auto 0px auto;
        -moz-box-shadow:    2px 2px 2px 1px #666;
        -webkit-box-shadow: 2px 2px 2px 1px #666;
        box-shadow:         2px 2px 2px 1px #666;
        background: #333333; /* Old browsers */
        background: -moz-linear-gradient(top, #333333 1%, #444444 100%); /* FF3.6+ */
        background: -webkit-gradient(linear, left top, left bottom, color-stop(1%,#333333), color-stop(100%,#444444)); /* Chrome,Safari4+ */
        background: -webkit-linear-gradient(top, #333333 1%,#444444 100%); /* Chrome10+,Safari5.1+ */
        background: -o-linear-gradient(top, #333333 1%,#444444 100%); /* Opera 11.10+ */
        background: -ms-linear-gradient(top, #333333 1%,#444444 100%); /* IE10+ */
        background: linear-gradient(to bottom, #333333 1%,#444444 100%); /* W3C */
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#333333', endColorstr='#444444',GradientType=0 ); /* IE6-9 */

        color:#ccc; font-size:12px; text-align:center; line-height:30px;
    }  
    #matchNavbar {
        position:relative; top:-21px; left:-19px; width:618px;
    }
    #matchNavbar .navbar-inner a {
        color:#333;
    }
    #tournament_activity {
        border: 1px solid #D4D4D4;
        background-color: #FFF;
        border-radius: 10px;
        width:25px; height:25px;
        margin:0 auto; float:none;
    }
</style>

<div class="row">
    <div class="span12">
        <div class='navbar'>
            <div class='navbar-inner'>
                <ul class="nav">
                    <a class="brand"> <?php echo "{$ladder_info['continent']} {$ladder_info['format']} {$ladder_info['name']}"; ?>  </a>
                    ></li>


                    <li class='divider-vertical'></li>
                </ul>
            </div>
        </div>


        <div class="clear" id='messages'></div>
    </div>
</div>
<div class="row">
    <div class="span12 event">
        <div class="banner">
            <img style="display:none;" class="img-header" id="tournament_game_banner" src="/slice_temp/img/event-banner-lol.png" />
            <img src="/img/event-banner-lol.png" />
            <div class="overlay">
                <div class="social">

                </div>
                <div class="bottom" id="calltoaction">
                    <h1><?php echo $t['name']; ?></h1>
                    <div class="progress progress-warning">
                        <div class="bar" style="width: 0%"></div>
                        <div class="text">Loading Event...</div>
                    </div>
                </div>
            </div>

        </div>
        <div class="nav-wrap">
            <ul class="nav nav-tabs nav-pills">
                <li class="active"><a id="about" onclick="Tournament.navAbout(this);">Event Info</a></li>
                <li><a onclick="Tournament.navSchedule(this);">Schedule</a></li>
                <li><a onclick="Tournament.navAdmins(this);">Admins</a></li>
                <li><a id="team_nav_count" onclick="Tournament.navTeams(this);">Participants</a></li>
                <li><a onclick="Tournament.navMatches(this);">Matches</a></li>
                <li><a onclick="Tournament.navDiscussion();">Discussion</a></li>
                <!--<li><a>Rules</a></li>-->
                <!--<li><a>Free Agents</a></li>-->
            </ul>
        </div>
        <div id="tournament_alert" style="display:none;" class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button></div>
        <div id="tournament_activity" style="display:none; margin-top:20px; text-align:center;"><img src="/img/tournament-loader.gif"/></div>
        <div class="tab-content" id="tournament_content" data-tournament-id="<?php echo $_REQUEST['id']; ?>" data-player-id="<?php echo $_SESSION['playerid']; ?>"></div>
    </div>
</div>
