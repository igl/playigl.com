<?php
/*
 * @copyright 2012 PlayIGL.com
 * @author Darryl Allen
 * This answers some Frequently Asked Questions
 */
?>

<div class='box'>
    <h1>Frequently Asked Questions</h1>
    <div class='inner_box' style='margin-top:10px'>
        <div class='articlecontent'>
            <ol>
                <li><p><strong>How do I add players to my team?</strong><br />Players must first register as a free agent. Once registered, captains can send invites from the free agents page</p></li>
                <li><p><strong>I created a team, but I don't see it in the standings.</strong><br />Teams cannot join a division until they have reached the minimum number of players required, usually 5. Once a team has joined a division, they are listed on the standings.</p></li>
                <li><p><strong>How do I join a team?</strong><br />To join a team, select the game & division from the menu. Here, you can <i>enlist</i> as a free agent.</strong></p></li>
                <li><p><strong>I have a friend who wants to join IGL, how can s/he?</strong><br />Send your friend a referral from your dashboard, or have them contact us.</p></li>
                <li><p><strong>Are there any location restrictions for IGL?</strong><br />No, unless otherwise stated for a specific game, division, or tournament.</p></li>
                <li><p><strong>How can I view job openings for IGL?</strong><br />We will often post on the forums, news or job boards when hiring. See <a href='http://jobsearch.monster.ca/jobs/?co=xw192182649wx&re=3101' target+'_blank'>here</a> for a list of paid openings.</p></li>
                <li><p><strong>How can I apply for a job at IGL?</strong><br />Click the jobs section at the footer of the site.</p></li>
                <li><p><strong>Where can I watch IGL livestreams?</strong><br />You can view schedules ahead of time on the IGL TV page, or visit us at <a href='http://www.twitch.tv/playigl'>www.twitch.tv/playigl</a></p></li>
                <li><p><strong>Does IGL offer prizes in its leagues?</strong><br />All leagues are free to play unless otherwise stated. Pay to play leagues or tournaments will often have prizes in products, services, and cash.</p></li>
                <li><p><strong>How can I get my match to be casted on IGL TV?</strong><br />Casted matches are often selected by IGL staff members. Contact your game's specific caster for more information.</p></li>
                <li><p><strong>Will IGL host LANs?</strong><br />Possible, but nothing planned as of yet.</p></li>
                <li><p><strong>When does IGL come out of beta?</strong><br />When the administration feels that the IGL site and products are ready enough for the open public, it will be launched.</p></li>
                <li><p><strong>Where can I view my upcoming and previous matches?</strong><br />Login, and click the blue button with your name in it. This will lead to the dashboard.</p></li>
                <li><p><strong>Where can I create or manage my team?</strong><br />Login, and click the blue button with your name in it. This will lead to the dashboard.</p></li>
                <li><p><strong>Where can I edit my profile?</strong><br />Login, and click the blue button with your name in it. This will lead to the dashboard.</p></li>
                <li><p><strong>How can I refer someone to IGL?</strong><br />Login, and click the blue button with your name in it. This will lead to the dashboard.</p></li>
                <li><p><strong>Where can I find the rules for a game or division?</strong><br />Find the division you are looking for with the Leagues navigation box, and select the Rules tab.</p></li>
                <li><p><strong>What is the Downloads section for?</strong><br />The Downloads section is to find and download popular files on IGL such as replays, configs, mods, etc. (Work in progress)</p></li>
            </ol>
        </div>
    </div>
</div>