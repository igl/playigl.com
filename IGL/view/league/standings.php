<div class='span8'>
    <div class='box' style='margin-top:12px'>
<?php
include('./view/league/subnavigation.php');
?>

<h1><?php echo $league_data['league_name']; ?> STANDINGS
<div style='margin:0px' class='input-append pull-right'>
    <form style='margin:0px' method='post' action='/standings/{$id}'>
        <select id='appendedInputButton' name='season' style="padding:2px; height:30px!important">
<?php
$count = 1;
while ($count <= $league_data['currentseason'])
{
    if($count == $_REQUEST['season'])
        echo "<option value='{$count}' selected='selected'>Season {$count}</option>";
    else
        echo "<option value='{$count}'>Season {$count}</option>";
    $count++;
}
?>
</select><input style='height:30px' type='submit' class='btn' type='button' value='Go'></form> </div></h1>
<table class='table table-stats table-condensed table-striped table-sortable'>      
<tr>
    <th width='440px'>Team</th><th><span rel='tooltip' title='Season'>S</span></th><th><span rel='tooltip' title='Wins'>W</span></th><th><span rel='tooltip' title='Losses'>L</span></th><th><span rel='tooltip' title='Rating Percentage Index'>RPI</span></th></tr>  
    
<?php
if(is_array($standings))
{
    $team = new team();
    foreach($standings as $key => $value)
    {
        $key++;
        echo "<tr><td>{$key}. {$value['team_name']}</td><td>{$value['season']}</td><td>{$value['wins']}</td><td>{$value['loses']}</td><td>{$value['rpi']}</td></tr>";
    }
}
echo "</table>";
echo "</div>";


echo "</div>";
?>