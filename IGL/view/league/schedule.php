<?php
if(IGL_PAGE!=true)die();
echo "<div class='span8'>";
include('./view/league/subnavigation.php');
echo "<div class='box' style='margin-top:12px'>";
echo "<h1>{$league_data['league_name']} SCHEDULE<div style='margin:0px' class='input-append pull-right'><form style='margin:0px' method='post' action='/schedule/id={$id}'><select style='width:90px' id='appendedInputButton' name='week' style='padding:2px; height:30px!important'>";
$count = 1;
$league = new league();
$currentweek = $league->currentweek($id);
while ($count <= $currentweek)
{
    echo "<option value='{$count}'";
    if($count == $_REQUEST['week'])
        echo "selected='selected'";
    echo ">Week {$count}</option>";
    $count++;
}
echo "</select><input type='submit' class='btn' type='button' value='Go'></form> </div></h1>";
echo "<table class='table table-stats table-condensed table-striped table-sortable'>";
echo "<tr><th>Date</th><th>Stream</th><th>Home</th><th>Away</th><th>Details...</th></tr>";
foreach($schedule as $key => $value)
    echo "<tr><td>".date('D m.d.y  g:ia',strtotime($value['officialdate']))."</td><td><img src='/img/live.png'></td><td>{$value['homename']} </td><td>{$value['awayname']}</td><td><a href='/match/id={$value['id']}'>MORE</a></td></tr>";
echo "</table>";
echo "</div>";
echo "</div>";