<?
if(IGL_PAGE!=true)
die();
?>
<div class="navbar navbar-division">
    <div class="navbar-inner">
    <?php
    echo "<img class='img-header' src='/img{$game_image}.png'>";
    ?>
    <ul class="nav">
        <?php
        $options = array(
            'standings',
            'schedule',
            'results',
            'stats',
            'rules',
            '');
        foreach( $options as $l )
            if( $_REQUEST['view'] == $l )
            {
                echo "<li class='active'><a href='/league/{$l}/{$_GET['id']}/".urlencode($league_data['game_name'])."/".urlencode($league_data['league_name'])."'>".strtoupper($l)."</a></li>";
            }
            else
            {
                echo "<li><a href='/league/{$l}/{$_GET['id']}/".urlencode($league_data['game_name'])."/".urlencode($league_data['league_name'])."'>".strtoupper($l)."</a></li>";
            }
        ?>
    </ul>
    </div>
    </div>