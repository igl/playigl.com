<?php
if(IGL_PAGE!=true)die();
echo "<div class='span8'>";
include('./view/league/subnavigation.php');
?>

<div class='box' style="margin-top:12px">
<h1><?php echo $league_data['league_name']; ?> RESULTS<div style='margin:0px' class='input-append pull-right'>
        <form style='margin:0px' method='post' action='/league/results/<?php echo $league_data['league_id']; ?>'>
            <select id='appendedInputButton' name='season' style="width:90px;padding:2px; height:30px!important">
<?php
$count = 1;
while ($count <= $league_data['currentseason'])
{
    echo "<option value='{$count}'";
    if($count == $season)
    {
        echo "selected='selected'";
    }
    echo ">Season {$count}</option>";
    $count++;
}
echo "</select>";
echo "<select id='appendedInputButton' name='week' style='width:100px;padding:2px; height:30px!important'>";

$count = 1;
while ($count <= $league_data['currentweek'])
{
    echo "<option value='{$count}'";
    if($count == $week)
    {
        echo "selected='selected'";
    }
    echo ">Week {$count}</option>";
    $count++;
}
?>
</select><input type='submit' class='btn' type='button' value='Go'></form> </div></h1>

<table class='table table-stats table-condensed table-striped table-sortable'>
<tr><th>Season</th><th>Week</th><th>Home</th><th>Away</th><th>Match Type</th><th>Winner</th><th>Loser</th></tr>

</table>
</div>
</div>