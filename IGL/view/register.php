<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<script>
    var root = "<?php echo $_SERVER['DOCUMENT_ROOT']; ?>";
    var referer = "<?php echo $_SERVER['HTTP_REFERER']; ?>";
</script>

<style>.tooltip-on { margin-left:10px; position:relative; top:4px; }</style>

<div id='register-error-0' class='alert alert-error' style='height:14px; line-height:14px; display:none; font-size:14px; margin-top:20px;'></div>

<div id="Registration-Step-1" class="clearfix" style="width:940px; padding:10px 20px;">
    <div class="well" style="height:250px; ">
        <div class="pull-left" style="width:440px;">
            <h2 style="font-weight:normal; margin:0 0 30px 0; padding-top:none;">Welcome! <small style="font-size:14px;">We just need a few things from you.</small></h2>
            <div style="margin-left:20px;">
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-user"></i></span>
                    <input id='register-username' style="height:20px !important;" class="span4" type="text" placeholder="Username">
                </div>
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-envelope-alt"></i></span>
                    <input id='register-email' style="height:20px !important;" class="span4" type="text" placeholder="Email">
                </div>
                <div class="input-prepend input-append">
                    <span class="add-on"><i class="icon-lock"></i></span>
                    <input id='register-password' style="height:20px !important; width:260px;" class="span4" type="password" placeholder="********">
                    <span class="add-on" style="cursor:pointer;" onclick="if (document.getElementById('register-password').type == 'password') { document.getElementById('register-password').type = 'text'; } else { document.getElementById('register-password').type = 'password'; }"><i class="icon-eye-open"></i></span>
                </div>
            </div>
            <div id='register-error-1' class='alert alert-error pull-left' style='height:14px; line-height:14px; display:none; font-size:14px; margin-left:20px; margin-top:20px; width:180px;'></div>
            <div onclick='Registration.checkInformation();' class="pull-right btn btn-red" style="margin-right:90px; margin-top:20px;">Register</div>
        </div>
        <div class="pull-left" style="height:250px; border-left:2px solid #ccc;"></div>
        <div class="pull-right" style="width:410px; padding-left:30p;">
            <h2 style="font-weight:normal; margin:0 0 30px 0; padding-top:none;"><small style="font-size:14px;">Or, register using a social network.</small></h2>
            <div style="margin-left:-20px;">
                <div class="social-connect">
                    <a href="#" class="connect-btn steam" onclick="Registration.connectSteam()"><div class="icon-wrap"><img class="icon" src="../img/register/icon-steam.png"></div> <span>Connect With Steam</span></a>
                    <a href="#" class="connect-btn twitch" onclick="Registration.connectTwitch()"><div class="icon-wrap"><img class="icon" src="../img/register/icon-twitch.png"></div> <span>Connect With Twitch</span></a>
                    <a href="#" class="connect-btn facebook" onclick="Registration.connectFacebook()"><div class="icon-wrap"><img class="icon" src="../img/register/icon-facebook.png"></div> <span>Connect With Facebook</span></a>
                    <a href="#" class="connect-btn twitter" onclick="Registration.connectTwitter()"><div class="icon-wrap"><img class="icon" src="../img/register/icon-twitter.png"></div> <span>Connect With Twitter</span></a>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="Registration-Step-2" class="clearfix" style="width:940px; padding:10px 20px; display:none;">
    <div class="well" style="height:320px; ">
        <div class="pull-left" style="width:440px;">
            <h2 style="font-weight:normal; margin:0 0 30px 0; padding-top:none;">Almost done! <small style="font-size:14px;">Help us complete your profile.</small></h2>
            <div style="margin-left:20px;">
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-user"></i></span>
                    <input id='register-username2' style="height:20px !important;" class="span4" type="text" placeholder="Username">
                </div>
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-envelope-alt"></i></span>
                    <input id='register-email2' style="height:20px !important;" class="span4" type="text" placeholder="Email">
                </div>
                <div class="input-prepend input-append">
                    <span class="add-on"><i class="icon-lock"></i></span>
                    <input id='register-password2' style="height:20px !important; width:260px;" class="span4" type="password" placeholder="********">
                    <span class="add-on" style="cursor:pointer;" onclick="if (document.getElementById('register-password2').type == 'password') { document.getElementById('register-password2').type = 'text'; } else { document.getElementById('register-password2').type = 'password'; }"><i class="icon-eye-open"></i></span>
                </div>
                <div class="input-prepend">
                    <span class="add-on"></span>
                    <input id='register-firstname' style="height:20px !important;" class="span4" type="text" placeholder="First Name">
                </div>
                <div class="input-prepend">
                    <span class="add-on"></span>
                    <input id='register-lastname' style="height:20px !important;" class="span4" type="text" placeholder="Last Name">
                </div>
            </div>
            <div id='register-error-2' class='alert alert-error pull-left' style='height:14px; line-height:14px; display:none; font-size:14px; margin-left:20px; margin-top:20px; width:180px;'></div>
            <div onclick='Registration.checkDetailedInformation();' class="pull-right btn btn-red" style="margin-right:90px; margin-top:20px;">Done</div>
        </div>
        <div class="pull-left" style="height:320px; border-left:2px solid #ccc;"></div>
        <div class="pull-right" style="width:410px; padding-left:30p;">
            <h2 style="font-weight:normal; margin:0 0 30px 0; padding-top:none;"><small style="font-size:14px;">Social Network Connected.</small></h2>
            <div style="margin-left:-20px;">
                <div class="social-connect" style='opacity:0.1; '>
                    <a href="#" class="connect-btn steam" onclick="Registration.connectSteam()"><div class="icon-wrap"><img class="icon" src="../img/register/icon-steam.png"></div> <span>Connect With Steam</span></a>
                    <a href="#" class="connect-btn twitch" onclick="Registration.connectTwitch()"><div class="icon-wrap"><img class="icon" src="../img/register/icon-twitch.png"></div> <span>Connect With Twitch</span></a>
                    <a href="#" class="connect-btn facebook" onclick="Registration.connectFacebook()"><div class="icon-wrap"><img class="icon" src="../img/register/icon-facebook.png"></div> <span>Connect With Facebook</span></a>
                    <a href="#" class="connect-btn twitter" onclick="Registration.connectTwitter()"><div class="icon-wrap"><img class="icon" src="../img/register/icon-twitter.png"></div> <span>Connect With Twitter</span></a>
                </div>
            </div>
        </div>
    </div>
</div>