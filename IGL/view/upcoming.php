<?php if(PAGE!="index" || !isset($_SESSION['playerid'])) { die('You must be logged in to view this page.'); } ?>

<?php
$schedule = new schedule();
$game = new game();
$league = new league();
$team = new team();
$matchtype = new matchtype();
?>

<div id="standard_content_box" style="width:576px;">
    <?php
        $todaysmatches = $schedule->todaysmatches($_SESSION['playerid']);

        if(count($todaysmatches)==0)
            echo "You do not have any matches today.";
        else
        {
            $date=date("M j, Y",strtotime($todaysmatches[0]['officialdate']));
            $time=date('H:i T',strtotime($todaysmatches[0]['officialdate']));
            $match = $todaysmatches[0];
    ?>

    <table>
        <tr>
            <td>
                <table>
                    <tr><td>Date: <? echo $date; ?></td></tr>
                    <tr><td>Time: <? echo $time; ?></td></tr>
                    <tr><td>Game: <? echo $game->name($league->game($match['league'])); ?></td></tr>
                    <tr><td>League:<? echo $league->name($match['league']); ?></td></tr>
                    <tr><td>Map: <? echo $match['map']; ?></td></tr>
                    <tr><td>Home: <? echo $team->name($match['home'],true); ?>(<? echo $team->wins($match['home'],$match['league'],$match['season'])."-".$team->loses($match['home'],$match['league'],$match['season']);?>)</td></tr>
                    <td>Away: <? echo $team->name($match['away'],true); ?>(<? echo $team->wins($match['home'],$match['league'],$match['season'])."-".$team->loses($match['home'],$match['league'],$match['season']); ?>)</td></tr>
                </table>
            </td>
            <td><img src="img/launch.jpg" /></td>
        </tr>    
    </table>
    <table>
    <tr><td>Date</td><td>Time</td><td>Game</td><td>League</td><td>Map</td><td>Home</td><td>&nbsp;</td><td>Away</td><td>&nbsp;</td><td>&nbsp;</td></tr>
    <?php
        $isFirst = true;
        foreach($todaysmatches as $match)
        {
            if($isFirst)
            {
                $isFirst = false;
                continue;
            }

            $date=date("M j, Y",strtotime($match['officialdate']));
            $time=date('H:i T',strtotime($match['officialdate']));

            echo <<< EOF
                <tr>
                    <td>{$date}</td>
                    <td>{$time}</td>
                    <td>{$game->name($league->game($match['league']))}</td>
                    <td>{$league->name($match['league'])}</td>
                    <td>{$match['map']}</td>
                    <td>{$team->name($match['home'],true)}</td>
                    <td>({$team->wins($match['home'],$match['league'],$match['season'])}-{$team->loses($match['home'],$match['league'],$match['season'])})</td>
                    <td>{$team->name($match['away'],true)}</td>
                    <td>({$team->wins($match['home'],$match['league'],$match['season'])}-{$team->loses($match['home'],$match['league'],$match['season'])})</td>
                    <td>More</td>
                </tr>
EOF;
        }

        echo '</table>';
        unset($match);
        }
    ?>
</div>
<div id="standard_content_box" style="width:576px;">
    Upcoming Matches...

    <?php
        $nextmatches = $schedule->nextmatches($_SESSION['playerid']);

        if(count($nextmatches)==0)
            echo "You do not have any future matches scheduled.";
	else
	{
            echo '<table>';
            echo '<tr><td>Date</td><td>Time</td><td>Game</td><td>League</td><td>Map</td><td>Home</td><td>&nbsp;</td><td>Away</td><td>&nbsp;</td><td>&nbsp;</td></tr>';
	
            foreach($nextmatches as $match)
            {
		if($match['officialdate']=='0000-00-00 00:00:00')
                {
                    $date=date("M j, Y",strtotime($match['scheduleddate']));
                    $time=date('H:i T',strtotime($match['scheduleddate']));
                }
                else
                {
                    $date=date("M j, Y",strtotime($match['officialdate']));
                    $time=date('H:i T',strtotime($match['officialdate']));
                }
                
                echo <<< EOF
                    <tr>
                        <td>{$date}</td>
                        <td>{$time}</td>
                        <td>{$game->name($league->game($match['league']))}</td>
                        <td>{$league->name($match['league'])}</td>
                        <td>{$match['map']}</td>
                        <td>{$team->name($match['home'],true)}</td>
                        <td>({$team->wins($match['home'],$match['league'],$match['season'])}-{$team->loses($match['home'],$match['league'],$match['season'])})</td>
                        <td>{$team->name($match['away'],true)}</td>
                        <td>({$team->wins($match['home'],$match['league'],$match['season'])}-{$team->loses($match['home'],$match['league'],$match['season'])})</td>
                        <td>More</td>
                    </tr>
EOF;
            }
            echo '</table>';
            unset($match);
	}
    ?>
</div>
<div id="standard_content_box" style="width:576px;">
    Last 5 matches...
    
    <?php
        #####
        #Previous Matches
        #####
        
        $previousmatches = $schedule->previousmatches($_SESSION['playerid']);

        if(count($previousmatches)==0)
            echo "You have not yet completed any matches.";
	else
	{	
            echo '<table>';
            echo '<tr><td>Date</td><td>Time</td><td>Game</td><td>League</td><td>Map</td><td>Home</td><td>&nbsp;</td><td>Away</td><td>&nbsp;</td><td>&nbsp;</td></tr>';
	
            foreach($previousmatches as $match)
            {
                if($match['officialdate']=='0000-00-00 00:00:00')
                {
                    $date=date("M j, Y",strtotime($match['scheduleddate']));
                    $time=date('H:i T',strtotime($match['scheduleddate']));
                }
                else
                {
                    $date=date("M j, Y",strtotime($match['officialdate']));
                    $time=date('H:i T',strtotime($match['officialdate']));
                }
                
                echo <<< EOF
                    <tr>
                        <td>{$date}</td>
                        <td>{$time}</td>
                        <td>{$game->name($league->game($match['league']))}</td>
                        <td>{$league->name($match['league'])}</td>
                        <td>{$match['map']}</td>
                        <td>{$team->name($match['home'],true)}</td>
                        <td>({$team->wins($match['home'],$match['league'],$match['season'])}-{$team->loses($match['home'],$match['league'],$match['season'])})</td>
                        <td>{$team->name($match['away'],true)}</td>
                        <td>({$team->wins($match['home'],$match['league'],$match['season'])}-{$team->loses($match['home'],$match['league'],$match['season'])})</td>
                        <td>More</td>
                    </tr>
EOF;
            }
            echo '</table>';
	}
    ?>
</div>