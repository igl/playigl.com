<?php
/*
 * @copyright 2013 PlayIGL.com
 * @Author Kevin Langlois
 */
require('./model/showmatch.class.php');
$showmatch = new showmatch();
$match = $showmatch->get($_REQUEST['id']);
?>
<div id="showmatch_data" data-showmatch-id="<?php echo $_REQUEST['id']; ?>" data-player-id="<?php echo $_SESSION['playerid']; ?>"></div>
<div class="span12 event event-showmatch">
                <div class="banner">
                    <img src="/img/showmatch-banner-temp.png" />
                    <div class="text-inner">
                        <div class="left">
                            <h1><?php echo $match['name']; ?></h1>
                            <strong>Match Dates:</strong> <?php echo date(DATE_RFC822, $match['startdatetime']); ?>
                        </div>
                        <div class="right">
                            <div class="item"><strong>Registration:</strong> Invite Only</div>
                            <div class="item"><strong>Max Teams:</strong> 4</div>
                            <div class="item"><strong>Status:</strong> Upcoming</div>
                        </div>
                    </div>
                </div>
                <div class="nav-wrap">
                    <ul class="nav nav-tabs nav-pills">
                        <li class="active"><a data-toggle="tab" href="#info">Event Info</a></li>
                        <li><a data-toggle="tab" href="#matches">Matches</a></li>
                        <li><a data-toggle="tab" href="#streams">Streams</a></li>
                    </ul>
                </div>
                <div class="tab-content">
                     <div class="tab-pane active" id="info">
                         <div class="match">
                             <div class="date">Tuesday, March 19th @ 5:00PM EST</div>
                             <div class="team">
                                 <img class="team-img" src="/img/showmatch-team.png" />
                                 <div class="name">Curse Gaming <img class="flag" src="http://dev.playigl.com/img/flags/ca.png" /></div>
                             </div>
                             <div class="score">
                                 <div class="vs">VS</div>
                                 <div class="win">1</div>
                                 <div class="loss">0</div>
                             </div>
                             <div class="team">
                                 <img class="team-img" src="/img/showmatch-team.png" />
                                 <div class="name">Curse Gaming <img class="flag" src="http://dev.playigl.com/img/flags/ca.png" /></div>
                             </div>

                             <div class="game clearfix">
                                 <div class="left loss">
                                     L
                                 </div>
                                 <div class="middle">
                                     <div class="title">Game 1</div>
                                     de_dust2
                                 </div>
                                 <div class="right win">
                                     W
                                 </div>
                             </div>
                             <div class="game clearfix">
                                 <div class="left win">
                                     W
                                 </div>
                                 <div class="middle">
                                     <div class="title">Game 2</div>
                                     de_aztec
                                 </div>
                                 <div class="right loss">
                                     L
                                 </div>
                             </div>
                         </div>
                         <div class="caster-wrap">
                             <h2>Casters</h2>
                             <div class="caster">
                                 <img src="http://dev.playigl.com/img276.png" class="pic" />
                                 <a href="#" class="user"><h3>hopesix</h3></a>
                                 <a href="#" class="twitter">@jassibacha</a>
                             </div>
                             <div class="caster">
                                 <img src="http://dev.playigl.com/img276.png" class="pic" />
                                 <a href="#" class="user"><h3>hopesix</h3></a>
                                 <a href="#" class="twitter">@jassibacha</a>
                             </div>
                             <div class="caster">
                                 <img src="http://dev.playigl.com/img276.png" class="pic" />
                                 <a href="#" class="user"><h3>hopesix</h3></a>
                                 <a href="#" class="twitter">@jassibacha</a>
                             </div>
                         </div>
                     </div>
                     <div class="tab-pane" id="matches">matches</div>
                     <div class="tab-pane" id="streams">streams</div>
                    
                </div>
            </div> 