<div class='span8'>
<?php
    include('./view/league/subnavigation.php');
?>
    <div class='box' style='margin-top:12px'>
        <h1>ABOUT THE GAME</h1>
        <div class='inner_box' style='margin-top:10px'><?php echo $league_data['about']; ?></div>
    </div>

    <div class='box' style='margin-top:12px'>
        <h1>NEWS</h1>
        <?php
            if(is_array($league['news']))
            {
                foreach($league['news'] as $key => $value)
                {
                    echo <<< EOF
                        <div class='inner_box' style='margin-top:10px'>
                            <h3>{$value['title']}</h3>
                            <div align='right'><strong>Posted: </strong>{$value['dateposted']} by {$value['poster']}</div>
                            <p>{$value['content']}</p>
                        </div>
EOF;
                }
            }
        ?>
    </div>
</div>