<?php
/*
 * @copyright 2013 PlayIGL.com
 * @Author Darryl Allen
 */

    if (!isset($team) || is_null($team) || !$team['success'])
    {
     ?>
            <div class="span8">
                <div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">x</button>Team not found!</div>
            </div>
    <?
    } else {
?>
<div class="span8">
    <div class="box team">
        <h1>TEAM RESULTS - <?php echo $team['team']['team_name']; ?></h1>
        <div class="inner_box">
            <div class="row">
                <div class="span3">
                    <a href="/team/<?php echo $id; ?>-<?php echo $team['team']['team_name']; ?>"><img alt="<?php echo $team['team']['team_name']; ?>" class="img-rounded" src="/img<?php echo $team['team']['team_logo']; ?>" /></a>
                </div>
                <div class="span4">
                    <table class="table table-striped table-condensed">
                        <tr><td><span class="tooltip-on" title="Last 10 matches">L10:</span></td><td colspan="2"><?php echo "{$last_10['wins']} - {$last_10['losses']}"; ?></td></tr>
                        <tr><td><span class="tooltip-on" title="Skill based on opponent strength">RPI:</span></td><td colspan="2"><?php echo $team['team']['team_rpi']; ?></td></tr>
                        <tr><td><span class="tooltip-on" title="Winning Percentage">PCT:</span></td><td colspan="2"><?php echo $team_record['pct']; ?></td></tr>
                        <!--<tr><td>League Record:</td><td><?php echo "{$league_record['wins']} - {$league_record['losses']}"; ?></td><td>(<?php if($league_record['pct']) echo $league_record['pct']; else echo "0.000"; ?>)</td></tr>--->
                        <tr><td>Ladder Record:</td><td><?php echo "{$ladder_record['wins']} - {$ladder_record['losses']}"; ?></td><td>(<?php if($ladder_record['pct']) echo $ladder_record['pct']; else echo "0.000"; ?>)</td></tr>
                        <tr><td>Tournament Record:</td><td><?php echo "{$tournament_record['wins']} - {$tournament_record['losses']}"; ?></td><td>(<?php if($tournament_record['pct']) echo $tournament_record['pct']; else echo "0.000"; ?>)</td></tr>
                    </table>
                </div>
            </div>
             </div>
             <div class="inner_box">
                <table id='ladder_team' class='table table-condensed table-striped table-bordered table-stats'>
                    <thead>
                        <tr><th>Date</th><th colspan="3">Details</th><th>Opponent</th><th>Score</th><th>Points</th></tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach($team_results as $r)
                        {
                            $my_score = explode("|",$r['my_score']);
                            $opponent_score = explode("|",$r['opponent_score']);
                            $maps = explode("|", $r['maps']);

                            $map_info = "<ul class=\"unstyled\">";
                            foreach($maps as $i => $map)
                                $map_info .= "<li><div class=\"pull-left\" style=\"width:100px;\">{$map}:</div><div class=\"pull-left\"> {$my_score[$i]} - {$opponent_score[$i]} </div></li>";
                            $map_info .= "</ul>";

                            $case = 'green';
                            $icon = 'icon-arrow-up';
                            if(array_sum($my_score) < array_sum($opponent_score))
                            {
                                $case = 'red';
                                $icon = 'icon-arrow-down';
                            }

                            if($r['comp'] == 'Ladder')
                            {
                                $point_details = "<i class='{$icon}'></i>{$r['my_points']} ({$r['my_diff']})</a>";
                            }

                            echo "<tr><td><a href='/match/{$r['match_id']}-".urlencode($r['my_name'])."-vs-".urlencode($r['opponent_name'])."'>".date('Y-m-d',$r['date'])."</a></td><td>{$r['game_name']}</td><td>{$r['comp']}</td><td>{$r['comp_name']} {$r['format']}</td><td><a class='tooltip-on' title='{$r['opponent_name']}' href='/team/results/{$r['opponent_id']}'>{$r['opponent_tag']}</a></td><td><a class='popover-on' data-html='true' data-title='Match Details' data-content='{$map_info}' style='color:{$case}' href='/match/{$r['match_id']}-".urlencode($r['my_name'])."-vs-".urlencode($r['opponent_name'])."'>".array_sum($my_score)." - ".array_sum($opponent_score)."</a></td><td><a href='/match/{$r['match_id']}-".urlencode($r['my_name'])."-vs-".urlencode($r['opponent_name'])."'>{$point_details}</td></tr>";
                        }
                        ?>
                    </tbody>
                </table>

            </div>

            
                
         </div><!-- /box -->
         
<div class="modal hide" id="Modal" tabindex="-1" role="dialog" aria-labelledby="teamModalLabel" aria-hide="true">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hide="true"><i class='icon-remove'></i></button>
<h3 id="ModalLabel">LOADING</h3>
</div>
<div class="modal-body" id="ModalBody">
<div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div>
</div>
<div class="modal-footer">
<button id="ModalButtonClose" class="btn" data-dismiss="modal" aria-hide="true">CLOSE</button>
<button id="ModalButton" class="btn btn-primary">SUBMIT</button>
</div>
</div>
</div>

<div id="server-data" style="display:none;" data-player-id="<?php echo $_SESSION['playerid']; ?>" data-team-id="<?php echo $id; ?>"></div>
<?php } ?>