<?php
  if (!isset($team) || is_null($team) || !$team['success']) {
?>
  <div class="span8">
    <div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">x</button>Team not found!</div>
  </div>
<?php
  } else {
?>
  <div class="span8">
    <div class="box team">
      <h1>TEAM PROFILE - <?php echo $team['team']['team_name']; ?></h1>
      <div class="inner_box">
        <div class="row">
          <div class="span">
            <img alt="<?php echo $team['team']['team_name']; ?>" class="img-rounded" src="/img<?php echo $team['team']['team_logo']; ?>" />
          </div>
          <div class="span3">
            <ul>
              <?php
            foreach ($team_html as $key => $val)
                echo "<li>{$val}</li>";
            foreach ($team_ladders as $key => $val)
                echo "<li>{$val}</li>";
              ?>
            </ul>
          </div>
          <div class="span"><h2 class="tooltip-on" title="Global team rating. Based on the Rating Percentage index."><?php echo $team['team']['team_rpi']; ?></h2><div id="follow_button_<?php echo $id ?>"></div></div>
        </div>
      </div><!-- /inner_box -->
      <?php
      if ($team['team']['team_biography']) {
        echo "<div class='inner_box'>";
        echo $team['team']['team_biography'];
        echo "</div>";
      }
      
      echo "<div class='inner_box'>";

      if (LEAGUEADMIN || in_array($_SESSION['playerid'], array($team['team']['team_captain_id'], $team['team']['team_alternate_id']))) {
        echo "<div style='width:100%'><i onclick='editTeam({$id})' class='close edit_team icon-edit tooltip-on' title='Manage Roster'></i></div>";
      }

      echo "<ul class='roster'>";

      foreach ($team['team']['team_roster'] as $value) {
        $username = truncate($value['player_username'], 9);
        $firstname = truncate($value['player_firstname'], 10);
        $lastname = truncate($value['player_lastname'], 10);
        echo "<li id='player_{$value['player_id']}'>";
        if ($value['player_id'] != $team['team']['team_captain_id']) {
          echo "<i class='editRoster close tooltip-on' title='Remove player' onclick='removePlayer({$value['player_id']},{$id})'></i>";
        }
        echo <<< EOF
          <p><a href='/player/{$value['player_username']}'><img class='tooltip-on' title='Active player' src='/img/roster_filled.png'></a><br>{$firstname}</p>
          <h4><a href='/player/{$value['player_username']}'>{$username}</a></h4>
          <p>{$lastname}</p>
          </li>
EOF;
        if ($value['player_id'] == $_SESSION['playerid'])
          $onteam = true;
      }
/*
 * We dont display invites anymore, as some teams send out dozens.
      foreach ($team['team']['invites'] as $key => $value) {
        $remove = LEAGUEADMIN || in_array($_SESSION['playerid'], array($team['team']['team_captain_id'], $team['team']['team_alternate_id'])) ? "<i class='editRoster close tooltip-on' title='Remove invite' onclick='removePlayer({$value['player']},{$id})'></i>" : "";
        echo <<< EOF
          <li id='player_{$value['player']}'>
          {$remove}
            <p><a href='/player/{$value['username']}'><img class='tooltip-on' title='Invite pending' src='/img/roster_empty.png'></a></p>
            <p>{$value['firstname']}</p>
            <h4><a href='/player/{$value['username']}'>{$value['username']}</a></h4>
            <p>{$value['lastname']}</p>
          </li>
EOF;
      }*/
      
      $num_invites = count($team['team']['invites']);
      $open_slots = (6 - (count($team['team']['team_roster']) + $num_invites ));

      if ($open_slots < 1)
        $open_slots = 1;

      $count = 0;
      while ($count < $open_slots) {
        if (in_array($_SESSION['playerid'], array($team['team']['team_captain_id'], $team['team']['team_alternate_id']))) {
          echo <<< EOF
            <li id='open_{$count}'>
              <p><img class='tooltip-on' title='Open slot' src='/img/roster_empty.png'></p>
              <p><button class='btn btn-primary join_team' onclick='editTeam({$team['team']['team_id']})'>ADD</button></p>
            </li>
EOF;
        } elseif (LEAGUEADMIN) {
          echo "<li id='open_{$count}'>";
          echo "<p><img class='tooltip-on' title='Open slot' src='/img/roster_empty.png'></p>";
          echo "<p><button class='btn btn-primary join_team' onclick='editTeam({$team['team']['team_id']})'>ADD <i class='icon-cog'></i></button></p>";
          if (!$onteam) {
            if ($_SESSION['communityid'] > 0 || $team['team']['team_game_steamauth'] == 0)
              echo "<p><button style='margin-top:6px' class='btn btn-primary join_team' onclick='joinTeam({$team['team']['team_id']},{$_SESSION['playerid']},{$count})'>JOIN</button></p>";
          }
          echo "</li>";
        }
        elseif (isset($_SESSION['playerid']) && $team['team']['team_open'] == 1) {
          if (!$onteam) {
            if ($_SESSION['communityid'] > 0 || $team['team']['team_game_steamauth'] == 0) {
              echo <<< EOF
                <li id='open_{$count}'>
                  <p><img class='tooltip-on' title='Open slot' src='/img/roster_empty.png'></p>
                  <p><button class='btn btn-primary join_team' onclick='joinTeam({$team['team']['team_id']},{$_SESSION['playerid']},{$count})'>JOIN</button></p>
                </li>
EOF;
            } else {
              echo <<< EOF
                <li id='open_{$count}'>
                  <p><img class='tooltip-on' title='Open slot' src='/img/roster_empty.png'></p>
                  <p><button class='btn btn-primary disabled' rel='tooltip' title='You must first authenticate with Steam'>JOIN</button></p>
                </li>
EOF;
            }
          }
        }
        $count++;
      }
      echo "</ul></div>";

      if (is_array($team['matches']['next'])) {
        echo <<< EOF
          <div class='inner_box'>
            <h5>Next Match</h5>
            <table>
              <tr>
                <th>Game</th>
                <th>League/Ladder</th>
                <th>S</th>
                <th>W</th>
                <th>VS</th>
                <th>Map</th>
                <th>Date</th>
              </tr>
EOF;

        foreach ($team['matches']['next'] as $key => $value) {
          if ($value['officialdate'] == '0000-00-00 00:00:00') {
            $date = date('Y-m-d', strtotime($value['scheduleddate']));
          } else {
            $date = date('Y-m-d', strtotime($value['officialdate']));
          }

          echo <<< EOF
            <tr>
              <td>{$this->game->name($value['game'])}</td>
              <td>{$this->league->name($value['league'])}</td>
              <td>{$value['season']}</td>
              <td>{$value['week']}</td>
              <td>{$this->team->name($value['opponent'], true)}</td>
              <td>{$value['map']}</td>
              <td>{$date}</td>
            </tr>
EOF;
        }
        echo "</table>";
        echo "</div>";
      }

      if (is_array($team['matches']['previous'])) {
        echo <<< EOF
          <div class='inner_box'>
            <h5>Last 5</h5>
              <table>
                <tr>
                  <th>Game</th>
                  <th>Week</th>
                  <th>League/Ladder</th>
                  <th>Season</th>
                  <th>Versus</th>
                  <th>Map</th>
                  <th>Date</th>
                </tr>
EOF;

        foreach ($team['matches']['previous'] as $key => $value) {
          $official_date = date('Y-m-d', $value['officialdate']);
          echo <<< EOF
            <tr>
              <td>{$this->game->name($value['game'])}</td>
              <td>{$value['week']}</td>
              <td>{$this->league->name($value['league'])}</td>
              <td>{$value['season']}</td>
              <td>{$this->team->name($value['opponent'], true)}</td>
              <td>{$value['map']}</td>
              <td>{$official_date}</td>
            </tr>
EOF;
        }
        echo "</table>";
        echo "</div>";
      }

      ?>
      <div class="inner_box">
        <table id='ladder_team' class='table table-condensed table-striped table-bordered table-stats'>
          <thead>
            <tr><th colspan='7'>Last 5 <small>(<a href="/team/results/<?php echo $id; ?>">View All Results</a>)</small></th></tr>
            <tr><th>Date</th><th colspan="3">Details</th><th>Opponent</th><th>Score</th><th>Points</th></tr>
          </thead>
          <tbody>
            <?php
            foreach ($team_results as $r) {
              $my_score = explode("|", $r['my_score']);
              $opponent_score = explode("|", $r['opponent_score']);
              $maps = explode("|", $r['maps']);

              $map_info = "<ul class=\"unstyled\">";
              foreach ($maps as $i => $map)
                $map_info .= "<li><div class=\"pull-left\" style=\"width:100px;\">{$map}:</div><div class=\"pull-left\"> {$my_score[$i]} - {$opponent_score[$i]} </div></li>";
              $map_info .= "</ul>";

              $case = 'green';
              $icon = 'icon-arrow-up';
              if (array_sum($my_score) < array_sum($opponent_score)) {
                $case = 'red';
                $icon = 'icon-arrow-down';
              }

              if ($r['comp'] == 'Ladder') {
                $point_details = "<i class='{$icon}'></i>{$r['my_points']} ({$r['my_diff']})</a>";
              }

              echo "<tr><td><a href='/match/{$r['match_id']}-" . urlencode($r['my_name']) . "-vs-" . urlencode($r['opponent_name']) . "'>" . date('Y-m-d', $r['date']) . "</a></td><td>{$r['game_name']}</td><td>{$r['comp']}</td><td>{$r['comp_name']} {$r['format']}</td><td><a href='/team/results/{$r['opponent_id']}' class='tooltip-on' title='{$r['opponent_name']}'>{$r['opponent_tag']}</a></td><td><a class='popover-on' data-html='true' data-title='Match Details' data-content='{$map_info}' style='color:{$case}' href='/match/{$r['match_id']}-" . urlencode($r['my_name']) . "-vs-" . urlencode($r['opponent_name']) . "'>" . array_sum($my_score) . " - " . array_sum($opponent_score) . "</a></td><td><a href='/match/{$r['match_id']}-" . urlencode($r['my_name']) . "-vs-" . urlencode($r['opponent_name']) . "'>{$point_details}</td></tr>";
            }

            ?>
          </tbody>
        </table>

      </div>
      <?php
      if (is_array($team['history'])) {
        echo <<< EOF
          <div class='inner_box'>
            <h5>HISTORY</h5>
            <table class='table table-condensed table-striped'>
              <tr>
                <th width='400px'>Player</th>
                <th>Joined</th>
                <th>Left</th>
                <th></th>
              </tr>
EOF;

        foreach ($team['history'] as $key => $value)
          echo "<tr><td><a href='/player/{$value['player_username']}'>{$value['player_username']}</a></td><td>{$value['joindate']}</td><td>{$value['leavedate']}</td><td></td></tr>";

        echo "</table>";
        echo "</div>";
      }

      ?>

      <div class="inner_box">
        <form id="comment_form">
          <input type="hidden" id="viewer_id" name="viewer_id" value="<?php echo $_SESSION['playerid']; ?>">
          <input type="hidden" id="page_id" name="page_id" value="<?php echo $_GET['id']; ?>">
          <input type="hidden" id="page_name" name="page_name" value="team">
          <textarea id="comment" name="comment" style="width:564px;height:60px" ></textarea>
          <div class="full" style="height:30px">
            <small class="pull-left">You have <span id="chars"></span> characters left.</small>
            <button id="button_post" type="button" onclick="commentPost('team',<?php echo $id; ?>)" class="btn btn-info pull-right">Post Comment</button>
          </div>
        </form>

        <ul id="team_comments" class="clear media-list">

        </ul>
      </div>
    </div><!-- /box -->

    <div class="modal hide" id="Modal" tabindex="-1" role="dialog" aria-labelledby="teamModalLabel" aria-hide="true">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hide="true"><i class='icon-remove'></i></button>
        <h3 id="ModalLabel">LOADING</h3>
      </div>
      <div class="modal-body" id="ModalBody">
      </div>
      <div class="modal-footer">
        <button id="ModalButtonClose" class="btn" data-dismiss="modal" aria-hide="true">CLOSE</button>
        <button id="ModalButton" class="btn btn-primary">SUBMIT</button>
      </div>
    </div>
  </div>

  <div id="server-data" style="display:none;" data-player-id="<?php echo $_SESSION['playerid']; ?>" data-team-id="<?php echo $id; ?>"></div>
  <script id="team_builder_template" type="text/template"><?php echo $team_builder_template; ?></script>
<?php } ?>