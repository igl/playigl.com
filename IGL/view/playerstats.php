<?php
set_time_limit(60);
/*
 * @copyright 2012 PlayIGL.com
 * @author Darryl Allen
 */
echo <<< EOF
    <div class="box">
        <h1 style="margin-bottom:12px;">PLAYER STATS - <a rel="tooltip" title="{$stats['schedule']['home_name']}" href="/team/{$stats['schedule']['home_id']}-{$stats['schedule']['home_name']}">{$stats['schedule']['home_tag']}</a> vs <a rel="tooltip" title="{$stats['schedule']['away_name']}" href="/team/{$stats['schedule']['away_id']}-{$stats['schedule']['away_name']}">{$stats['schedule']['away_tag']}</a></h1>
        <h4><a rel="tooltip" title="{$stats['player']['ingame_name']}" href="/player/{$stats['player']['username']}">{$stats['player']['username']}</a> <img src="/img/flags/{$stats['player']['country']}.png"></h4>
EOF;

foreach($stats['round'] as $test => $round)
{
    echo <<< EOF
        <div class="inner_box">
            <h5>Round</h5>
            <table class="table table-condensed table-striped">
                <tr><th colspan="2"><b>Event Summary</b></th></tr>
EOF;
    
    foreach($round['events'] as $key => $value)
        echo "<tr><td width='520px'>{$key}</td><td>{$value}</td></tr>";
    
    echo <<< EOF
            </table>
        <div>
        <div class="pull-left" style="width:50%">
            <table class="table table-condensed table-striped">
                <tr><th colspan="2"><b>Kills</b></th></tr>
EOF;
        
    foreach($round['kills'] as $key => $value)
    {
        echo "<tr><td width='130px'><a rel='tooltip' title='{$value['weapon_name']}'><img src='/img/weapons/{$value['weapon_code']}.png'></a>";
        
        if($value['headshot'])
            echo "&nbsp;<i class='icon-screenshot' rel='tooltip' title='Headshot'></i> ";
        
        echo "</td><td><a rel='tooltip' title='{$value['ingame_name']}' href='/playerstats/{$id}/{$value['playerid']}'>{$value['username']}</a></td></tr>";
    }
    
    echo <<< EOF
            </table>
        <div>
        <div class="pull-left" style="width:50%">
            <table class="table table-condensed table-striped">
                <tr><th colspan="2"><b>Deaths</b></th></tr>
EOF;

    foreach($round['deaths'] as $key => $value)
    {
        echo "<tr><td width='130px'><a rel='tooltip' title='{$value['weapon_name']}'><img src='/img/weapons/{$value['weapon_code']}.png'></a>";
        
        if($value['headshot'])
            echo "<i class='icon-screenshot' rel='tooltip' title='Headshot'></i>";
        
        echo "</td><td><a rel='tooltip' title='{$value['ingame_name']}' href='/playerstats/{$id}/{$value['playerid']}'>{$value['username']}</a></td></tr>";
    }
    
    echo <<< EOF
                </table>
            </div>
        </div>
    </div>
EOF;
}

echo "</div>";
?>