<?php
/*
 * @copyright 2013 PlayIGL.com
 * @Author Kevin Langlois
 */
require('./model/showmatch.class.php');
$showmatch = new showmatch();
$match = $showmatch->get($_REQUEST['id']);
?>
<div id="showmatch_data" data-showmatch-id="<?php echo $_REQUEST['id']; ?>" data-player-id="<?php echo $_SESSION['playerid']; ?>"></div>
<div class="span12 event event-showmatch">
                <div class="banner">
                    <?php if ($_REQUEST['id'] == 7) { 
                        $ss_banner = "/img/ss2_banner.png" ;       
                    }else {
                      $ss_banner = "/img/ss_banner.png" ; 
                    }
                    ?>
                    <img src="<?php echo $ss_banner; ?>" />
                    <div class="text-inner">
                        <div class="left">
                             <?php if ($_REQUEST['id'] == 7) { 
                        $match['startdatetime'] = "1373821200"; 
                        $match['maxteams'] = '10';
                    }else {
                      $match['startdatetime'] = "Completed"; 
                      $match['maxteams'] = '8';
                    }
                    ?>
                            <h1><?php echo $match['name']; ?></h1>
                            <strong>Event Starts:</strong> <span class="localTime"><?php echo $match['startdatetime']; ?></span>
                        </div>
                        <div class="right">
                            <div class="item"><strong>Registration:</strong> Invite Only</div>
                            <div class="item"><strong>Max Teams:</strong> <?php echo $match['maxteams']; ?></div>
                            <div class="item"><strong>Status:</strong> Upcoming</div>
                        </div>
                    </div>
                </div>
                <div class="nav-wrap">
                    <ul class="nav nav-tabs nav-pills">
                                                <li class="active"><a data-toggle="tab" href="#results">Results</a></li>
                        <li><a data-toggle="tab" href="#info">Event Info</a></li>

                    </ul>
                </div>
                <div class="tab-content">
                     
                     <div class="tab-pane active" id="results">
                         <?php if ($_REQUEST['id'] == 7) { $ss_results = "/img/ss2_results.png"; }
                    else {
                      $ss_results = "/img/ss_results.png";  
                    }
                    $cache = filemtime(getcwd().$ss_results);
                    ?>
                        <img src="<?php echo "{$ss_results}?{$cache}"; ?>" /> 
                     </div>
                    
                    <div class="tab-pane" id="info">
                         <?php if ($_REQUEST['id'] == 7) { $ss_info = "/img/ss2_info.png"; }
                    else {
                      $ss_info = "/img/ss_info.png";  
                    }
                    $cache = filemtime(getcwd().$ss_info);
                    ?>
                         <img src="<?php echo "{$ss_info}?{$cache}"; ?>" />
                     </div>
                    
                </div>
            </div> 
<script type="text/javascript">
    window.onload = function() {
        PlayIGL.Analytics.track('Visted Mini Event Page', {'referer': document.referrer});
    
    }
</script>