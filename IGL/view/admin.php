<?php
if( PRESS )
{
?>
<div class="span12">
    <?php include('./view/admin/subnavigation.php'); ?>
    <div class='box' style='margin-top:12px'>
        <h1>International Gaming League - Admin Panel</h1>
        <div class='inner_box' style='margin-top:12px'>
<?php

/*
 * this file gets some IGL stats =)
 * @author Darryl Allen
 */

$stats = json_decode(file_get_contents(BASEURL."api/igl_stats.php"),true);
if(is_array($stats))
{
    if($stats['num_teams'] > 0)
        $avg_team_size = ceil($stats['num_players'] / $stats['num_teams']);
    else
        $avg_team_size = 0;

    if($stats['num_referrals']>0)
        $percent_converted = ceil($stats['num_referrals_converted']/$stats['num_referrals']*100);
    else 
        $percent_converted = 0;
?>

    <table class='table table-condensed table-striped'>
        <tr><td colspan='2'><h4>IGL STATS</h4></td>
        <tr><td><b>Games Hosted</b></td><td><?php echo $stats['num_games']; ?></td></tr>
        <tr><td><b>Total Leagues</b></td><td><?php echo $stats['num_leagues']; ?></td></tr>
        <tr><td><b>Teams in Leagues</b></td><td><?php echo $stats['num_teams']; ?></td></tr>
        <tr><td><b>Avg. Team Size</b></td><td><?php echo $avg_team_size; ?></td></tr>
        <tr><td><b>Rostered Players</b></td><td><?php echo $stats['num_players']; ?></td></tr>
        <tr><td><b>Free Agents</b></td><td><?php echo $stats['num_freeagents']; ?></td></tr>
        <tr><td><b>Registered Users</b></td><td><?php echo $stats['num_registered']; ?></td></tr>
        <tr><td><b>New Users 24 hrs</b></td><td><?php echo $stats['num_new24h']; ?></td></tr>
        <tr><td><b>New Users 7 days</b></td><td><?php echo $stats['num_new7d']; ?></td></tr>
        <tr><td><b>New Users 30 days</b></td><td><?php echo $stats['num_new30d']; ?></td></tr>
        <tr><td><b>Online last 24hrs</b></td><td><?php echo $stats['num_online']; ?></td></tr>
        <tr><td><b>Referrals Sent</b></td><td><?php echo $stats['num_referrals']; ?></td></tr>
        <tr><td><b>Referrals Converted</b></td><td><?php echo $stats['num_referrals_converted']; ?> (<?php echo $percent_converted; ?>%)</td></tr>
    </table>

    <table class='table table-condensed table-striped stats'>
    <tr><td><img src='<?php echo BASEURL; ?>img/igl_icon.png'></td><td><h4>GAME</h4></td><td><h4>LEAGUES</h4></td><td><h4>TEAMS</h4></td><td><h4>PLAYERS</h4></td><td><h4>AGENTS</h4></td></tr>
    <?php
    if(is_array($stats['game']))
    foreach($stats['game'] as $key => $games)
    {
        ?>
        <tr><td class='span1'><img class='img-rounded' src='<?php echo BASEURL; ?>img<?php echo $games['logo']; ?>'></td><td class='span4'><h5><a onclick='toggle("game_<?php echo $games['game_id']; ?>")'><?php echo $games['game_name']; ?></a></h5></td><td class='span2'><?php echo $games['num_leagues']; ?></td><td class='span2'><?php echo $games['num_teams']; ?></td><td class='span1'><?php echo $games['num_players']; ?></td><td class='span1'><?php echo $games['num_freeagents']; ?></td></tr>
        <tr class='game_<?php echo $games['game_id']; ?>' style='display:none'><td colspan='6'>
        <?php
        if(is_array($games['leagues']))
        {
            foreach($games['leagues'] as $key => $league)
            {
                ?>
                <table class='table table-condensed'><tbody>
                <tr><td class='span1'><img class='img-rounded' src='<?php echo BASEURL; ?>img<?php echo $league['icon']; ?>'></td><td class='span4'><a target='_admin' href='../league/<?php echo $league['league_id']; ?>-<?php echo $league['league_name']; ?>'><h5><a onclick='toggle("teams_<?php echo $league['league_id']; ?>")'><?php echo $league['league_name']; ?></a></h5></a></td><td class='span2'>&nbsp;</td><td class='span2'><?php echo $league['num_teams']; ?></td><td class='span1'><?php echo $league['num_players']; ?></td><td class='span1'>&nbsp;</td></tr>
                <?php
                if(is_array($league['teams']))
                {
                    foreach($league['teams'] as $key => $teams)
                    {
                        ?>
                        <tr class='teams_<?php echo $league['league_id']; ?>' style='display:none'><td>&nbsp;</td><td><b><a target='_admin' href='../team/<?php echo $teams['team_id']; ?>-<?php echo $teams['team_name']; ?>'><?php echo $teams['team_name']; ?></a></b> (<?php echo $teams['team_wins']; ?> - <?php echo $teams['team_losses']; ?>)</td><td>&nbsp;</td><td>&nbsp;</td><td><?php echo $teams['num_players']; ?></td><td>&nbsp;</td></tr>
                        <?php
                    }
                }
                ?></tbody></table><?php
            }
        }
        ?></td></tr><?php
    }
    ?></table><?php
}
else
{
    ?>
    <div class='alert alert-error'>Stats currently unavailable.</div>
    <?php
}
?>

    </div>
</div>
</div>
<?php
}
?>