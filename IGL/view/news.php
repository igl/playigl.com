<script type="text/javascript">
  var disqus_title = "<?php echo $news['title']; ?>"; 
</script>
<?php
    $title = stripslashes($news['title']);
    $content = stripslashes(urldecode($news['content']));
    $summary = truncate(str_replace(array("\r\n", "\n", "\r"),'',strip_tags($content)),200);
    $date = date('F jS H:i',$news['dateposted']);
    $social = array(
        "Facebook" => "igl_icon.png",
        "Twitter" => "igl_icon.png",
        "LinkedIn" => "igl_icon.png",
        "Pinterest" => "igl_icon.png"
    );
?>

<div class='box' style='margin-top:12px;'>
    <h1><?php echo $title; ?></h1>
    <div style='padding:6px'>
        <div class='inner_box'>
            <p class='posted'><strong>Posted: </strong><?php echo $date; ?> by <?php echo $news['poster']; ?></p>
            <div class='articlecontent'><?php echo $content; ?></div>
            <?php
                foreach($social as $key => $val) {
                    echo "<span class='st_".strtolower($key)."_vcount' st_image='".BASEURL."/img/{$val}' st_title='{$article['title']}' st_summary='{$summary}' displayText='{$key}'></span>";
                }
            ?>
        </div>
    </div>
</div>
?>