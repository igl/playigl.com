<?php
/*
 * @copyright 2013 PlayIGL.com
 * @Author Darryl Allen
 */

include_once 'navigation.php';
?>

<div class="row">
    <div class='span12'>
        <div class="alert alert-info"><button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button><i class="icon-info-sign"></i> <b>Tip:</b> Captains can hover over a team name to issue a challenge.</div>
    </div>
</div>
<div class="row">
    <div class='span12'>
        <table  class='table table-striped table-stats'>
            <thead>
                <tr><th width='50px'>Rank</th><th width='100px'>Tag</th><th>Name</th><th width='50px'>Points</th><th width='50px'>Highest</th></tr>
            </thead>
            <tbody>
                <?php
                $rank = 1;
                foreach ($standings as $t) {
                    if ($is_ladder_captain) {
                        echo "<tr><td>{$rank}</td><td>{$t['team_tag']}</td><td class='challenge_link'><a class='pull-left' href='/team/{$t['team_id']}-" . urlencode($t['team_name']) . "'>{$t['team_name']}</a> <span class='pull-left'>&nbsp; ({$t['team_ladder_wins']}-{$t['team_ladder_loses']})</span> <a onclick='challengeTimes({$tier_id},{$t['team_id']})' class='do_challenge'>&nbsp; &nbsp; &nbsp; &nbsp;<small>Challenge <i class='icon-trophy'></i></small></a></td><td><a href='/team/results/{$t['team_id']}'>{$t['team_points']}</a></td><td>{$t['team_highest_points']}</td></tr>";
                    } else {
                        echo "<tr><td>{$rank}</td><td>{$t['team_tag']}</td><td><a class='pull-left' href='/team/{$t['team_id']}-" . urlencode($t['team_name']) . "'>{$t['team_name']}</a> <span class='pull-left'> ({$t['team_ladder_wins']}-{$t['team_ladder_loses']})</span> </td><td><a href='/team/results/{$t['team_id']}'>{$t['team_points']}</a></td><td>{$t['team_highest_points']}</td></tr>";
                    }
                    $rank++;
                }
                ?>
            </tbody>
        </table>
    </div>
</div>
<div class="modal hide" id="ladderModal" tabindex="-1" role="dialog" aria-labelledby="ladderModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class='icon-remove'></i></button>
        <h3 id="ladderModalLabel">LOADING</h3>
    </div>
    <div class="modal-body" id="ladderModalBody">
        <div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">CLOSE</button>
        <button id="ladderModalButton" class="btn btn-primary">CHALLENGE</button>
    </div>
</div>