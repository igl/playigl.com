<?php
/*
 * @copyright 2013 PlayIGL.com
 * @Author Darryl Allen
 */
include_once('navigation.php');
?>
<div class="row">
    <div class='span12'>
        <table class='table table-striped table-stats'>
            <thead>
                <tr><th>Match ID</th><th width='50%'>Date</th><th>Home</th><th>Away</th><th>Score</th></tr>
            </thead>
            <tbody>
                <?php
                foreach ($results as $r) {
                    $home_style = 'green';
                    $away_style = 'green';
                    if ($r['home_diff'] < 0)
                        $home_style = 'red';
                    if ($r['away_diff'] < 0)
                        $away_style = 'red';

                    $home_score = explode("|", $r['home_score']);
                    $away_score = explode("|", $r['away_score']);
                    $maps = explode("|", $r['maps']);

                    $map_info = "<table class=\" table table-condensed\">";
                    foreach ($maps as $i => $map)
                        $map_info .= "<tr><td>{$map}:</td><td> {$home_score[$i]} - {$away_score[$i]} </td></tr>";
                    $map_info .= "</table>";

                    echo "<tr>
                        <td><a href='/match/{$r['match_id']}-" . urlencode($r[home_name]) . "+vs+" . urlencode($r['away_name']) . "' class='popover-on' data-html='true' data-title='Match Details' data-content='{$map_info}'>{$r['match_id']}</a></td>
                            <td class='localTime'>{$r['date']}</td>
                            <td><a href='/team/{$r['home_id']}-{$r['home_name']}' class='tooltip-on' title='{$r['home_name']}'>{$r['home_tag']}</a> {$r['home_points']} (<span style='color:{$home_style}'>{$r['home_diff']}</span>)</td>
                            <td><a href='/team/{$r['away_id']}-{$r['away_name']}' class='tooltip-on' title='{$r['away_name']}'>{$r['away_tag']}</a> {$r['away_points']} (<span style='color:{$away_style}'>{$r['away_diff']}</span>)</td>
                            <td><a href='/match/{$r['match_id']}-" . urlencode($r[home_name]) . "+vs+" . urlencode($r['away_name']) . "' class='popover-on' data-html='true' data-title='Match Details' data-content='{$map_info}'>" . array_sum($home_score) . " - " . array_sum($away_score) . "</a></td>
                         </tr>\n";
                }
                ?>
            </tbody>
        </table>
    </div>
</div>