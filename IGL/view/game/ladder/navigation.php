<?php
/*
 * @copyright 2013 PlayIGL.com
 * @Author Darryl Allen
 */
if ($ladder_info['ladder_livedate'] > time())
    $live_status = "Live: <span class='label label-warning'>" . date('F jS', $ladder_info['ladder_livedate']) . "</span>";

if ($ladder_info['ladder_registrationdate'] > time())
    $registration_status = "Reg: <span class='label label-warning'>" . date('F jS', $ladder_info['ladder_registrationdate']) . "</span>";


$options = array('Open Matches', 'Standings', 'Schedule', 'Results', 'Rules', 'FAQ');
?>
<div class="row">
    <div class="span12"><?php echo $registration_status; ?><?php echo $live_status; ?></div>
</div>
<div class="row">
    <div class="span12">
        <div class='navbar'>
            <div class='navbar-inner'>
                <span class="brand"><i class="icon-list"></i> <?php echo "{$ladder_info['continent']} {$ladder_info['format']} {$ladder_info['name']}"; ?> Division <?php echo $_GET['division']; ?> </span>
                <ul class="nav pull-right">
                    <?php
                    foreach ($options as $i => $o) {
                        if($i != 0) {
                            echo "<li class='divider-vertical'></li>";
                        }
                        if (strtolower($o) == strtolower($_GET['sub_view'])) {
                            echo "<li class='active'><a href='{$o}'>{$o}</a></li>";
                        } else {
                            echo "<li><a href='{$o}'>{$o}</a></li>";
                        }
                    }
                    ?>
                </ul>
            </div>
        </div>
        
    </div>
</div>

<?php
if($is_ladder_captain) {
?>
<div class="row">
    <div class='span12'>
        <button onclick='availability(<?php echo $tier_id; ?>)' class="btn btn-success pull-right btn-mini">Add/Edit Match <i class="icon-plus"></i></button> &nbsp; 
        <?php
if (!$on_game_team) { ?>
    <a href="/dashboard/teams" class="btn btn-success pull-right btn-mini">New Team <i class="icon-plus"></i></a>
<?php } elseif ($is_game_captain && !$on_ladder_team) { ?>
    <button id="join_ladder" class="btn btn-success pull-right btn-mini">Join Ladder <i class="icon-ok"></i></button>
<?php } elseif ($is_game_captain && $on_ladder_team) { ?>
    <button id="join_ladder" class="btn btn-danger pull-right btn-mini">Leave Ladder <i class="icon-remove"></i></button>
<?php } ?>
    </div>
</div>
<?php
}
?>

    <input type="hidden" id="tier_id" value="<?php echo $tier_id; ?>">