<?php
/*
 * @copyright 2013 PlayIGL.com
 * @Author Darryl Allen
 */
include_once('navigation.php');
$messages = Array(
    'Only team captains can register teams for matches.',
    'You may only register for matches within your own division',
    'A match becomes official upon the listing team\'s acceptance of the challenge',
    'Challenges can be viewed under <a href="/dashboar/matches">my matches</a>',
    'A team does not have to accept your challenge'
);

if (count($match_listings) == 0) {
    if ($is_ladder_captain) {
        ?>
        <div class="row">
            <div class='span12'>
                <div class="alert alert-error"><button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button><i class="icon-warning-sign"></i> <b>No matches posted.</b> <a onclick="availability(<?php echo $tier_id; ?>)">Create a listing</a> for your team.</div>
            </div>
        </div>
    <?php } else {
        ?>
        <div class="row">
            <div class='span12'>
                <div class="alert alert-error"><button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button><i class="icon-warning-sign"></i> <b>No matches posted.</b> <a href="/dashboard/teams">Create a team</a> and sign-up!</div>
            </div>
        </div>

        <?php
    }
} else {
    ?>
    <div class="row">
        <div class='span12'>
            <div class="alert alert-info"><button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button><i class="icon-info-sign"></i> <b>Tip:</b> <?php echo $messages[array_rand($messages)]; ?></div>
        </div>
    </div>


    <div class="row">
        <div class='span12'>
            <table class='table table-striped table-stats'>
                <thead>
                    <tr><th width="25%">Team</th><th width="60%">Date / Time</th><th>Map</th><th>&nbsp;</th></tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($match_listings as $m) {
                        if ($m['match_id']) {
                            $button_text = "<a href='/match/{$m['match_id']}' class='btn btn-mini btn-success'>Accepted <i class='icon-external-link'></i></a>";
                        } else {
                            if(isLoggedIn() && in_array($_SESSION['playerid'],array($m['captain_id'],$m['alternate_id']))) {
                                $button_text = "<button id='listing_{$m['id']}' class='btn btn-mini btn-danger' onclick='removeChallenge({$m['id']})'>Remove <i class='icon-remove'></i></button>";
                            } else {
                                if(!isLoggedIn()) {
                                    $status = "disabled tooltip-on";
                                    $hover = "title='You must be logged in..'";
                                }
                            
                            if ($m['challenged']) {
                                $button_text = "<button id='listing_{$m['id']}' class='btn btn-mini btn-success {$status}' {$hover}>Pending <i class='icon-clock'></i></button>";
                            } else {
                                $button_text = "<button id='listing_{$m['id']}' class='btn btn-mini btn-success {$status}' {$hover} onclick='doChallenge({$m['id']})'>Challenge</button>";
                            }
                            }
                        }
                        
                        echo "<tr id='listing_{$m['id']}'><td><a href='/team/{$m['team_id']}-{$m['team_name']}'>{$m['team_name']}</a></td><td class='localTime'>{$m['time']}</td><td>{$m['map']}</td><td>{$button_text}</td></tr>\n";
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <?php
}
?>
<div class="modal hide" id="ladderModal" tabindex="-1" role="dialog" aria-labelledby="ladderModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class='icon-remove'></i></button>
        <h3 id="ladderModalLabel">LOADING</h3>
    </div>
    <div class="modal-body" id="ladderModalBody">
        <div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">CLOSE</button>
        <button id="ladderModalButton" class="btn btn-primary">CHALLENGE</button>
    </div>
</div>