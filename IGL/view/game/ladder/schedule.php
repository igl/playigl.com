<?php
/*
 * @copyright 2013 PlayIGL.com
 * @Author Darryl Allen
 */
include_once('navigation.php');
?>
<div class="row">
    <div class='span12'>
        <table class='table table-striped table-stats'>
            <thead>
                <tr><th>Match ID</th><th width="75%">Date</th><th>Home</th><th>vs</th><th>Away</th></tr>
            </thead>
            <tbody>
                <?php
                foreach ($schedule as $s) {
                    echo "<tr><td><a href='/match/{$s['match_id']}-" . urlencode($s[home_name]) . "+vs+" . urlencode($s['away_name']) . "'>{$s['match_id']}</a></td><td class='localTime'>{$s['date']}</td><td><a href='/team/{$s['home_id']}-".urlencode($s['home_name'])."' class='tooltip-on' title='{$s['home_name']}'>{$s['home_tag']}</a></td><td></td><td><a href='/team/{$s['away_id']}-".urlencode($s['away_name'])."' class='tooltip-on' title='{$s['away_name']}'>{$s['away_tag']}</a></td></tr>\n";
                }
                ?>
            </tbody>
        </table>
    </div>
</div>