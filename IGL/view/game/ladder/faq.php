<?php
/*
 * @copyright 2013 PlayIGL.com
 * @Author Darryl Allen
 */
include_once('navigation.php')
?>
<div class="row">
    <div class='span12'>    
        <div class="clear well aboutladder"><a name="help"></a>
            <h4>How does this thing work?</h4>
            <p>Matches occur every hour, on the hour. As a captain you have the ability to register your team for a match up to 4 hours in advance. Once your team is registered, you may vote for the map of your choice. Map selections are randomly generated from a pool of available maps. Schedules are generated at the top of the hour, matches will take place on the map with the most votes. Each team will be paired against a team within their own skill range. Teams will not play each other twice in a row.</p>
            <p>Match times are displayed in a user's local time zone.</p>

            <h4>How many teams can register for a time slot?</h4>
            <p>Each ladder has a different number of servers available depending on demand. The current limit for this ladder is <?php echo $ladder_info['ladder']['ladder_slots']; ?> teams per time slot.</p>

            <h4>It's match time! How do I join?</h4>
            <p>You can join matches from your <a href="/dashboard/matches">dashboard</a>.</p>
            <h4>What if there's an odd number of teams registered for a pool?</h4>
            <p>The IGL ladder is first come, first serve. If there is an odd number of teams, the last team registered will receive a bye.</p>

            <h4>I joined the ladder, but don't see my team.</h4>
            <p>New teams start at the lowest tier for a ladder. Advance quickly by playing lots of matches, and challenging teams higher than you.</p>

            <h4>How are points calculated?</h4>
            <ul class="unstyled">
                <li>All teams start at 100 points.</li>
                <li>Beating a team ranked higher than you will give you an additional bonus of 5% of the opponent’s value.</li>
                <li>Beating a team ranked lower than you will give you an additional bonus of 2.5% of the opponent’s value.</li>
                <li>Losing to a team ranked higher than you will remove 2.5% of your own value.</li>
                <li>Losing to a team ranked lower than you will remove 5% of your own value.</li>
                <li>A forfeit will remove 7% of your own value.</li>
                <li>A Tie will give both teams Value = Value - Value * 0.05 + Opponent * 0.05</li>
                <li>Streaks of 5 wins and over will give the winning team additional bonus points equal to their current streak, up to a max of 20.</li>
                <li>The maximum gain a team will get for a single win is 10% of their own value.</li>
                <li></li>
                <li></li>
            </ul>

            <h4>Why did my team lose points?</h4>
            <p>IGL requires that teams play a minimum of 1 match every 7 days. Teams not having played a match within 7 days lose 10% of their point value.</p>

            <h4>Why was my team removed?</h4>
            <p>After 15 days of inactivity, your team is removed from the ladder. Upon removal, all points have been lost.</p>

            <h4>My team has more points than a team in a higher tier. Why haven't we moved up?</h4>
            <p>Tiers are aligned on the 1st and 15th of every month. Work hard to keep up your points!</p>

            <h4>How do tiers work?</h4>
            <p>Tiers are limited to a variable number of teams, set by the admin. This ladder is limited to <?php echo $ladder_info['ladder']['ladder_maxteams']; ?> teams. Once the tier reaches 150% the max number of teams <?php echo ($ladder_info['ladder']['ladder_maxteams'] * 1.5); ?>, the tier is split. If a ladder has less than 25% of the max allowed teams <?php echo ($ladder_info['ladder']['ladder_maxteams'] * .25); ?>, the bottom tiers are merged.</p>

        </div>
        <input type="hidden" id="page_id" name="page_id" value="<?php echo $id; ?>">
    </div>
</div>