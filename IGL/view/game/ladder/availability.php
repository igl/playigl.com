<?php
/*
 * @copyright 2013 PlayIGL.com
 */

include('../../../includes/config.php');
include('../../../model/mysql.class.php');
include('../../../model/ladder.class.php');
$ladder = new ladder();
$maps = $ladder->getMaps($_GET['tier_id']);
$select = "<select name='map[]'>";
foreach ($maps as $m) {
    $select .= "<option value='{$m['map']}'>{$m['map']}</option>";
}
$select .= "</select>";
if(!is_numeric($_SESSION['playerid'])) {
    die('You must be logged in.');
}
$availability = $ladder->getAvailability();
$slots = 3 - count($availability);
?>
<form id="availability">
    <table class="table table-striped">
        <thead>
            <tr><th>Date / Time</th><th>Map</th></tr>
        </thead>
        <tbody>
            <?php
            foreach($availability as $a)
            {
                $time = $a['time'] + ($_GET['offset'] * 3600);
                echo "<tr><td><input class='dateTimePicker' name='time[]' type='text' style='margin-top: 10px' value='".date('Y-m-d H:i:s',$time)."'></td><td>{$select}</td></tr>";
            }
            while($slots) {
                echo "<tr><td><input class='dateTimePicker' name='time[]' type='text' style='margin-top: 10px'></td><td>{$select}</td></tr>";
                $slots--;
            }
            ?>
        </tbody>
    </table>
    <input type="hidden" name="tier_id" value="<?php echo $_GET['tier_id']; ?> "><input type="hidden" name="offset" value="<?php echo $_GET['offset']; ?>">
</form>