</div>

<style>
    #sub-game-nav li { border-right:1px solid #eee; margin:0 2px;  }
    #sub-game-nav li:last-child { border-right:none; }
    
</style>

<div class="row">
    <div class="span12">
        <div class="navbar" id="sub-game-nav-bar">
            <div class="navbar-inner">
                <span class="brand" href="#"><i class="icon-gamepad" style="margin-right:10px;"></i> <?php echo $game_name; ?></span>
                <ul class="nav pull-right" id="sub-game-nav">
                    <?php
                    foreach ($comp_navi as $i => $n) {

                        if (strtolower($_GET['view']) == strtolower($i)) {
                            echo "<li class='dropdown active'>";
                        } else {
                            echo "<li class='dropdown'>";
                        }
                        echo "<a data-toggle='dropdown' class='dropdown-toggle' href='#'>{$i}s <b class='caret'></b></a>
                            
                        <ul class='dropdown-menu'>";
                        foreach ($n as $o) {
                            //Display continent names as sub-headers
                            if (isset($o['comp_continent']) && $continent != $o['comp_continent']) {
                                echo "<li class='nav-header'>Region: {$o['comp_continent']}</li>";
                                $continent = $o['comp_continent'];
                            }
                            if (is_array($o['divisions'])) {
                                echo "<li class='dropdown-submenu'><a tabindex='-1'>{$o['comp_name']} {$o['comp_format']}</a>";
                                echo "<ul class='dropdown-menu'>";
                                $d = 1;
                                foreach ($o['divisions'] as $t) {
                                    if (isset($_GET['sub_view']) && !empty($_GET['sub_view'])) {
                                        echo "<li><a href='/game/{$_GET['name']}/{$i}/{$o['comp_continent']}/{$o['comp_name']}/{$d}/{$_GET['sub_view']}'>Division {$d}</a></li>";
                                    } else {
                                        echo "<li><a href='/game/{$_GET['name']}/{$i}/{$o['comp_continent']}/{$o['comp_name']}/{$d}/Open Matches'>Division {$d}</a></li>";
                                    }

                                    $d++;
                                }
                                echo "</ul>";
                            } else {
                                echo "<li><a tabindex='-1' href='/game/{$_GET['name']}/{$i}/{$o['comp_continent']}/{$o['comp_name']}'>{$o['comp_name']} {$o['comp_format']}</a>";
                            }
                            echo "</li>";
                        }
                        ?></ul>
                    </li><?php
                }
                foreach ($game_navi as $n) {
                        if (strtolower($_GET['view']) == strtolower($n)) {
                            echo "<li class='active'><a href='/game/{$game_name}/{$n}'>{$n}</a></li>\n";
                        } else {
                            echo "<li><a href='/game/{$game_name}/{$n}'>{$n}</a></li>\n";
                        }
                    }
                    ?>
                </ul>
            </div>
        </div>
    </div>
</div>