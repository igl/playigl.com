<div class="row">
    <div class="span12">
        <?php if (!$on_game_team) { ?>
        <a href="/dashboard/teams" class="btn btn-success pull-right" style="margin-bottom:16px">New Team <i class="icon-plus"></i></a>
        <?php } ?>
        <table id='ladder_schedule' class='table table-striped table-stats'>
            <thead>
                <tr><th width="60px">Team ID</th><th>Name</th><th>Tag</th><th>Captain</th><th>Alternate</th><th>Location</th><th>Wins</th><th>Loses</th><th width='50px'>RPI</th><th>Connect</th></tr>
            </thead>
            <tbody>
                <?php
                $connect = array('team_steamgroup', 'team_twitch', 'team_twitter', 'team_facebook');
                foreach ($teams['teams'] as $t) {
                    echo "<tr><td><a href='/team/{$t['team_id']}-{$t['team_name']}'>{$t['team_id']}</a></td><td><a href='/team/{$t['team_id']}-{$t['team_name']}'>{$t['team_name']}</a></td><td>{$t['team_tag']}</td><td><a href='/player/{$t['team_captain_name']}'>{$t['team_captain_name']}</a></td><td><a href='/player/{$t['team_alternate_name']}'>{$t['team_alternate_name']}</a></td><td>{$t['team_continent']} - {$t['team_region']}</td><td>{$t['team_wins']}</td><td>{$t['team_loses']}</td><td>{$t['team_rpi']}</td><td>";

                    foreach ($connect as $c) {
                        if (isset($t[$c]) && !empty($t[$c])) {
                            switch ($c) {
                                case "team_steamgroup":
                                    $t[$c] = str_replace('http://steamcommunity.com/groups/', '', $t[$c]);
                                    echo " <a target='_blank' href='http://steamcommunity.com/groups/{$t[$c]}'><img src='/img/social/steam.png'></a>";
                                    break;
                                case "team_facebook":
                                    echo " <a target='_blank' href='http://www.facebook.com/{$t[$c]}'><img src='/img/social/facebook.png'></a>";
                                    break;
                                case "team_twitter":
                                    echo " <a target='_blank' href='http://www.twitter.com/{$t[$c]}'><img src='/img/social/twitter.png'></a>";
                                    break;
                                case "team_twitch":
                                    echo " <a target='_blank' href='http://www.twitch.tv/{$t[$c]}'><img src='/img/social/twitch.png'></a>";
                                    break;
                            }
                        }
                    }
                    echo "</td></tr>\n";
                }
                ?>
            </tbody>
        </table>
        <div class="pagination pull-right">
            <ul>
                <?php
                if ($_GET['p'] > 1) {
                    echo "<li><a href='?p=" . ($_GET['p'] - 1) . "'>«</a></li>";
                }
                $page = 1;

                $count = ceil($teams['num_teams'] / 50);
                while ($count) {
                    if ($page == $_GET['p']) {
                        echo "<li class='active'><a href='?p={$page}'>{$page}</a></li>";
                    } else {
                        echo "<li><a href='?p={$page}'>{$page}</a></li>";
                    }

                    $page++;
                    $count = $count - 1;
                }

                if ($page != ($_GET['p'] + 1)) {
                    echo "<li><a href='?p=" . ($_GET['p'] + 1) . "'>»</a></li>";
                }
                ?>
            </ul>
        </div>

    </div>
</div>