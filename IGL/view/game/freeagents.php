<div class="row">
    <div class="span12">
        <?php if ($is_agent) { ?>
            <button class="btn btn-danger pull-right" id="button_enlist" style="margin-bottom:16px;" onclick="enlist(<?php echo $_SESSION['playerid']; ?>,<?php echo $game_id; ?>,'remove')">Remove <i class="icon-minus"></i></button>
        <?php } elseif (!$on_game_team && !$is_agent && isset($_SESSION['playerid'])) { ?>
            <button class="btn btn-success pull-right" id="button_enlist" style="margin-bottom:16px;" onclick="enlist(<?php echo $_SESSION['playerid']; ?>,<?php echo $game_id; ?>,'enlist')">Enlist <i class="icon-ok"></i></button>
        <?php } ?>
        
        <?php if(count($players['players'])==0) { ?>
        <div class="alert alert-warning"><b><i class="icon-warning-sign"></i> There are no free agents registered.</b> Click the enlist button to register.</div>
        <?php } else { ?>
        <div class="alert alert-info"><b><i class="icon-info-sign"></i> Tip:</b> Free agents can view invites received from captains on the <a href="/dashboard/teams">My Teams</a> page.</div>
            <table id='ladder_schedule' class='table table-striped table-stats'>
            <thead>
                <tr><th width="60px">Player ID</th><th>Username</th><th>Name</th><th>Location</th><th>Enlisted</th><th>Connect</th></tr>
            </thead>
            <tbody>
                <?php
                $connect = array('player_community_id', 'player_twitch', 'player_twitter', 'player_facebook');
                foreach ($players['players'] as $p) {
                    echo "<tr><td><a href='/player/{$p['player_name']}'>{$p['player_id']}</a></td><td><img src='/img/flags/{$p['player_country']}.png'> <a href='/player/{$p['player_username']}'>{$p['player_username']}</a></td><td>{$p['player_firstname']} {$p['player_lastname']}</td><td>{$p['player_city']} {$p['player_state']}</td><td>" . date('M dS', strtotime($p['player_join_date'])) . "</td><td>";

                    foreach ($connect as $c) {
                        if (isset($p[$c]) && !empty($p[$c])) {
                            switch ($c) {
                                case "player_community_id":
                                    echo " <a target='_blank' href='http://steamcommunity.com/profiles/{$p[$c]}'><img src='/img/social/steam.png'></a>";
                                    break;
                                case "player_facebook":
                                    echo " <a target='_blank' href='http://www.facebook.com/{$p[$c]}'><img src='/img/social/facebook.png'></a>";
                                    break;
                                case "player_twitter":
                                    echo " <a target='_blank' href='http://www.twitter.com/{$p[$c]}'><img src='/img/social/twitter.png'></a>";
                                    break;
                                case "player_twitch":
                                    echo " <a target='_blank' href='http://www.twitch.tv/{$p[$c]}'><img src='/img/social/twitch.png'></a>";
                                    break;
                            }
                        }
                    }
                    echo "</td></tr>\n";
                }
                ?>
            </tbody>
        </table>

        <div class="pagination pull-right">
            <ul>
                <?php
                if ($_GET['p'] > 1) {
                    echo "<li><a href='?p=" . ($_GET['p'] - 1) . "'>«</a></li>";
                }
                $page = 1;

                $count = ceil($players['num_players'] / 50);
                while ($count) {
                    if ($page == $_GET['p']) {
                        echo "<li class='active'><a href='?p={$page}'>{$page}</a></li>";
                    } else {
                        echo "<li><a href='?p={$page}'>{$page}</a></li>";
                    }
                    $page++;
                    $count = $count - 1;
                }

                if ($page != ($_GET['p'] + 1)) {
                    echo "<li><a href='?p=" . ($_GET['p'] + 1) . "'>»</a></li>";
                }
                ?>
            </ul>
        </div>
        <?php } ?>
    </div>
</div>