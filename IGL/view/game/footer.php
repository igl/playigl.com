<div class="row">
    <div class="span12">
        <h2 class="bar">Popular Matches Right Now <i class="icon-star"></i></h2>
        <ul class="match-cards">
            <?php
            $popular_count = 0;
            foreach ($match_cards as $card) {
                $card_type = $card['sponsored'] > 0 ? "pushpin" : ($card['featured'] > 0 ? "star" : false);
                $card_text = $card['sponsored'] > 0 ? "Sponsored" : ($card['featured'] > 0 ? "Featured" : false);
                $badge = "";

                if ($card_type === false) {
                    if ($popular_count < 3) {
                        $card_type = "heart";
                        $card_text = "Popular";
                        $popular_count++;
                    }
                }

                if ($card_type) {
                    $badge = <<< BADGE
              <div class="badge-top">
                <div style="padding:5px"><i class="icon-{$card_type}"></i> {$card_text} Match</div>
              </div>
BADGE;
                }

                echo <<< LI
            <li class='game'>
              <div class="inner">
                <div class="image"><a href="/match/{$card['id']}"><img class="match-cards" src="/img{$card['game_tile']}" /></a></div>
                <div class="title">
                  <div class="hoverpop">
                    <div class="follows"><a href="#"><i class="icon-heart match-card-upvote tooltip-on" data-id="{$card['id']}" title="Click to upvote"></i><span>{$card['popularity']}</span></a></div>
                    <div class="comments"><a href="/match/{$card['id']}#comments"><i class="icon-comment"></i>0</a></div>
                    <div class="link"><a href="/match/{$card['id']}"><i class="icon-link"></i></a></div>
                  </div>
                  <h4><a href="/match/{$card['id']}">{$card['home_tag']} vs {$card['away_tag']}</a></h4>
                  <span class="game"><a href="/match/{$card['id']}">{$card['game_name']}</a></span>
                </div>
                {$badge}
              </div>
            </li>
LI;
            }

            foreach ($youtube_vods as $vod) {
                echo <<< LI
            <li class='game'>
              <div class="inner">
                <div class="image" style="height:100%;">
                <div class="badge-top"><div style="padding:5px"><i class="icon-play-circle"></i> Youtube Highlight</div></div>
                <a href="{$vod['link']}" target="_blank"><img src="{$vod['thumbnail']}" style="height:100%;" /></a></div>
                <div class="title">
                  <h4><a href="{$vod['link']}" target="_blank">{$vod['title']}</a></h4>
                </div>
              </div>
            </li>
LI;
            }
            ?>
        </ul>
    </div>
</div>
<!--
<div class="row">
  <div class="span12 power-rankings">
    <h2 class="bar no-bottom">Power Rankings <span class="badge badge-help"><a class="tooltip-on" title="How does this work?"><i class="icon-question-sign"></i></a></h2>
    <table class="table table-bordered rankings">
      <tr>
        <td class="pos" style="background:gray;">&nbsp;</td>
        <th>Open</td>
        <th>Main</td>
        <th>Select</td>
        <th>Premier</td>
      </tr>
      <tr>
        <td class="pos pos1">1</td>
        <td><a href="#"><img src="img/ranking-team.png" /></a></td>
        <td><a href="#"><img src="img/ranking-team.png" /></a></td>
        <td><a href="#"><img src="img/ranking-team.png" /></a></td>
        <td><a href="#"><img src="img/ranking-team.png" /></a></td>
      </tr>
    </table>
  </div>
</div>
-->
<div class="row">
    <div class="span6">
        <h2 class="bar no-bottom">Newest Forum Discussions <span class="badge badge-help"><a class="tooltip-on" title="How does this work?"><i class="icon-question-sign"></i></a></h2>
        <div class="box padded">
            <ul class="forums">
                <!-- Top Posts go here -->
                <?php
                foreach ($new_threads as $thread) {
                    echo $thread;
                }
                ?>
            </ul>
        </div>
    </div>
    <div class="span6">
        <h2 class="bar no-bottom">Top Contributors <span class="badge badge-help"><a class="tooltip-on" title="How does this work?"><i class="icon-question-sign"></i></a></h2>
        <div class="box padded">
            <ul class="forums">
                <!-- Top Posters go here -->
                <?php
                foreach ($top_posters as $poster) {
                    echo $poster;
                }
                ?>
            </ul>
        </div>
    </div>