<div class="row">
    <div class="span12">
        <?php if(count($schedule)==0) { ?>
        <div class="alert alert-warning"><b><i class="icon-warning-sign"></i> There are no matches scheduled.</b> Challenge teams on the ladder to get a match.</div>
        <div class="alert alert-info"><b><i class="icon-info-sign"></i> Tip:</b> Challenge teams on the ladder to get a match.</div>
        <?php } else { ?>
        <table id='ladder_schedule' class='table table-striped table-stats'>
            <thead>
                <tr><th width="60px">Match ID</th><th>Match Type</th><th>Match Style</th><th>Match Format</th><th>Home</th><th>vs</th><th>Away</th><th id='timezone'>Date GMT</th><th width='50px'>Details</th></tr>
            </thead>
            <tbody>
                <?php
                foreach ($schedule as $s) {
                    echo "<tr><td><a href='/match/{$s['match_id']}-" . urlencode($s[home_name]) . "+vs+" . urlencode($s['away_name']) . "'>{$s['match_id']}</a></td><td>{$s['comp_type']}</td><td><a href='/{$s['comp_type']}/{$s['comp_id']}/{$s['comp_name']}'>{$s['comp_name']}</a></td><td>{$s['comp_format']}</td><td><a href='/team/{$s['home_id']}-{$s['home_name']}' class='tooltip-on' title='{$s['home_name']}'>{$s['home_tag']}</a></td><td>&nbsp;</td><td><a href='/team/{$s['away_id']}-{$s['away_name']}' class='tooltip-on' title='{$s['away_name']}'>{$s['away_tag']}</a></td><td><span class='localTime'>{$s['date']}</span></td><td><a href='/match/{$s['match_id']}-" . urlencode($s[home_name]) . "+vs+" . urlencode($s['away_name']) . "'>view</a></td></tr>\n";
                }
                ?>
            </tbody>
        </table>
        <?php } ?>

    </div>
</div>