<?php
/*
 * @copyright 2013 PlayIGL.com
 * @Author Kevin Langlois
 */
?>
<div id="showmatch_data" data-showmatch-id="<?php echo $_REQUEST['id']; ?>" data-player-id="<?php echo $_SESSION['playerid']; ?>"></div>
<div class="row">
<div class="span12 event event-showmatch">
                <div class="banner">
                    <img src="/img/ss_banner.png" />
                    <div class="text-inner">
                        <div class="left">
                            <h1><?php echo $match['name']; ?></h1>
                            <strong>Event Starts:</strong> <span class="localTime"><?php echo $match['startdatetime']; ?></span>
                        </div>
                        <div class="right">
                            <div class="item"><strong>Registration:</strong> Invite Only</div>
                            <div class="item"><strong>Max Teams:</strong> 8</div>
                            <div class="item"><strong>Status:</strong> Upcoming</div>
                        </div>
                    </div>
                </div>
                <div class="nav-wrap">
                    <ul class="nav nav-tabs nav-pills">
                        <li class="active"><a data-toggle="tab" href="#info">Event Info</a></li>
                        <li><a data-toggle="tab" href="#results">Results</a></li>
                    </ul>
                </div>
                <div class="tab-content">
                     <div class="tab-pane active" id="info">
                         <img src="/img/ss_info.png" />
                     </div>
                     <div class="tab-pane" id="results">
                        <img src="/img/ss_results.png" /> 
                     </div>
                    
                </div>
            </div>
    </div>
<script type="text/javascript">
    window.onload = function() {
        PlayIGL.Analytics.track('Visted Mini Event Page', {'referer': document.referrer});
    
    }
</script>
