<div class="row">
    <div class="span12">
        <?php if(count($players['players'])==0) { ?>
        <div class="alert alert-warning"><b><i class="icon-warning-sign"></i> There are no players for this game.</b> <a href="/dashboard/teams">Create a team</a> or register as a <a href="/Free Agents">free agent</a>.</div>
        <?php } else { ?>
        <div class="alert alert-info"><b><i class="icon-info-sign"></i> Tip:</b> Only players on a team are listed.</div>
        <table id='ladder_schedule' class='table table-striped table-stats'>
            <thead>
                <tr><th width="60px">Player ID</th><th>Username</th><th>Name</th><th>Location</th><th>Team</th><th>Connect</th></tr>
            </thead>
            <tbody>
                <?php
                $connect = array('player_community_id', 'player_twitch', 'player_twitter', 'player_facebook');
                foreach ($players['players'] as $p) {
                    echo "<tr><td><a href='/player/{$p['player_name']}'>{$p['player_id']}</a></td><td><img src='/img/flags/{$p['player_country']}.png'> <a href='/player/{$p['player_username']}'>{$p['player_username']}</a></td><td>{$p['player_firstname']} {$p['player_lastname']}</td><td>{$p['player_city']} {$p['player_state']}</td><td><a href='/team/{$p['player_team_id']}-{$p['player_team_name']}'>{$p['player_team_name']}</a></td><td>";

                    foreach ($connect as $c) {
                        if (isset($p[$c]) && !empty($p[$c])) {
                            switch ($c) {
                                case "player_community_id":
                                    echo " <a target='_blank' href='http://steamcommunity.com/profiles/{$p[$c]}'><img src='/img/social/steam.png'></a>";
                                    break;
                                case "player_facebook":
                                    echo " <a target='_blank' href='http://www.facebook.com/{$p[$c]}'><img src='/img/social/facebook.png'></a>";
                                    break;
                                case "player_twitter":
                                    echo " <a target='_blank' href='http://www.twitter.com/{$p[$c]}'><img src='/img/social/twitter.png'></a>";
                                    break;
                                case "player_twitch":
                                    echo " <a target='_blank' href='http://www.twitch.tv/{$p[$c]}'><img src='/img/social/twitch.png'></a>";
                                    break;
                            }
                        }
                    }
                    echo "</td></tr>\n";
                }
                ?>
            </tbody>
        </table>

        <div class="pagination pull-right">
            <ul>
                <?php
                if ($_GET['p'] > 1) {
                    echo "<li><a href='?p=" . ($_GET['p'] - 1) . "'>«</a></li>";
                }
                $page = 1;

                $count = ceil($players['num_players']/50);
                while ($count) {
                    if ($page == $_GET['p']) {
                        echo "<li class='active'><a href='?p={$page}'>{$page}</a></li>";
                    } else {
                        echo "<li><a href='?p={$page}'>{$page}</a></li>";
                    }
                    $page++;
                    $count = $count - 1;
                }

                if ($page != ($_GET['p']+1)) {
                    echo "<li><a href='?p=" . ($_GET['p'] + 1) . "'>»</a></li>";
                }
                ?>
            </ul>
        </div>
        <?php } ?>
    </div>
</div>