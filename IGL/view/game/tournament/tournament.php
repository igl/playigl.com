<?php
/*
 * @copyright 2013 PlayIGL.com
 * @Author Kevin Langlois
 */
?>
<style>
    #tournament_content {
        overflow:hidden;
    }
    #bracketSystem {
        width:940px; margin-left:-19px;
    }
    #bracketSystem .team {
        width:350px; height:80px;
        background-color: #444; 
        color:#fff; font-size:16px; font-weight:bold;
    } 
    #bracketSystem .team span { font-size:12px; font-weight:normal; }
    #bracketSystem .team img {
        height: 64px; width:64px; float:left; margin:7px; border:1px solid #333;
    }
    #bracketSystem h4 { color:#fff; font-size:14px; margin:8px 0 0 0; font-weight:normal; padding:0; }
    #bracketSystem .team1 { float:left; text-align: left;}
    #bracketSystem .team2 { float:right; text-align: right; }
    #bracketSystem .team1 .score { float:right; margin-right:2px; }
    #bracketSystem .team2 .score { float:left; margin-left:2px; }
    #bracketSystem .team1 img { float:left; }
    #bracketSystem .team2 img { float:right; }

    #bracketSystem .score {
        width:50px; height:80px; float:right;
        background-image: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#aa3f3f), to(#981e1d));
        background-image: -webkit-linear-gradient(top, #aa3f3f, #981e1d); 
        background-image:    -moz-linear-gradient(top, #aa3f3f, #981e1d);
        background-image:     -ms-linear-gradient(top, #aa3f3f, #981e1d);
        background-image:      -o-linear-gradient(top, #aa3f3f, #981e1d); 
        color:#fff; font-size:22px; font-weight:bold;
        line-height:80px; text-align: center;
        position:relative; top:0px;
    }  
    #bracketSystem .matchbox {
        width:700px; height:80px;
        margin:0 auto;
        margin-bottom:0px;
        -moz-box-shadow:    2px 2px 2px 1px #666;
        -webkit-box-shadow: 2px 2px 2px 1px #666;
        box-shadow:         2px 2px 2px 1px #666;
    } 
    #bracketSystem .match_row {
        padding:19px;
    }
    #bracketSystem .grey {
        background:#E5E5E5;
    }
    .match_details {
        width:350px; height:30px;
        margin:0 auto 0px auto;
        -moz-box-shadow:    2px 2px 2px 1px #666;
        -webkit-box-shadow: 2px 2px 2px 1px #666;
        box-shadow:         2px 2px 2px 1px #666;
        background: #333333; /* Old browsers */
        background: -moz-linear-gradient(top, #333333 1%, #444444 100%); /* FF3.6+ */
        background: -webkit-gradient(linear, left top, left bottom, color-stop(1%,#333333), color-stop(100%,#444444)); /* Chrome,Safari4+ */
        background: -webkit-linear-gradient(top, #333333 1%,#444444 100%); /* Chrome10+,Safari5.1+ */
        background: -o-linear-gradient(top, #333333 1%,#444444 100%); /* Opera 11.10+ */
        background: -ms-linear-gradient(top, #333333 1%,#444444 100%); /* IE10+ */
        background: linear-gradient(to bottom, #333333 1%,#444444 100%); /* W3C */
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#333333', endColorstr='#444444',GradientType=0 ); /* IE6-9 */

        color:#ccc; font-size:12px; text-align:center; line-height:30px;
    }  
    #tournament_activity {
        border: 1px solid #D4D4D4;
        background-color: #FFF;
        border-radius: 10px;
        width:25px; height:25px;
        margin:0 auto; float:none;
    }
    

    #sub-game-tournament-nav li { border-right:1px solid #333; margin:0 2px;  }
    #sub-game-tournament-nav li:last-child { border-right:none; }
    
    .sub-header {
        width:100%;
        height:30px;
        background: #111111; /* Old browsers */
background: -moz-linear-gradient(top, #111111 42%, #ffffff 99%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(42%,#111111), color-stop(99%,#ffffff)); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top, #111111 42%,#ffffff 99%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top, #111111 42%,#ffffff 99%); /* Opera 11.10+ */
background: -ms-linear-gradient(top, #111111 42%,#ffffff 99%); /* IE10+ */
background: linear-gradient(to bottom, #111111 42%,#ffffff 99%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#111111', endColorstr='#ffffff',GradientType=0 ); /* IE6-9 */
    }
    
    #matchNavbar { 
        margin:-20px;
        border:none;
    }
    
    #event-status {
        width:100%; height:45px;
        background-color: #1B1B1B;
        background-image: -moz-linear-gradient(top, #111, #222);
        background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#111), to(#222));
        background-image: -webkit-linear-gradient(top, #111, #222);
        background-image: -o-linear-gradient(top, #111, #222);
        background-image: linear-gradient(to bottom, #111, #222);
        background-repeat: repeat-x;
        border-color: #252525;
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff111111', endColorstr='#ff222222', GradientType=0);
    }

    #event-progress-bar {
        position:relative; top:12px; left:15px; width:720px;
        background-color: #222222;
    background-image: -moz-linear-gradient(top, #222222, #333333);
    background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#222222), to(#333333));
    background-image: -webkit-linear-gradient(top, #222222, #333333);
    background-image: -o-linear-gradient(top, #222222, #333333);
    background-image: linear-gradient(to bottom, #222222, #333333);
    }
    #event-progress-bar .text { position:absolute; left:10px; color:#eee; font-size:14px; }
    
    #event-action-btn { border-radius:none; width:150px; }
</style>

<div class="row" style="position:relative; top:-10px;">
    <div class="span12">
        <div class='navbar navbar-inverse' style="margin-bottom:0;">
            <div class='navbar-inner'>
                <a class="brand" style="color:#fff;" ><i class="icon-trophy" style="margin-right:10px;"></i> <?php echo $r['tournament_name']; if ($r['tournament_format'] != 0) { echo $r['tournament_format']; } ?>  </a>
                <ul class="nav pull-right" id="sub-game-tournament-nav">
                    <li class="active"><a id="about" onclick="Tournament.navAbout(this);">Event Info</a></li>
                    <li><a onclick="Tournament.navSchedule(this);">Schedule</a></li>
                    <li><a onclick="Tournament.navAdmins(this);">Admins</a></li>
                    <li><a id="team_nav_count" onclick="Tournament.navTeams(this);">Participants</a></li>
                    <li><a onclick="Tournament.navMatches(this);">Matches</a></li>
                    <li><a onclick="Tournament.navDiscussion();">Discussion</a></li>
                    <!--<li><a>Rules</a></li>-->
                    <!--<li><a>Free Agents</a></li>-->
                </ul>
            </div>
        </div>


        <div class="clear" id='messages'></div>
    </div>
</div>
<div class="row" style="position:relative; top:-10px;">
    <div class="span12 event">
        <div class="banner" style="width:100%; height:200px;">
            <img style="box-shadow:0px 0px 5px 0px #333; width:950px; max-width:950px; height:200px; position:relative; left:-5px;" id="tournament_game_banner" src="/img/lolbanner.jpg" />

        </div>
        <div id="event-status">
            <div id="event-action-btn" style="margin:6px 10px;" class="btn btn-red pull-right">Closed</div>
                    <div id="event-progress-bar" class="progress progress-danger ">
                        <div class="bar" style="width: 0%"></div>
                        <div class="text">Loading event...</div>
                    </div>
        </div>
        <div id="tournament_alert" style="display:none;" class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button></div>
        <div id="tournament_activity" style="display:none; margin-top:20px; text-align:center;"><img src="/img/tournament-loader.gif"/></div>
        
        <div class="tab-content" id="tournament_content" data-tournament-id="<?php echo $r['tournament_id']; ?>" data-player-id="<?php echo $_SESSION['playerid']; ?>"></div>
    </div>
</div>