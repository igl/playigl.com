<?php if(isset($error) && is_array($error)) { ?>
    <div class="alert alert-error">
    <button type="button" class="close" data-dismiss="alert">x</button>
    <ul><?php foreach($error as $key => $warning) { ?><li><?php echo $warning; ?></li><?php } ?></ul>
    </div>
<?php } 
if(isset($success) && is_array($success)) { ?>
    <div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert">x</button>
    <ul><?php foreach($success as $key => $message) { ?><li><?php echo $message; ?></li><?php } ?></ul>
    </div>
<?php } ?>

<div class="span8">
    <div class="navbar">
        <div class="navbar-inner">
            <ul class="nav">
                <li><a onclick="switchView('profile')"><i class="icon-user icon-large"></i> PROFILE</a></li>
                <li class="divider-vertical"></li>
                <li><a onclick="switchView('matches')"><i class="icon-calendar icon-large"></i> MATCHES</a></li>
                <li class="divider-vertical"></li>
                <li><a onclick="switchView('teams')"><i class="icon-group icon-large"></i> TEAMS</a></li>
                <li class="divider-vertical"></li>
                <li><a onclick="switchView('referrals')"><i class="icon-share-alt icon-large"></i> REFERRALS</a></li>
                <li class="divider-vertical"></li>
                <li><a onclick="switchView('following')"><i class="icon-check icon-large"></i> FOLLOWING</a></li>
            </ul>
        </div>
    </div>
    <div id="dashboardresults" class="form"></div>
</div>
<div class="span4">
    <div class="notifications form"></div>
</div>
<div class="modal hide" id="Modal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class='icon-remove'></i></button>
        <h3 id="ModalLabel">LOADING</h3>
    </div>
    <div class="modal-body" id="ModalBody"></div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true" name="close">CLOSE</button>
    </div>
</div>
<div style="clear:both; margin-bottom:20px;"></div>

<script type="text/javascript">var all_games = <?php echo $all_games; ?>; var data = <?php echo $data; ?>; var current_view = "<?php echo $current_view; ?>"; </script>
<script id="team_create_template" type="text/template"><?php echo $team_create_template; ?></script>
<script id="avatar_template" type="text/template"><?php echo $avatar_template; ?></script>
<script id="profile_template" type="text/template"><?php echo $profile_template; ?></script>
<script id="teams_template" type="text/template"><?php echo $teams_template; ?></script>
<script id="team_admin_template" type="text/template"><?php echo $team_admin_template; ?></script>
<script id="team_details_template" type="text/template"><?php echo $team_details_template; ?></script>
<script id="team_edit_template" type="text/template"><?php echo $team_edit_template; ?></script>
<script id="team_password_template" type="text/template"><?php echo $team_password_template; ?></script>
<script id="team_builder_template" type="text/template"><?php echo $team_builder_template; ?></script>
<script id="team_leave_template" type="text/template"><?php echo $team_leave_template; ?></script>
<script id="matches_template" type="text/template"><?php echo $matches_template; ?></script>
<script id="referral_template" type="text/template"><?php echo $referral_template; ?></script>
<script id="followee_template" type="text/template"><?php echo $followee_template; ?></script>
<script id="score_template" type="text/template"><?php echo $score_template; ?></script>