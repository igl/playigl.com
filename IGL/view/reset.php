<div class="span12">
<div class="span8 well" style="margin:0 auto 10px auto; float:none;">
    
<style>.tooltip-on { margin-left:10px; position:relative; top:4px; }</style>
<div class="modal-body" id="ModalBody">
    <h2>Password Reset</h3><br/>
    <?php 
    if (isset($_POST['email']) && $_POST['email'] != "") {
        $password_reset_token = rand(1000000000, 9999999999);
        if ($db->update('player', array('password_reset_token' => $password_reset_token), array("email" => $_POST['email']))) {
        $message = "Follow this link to reset your PlayIGL password: http://dev.playigl.com/reset/".$password_reset_token;
        $headers = 'From: noreply@playigl.com' . "\r\n" .
        'Reply-To: noreply@playigl.com' . "\r\n" .
        'X-Mailer: PHP/' . phpversion();
        mail($_POST['email'], 'PlayIGL Password Reset', $message, $headers);
            echo "Password reset instructions sent to ".$_POST['email'].".";
        } else {
            echo "Password reset failed. Please contact support.";
            echo $db->LastError();
        }
    } else if (isset($_POST['password']) && $_POST['password'] != "" && isset($_POST['token']) && $_POST['token'] != "") {
        $salt = rand(1000, 99999);
        $pass = md5(md5($_POST['password']).''.$salt);
        if ($db->update('player', array('password' => $pass, 'salt' => $salt, 'password_reset_token' => ""), array('password_reset_token' => $_POST['token']))) {
            echo "Password has been reset."; 
        } else {
            echo "Token no longer valid."; 
        }
    } else {
        if ($_GET['token'] == "") {
        ?>
        <form action="" method="post">
            <label>Please enter your email</label>
            <input type="text" id="email" name="email" /><br/>
            <div onclick="$(this).parent().submit();" class="btn btn-large">Reset Password</div>
        </form>
        <?php
        } else {
        ?>
        <form action="" method="post">
            <label>Please enter a new password</label>
            <input type="password" id="password" name="password" /><br/>
            <input type="hidden" name="token" value="<?php echo $_GET['token']; ?>"/>
            <div onclick="$(this).parent().submit();" class="btn btn-large">Reset Password</div>
        </form>
        <?php
        }
    }
        ?>
    
</div>
    
</div>
</div>
