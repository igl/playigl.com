<?php
/**
 * Serves backbone.js requests
 * @author Shaun Delaney <sdelaney@playigl.com>
 * @return JSON
 * @copyright Copyright 2013 PlayIGL.com
 */
include_once('../includes/config.php');
include_once('../model/mysql.class.php');
include_once('../model/backbone.class.php');
include_once('../includes/functions.php');

$backbone = new Backbone();

switch ($_SERVER['REQUEST_METHOD']) {
  case 'POST':
    // create new item
    $profile = json_decode($_POST['model']);
    $action = $profile->action;
    unset($profile->action);
    
    switch ($action) {
      case "invite email to join team":
      case "import roster from ugc":
        $backbone->saveData($action, "dashboard", "teams", $profile);
        break;
    }

    returnJSON(array("success" => true));
    
    break;

  case 'GET':
    // get item(s)
    break;

  case 'PUT':
    // update item
    parse_str(file_get_contents("php://input"), $post_vars);
    $profile = json_decode($post_vars['model']);
    $action = $profile->action;
    unset($profile->action);

    switch ($action) {
      case "promote player to captain":
      case "promote player to alternate captain":
      case "demote player from alternate captain":
      case "remove player from team":
      case "cancel invite to team":
        $backbone->saveData($action, "dashboard", "teams", $profile);
        break;

      case "accept team join request":
      case "reject team join request":
      case "invite player to join team":
        //include_once('../sendgrid-php/SendGrid_loader.php');
        //$send_grid = new SendGrid('playigl', 'xeCr9feJ');

        switch ($action) {
          case "accept team join request": $template = "joinrequest_accept";
            break;
          case "reject team join request": $template = "joinrequest_deny";
            break;
          case "invite player to join team": $template = "invite";
            break;
        }

        $backbone->saveData($action, "dashboard", "teams", $profile);
        //$send_grid->web->send($backbone->createMail($profile, $template, new SendGrid\Mail()));
        break;
    }
    break;

  case 'DELETE':
    // delete item
    break;
}
echo $profile->action;