<?php
/**
 * Serves backbone.js requests
 * @author Shaun Delaney <sdelaney@playigl.com>
 * @return JSON
 * @copyright Copyright 2013 PlayIGL.com
 */
include_once('../includes/config.php');
include_once('../model/mysql.class.php');
include_once('../model/backbone.class.php');
include_once('../includes/functions.php');

$backbone = new Backbone();

switch ($_SERVER['REQUEST_METHOD']) {
  case 'POST':
    // create new item
    break;

  case 'GET':
    // get item(s)
    $backbone->getData("player", "dashboard", "profile", array("id" => $_SESSION['playerid']));
    returnJSON($backbone->data);
    break;

  case 'PUT':
    // update item
    parse_str(file_get_contents("php://input"), $post_vars);
    $profile = json_decode($post_vars['model']);
    $backbone->saveData("player", "dashboard", "profile", $profile);
    break;

  case 'DELETE':
    // delete item
    break;
}