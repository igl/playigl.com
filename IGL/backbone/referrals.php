<?php
/**
 * Serves backbone.js requests
 * @author Shaun Delaney <sdelaney@playigl.com>
 * @return JSON
 * @copyright Copyright 2013 PlayIGL.com
 */
include_once('../includes/config.php');
include_once('../model/mysql.class.php');
include_once('../model/backbone.class.php');
include_once('../includes/functions.php');

$backbone = new Backbone();

switch ($_SERVER['REQUEST_METHOD']) {
  case 'POST':
    // create new item
    //include_once('../sendgrid-php/SendGrid_loader.php');
    //$send_grid = new SendGrid('playigl', 'xeCr9feJ');
    $referral = json_decode($_POST['model']);
    $action = "send referral";
    $backbone->saveData($action, "dashboard", "referrals", $referral);
    //$send_grid->web->send($backbone->createMail($referral, "send_referral", new SendGrid\Mail()));
    break;

  case 'GET':
    // get item(s)
    $backbone->getData("referrals", "dashboard", "referrals", NULL);
    returnJSON($backbone->data);
    break;

  case 'PUT':
    // update item
    parse_str(file_get_contents("php://input"), $post_vars);
    $referral = json_decode($post_vars['model']);
    $action = $referral->action;
    unset($referral->action);

    switch ($action) {
      case "revoke referral":
        $backbone->saveData($action, "dashboard", "referrals", $referral);
        trackAction('Revoke Referral', 1, '');
        break;
    }
    break;

  case 'DELETE':
    // delete item
    break;
}