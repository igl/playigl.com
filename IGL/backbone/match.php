<?php
/**
 * Serves backbone.js requests
 * @author Shaun Delaney <sdelaney@playigl.com>
 * @return JSON
 * @copyright Copyright 2013 PlayIGL.com
 */
include_once('../includes/config.php');
include_once('../model/mysql.class.php');
include_once('../model/backbone.class.php');
include_once('../includes/functions.php');

$backbone = new Backbone();

switch ($_SERVER['REQUEST_METHOD']) {
  case 'POST':
    // create new item
    break;

  case 'GET':
    // get item(s)
    $id = isset($_GET['id']) && !empty($_GET['id']) ? (int) $_GET['id'] : NULL;
    $action = isset($_GET['action']) && !empty($_GET['action']) ? $_GET['action'] : NULL;

    switch ($action) {
      case "get all matches for player":
        $backbone->getData($action, "dashboard", "matches", NULL);
        break;

      case "get team data for score submission":
        $backbone->getData($action, "dashboard", "matches", array("id" => $id));
        break;
    }

    returnJSON($backbone->data);
    break;

  case 'PUT':
    // update item
    parse_str(file_get_contents("php://input"), $post_vars);
    $score = json_decode($post_vars['model']);
    $action = $score->action;
    unset($score->action);
    
    switch ($action) {
      case "submit match url":
        $backbone->saveData($action, "dashboard", "matches", $score);
        break;
      case "submit match score":
      case "accept challenge":
      case "reject challenge":
        $backbone->saveData($action, "dashboard", "matches", $score);
        break;
    }
    break;

  case 'DELETE':
    // delete item
    break;
}