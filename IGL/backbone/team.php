<?php
/**
 * Serves backbone.js requests
 * @author Shaun Delaney <sdelaney@playigl.com>
 * @return JSON
 * @copyright Copyright 2013 PlayIGL.com
 */
include_once('../includes/config.php');
include_once('../model/mysql.class.php');
include_once('../model/backbone.class.php');
include_once('../includes/functions.php');
include_once('../includes/pusher.php');

$backbone = new Backbone();

switch ($_SERVER['REQUEST_METHOD']) {
  case 'POST':
    // create new item
    $profile = json_decode($_POST['model']);
    $action = $profile->action;
    unset($profile->action);

    switch ($action) {
      case "create new team":
        $backbone->saveData($action, "dashboard", "teams", $profile);
        break;
    }
    break;

  case 'GET':
    // get item(s)
    $id = isset($_GET['id']) && !empty($_GET['id']) ? (int) $_GET['id'] : NULL;
    $action = isset($_GET['action']) && !empty($_GET['action']) ? $_GET['action'] : NULL;

    switch ($action) {
      case "get all player teams and invites":
      case "get all player teams":
      case "get player team":
        $backbone->getData($action, "dashboard", "teams", array("id" => $_SESSION['playerid'], "team_id" => $id));
        break;

      case "get team data":
      case "get team password":
      case "get team recruitment":
        $backbone->getData($action, "dashboard", "teams", array("id" => $id));
        break;
    }

    returnJSON($backbone->data);
    break;

  case 'PUT':
    // update item
    parse_str(file_get_contents("php://input"), $post_vars);
    $team = json_decode($post_vars['model']);
    $action = $team->action;
    unset($team->action);

    switch ($action) {
      case "create new team":
      case "accept team invite":
      case "reject team invite":
      case "save team details":
      case "save team password":
      case "leave team":
        $backbone->saveData($action, "dashboard", "teams", $team);
        break;
    }
    break;

  case 'DELETE':
    // delete item
    break;
}