<div class="span6">
  <form>
    <table class="table table-condensed">
      <tr>
        <th class="row-header white">
          <h5>INVITE FRIENDS!</h5>
        </th>
      </tr>
      <tr>
        <td>
          <input type="text" placeholder="Email..." name="email" class="loginfield">
        </td>
      </tr>
      <tr>
        <td>
          <button class="btn btn-action" type="button" name="save"><i></i> SEND</button>
        </td>
      </tr>
    </table>
  </form>
</div>
<div class="span6">
  <table class="table table-condensed">
    <tr>
      <th class="row-header white" colspan="2">
        <h5>STATS!</h5>
      </th>
    </tr>
    <tr>
      <td>
        <h6>Conversion Rate</h6>
      </td>
      <td>
        <div class="progress tooltip-on" title="Activated: <%= progress %> (<%= progress_percent %>%)">
          <div style="width:<%= progress_percent %>%; cursor:default;" class="bar tooltip-on"><%= progress_percent %>%</div>
        </div>
      </td>
    </tr>
    <% _.each(invites.models, function(invite) { %>
      <tr>
        <% if (invite.get("activated") > 0) { %>
          <td><%= invite.get("email") %></td>
          <td style="text-align:right;">
            <a href="/player/<%= invite.get("player_id") %>"><i class="icon icon-link tooltip-on" title="View Player"></i></a>
            <i class="icon icon-ok tooltip-on" title="Invite Accepted" style="color:#468847"></i>
          </td>
        <% } else { %>
          <td><%= invite.get("email") %></td>
          <td style="text-align:right;">
            <i class="icon icon-remove tooltip-on" title="Revoke Invite" data-id="<%= invite.get("id") %>" style="cursor:pointer"></i>
          </td>
        <% } %>
      </tr>
    <% }) %>
  </table>
</div>