<table class="table table-condensed table-bordered">  
<% if (scoring_method == 1 ) { %>
    <tr>
      <th colspan='5'>Submit scores for each round played</th>
    </tr>
    <% if (round_details == 1 ) { %>
      <tr>
        <th colspan='5'>Select the winner for each round.</th>
      </tr>
      <tr>
        <th>Game/Map</th>
        <th colspan="2">Home Team</th>
        <th colspan="2">Away Team</th>
      </tr>
      <% for(var i = 1; i <= rounds; i++) { %>
        <tr>
          <td><%= i %></td>
          <td class='home'><%= home_name %></td>
          <td><input type='checkbox' value='1' name='home_score[]'></td>
          <td class='away'><%= away_name %></td>
          <td><input type='checkbox' value='1' name='away_score[]'>
        </tr>
      <% } %>
    <% } else { %>
      <tr>
        <th colspan='3'>Enter exact points per map or round.</th>
      </tr>
      <tr>
        <th>Game/Map</th>
        <th>Home Team</th>
        <th>Away Team</th>
      </tr>
      <% for(var i = 1; i <= rounds; i++) { %>
        <tr>
          <td><%= i %></td>
          <td class='home'><input type='text' name='home_score[]'></td>
          <td class='away'><input type='text' name='away_score[]'></td>
        </tr>
      <% } %>
    <% } %>
    
    
    
  <% } else { %>
    <tr>
      <th colspan='2'>Submit the total score of the match.</th>
    </tr>
    <% if (round_details == 1) { %>
    <tr>
        <th colspan='2'>Reports total results for each team.</th>
      </tr>
      <tr>
        <td><%= home_name %> <input type='text' name='home_score[]'></td>
        <td><%= away_name %> <input type='text' name='away_score[]'></td>
      </tr>
    <% } else { %>
      
      <tr>
        <th colspan='2'>Select the winner</th>
      </tr>
      <tr>
        <td><%= home_name %> <input type='checkbox' value='1' name='home_score[]'></td>
        <td><%= away_name %> <input type='checkbox' value='1' name='away_score[]'></td>
      </tr>
    <% } %>
  <% } %>
  <tr><td colspan="3">Forfeit win <input type='checkbox' value='1' name='forfeit'></td></tr>
</table>

<input type="reset" value="Clear" class="btn">&nbsp;
<button class="btn" name="cancel">Cancel</button>
<button class="btn btn-action" name="save"><i></i> Submit</button>