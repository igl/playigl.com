<% now = (new Date).getTime()/1000 %>
<table class="table table-condensed table-striped table-bordered">
  <tr>
    <th class="row-header" colspan="5"><h5>Pending Matches</h5></th>
  </tr>
  <% if (challenge.models.length) { %>
    <tr>
      <th colspan="2" style="padding-left:36px">Challenges</th>
      <th>Team</th>
      <th colspan="2">Map</th>
    </tr>
    <% _.each(challenge.models, function(c) { %>
      <% if (c.get("on_challenger_team") > 0) { %>
        <tr>
          <td style="text-align:center; vertical-align:middle; width:20px;">
            <img src="/img<%= c.get("game_logo") %>" style="height:20px; width:20px;" class="tooltip-on" title="<%= c.get("game_name") %>">
          </td>
          <td><a class="tooltip-on" title="Proposed Match Time" style="cursor:default"><%= showLocalDate(c.get("match_time")) %> (GMT <%= localTimezone() %>)</a></td>
          <td><a class="tooltip-on" title="Team [<%= c.get("team_name") %>] Challenged Team [<%= c.get("this_team_name") %>]" href="/team/<%= c.get("this_team_id") %>">[<%= c.get("this_team_tag") %>]</a></td>
          <td><%= c.get("map") %></td>
          <td style="text-align:center;">
            <% if (player_id == c.get("team_captain") || player_id == c.get("team_alternate_captain")) { %>
              <button class="btn btn-danger btn-mini withdraw" data-id="<%= c.get("id") %>"><i></i> Withdraw</button>
            <% } else { %>
              Awaiting Team Captain's Response
            <% } %>
          </td>
        </tr>
      <% } else if (c.get("on_challengee_team") > 0) { %>
        <tr>
          <td style="text-align:center; vertical-align:middle; width:20px;">
            <img src="/img<%= c.get("game_logo") %>" style="height:20px; width:20px;" class="tooltip-on" title="<%= c.get("game_name") %>">
          </td>
          <td><a class="tooltip-on" title="Proposed Match Time" style="cursor:default"><%= showLocalDate(c.get("match_time")) %> (GMT <%= localTimezone() %>)</a></td>
          <td><a class="tooltip-on" title="Team [<%= c.get("team_name") %>] Challenged Team [<%= c.get("this_team_name") %>]" href="/team/<%= c.get("team_id") %>">[<%= c.get("team_tag") %>]</a></td>
          <td><%= c.get("map") %></td>
          <td style="text-align:center;">
            <% if (player_id == c.get("this_captain") || player_id == c.get("this_alternate_captain")) { %>
              <button class="btn btn-action btn-mini accept" data-id="<%= c.get("id") %>"><i></i> Accept</button>&nbsp;
              <button class="btn btn-danger btn-mini reject" data-id="<%= c.get("id") %>"><i></i> Reject</button>
            <% } else { %>
              Awaiting Team Captain's Response
            <% } %>
          </td>
        </tr>
      <% } %>
    <% }) %>
  <% } %>
  <% if (upcoming.models.length) { %>
    <tr>
      <th colspan="2" style="padding-left:36px">Matches</th>
      <th>Home</th>
      <th colspan="2">Away</th>
    </tr>
    <% _.each(upcoming.models, function(match) { %>
      <tr id="match_<%= match.get("id") %>">
        <td style="text-align:center; vertical-align:middle; width:20px;">
          <img src="/img<%= match.get("game_logo") %>" style="height:20px; width:20px;" class="tooltip-on" title="<%= match.get("game_name") %>">
        </td>
        <td><a class="tooltip-on" title="Click to view match details" href="/match/<%= match.get("id") %>"><%= showLocalDate(match.get("date")) %> (GMT <%= localTimezone() %>)</a></td>
        <td>
          <a class="tooltip-on" title="<%= match.get("home_name") %>" href="/team/<%= match.get("home_id") %>">
            <% if (match.get("home_tag").length) { %>
              <%= match.get("home_tag") %>
            <% } else { %>
              N/A
            <% } %>
          </a>
        </td>
        <td>
          <a class="tooltip-on" title="<%= match.get("away_name") %>" href="/team/<%= match.get("away_id") %>">
            <% if (match.get("away_tag").length) { %>
              <%= match.get("away_tag") %>
            <% } else { %>
              N/A
            <% } %>
          </a>
        </td>
        
        <td style="text-align:center">
            <% if ( now > (match.get("date"))) { %>
                <button class="btn btn-mini btn-info score" data-id="<%= match.get("id") %>"><i class="icon-check"></i> Submit Score</button>
                <% if ( match.get("match_url") == 1 ) { %>
                <% if (match.get("match_url_address") !== null) { %>
                  <a class="btn btn-mini btn-action" href="<%= match.get("match_url_address") %>">Play</a>
                <% } else { %>
                  <% if (player_id == match.get("home_captain") || player_id == match.get("home_alternate")) { %>
                  <div class="input-append"><input placeholder="Match URL" style="width:60px;height:13px!important;padding:0px" type="text"> <button class="btn btn-mini btn-action matchurl" type="button" data-id="<%= match.get("id") %>"><i class='icon-check'></i></button></div>
                  <% } else { %>
                  <a class="btn btn-mini btn-info tooltip-on" title="Waiting for home team to post match URL">Data Pending</a>
                  <% } %>
                <% } %>
              <% } else if (match.get("server_ip") !== null) { %>
                <a class="btn btn-mini btn-action tooltip-on" title="Enter Match!" href='steam://connect/<%= match.get("server_ip") %>:<%= match.get("server_port") %>/<%= match.get("server_password") %>'>Play</a>
              <% } else { %>
                <a class="btn btn-mini btn-action tooltip-on" title="Pending match data..." disabled="true">Play</a>
              <% } %>
            
            <% } else { %>
                <% if ( match.get("match_url") == 1 ) { %>
                <% if (match.get("match_url_address") !== null) { %>
                  <a class="btn btn-mini btn-action tooltip-on" title="Pending match time..." disabled="true">Play</a>
                <% } else { %>
                  <% if (player_id == match.get("home_captain") || player_id == match.get("home_alternate")) { %>
                  <div class="input-append"><input placeholder="Match URL" style="width:60px;height:13px!important;padding:0px" type="text"> <button class="btn btn-mini btn-action matchurl" type="button" data-id="<%= match.get("id") %>"><i class='icon-check'></i></button></div>
                  <% } else { %>
                  <a class="btn btn-mini btn-info tooltip-on" title="Waiting for home team to post match URL">Data Pending</a>
                  <% } %>
                <% } %>
              <% } else if (match.get("server_ip") !== null) { %>
                <a class="btn btn-mini btn-action tooltip-on" title="Pending match time..." disabled="true">Play</a>
              <% } else { %>
                <a class="btn btn-mini btn-action tooltip-on" title="Pending match data..." disabled="true">Play</a>
              <% } %>
            <% } %>
        </td>
      </td>
      </tr>
    <% }) %>
  <% } else { %>
    <tr>
      <td colspan="5"><p class="alert alert-error text-center" style="margin:0;">You have no other pending matches.</p></td>
    </tr>
  <% } %>
</table>

<table class="table table-condensed table-striped table-bordered">
  <tr>
    <th class="row-header" colspan="6"><h5>Completed Matches</h5></th>
  </tr>
  <% if (completed.models.length) { %>
    <tr>
      <th colspan="2" style="padding-left:36px">Date & Time</th>
      <th>Home</th>
      <th>Away</th>
      <th>Results</th>
    </tr>
    <% _.each(completed.models, function(match) { %>
      <tr>
        <td style="text-align:center; vertical-align:middle; width:20px;">
          <img src="/img<%= match.get("game_logo") %>" style="height:20px; width:20px;" class="tooltip-on" title="<%= match.get("game_name") %>">
        </td>
        <td><a class="tooltip-on" title="Click to view match details" href="/match/<%= match.get("id") %>"><%= showLocalDate(match.get("date")) %> (GMT <%= localTimezone() %>)</a></td>
        <td>
          <a class="tooltip-on" title="<%= match.get("home_name") %>" href="/team/<%= match.get("home_id") %>">
            <% if (match.get("home_tag").length) { %>
              <%= match.get("home_tag") %>
            <% } else { %>
              N/A
            <% } %>
          </a>
        </td>
        <td>
          <a class="tooltip-on" title="<%= match.get("away_name") %>" href="/team/<%= match.get("away_id") %>">
            <% if (match.get("away_tag").length) { %>
              <%= match.get("away_tag") %>
            <% } else { %>
              N/A
            <% } %>
          </a>
        </td>
        <td>
          <a class="btn btn-mini btn-danger tooltip-on disabled pull-right" title="Coming Soon">Dispute</a>
          Coming Soon!
        </td>
      </tr>
    <% }) %>
  <% } else { %>
    <tr>
      <td><p class="alert alert-warning text-center" style="margin:0;">You have no completed matches.</p></td>
    </tr>
  <% } %>
</table>