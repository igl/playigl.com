<?php
$countries = "";
$api_countries = json_decode(file_get_contents(BASEURL . "api/country"));
if ($api_countries->success) {
  foreach ($api_countries->country_list as $key => $val) {
    $countries .= <<< COUNTRY
                <option value='{$val->iso}'> {$val->printable_name}</option>
COUNTRY;
  }
}

?>

<%
  this_birthday = birthday.split("-");
  year = this_birthday[0];
  month = this_birthday[1];
  day = this_birthday[2];
%>

<h3><%= user_name %> <img class="dashboard-flag" alt="ca" src="/img/flags/<%= country %>.png"></h3>
<table class="table table-striped table-condensed">
  <tbody>
    <tr>
      <td style="padding:0;">
        <table>
          <tbody>
            <tr>
              <td style="border-top:none">
                <h4>FIRST NAME</h4>
                <input name="first_name" value="<%= first_name %>" placeholder="Ex. John" type="text">
              </td>
            </tr>
            <tr>
              <td>
                <h4>LAST NAME</h4>
                <input class="loginfield" name="last_name" value="<%= last_name %>" placeholder="ex. Smith" type="text">
              </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td>
        <a data-toggle="modal" data-target="#avatarModal">
          <img alt="Avatar" class="img-rounded dashboard-avatar tooltip-on" src="/img<%= avatar %>" height="184" width="184" title="Click to upload a new avatar">
        </a>
      </td>
    </tr>
    <tr>
      <td>
        <h4>SEX</h4>
        <select name="sex" style="height:40px;padding-top:7px;">
          <option value="MALE">MALE</option>
          <option value="FEMALE">FEMALE</option>
        </select>
      </td>
      <td>
        <!--<h4 class="pull-right">Age</h4>-->
        <h4 class="pull-left">DATE OF BIRTH</h4>
        <select class="pull-left" style="width:70px; clear:left; margin-right:5px;" name="birthday-year">
          <% for(var i = new Date().getFullYear(); i > new Date().getFullYear()-65; i--) { %>
            <% if (year == i) { %>
              <option value="<%= i %>" selected><%= i %></option>
            <% } else { %>
              <option value="<%= i %>"><%= i %></option>
            <% } %>
          <% } %>
        </select>
        <select class="pull-left" style="width:50px; margin-right:5px;" name="birthday-month">
          <% for(var i = 1; i <= 12; i++) { %>
            <% if (month == i) { %>
              <option value="<%= i %>" selected><%= i %></option>
            <% } else { %>
              <option value="<%= i %>"><%= i %></option>
            <% } %>
          <% } %>
        </select>
        <select class="pull-left" style="width:50px; margin-right:5px;" name="birthday-day">
          <% for(var i = 1; i <= 31; i++) { %>
            <% if (day == i) { %>
              <option value="<%= i %>" selected><%= i %></option>
            <% } else { %>
              <option value="<%= i %>"><%= i %></option>
            <% } %>
          <% } %>
        </select>
        <!--<input class="pull-right" style="width:40px;" name="age" value="" placeholder="" type="text">-->
      </td>
    </tr>
    <tr>
      <td>
        <h4>COUNTRY</h4>
        <select style="height:40px;padding-top:7px;" name="country">
<?php echo $countries; ?>
        </select>
      </td>
      <td>
        <h4>TIME ZONE</h4>
        <select name="time_zone_offset" style="height:40px;padding-top:7px;">
          <option value="-12">(GMT -12:00) Eniwetok, Kwajalein</option>
          <option value="-11">(GMT -11:00) Midway Island, Samoa</option>
          <option value="-10">(GMT -10:00) Hawaii</option>
          <option value="-9">(GMT -9:00) Alaska</option>
          <option value="-8">(GMT -8:00) Pacific Time (US &amp; Canada)</option>
          <option value="-7">(GMT -7:00) Mountain Time (US &amp; Canada)</option>
          <option value="-6">(GMT -6:00) Central Time (US &amp; Canada), Mexico City</option>
          <option value="-5">(GMT -5:00) Eastern Time (US &amp; Canada), Bogota, Lima</option>
          <option value="-4">(GMT -4:00) Atlantic Time (Canada), Caracas, La Paz</option>
          <option value="-3.5">(GMT -3:30) Newfoundland</option>
          <option value="-3">(GMT -3:00) Brazil, Buenos Aires, Georgetown</option>
          <option value="-2">(GMT -2:00) Mid-Atlantic</option>
          <option value="-1">(GMT -1:00 hour) Azores, Cape Verde Islands</option>
          <option value="0">(GMT) Western Europe Time, London, Lisbon, Casablanca</option>
          <option value="1">(GMT +1:00 hour) Brussels, Copenhagen, Madrid, Paris</option>
          <option value="2">(GMT +2:00) Kaliningrad, South Africa</option>
          <option value="3">(GMT +3:00) Baghdad, Riyadh, Moscow, St. Petersburg</option>
          <option value="3.5">(GMT +3:30) Tehran</option>
          <option value="4">(GMT +4:00) Abu Dhabi, Muscat, Baku, Tbilisi</option>
          <option value="4.5">(GMT +4:30) Kabul</option>
          <option value="5">(GMT +5:00) Ekaterinburg, Islamabad, Karachi, Tashkent</option>
          <option value="5.5">(GMT +5:30) Bombay, Calcutta, Madras, New Delhi</option>
          <option value="6">(GMT +6:00) Almaty, Dhaka, Colombo</option>
          <option value="7">(GMT +7:00) Bangkok, Hanoi, Jakarta</option>
          <option value="8">(GMT +8:00) Beijing, Perth, Singapore, Hong Kong</option>
          <option value="9">(GMT +9:00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk</option>
          <option value="9.5">(GMT +9:30) Adelaide, Darwin</option>
          <option value="10">(GMT +10:00) Eastern Australia, Guam, Vladivostok</option>
          <option value="11">(GMT +11:00) Magadan, Solomon Islands, New Caledonia</option>
          <option value="12">(GMT +12:00) Auckland, Wellington, Fiji, Kamchatka</option>
        </select>
      </td>
    </tr>
    <tr>
      <td>
        <h4>STATE/PROVINCE</h4>
        <input class="loginfield" name="state" value="<%= state %>" placeholder="ex. New York" type="text">
      </td>
      <td>
        <h4>CITY/TOWN</h4>
        <input class="loginfield" name="city" value="<%= city %>" placeholder="ex. New York City" type="text">
      </td>
    </tr>
    <tr>
      <td>
        <h4>WEBSITE</h4>
        <input class="loginfield" name="website" value="<%= website %>" placeholder="ex. www.playigl.com" type="text">
      </td>
      <td>
        <h4>CHANGE PASSWORD</h4>
        <div class="input-prepend">
          <span class="add-on" style="height:24px; padding-top:10px;">
            <i class="icon-lock"></i>
          </span>
          <input class="social" name="password" type="password">
        </div>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <h4>In-Game Names</h4>
      </td>
    </tr>
    <%
      var game_counter = 1;
      
      for (game in game_labels) { %>
        <% if (game_counter % 2 == 1) { %>
          <tr>
        <% } %>
          <td>
            <h5><%= game_labels[game].name %></h5>
            <% if (ingame_name !== null) { %>
              <input name="game_label" data-id="<%= game_labels[game].id %>" value="<%= ingame_name[game_labels[game].id] %>">
            <% } else { %>
              <input name="game_label" data-id="<%= game_labels[game].id %>">
            <% } %>
          </td>
        <% if (game_counter % 2 == 0) { %>
          </tr>
        <% } %>
        <% game_counter++ %>
    <% } %>
    <tr>
      <td colspan="2">
        <h4>Social Connections</h4>
        <div class="social-connect">
          <input id="twitter_token" name="twitter_token" value="<%= twitter_token %>" type="hidden" />
          <input id="facebook_token" name="facebook_token" value="<%= facebook_token %>" type="hidden" />
          <input id="community_id" name="community_id" value="<%= community_id %>" type="hidden" />
          <input id="twitch" name="twitch" value="<%= twitch %>" type="hidden" />
          <% if (community_id) { %>
          <a class="connect-btn steam"><div class="icon-wrap"><img class="icon" src="/img/register/icon-steam.png"></div> <span><%= community_id %></span></a> <i class="steami icon-ok icon-2x tooltip-on" title="Connect with Steam: Connect your Steam account to track your statistics and join matches with lightning speed. "></i>
          <% } else { %>
          <a class="connect-btn steam" onclick="Registration.connectSteam();"><div class="icon-wrap"><img class="icon" src="/img/register/icon-steam.png"></div> <span>Connect With Steam</span></a> <i class="steami icon-question-sign icon-2x tooltip-on" title="Connect with Steam: Connect your Steam account to track your statistics and join matches with lightning speed. "></i>
          <% } %>
          <% if (twitch) { %>
          <a class="connect-btn twitch"><div class="icon-wrap"><img class="icon" src="/img/register/icon-twitch.png"></div> <span>Connected</span></a> <i class="twitchi  icon-ok icon-2x tooltip-on" title="Connect with Twitch: Connect your Twitch account to share your livestreamed content with the world."></i>
          <% } else { %>
          <a class="connect-btn twitch" onclick="Registration.connectTwitch();"><div class="icon-wrap"><img class="icon" src="/img/register/icon-twitch.png"></div> <span>Connect With Twitch</span></a> <i class="twitchi icon-question-sign icon-2x tooltip-on" title="Connect with Twitch: Connect your Twitch account to share your livestreamed content with the world."></i>
          <% } %>
          <% if (facebook_token) { %>
          <a class="connect-btn facebook"><div class="icon-wrap"><img class="icon" src="/img/register/icon-facebook.png"></div> <span><%= facebook %></span></a> <i class="facebooki  icon-ok icon-2x tooltip-on" title="Connect with Facebook: Connect with Facebook to expand your network and invite your friends."></i>
          <% } else { %>
          <a class="connect-btn facebook" onclick="Registration.connectFacebook();"><div class="icon-wrap"><img class="icon" src="/img/register/icon-facebook.png"></div> <span>Connect With Facebook</span></a> <i class="facebooki icon-question-sign icon-2x tooltip-on" title="Connect with Facebook: Connect with Facebook to expand your network and invite your friends."></i>
          <% } %>
          <% if (twitter_token) { %>
          <a class="connect-btn twitter"><div class="icon-wrap"><img class="icon" src="/img/register/icon-twitter.png"></div> <span><%= twitter %></span></a> <i class="twitteri icon-ok icon-2x tooltip-on" title="Connect with Twitter: Connect with Twitter to share the @playigl experience with your followers."></i>
          <% } else { %>
          <a class="connect-btn twitter" onclick="Registration.connectTwitter();"><div class="icon-wrap"><img class="icon" src="/img/register/icon-twitter.png"></div> <span>Connect With Twitter</span></a> <i class="twitteri icon-question-sign icon-2x tooltip-on" title="Connect with Twitter: Connect with Twitter to share the @playigl experience with your followers."></i>
          <% } %>
        </div>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <h4>APPLY CHANGES</h4>
        <button type="button" name="save" class="btn btn-action"><i></i> SAVE</button>
      </td>
    </tr>
    <tr>
      <td></td>
      <td></td>
    </tr>
  </tbody>
</table>