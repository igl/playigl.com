<label style="display:inline;">Password (8 Characters):</label>
<input type="text" name="password" maxlength="8" class="tooltip-on" title="Combination of 8 letters & numbers">
<button class="btn btn-confirm" name="save" type="button"><i></i> Save</button>
<% if (password == true) { %>
  <button class="btn btn-danger" name="remove" type="button"><i></i> Remove</button>
<% } %>
<button class="btn" name="close" type="button">Close</button>