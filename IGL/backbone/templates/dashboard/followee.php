<table class="table table-condensed table-striped">
  <% if (!follows.models.length) { %>
    <tr>
      <td colspan="2">You are not following anyone.</td>
    </tr>
  <% } else { %>
    <% _.each(follows.models, function(follow) { %>
    <tr>
      <td width="460px">
        <img alt="Avatar" class="img-rounded smallthumb" style="margin-right: 6px; vertical-align:middle" src="/img<%= follow.get("avatar") %>">
        <h6 style="display:inline;">
          <img alt="<%= follow.get("country") %>" src="/img/flags/<%= follow.get("country") %>.png">
          <a href="/player/<%= follow.get("user_name") %>"><%= follow.get("first_name") %> "<%= follow.get("user_name") %>" <%= follow.get("last_name") %></a>
        </h6>
      </td>
      <td>
        <div class="text-right">
          <i class="icon-check icon-large icon-white"></i> Following
        </div>
      </td>
    </tr>
    <% }) %>
  <% } %>
</table>
