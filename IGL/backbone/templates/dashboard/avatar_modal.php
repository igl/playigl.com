<div id="avatarModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Avatar</h3>
  </div>
  <div class="modal-body">
    <iframe style="display:none;" name="avatar-upload-frame"></iframe>
    <form id="upload_avatar" class="text-right" enctype="multipart/form-data" target="avatar-upload-frame" method="post" action="/avatar.php">
      <img alt="Avatar" class="img-rounded" src="<%= avatar %>" height="184" width="184">
      <div class="pull-left text-left">
        <label>Upload New Avatar:</label><br>
        <input accept="image/jpg,image/png,image/jpeg" name="avatar" type="file">
      </div>      
      <input type="hidden" value="<%= media_type %>" name="media_type">
      <input type="hidden" value="<%= field_name %>" name="field">
      <input type="hidden" value="<%= id %>" name="id">
    </form>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
  </div>
</div>