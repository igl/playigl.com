<?php
$locations = array("continents" => array(), "regions" => array());
$api_locations = json_decode(file_get_contents(BASEURL . 'ajax/admin/server_locations.php'), true);
foreach ($api_locations['continents'] as $l)
  $location['continents'] .= "<option value='{$l['continent']}'>{$l['continent']}</option>";

foreach ($api_locations['regions'] as $l)
  $location['regions'] .= "<option value='{$l['region']}'>{$l['region']}</option>";
?>

<table class="table" style="margin:0;">
  <tr>
    <td rowspan="2">
      <a data-toggle="modal" data-target="#avatarModal">
        <img alt="Avatar" class="img-rounded dashboard-avatar tooltip-on" src="/img<%= avatar %>" height="184" width="184" title="Click to upload a new avatar">
      </a>
    </td>
    <td>
      <h4>Tag</h4>
      <input type="text" value="<%= tag %>" placeholder="" name="tag">
    </td>
  </tr>
  <tr>
    <td>
      <h4>Website</h4>
      <input type="text" value="<%= website %>" placeholder="ex: http://www.playigl.com/" name="website">
    </td>
  </tr>
  <tr>
    <td>
      <h4>Twitter</h4>
      <div class="input-prepend">
        <span style="height:24px; padding-top:10px;" class="add-on">@</span>
        <input type="text" value="<%= twitter %>" placeholder="ex: playigl" name="twitter">
      </div>
    </td>
    <td>
      <h4>IRC</h4>
      <input type="text" value="<%= irc %>" placeholder="ex: #playigl" name="irc">
    </td>
  </tr>
  <tr>
    <td>
      <h4>Steam Group</h4>
      <input type="text" value="<%= steam_group %>" placeholder="ex: playigl" name="steam_group">
    </td>
    <td>
      <h4 class="tooltip-on" title="Allow random players to send join requests">Allow Applicants</h4>
      <select id="allowApplicants" name="open">
        <option value="1"<% if (open == 1) { %> selected<% } %>>Yes</option>
        <option value="0"<% if (open == 0) { %> selected<% } %>>No</option>
      </select>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <h4>Location</h4>
      <select class="span2" name='continent'>
          <option selected disabled style="display:none;" value="<%= continent %>"><%= continent %></option>
        <?php echo $location['continents']; ?>
      </select>
      <select name='region'>
          <option selected disabled style="display:none;" value="<%= region %>"><%= region %></option>
        <?php echo $location['regions']; ?>
      </select>
    </td>
  </tr>
</table>
<div class="form-actions clear" style="margin-top:0;">
  <button class="btn btn-primary" type="button" name="save"><i></i> Save Changes</button>
  <button class="btn" type="button" name="cancel">Cancel</button>
</div>