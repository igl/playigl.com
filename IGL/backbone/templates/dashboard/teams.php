<p class="text-right" style="margin:0 0 20px;"><button class="btn btn-action" name="create">CREATE TEAM</button></p>
<div class="create-team-form"></div>
<table class="table table-bordered table-condensed">
  <tbody>
    <% if (invites.models.length > 0) { %>
      <tr id="invite-requests" class="section-head">
        <th class="row-header" colspan="3">
          <h5>Invite Requests</h5>
        </th>
      </tr>
      <% _.each(invites.models, function(invite) { %>
        <tr>
          <td width="40px">
            <img alt="Game" class="img-rounded img-profile-small" src="/img<%= invite.get("game_logo") %>">
          </td>
          <td width="40px">
            <a href="/team/<%= invite.get("team_id") %>">
              <img alt="<%= invite.get("team_name") %>" class="img-rounded img-profile-small" src="/img<%= invite.get("team_logo") %>">
            </a>
          </td>
          <td>
            <a href="/team/<%= invite.get("team_id") %>"><%= invite.get("team_name") %></a><br>
            <button class="btn btn-mini btn-action accept" data-id="<%= invite.get("id") %>"><i></i> Accept Invite</button>
            <button class="btn btn-mini btn-red reject" data-id="<%= invite.get("id") %>"><i></i> Reject Invite</button>
          </td>
        </tr>
      <% }); %>
    <% } %>
    <tr id="current-teams" class="section-head">
      <th class="row-header" colspan="3">
        <h5>Current Teams</h5>
      </th>
    </tr>
    <% if (teams.models.length <= 0) { %>
      <tr>
        <td colspan="3"><p class="alert alert-error text-center" style="margin:0;">You are not part of any teams.</p></td>
      </tr>
    <% } else { %>
      <% _.each(teams.models, function(team) { %>
        <tr>
          <td width="40px">
            <img alt="Game" class="img-rounded img-profile-small" src="/img<%= team.get("game_logo") %>">
          </td>
          <td width="40px">
            <a href="/team/<%= team.get("id") %>">
              <img alt="<%= team.get("name") %>" class="img-rounded img-profile-small" src="/img<%= team.get("logo") %>">
            </a>
          </td>
          <td>
            <a href="/team/<%= team.get("id") %>"><%= team.get("name") %></a><br>
            <button class="btn btn-mini btn-info tooltip-on followers" data-id="<%= team.get("id") %>" title="<%= team.get("name") %> have <%= team.get("followers") %> Followers">
              <% if (team.get("followers")) { %>
                <i class="icon-star"></i> <%= team.get("followers") %>
              <% } else { %>
                <i class="icon-star-empty"></i>
              <% } %>
            </button>
            <% if (team.get("captain") == team.get("player_id") || team.get("alternate") == team.get("player_id")) {%>
              <button class="btn btn-mini tooltip-on details" data-id="<%= team.get("id") %>" title="Team Details">
                <i class="icon-pencil"></i>
              </button>
              <% if (team.get("password") == true) { %>
                <button class="btn btn-mini tooltip-on password" data-id="<%= team.get("id") %>" title="Password Enabled">
                  <i class="icon-lock tooltip-on"></i>
                </button>
              <% } else { %>
                <button class="btn btn-mini tooltip-on password" data-id="<%= team.get("id") %>" title="Set Join Password">
                  <i class="icon-key"></i>
                </button>
              <% } %>
              <button class="btn btn-mini build" data-id="<%= team.get("id") %>">Team Builder</button>
              <% if (team.get("league") == null || team.get("league") == 0) { %>
                <button class="btn btn-mini tooltip-on disabled" title="Coming soon">Join League</button>
              <% } else { %>
                <button class="btn btn-mini btn-red" data-id="<%= team.get("id") %>">Leave League</button>
              <% } %>
              <% if (team.get("ladder") == null || team.get("ladder") == 0) { %>
                <button class="btn btn-mini tooltip-on disabled" title="Coming soon">Join Ladder</button>
              <% } else { %>
                <button class="btn btn-mini btn-red" data-id="<%= team.get("id") %>">Leave Ladder</button>
              <% } %>
            <% } %>
            <% if (team.get("player_id") != team.get("captain")) { %>
              <button class="btn btn-mini btn-red leave" data-id="<%= team.get("id") %>">Leave Team</button>
            <% } else { %>
              <% if (team.get("total_players") > 1) { %>
                <button class="btn btn-mini btn-red tooltip-on disabled" title="You must remove all players, or designate a new captain, before leaving">Leave Team</button>
              <% } else { %>
                <button class="btn btn-mini btn-red leave" data-id="<%= team.get("id") %>">Leave Team</button>
              <% } %>
            <% } %>
          </td>
        </tr>
      <% }); %>
    <% } %>
  </tbody>
</table>