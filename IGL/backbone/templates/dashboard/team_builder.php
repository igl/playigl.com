<% if (game_id == 9) { %>
<h5>Import Existing UGC Team Roster <span style="cursor: pointer" onclick='aboutImport()'><i class="icon-question-sign"></i></span></h5>
    <div class="input-append">
    <input class="span1" id="clan_id" type="text" style="height:20px!important;" placeholder="Clan ID">
    <button class="btn import-ugc">Import UGC Roster <i class="icon-cloud-download"></i></button>
    </div>
<% } %>
<h5>
  Current Roster (<%= roster.models.length %>) &nbsp;&nbsp;
  <% for(var i = 0; i < roster.models.length; i++) { %>
      <span class="icon-user" style="color:#777;"></span>
  <% } %>
</h5>
<ul>
  <% if (!roster.models.length) { %>
    <li style="line-height:30px;">
      <p style="margin:0;">Currently empty</p>
    </li>
  <% } %>
  <% _.each(roster.models, function(player) { %>
    <li style="line-height:30px;">
      <div class="pull-right">
        <% if (player.get("id") == captain_id) { %>
        <% } else { %>
          <% if (player.get("id") == alternate_id) { %>
            <button class="btn btn-mini tooltip-on demote-alternate btn-danger" data-id="<%= player.get("id") %>" style="vertical-align:middle;" title="Demote <%= player.get("user_name") %> from Alternate Captain"><i class="icon icon-white icon-double-angle-down"></i></button>
            <button class="btn btn-mini btn-action tooltip-on" style="vertical-align:middle; cursor:default;" title="Alternate Captain" onclick="return false"><i class="icon icon-double-angle-up"></i></button>
          <% } else { %>
            <button class="btn btn-mini tooltip-on promote-alternate btn-success" data-id="<%= player.get("id") %>" style="vertical-align:middle;" title="Promote <%= player.get("user_name") %> to Alternate Captain"><i class="icon icon-white icon-double-angle-up"></i></button>
          <% } %>
          <button class="btn btn-mini tooltip-on promote-captain" data-id="<%= player.get("id") %>" style="vertical-align:middle;" title="Promote <%= player.get("user_name") %> to Captain"><i class="icon icon-white icon-trophy"></i></button>
          <button class="btn btn-mini btn-red tooltip-on remove-player" data-id="<%= player.get("id") %>" style="vertical-align:middle;" title="Remove <%= player.get("user_name") %> from Team"><i class="icon icon-white icon-remove"></i></button>
        <% } %>
      </div>
      <p style="margin:0;">
        <img style="vertical-align:middle;" src="/img<%= player.get("avatar") %>" height="20" width="20">&nbsp;
        <img style="vertical-align:middle;" src="/img/flags/<%= player.get("country") %>.png" height="20">&nbsp;
        <%= player.get("first_name") %> "<%= player.get("user_name") %>" <%= player.get("last_name") %>
        <% if (player.get("id" != captain_id)) { %>
          <a class="icon icon-link tooltip-on" title="View this player's profile" href="/player/<%= player.get("user_name") %>"></a>
        <% } %>
      </p>
    </li>
  <% }) %>
</ul>
<h5>Pending Invites from Team Captain (<%= invites.models.length %>)</h5>
<div style="max-height:300px; overflow:auto;">
  <ul>
    <% if (!invites.models.length) { %>
      <li style="line-height:30px;">
        <p style="margin:0;">Currently no Invites are pending</p>
      </li>
    <% } %>
    <% _.each(invites.models, function(invite) { %>
      <li style="line-height:30px;">
        <div class="pull-right"><button class="btn btn-mini btn-red tooltip-on cancel-invite" data-id="<%= invite.get("id") %>" style="vertical-align:middle;" type="button" title="Cancel Pending Invite to <%= invite.get("username") %>"><i class="icon icon-white icon-remove"></i></button></div>
        <p style="margin:0;">
          <img style="vertical-align:middle;" src="/img<%= invite.get("avatar") %>" height="20" width="20">&nbsp;
          <img style="vertical-align:middle;" src="/img/flags/<%= invite.get("country") %>.png" height="20">&nbsp;
          <%= invite.get("first_name") %> "<%= invite.get("user_name") %>" <%= invite.get("last_name") %>
          <a class="icon icon-link tooltip-on" title="View this player's profile" href="/player/<%= invite.get("user_name") %>"></a>
        </p>
      </li>
    <% }) %>
  </ul>
</div>
<h5>Join Requests from Players (<%= joins.models.length %>)</h5>
<div style="max-height:300px; overflow:auto;">
  <ul>
    <% if (!joins.models.length) { %>
      <li style="line-height:30px;">
        <p style="margin:0;">Currently there are no Join Requests</p>
      </li>
    <% } %>
    <% _.each(joins.models, function(join) { %>
      <li style="line-height:30px;">
        <div class="pull-right">
          <button class="btn btn-mini btn-action tooltip-on accept-join" data-id="<%= join.get("id") %>" style="vertical-align:middle;" type="button" title="Accept Join Request from <%= join.get("username") %>"><i class="icon icon-white icon-ok"></i></button>
          <button class="btn btn-mini btn-red tooltip-on reject-join" data-id="<%= join.get("id") %>" style="vertical-align:middle;" type="button" title="Reject Join Request from <%= join.get("username") %>"><i class="icon icon-white icon-remove"></i></button>
        </div>
        <p style="margin:0;">
          <img style="vertical-align:middle;" src="/img<%= join.get("avatar") %>" height="20" width="20">&nbsp;
          <img style="vertical-align:middle;" src="/img/flags/<%= join.get("country") %>.png" height="20">&nbsp;
          <%= join.get("first_name") %> "<%= join.get("user_name") %>" <%= join.get("last_name") %>
          <a class="icon icon-link tooltip-on" title="View this player's profile" href="/player/<%= join.get("user_name") %>"></a>
        </p>
      </li>
    <% }) %>
  </ul>
</div>
<h5>Invite Players by Email:</h5>
<div>
  Email Address: <input class="input-medium email-addr" type="text" style="height:13px !important;">&nbsp;<button class="btn btn-mini tooltip-on invite-email" title="Invite to Register & Join the Team"><i class="icon-envelope"></i> Invite</button>
</div>
<h5>Free Agents in <%= game_name %> (<%= free_agents.models.length %>)</h5>
<div style="max-height:300px; overflow:auto;">
  <ul>
    <% if (!free_agents.models.length) { %>
      <li style="line-height:30px;">
        <p style="margin:0;">Currently, there are no Free Agents available</p>
      </li>
    <% } %>
    <% _.each(free_agents.models, function(agent) { %>
      <% if (!_.contains(invites.pluck("id"), agent.get("id")) && !_.contains(joins.pluck("id"), agent.get("id"))) { %>
        <li style="line-height:30px;">
          <div class="pull-right">
            <button class="btn btn-mini tooltip-on invite-player" data-id="<%= agent.get("id") %>" title="Invite <%= agent.get("username") %> to Join the Team"><i class="icon-envelope"></i> Invite</button>
          </div>
          <p style="margin:0;">
            <img style="vertical-align:middle;" src="/img<%= agent.get("avatar") %>" height="20" width="20">&nbsp;
            <%= agent.get("first_name") %> "<%= agent.get("user_name") %>" <%= agent.get("last_name") %>
            <a class="icon icon-link tooltip-on" title="View this player's profile" href="/player/<%= agent.get("user_name") %>"></a>
          </p>
        </li>
      <% } %>
    <% }) %>
  </ul>
</div>
<% if (!target) { %>
<div style="margin-top:0;" class="form-actions clear">
  <button name="close" type="button" class="btn">Close</button>
</div>
<% } %>
