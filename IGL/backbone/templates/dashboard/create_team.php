<?php
$locations = array("continents" => array(), "regions" => array());
$api_locations = json_decode(file_get_contents(BASEURL . 'ajax/admin/server_locations.php'), true);
foreach ($api_locations['continents'] as $l)
  $location['continents'] .= "<option value='{$l['continent']}'>{$l['continent']}</option>";

foreach ($api_locations['regions'] as $l)
  $location['regions'] .= "<option value='{$l['region']}'>{$l['region']}</option>";
?>

<h4>Create New Team</h4>
<table class="table">
  <tr>
    <td>
      <h5>TEAM NAME</h5>
      <input class="tooltip-on" name='name' type='text' placeholder='Enter Team Name' maxlength="20" title="Max length 20 characters">
    </td>
    <td>
      <h5>Location</h5>
      <select class="span2" name='continent'>
          <option value='' disabled selected style='display:none;'>Continent...</option>
        <?php echo $location['continents']; ?>
      </select>
      <select class="span2" name='region'>
          <option value='' disabled selected style='display:none;'>Region...</option>
        <?php echo $location['regions']; ?>
      </select>
    </td>
  </tr>
  <tr>
    <td>
      <h5>TAG</h5>
      <input class="tooltip-on" name='tag' type='text' placeholder='Enter Tag' maxlength="5" title="Max length 5 characters">
    </td>
    <td>
      <h5>GAME</h5>
      <select id='game' name='game'>";
        <option value='' disabled selected style='display:none;'>Select game...</option>
        <% _.each(all_games, function(game) { %>
          <% if (!_.contains(disallowed_games, game.id)) { %>
            <option value="<%= game.id %>"><%= game.name %></option>
          <% } %>
        <% }) %>
    </td>
  </tr>
</table>
<div style="margin-top:0;" class="form-actions clear">
  <button name="save" type="button" class="btn btn-primary"><i></i> Create Team</button>
  <button name="close" type="button" class="btn">Cancel</button>
</div>